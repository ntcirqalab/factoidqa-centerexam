package org.kachako.components.analysis_engine;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.StringArray;
import org.apache.uima.jcas.cas.TOP;
import org.apache.uima.resource.ResourceInitializationException;

import org.kachako.share.util.ZipUtil;
import org.kachako.types.qa.AnswerType;
import org.kachako.types.qa.Document;
import org.kachako.types.qa.KeyTerm;
import org.kachako.types.qa.Query;
import org.kachako.types.qa.Question;
import org.kachako.types.qa.ViewName;
import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

import edu.cmu.lti.indri.RunQueryCommandWrapper;
import edu.cmu.lti.meqa.component.QueryFormulator;
import edu.cmu.lti.meqa.impl.baseline.IndriRetrievalStrategist;
import edu.cmu.lti.meqa.impl.baseline.SimpleCharBasedIndriQF;
import edu.cmu.lti.nlp.wrapper.ja.util.Converter;
import edu.cmu.lti.sepia.infra.run.model.InfraAnswerType;
import edu.cmu.lti.sepia.infra.run.model.InfraDocument;
import edu.cmu.lti.sepia.infra.run.model.InfraQuestionAnalysisResult;
import edu.cmu.lti.sepia.infra.run.model.InfraRetrievalResult;
import edu.cmu.lti.sepia.infra.run.model.InfraTerm;

public class IndriAnalysisEngine extends JCasAnnotator_ImplBase
{
	/* RS実行結果 VIEW名 PREFIX */
	private static final String RS_RESULT_VIEW_NAME_PREFIX	= "RS";

	private final String PARAM_DEFAULT_SERVER	= "IndriDefaultServer";		// Indri サーバ名
	private final String PARAM_INDICES_PATH	= "IndriIndicesFileDirectory";	// Indri インデックスファイル名
	private final String PARAM_INDRI_COMMAND	= "IndriCommandFile";		// Indri 実行コマンド
	private final String PARAM_ENCODING		= "IndriEncoding";			// Indri 文字エンコード
	private final String PARAM_INDRI_MAX_DOCS	= "IndriMaxDocs";			// Indri 最大検索件数
	private final String PARAM_INDRI_MODE		= "IndriMode";				// Indri モード
	private final String PARAM_SOFA_PREFIX		= "SofaPrefix";			// Indri実行対象Sofa名のPrefix
	private final String PARAM_DOC_DIRECTORY	= "DocumentDirectory"; // Indriの検索ドキュメントファイル保存ディレクトリ
	private final String PARAM_KEY_WEIGHT	= "KeyWeight";	// キーワードの重み付けするか

	private static String defaultServer	= null;
	private static String indicesFilePath	= null;
	private static String indriCommand	= null;
	private static String encoding			= null;
	private static int indriMaxDocs		= 0;
	private static String indriMode			= null;
	private static String sofaPrefix		= null;
	private static String docDirectory		= null;
	private static boolean keyWeight	= false;

	private QueryFormulator qf	= null;
	private Path docDirPath = null;
	private Path processDirPath	= null;

	private final String INDEX_SEPARATOR = "\t";
	private final String LINE_SEPARATOR = "\n";
	private final String QUERY_SEPARATOR = " ";

	private int rs_count = 1;
	
	@Override
	public void initialize(UimaContext aContext)
			throws ResourceInitializationException {
		super.initialize(aContext);

		// サーバ名取得
		defaultServer = (String)aContext.getConfigParameterValue(PARAM_DEFAULT_SERVER);

		// インデックスファイル名取得
		indicesFilePath = (String)aContext.getConfigParameterValue(PARAM_INDICES_PATH);

		// Indri実行コマンド取得
		indriCommand = (String)aContext.getConfigParameterValue(PARAM_INDRI_COMMAND);

		// 文字エンコード取得
		encoding = (String)aContext.getConfigParameterValue(PARAM_ENCODING);

		// Indri 最大検索件数取得
		indriMaxDocs = (int)aContext.getConfigParameterValue(PARAM_INDRI_MAX_DOCS);

		// Indriモード取得
		indriMode = (String)aContext.getConfigParameterValue(PARAM_INDRI_MODE);

		// Indri実行対象Sofa名のPrefix取得
		sofaPrefix = (String)aContext.getConfigParameterValue(PARAM_SOFA_PREFIX);

		// Indriの検索ドキュメントファイル保存ディレクトリ取得
		docDirectory = (String)aContext.getConfigParameterValue(PARAM_DOC_DIRECTORY);

		docDirPath	= Paths.get(docDirectory);
		if(!Files.exists(docDirPath)) {
			// ディレクトリなければ作成
			try {
				Files.createDirectory(docDirPath);
			} catch(IOException e) {
				e.printStackTrace();
			}
		}

		// キーワードの重み付けするかの取得
		keyWeight = (boolean)aContext.getConfigParameterValue(PARAM_KEY_WEIGHT);
		
		rs_count = 1;
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException
	{
System.out.println("----------IndriAnalysisEngine start----------");

		try {
			Iterator<JCas> jCasIterator = jCas.getViewIterator(sofaPrefix);

			if(jCasIterator != null) {
//				int	count	= 1;
				while(jCasIterator.hasNext()) {
					JCas	targetViewCas	= jCasIterator.next();
					String	targetViewName	= targetViewCas.getViewName();
System.out.println("○ targetViewCas found");
					Iterator<TOP> questionFSIterator = targetViewCas.getJFSIndexRepository().getAllIndexedFS(Question.type);
					String	questionText	= null;
					if(questionFSIterator.hasNext()) {
						// 質問文 取得
						Question	questionFS	= (Question)questionFSIterator.next();
						questionText	= questionFS.getCoveredText();
System.out.println("questionText=["+questionText+"]");
					} else {
System.out.println("× questionText not found");
					}

					String	topicId	= null;
					Iterator<TOP> sourceDocInfoFSIterator = targetViewCas.getJFSIndexRepository().getAllIndexedFS(SourceDocumentInformation.type);
					if(sourceDocInfoFSIterator.hasNext()) {
						// TOPIC_ID 取得
						topicId = ((SourceDocumentInformation) sourceDocInfoFSIterator.next()).getUri();
System.out.println("topic id="+topicId);
					} else {
System.out.println("× SourceDocumentInformation not found");
					}

					if(questionText == null || topicId == null) {
	System.out.println("×× DocumentText and SourceDocumentInformation not found");
//						continue;
					}

					List<KeyTerm> keytermList	= getKeyTermList(targetViewCas);
					List<AnswerType> answerTypeList	= getAnswerTypeList(targetViewCas);

					// Query設定
					StringBuilder sbQuery	= new StringBuilder();
					for(KeyTerm keyTerm : keytermList) {
						String text	= keyTerm.getCoveredText();
						sbQuery.append(text).append(QUERY_SEPARATOR);
					}
					String query	= sbQuery.toString().trim();

					setQuery(targetViewCas, query);

					// InfraQuestionクラス生成
					//InfraQuestion	question = new InfraQuestion(topicId,questionText);
					// InfraQuestionAnalysisResult取得
					InfraQuestionAnalysisResult qaResult	= getQuestionAnalysisResult(keytermList, answerTypeList);

					// Indriの実行
					InfraRetrievalResult	rsResult	= runIndri(qaResult,targetViewCas);
					if(rsResult == null || rsResult.size() <= 0) {
System.out.println("runIndri null.");
						continue;
					}

					//---- TargetLanguageQuestonViewへView間の依存関係を設定 ----
					ViewName targetViewNameFS	= new ViewName(targetViewCas);
					targetViewNameFS.addToIndexes();
					StringArray fsChildren		= new StringArray(targetViewCas,1);
					String rsViewName = String.format("%s.%d", RS_RESULT_VIEW_NAME_PREFIX, rs_count);
					JCas rsViewCas	= jCas.createView(rsViewName);

					//---- RSへView間の依存関係を設定 ----
					ViewName rsViewNameFS	= new ViewName(rsViewCas);
					rsViewNameFS.addToIndexes();
					StringArray fsParents		= new StringArray(rsViewCas,1);
					fsParents.set(0, targetViewName);
					fsChildren.set(0, rsViewName);
					rsViewNameFS.setParents(fsParents);
					targetViewNameFS.setChildren(fsChildren);

					int rank	= 1;
					StringBuilder sb	= new StringBuilder();
					for(InfraDocument rsDocument : rsResult) {
						String	docId		= rsDocument.getDocid();
						String	content	= rsDocument.getContent();
						double score	= rsDocument.getScore();
						String	text		= rsDocument.getText();
						//String	title	= rsDocument.getTitle();

//System.out.println("document Docid="+docId+", Score="+score
//								+", content="+content+", title="+title
//								+", text=["+text+"]");

						if(content == null || text == null) {
							continue;
						}

						int beginIndex	= sb.length();
						sb.append(content).append(LINE_SEPARATOR).append(LINE_SEPARATOR);// 改行二つ
//						int endIndex	= beginIndex + content.length();
						int endIndex	= sb.length() - 2; // 改行二つ分
						//int docBegin	= content.indexOf("<TEXT>") + "<TEXT".length();
						int docBegin	= content.indexOf("<TEXT>") + "<TEXT>".length();
						int docEnd	= content.indexOf("</TEXT>");
						//int beginIndex	= 0;
						//int endIndex	= text.length();
						
						Document document	= new Document(rsViewCas);
						document.setRank(rank);
						document.setDocId(docId);
						document.setScore((float)score);
						document.setContent(content);
						document.setBegin(beginIndex);
						document.setEnd(endIndex);
						document.setDocBegin(docBegin);
						document.setDocEnd(docEnd);
						document.addToIndexes();
						++rank;
					}

					// Documentファイル格納処理
					try {
						// Indexファイル（検索クエリと格納先）
						Path indexPath	= Paths.get(docDirPath.toString(),"doc.index");
						if(!Files.exists(indexPath)) {
							Files.createFile(indexPath);
						}

						LinkedHashMap<String,Path> indexMap	= getIndexFileMap(indexPath);
						Path linkPath	= indexMap.get(query);
						if(linkPath != null) {
							// ファイル存在
							// 既存ファイルの中身と比較
							String data	= null;
							if(Files.isSymbolicLink(linkPath)) {
								data	= ZipUtil.readFile(Files.readSymbolicLink(linkPath).toString(), linkPath.toFile().getName()+".txt");
							}
							else {
								data	= ZipUtil.readFile(linkPath.toString(), linkPath.toFile().getName()+".txt");
							}

							if(!data.equals(sb.toString())) {
								// 一致しないので新規作成
								Path zipFilePath = createDocumentFile(sb.toString(), linkPath.toFile().getName());
								deteleSymbolicLink(linkPath);
								Path newSymLinkPath	= createSymbolicLink(zipFilePath);

								indexMap.put(query, newSymLinkPath);
								writeIndexFile(indexPath, indexMap);

								rsViewCas.setSofaDataURI(newSymLinkPath.toString(), "");
							}
							else {
								// 一致するので既存ファイルパス設定
								rsViewCas.setSofaDataURI(linkPath.toString(), "");
							}
						}
						else {
							// 新規作成
							Path zipFilePath = createDocumentFile(sb.toString(), null);
							Path newSymLinkPath	= createSymbolicLink(zipFilePath);

							indexMap.put(query, newSymLinkPath);
							writeIndexFile(indexPath, indexMap);

							rsViewCas.setSofaDataURI(newSymLinkPath.toString(), "");
						}
					} catch(Exception e) {
						e.printStackTrace();
						throw new AnalysisEngineProcessException(e);
					}
					//rsViewCas.setDocumentText(sb.toString());
					////rsViewCas.setDocumentText(text);
					SourceDocumentInformation rsSofaSourceDocumentInformationFS = 
													new SourceDocumentInformation(rsViewCas);
					rsSofaSourceDocumentInformationFS.addToIndexes();
					rsSofaSourceDocumentInformationFS.setUri(topicId);

					rs_count++;

System.out.println("");			
				}
			}

		} catch(CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}


		return;
	}

	private List<KeyTerm> getKeyTermList(JCas jCas)
	{
		Iterator<TOP> keytermFSIterator = jCas.getJFSIndexRepository().getAllIndexedFS(KeyTerm.type);

		List<KeyTerm> keytermList = new ArrayList<>();
		while(keytermFSIterator.hasNext()) {
			keytermList.add((KeyTerm)keytermFSIterator.next());
		}

		if(keytermList != null) {
			// ソート
			Collections.sort(keytermList, new Comparator<KeyTerm>(){
				public int compare(KeyTerm x, KeyTerm y){
					return x.getBegin() - y.getBegin();
				}
			});
		}
		return keytermList;
	}

	private List<AnswerType> getAnswerTypeList(JCas jCas)
	{
		Iterator<TOP> answerTypeFSIterator = jCas.getJFSIndexRepository().getAllIndexedFS(AnswerType.type);

		List<AnswerType> answerTypeList = new ArrayList<>();
		while(answerTypeFSIterator.hasNext()) {
			answerTypeList.add((AnswerType)answerTypeFSIterator.next());
		}
		return answerTypeList;
	}

	private void setQuery(JCas jCas, String query)
	{
System.out.println("query="+query);

		Query queryFS	= new Query(jCas);
		queryFS.setQuery(query);
		queryFS.addToIndexes();

		return;
	}

	/**
	 * QA(QuestionAnalyzer：質問分析)の結果を取得する.
	 * 
	 * @param jCas
	 * @return InfraQuestionAnalysisResult
	 */
	private InfraQuestionAnalysisResult getQuestionAnalysisResult(List<KeyTerm> keytermList, List<AnswerType> answerTypeList)
	{
		List<InfraTerm> infraKeyterms = new ArrayList<InfraTerm>();
		for(KeyTerm keyTerm : keytermList) {
//System.out.println("begin="+keyTerm.getBegin()+",end="+keyTerm.getEnd());
//System.out.println("text="+keyTerm.getCoveredText());
			String term		= Converter.toWideChars(keyTerm.getCoveredText());
			double score	= keyTerm.getScore();
//System.out.println(term);
			InfraTerm infraTerm = new InfraTerm(term,score);
			infraKeyterms.add(infraTerm);
		}

		List<InfraAnswerType> infraAnswerTypes = new ArrayList<InfraAnswerType>();
		for(AnswerType answerType : answerTypeList) {
			String type		= answerType.getAnswerType();
			double score	= answerType.getScore();
			InfraAnswerType infraAnswerType = new InfraAnswerType(type,score);
			infraAnswerTypes.add(infraAnswerType);
		}

		// QA結果クラスの生成
		InfraQuestionAnalysisResult qaResult = new InfraQuestionAnalysisResult();
		qaResult.addTerms(infraKeyterms);
		qaResult.addAnswerTypes(infraAnswerTypes);

		return qaResult;
	}

	/**
	 * Indriを実行する.
	 *
	 * @param qaResult
	 * @param jCas
	 * @return InfraRetrievalResult
	 */
	private InfraRetrievalResult runIndri(InfraQuestionAnalysisResult qaResult, JCas jCas)
	{
System.out.println("runRS(Indri)");

		// 形態素解析結果を取得
		Iterator<TOP> morphemeFSIterator = jCas.getJFSIndexRepository().getAllIndexedFS(Morpheme.type);
		List<Morpheme> morphemeList = new ArrayList<Morpheme>();
		while(morphemeFSIterator.hasNext()) {
			Morpheme	morphemeFS	= (Morpheme)morphemeFSIterator.next();
//System.out.println("Morpheme text="+morphemeFS.getCoveredText()+" pos="+morphemeFS.getPos()+" detailedPos="+morphemeFS.getDetailedPos());
			morphemeList.add(morphemeFS);
		}

		try {
			IndriRetrievalStrategist indriRs	= getIndriRetrievalStrategist();
			InfraRetrievalResult	rsResult	= indriRs.run(qaResult,morphemeList);
			indriRs	= null;
			return rsResult;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * RS : IndriRetrievalStrategist
	 *
	 * @return
	 */
	private IndriRetrievalStrategist getIndriRetrievalStrategist() throws Exception
	{
		if(qf == null) {
			qf = new SimpleCharBasedIndriQF();
		}

		try {
			// Indri実行コマンド設定
			RunQueryCommandWrapper	runQuery	= new RunQueryCommandWrapper(defaultServer,indicesFilePath,
																											indriCommand,encoding,indriMode);
			return new IndriRetrievalStrategist(qf,runQuery,indriMaxDocs,keyWeight);
		} catch(Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	private Path createDocumentFile(String document, String fileName) throws IOException
	{
		if(processDirPath == null) {
			String date = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(Calendar.getInstance().getTime());
			processDirPath	= Files.createDirectory(Paths.get(docDirPath.toString(), date));
		}

		Path docFilePath	= null;
		if(fileName == null) {
			//File dataFile = File.createTempFile("indri", ".txt", processDirPath.toFile());
			//docFilePath	= dataFile.toPath();
			//fileName	= dataFile.getName().substring(0, dataFile.getName().lastIndexOf("."));
			String date = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(Calendar.getInstance().getTime());
			docFilePath	= Files.createFile(Paths.get(processDirPath.toString(), "indri" + date + ".txt"));
			fileName	= docFilePath.toFile().getName().substring(0, docFilePath.toFile().getName().lastIndexOf("."));
		}
		else {
			String dataFileName	= fileName + ".txt";
			docFilePath	= Paths.get(processDirPath.toString(), dataFileName);
		}

		Charset charset = Charset.forName("UTF-8");
		try(BufferedWriter writer = Files.newBufferedWriter(docFilePath, charset)) {
			writer.write(document, 0, document.length());
		} catch (IOException e) {
			e.printStackTrace();
		}
//System.out.println(Files.size(docFilePath));
		String zipFileName	= fileName + ".zip";
		Path zipFilePath	= Paths.get(processDirPath.toString(), zipFileName);

		ZipUtil.compress(docFilePath.toString(), zipFilePath.toString());
		Files.delete(docFilePath);

		return zipFilePath;
	}

	private LinkedHashMap<String,Path> getIndexFileMap(Path indexPath)
	{
		LinkedHashMap<String,Path> indexMap	= new LinkedHashMap<String,Path>();
		if(!Files.exists(indexPath)) {
System.out.println("indexPath null");
			return indexMap;
		}

		Charset charset = Charset.forName("UTF-8");
		try(BufferedReader reader = Files.newBufferedReader(indexPath, charset)) {
			String line = null;
			while((line = reader.readLine()) != null) {
//System.out.println(line);
				line	= line.trim();
				String[] item	= line.split(INDEX_SEPARATOR);
				if(item.length != 2) {
					continue;
				}
				Path symbolicLink	= Paths.get(item[1]);
				indexMap.put(item[0],symbolicLink);
			}
		} catch(IOException e) {
			e.printStackTrace();
		}

		return indexMap;
	}

	private void writeIndexFile(Path indexPath, HashMap<String,Path> indexMap)
	{
		// データ
		StringBuilder sb	= new StringBuilder();
		for(Map.Entry<String, Path> entry : indexMap.entrySet()) {
			sb.append(entry.getKey());
			sb.append(INDEX_SEPARATOR);
			sb.append(entry.getValue());
			sb.append(System.lineSeparator());
		}

		FileOutputStream os	= null;
		BufferedWriter bw		= null;
		FileChannel channel	= null;
		try {
			os = new FileOutputStream(indexPath.toFile());
			bw	= new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
			channel	= os.getChannel();
			FileLock flock	= channel.lock();

			try {
//System.out.println("["+sb.toString()+"]");
				bw.write(sb.toString());
				bw.flush();
			} finally {
				flock.release();
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(Exception e) {
					e.printStackTrace();
				}	
			}
			if(os != null) {
				try {
					os.close();
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
			if(channel != null) {
				try {
					channel.close();
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
		return;
	}

	private Path createSymbolicLink(Path targetPath) throws IOException
	{
		String name	= targetPath.toFile().getName().substring(0, targetPath.toFile().getName().lastIndexOf("."));
		Path symLink	= Paths.get(docDirPath.toString(), name);
//System.out.println("symLink="+symLink);
//System.out.println("targetPath="+targetPath);

		Files.createSymbolicLink(symLink, targetPath);

		return symLink;
	}

	private void deteleSymbolicLink(Path symLink) throws IOException
	{
		if(!Files.exists(symLink)) {
			return;
		}

		Files.delete(symLink);

	    return;
	}
}
