package org.kachako.components.analysis_engine;

import java.util.Iterator;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.TOP;
import org.apache.uima.resource.ResourceInitializationException;
import org.kachako.types.qa.KeyTerm;

import edu.cmu.lti.indri.DumpIndexCommandWrapper;
import edu.cmu.lti.nlp.wrapper.ja.util.Converter;

public class IndriDumpIndexAnalysisEngine extends JCasAnnotator_ImplBase
{
	/* QuestionCollectionReader View名 */
	private static final String TARGET_LANGUAGE_QUESTION_VIEW_NAME	= "TargetLanguageQuestionView";

	private final String PARAM_DEFAULT_SERVER	= "IndriDefaultServer";		// Indri サーバ名
	private final String PARAM_INDICES_PATH		= "IndriIndicesFileDirectory";	// Indri インデックスファイル名
	private final String PARAM_INDRI_DUMPINDEX	= "IndriDumpIndexFile";		// Indri dumpIndex実行コマンド
	private final String PARAM_INDRI_MODE		= "IndriMode";				// Indri モード

	private static String defaultServer		= null;
	private static String indicesFilePath	= null;
	private static String indriDumpIndex	= null;
	private static String indriMode			= null;

	@Override
	public void initialize(UimaContext aContext)
			throws ResourceInitializationException {
		super.initialize(aContext);

		// サーバ名取得
		defaultServer = (String)aContext.getConfigParameterValue(PARAM_DEFAULT_SERVER);

		// インデックスファイル名取得
		indicesFilePath = (String)aContext.getConfigParameterValue(PARAM_INDICES_PATH);

		// Indri dumpIndex実行コマンド
		indriDumpIndex = (String)aContext.getConfigParameterValue(PARAM_INDRI_DUMPINDEX);

		// Indriモード取得
		indriMode = (String)aContext.getConfigParameterValue(PARAM_INDRI_MODE);
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException
	{
//System.out.println("----------IndriDumpIndexAnalysisEngine start----------");
		try {
			JCas targetLangQuestionViewCas	=  jCas.getView(TARGET_LANGUAGE_QUESTION_VIEW_NAME);

			DumpIndexCommandWrapper	dumpIndex	= new DumpIndexCommandWrapper(defaultServer,indicesFilePath,
																				indriDumpIndex,indriMode);

			// QA結果のKeyTerm取得
			Iterator<TOP> keyTermFSIterator = targetLangQuestionViewCas.getJFSIndexRepository().getAllIndexedFS(KeyTerm.type);

			while(keyTermFSIterator.hasNext()) {
				KeyTerm keyTermFS	= (KeyTerm)keyTermFSIterator.next();
//System.out.println("begin="+keyTermFS.getBegin()+",end="+keyTermFS.getEnd());
				String term		= Converter.toWideChars(keyTermFS.getCoveredText());
//System.out.println(term);
				StringBuilder	sb = new StringBuilder();
				for (int index = 0; index < term.length(); index++) {
					sb.append(index > 0 ? " ":"");
					sb.append(term.charAt(index));
				}
				long x = dumpIndex.termCount(sb.toString());
				keyTermFS.setScore(x);
				keyTermFS.addToIndexes();
			}

System.out.println("");

		} catch(CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}

		return;
	}

}
