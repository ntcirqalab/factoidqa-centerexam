package edu.cmu.lti.commons.text;

import java.util.regex.Pattern;


public class Unsegmenter
{
	public static String segment(String lang, String sentence) {
		String result = sentence;
//		if (lang.matches("jp|JP|ja|JA")) {
//			result = segmentJP(sentence);
//		} else if (lang.matches("cs|CS")) {
//			result = segmentCS(sentence);
//		} else if (lang.matches("ct|CT")) {
//			result = segmentCT(sentence);
//		}

		return result;
	}

	private final static String TEMP_STRING = ":::SPACE:::";
	private final static String ASCII = "[\\p{ASCII}&&[^ ]]";

	private final static Pattern SPACES = Pattern.compile("[ ]{3}");
	private final static Pattern SPACE = Pattern.compile(" ");
	private final static Pattern TEMP = Pattern.compile(TEMP_STRING);
	private final static Pattern ESCAPE1 = Pattern.compile("("+ASCII+")[ ]");
	private final static Pattern ESCAPE2 = Pattern.compile("[ ]("+ASCII+")");

	public static String unsegment(String text) {
		if (LanguageDetector.isEnglish(text)) {
			// Do nothing for English
			return text;
		}
		text = SPACES.matcher(text).replaceAll(TEMP_STRING);
		text = ESCAPE1.matcher(text).replaceAll("$1"+TEMP_STRING);
		text = ESCAPE2.matcher(text).replaceAll(TEMP_STRING+"$1");
		text = SPACE.matcher(text).replaceAll("");
		text = TEMP.matcher(text).replaceAll(" ");

		return text;
	}
}
