package edu.cmu.lti.commons.text;

import java.util.regex.Pattern;


public class LanguageDetector
{
	@SuppressWarnings("unused")
	private final static Pattern EnglishPattern = Pattern.compile("^[a-zA-Z0-9\\p{P}\\p{S}\\p{Z}]+$");
	private final static Pattern NonEnglishPattern = Pattern.compile("[^\\p{ASCII}]");

	public static boolean isEnglish(String text) {
		return !NonEnglishPattern.matcher(text).find();
		//    return EnglishPattern.matcher(text).find(); // slower
	}
}
