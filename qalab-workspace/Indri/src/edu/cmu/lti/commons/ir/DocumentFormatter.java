/*
 * Copyright 2008-2009 Carnegie-Mellon University
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package edu.cmu.lti.commons.ir;

import java.util.regex.Pattern;

import edu.cmu.lti.commons.text.Unsegmenter;


public class DocumentFormatter
{
	private static Pattern pDeleteTag = Pattern.compile("(<WORDS>.*?</WORDS>)|(<LANG>.*?</LANG>)|(<AE>.*?</AE>)|(<SECTION>.*</SECTION>)", Pattern.DOTALL+Pattern.CASE_INSENSITIVE);
	private static Pattern pSpaces1 = Pattern.compile("(\n[ ]?)+", Pattern.DOTALL);
	private static Pattern pSpaces2 = Pattern.compile("^[ \n\t]+|[ \n\t]+?$", Pattern.DOTALL);

	public static String getCleanDocument(String lang, String doc) {
		if (lang != null && !lang.matches("en|EN")) {
			doc = Unsegmenter.unsegment(doc);
		}
		doc = doc.replaceAll("<P>|</P>", " ");
		doc = pDeleteTag.matcher(doc).replaceAll("");
		doc = pSpaces1.matcher(doc).replaceAll("\n");
		doc = pSpaces2.matcher(doc).replaceAll("");
		return doc;
	}

	public static String cleanUp(String text) {
		return text.replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\n", "<br>");
	}
}
