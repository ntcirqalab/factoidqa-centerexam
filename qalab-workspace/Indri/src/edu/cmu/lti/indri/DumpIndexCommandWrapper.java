/*
 * Copyright 2008 Carnegie-Mellon University
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package edu.cmu.lti.indri;

import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import org.apache.log4j.Logger;

import edu.cmu.lti.commons.command.CommandExecutor;
import edu.cmu.lti.commons.command.CommandService;
import edu.cmu.lti.commons.util.ExceptionUtil;


public class DumpIndexCommandWrapper
{
// Key: taskNameShort
//private static Map<String, RunQueryCommandWrapper> retrievers = new HashMap<String, RunQueryCommandWrapper>();

//	private Logger log = Logger.getLogger(DumpIndexCommandWrapper.class);

	public String validationReport;

	private String dumpIndexBin;
	private boolean local = false;

	private CommandExecutor[] executors;
	private String[] indexArray;

	// could be the:5.48002e+07
	private final static Pattern pXCountResult = Pattern.compile("(?:.+):([^\\s]+)$");


	public DumpIndexCommandWrapper(Properties properties) {
		this(properties,properties.getProperty("default.prefix"));
	}

	/**
	 * 
	 * @param properties
	 * @param prefix
	 */
	public DumpIndexCommandWrapper(Properties properties, String prefix) { // indri.ntcir8.ja
		String serverPort = (String) properties.get("default.server");
		CommandService service = CommandService.parseArguments(new String[] { serverPort });

		dumpIndexBin = properties.getProperty("indri.command.dumpindex","/home/javelin/extern/indri/bin/dumpindex");
		String indriMode = properties.getProperty("indri.mode", "remote");
		local = indriMode.equalsIgnoreCase("local");

		String indices = properties.getProperty(prefix+".indices");
		indexArray = indices.split(";");
		executors = new CommandExecutor[indexArray.length];
		try {
			if (local) {
				String bin = dumpIndexBin;
				validationReport = IndriUtil.validateIndri(bin, indexArray);
			}
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		for (int i=0; i<indexArray.length; i++) {
			String index = indexArray[i];
			String[] cmdArrayDumpIndex = { dumpIndexBin, index };
			executors[i] = new CommandExecutor(service,cmdArrayDumpIndex);
//			executors[i].setLocalMode(local);
		}

		return;
	}

	/**
	 * 
	 * @param defaultServer
	 * @param indicesFilePath
	 * @param indriDumpIndexCommand
	 * @param indriMode
	 */
	public DumpIndexCommandWrapper(String defaultServer,String indicesFilePath,
										String indriDumpIndexCommand,String indriMode)
	{
		CommandService service = CommandService.parseArguments(new String[] { defaultServer });

		dumpIndexBin	= indriDumpIndexCommand;
		if(indriDumpIndexCommand == null) {
			// 決めつけパスでいいのだろうか？
			//dumpIndexBin	= "/home/javelin/extern/indri/bin/dumpindex";
		}
		if(indriMode != null && indriMode.equalsIgnoreCase("local")) {
			local	= true;
		}

		String[] indexArray = indicesFilePath.split(";");
		executors = new CommandExecutor[indexArray.length];
		//try {
		//	if(local) {
		//		String bin = dumpIndexBin;
		//		validationReport = IndriUtil.validateIndri(bin, indexArray);
		//	}
		///} catch ( Exception e ) {
		//	e.printStackTrace();
		//}
		for (int i=0; i<indexArray.length; i++) {
			String index = indexArray[i];
			String[] cmdArrayDumpIndex = { dumpIndexBin, index };
			executors[i] = new CommandExecutor(service,cmdArrayDumpIndex);
//			executors[i].setLocalMode(local);
		}

		return;
	}

	public int expressionCount(String expression)
	{
		// You don't need to enclose expression with double quote!!!!
		int count = 0;

		for (CommandExecutor executor : executors) {
			try {
				String stdout;
				if (local) {
					stdout = executor.executeLocal(new String[]{"x",expression});
				} else {
					stdout = executor.executeRemote(new String[]{"x",expression});
				}
				Matcher mXCountResult = pXCountResult.matcher( stdout );
				if ( mXCountResult.find() ) {
					String countText = mXCountResult.group(1);
					int ePos = countText.indexOf("e");
					if ( ePos==-1 ) {
						count += Integer.parseInt( mXCountResult.group(1) );
					} else {
						double base = Double.parseDouble( countText.substring(0,ePos) );
						int power = Integer.parseInt(countText.substring(ePos+2));
						count += (int) ( base * Math.pow(10, power) );
					}
				}
			} catch (Exception e) {
//				log.debug("Problem in dumpindex x "+expression);
				e.printStackTrace();
			}
		}

		return count;
	}

	public long termCount(String term) {
		String expression = "#1( "+term+" )";
		return expressionCount( expression );
	}

	public String getDocumentByDocno(String docno)
	{
		for (CommandExecutor executor : executors) {
			try {
				String document;
				if (local) {
					String internalDocid = executor.executeLocal(new String[]{"di","docno",docno}).trim();
					document = executor.executeLocal(new String[]{"dt", internalDocid});
				} else {
					String internalDocid = executor.executeRemote(new String[]{"di","docno",docno}).trim();
					document = executor.executeRemote(new String[]{"dt", internalDocid});
				}
				if (document.length()>10) return document;
			} catch (Exception e) {
				e.printStackTrace();
				continue;
//				log.debug("Problem in dumpindex dt "+internalDocid);
			}
		}

		return null;
	}

	public String getCorpusStatistics()
	{
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<executors.length; i++) {
			if (sb.toString().length()>0) sb.append("\n");
			CommandExecutor executor = executors[i];
			try {
				if (local) {
					sb.append(executor.executeLocal(new String[]{"s"}));
				} else {
					sb.append(executor.executeRemote(new String[]{"s"}));
				}
				sb.append("index:\t\t"+indexArray[i]+"\n");
			} catch (Exception e) {
				e.printStackTrace();
//				log.debug("Problem in dumpindex s");
				sb.append(ExceptionUtil.exceptionToString(e)+"\n");
			}
		}

		return sb.toString();
	}
}
