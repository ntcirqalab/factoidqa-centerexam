package edu.cmu.lti.indri;


/**
 * Mocking lemurproject.indri.ScoredExtentResult
 * @author Hideki
 *
 */
public class ScoredExtentResult
{
	public int internalId;
	public String externalId;
	public String content;
	public String text;
	public double score;
	public int begin; // Token offset!
	public int end;

	ScoredExtentResult( double score, int internalId, String externalId ) {
		this.score = score;
		this.internalId = internalId;
		this.externalId = externalId;
	}
}
