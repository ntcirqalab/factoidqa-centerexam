/*
 * Copyright 2008 Carnegie-Mellon University
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package edu.cmu.lti.indri;

import java.rmi.registry.Registry;
import java.util.Properties;

import edu.cmu.lti.commons.command.CommandExecutor;
import edu.cmu.lti.commons.command.CommandExecutorFactory;
import edu.cmu.lti.commons.ir.DocumentFormatter;


public class RunQueryCommandWrapper
{
	// Key: taskNameShort
	//private static Map<String, RunQueryCommandWrapper> retrievers = new HashMap<String, RunQueryCommandWrapper>();

	public String validationReport;

	private QueryEnvironment env;
	private boolean local = false;

	public RunQueryCommandWrapper(Properties properties) {
		this(properties,properties.getProperty("default.prefix"));
	}

	public RunQueryCommandWrapper(Properties properties, String prefix) { // indri.ntcir8.ja
		CommandExecutor executor = CommandExecutorFactory.create(properties, "indri");

		String withoutDocuments = properties.getProperty("withoutDocuments");
		boolean withDocuments = withoutDocuments==null;

		String[] args = { "-memory=1200m", "-printQuery=true" };
		for ( String arg : args ) {
			executor.appendArgument(arg);
		}
		if (withDocuments) {
			executor.appendArgument("-printDocuments=true");
		}
		env = new QueryEnvironment(executor);

		try {
			String indices = properties.getProperty(prefix+".indices");
			String[] indexArray = indices.split(";");

			String indriMode = properties.getProperty("indri.mode", "remote");
			local = indriMode.equalsIgnoreCase("local");
//			executor.setLocalMode(local);

			if (local) {
				String bin = properties.getProperty("indri.command");
				validationReport = IndriUtil.validateIndri(bin, indexArray);
			}
			for (String index : indexArray) {
				env.addIndex(index);
			}
		} catch ( Exception e ) {
			e.printStackTrace();
		}
	}

	// JSA 追加
	/**
	 * 
	 * @param defaultServer サーバ名
	 * @param indicesFilePath インデックスファイルパス
	 * @param indriCommand Indri実行コマンド
	 * @param encoding 文字エンコード
	 */
	public RunQueryCommandWrapper(String defaultServer,String indicesFilePath,
									String indriCommand,String encoding,String indriMode)
	{
		String[]	hostAndPort = defaultServer.trim().split(":");
		String		host		= hostAndPort[0];
		int port = Registry.REGISTRY_PORT;
		if(hostAndPort.length == 2) {
			port = Integer.parseInt(hostAndPort[1]);
		}

		if(encoding == null) {
			encoding = "utf-8";
		}

	    String[] cmdarray = {indriCommand.trim()};

		CommandExecutor executor = new CommandExecutor(host, port, cmdarray, encoding.trim());

		String[] args = { "-memory=1200m", "-printQuery=true" };
		for(String arg : args) {
			executor.appendArgument(arg);
		}
		executor.appendArgument("-printDocuments=true");
		if(indriMode != null && indriMode.equalsIgnoreCase("local")) {
			env = new QueryEnvironment(executor, true);
		}
		else {
			env = new QueryEnvironment(executor, false);
		}

		try {
			String[] indexArray = indicesFilePath.split(";");

//			if(indriMode != null && indriMode.equalsIgnoreCase("local")) {
//				local	= true;
//			}
//			executor.setLocalMode(local);

//			if (local) {
//				String bin = indriCommand;
//				validationReport = IndriUtil.validateIndri(bin, indexArray);
//			}
			for (String index : indexArray) {
				env.addIndex(index);
			}
		} catch ( Exception e ) {
			e.printStackTrace();
		}
	}

	/**
	 * Indriを実行します.
	 * 
	 * @param query
	 * @param maxDocs
	 * @return
	 */
	public ScoredExtentResult[] retrieve(String query, int maxDocs)
	{
		try {
			return env.runQuery(query, maxDocs);
		} catch (Exception e) {
			// StringWriter sw = new StringWriter();
			// PrintWriter pw = new PrintWriter(sw);
			// e.printStackTrace( pw );
			// return "No results matched."+sw.toString();
			e.printStackTrace();
			return null;
		}
	}

	// JSA 追加
	/**
	 * Indriを実行します.
	 * 
	 * @param query
	 * @param maxDocs
	 * @return
	 */
	public ScoredExtentResult[] retrieve(String query, int maxDocs, boolean local)
	{
		try {
			return env.runQuery(query, maxDocs);
		} catch (Exception e) {
			// StringWriter sw = new StringWriter();
			// PrintWriter pw = new PrintWriter(sw);
			// e.printStackTrace( pw );
			// return "No results matched."+sw.toString();
			e.printStackTrace();
			return null;
		}
	}

	public String getDocument(String lang, ParsedDocument pd) {
		return DocumentFormatter.getCleanDocument(lang, pd.content);
	}
}
