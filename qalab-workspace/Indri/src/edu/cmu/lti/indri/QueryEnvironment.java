package edu.cmu.lti.indri;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.cmu.lti.commons.command.CommandExecutor;
import edu.cmu.lti.commons.util.ExceptionUtil;


/**
 * Mocking lemurproject.indri.QueryEnvironment
 * @author Hideki
 *
 */
public class QueryEnvironment
{
	private List<String> indices;
	private CommandExecutor c;
	private boolean local	= false;

	// #combine[sentence] doesn't always return sentence offset!!!
	boolean printTokenOffset = false;

	public QueryEnvironment( CommandExecutor c ) {
		//indices = new ArrayList<String>();
		this.c = c;
	}

	public QueryEnvironment( CommandExecutor c, boolean local ) {
		//indices = new ArrayList<String>();
		this.c = c;
		this.local = local;
	}

	public void addIndex( String index ) {
		//indices.add(index);
		c.appendArgument("-index="+index);
	}

	private final static Pattern pIndriResult = Pattern.compile("(-[0-9.]+\\t[^\\s]+?\\t[0-9]+\\t[0-9]+)[\\n\\r]+(.*?)[\\n\\r]*(?=-[0-9.]+\\t[^\\s]+?\\t[0-9]+\\t[0-9]+[\\n\\r]+|$)", Pattern.DOTALL);
//	private final static Pattern pText = Pattern.compile("<DOCNO>(.+?)</DOCNO>.+?<TEXT>[\\s]*(.+?)[\\s]*</TEXT>", Pattern.DOTALL);

	public ScoredExtentResult[] runQuery( String query, int maxDocs ) {

		String stdout;
		try {
//      if (c.executeFailSafe()) {
//    		stdout = c.executeLocal( null, new String[]{"-query="+query, "-count="+maxDocs} );
//      } else {
//        stdout = c.executeRemote( null, new String[]{"-query="+query, "-count="+maxDocs} );
//      }
//stdout = c.executeFailSafe( null, new String[]{"-query="+query, "-count="+maxDocs} );

			if (local) {
				stdout = c.executeLocal( null, new String[]{"-query="+query, "-count="+maxDocs} );
			} else {
				stdout = c.executeRemote( null, new String[]{"-query="+query, "-count="+maxDocs} );
			}

			//postprocess Japanese tilder
			stdout = stdout.replace('\u301C','\uFF5E');
		} catch ( Exception e ) {
			e.printStackTrace();
			stdout = ExceptionUtil.exceptionToString(e);
		}

		List<ScoredExtentResult> results = new ArrayList<ScoredExtentResult>();
		ScoredExtentResult[] sers	= null;
		try {
			Matcher mIndriResult = pIndriResult.matcher( stdout );
			int co=0;
			while ( mIndriResult.find() ) {
				co++;
				String head = mIndriResult.group(1);

				String[] elements = head.split("\t");
				double score = Double.parseDouble( elements[0] );
				int internalId = 0;
				String externalId = elements[1];

				ScoredExtentResult	r	= new ScoredExtentResult( score, internalId, externalId );
				results.add( r );

				int begin = Integer.parseInt(elements[2]);
				int end = Integer.parseInt(elements[3]);
				r.begin = begin;
				r.end = end;

				String document = mIndriResult.group(2);
				document = document.replaceAll("(<|</)text\\b", "$1TEXT");
				int posTextStart = document.indexOf("<TEXT")+"<TEXT".length();
				int posTextEnd = document.indexOf("</TEXT>");

				if (posTextEnd==-1) continue;
				String text = document.substring(posTextStart, posTextEnd);
				text = text.replaceAll("\n$", "");
				text = text.replaceAll("([\\s&&[^ ]])", " $1 ");

				{//debug
//					System.out.println(externalId+"\t\""+sb+"\"");
//					if (begin<100 && dv.length<500 && false) {
					if (printTokenOffset) {
						String[] dv = text.replaceAll("(^ )|( $)", "").split(" +");
						StringBuilder sb = new StringBuilder();
						for ( int i = begin; i< end; i++ ) {
							sb.append( i>begin?" ":"" );
							sb.append( dv[i] );
						}

						System.out.println("----");
						System.out.println("QUERY="+query);
//						System.out.println("DOCLEN="+document.length()+", TEXT_BEGIN="+posTextStart+", TEXT"+posTextEnd+" "+document.length());
						System.out.println("BEGIN="+begin+", END="+end+", LENGTH="+dv.length);
						System.out.println("TEXT=\""+sb.toString().replaceAll("\n", "\\\\n")+"\"");
						System.out.println(document);
						for (int i=0; i<dv.length;i++) {
							System.out.println(i+" "+dv[i].replaceAll("\n", "\\\\n"));
						}
					}
				}

				r.content = document;

//				Matcher mText = pText.matcher(document);
//				if (mText.find()) {
//				  //r.externalId = mText.group(1);
//				  r.text = mText.group(2);
//				}
//				r.text = sb.toString();
				r.text = text;
			}

			// TODO なぜかクラスのロードエラーがでるので、その対応
			Class.forName("edu.cmu.lti.indri.ScoredExtentResult");

			sers	= /*(ScoredExtentResult[])*/ results.toArray(new ScoredExtentResult[results.size()]);
		} catch (Exception e) {
			e.printStackTrace();
			sers = new ScoredExtentResult[1];
			sers[0] = new ScoredExtentResult(0.00, 1, "ABC");
			sers[0].text = "text part"+e.getMessage();
		}

		return sers;
	}

	public ParsedDocument[] documents( ScoredExtentResult[] sers ) {
		ParsedDocument[] pds = new ParsedDocument[ sers.length ];
		for ( int i=0; i<sers.length; i++ ) {
			if (sers[i]==null) continue;
			pds[i] = new ParsedDocument();
			pds[i].content = sers[i].content;
			pds[i].text = sers[i].text;
		}

		return pds;
	}

	public ParsedDocument[] documentsFromMetadata( String field, String[] values ) {
		ParsedDocument[] pds = new ParsedDocument[ 1 ];
		return pds;
	}
}
