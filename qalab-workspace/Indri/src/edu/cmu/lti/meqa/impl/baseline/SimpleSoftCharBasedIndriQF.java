package edu.cmu.lti.meqa.impl.baseline;

import edu.cmu.lti.meqa.component.QueryFormulator_ImplBase;


public class SimpleSoftCharBasedIndriQF extends QueryFormulator_ImplBase {

  @Override
  public String wrapPhrase(String phrase) {
    String segmentedPhrase = segmentByChar(phrase);
    return "#weight( 0.9 "+enclosePhrase(segmentedPhrase)
                  +" 0.1 "+encloseCombine(segmentedPhrase)
                  +" )";
  }

}
