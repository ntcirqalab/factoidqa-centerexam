package edu.cmu.lti.meqa.impl.baseline;

import edu.cmu.lti.meqa.component.QueryFormulator_ImplBase;

public class SimpleCharBasedIndriQF extends QueryFormulator_ImplBase {

	@Override
	public String wrapPhrase(String phrase) {
		return enclosePhrase(segmentByChar(phrase));
	}

}
