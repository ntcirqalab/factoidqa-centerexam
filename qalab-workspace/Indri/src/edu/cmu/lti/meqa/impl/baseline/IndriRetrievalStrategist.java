package edu.cmu.lti.meqa.impl.baseline;

//import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

import edu.cmu.lti.commons.text.Unsegmenter;
import edu.cmu.lti.indri.RunQueryCommandWrapper;
import edu.cmu.lti.indri.ScoredExtentResult;
import edu.cmu.lti.meqa.component.QueryFormulator;
import edu.cmu.lti.meqa.component.RetrievalStrategist_ImplBase;
import edu.cmu.lti.sepia.infra.run.model.InfraDocument;
import edu.cmu.lti.sepia.infra.run.model.InfraQuestion;
import edu.cmu.lti.sepia.infra.run.model.InfraQuestionAnalysisResult;
import edu.cmu.lti.sepia.infra.run.model.InfraRetrievalResult;
import edu.cmu.lti.sepia.infra.run.model.InfraTerm;


/**
 * Indri RetrievalStrategist（RS）を提供します.
 *
 * @author
 * @version $Revision: 1.0$ $Date: 0000-00-00 00:00:00 +0900$
 */
public class IndriRetrievalStrategist extends RetrievalStrategist_ImplBase
{
	private RunQueryCommandWrapper	runQuery;
	private QueryFormulator	qf;
//	private final static int MAX_DOCS = 1000;
	private int maxDocs;
//	private final static int MAX_DOCS = 10;

	// キーワードに重み付けするか
	private boolean keyWeight	= false;

//	private NLPWrapper nlp = null;
//	private NLPWrapper nlp = new NAISTWrapper(PropertyFactory.createProperties());
//  private NLPWrapper nlp = new IBMWrapper();
//	private GoogleTranslator tm = new GoogleTranslator();

//	private boolean	monolingual	= true;
//	private boolean	charMode	= true;

	/**
	 * Indri RetrievalStrategistを構築します.
	 *
	 * @param qf
	 * @param runQuery
	 * @param maxDocs
	 * @param keyWeight
	 * @throws IllegalArgumentException 不正な引数で呼び出した場合
	 */
	public IndriRetrievalStrategist(QueryFormulator qf,RunQueryCommandWrapper runQuery,int maxDocs, boolean keyWeight)
	{
		if(qf == null) {
			throw new IllegalArgumentException("Illegal parameter. qf is null");
		}
		if(runQuery == null) {
			throw new IllegalArgumentException("Illegal parameter. runQuery is null");
		}

		this.runQuery		= runQuery;
		this.qf				= qf;
		this.maxDocs		= maxDocs;
		this.keyWeight		= keyWeight;

		return;
	}

	/**
	 *
	 * @param question
	 * @param qaResult
	 * @return
	 */
	@Override
	public InfraRetrievalResult run(InfraQuestion question,InfraQuestionAnalysisResult qaResult)
	{
//		List<InfraTerm>	keyterms	= qaResult.getKeyterms();
//
//		String[]	termsArray	= new String[keyterms.size()];
//		for(int index=0; index < keyterms.size(); index++) {
//			termsArray[index]	= keyterms.get(index).getText();
//		}
//
//		String	q;
//		if(monolingual) {
//			q	= question.getText();
//		} else {
//			q	= tm.translate(Language.ENGLISH, Language.JAPANESE, question.getText());
//		}
//
//		String[]	bow;
//		if(charMode) {
//			bow	= nlp.extractChars(q);
//		} else {
//			bow	= nlp.segment(q);
//		}
//
//		String	indriQuery	= qf.generateQuery(termsArray,bow);
//
//		// clean up!
//		indriQuery	= indriQuery.replaceAll("['\"\\?]", " ");
//
////		log.trace("Query: "+indriQuery);
//		ScoredExtentResult[]	sers	= runQuery.retrieve(indriQuery,MAX_DOCS);
//		InfraRetrievalResult rsResult = new InfraRetrievalResult();
//		for(int index=0; index < sers.length; index++) {
//			String	externalId	= sers[index].externalId;
//			double	score		= sers[index].score;
//
//			InfraDocument	d	= new InfraDocument(externalId,score);
//			if(sers[index].content != null && sers[index].text != null) {
//				String	unsegmentedContent	= Unsegmenter.unsegment(sers[index].content);
//				String	unsegmentedBody		= Unsegmenter.unsegment(sers[index].text);
//
//				d.setContent(unsegmentedContent);
//				d.setText(unsegmentedBody);
//			}
//			rsResult.add(d);
//		}
//
////		log.trace(sers.length+" docs returned"+
////					(sers.length>0?(" from "+sers[0].externalId+" to "+sers[sers.length-1].externalId):"")+".");
////		if (sers.length<MAX_DOCS) log.warn("Only "+sers.length+" < MAX_DOCS docs has been retrieved.");
//
//		return rsResult;
		return null;
	}

	/**
	 * Indriを実行します.
	 * 
	 * @param qaResult
	 * @param morphemeList
	 * @return
	 */
	public InfraRetrievalResult run(InfraQuestionAnalysisResult qaResult,List<Morpheme> morphemeList)
	{
		// 形態素解析結果
		String[] backoffWords	 =  new String[morphemeList.size()];
		int	count	= 0;
		for(Morpheme morpheme : morphemeList) {
			String	text	= morpheme.getCoveredText();
			backoffWords[count]	= text;
			++count;
		}

		List<InfraTerm>	keyterms	= qaResult.getKeyterms();

		String	indriQuery	= null;
		if(!keyWeight) {
			String[]	termsArray	= new String[keyterms.size()];
			for(int index=0; index < keyterms.size(); index++) {
				termsArray[index]	= keyterms.get(index).getText();
			}
			indriQuery	= qf.generateQuery(termsArray,backoffWords);
		}
		else {
			// 重み付け検索
			LinkedHashMap<String, Double> termScoreMap	= new LinkedHashMap<String, Double>();
			for(int index=0; index < keyterms.size(); index++) {
				String term	= keyterms.get(index).getText();
				double score	= keyterms.get(index).getScore();
				termScoreMap.put(term, score);
			}
			indriQuery	= qf.generateQueryByWeight(termScoreMap,backoffWords);
		}

		// clean up!
		indriQuery	= indriQuery.replaceAll("['\"\\?]", " ");

System.out.println("Query: "+indriQuery);
//		log.trace("Query: "+indriQuery);
		ScoredExtentResult[]	sers	= runQuery.retrieve(indriQuery,maxDocs);
		InfraRetrievalResult rsResult = new InfraRetrievalResult();
		for(int index=0; index < sers.length; index++) {
			String	externalId	= sers[index].externalId;
			double	score		= sers[index].score;

			InfraDocument	d	= new InfraDocument(externalId,score);
			if(sers[index].content != null && sers[index].text != null) {
				String	unsegmentedContent	= Unsegmenter.unsegment(sers[index].content);
				String	unsegmentedBody		= Unsegmenter.unsegment(sers[index].text);

				d.setContent(unsegmentedContent);
				d.setText(unsegmentedBody);
			}
			rsResult.add(d);
		}

//		log.trace(sers.length+" docs returned"+
//					(sers.length>0?(" from "+sers[0].externalId+" to "+sers[sers.length-1].externalId):"")+".");
//		if (sers.length<MAX_DOCS) log.warn("Only "+sers.length+" < MAX_DOCS docs has been retrieved.");

		return rsResult;
	}
}
