package edu.cmu.lti.meqa.component;

import java.util.LinkedHashMap;

public interface QueryFormulator {

	String generateQuery( String[] words, String[] bow );

	// 追加
	// 重み付き
	String generateQueryByWeight(LinkedHashMap<String, Double> wordWeightMap, String[] bow);
}
