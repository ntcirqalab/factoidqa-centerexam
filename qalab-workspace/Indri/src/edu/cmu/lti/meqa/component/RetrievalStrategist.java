package edu.cmu.lti.meqa.component;

import edu.cmu.lti.sepia.infra.run.model.InfraQuestion;
import edu.cmu.lti.sepia.infra.run.model.InfraQuestionAnalysisResult;
import edu.cmu.lti.sepia.infra.run.model.InfraRetrievalResult;
import edu.cmu.lti.sepia.infra.run.model.InfraRunResult;


/**
 * RetrievalStrategist（RS）を実装するためのインタフェースを定義します.
 *
 * @author
 * @version $Revision: 1.0$ $Date: 0000-00-00 00:00:00 +0900$
 */
public interface RetrievalStrategist
{
	/**
	 *
	 * @param question
	 * @param qaResult
	 * @return
	 */
	InfraRetrievalResult run(InfraQuestion question,InfraQuestionAnalysisResult qaResult);

	/**
	 *
	 * @param runResult
	 */
	void run(InfraRunResult runResult);

	/**
	 *
	 * @param runResult
	 * @throws InvalidResultException
	 */
	void validate(InfraRunResult runResult) throws InvalidResultException;
}
