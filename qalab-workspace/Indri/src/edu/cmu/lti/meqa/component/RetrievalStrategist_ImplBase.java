package edu.cmu.lti.meqa.component;

import edu.cmu.lti.sepia.infra.run.model.InfraQuestion;
import edu.cmu.lti.sepia.infra.run.model.InfraQuestionAnalysisResult;
import edu.cmu.lti.sepia.infra.run.model.InfraRetrievalResult;
import edu.cmu.lti.sepia.infra.run.model.InfraRunResult;


/**
 * RetrievalStrategist（RS）を実装するための抽象クラスです.
 *
 * @author
 * @version $Revision: 1.0$ $Date: 0000-00-00 00:00:00 +0900$
 * @see edu.cmu.lti.meqa.component.RetrievalStrategist
 */
public abstract class RetrievalStrategist_ImplBase implements RetrievalStrategist
{
	/**
	 *
	 * @param runResult
	 */
	@Override
	public void run(InfraRunResult runResult)
	{
		InfraQuestion				q			= runResult.getQuestion();
		InfraQuestionAnalysisResult	qaResult	= runResult.getQuestionAnalysisResult();

		InfraRetrievalResult	result	= run(q,qaResult);
		runResult.setRetrievalResult(result);

		return;
	}

	/**
	 *
	 * @param runResult
	 * @throws InvalidResultException
	 */
	@Override
	public void validate(InfraRunResult runResult) throws InvalidResultException {
		if (runResult.getRetrievalResult()==null) throw new InvalidResultException();
	}
}
