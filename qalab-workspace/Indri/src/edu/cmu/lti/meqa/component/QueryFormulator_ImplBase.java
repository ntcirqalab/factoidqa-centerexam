package edu.cmu.lti.meqa.component;

import java.util.LinkedHashMap;
import java.util.Map;

public abstract class QueryFormulator_ImplBase implements QueryFormulator {

	public String generateQuery( String[] words, String[] backoffWords ) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < words.length; i++) {
			sb.append((i > 0) ? " " : "");
			sb.append(wrapPhrase(words[i]));
		}
		if (backoffWords==null) {
			return encloseCombine(sb.toString());
		} else {
			String s = sb.toString();
			if (s.length()>0) {
				return "#weight( 100 "+encloseCombine(sb.toString())
						+" 1 "+encloseCombine(concatenate(backoffWords))+" )";
			} else {
				return encloseCombine(concatenate(backoffWords));
			}
		}
	}

	@Override
	public String generateQueryByWeight(LinkedHashMap<String, Double> wordWeightMap, String[] backoffWords) {
		StringBuilder sb = new StringBuilder();
		int i	= 0;
		for(Map.Entry<String, Double> entry : wordWeightMap.entrySet()) {
			String word	= entry.getKey();
			double weight	= entry.getValue();
			sb.append((i > 0) ? " " : "");
			sb.append(weight);
			sb.append(" ");
			sb.append(wrapPhrase(word));
			++i;
		}

		if(backoffWords==null) {
			return encloseCombine(sb.toString());
		} else {
			String s = sb.toString();
			if (s.length()>0) {
//				return "#weight( 100 "+ "(" + sb.toString() +")"
//						+" 1 "+encloseCombine(concatenate(backoffWords))+" )";
				return "#weight(" + sb.toString() +")";
			} else {
				return encloseCombine(concatenate(backoffWords));
			}
		}
	}

	// may or may not be segmented
	public abstract String wrapPhrase( String phrase );

	protected String enclosePhrase( String s ) {
		return "#1( "+ s +" )";
	}

	protected String encloseCombine( String s ) {
		return "#combine( "+ s +" )";
	}

	protected String concatenate( String[] tokens ) {
		StringBuilder sb = new StringBuilder();
		for ( int i=0; i<tokens.length; i++ ) {
			sb.append( (i>0)?" ":"" );
			sb.append( tokens[i] );
		}
		return sb.toString();
	}

	protected String segmentByChar( String word ) {
		word = preprocessWord(word);
		String[] chars = word.split("");
		StringBuilder sb = new StringBuilder();
		for ( int i=0; i<chars.length; i++ ) {
			sb.append( (i>1)?" ":"" );
			sb.append( chars[i] );
		}
		return sb.toString();
	}

	protected String preprocessWord( String word ) {
		word = word.replaceAll("[\\s]+$", "");
		word = word.replaceAll("^[\\s]+", "");
		word = word.replaceAll("　", " ");
		return word;
	}

}
