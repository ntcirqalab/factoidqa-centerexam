package edu.cmu.lti.nlp;

import edu.cmu.lti.commons.model.Annotation;


public interface NLPWrapper
{
	String[] segment(String sentence);

	String[] extractChars(String sentence);

	Annotation[] extractWords(String sentence);

	Annotation[] extractContentWords(String sentence);

	Annotation[] extractNamedEntities(String sentence);

	Annotation[] extractQuotedText(String sentence);

	Annotation[] extractNP(String sentence);

	Annotation[] extractConsecutiveTypes(String sentence);

//	unsegment();

//	abstract public Object getSyntacticTree( String sentence );
//	abstract public Object getSemanticTree( String sentence );
}
