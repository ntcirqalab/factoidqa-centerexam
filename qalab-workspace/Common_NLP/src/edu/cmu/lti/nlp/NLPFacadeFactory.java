package edu.cmu.lti.nlp;

import java.util.Properties;

//import edu.cmu.lti.nlp.wrapper.en.BaseEnglishWrapper;
import edu.cmu.lti.nlp.wrapper.ja.NAISTWrapper;
//import edu.cmu.lti.nlp.wrapper.ja.util.PropertyFactory;


public class NLPFacadeFactory
{
	/**
	 * 指定のNLPツールを作成します.
	 *
	 * 引数のプロパティは暫定。
	 * 現状、nlp.default.propertiesファイルのプロパティが指定される前提。
	 * ※ mecabの設定になります。
	 *
	 * @param prop プロパティ
	 */
	public static NLPWrapper createByName(NLPTool nlpTool,Properties prop)
	{
//
//		//NLPTool nlpTool = NLPTool.valueOf(p.getProperty("nlptool"));
//		if (nlpTool.equals(NLPTool.IBM)) {
//			Properties p = PropertyFactory.createProperties();
//			return new NAISTWrapper(p);
//			//      return new IBMWrapper();
//		} else if (nlpTool.equals(NLPTool.NAIST)) {
		if (nlpTool.equals(NLPTool.NAIST)) {
//			Properties p = PropertyFactory.createProperties();
			return new NAISTWrapper(prop);
//		} else if (nlpTool.equals(NLPTool.ENG)) {
//			return new BaseEnglishWrapper();
		}
		return null;
	}

	public enum NLPTool {
		NAIST, IBM, ENG
	}
}
