package edu.cmu.lti.nlp.wrapper.ja.mecab;

import java.util.Properties;

import edu.cmu.lti.commons.command.AbstractCommandWrapper;
import edu.cmu.lti.commons.command.CommandExecutor;

public class MecabCommandWrapper extends AbstractCommandWrapper {

  public MecabCommandWrapper(CommandExecutor executor) {
    super(executor);
  }

  public MecabCommandWrapper(Properties properties) {
    super(properties);
  }

  private final static String[] ARGS_WAKATI = new String[]{"--output-format-type=wakati"};

	public String runMecab( String text ) {
		return runMecab( text, 1 );
	}

	public String runMecab( String text, int nBest ) {
		try {
			return executor.executeFailSafe(text,new String[]{"-N"+nBest});
		} catch( Exception e ){
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Segmentation tool for Japanese (by MeCab)
	 * @param text Japanese sentence
	 * @return segmented sentence with ASCII white space
	 */
	public String segment( String text ) {
		try {
		  String stdout = executor.executeFailSafe(text,ARGS_WAKATI);
		  if ( stdout.charAt(stdout.length()-1)=='\n' ) {
		    stdout = stdout.substring(0, stdout.length()-1);
		  }
		  return stdout;
		} catch( Exception e ) {
			e.printStackTrace();
			return null;
		}
	}

  @Override
  protected String getPropertyPrefix() {
    return "mecab";
  }

}
