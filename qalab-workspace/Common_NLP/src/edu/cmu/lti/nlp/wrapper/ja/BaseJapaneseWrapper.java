package edu.cmu.lti.nlp.wrapper.ja;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.cmu.lti.commons.model.Annotation;
import edu.cmu.lti.nlp.NLPWrapper;


public class BaseJapaneseWrapper implements NLPWrapper
{
	private Pattern	pQuote	= Pattern.compile("[(（『「](.+?)[」』）)]");

//\\u30FB ... nakaguro
//\\u3005 ... repeat
	private Pattern	pConsecutiveKatakana	= Pattern.compile( "([\\p{InKatakana}\\u30FB]+)" );//[a-zA-Z' ]
	private Pattern	pConsecutiveKanji		= Pattern.compile( "([\\p{InCJKUnifiedIdeographs}\\u30FB\\u3005]{2,})" );

	@Override
	public String[] extractChars( String sentence ) {
		Annotation[] words = extractWords( sentence );
		String[] strWords = new String[words.length];
		for ( int i=0; i< words.length; i++ ) {
			strWords[i] = words[i].getLabel();
		}

		return strWords;
	}

	@Override
	public Annotation[] extractConsecutiveTypes(String sentence) {
		List<Annotation> annotation = new ArrayList<Annotation>();
		Matcher mKatakana = pConsecutiveKatakana.matcher(sentence);
		while (mKatakana.find()) {
			Annotation a = new Annotation();
			a.setBegin(mKatakana.start());
			a.setEnd(mKatakana.end());
			a.setLabel(mKatakana.group(1));
			annotation.add(a);
		}
		Matcher mKanji = pConsecutiveKanji.matcher(sentence);
		while (mKanji.find()) {
			Annotation a = new Annotation();
			a.setBegin(mKanji.start());
			a.setEnd(mKanji.end());
			a.setLabel(mKanji.group(1));
			annotation.add(a);
		}
		return (Annotation[]) annotation.toArray(new Annotation[annotation.size()]);
	}

	@Override
	public Annotation[] extractContentWords(String sentence) {
		System.err.println("WARNING1: This method is not supported : BaseJapaneseWrapper.extractContentWords()");
		return null;
	}

	@Override
	public Annotation[] extractNP(String sentence) {
		System.err.println("WARNING2: This method is not supported : BaseJapaneseWrapper.extractNP()");
		return null;
	}

	@Override
	public Annotation[] extractNamedEntities(String sentence) {
		System.err.println("WARNING3: This method is not supported : BaseJapaneseWrapper.extractNamedEntities()");
		return null;
	}

	@Override
	public Annotation[] extractQuotedText(String sentence) {
		List<Annotation> annotation = new ArrayList<Annotation>();
		Matcher mQuote = pQuote.matcher(sentence);
		while (mQuote.find()) {
			Annotation a = new Annotation();
			a.setBegin(mQuote.start());
			a.setEnd(mQuote.end());
			a.setLabel(mQuote.group(1));
			annotation.add(a);
		}
		return (Annotation[]) annotation.toArray(new Annotation[annotation.size()]);
	}

	@Override
	public Annotation[] extractWords(String sentence) {
		System.err.println("WARNING4: This method is not supported : BaseJapaneseWrapper.extractWords()");
		return null;
	}

	@Override
	public String[] segment(String sentence) {
		System.err.println("WARNING5: This method is not supported : BaseJapaneseWrapper.segment()");
		return null;
	}
}
