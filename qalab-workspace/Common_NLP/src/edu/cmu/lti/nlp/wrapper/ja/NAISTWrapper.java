package edu.cmu.lti.nlp.wrapper.ja;

import java.util.Properties;

import edu.cmu.lti.commons.model.Annotation;
//import edu.cmu.lti.nlp.wrapper.ja.cabocha.CabochaProcessor;
import edu.cmu.lti.nlp.wrapper.ja.mecab.MecabProcessor;


/*
 *
 * Cabocha未使用なので、コメントアウト
 * コンストラクタの引数をPropertiesでなく、MecabProcessorに。
 * ※  MecabProcessorは、抽象化？されていないので正しいとは思わないが、取り急ぎクラスの機能定義が不明なため
 *   の処置。
 */

public class NAISTWrapper extends BaseJapaneseWrapper
{
	private MecabProcessor	mecab;
//	private CabochaProcessor cabocha;

	public NAISTWrapper(MecabProcessor mecabProcessor) {
		this.mecab	= mecabProcessor;
		return;
	}

	public NAISTWrapper(Properties p) {
		mecab = new MecabProcessor( p );
//		cabocha = new CabochaProcessor( p );
	}

	@Override
	public String[] segment( String sentence ) {
		return mecab.segment( sentence ).split(" ");
	}

	@Override
	public Annotation[] extractWords( String sentence ) {
		return mecab.extractWords( sentence );
	}

	@Override
	public Annotation[] extractContentWords( String sentence ) {
		return mecab.extractContentWords( sentence );
	}

	@Override
	public Annotation[] extractNamedEntities(String sentence) {
		//cabocha.extractNamedEntities(sentence);
		//FIXME: implement cabocha NE
		return null;
	}

//	@Override
//	public Annotation[] extractNP(String sentence) {
//		return mecab.extractNP(sentence);
//		return null
//	}
}
