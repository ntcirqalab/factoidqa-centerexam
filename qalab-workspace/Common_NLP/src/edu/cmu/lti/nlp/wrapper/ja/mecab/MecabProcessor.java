package edu.cmu.lti.nlp.wrapper.ja.mecab;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

//import org.apache.log4j.Logger;

import edu.cmu.lti.commons.command.CommandExecutor;
import edu.cmu.lti.commons.model.Annotation;
import edu.cmu.lti.nlp.wrapper.ja.util.Converter;
import edu.cmu.lti.nlp.wrapper.ja.util.NumberUtil;

public class MecabProcessor {

  public final static String KEY_LEMMA = "lemma";
  public final static String KEY_POS = "pos";

	public static final int WORD = 0;
	public static final int POS = 1;
	public static final int SETSUZOKU = 2;
	public static final int ROOT = 7;
	public static final int READING = 8;

	private MecabCommandWrapper mecab;

	public MecabProcessor( Properties p ) {
	  mecab = new MecabCommandWrapper(p);
	}

	public MecabProcessor( CommandExecutor executor ) {
	  mecab = new MecabCommandWrapper(executor);
	}

	/**
	 * Segmentation tool for Japanese (by MeCab)
	 * @param text Japanese sentence
	 * @return segmented sentence with ASCII white space
	 */
  public String segment(String text) {
    try {
      String segmentedWord = mecab.segment(text);
      segmentedWord = Converter.toWideChars(segmentedWord);
      segmentedWord = NumberUtil.reformatNumber(segmentedWord);
      return segmentedWord;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

//	public String stemVerb( String verb ) {
//		try {
//			if ( analyzeMorpheme(verb)!=null && analyzeMorpheme(verb).size() > 0 ) {
//				List<String> morphes = analyzeMorpheme(verb).get(0);
//				if (morphes.get(NLPTool.POS).equals("動詞")
//						|| morphes.get(NLPTool.SETSUZOKU).equals("サ変接続")) {
//					return morphes.get(NLPTool.ROOT);
//				}
//			}
//			return verb;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return verb;
//		}
//	}

    /* Example output
     * 世界 名詞,一般,*,*,*,*,世界,セカイ,セカイ
     * 遺産 名詞,一般,*,*,*,*,遺産,イサン,イサン
     * 条約 名詞,一般,*,*,*,*,条約,ジョウヤク,ジョーヤク
     * と   助詞,格助詞,一般,*,*,*,と,ト,ト
     * は   助詞,係助詞,*,*,*,*,は,ハ,ワ
     * どの 連体詞,*,*,*,*,*,どの,ドノ,ドノ
     * よう 名詞,非自立,助動詞語幹,*,*,*,よう,ヨウ,ヨー
     * な   助動詞,*,*,*,特殊・ダ,体言接続,だ,ナ,ナ
     * 条約 名詞,一般,*,*,*,*,条約,ジョウヤク,ジョーヤク
     * です 助動詞,*,*,*,特殊・デス,基本形,です,デス,デス
     * か   助詞,副助詞／並立助詞／終助詞,*,*,*,*,か,カ,カ
     * 。   記号,句点,*,*,*,*,。,。,。
     * EOS
     */
  private List<List<String>> analyzeMorpheme(String text) {
    List<List<String>> result = new ArrayList<List<String>>(64);
    String mecabOut = mecab.runMecab(text);
    String[] lines = mecabOut.split("\n");
		for (String line : lines) {
			if (!line.equals("EOS")) {
				List<String> al = new ArrayList<String>();
				String[] temp = line.split("\t");

				// Avoiding out of boudary exception.
				if (temp == null || temp.length <= 1) continue;

				al.add(temp[0]);
				String[] analysis = temp[1].split(",");
				for (String s : analysis) {
					al.add(s);
				}
				result.add(al);
			}
		}
		return result;
	}

	public Annotation[] extractWords( String sentence ) {
		List<List<String>> morphemes = analyzeMorpheme( sentence );
		Annotation[] result = new Annotation[ morphemes.size() ];
		for ( int i=0; i<morphemes.size(); i++ ) {
			Annotation a = new Annotation();
			a.setLabel( morphemes.get(i).get( WORD ) );
			a.putFeature(KEY_LEMMA, morphemes.get(i).get( WORD ) );
			a.putFeature(KEY_POS, morphemes.get(i).get( POS ) );
			result[i] = a;
		}
		return result;
	}

	public Annotation[] extractContentWords( String sentence ) {
		List<List<String>> morphemes = analyzeMorpheme( sentence );
		List<Annotation> resultList = new ArrayList<Annotation>( morphemes.size() );
		for ( int i=0; i<morphemes.size(); i++ ) {
			String pos = morphemes.get(i).get( POS );
			String set = morphemes.get(i).get( SETSUZOKU );
			boolean major = pos.matches("動詞|名詞|形容詞");
			boolean minor = pos.matches("副詞") && set.matches("一般");
			if ( ! ( major || minor ) ) continue;
			Annotation a = new Annotation();
      a.setLabel( morphemes.get(i).get( WORD ) );
			a.putFeature(KEY_LEMMA, morphemes.get(i).get( WORD ) );
      a.putFeature(KEY_POS, morphemes.get(i).get( POS ) );
			resultList.add( a );
		}
		return (Annotation[]) resultList.toArray(new Annotation[resultList.size()]);
	}

    /**
     * Extract NP (used in Espresso)
     * @param subString
     * @param getFirst
     * @return
     */
  public String extractNP(String subString, boolean getFirst) {
    List<List<String>> mecabOut = analyzeMorpheme(subString);

    int N = mecabOut.size();
    List<String> morphemes = new ArrayList<String>(N);
    List<Boolean> valid = new ArrayList<Boolean>(N);
    // System.out.println(mecabOut);
    for (int i = 0; i < N; i++) {
      int j = getFirst ? i : (N - 1 - i);
      // System.out.println( "i="+j+", size="+mecabOut.size() );
      // |括弧.*
      // we don't allow NO anymore JAN-23-2009
      // || mecabOut.get(j).get(SETSUZOKU).matches("アルファベット|並立助詞|連体化")
      boolean dot = mecabOut.get(j).get(WORD).matches("・|の");
      boolean rentai = mecabOut.get(j).get(SETSUZOKU).matches("アルファベット|並立助詞|連体化");
      if (mecabOut.get(j).get(POS).matches("名詞|接頭詞") || dot || rentai) {
        if (getFirst) {
          morphemes.add(mecabOut.get(j).get(0));
          valid.add(!(dot || rentai));
        } else {
          morphemes.add(mecabOut.get(j).get(0));
          valid.add(!(dot || rentai));
        }
      } else if (morphemes.size() > 0) {
        break;
      }
    }
    StringBuilder sb = new StringBuilder();
    int M = morphemes.size();
    for (int i = 0; i < M; i++) {
      int j = getFirst ? i : (M - 1 - i);
      boolean in = 1 <= j && j <= M - 2;
      if (in || (!in && valid.get(j))) {
        sb.append(morphemes.get(j) + " ");
      }
    }

    // System.out.println( sb+"\t<=\t"+subString );
    String result = sb.toString();
    result = result.replaceAll("^[ ]|[ ]$", "");
    return result;
  }
}
