package edu.cmu.lti.nlp.wrapper.ja.util;

public class Converter {

  private final static int DIFF = 'ａ' - 'a';

	public static String toWideChars(String text) {
    char[] chars = text.toCharArray();
    for (int i = 0; i < chars.length; i++) {
      int c = chars[i];
      if ('0' <= c && c <= 'z') {
        chars[i] = (char)(chars[i] + DIFF);
      }
    }
    return new String(chars);
	}

}
