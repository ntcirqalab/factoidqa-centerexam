package edu.cmu.lti.meqa.component;


/**
 * 結果を返せなかった場合にスローします.
 *
 * @author
 * @version $Revision: 1.0$ $Date: 0000-00-00 00:00:00 +0900$
 */
public class InvalidResultException extends Exception
{
	/**
	 * シリアル・バージョンID.
	 */
	private static final long	serialVersionUID	= 1L;


	/**
	 * 詳細メッセージを持たないInvalidResultExceptionを構築します.
	 */
	public InvalidResultException() {
		super();
		return;
	}

	/**
	 * 指定された詳細メッセージを持つInvalidResultExceptionを構築します.
	 *
	 * @param message 詳細メッセージ
	 */
	public InvalidResultException(String message) {
		super(message);
		return;
	}

	/**
	 * 指定された詳細メッセージと、原因の例外を持つ、InvalidResultExceptionを生成します.
	 *
	 * @param message 詳細メッセージ
	 * @param cause 原因の例外
	 */
	public InvalidResultException(String message,Throwable cause) {
		super(message,cause);
		return;
	}
}
