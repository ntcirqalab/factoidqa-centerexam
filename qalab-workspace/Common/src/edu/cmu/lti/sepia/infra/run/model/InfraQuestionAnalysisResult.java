package edu.cmu.lti.sepia.infra.run.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import edu.cmu.lti.commons.xml.IXmlSerializable;

public class InfraQuestionAnalysisResult implements IXmlSerializable {

	protected List<InfraAnswerType> answerTypes;
	protected List<InfraTerm> keyterms;

	public InfraQuestionAnalysisResult( InfraAnswerType answerType, List<InfraTerm> keyterms ) {
		this.answerTypes = new ArrayList<InfraAnswerType>(1);
		this.answerTypes.add(answerType);
		this.keyterms = keyterms;
	}

	public InfraQuestionAnalysisResult() {
		this.answerTypes = new ArrayList<InfraAnswerType>();
		this.keyterms = new ArrayList<InfraTerm>();
	}

	public void addAnswerType( InfraAnswerType atype ) {
		answerTypes.add( atype );
	}

	public void addAnswerTypes(Collection<InfraAnswerType> atypes) {
		answerTypes.addAll(atypes);
	}

	public void addTerm(InfraTerm term) {
		keyterms.add(term);
	}

	public void addTerms(Collection<InfraTerm> terms) {
		keyterms.addAll(terms);
	}

	/**
	 * @return the answerType
	 */
	public List<InfraAnswerType> getAnswerTypes() {
		return answerTypes;
	}

	/**
	 * @return the answerTypes
	 */
	public InfraAnswerType getAnswerType() {
		return getMostLikelyAnswerType();
	}

	/**
	 * @return the most likely answer type
	 */
	public InfraAnswerType getMostLikelyAnswerType() {
		InfraAnswerType argmax = null;
		double max = Double.NEGATIVE_INFINITY ;
		for (InfraAnswerType atype : answerTypes) {
			if (max < atype.getScore()) {
				max = atype.getScore();
				argmax = atype;
			}
		}
		return argmax;
	}

	/**
	 * @return the keyterms
	 */
	public List<InfraTerm> getKeyterms() {
		return keyterms;
	}
}
