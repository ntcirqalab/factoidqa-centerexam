package edu.cmu.lti.sepia.infra.run.model;


public class InfraAnswerType {

  private String type;
  private double score;

  public InfraAnswerType( String type, double score ) {
    this.type = type;
    this.score = score;
  }

  public String getText() {
    return type.toString();
  }

  public double getScore() {
    return score;
  }
}
