package edu.cmu.lti.sepia.infra.run.api.xml;

import java.io.InputStream;

import edu.cmu.lti.commons.xml.Deserializer;
import edu.cmu.lti.sepia.infra.run.model.InfraExtractionResultSet;
import edu.cmu.lti.sepia.infra.run.model.InfraQuestionAnalysisResultSet;
import edu.cmu.lti.sepia.infra.run.model.InfraRetrievalResultSet;
import edu.cmu.lti.sepia.infra.run.model.InfraSelectionResultSet;

public class RunXmlFacade {

  // Never make Deserializers static class instance!!

  public static InfraQuestionAnalysisResultSet deserializeQuestionAnalysisResultSet( InputStream is ) {
    Deserializer qaDeserializer = new Deserializer(new QuestionAnalysisXmlToObj());
    return (InfraQuestionAnalysisResultSet)qaDeserializer.deserialize(is);
  }

  public static InfraRetrievalResultSet deserializeRetrievalResultSet( InputStream is ) {
    Deserializer rsDeserializer = new Deserializer(new RetrievalResultXmlToObj());
    return (InfraRetrievalResultSet)rsDeserializer.deserialize(is);
  }

  public static InfraSelectionResultSet deserializeSelectionResultSet( InputStream is ) {
    Deserializer agDeserializer = new Deserializer(new SelectionResultXmlToObj());
    return (InfraSelectionResultSet)agDeserializer.deserialize(is);
  }

  public static String serializeQuestionAnalysisResultSet( InfraQuestionAnalysisResultSet resultSet ) {
    return QuestionAnalysisObjToXml.serialize(resultSet);
  }

  public static String serializeRetrievalResultSet( InfraRetrievalResultSet resultSet ) {
    return RetrievalResultObjToXml.serialize(resultSet);
  }

  public static String serializeExtractionResultSet( InfraExtractionResultSet resultSet ) {
    return ExtractionResultObjToXml.serialize(resultSet);
  }

  public static String serializeSelectionResultSet( InfraSelectionResultSet resultSet ) {
    return SelectionResultObjToXml.serialize(resultSet);
  }

}
