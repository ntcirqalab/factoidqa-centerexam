package edu.cmu.lti.sepia.infra.run.model;

public class InfraDocument {

	protected String docid;
	protected double score;
	protected String content;
	protected String title;
	protected String text;

	public InfraDocument(String docid) {
    this.docid = docid;
  }

  public InfraDocument(String docid, double score) {
    this.docid = docid;
    this.score = score;
  }

  /**
   * @return the docid
   */
  public String getDocid() {
    return docid;
  }
  /**
   * @return the score
   */
  public double getScore() {
    return score;
  }
  /**
   * @return the content
   */
  public String getContent() {
    return content;
  }
  /**
   * @param content the content to set
   */
  public void setContent(String content) {
    this.content = content;
  }
  /**
   * @return the title
   */
  public String getTitle() {
    return title;
  }
  /**
   * @param title the title to set
   */
  public void setTitle(String title) {
    this.title = title;
  }
  /**
   * @return the text
   */
  public String getText() {
    return text;
  }
  /**
   * @param text the text to set
   */
  public void setText(String text) {
    this.text = text;
  }

}
