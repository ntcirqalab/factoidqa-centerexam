package edu.cmu.lti.sepia.infra.run.model;

import edu.cmu.lti.commons.xml.IXmlSerializable;
import edu.cmu.lti.sepia.infra.shared.model.TopicMap;

public class InfraSelectionResultSet
	extends TopicMap<String,InfraSelectionResult> implements IXmlSerializable {

  public InfraSelectionResultSet() {
    super();
  }

	public InfraSelectionResultSet(int initialCapacity) {
    super(initialCapacity);
  }

  private static final long serialVersionUID = 1L;

}
