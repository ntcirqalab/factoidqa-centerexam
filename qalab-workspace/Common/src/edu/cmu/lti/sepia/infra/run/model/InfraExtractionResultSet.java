package edu.cmu.lti.sepia.infra.run.model;

import edu.cmu.lti.commons.xml.IXmlSerializable;
import edu.cmu.lti.sepia.infra.shared.model.TopicMap;

public class InfraExtractionResultSet
	extends TopicMap<String,InfraExtractionResult> implements IXmlSerializable {

	private static final long serialVersionUID = 1L;

  public InfraExtractionResultSet() {
    super();
  }

  public InfraExtractionResultSet(int initialCapacity) {
    super(initialCapacity);
  }
}
