package edu.cmu.lti.sepia.infra.shared.model;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Represents a collection of topics where each key
 * (i.e. topic id) is associated with topic related data
 * @author hideki
 *
 * @param <K>
 * @param <V>
 */
public class TopicMap<K,V> extends LinkedHashMap<K,V> {

  public TopicMap() {
    super();
  }

  public TopicMap(int initialCapacity) {
    super(initialCapacity);
  }

  public TopicMap(Map<? extends K, ? extends V> m) {
    super(m);
  }

  private static final long serialVersionUID = 1L;

  private String sourceLanguage;
  private String targetLanguage;

  private Map<String,String> metadata = new LinkedHashMap<String,String>();

  public Map<String, String> getMetadata() {
    return metadata;
  }

  public void setMetadata(Map<String, String> metadata) {
    this.metadata = metadata;
  }

  /**
   * @return the sourceLanguage
   */
  public String getSourceLanguage() {
    return sourceLanguage;
  }

  /**
   * @param sourceLanguage the sourceLanguage to set
   */
  public void setSourceLanguage(String sourceLanguage) {
    this.sourceLanguage = sourceLanguage;
  }

  public String getTargetLanguage() {
    return targetLanguage;
  }

  public void setTargetLanguage(String targetLanguage) {
    this.targetLanguage = targetLanguage;
  }

}
