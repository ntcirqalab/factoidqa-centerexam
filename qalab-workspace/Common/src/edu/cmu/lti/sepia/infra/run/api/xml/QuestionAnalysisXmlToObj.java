package edu.cmu.lti.sepia.infra.run.api.xml;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import edu.cmu.lti.commons.xml.IXmlHandler;
import edu.cmu.lti.sepia.infra.run.model.InfraAnswerType;
import edu.cmu.lti.sepia.infra.run.model.InfraQuestionAnalysisResult;
import edu.cmu.lti.sepia.infra.run.model.InfraQuestionAnalysisResultSet;
import edu.cmu.lti.sepia.infra.run.model.InfraTerm;

public class QuestionAnalysisXmlToObj extends DefaultHandler implements IXmlHandler {

	enum XML {
		TOPIC, ANSWERTYPE, KEYTERM, ID, SCORE, METADATA, RUNID, DESCRIPTION
	}

	private InfraQuestionAnalysisResultSet result = new InfraQuestionAnalysisResultSet();

	private boolean inRunid = false;
	private boolean inDescription = false;

	private boolean inAnswerType = false;
	private boolean inKeyterm = false;

	private String currentId;
	private InfraQuestionAnalysisResult currentResult;
	private double currentTypeScore;
	private double currentTermScore;

	private StringBuilder buffer = new StringBuilder();

	@Override
	public void startElement( String uri, String localName,
			String qName, Attributes attributes ) {
	  if ( XML.RUNID.toString().equals(qName) ) {
	    inRunid = true;
	    buffer = new StringBuilder();
	  } else if ( XML.DESCRIPTION.toString().equals(qName) ) {
	    inDescription = true;
	    buffer = new StringBuilder();
	  } else if ( XML.TOPIC.toString().equals(qName) ) {
			currentResult = new InfraQuestionAnalysisResult();
			currentId = attributes.getValue(XML.ID.toString());
		} else if ( XML.ANSWERTYPE.toString().equals(qName) ) {
			inAnswerType = true;
			buffer = new StringBuilder();
			String scoreText = attributes.getValue(XML.SCORE.toString());
			if (scoreText!=null) {
			  currentTypeScore = Double.parseDouble( scoreText );
			} else {
			  currentTypeScore = 1.0D;
			}
		} else if ( XML.KEYTERM.toString().equals(qName) ) {
			inKeyterm = true;
			buffer = new StringBuilder();
			String scoreText = attributes.getValue(XML.SCORE.toString());
			if (scoreText!=null) {
			  currentTermScore = Double.parseDouble( scoreText );
      } else {
        currentTermScore = 1.0D;
      }
		}
	}

	@Override
	public void characters(char[] ch, int offset, int length) {
	  if (inRunid||inDescription||inAnswerType||inKeyterm) {
	    String value = new String(ch, offset, length);
	    buffer.append(value);
	  }
	}

	@Override
	public void endElement( String uri, String localName, String qName ) {
   if (inRunid) {
      result.getMetadata().put("RUNID", buffer.toString());
      inRunid = false;
    } else if (inDescription) {
      result.getMetadata().put("DESCRIPTION", buffer.toString());
      inDescription = false;
    } if ( inAnswerType ) {
      InfraAnswerType type = new InfraAnswerType(buffer.toString(), currentTypeScore);
      currentResult.addAnswerType( type );
      inAnswerType = false;
    } else if ( inKeyterm ) {
      InfraTerm t = new InfraTerm(buffer.toString(), currentTermScore);
      currentResult.addTerm( t );
      inKeyterm = false;
    }

    // end of file
	  if ( XML.TOPIC.toString().equals(qName) ) {
			result.put(currentId, currentResult);
		}
	}

	public InfraQuestionAnalysisResultSet getParsed() {
	  return result;
	}

}
