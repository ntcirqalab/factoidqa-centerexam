package edu.cmu.lti.sepia.infra.run.model;

import edu.cmu.lti.commons.xml.IXmlSerializable;
import edu.cmu.lti.sepia.infra.shared.model.TopicMap;

public class InfraRetrievalResultSet
	extends TopicMap<String,InfraRetrievalResult> implements IXmlSerializable {

  public InfraRetrievalResultSet() {
    super();
  }

  public InfraRetrievalResultSet(int initialCapacity) {
    super(initialCapacity);
  }

	private static final long serialVersionUID = 1L;

}
