package edu.cmu.lti.sepia.infra.run.model;

import edu.cmu.lti.commons.xml.IXmlSerializable;
import edu.cmu.lti.sepia.infra.shared.model.TopicMap;

public class InfraQuestionAnalysisResultSet
	extends TopicMap<String,InfraQuestionAnalysisResult> implements IXmlSerializable {

  private static final long serialVersionUID = 1L;

  public InfraQuestionAnalysisResultSet() {
    super();
  }

  public InfraQuestionAnalysisResultSet(int initialCapacity) {
    super(initialCapacity);
  }

}
