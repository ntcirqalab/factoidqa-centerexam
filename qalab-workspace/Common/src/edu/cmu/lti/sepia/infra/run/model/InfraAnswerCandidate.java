package edu.cmu.lti.sepia.infra.run.model;

import edu.cmu.lti.commons.model.BasicTextWrapper;

public class InfraAnswerCandidate extends BasicTextWrapper {

	private double score;
	private String docid;

  public InfraAnswerCandidate(String text, double score, String docid) {
    super(text);
    this.score = score;
    this.docid = docid;
  }

  /**
   * @return the score
   */
  public double getScore() {
    return score;
  }
  /**
   * @return the docid
   */
  public String getDocid() {
    return docid;
  }

  /**
   * @param score the score to set
   */
  public void setScore(double score) {
    this.score = score;
  }

  /**
   * @param docid the docid to set
   */
  public void setDocid(String docid) {
    this.docid = docid;
  }

}
