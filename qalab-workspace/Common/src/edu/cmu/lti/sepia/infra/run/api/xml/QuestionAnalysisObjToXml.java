package edu.cmu.lti.sepia.infra.run.api.xml;

import java.util.List;

import edu.cmu.lti.commons.util.FormatUtil;
import edu.cmu.lti.sepia.infra.run.model.InfraAnswerType;
import edu.cmu.lti.sepia.infra.run.model.InfraQuestionAnalysisResult;
import edu.cmu.lti.sepia.infra.run.model.InfraQuestionAnalysisResultSet;
import edu.cmu.lti.sepia.infra.run.model.InfraTerm;

public class QuestionAnalysisObjToXml {

  public static String serialize( InfraQuestionAnalysisResultSet resultSet ) {
    StringBuilder sb = new StringBuilder();
    sb.append("<TOPIC_SET>\n");
    if (resultSet.getMetadata().size()>0) {
      sb.append("  <METADATA>");
      for ( String key : resultSet.getMetadata().keySet() ) {
        sb.append("  <"+key+">"+resultSet.getMetadata().get(key)+"</"+key+">");
      }
      sb.append("  </METADATA>");
    }

    for ( String topicId : resultSet.keySet() ) {
      InfraQuestionAnalysisResult result = resultSet.get(topicId);
      sb.append("  <TOPIC ID=\""+topicId+"\">\n");
      sb.append("    <QUESTION_ANALYSIS>\n");
      InfraAnswerType atype = result.getAnswerType();
      if (atype!=null) {
        sb.append("      <ANSWERTYPE SCORE=\""+FormatUtil.nf3(atype.getScore())+"\">"+atype.getText()+"</ANSWERTYPE>\n");
      }
      List<InfraTerm> terms = result.getKeyterms();
      if (terms!=null && terms.size()>0) {
        String lang = resultSet.getTargetLanguage();
        sb.append("      <KEYTERMS" );
        sb.append( lang !=null ? (" LANGUAGE=\"" +resultSet.getTargetLanguage()+"\""):"" );
        sb.append(">\n");
        for ( InfraTerm t : terms ) {
          sb.append("        <KEYTERM SCORE=\""+FormatUtil.nf3(t.getScore())+"\">");
          sb.append("<![CDATA["+ t.getText() +"]]>");
          sb.append("</KEYTERM>\n");
        }
        sb.append("      </KEYTERMS>\n");
      }
      sb.append("    </QUESTION_ANALYSIS>\n");
      sb.append("  </TOPIC>\n");
    }
    sb.append("</TOPIC_SET>\n");

    return sb.toString();
  }

}
