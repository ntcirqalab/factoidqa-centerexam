package edu.cmu.lti.sepia.infra.run.api.xml;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import edu.cmu.lti.commons.xml.IXmlHandler;
import edu.cmu.lti.sepia.infra.run.model.InfraAnswerCandidate;
import edu.cmu.lti.sepia.infra.run.model.InfraSelectionResult;
import edu.cmu.lti.sepia.infra.run.model.InfraSelectionResultSet;

public class SelectionResultXmlToObj extends DefaultHandler implements IXmlHandler {

  enum XML {
    TOPIC, ID, ANSWER_CANDIDATE, DOCID, SCORE, METADATA, RUNID, DESCRIPTION
  }

  private InfraSelectionResultSet result = new InfraSelectionResultSet();

  private boolean inRunid = false;
  private boolean inDescription = false;
  private boolean inAnswer = false;

  private String currentId;
  private InfraSelectionResult currentResult;
  private double currentScore;
  private String currentDocidText;
  private StringBuilder buffer = new StringBuilder();

	@Override
	public void startElement( String uri, String localName,
			String qName, Attributes attributes ) {
	  if ( XML.RUNID.toString().equals(qName) ) {
      inRunid = true;
      buffer = new StringBuilder();
    } else if ( XML.DESCRIPTION.toString().equals(qName) ) {
      inDescription = true;
      buffer = new StringBuilder();
    } else if ( XML.TOPIC.toString().equals(qName) ) {
			currentResult = new InfraSelectionResult();
			currentId = attributes.getValue(XML.ID.toString());
		} else if ( XML.ANSWER_CANDIDATE.toString().equals(qName) ) {
		  inAnswer = true;
		  buffer = new StringBuilder();
			currentDocidText = attributes.getValue(XML.DOCID.toString());
			double score;
			String scoreText = attributes.getValue(XML.SCORE.toString());
      if (scoreText!=null) {
        score = Double.parseDouble( scoreText );
      } else {
        score = 1.0D;
      }
			currentScore = score;
		}
	}

  @Override
  public void characters(char[] ch, int offset, int length) {
    if (inRunid||inDescription||inAnswer) {
      String value = new String(ch, offset, length);
      buffer.append(value);
    }
  }

	@Override
	public void endElement( String uri, String localName, String qName ) {
	  if (inAnswer) {
      InfraAnswerCandidate a = new InfraAnswerCandidate(buffer.toString(),
              currentScore, currentDocidText);
      currentResult.add(a);
      inAnswer = false;
    } else if (inRunid) {
      result.getMetadata().put("RUNID", buffer.toString());
      inRunid = false;
    } else if (inDescription) {
      result.getMetadata().put("DESCRIPTION", buffer.toString());
      inDescription = false;
    }
	  // end of file
	  if ( XML.TOPIC.toString().equals(qName) ) {
			result.put(currentId, currentResult);
		}
	}

	public InfraSelectionResultSet getParsed() {
	  return result;
	}

}
