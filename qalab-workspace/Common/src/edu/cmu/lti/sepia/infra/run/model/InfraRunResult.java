package edu.cmu.lti.sepia.infra.run.model;

public class InfraRunResult {

  private InfraQuestion question;
  private InfraQuestionAnalysisResult qaResult;
  private InfraRetrievalResult rsResult;
  private InfraExtractionResult ixResult;
  private InfraSelectionResult agResult;

  public InfraRunResult(InfraQuestion question) {
    this.question = question;
  }

  /**
   * @param question the question to set
   */
  public void setQuestion(InfraQuestion question) {
    this.question = question;
  }

  /**
   * @param qaResult the qaResult to set
   */
  public void setQuestionAnalysisResult(InfraQuestionAnalysisResult qaResult) {
    this.qaResult = qaResult;
  }

  /**
   * @param rsResult the rsResult to set
   */
  public void setRetrievalResult(InfraRetrievalResult rsResult) {
    this.rsResult = rsResult;
  }

  /**
   * @param ixResult the ixResult to set
   */
  public void setExtractionResult(InfraExtractionResult ixResult) {
    this.ixResult = ixResult;
  }

  /**
   * @param agResult the agResult to set
   */
  public void setSelectionResult(InfraSelectionResult agResult) {
    this.agResult = agResult;
  }

  /**
   * @return the question
   */
  public InfraQuestion getQuestion() {
    return question;
  }
  /**
   * @return the qaResult
   */
  public InfraQuestionAnalysisResult getQuestionAnalysisResult() {
    return qaResult;
  }
  /**
   * @return the rsResult
   */
  public InfraRetrievalResult getRetrievalResult() {
    return rsResult;
  }
  /**
   * @return the ixResult
   */
  public InfraExtractionResult getExtractionResult() {
    return ixResult;
  }
  /**
   * @return the agResult
   */
  public InfraSelectionResult getSelectionResult() {
    return agResult;
  }

}
