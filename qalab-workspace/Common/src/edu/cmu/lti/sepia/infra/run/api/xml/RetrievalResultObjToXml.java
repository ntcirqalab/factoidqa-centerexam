package edu.cmu.lti.sepia.infra.run.api.xml;

import edu.cmu.lti.commons.util.FormatUtil;
import edu.cmu.lti.sepia.infra.run.model.InfraDocument;
import edu.cmu.lti.sepia.infra.run.model.InfraRetrievalResult;
import edu.cmu.lti.sepia.infra.run.model.InfraRetrievalResultSet;

public class RetrievalResultObjToXml {

  public static String serialize( InfraRetrievalResultSet resultSet ) {
    StringBuilder sb = new StringBuilder();
    sb.append("<TOPIC_SET>\n");
    if (resultSet.getMetadata().size()>0) {
      sb.append("  <METADATA>");
      for ( String key : resultSet.getMetadata().keySet() ) {
        sb.append("  <"+key+">"+resultSet.getMetadata().get(key)+"</"+key+">");
      }
      sb.append("  </METADATA>");
    }
    for ( String topicId : resultSet.keySet() ) {
      InfraRetrievalResult result = resultSet.get(topicId);
      sb.append("  <TOPIC ID=\""+topicId+"\">\n");
      sb.append("    <IR4QA_RESULT>\n");
      for ( int i=0; i<result.size(); i++ ) {
        InfraDocument d = result.get(i);
        sb.append("      <DOCUMENT RANK=\""+(i+1)+"\" ");
        sb.append("DOCID=\""+d.getDocid()+"\" ");
        sb.append("SCORE=\""+FormatUtil.nf3(d.getScore())+"\" />\n");
      }
      sb.append("    </IR4QA_RESULT>\n");
      sb.append("  </TOPIC>\n");
    }
    sb.append("</TOPIC_SET>\n");

    return sb.toString();
  }

}
