package edu.cmu.lti.sepia.infra.run.model;

public class InfraTerm {

  private String term;
  private double score;

  public InfraTerm( String term, double score ) {
    this.term = term;
    this.score = score;
  }

  /**
   * @param text the term to set
   */
  public void setText(String text) {
    this.term = text;
  }

  /**
   * @param score the score to set
   */
  public void setScore(double score) {
    this.score = score;
  }

  public String getText() {
    return term;
  }

  public double getScore() {
    return score;
  }

  @Override
  public String toString() {
    return term;
  }

}
