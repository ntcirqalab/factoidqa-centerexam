package edu.cmu.lti.sepia.infra.run.api.xml;

import edu.cmu.lti.commons.util.FormatUtil;
import edu.cmu.lti.sepia.infra.run.model.InfraAnswerCandidate;
import edu.cmu.lti.sepia.infra.run.model.InfraSelectionResult;
import edu.cmu.lti.sepia.infra.run.model.InfraSelectionResultSet;

public class SelectionResultObjToXml {

  public static String serialize( InfraSelectionResultSet resultSet ) {
    StringBuilder sb = new StringBuilder();
    sb.append("<TOPIC_SET>\n");
    if (resultSet.getMetadata().size()>0) {
      sb.append("  <METADATA>");
      for ( String key : resultSet.getMetadata().keySet() ) {
        sb.append("  <"+key+">"+resultSet.getMetadata().get(key)+"</"+key+">");
      }
      sb.append("  </METADATA>");
    }

    for ( String topicId : resultSet.keySet() ) {
      InfraSelectionResult result = resultSet.get(topicId);
      sb.append("  <TOPIC ID=\""+topicId+"\">\n");
      sb.append("    <CCLQA_RESULT>\n");
      for ( int i=0; i<result.size(); i++ ) {
        InfraAnswerCandidate a = result.get(i);
        sb.append("      <ANSWER_CANDIDATE RANK=\""+(i+1)+"\" ");
        sb.append("DOCID=\""+a.getDocid()+"\" ");
        sb.append("SCORE=\""+FormatUtil.nf3(a.getScore())+"\">");
        sb.append("<![CDATA["+ a.getText() +"]]>");
        sb.append("</ANSWER_CANDIDATE>\n");
      }
      sb.append("    </CCLQA_RESULT>\n");
      sb.append("  </TOPIC>\n");
    }
    sb.append("</TOPIC_SET>\n");

    return sb.toString();
  }

}
