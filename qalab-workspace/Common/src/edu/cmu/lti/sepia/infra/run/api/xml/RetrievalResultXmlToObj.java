package edu.cmu.lti.sepia.infra.run.api.xml;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import edu.cmu.lti.commons.xml.IXmlHandler;
import edu.cmu.lti.sepia.infra.run.model.InfraDocument;
import edu.cmu.lti.sepia.infra.run.model.InfraRetrievalResult;
import edu.cmu.lti.sepia.infra.run.model.InfraRetrievalResultSet;

public class RetrievalResultXmlToObj extends DefaultHandler implements IXmlHandler {

	enum XML {
		TOPIC, ID, DOCUMENT, DOCID, SCORE, METADATA, RUNID, DESCRIPTION
	}

	private InfraRetrievalResultSet result = new InfraRetrievalResultSet();

  private boolean inRunid = false;
  private boolean inDescription = false;

	private String currentId;
	private InfraRetrievalResult currentResult;

	private StringBuilder buffer = new StringBuilder();

	@Override
	public void startElement( String uri, String localName,
			String qName, Attributes attributes ) {
	  if ( XML.RUNID.toString().equals(qName) ) {
      inRunid = true;
      buffer = new StringBuilder();
    } else if ( XML.DESCRIPTION.toString().equals(qName) ) {
      inDescription = true;
      buffer = new StringBuilder();
    } else if ( XML.TOPIC.toString().equals(qName) ) {
			currentResult = new InfraRetrievalResult();
			currentId = attributes.getValue(XML.ID.toString());
		} else if ( XML.DOCUMENT.toString().equals(qName) ) {
			String docidText = attributes.getValue(XML.DOCID.toString());
			double score;
			String scoreText = attributes.getValue(XML.SCORE.toString());
      if (scoreText!=null) {
        score = Double.parseDouble( scoreText );
      } else {
        score = 1.0D;
      }
			InfraDocument d = new InfraDocument( docidText, score );
			currentResult.add(d);
		}
	}

	@Override
  public void characters(char[] ch, int offset, int length) {
    if (inRunid||inDescription) {
      String value = new String(ch, offset, length);
      buffer.append(value);
    }
  }

	@Override
	public void endElement( String uri, String localName, String qName ) {
    if (inRunid) {
      result.getMetadata().put("RUNID", buffer.toString());
      inRunid = false;
    } else if (inDescription) {
      result.getMetadata().put("DESCRIPTION", buffer.toString());
      inDescription = false;
    }

    //end of file
	  if ( XML.TOPIC.toString().equals(qName) ) {
			result.put(currentId, currentResult);
		}
	}

	public InfraRetrievalResultSet getParsed() {
	  return result;
	}

}
