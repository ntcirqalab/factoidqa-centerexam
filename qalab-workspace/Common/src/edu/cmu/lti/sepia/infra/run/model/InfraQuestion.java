package edu.cmu.lti.sepia.infra.run.model;

import edu.cmu.lti.commons.model.BasicTextWrapper;

public class InfraQuestion extends BasicTextWrapper {

	protected String id;
	//private String informationNeed;
	//private LangPair;

	public InfraQuestion( String id, String question ) {
	  super(question);
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the question
	 */
	public String getQuestion() {
		return getText();
	}

  @Override
  public String toString() {
    return "Question [id=" + id + ", question=" + getText() + "]";
  }

}
