package edu.cmu.lti.indri;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IndriUtil
{
	private final static Pattern pPhrase = Pattern.compile("#[12]\\((.+?)\\)");

	//Note: segmentation lost #1(A B) => AB
	public static String[] getQueryTerms( String indriQuery ) {
		String q = indriQuery.replaceAll(" ", "");
		q = q.replaceAll("\"", " ");
		q = q.replaceAll( "#.*?\\(", "(" );
		q = q.replaceAll( "\\(|\\)", " " );
		q = q.replaceAll( "(^[ ]+)|([ ]+$)", "" );
		String[] queryTerms = q.split("[ ]+");
		//System.out.println("Query: "+java.util.Arrays.toString(queryTerms));
		return queryTerms;
	}

	public static String[] getQueryPhrasesWithSegmentation( String indriQuery ) {
		List<String> queryPhrases = new ArrayList<String>();
		Matcher mPhrase = pPhrase.matcher(indriQuery);
		while ( mPhrase.find() ) {
			String phrase = mPhrase.group(1);
			queryPhrases.add(phrase.trim());
		}
		return (String[]) queryPhrases.toArray(new String[queryPhrases.size()]);
	}

	/**
	 * This method is for filtering out bad sentences
	 * returned from Indri's boolean sentence retrieval.
	 *
	 * Indri sometimes returns bad sentences in boolean sentence retrieval. For example,
	 * sentence retrieval given term X and Y will be done with a query <code>
	 * #combine[sentence]( #uw( X Y ) ) </code> which returns sentences that
	 * do not contain X or Y.
	 *
	 * @param results list of indri results
	 * @param requiredTerms terms that must be contained in the retrieved sentences
	 * @return filtered results
	 */
	public static List<String> selectSentences(List<String> sentenceCandidates, List<String> requiredTerms) {
		Set<String> newResults = new LinkedHashSet<String>( sentenceCandidates.size() );
		Set<String> ngResults = new LinkedHashSet<String>( sentenceCandidates.size() );

		newResults.addAll( sentenceCandidates );

		for ( String s : newResults ) {
			for ( String term : requiredTerms ) {
				if ( s.indexOf( term ) == -1 ) {
					ngResults.add(s);
					break;
				}
			}
		}
		newResults.removeAll( ngResults );

//		log.trace("Filtered out "+ngResults.size()+", obtained "+newResults.size()+" sentence candidates.");
//		log.trace("Retrieved sentences for " + requiredTerms + "\n"+newResults.toString().replaceAll(", ", "\n") );

		return new ArrayList<String>(newResults);
	}

	public static String validateIndri( String indriHome, String[] indices )
	{
		String validationReport = "";
		File indriHomeDir = new File(indriHome);
		if ( ! indriHomeDir.exists() ) {
			validationReport += "INDRI home cannot be found at: "+indriHomeDir.getAbsolutePath()+"<br>\n";
		}
		File runQuery = new File(indriHome+"/bin/runquery");
		if ( ! runQuery.exists() ) {
			validationReport += "INDRI binary cannot be found at: "+runQuery.getAbsolutePath()+"<br>\n";
		}
		for ( String index : indices ) {
			File indexFile = new File(index);
			if ( ! indexFile.exists() ) {
				validationReport += "INDRI index cannot be found at: "+indexFile.getAbsolutePath()+"<br>\n";
			}
		}
		return validationReport;
	}
}
