package edu.cmu.lti.kiji.aclia2.tm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedHashMap;
import java.util.Map;

//import org.apache.log4j.Logger;

import com.google.api.translate.Language;


public class TranslationCache
{
//	private static final long serialVersionUID = 20100113L;

	public final static String CACHE_PATH = "src/main/resources/translation.cache";

	private final static int CACHE_INTERVAL = 10;

	private int cacheCounter = 0;

	//  private final static Logger log = Logger.getLogger(TranslationCache.class.getName());

	//Language to src word to trg word
	private Map<String,String> cache;
	//private final static int CACHE_SIZE = 10000;

	public void store(Language srcLang, Language trgLang, String srcText, String trgText) {
		if (trgText==null) {
			return;
		}
		if (cache==null) {
			load();
		}
		String key = genKey(srcLang,trgLang,srcText);
		cache.put(key, trgText);

		//save cached content every CACHE_INTERVAL execution
		if (cacheCounter++>CACHE_INTERVAL) {
			save();
			cacheCounter=0;
		}
	}

	public String lookup(Language srcLang, Language trgLang, String srcText) {
		if (cache==null) {
			load();
		}

		String key = genKey(srcLang,trgLang,srcText);
		return cache.get(key);
	}

	private String genKey(Language srcLang, Language trgLang, String srcText) {
		return srcLang.toString()+"<>"+trgLang.toString()+"<>"+srcText;
	}

	/**
	 * @return the cache
	 */
	public Map<String, String> getCache() {
		return cache;
	}

	@SuppressWarnings("unchecked")
	public void load() {
		try {
			File file = new File(CACHE_PATH);
			FileInputStream inFile = new FileInputStream(file);
			ObjectInputStream inObject = new ObjectInputStream(inFile);
			Map<String, String> tc = (Map<String, String>) inObject.readObject();
			this.cache = tc;
			//      log.info("Loaded translation cache with "+tc.size()+" entries in "+file.length()+" bytes.");
			inObject.close();
			inFile.close();
		} catch (Exception e) {
			e.printStackTrace();
			cache = new LinkedHashMap<String,String>(1024);
//			log.info("Initialized translation cache.");
		}
	}

	public void save() {
		try {
			File file = new File(CACHE_PATH);
			FileOutputStream outFile = new FileOutputStream(file);
			ObjectOutputStream outObject = new ObjectOutputStream(outFile);
			outObject.writeObject(cache);
			outObject.close();
			outFile.close();
			//      log.info("Saved translation cache with "+cache.size()+" entries in "+file.length()+" bytes.");
		} catch (Exception e) {
			e.printStackTrace();
//			log.info("Failed to save translation cache.");
		}
	}

	public void clear() {
		cache = new LinkedHashMap<String,String>(1024);
//		log.info("Cleared translation cache");
	}
}
