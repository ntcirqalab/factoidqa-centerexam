package edu.cmu.lti.kiji.aclia2.tm;

import java.util.Random;

import com.google.api.translate.Language;
import com.google.api.translate.Translate;


public class GoogleTranslator
{
	private TranslationCache tc;

	public GoogleTranslator() {
		Translate.setHttpReferrer("http://mu.lti.cs.cmu.edu/");
		tc = new TranslationCache();
	}

	@SuppressWarnings("static-access")
	public String translate(Language srcLang, Language trgLang, String srcText) {
		String lookupResult = tc.lookup(srcLang, trgLang, srcText);
		if (lookupResult!=null) {
			return lookupResult;
		}
		String trgText = null;
		try {
			Thread.currentThread().sleep((long)(2000*(1+new Random().nextDouble())));
			trgText = Translate.execute(srcText, srcLang, trgLang);
			tc.store(srcLang, trgLang, srcText, trgText);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return trgText;
	}

	public void saveCache() {
		tc.save();
	}
}
