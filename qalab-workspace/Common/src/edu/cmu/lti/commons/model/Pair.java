package edu.cmu.lti.commons.model;

public class Pair<T1, T2> {

  private T1 firstElement;

  private T2 secondElement;

  public Pair(T1 firstElement, T2 secondElement) {
    this.firstElement = firstElement;
    this.secondElement = secondElement;
  }

  /**
   * @return the firstElement
   */
  public T1 getFirstElement() {
    return firstElement;
  }

  /**
   * @param firstElement
   *          the firstElement to set
   */
  public void setFirstElement(T1 firstElement) {
    this.firstElement = firstElement;
  }

  /**
   * @return the secondElement
   */
  public T2 getSecondElement() {
    return secondElement;
  }

  /**
   * @param secondElement
   *          the secondElement to set
   */
  public void setSecondElement(T2 secondElement) {
    this.secondElement = secondElement;
  }

}
