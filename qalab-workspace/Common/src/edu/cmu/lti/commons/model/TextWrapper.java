package edu.cmu.lti.commons.model;

import java.util.List;

public interface TextWrapper {
  String getText();
  List<String> getTokens();
}
//
//interface TermWrapper {
//  String unwrap();
//  double getDF();
//  String getPOS();
//  String getSynset();
//}