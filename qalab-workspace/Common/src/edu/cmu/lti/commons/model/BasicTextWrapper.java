package edu.cmu.lti.commons.model;

import java.util.List;

public class BasicTextWrapper implements TextWrapper {
  protected String text;
  protected List<String> tokens;

  /**
   * @param text
   */
  public BasicTextWrapper(String text) {
    this.text = text;
  }

  /**
   * @param text
   * @param tokens
   */
  public BasicTextWrapper(String text, List<String> tokens) {
    this.text = text;
    this.tokens = tokens;
  }

  public String getText() {
    return text;
  }

  public List<String> getTokens() {
    return tokens;
  }

  /**
   * @param text the text to set
   */
  public void setText(String text) {
    this.text = text;
  }

  /**
   * @param tokens the tokens to set
   */
  public void setTokens(List<String> tokens) {
    this.tokens = tokens;
  }

  @Override
  public String toString() {
    return getText();
  }

}
//
//interface TermWrapper {
//  String unwrap();
//  double getDF();
//  String getPOS();
//  String getSynset();
//}