package edu.cmu.lti.commons.model;

import java.util.HashMap;
import java.util.Map;

public class Annotation {

	private int begin;
	private int end;
	private String label;
	private Map<String,String> features;

	public String getCoveredText(String text) {
	  return text.substring(begin, end);
	}

  /**
	 * @return the begin
	 */
	public int getBegin() {
		return begin;
	}
	/**
	 * @param begin the begin to set
	 */
	public void setBegin(int begin) {
		this.begin = begin;
	}
	 /**
   * @return the end
   */
  public int getEnd() {
    return end;
  }
  /**
   * @param end the end to set
   */
  public void setEnd(int end) {
    this.end = end;
  }
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

  /**
   * @return the features
   */
  public Map<String, String> getFeatures() {
    return features;
  }
  /**
   * @param features the feature to set
   */
  public void setFeatures(Map<String, String> features) {
    this.features = features;
  }

  public String getFeature(String key) {
    return (features!=null)?features.get(key):null;
  }

  public void putFeature(String key, String value) {
    if (features==null) features = new HashMap<String,String>();
    features.put(key, value);
  }

}
