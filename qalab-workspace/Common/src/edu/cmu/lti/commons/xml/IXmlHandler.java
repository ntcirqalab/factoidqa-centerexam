package edu.cmu.lti.commons.xml;

import org.xml.sax.ContentHandler;

public interface IXmlHandler extends ContentHandler {
  public IXmlSerializable getParsed();
}
