package edu.cmu.lti.commons.xml;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class Deserializer {

  private IXmlHandler handler;

  public Deserializer(IXmlHandler handler) {
    this.handler = handler;
  }

  public IXmlSerializable deserialize(InputStream inputStream) {
    IXmlSerializable result = null;
    try {
      XMLReader xmlReader = XMLReaderFactory.createXMLReader();
      xmlReader.setContentHandler(handler);
      xmlReader.parse(new InputSource(inputStream));
      result = handler.getParsed();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (SAXException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      try {
        inputStream.close();
      } catch (IOException e) {
      }
    }
    return result;
  }

}
