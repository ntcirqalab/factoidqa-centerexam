package edu.cmu.lti.commons.util;

import java.text.NumberFormat;

public class FormatUtil {

  //new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime())??

  private static NumberFormat nf1 = NumberFormat.getNumberInstance();
	private static NumberFormat nf2 = NumberFormat.getNumberInstance();
	private static NumberFormat nf3 = NumberFormat.getNumberInstance();
	private static NumberFormat nf4 = NumberFormat.getNumberInstance();

	static {
	  nf1.setMaximumFractionDigits(2);
	  nf1.setMinimumFractionDigits(2);
		nf2.setMaximumFractionDigits(2);
		nf2.setMinimumFractionDigits(2);
		nf3.setMaximumFractionDigits(3);
		nf3.setMinimumFractionDigits(3);
		nf4.setMaximumFractionDigits(4);
		nf4.setMinimumFractionDigits(4);
	}

	public static String nf2( double d ) {
		return nf2.format(d);
	}
	public static String nf3( double d ) {
	  return nf3.format(d);
	}
	public static String nf4( double d ) {
	  return nf4.format(d);
	}

}
