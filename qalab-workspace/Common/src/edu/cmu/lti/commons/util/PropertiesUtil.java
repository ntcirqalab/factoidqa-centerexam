package edu.cmu.lti.commons.util;

import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtil {

	public static Properties loadProperties( String propertyFileName ) {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		InputStream is = cl.getResourceAsStream( propertyFileName );

		Properties p = new Properties();
		try {
			p.load(is);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return p;
	}
}
