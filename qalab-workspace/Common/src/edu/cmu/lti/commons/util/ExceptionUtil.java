package edu.cmu.lti.commons.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionUtil {

  public static String exceptionToString(Exception e) {
    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);
    e.printStackTrace(pw);
    return sw.toString();

//// Alternative impl
//  ByteArrayOutputStream baos = new ByteArrayOutputStream();
//  PrintWriter printWriter = new PrintWriter(baos);
//  e.printStackTrace(printWriter);
//  printWriter.flush();
//  String stackTrace = new String(baos.toByteArray());

  }

}
