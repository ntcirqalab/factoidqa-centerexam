package edu.cmu.lti.commons.command;

import java.util.Properties;

public abstract class AbstractCommandWrapper {

  protected CommandExecutor executor;

  public AbstractCommandWrapper( Properties properties ) {
    this.executor = CommandExecutorFactory.create(properties, getPropertyPrefix());
  }

  public AbstractCommandWrapper( CommandExecutor executor ) {
    this.executor = executor;
  }

  protected abstract String getPropertyPrefix();

}
