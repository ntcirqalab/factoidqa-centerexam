package edu.cmu.lti.commons.command;

import java.io.IOException;

import javax.management.MXBean;

//Needs to be a public interface
@MXBean
public interface ICommandMXBean {
  public String execute( String[] cmdarray, String stdin, String encoding ) throws IOException;
}