package edu.cmu.lti.commons.command;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import edu.cmu.lti.commons.util.ExceptionUtil;

public class CommandExecutor
{
	private final static String EXECUTE = "execute";
	private final CommandService service;
	private ICommandExecutor local;
	private ICommandExecutor remote;
	private boolean localFailed = false;

	private List<String> cmdlist;
	private String encoding;

	public CommandExecutor(String hostname, int port, String[] cmdarray, String encoding) {
		this(new CommandService(hostname, port), cmdarray, encoding);
	}

	public CommandExecutor(String hostname, int port, String[] cmdarray) {
		this(new CommandService(hostname, port), cmdarray, null);
	}

	public CommandExecutor(CommandService service, String[] cmdarray) {
		this(service, cmdarray, null);
	}

	public CommandExecutor(CommandService service, String[] cmdarray, String encoding) {
		this.service = service;
		this.cmdlist = new ArrayList<String>(Arrays.asList(cmdarray));
		this.encoding = encoding;
	}

	public void appendArgument( String arg ) {
		cmdlist.add(arg);
	}

	public String executeFailSafe(String stdin, String[] additionalArgs) throws IOException {
		if (localFailed) {
			return executeRemote(stdin,additionalArgs);
		} else {
			try {
				return executeLocal(stdin,additionalArgs);
			} catch ( IOException e ) {
				e.printStackTrace();
				localFailed = true;
				return executeRemote(stdin,additionalArgs);
			}
		}
	}

	public String executeFailSafe(String[] additionalArgs) throws IOException {
		return executeFailSafe(null,additionalArgs);
	}

	public String executeFailSafe(String stdin) throws IOException {
		return executeFailSafe(stdin,null);
	}

	public String executeFailSafe() throws IOException {
		return executeFailSafe(null,null);
	}

	public String executeLocal(String stdin, String[] additionalArgs) throws IOException {
		if (local==null) local = new LocalCommandExecutor();
		String[] cmdarray = generateCommandArray(additionalArgs);
		return local.execute(cmdarray,stdin,encoding);
	}

	public String executeLocal(String[] additionalArgs) throws IOException {
		return executeLocal(null, additionalArgs);
	}

	public String executeLocal(String stdin) throws IOException {
		return executeLocal(stdin, null);
	}

	public String executeLocal() throws IOException {
		return executeLocal(null, null);
	}

	public String executeRemote(String stdin, String[] additionalArgs) throws IOException {
		if (remote==null) remote = new RemoteCommandExecutor();
		String[] cmdarray = generateCommandArray(additionalArgs);
		return remote.execute(cmdarray,stdin,encoding);
	}

	public String executeRemote(String[] additionalArgs) throws IOException {
		return executeRemote(null,additionalArgs);
	}

	public String executeRemote(String stdin) throws IOException {
		return executeRemote(stdin,null);
	}

	public String executeRemote() throws IOException {
		return executeRemote(null,null);
	}

	private String[] generateCommandArray( String[] additionalArgs ) {
		int n = cmdlist.size();
		if ( additionalArgs==null ) {
			return (String[]) cmdlist.toArray(new String[n]);
		} else {
			String[] cmdarray = new String[n+additionalArgs.length];
			for (int i=0; i<n; i++) {
				cmdarray[i] = cmdlist.get(i);
			}
			for (int i=0; i<additionalArgs.length; i++) {
				cmdarray[n+i] = additionalArgs[i];
			}
			return cmdarray;
		}
	}

	private class RemoteCommandExecutor implements ICommandExecutor {
		public String execute(String[] cmdarray, String stdin, String encoding) {
			try {
				JMXServiceURL url = service.getJMXServiceURL();
				JMXConnector connector = JMXConnectorFactory.connect(url);
				MBeanServerConnection conn = connector.getMBeanServerConnection();
				ObjectName name = service.getObjectName();

				Object[] args = new Object[]{cmdarray,stdin,encoding};
				String[] signature = new String[]{"[Ljava.lang.String;","java.lang.String","java.lang.String"};
				return (String) conn.invoke(name, EXECUTE, args, signature);
			} catch (Exception e) {
				e.printStackTrace();
				return ExceptionUtil.exceptionToString(e);
			}
		}
	}

	private class LocalCommandExecutor implements ICommandExecutor {
		private ICommandMXBean command = new CommandImpl();
		public String execute(String[] cmdarray,String stdin,String encoding) throws IOException {
			return command.execute(cmdarray,stdin,encoding);
		}
	}

	private interface ICommandExecutor {
		String execute( String[] cmdarray, String stdin, String encoding ) throws IOException;
	}

	public static void main(String[] args) throws IOException {
		String[] cmdarray = {"/bin/ls","-al"};

		CommandService service = CommandService.parseArguments(args);
		CommandExecutor c = new CommandExecutor(service,cmdarray);
		//    String[] cmdarray = {"cmd", "/C", "dir"};

		System.out.println( c.executeRemote() );
	}
}
