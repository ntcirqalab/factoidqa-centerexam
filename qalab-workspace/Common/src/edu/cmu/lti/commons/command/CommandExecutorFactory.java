package edu.cmu.lti.commons.command;

import java.rmi.registry.Registry;
import java.util.Properties;

public class CommandExecutorFactory {

  public static CommandExecutor create( Properties p, String prefix ) {
    String serverText = p.getProperty(prefix+".server"); // "server.lti.cs.cmu.edu:1099" or "server.lti.cs.cmu.edu"
    if ( serverText == null ) {
      serverText = p.getProperty("default.server");
    }
    String[] hostAndPort = serverText.trim().split(":");
    String host = hostAndPort[0];
    int port = Registry.REGISTRY_PORT;
    if ( hostAndPort.length==2 ) {
      port = Integer.parseInt(hostAndPort[1]);
    }

    String cmdarrayText = p.getProperty(prefix+".command");
    if ( cmdarrayText == null ) {
      cmdarrayText = p.getProperty("default.command");
    }
    String encodingText = p.getProperty(prefix+".encoding");
    if ( encodingText == null ) {
      encodingText = p.getProperty("default.encoding");
    }
    if ( encodingText == null ) {
      encodingText = "utf-8";
    }

    String[] cmdarray = {cmdarrayText.trim()};
    return new CommandExecutor(host, port, cmdarray, encodingText.trim());
  }

}
