package edu.cmu.lti.commons.command;

import java.lang.management.ManagementFactory;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;

public class CommandService {

	private static String OBJECT_NAME = "edu.cmu.lti.commons.command:type=CommandImpl";

	private static ObjectName objectName;
	private JMXServiceURL url;
	private String hostname;
	private int port;

	static {
		try {
			objectName = new ObjectName(OBJECT_NAME);
		} catch ( Exception e ) {
			e.printStackTrace();
		}
	}

	public CommandService( String hostname, int port ) {
		try {
			this.hostname = hostname;
			this.port = port;
			this.url = new JMXServiceURL(getAddressFromHost(hostname,port));
		} catch ( Exception e ) {
			e.printStackTrace();
		}
	}

	private String getAddressFromHost( String hostname, int port ) {
		return "service:jmx:rmi:///jndi/rmi://"+hostname+":"+port+"/edu.cmu.lti.commons.command";
	}

	/**
	 * @return the hostname
	 */
	public String getHostname() {
		return hostname;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @return the objectName
	 */
	public ObjectName getObjectName() {
		return objectName;
	}

	public JMXServiceURL getJMXServiceURL() {
		return url;
	}

	public void startService() throws Exception {
		LocateRegistry.createRegistry(port);
		MBeanServer server = ManagementFactory.getPlatformMBeanServer();
		server.registerMBean(new CommandImpl(), objectName);
		JMXConnectorServer connector = JMXConnectorServerFactory.newJMXConnectorServer(url, null, server);
		//  System.out.println( connector.isActive()?"active":"not active" );
		connector.start();
		//  System.out.println("start...");
	}

	public static CommandService parseArguments(String[] args) {
		CommandService service = null;
		if ( args.length>0 ) {
			String[] element = args[0].split(":");
			String hostname = element[0];
			int port = Registry.REGISTRY_PORT;
			try {
				port = Integer.parseInt(element[1]);
			} catch (Exception e) {
				e.printStackTrace();
			}
			service = new CommandService( hostname, port );
		}
		return service;
	}

	//Expected arg: "localhost". In the future, port number will be supported e.g. "localhost:1200".
	public static void main(String[] args) {
		CommandService service = parseArguments(args);
		try {
			service.startService();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

}
