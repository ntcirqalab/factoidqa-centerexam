package edu.cmu.lti.commons.command;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.PumpStreamHandler;

public class CommandImpl implements ICommandMXBean {

	private final static int DEFAULT_TIMEOUT = 30000;

	public String execute( String[] cmdarray ) throws IOException {
		return execute( toCommandLine(cmdarray), null, null, DEFAULT_TIMEOUT );
	}

	public String execute( String[] cmdarray, String stdin, String encoding ) throws IOException {
		return execute( toCommandLine(cmdarray), stdin, encoding, DEFAULT_TIMEOUT );

//		return execute( toCommandLine(cmdarray,encoding), stdin, encoding, DEFAULT_TIMEOUT );
	}

	private CommandLine toCommandLine( String[] cmdarray ) {
		CommandLine commandLine = CommandLine.parse(cmdarray[0]);
		for ( int i=1; i<cmdarray.length; i++ ) {
			commandLine.addArgument(cmdarray[i], false);
		}
		return commandLine;
	}

	private CommandLine toCommandLine( String[] cmdarray,String encoding )
	{
		CommandLine commandLine	= null;
		try {
//System.out.println("CommandLine="+new String(cmdarray[0].getBytes(encoding),"ISO-8859-1"));
			commandLine = CommandLine.parse(new String(cmdarray[0].getBytes(encoding),"ISO-8859-1"));
			for ( int i=1; i<cmdarray.length; i++ ) {
//				commandLine.addArgument(new String(cmdarray[i].getBytes(encoding),"ISO-8859-1"), false);
				String	s1	= new String(cmdarray[i].getBytes(encoding),"ISO-8859-1");
				String	s2	= new String(s1.getBytes("Windows-31J"),"ISO-8859-1");
				commandLine.addArgument(s2, false);
//				commandLine.addArgument(new String(cmdarray[i].getBytes(encoding),"ISO-8859-1").getBytes("Windows-31J"), false);
			}
		} catch (UnsupportedEncodingException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return commandLine;
	}

	private String execute( CommandLine commandLine, String stdin, String encoding, int timeout ) throws IOException {
		ExecuteWatchdog watchdog = new ExecuteWatchdog(timeout);
		DefaultExecutor executor = new DefaultExecutor();
		executor.setWatchdog(watchdog);

		if (encoding==null) {
			encoding = "utf-8";
		}

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		PumpStreamHandler psh;
		if (stdin!=null) {
			InputStream is = new ByteArrayInputStream(stdin.getBytes(encoding));
			psh = new PumpStreamHandler(out,out,is);
		} else {
			psh = new PumpStreamHandler(out,out);
		}

		executor.setStreamHandler(psh);
		@SuppressWarnings("unused")
		int exitValue = executor.execute(commandLine);

		String	result	= new String(out.toByteArray(), encoding);

//System.out.println("command:"+commandLine+",result"+result);
//System.out.println("command:"+commandLine);
//System.out.println("result"+new String(out.toByteArray(),"utf-8"));

// TODO Windowsでの開発ということで、暫定で固定に
//return new String(out.toByteArray(), "windows-31j");

//		return result;
		return new String(out.toByteArray(), encoding);
	}
}

