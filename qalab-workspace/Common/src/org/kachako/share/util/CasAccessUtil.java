package org.kachako.share.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.uima.jcas.JCas;


/*
 * CASにアクセスするためのユーティリティクラス.
 */
public class CasAccessUtil {

	public CasAccessUtil() {
	}

	/**
	 * CASのドキュメントの内容を取得する.
	 * 
	 * @param jCas
	 * @return テキスト
	 */
	public static String getTextData(JCas jCas)
	{
		if(jCas == null) {
			return null;
		}

		String document	= jCas.getDocumentText();
		if(document != null) {
			return document;
		}

		String sofaUri	= jCas.getSofaDataURI();
		if(sofaUri == null) {
			return null;
		}

		Path path	= Paths.get(sofaUri);
		if(!Files.exists(path)) {
			return null;
		}

		if(Files.isSymbolicLink(Paths.get(sofaUri))) {
			// シンボリックリンクの場合
			try {
				String data	= ZipUtil.readFile(Files.readSymbolicLink(path).toString(), path.toFile().getName()+".txt");
				return data;
			} catch (IOException e) {
			      e.printStackTrace();
			}
		      return null;
		}
		
		Pattern pattern	= Pattern.compile("\\.zip$");
		Matcher matcher	= pattern.matcher(sofaUri);
		if(matcher.find()) {
			// zipファイルの場合
			String fileName = path.toFile().getName(); 
			String targetFilePath	= fileName.substring(0, fileName.lastIndexOf(".")) + ".txt";

			String data	= ZipUtil.readFile(path.toString(),targetFilePath);
			return data;
		}

		// その他のファイル
		StringBuilder sb	= new StringBuilder();
		BufferedReader br	= null;
		try {
			br	= new BufferedReader(new FileReader(path.toFile()));
			String str	= null;
			while((str = br.readLine()) != null) {
				sb.append(str);
				sb.append("\n");
			}
			return sb.toString();
		} catch (FileNotFoundException e) {
		      e.printStackTrace();
		} catch (IOException e) {
		      e.printStackTrace();
		} finally {
			try {
				if(br != null) {
					br.close();
				}
			} catch (Exception e) {
			      e.printStackTrace();
			}
		}

		return null;
	}
}
