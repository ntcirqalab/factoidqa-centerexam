package org.kachako.share.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 *  ファイルをZip形式に圧縮するなどの機能を提供する.
 *  
 * @author JSA saitou
 *
 */
public class ZipUtil
{

	/**
	 * ファイルを圧縮する.
	 * 
	 * @param targetFilePath 圧縮対象ファイル(フルパス指定) 
	 * @param zipFilePath Zipファイル(フルパス指定)
	 * 
	 * @throws IOException
	 */
	public static void compress(String targetFilePath, String zipFilePath) throws IOException
	{
	    // ファイル存在チェック
		File targetFile = new File(targetFilePath);
		if(!targetFile.isFile()) {
			throw new FileNotFoundException(targetFilePath);
		}

		String originalFileName	= targetFile.getName();

		ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(zipFilePath));
		BufferedInputStream input = new BufferedInputStream(new FileInputStream(targetFile));

		ZipEntry entry = new ZipEntry(originalFileName);
		zipOut.putNextEntry(entry);

		// データを圧縮して書き込む
		int b;
		while((b = input.read()) != -1) {
			zipOut.write(b);
		}

		// エントリと入力ストリームを閉じる
		zipOut.closeEntry();
		input.close();

		// 出力ストリームを閉じる
		zipOut.flush();
		zipOut.close();

		return;
	}

	/**
	 * ZIPファイルの全ファイルの内容を読み出す.（テキスト）
	 * 
	 * @param zipFilePath ZIPファイル(フルパス指定)
	 * 
	 * @return 全ファイルの内容一覧
	 */
	public static List<String> readAllFiles(String zipFilePath)
	{
		List<String> textList	= new ArrayList<String>();

		ZipFile zipFile	= null;
		InputStream is	= null;
		BufferedReader br	= null;
		try {
			zipFile	= new ZipFile(zipFilePath);
			Enumeration<? extends ZipEntry> entries = zipFile.entries();
			while (entries.hasMoreElements()) {
				ZipEntry zipEntry	= entries.nextElement();
				String entryName	= zipEntry.getName();
System.out.println(entryName);
				is	= zipFile.getInputStream(zipEntry);
				br	= new BufferedReader(new InputStreamReader(is, "utf8"));

				StringBuilder sb	= new StringBuilder();
				String line = null;
				while((line = br.readLine()) != null) {
					sb.append(line);
					sb.append("\n");
				}
				textList.add(sb.toString());
			}
			return textList;
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(zipFile != null) {
					zipFile.close();
				}
				if(is != null) {
					is.close();
				}
				if(br != null) {
					br.close();
				}
			} catch (Exception e) {
			      e.printStackTrace();
			}
		}
	      return null;
	}

	/**
	 * ZIPファイル内の該当ファイルの内容を読み出す.（テキスト）
	 * 
	 * @param zipFilePath ZIPファイル(フルパス指定)
	 * @param targetFilePath 読み出すファイル
	 * 
	 * @return ファイルの内容
	 */
	public static String readFile(String zipFilePath, String fileName)
	{
		ZipFile zipFile	= null;
		InputStream is	= null;
		BufferedReader br	= null;
		try {
			zipFile	= new ZipFile(zipFilePath);
			ZipEntry zipEntry	= zipFile.getEntry(fileName);
			is	= zipFile.getInputStream(zipEntry);
			br	= new BufferedReader(new InputStreamReader(is, "utf8"));

			StringBuilder sb	= new StringBuilder();
			int c;
			while((c = br.read()) != -1) {
				sb.append((char)c);
			}
			return sb.toString();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(zipFile != null) {
					zipFile.close();
				}
				if(is != null) {
					is.close();
				}
				if(br != null) {
					br.close();
				}
			} catch (Exception e) {
			      e.printStackTrace();
			}
		}
	      return null;
	}
}
