package org.kachako.components.collection_reader;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

import javax.xml.bind.JAXBException;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.examples.SourceDocumentInformation;

import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;

import org.kachako.types.qa.Metadata;
import org.kachako.types.qa.Question;

/**
 * @author Takayuki SUZUKI
 *
 */
public class QuestionCollectionReader extends AcliaCollectionReaderAbstract {
	protected Queue<org.kachako.components.aclia.generated.gold_standard.TOPIC> topicElementQueue = new LinkedList<org.kachako.components.aclia.generated.gold_standard.TOPIC>();
	protected org.kachako.components.aclia.generated.gold_standard.METADATA metadataElement = null;
	protected static final String GENERATED_CLASS_PACKAGE_NAME = "org.kachako.components.aclia.generated.gold_standard";
	protected static final String TARGET_LANGUAGE_QUESTION_VIEW_NAME = "TargetLanguageQuestionView"; //TODO decide view name
	protected static final String ENGLISH_QUESTION_VIEW_NAME = "EnglishQuestionView"; //TODO decide view name
	protected static final String PARAM_INPUT_XML_FILE_NAME = "InputXmlFile";

	@Override
	public void initialize() throws ResourceInitializationException {
		super.initialize();

		//get input file name
		String inputXmlFileName = (String) getConfigParameterValue(PARAM_INPUT_XML_FILE_NAME);

		try {
			//get TOPIC_SET
			org.kachako.components.aclia.generated.gold_standard.TOPICSET topicSetElement = (org.kachako.components.aclia.generated.gold_standard.TOPICSET) unmarshall(inputXmlFileName, GENERATED_CLASS_PACKAGE_NAME);

			//get TOPICs and add queue
			topicElementQueue.addAll(topicSetElement.getTOPIC());

			//get METADATA
			metadataElement = topicSetElement.getMETADATA();
		} catch (JAXBException e) {
			e.printStackTrace();
			throw new ResourceInitializationException(e);
		}
	}

	protected void addMetadata(JCas targetJCas){
		Metadata metadataFS = new Metadata(targetJCas);
		metadataFS.addToIndexes();
		metadataFS.setDescription(metadataElement.getDESCRIPTION());
		metadataFS.setVersion(metadataElement.getVERSION());
		metadataFS.setSourceLanguage(metadataElement.getLANGUAGE().getSOURCE());
		metadataFS.setTargetLanguage(metadataElement.getLANGUAGE().getTARGET());
		metadataFS.setCorpus(metadataElement.getCORPUS());
	}

	@Override
	public void getNext(CAS topicCAS) throws IOException, CollectionException {
		try {
			//get current TOPIC
			org.kachako.components.aclia.generated.gold_standard.TOPIC currentTopicElement = topicElementQueue.poll();

			//get JCas
			JCas topicJCas = topicCAS.getJCas();

			//create Question and Answer views
			JCas targetLanguageQuestionViewJCas = topicJCas.createView(TARGET_LANGUAGE_QUESTION_VIEW_NAME+".1"); //TODO decide sofa name
			JCas englishQuestionViewJCas = topicJCas.createView(ENGLISH_QUESTION_VIEW_NAME+".1"); //TODO decide sofa name

			//add Metadata
			JCas tjcas = topicCAS.getJCas();
			
			Iterator<JCas> jCasIterator = tjcas.getViewIterator(TARGET_LANGUAGE_QUESTION_VIEW_NAME);
			if(jCasIterator == null) {
				System.out.println("non found "+TARGET_LANGUAGE_QUESTION_VIEW_NAME);
				return;
			}
			if (jCasIterator.hasNext()) {
				JCas targetViewCas = jCasIterator.next();
				addMetadata(targetViewCas);
			}
			jCasIterator = tjcas.getViewIterator(ENGLISH_QUESTION_VIEW_NAME);
			if(jCasIterator == null) {
				System.out.println("non found "+ENGLISH_QUESTION_VIEW_NAME);
				return;
			}
			if (jCasIterator.hasNext()) {
				JCas targetViewCas = jCasIterator.next();
				addMetadata(targetViewCas);
			}
			
//			addMetadata(topicJCas.getView(ENGLISH_QUESTION_VIEW_NAME));

			//create and set Documents of Question views, and add Question FSs
			setUpQuestionViews(currentTopicElement, targetLanguageQuestionViewJCas, englishQuestionViewJCas);

		} catch (CASException e) {
			e.printStackTrace();
			throw new CollectionException(e);
		}
	}

	private void addQuestion(org.kachako.components.aclia.generated.gold_standard.TOPIC currentTopicElement, JCas questionViewJCas, StringBuilder questionSofaDocument){
		Question questionFS = new Question(questionViewJCas);
		questionFS.addToIndexes(questionViewJCas);
		
		//search a corresponding Question element to the given Question view
		org.kachako.components.aclia.generated.gold_standard.QUESTION questionElement = null;

System.out.println("questionViewJCas.getViewName()="+questionViewJCas.getViewName());
		for(org.kachako.components.aclia.generated.gold_standard.QUESTION currentQuestionElement : currentTopicElement.getQUESTION()) {
			if(questionViewJCas.getViewName().contains(ENGLISH_QUESTION_VIEW_NAME) && currentQuestionElement.getLANG().equals("EN")){
				questionElement = currentQuestionElement;
			}
			else if(questionViewJCas.getViewName().contains(TARGET_LANGUAGE_QUESTION_VIEW_NAME) && !currentQuestionElement.getLANG().equals("EN")){
				questionElement = currentQuestionElement; 
			}
		}

		//set span and add document text
		int beginningOfQuestionIndex = questionSofaDocument.length();
		questionSofaDocument.append(questionElement.getvalue()).append(LINE_SEPARATOR);
		int endOfQuestionIndex = questionSofaDocument.length() - 1;
		questionFS.setBegin(beginningOfQuestionIndex);
		questionFS.setEnd(endOfQuestionIndex);
		
		//add Narrative Feature
		org.kachako.components.aclia.generated.gold_standard.NARRATIVE narrativeElement = null;
		for(org.kachako.components.aclia.generated.gold_standard.NARRATIVE currentNarrativeElement : currentTopicElement.getNARRATIVE()) {
			if(questionViewJCas.getViewName().contains(ENGLISH_QUESTION_VIEW_NAME) && currentNarrativeElement.getLANG().equals("EN")){
				narrativeElement = currentNarrativeElement;
			}
			else if(questionViewJCas.getViewName().contains(TARGET_LANGUAGE_QUESTION_VIEW_NAME) && !currentNarrativeElement.getLANG().equals("EN")){
				narrativeElement = currentNarrativeElement; 
			}
		}
		questionFS.setNarrative(narrativeElement.getvalue());

		//add Title Feature
		if(questionViewJCas.getViewName().contains(TARGET_LANGUAGE_QUESTION_VIEW_NAME)){
			questionFS.setTitle(currentTopicElement.getTITLE());
		}
	}

	protected void setUpQuestionViews(org.kachako.components.aclia.generated.gold_standard.TOPIC currentTopicElement, JCas targetLanguageQuestionViewJCas, JCas englishQuestionViewJCas){
		StringBuilder targetLanguageQuestionSofaDocument = new StringBuilder();
		StringBuilder englishQuestionSofaDocument = new StringBuilder();

		//add Question FSs and create document of Question views
		addQuestion(currentTopicElement, targetLanguageQuestionViewJCas, targetLanguageQuestionSofaDocument);
		addQuestion(currentTopicElement, englishQuestionViewJCas, englishQuestionSofaDocument);

		//set document of Question views
		targetLanguageQuestionViewJCas.setDocumentText(targetLanguageQuestionSofaDocument.toString());
		englishQuestionViewJCas.setDocumentText(englishQuestionSofaDocument.toString());

		//set URI of Question views
		SourceDocumentInformation targetLanguageQuestionSofaSourceDocumentInformationFS = new SourceDocumentInformation(targetLanguageQuestionViewJCas);
		targetLanguageQuestionSofaSourceDocumentInformationFS.addToIndexes(targetLanguageQuestionViewJCas);
		targetLanguageQuestionSofaSourceDocumentInformationFS.setUri(currentTopicElement.getID());

		SourceDocumentInformation englishQuestionSofaSourceDocumentInformationFS = new SourceDocumentInformation(englishQuestionViewJCas);
		englishQuestionSofaSourceDocumentInformationFS.addToIndexes(englishQuestionViewJCas);
		englishQuestionSofaSourceDocumentInformationFS.setUri(currentTopicElement.getID());
	}

	@Override
	public boolean hasNext() throws IOException, CollectionException {
		return !topicElementQueue.isEmpty();
	}
}
