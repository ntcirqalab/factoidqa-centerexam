package org.kachako.components.collection_reader;

import java.io.IOException;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;

import org.kachako.types.qa.Answer;
import org.kachako.types.qa.AnswerType;
import org.kachako.types.qa.Nugget;


/**
 * @author Takayuki SUZUKI
 *
 */
public class GoldStandardCollectionReader extends QuestionCollectionReader {
	protected static final String ANSWER_VIEW_NAME = "AnswerView";

	@Override
	public void getNext(CAS topicCAS) throws IOException, CollectionException {
		try {
			//get current TOPIC
			org.kachako.components.aclia.generated.gold_standard.TOPIC currentTopicElement = topicElementQueue.poll();

			//get JCas
			JCas topicJCas = topicCAS.getJCas();

			//create Question and Answer views
			JCas targetLanguageQuestionViewJCas = topicJCas.createView(TARGET_LANGUAGE_QUESTION_VIEW_NAME); //TODO decide view name
			JCas englishQuestionViewJCas = topicJCas.createView(ENGLISH_QUESTION_VIEW_NAME); //TODO decide view name
			JCas answerViewJCas = topicJCas.createView(ANSWER_VIEW_NAME);

			//add Metadata
			addMetadata(topicJCas.getView(TARGET_LANGUAGE_QUESTION_VIEW_NAME));
			addMetadata(topicJCas.getView(ENGLISH_QUESTION_VIEW_NAME));
			addMetadata(topicJCas.getView(ANSWER_VIEW_NAME));

			//create and set Documents of Question views, and add Question FSs
			setUpQuestionViews(currentTopicElement, targetLanguageQuestionViewJCas, englishQuestionViewJCas);

			//create and set Document of Answer view, and add AnswerType FS, Answer FSs and Nugget FSs
			setUpAnswerView(currentTopicElement, answerViewJCas);
		} catch (CASException e) {
			e.printStackTrace();
			throw new CollectionException(e);
		}
	}

	private void setUpAnswerView(org.kachako.components.aclia.generated.gold_standard.TOPIC currentTopicElement, JCas answerViewJCas){
		StringBuilder answerSofaDocument = new StringBuilder();
		org.kachako.components.aclia.generated.gold_standard.ANSWER answerElement = currentTopicElement.getANSWER();

		//add AnswerType
		addAnswerType(currentTopicElement, answerViewJCas);

		//add Answers
		for(org.kachako.components.aclia.generated.gold_standard.TEXT textElement : answerElement.getTEXT()) {
			addAnswer(textElement, answerViewJCas, answerSofaDocument);
		}

		//add Nuggets
		for(org.kachako.components.aclia.generated.gold_standard.NUGGET nuggetElement : answerElement.getNUGGET()) {
			addNugget(nuggetElement, answerViewJCas);
		}

		//set document of Answer Sofa
		answerViewJCas.setDocumentText(answerSofaDocument.toString());
		
		//set URI of Answer Sofa
		SourceDocumentInformation answerSofaSourceDocumentInformationFS = new SourceDocumentInformation(answerViewJCas);
		answerSofaSourceDocumentInformationFS.addToIndexes();
		answerSofaSourceDocumentInformationFS.setUri(currentTopicElement.getID());
	}

	private void addAnswerType(org.kachako.components.aclia.generated.gold_standard.TOPIC currentTopicElement, JCas answerViewJCas){
		for(org.kachako.components.aclia.generated.gold_standard.ANSWERTYPE answerTypeElement : currentTopicElement.getANSWERTYPE()) {
			AnswerType answerTypeFS = new AnswerType(answerViewJCas);
			answerTypeFS.addToIndexes();
			answerTypeFS.setAnswerType(answerTypeElement.getvalue());
		}
	}

	private void addAnswer(org.kachako.components.aclia.generated.gold_standard.TEXT currentTextElement, JCas answerViewJCas, StringBuilder answerSofaDocument){
		//set document text
		int beginingOfAnswerText = answerSofaDocument.length();
		answerSofaDocument.append(currentTextElement.getvalue()).append(LINE_SEPARATOR);
		int endOfAnswerText = answerSofaDocument.length() - 1;

		//set span, id and doc number 
		Answer answerFS = new Answer(answerViewJCas);
		answerFS.addToIndexes();
		answerFS.setBegin(beginingOfAnswerText);
		answerFS.setEnd(endOfAnswerText);
		answerFS.setId(currentTextElement.getID());
		answerFS.setDocNo(currentTextElement.getDOCNO());
	}

	private void addNugget(org.kachako.components.aclia.generated.gold_standard.NUGGET currentNuggetElement, JCas answerViewJCas){
		Nugget nuggetFS = new Nugget(answerViewJCas);
		nuggetFS.addToIndexes();
		nuggetFS.setId(currentNuggetElement.getID());
		nuggetFS.setVital(Integer.parseInt(currentNuggetElement.getVITAL()));
		nuggetFS.setNonVital(Integer.parseInt(currentNuggetElement.getNONVITAL()));
		nuggetFS.setScore(Double.parseDouble(currentNuggetElement.getSCORE()));
		nuggetFS.setNuggetText(currentNuggetElement.getvalue());
	}
}
