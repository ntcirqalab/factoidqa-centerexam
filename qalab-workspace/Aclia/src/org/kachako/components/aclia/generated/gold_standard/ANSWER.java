//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2011.12.08 at 12:46:57 �ߌ� JST 
//


package org.kachako.components.aclia.generated.gold_standard;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "text",
    "nugget"
})
@XmlRootElement(name = "ANSWER")
public class ANSWER {

    @XmlElement(name = "TEXT")
    protected List<TEXT> text;
    @XmlElement(name = "NUGGET")
    protected List<NUGGET> nugget;

    /**
     * Gets the value of the text property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the text property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTEXT().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TEXT }
     * 
     * 
     */
    public List<TEXT> getTEXT() {
        if (text == null) {
            text = new ArrayList<TEXT>();
        }
        return this.text;
    }

    /**
     * Gets the value of the nugget property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nugget property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNUGGET().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NUGGET }
     * 
     * 
     */
    public List<NUGGET> getNUGGET() {
        if (nugget == null) {
            nugget = new ArrayList<NUGGET>();
        }
        return this.nugget;
    }

}
