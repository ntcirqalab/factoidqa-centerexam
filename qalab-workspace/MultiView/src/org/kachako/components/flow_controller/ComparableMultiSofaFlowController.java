package org.kachako.components.flow_controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.analysis_engine.TypeOrFeature;
import org.apache.uima.analysis_engine.metadata.AnalysisEngineMetaData;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.flow.FinalStep;
import org.apache.uima.flow.Flow;
import org.apache.uima.flow.FlowControllerContext;
import org.apache.uima.flow.JCasFlowController_ImplBase;
import org.apache.uima.flow.JCasFlow_ImplBase;
import org.apache.uima.flow.SimpleStep;
import org.apache.uima.flow.Step;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.StringArray;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.metadata.Capability;

//import org.u_compare.shared.ComponentID;
//import org.u_compare.shared.comparable.AnnotationGroup;
//import org.u_compare.shared.comparable.InputCapability;

import org.kachako.types.ComponentID;
import org.kachako.types.qa.InputMultiSofaID;

public class ComparableMultiSofaFlowController extends
		JCasFlowController_ImplBase {

	// private ArrayList<AbstractComponent> testComponents = new
	// ArrayList<AbstractComponent>();

	public static final String PARAM_SOFANAME = "SofaName";

	private String sofaName;

	@Override
	public void initialize(FlowControllerContext aContext)
			throws ResourceInitializationException {
		// TODO Auto-generated method stub
		super.initialize(aContext);

		// fetch parameters.
		sofaName = (String) aContext.getConfigParameterValue(PARAM_SOFANAME);

	}

	@Override
	public Flow computeFlow(JCas arg0) throws AnalysisEngineProcessException {
		// TODO Auto-generated method stub
		ComparableAggregateFlow flow = new ComparableAggregateFlow();
		flow.setJCas(arg0);

		return flow;
	}

	// ------------------------------------------------------------------------------------------
	// Class ComparableAggregateFlow
	// ---------------------------------------------

	public class ComparableAggregateFlow extends JCasFlow_ImplBase {

		private static final String adapterAEKey = "ComparableMultiSofaAnalysisEngineAdapter";
		private boolean endOfIteration = true;

		private String nextAeKey;
		private String nextAdapterKey;
		private ArrayList<String> sofaNames;

//		private ArrayList<String> aeKeys;
		//private int aeIndex = 0;

		@Override
		protected Flow newCasProduced(JCas newCas, String producedBy)
				throws AnalysisEngineProcessException {

			String aeKey = new String(nextAeKey);
			String adapterKey = new String(nextAdapterKey);
			ComponentID componentID = new ComponentID(newCas);
			componentID.setComponentID(aeKey);
			componentID.addToIndexes();

			// endOfIteration = true;

			return new MultipliedCasFlow(aeKey, adapterKey);
		}

		@Override
		public Step next() {
			// TODO Auto-generated method stub

			try {
				JCas cas = getJCas();
				FlowControllerContext context = getContext();

				
				// start new loop
				// Iterator<AbstractComponent> iterator =
				// testComponents.iterator();

				Map analysisEngineMetaDataMap = context
						.getAnalysisEngineMetaDataMap();

				// init
				if (nextAeKey == null) {
//					aeKeys = new ArrayList<String>();
					Iterator aeIter2 = analysisEngineMetaDataMap.entrySet()
							.iterator();
					while (aeIter2.hasNext()) {
						Map.Entry entry = (Map.Entry) aeIter2.next();
						String aeKey = new String(entry.getKey().toString());
//						AnalysisEngineMetaData analysisEngineMetaData = context.getAnalysisEngineMetaDataMap().get(aeKey);
	//					analysisEngineMetaData.get
						if (aeKey.startsWith(adapterAEKey))
							nextAdapterKey = aeKey;
						else {
							if (nextAeKey != null) {
								System.err.println("more than one ae exist!!");
								break;
							}
							nextAeKey = aeKey;
						}
					}
				}
				
				if (sofaNames == null) {
					sofaNames = new ArrayList<String>();
					Iterator<JCas> sofaIterator = cas.getViewIterator(sofaName);
					while (sofaIterator.hasNext()) {
						String aeKey = sofaIterator.next().getViewName();						
						sofaNames.add(aeKey);
					}
				}

				if(sofaNames.isEmpty()){
					return new FinalStep();
				}

//				if (sofaIterator.hasNext()) {

//					nextAeKey = aeKeys.get(0);
	//				nextAdapterKey = adapterAEKey;
					//aeIndex++;
					
					String sofaID = sofaNames.remove(sofaNames.size()-1);
System.err.println("processing sofa: " + sofaID);
					
					FSIterator inputMultiSofaIDs = cas.getFSIndexRepository()
							.getAllIndexedFS(
									cas.getTypeSystem().getType(
											InputMultiSofaID.class.getName()));
					while (inputMultiSofaIDs.hasNext()) {
						InputMultiSofaID inputSofaID = (InputMultiSofaID) inputMultiSofaIDs
								.next();
						inputSofaID.removeFromIndexes();
					}

					InputMultiSofaID inputMultiSofaIDsToCas = new InputMultiSofaID(
							cas);
					inputMultiSofaIDsToCas.setSofaID(sofaID);
					inputMultiSofaIDsToCas.addToIndexes();
System.err.println("next ae: " + nextAdapterKey);

					return new SimpleStep(nextAdapterKey);

//				} else {
//					System.err.println("<CombinationMultiSofaFlows FINISHED>");
//
//					return new FinalStep();
//				}

			} catch (CASException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 

			 return null;
		}
	}

	// Class ComparableAggregateFlow
	// END---------------------------------------------
	// ------------------------------------------------------------------------------------------

	// ------------------------------------------------------------------------------------------
	// Class MultipliedCasFlow ---------------------------------------------
	private static class MultipliedCasFlow extends JCasFlow_ImplBase {
		private String aeKey, adapterAeKey;
		private boolean aeAlreadyProcessed = false, finished = false;;

		public MultipliedCasFlow(String aeKey, String adapterAeKey) {
			super();
			this.aeKey = aeKey;
			this.adapterAeKey = adapterAeKey;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.apache.uima.flow.Flow#next()
		 */
		public Step next() throws AnalysisEngineProcessException {
			if (aeAlreadyProcessed) {

				if (finished)
					return new FinalStep() {
					};

				finished = true;
				
				System.err.println("next ae adapterAeKey = " + adapterAeKey+ " "
						+ Thread.currentThread().getStackTrace()[1]);
				
				return new SimpleStep(adapterAeKey);
			}

			aeAlreadyProcessed = true;
			System.err.println("next aeKey = " + aeKey+ " "
					+ Thread.currentThread().getStackTrace()[1]);
			
			return new SimpleStep(aeKey);
		}
	}
	// Class ComparableAggregateFlow
	// END---------------------------------------------
	// ------------------------------------------------------------------------------------------
}
