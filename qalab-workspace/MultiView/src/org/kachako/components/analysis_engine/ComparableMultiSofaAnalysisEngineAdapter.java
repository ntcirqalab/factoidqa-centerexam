package org.kachako.components.analysis_engine;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.uima.analysis_component.JCasMultiplier_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.AbstractCas;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.CommonArrayFS;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.SofaID;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.NonEmptyFSList;
import org.apache.uima.jcas.cas.Sofa;
import org.apache.uima.jcas.cas.TOP;
import org.apache.uima.jcas.tcas.DocumentAnnotation;
import org.apache.uima.util.CasCopier;
//import org.u_compare.comparable.utils.FSListIterator;
//import org.u_compare.shared.AnnotationMetadata;
//import org.u_compare.shared.ExternalReference;
//import org.u_compare.shared.comparable.AnnotationGroup;
//import org.u_compare.shared.comparable.Iteration;
//import org.u_compare.shared.label.UniqueLabel;
//import org.u_compare.shared.semantic.LinkedAnnotationSet;

import org.kachako.comparable.utils.FSListIterator;
import org.kachako.types.AnnotationMetadata;
import org.kachako.types.ExternalReference;
import org.kachako.types.qa.InputMultiSofaID;

public class ComparableMultiSofaAnalysisEngineAdapter extends JCasMultiplier_ImplBase{

	private JCas originalCas;
	private String sofaID;
	
	private boolean isPhaseOneFinished = false;
	private HashMap<TOP, TOP> copiedToOriginalMap;
	
	@Override
	public boolean hasNext() throws AnalysisEngineProcessException {
		// TODO Auto-generated method stub
		
		if(!isPhaseOneFinished){
			isPhaseOneFinished = true;
			return true;
		}else{
//			isPhaseOneFinished = false;
			return false;
		}
		
	}

	@Override
	public AbstractCas next() throws AnalysisEngineProcessException {
		JCas emptyJCas = getEmptyJCas();

		try{
			//setSOFA
			JCas view = originalCas.getView(sofaID);
			
			emptyJCas.setDocumentText(view.getDocumentText());
	    	emptyJCas.setDocumentLanguage(view.getDocumentLanguage());
	    	
	    	//copy
	    	
	    	//Setting documentAnnotation
	    	DocumentAnnotation documentAnnotation = new DocumentAnnotation(emptyJCas);
	    	documentAnnotation.setBegin(0);
	    	documentAnnotation.setEnd(emptyJCas.getDocumentText().length());
	    	documentAnnotation.addToIndexes();
	    
//	    	if(d.D.d) System.out.println(Arrays.deepToString(Thread.currentThread().getStackTrace()));
	    
	    	//new codes
	    	HashMap<TOP, TOP> originalToCopiedMap = new HashMap<TOP, TOP>();
	    	copyAnnotationGroupToEmptyCas(emptyJCas, originalToCopiedMap, originalCas.getTypeSystem(), view);
	  
	    	/// one pass
    		copiedToOriginalMap = makeReversedMap(originalToCopiedMap);

    		// two pass: fill fields and resolve references
    		setFeatures(originalToCopiedMap);
//    		setFeatures(originalToCopiedMap, originalToCopiedMap);

//    copiedToOriginalMap.clear();
	// copiedToOriginalMap = null;   	
		}catch(Exception e){
			emptyJCas.release();
			e.printStackTrace();
		}
		
		
		return emptyJCas;
	}

	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		// TODO Auto-generated method stub
		//fetch id from cas;
		TypeSystem typeSystem = aJCas.getTypeSystem();
		
		FSIterator inputSofaIt = aJCas.getFSIndexRepository().getAllIndexedFS(
		        typeSystem.getType(InputMultiSofaID.class.getName()));
		InputMultiSofaID inputSofaID = null;
		if (inputSofaIt.hasNext()) inputSofaID = (InputMultiSofaID) inputSofaIt.next();
		
		
		if(inputSofaID != null){
			//clone sofa from multisofa and create a cas
			this.originalCas = aJCas;
			
			sofaID = inputSofaID.getSofaID();

			isPhaseOneFinished = false;		

			//remove inputSofaIt;
			inputSofaID.removeFromIndexes();
			
			
		}else{
			//use current cas to create a sofa and copy back to originalCas
			
//			HashMap<TOP, TOP> copiedToOriginalMap = null;
			if ( copiedToOriginalMap == null ) copiedToOriginalMap = new HashMap<TOP, TOP>();
	    	//if (debugShowInstance) if(d.D.d) System.out.println("copiedToOriginalMap = "+copiedToOriginalMap+" "+Thread.currentThread().getStackTrace()[1]);

			//HashMap<TOP, TOP> newCopiedToOriginalMap = new HashMap<TOP, TOP>();
	      
	    	// one pass
	    	//TODO change here.
	    	try {
//				NonEmptyFSList nonEmptyFSList = copyTopFromTemporaryCAS(aJCas, originalCas, newCopiedToOriginalMap,sofaID);
				NonEmptyFSList nonEmptyFSList = copyTopFromTemporaryCAS(aJCas, originalCas, copiedToOriginalMap,sofaID);
			} catch (CASException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
	    	// two pass: fill fields and resolve references
	    	setFeatures(copiedToOriginalMap);
//	    	System.out.println("copiedToOriginalMap = " + copiedToOriginalMap+ " "
//	    			+ Thread.currentThread().getStackTrace()[1]);
//	    	copiedToOriginalMap.putAll(newCopiedToOriginalMap);
//	    	setFeatures(copiedToOriginalMap, newCopiedToOriginalMap);
	   copiedToOriginalMap.clear();
	   copiedToOriginalMap = null;   	
	   originalCas = null;

		}
		
		
		
	}

	
	//--------------------------------------------------------------------------------//
	//----METHODS-----------//
	
	private static NonEmptyFSList copyTopFromTemporaryCAS(JCas fromCAS, JCas toCAS, 
		      HashMap<TOP, TOP> copiedToOriginalMap, String sofaStr) throws CASException {
		
		ArrayList annotations = new ArrayList();
		JCas sofa = toCAS.getView(sofaStr);
		
		TypeSystem typeSystem = fromCAS.getTypeSystem();
		NonEmptyFSList firstList = null, nonEmptyFSList = null;	
		    
		FSIterator indexedFS = fromCAS.getFSIndexRepository().getAllIndexedFS(typeSystem.getTopType());

		while (indexedFS.hasNext()) {
			TOP top = (TOP) indexedFS.next();
			if ( copiedToOriginalMap.containsKey(top) ) {
				continue; 
			}
			registerReferredAnnotationsRecursively(annotations, top, typeSystem, copiedToOriginalMap, false);
		      
			//if ( null == satisfies(outputTypes, top.getType(), typeSystem)) continue;
		    
			TOP copiedTop = putFS(sofa, top, copiedToOriginalMap);
			
//			nonEmptyFSList = FSListIterator.setHeadAndGetNextTail(sofa, nonEmptyFSList, copiedTop, ! annotations.isEmpty() || indexedFS.hasNext());
//			if ( firstList == null ) firstList = nonEmptyFSList;
		}
	
		annotations.removeAll(copiedToOriginalMap.keySet());

		for (int i = 0; i < annotations.size(); i++) {
			TOP top = (TOP) annotations.get(i);
			TOP copiedTop = putFS(sofa, top, copiedToOriginalMap);
			nonEmptyFSList = FSListIterator.setHeadAndGetNextTail(sofa, nonEmptyFSList, copiedTop, i + 1 < annotations.size());
			if ( firstList == null ) firstList = nonEmptyFSList;
		}
		return firstList;
	}
	
	public static void registerReferredAnnotationsRecursively(ArrayList annotations, TOP top, TypeSystem typeSystem,
		      HashMap<TOP, TOP> originalToCopiedMap, boolean metaAnnotationsOnly) {
		if ( top == null ) return;
		if ( annotations.contains(top) ) return;
		if ( ((! metaAnnotationsOnly) /*|| (top instanceof UniqueLabel)*/)
				|| isMetadataAnnotation(top) ) {
			if (top != null) annotations.add(top);
		}

		List features = top.getType().getFeatures();
		for (int i = 0; i < features.size(); i++) {
			Feature feature = (Feature) features.get(i);
			Type range = feature.getRange();
			if ( ! range.isPrimitive() ) {
				
				FeatureStructure featureValue = top.getFeatureValue(feature);
				if ( ! metaAnnotationsOnly || isMetadataAnnotation(featureValue) ) {
					if ( ! originalToCopiedMap.containsKey(featureValue) )
						registerReferredAnnotationsRecursively(annotations, (TOP) featureValue, typeSystem, originalToCopiedMap, metaAnnotationsOnly);
				}
			}
		}
		    
		if (top instanceof FSArray) {
			FSArray fsArray = (FSArray)top;
		    	for (int i = 0; i < fsArray.size(); i++) {
		    		FeatureStructure featureValue = fsArray.get(i);
		    		if ( ! metaAnnotationsOnly || isMetadataAnnotation(featureValue) ) {
		    			if ( ! originalToCopiedMap.containsKey(featureValue) )
		    				registerReferredAnnotationsRecursively(annotations, (TOP) featureValue, typeSystem, originalToCopiedMap, metaAnnotationsOnly);
		    		}        
		    	}
		}
	}
	
	
	private static boolean isMetadataAnnotation(FeatureStructure annotation) {
	    return ( annotation instanceof FSArray 
//	        || annotation instanceof LinkedAnnotationSet 
	        || annotation instanceof AnnotationMetadata
	        || annotation instanceof ExternalReference
	        );
	}
	
	private static TOP putFS(JCas sofa, TOP original, Map<TOP,TOP> originalToCopiedMap) {
		TOP object = null;
		try {
			if ( originalToCopiedMap.containsKey(original) ) {
//					if(d.D.d) 
						System.out.println("skipping, already contains = " + original+ " "
							+ Thread.currentThread().getStackTrace()[1]);
				return originalToCopiedMap.get(original);        
			}
	      
			if ( original == null ) return null;
			Class originalClass = original.getClass();
			if ( originalClass.equals(org.apache.uima.jcas.cas.Sofa.class) ) return null;

			if ( original.getType().isArray() ) {
				Method getSizeMethod = originalClass.getDeclaredMethod("size", null);
				Method getMethod = originalClass.getDeclaredMethod("get", int.class);
				
				int size = (Integer) getSizeMethod.invoke(original, null);
				object = (TOP) originalClass.getConstructor(JCas.class, int.class).newInstance(sofa, size);        
				object.addToIndexes(sofa);
				TOP oldTop = originalToCopiedMap.put(original, object);
			} else {

				object = (TOP) originalClass.getConstructor(JCas.class).newInstance(sofa);
				object.addToIndexes(sofa);
				TOP oldTop = originalToCopiedMap.put(original, object);
			}
		} catch (IllegalArgumentException e) {
	      // TODO 自動生成された catch ブロック
	      e.printStackTrace();
	    } catch (SecurityException e) {
	      // TODO 自動生成された catch ブロック
	      e.printStackTrace();
	    } catch (InstantiationException e) {
	      // TODO 自動生成された catch ブロック
	      e.printStackTrace();
	    } catch (IllegalAccessException e) {
	      // TODO 自動生成された catch ブロック
	      e.printStackTrace();
	    } catch (InvocationTargetException e) {
	      // TODO 自動生成された catch ブロック
	      e.printStackTrace();
	    } catch (NoSuchMethodException e) {
	      // TODO 自動生成された catch ブロック
	      e.printStackTrace();
	    }
	    return object;
	}
	/**
	 * copy key feature values to value
	 * @param originalToCopiedMap
	 */
	private static void setFeatures(HashMap<TOP, TOP> originalToCopiedMap) {
//			private static void setFeatures(HashMap<TOP, TOP> originalToCopiedMap,
//		      HashMap<TOP, TOP> newOriginalToCopiedMap) {

//		Iterator<TOP> newOriginalsIt = newOriginalToCopiedMap.keySet().iterator();
		Iterator<TOP> newOriginalsIt = originalToCopiedMap.keySet().iterator();
		while (newOriginalsIt.hasNext()) {
			TOP original = newOriginalsIt.next();
//			TOP copied = newOriginalToCopiedMap.get(original);
			TOP copied = originalToCopiedMap.get(original);
			List features = original.getType().getFeatures();
			if ( original.getType().isArray() ) {
				CommonArrayFS commonArrayFS = (CommonArrayFS) original;
				if ( commonArrayFS instanceof FSArray ) {
					FSArray originalArray = (FSArray) commonArrayFS;
					FSArray copiedArray = (FSArray) copied;
		          
					for (int i = 0; i < originalArray.size(); i++) {
						copiedArray.set(i, originalToCopiedMap.get(originalArray.get(i)));
					}
				} else {
					String[] array = commonArrayFS.toStringArray();
					((CommonArrayFS)copied).copyFromArray(array, 0, 0, array.length);
				}
			}
		      
			for (int i = 0; i < features.size(); i++) {
				Feature feature = (Feature) features.get(i);
				Type range = feature.getRange();
				if (range.getName().equals("uima.cas.Sofa")) {
				} else if ( range.isPrimitive() ) {
					copied.setFeatureValueFromString(feature, original.getFeatureValueAsString(feature));					
		       } else {
		        	FeatureStructure featureValue = original.getFeatureValue(feature);
		        	TOP ref = originalToCopiedMap.get(featureValue);
		        	copied.setFeatureValue(feature, ref);
		        }
		          
			}
			
		}
	}
	
	
	private void copyAnnotationGroupToEmptyCas(JCas emptyJCas, HashMap<TOP, TOP> originalToCopiedMap, TypeSystem typeSystem, JCas tmpJCas) {
		ArrayList annotations = new ArrayList();
	    FSIterator topFS = tmpJCas.getFSIndexRepository().getAllIndexedFS(typeSystem.getTopType());
	    //FSListIterator<TOP> annotationsIt = new FSListIterator<TOP>(tmpJCas.getAnnotations());
	    while (topFS.hasNext()) {
	    	TOP top = (TOP)topFS.next();
	    	if (top == null) continue;
	    	
	    	registerReferredAnnotationsRecursively(annotations, top, typeSystem, originalToCopiedMap, false);
	    	TOP copiedTop = putFS(emptyJCas, top, originalToCopiedMap);
	    }
    
	    annotations.removeAll(originalToCopiedMap.keySet());

	    for (int i = 0; i < annotations.size(); i++) {
	    	TOP top = (TOP) annotations.get(i);
	    	TOP copiedTop = putFS(emptyJCas, top, originalToCopiedMap);
	    }
	}
	
	private static HashMap<TOP,TOP> makeReversedMap( HashMap<TOP,TOP> map ) {
		HashMap<TOP, TOP> reversedMap = new HashMap<TOP,TOP>();
	    Iterator<TOP> iterator = map.keySet().iterator();
	    while (iterator.hasNext()) {
	    	TOP top = iterator.next();
	    	TOP value = map.get(top);
	      
	    	reversedMap.put(value, top);
	    }
	    
	    return reversedMap;
	}
}
