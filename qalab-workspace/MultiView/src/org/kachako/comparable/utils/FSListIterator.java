/**
 * 
 */
package org.kachako.comparable.utils;

import java.util.Iterator;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.EmptyFSList;
import org.apache.uima.jcas.cas.FSList;
import org.apache.uima.jcas.cas.NonEmptyFSList;
import org.apache.uima.jcas.cas.TOP;

public class FSListIterator<O> implements Iterator<O> {
  private FSList currentTail;
  public FSListIterator(FSList fslist) {
    this.currentTail = fslist;
  }
  /* (non-Javadoc)
   * @see java.util.Iterator#hasNext()
   */
  public boolean hasNext() {
    if ( currentTail == null ) return false;
    return !(currentTail instanceof EmptyFSList);
  }
  
  /* (non-Javadoc)
   * @see java.util.Iterator#next()
   */
  public O next() {
    NonEmptyFSList nonEmptyFSList = (NonEmptyFSList) currentTail;
    currentTail = nonEmptyFSList.getTail();
    return (O) nonEmptyFSList.getHead();
  }
  /* (non-Javadoc)
   * @see java.util.Iterator#remove()
   */
  public void remove() {
    // 自動生成
    
  }

  public static NonEmptyFSList setHeadAndGetNextTail( JCas originalCas,
        NonEmptyFSList nonEmptyFSList, TOP copiedTop, boolean hasNext) {
  
    // set head
    NonEmptyFSList list = new NonEmptyFSList(originalCas);
    list.setHead(copiedTop);

    if ( nonEmptyFSList != null ) nonEmptyFSList.setTail(list);

    // set emptyHead if end-of-list specified
    if ( ! hasNext ) {
      EmptyFSList emptyFSList = new EmptyFSList(originalCas);

      list.setTail(emptyFSList);
//      return null;
    }
      

      //    nonEmptyFSList = list;
    return list;
  }
  
  public static int countListLength( FSList list ) {
    int count = 0;
    FSListIterator<Object> listIterator = new FSListIterator<Object>(list);
    while ( listIterator.hasNext() ) {
      count++;
      listIterator.next();
    }
    return count;
  }
  
  /*
  public static NonEmptyFSList setHeadAndGetNextTail( JCas originalCas,
      NonEmptyFSList nonEmptyFSList, TOP copiedTop, boolean hasNext) {

    if ( ! hasNext ) {
      EmptyFSList emptyFSList = new EmptyFSList(originalCas);
//      emptyFSList.addToIndexes();
      if ( nonEmptyFSList != null )
        nonEmptyFSList.setTail(emptyFSList);
      return null;
    }
    
//    nonEmptyFSList.setHead(copiedTop);
    NonEmptyFSList list = new NonEmptyFSList(originalCas);
    list.setHead(copiedTop);

    if ( nonEmptyFSList != null ) nonEmptyFSList.setTail(list);
//    nonEmptyFSList = list;
    return list;
  }
  */
}