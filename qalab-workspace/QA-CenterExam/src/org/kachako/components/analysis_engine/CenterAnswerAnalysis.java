package org.kachako.components.analysis_engine;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.TOP;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.kachako.components.analysis_engine.module.CeAnswer;
import org.kachako.components.analysis_engine.module.CasData2CenterAnalysisQuestionData;
import org.kachako.components.analysis_engine.module.CeQuestion;
import org.kachako.components.analysis_engine.module.GetAnswer;
import org.kachako.components.analysis_engine.module.MakeQuery;
import org.kachako.components.analysis_engine.module.TextUtil;
import org.kachako.share.regex.RegexCache;
import org.kachako.types.centerexam.AnsColumn;
import org.kachako.types.centerexam.Data;
import org.kachako.types.centerexam.Question;
import org.kachako.types.centerexam.Ref;
import org.kachako.types.centerexam.TagList;
import org.kachako.types.centerexam.answertable.Answer;
import org.kachako.types.centerexam.answertable.AnswerColumn;
import org.kachako.types.centerexam.answertable.AnswerColumnID;
import org.kachako.types.centerexam.answertable.AnswerMachineType;
import org.kachako.types.centerexam.answertable.AnswerTable;
import org.kachako.types.centerexam.answertable.AnswerTableAnnotationList;
import org.kachako.types.centerexam.answertable.AnswerTableEndList;
import org.kachako.types.centerexam.answertable.AnswerType;
import org.kachako.types.centerexam.answertable.DataAT;
import org.kachako.types.centerexam.answertable.Knowledge;
import org.kachako.types.centerexam.answertable.ProcessLog;
import org.kachako.types.centerexam.answertable.QuestionAT;
import org.kachako.types.centerexam.answertable.QuestionID;
import org.kachako.types.centerexam.answertable.Score;
import org.kachako.types.centerexam.answertable.Section;
import org.kachako.types.qa.AnswerCandidate;


/**
 * ExamReaderの出力ファイルから読み込む情報
 * ($id_q:"xmlファイル名-空欄ID")
 * $exam{$id_q}->{answertype}->{0}: answer_typeタグの中身
 * $exam{$id_q}->{q_line}->{org}: 空欄に対応するinstructionタグの中身
 * $exam{$id_q}->{q_line}->{0}: instructionタグの中身で、参照がある場合は参照IDで置き換えたもの
 * $exam{$id_q}->{knowledge}->{0}:　knowledge_typeタグの中身
 * $exam{$id_q}->{answernum}->{0}: 空欄番号(空欄IDからAを取り除いたもの)
 * $exam{$id_q}->{choice}->{1～n}: 選択肢の情報 
 * $exam{$id_q}->{ref_utext}->{$ref_ID}: instructionやchoiceの中に出てきた参照IDの参照先(空欄の参照の場合は、空欄が含まれている文章すべて)
 * $exam{$id_q}->{questiontype}->{0}: 問のタイプ
 * $exam{$id_q}->{QA_query}->{$question_ID}: QAシステムへの入力用に作った質問文
 * $exam{$id_q}->{sup_ans}->{$question_ID}: 対応する質問文の想定解(特定のquestiontypeのみ)
 *
 * factoidQAの結果ファイルから読み込む情報
 * (このプログラムでは、結果ファイルdoc*をxmlファイルとして読み込んで解析している)
 * ($target_q_id:"xmlファイル名-空欄ID")
 * ($target_num:"空欄IDごとの質問文ID")
 * $qa_ans{$target_q_id}->{$target_num}->{$score_sofa_id}->{text}: 解候補の文字列
 * $qa_ans{$target_q_id}->{$target_num}->{$score_sofa_id}->{rank}: 解候補の順位
 * $qa_ans{$target_q_id}->{$target_num}->{$score_sofa_id}->{score}: 解候補のスコア(未使用)
 *
 * @author ynishi
 *
 */
public class CenterAnswerAnalysis extends JCasAnnotator_ImplBase {
	
	protected static final String TARGET_LANGUAGE_QUESTION_VIEW_NAME = "QuestionViewName";
	protected static final String DBDIRECTORY = "dbDirectory";
	protected static final String CABOCHAPATH = "cabochaPath";
	// FindAnswer実行結果 VIEW名 PREFIX
	private static final String AG_RESULT_VIEW_NAME_PREFIX	= "AG";

	
	private String cabochaPath;
	private String dbDirectory;
	private MakeQuery makeQuery;
	private GetAnswer getAnswer;
	
	private final RegexCache regexCache = new RegexCache();
	
	final static String PARAM_DEFANS = "DefaultAnswer";
	static String DEFAULT_ANSWER;
	
	@Override
	public void initialize(UimaContext aContext) throws ResourceInitializationException {
		System.out.println("--------AnswerAnalysis initialize---------");

		super.initialize(aContext);
		
		cabochaPath	= (String)aContext.getConfigParameterValue(CABOCHAPATH);
		dbDirectory	= (String)aContext.getConfigParameterValue(DBDIRECTORY);
		//targetQuestionViewName = (String)aContext.getConfigParameterValue(TARGET_LANGUAGE_QUESTION_VIEW_NAME);

		makeQuery = new MakeQuery(dbDirectory, cabochaPath);
		getAnswer = new GetAnswer(dbDirectory);

//		DEFAULT_ANSWER = (String)aContext.getConfigParameterValue(PARAM_DEFANS);
//		if(DEFAULT_ANSWER == null || DEFAULT_ANSWER.trim().length() == 0)
//			DEFAULT_ANSWER = "1";
//		DEFAULT_ANSWER = DEFAULT_ANSWER.trim();
//		answerMachine = new AnswerMachine(DEFAULT_ANSWER);
	}
	
	
	private JCas findInputView(JCas aJCas, String viewName) {

		JCas targetCas = null;
		Iterator<JCas> vIter = null;
		try {
			vIter = aJCas.getViewIterator();
		} catch (CASException e1) {
			e1.printStackTrace();
		}
		while (vIter.hasNext()) { 
			JCas jcas = vIter.next();
			String name = jcas.getViewName();
			if (name.equals(viewName)) {
				targetCas = jcas;
				break;
			}
		}
		if (targetCas == null) {
			try {
				targetCas = aJCas.getView("_InitialView");
			} catch (CASException e) {
				e.printStackTrace();
			}
		}
		return targetCas;
	}
	
	
	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		System.out.println("--------AnswerAnalysis start---------");

		// SourceDocumentInformation uriの取得　------------
		JCas inView = findInputView(jCas, "Text");
		if (inView == null) 
			System.err.println("non exist Text View?");

		String fileName = "";
		TypeSystem typeSystem = inView.getTypeSystem();
		Type type = typeSystem.getType(SourceDocumentInformation.class.getName());
		FSIterator<FeatureStructure> fsIterator = inView.getFSIndexRepository().getAllIndexedFS(type);
		if(fsIterator.hasNext()){
			SourceDocumentInformation srcDocInfo = (SourceDocumentInformation) fsIterator.next();
			try {
				String name = new File(new URI(srcDocInfo.getUri()).getPath()).getName();
				fileName = name.substring(0, name.indexOf(".xml"));
			} catch (URISyntaxException e) {
			}
		}

		
		Iterator<TOP> tagListFSIterator = inView.getJFSIndexRepository().getAllIndexedFS(TagList.type);
		Map<String, CeQuestion>exams = null;
        StringBuilder selected = new StringBuilder();

		if (tagListFSIterator.hasNext()) {
			TagList tagListFS	= (TagList)tagListFSIterator.next();
			FSArray tagListFsArray = tagListFS.getAnnotationList();

			Map<Question,LinkedHashSet<Data>> maximalQsMap	= new LinkedHashMap<Question,LinkedHashSet<Data>>();
			Map<Question,Data> miniQsDataMap				= new LinkedHashMap<Question,Data>();
			Map<Question, List<Question>> miniQuestionMap	= new LinkedHashMap<Question, List<Question>>();
			Map<Question, List<AnsColumn>> miniAnsColumnMap	= new LinkedHashMap<Question, List<AnsColumn>>();
			Map<Question,List<Annotation>> minimalQsAnnotationMap	= new LinkedHashMap<Question,List<Annotation>>();
			Map<Question,AnsColumn> minimalQsAnsColumnMap	= new LinkedHashMap<Question,AnsColumn>();
			
			List<Ref> refList = new ArrayList<Ref>();
			List<Annotation> blankList = new ArrayList<Annotation>();
			List<Annotation> labelList = new ArrayList<Annotation>();
			
			boolean yozemiFlag = false;

			CasData2CenterAnalysisQuestionData paseCas = new CasData2CenterAnalysisQuestionData();
			paseCas.parseQuestionData(tagListFsArray, maximalQsMap,
										miniQsDataMap, miniQuestionMap, miniAnsColumnMap,minimalQsAnnotationMap, 
										minimalQsAnsColumnMap, 
										refList, blankList, labelList,
										yozemiFlag, selected);

			if (maximalQsMap != null) {
				exams = paseCas.makeCeQuestionData(miniQuestionMap, minimalQsAnnotationMap, miniQsDataMap, minimalQsAnsColumnMap,
												labelList, blankList, refList);
			}
		}
		
		// ここでFindAnswerの結果を読んでqa_anssに入れる
		Map<String, Map<String, Map<Integer, CeAnswer>>>qa_anss = getQAAns(jCas);
		
		List<Annotation> beginList = new ArrayList<Annotation>();
		List<Annotation> endList = new ArrayList<Annotation>();
		AnswerTable answerTable = new AnswerTable(inView);
		answerTable.setBegin(0);
		answerTable.setFile(fileName);
		answerTable.setSelected(selected.toString().trim());
		answerTable.addToIndexes();
		beginList.add(answerTable);
		
		StringBuilder sb = new StringBuilder();
		
		for (String id_q : exams.keySet()) {
			//String target_anum = "";
			CeQuestion q = exams.get(id_q);
			if (q.getQuestionType().equals("TF") ||
					q.getQuestionType().equals("TF-comb") ||	
					q.getQuestionType().equals("T-comb")) {	
				Map<String, String> supans = makeQuery.makeSupAns(q);
System.out.println("makeSupAns:"+supans.size());				
				q.setSupAns(supans);
			}
System.out.println("Question:"+id_q+" Type["+q.getQuestionType()+"]");

			if (id_q == null || qa_anss == null)
				return;
			
			int choice = 0;
			Map<String, Map<Integer, CeAnswer>> targetMap = qa_anss.get(id_q);
			if (targetMap == null) {
				System.out.println("qa_anss("+id_q+") is null");
			} else {
				String qtype = q.getQuestionType();
				// String answenum = q.getAnswernum();
				if (qtype.equals("factoid") || qtype.equals("blank") || qtype.equals("factoid-comb") || qtype.equals("blank-comb")) {
					System.out.println(" > getAnswerTerm");
					choice = getAnswer.getAnswerTerm(q, targetMap);
				} else if (qtype.equals("TF-underline") || qtype.equals("TF")) {
					System.out.println(" > getAnswerTermTf");
					choice = getAnswer.getAnswerTf(q, targetMap);
				} else if (qtype.equals("order")) {
					System.out.println(" > getAnswerTermOrder");
					choice = getAnswer.getAnswerOrder(q, targetMap);
				} else if (qtype.equals("TF-comb")|| qtype.equals("T-comb")){
					System.out.println(" > getAnswerTermTfComb");
					choice = getAnswer.getAnswerTfComb(q, targetMap);
				}
			}
			if (choice == 0) {
				System.out.println("choice = 0 ???");
				choice = 1;
			}
			System.out.println("****** choice="+choice+ " ******");

			// Question
			Question question = new Question(inView);
			question = q.getQuestion();
			
			String maxiNumStr = getMaxiQuestionNum(exams, question);
			String miniNumStr = question.getCoveredText().trim();
			if (regexCache.find("^問(\\S+)\\s", miniNumStr)) {
				miniNumStr = regexCache.lastMatched(1);
				miniNumStr = TextUtil.convertTwoByteToOneByte(miniNumStr);
			}
			
			question.setSectionId(maxiNumStr+"-"+miniNumStr);

			String answerType = question.getAnswer_type();
//			String answerStyle = question.getAnswer_style();
			String knowledgeType = question.getKnowledge_type();
			String questionId = question.getId();
			String ansColumnId = question.getAnscol();
			
			// Output Answer
			DataAT dataATFS = new DataAT(inView);
//			dataATFS.setBegin(sb.length());
			dataATFS.addToIndexes();
			beginList.add(dataATFS);
			
			// Section
			Section sectionFS = new Section(inView);
//			sectionFS.setBegin(sb.length());
			sectionFS.setString("第"+maxiNumStr+"問");
			sb.append("第"+maxiNumStr+"問");
//			sectionFS.setEnd(sb.length());
			sectionFS.addToIndexes();
			beginList.add(sectionFS);
			endList.add(sectionFS);
			// questionAT
			QuestionAT questionATFS = new QuestionAT(inView);
//			questionATFS.setBegin(sb.length());
			questionATFS.setString(miniNumStr);
			sb.append(miniNumStr);
//			questionATFS.setEnd(sb.length());
			questionATFS.addToIndexes();
			beginList.add(questionATFS);
			endList.add(questionATFS);
			// answerColumn
			AnswerColumn answerColumnFS = new AnswerColumn(inView);
//			answerColumnFS.setBegin(sb.length());
			answerColumnFS.setString(q.getAnswernum());
			sb.append(q.getAnswernum());
//			answerColumnFS.setEnd(sb.length());
			answerColumnFS.addToIndexes();
			beginList.add(answerColumnFS);
			endList.add(answerColumnFS);
			// answer
			Answer answerFS = new Answer(inView);
//			answerFS.setBegin(sb.length());
			answerFS.setSystemAnswer("no");
			answerFS.setAnsColumn(q.getAnswernum());
			answerFS.setString(String.valueOf(choice));
			sb.append(String.valueOf(choice));
//			answerFS.setEnd(sb.length());
			answerFS.addToIndexes();
			beginList.add(answerFS);
			endList.add(answerFS);
			// score
			Score scoreFS = new Score(inView);
//			scoreFS.setBegin(sb.length());
			scoreFS.setString(String.valueOf(q.getScore()));
			sb.append(String.valueOf(q.getScore()));
//			scoreFS.setEnd(sb.length());
			scoreFS.addToIndexes();
			beginList.add(scoreFS);
			endList.add(scoreFS);
			// answerType
			AnswerType answerTypeFS = new AnswerType(inView);
//			answerTypeFS.setBegin(sb.length());
			answerTypeFS.setString(answerType);
			sb.append(answerType);
//			answerTypeFS.setEnd(sb.length());
			answerTypeFS.addToIndexes();
			beginList.add(answerTypeFS);
			endList.add(answerTypeFS);
			// answerStyle
			/*
			if (answerStyle != null) {
				AnswerStyle answerStyleFS = new AnswerStyle(jCas);
				answerStyleFS.setBegin(sb.length());
				answerStyleFS.setString(answerStyle);
				sb.append(answerStyle);
				answerStyleFS.setEnd(sb.length());
				answerStyleFS.addToIndexes();
				beginList.add(answerStyleFS);
				endList.add(answerStyleFS);
			}
			*/
			// knowledge
			Knowledge knowledgeFS = new Knowledge(inView);
//			knowledgeFS.setBegin(sb.length());
			knowledgeFS.setString(knowledgeType);
			sb.append(knowledgeType);
//			knowledgeFS.setEnd(sb.length());
			knowledgeFS.addToIndexes();
			beginList.add(knowledgeFS);
			endList.add(knowledgeFS);
			// question_ID
			QuestionID questionIDFS = new QuestionID(inView);
//			questionIDFS.setBegin(sb.length());
			questionIDFS.setString(questionId);
			sb.append(questionId);
//			questionIDFS.setEnd(sb.length());
			questionIDFS.addToIndexes();
			beginList.add(questionIDFS);
			endList.add(questionIDFS);
 
			AnswerColumnID answerColumnIDFS = new AnswerColumnID(inView);
//			answerColumnIDFS.setBegin(sb.length());
			answerColumnIDFS.setString(ansColumnId);	// ansColumnId
			sb.append(ansColumnId);						// ansColumnId
//			answerColumnIDFS.setEnd(sb.length());
			answerColumnIDFS.addToIndexes();
			beginList.add(answerColumnIDFS);
			endList.add(answerColumnIDFS);

			// キーワードの重み数（Indriクエリ？）を入れる
			ProcessLog processLog = new ProcessLog(inView);
//			processLog.setBegin(sb.length());
			processLog.setString("CenterAnswerAnalysis log");	//
			sb.append("CenterAnswerAnalysis log");
//			processLog.setEnd(sb.length());
			processLog.addToIndexes();
			beginList.add(processLog);
			endList.add(processLog);
			
//System.out.println(sb.toString());
			dataATFS.setString(sb.toString());
//			dataATFS.setEnd(sb.length());
			endList.add(dataATFS);
		}
//		answerTable.setEnd(sb.length());
		endList.add(answerTable);

		AnswerMachineType answerMachineType = new AnswerMachineType(inView);
		answerMachineType.addToIndexes();

		AnswerTableAnnotationList answerTableAnnotationList = new AnswerTableAnnotationList(inView);
		answerTableAnnotationList.addToIndexes();

		AnswerTableEndList answerTableEndListFS = new AnswerTableEndList(inView);
		answerTableEndListFS.addToIndexes();

		FSArray fsAnswerTableAnnotationList = new FSArray(inView, beginList.size());
		FSArray fsAnswerTableEndList = new FSArray(inView, endList.size());

		for (int i = 0; i < beginList.size(); i++) {
			fsAnswerTableAnnotationList.set(i, beginList.get(i));
			fsAnswerTableEndList.set(i, endList.get(i));
		}
		answerTableAnnotationList.setBeginList(fsAnswerTableAnnotationList);
		answerTableEndListFS.setEndList(fsAnswerTableEndList);
	}

	private String getMaxiQuestionNum(Map<String, CeQuestion>exams, Question miniq) {
		
		Set<String> idset = exams.keySet(); 
		int index = 1;
		for (String id : idset) {
			CeQuestion ceq = exams.get(id);
			Question maxiq = ceq.getQuestion();
			if (maxiq.getBegin() <= miniq.getBegin() && maxiq.getEnd() >= miniq.getEnd())
				return String.valueOf(index);
			index++;
		}
		return "";		
	}
	
	private Map<String, Map<String, Map<Integer, CeAnswer>>> getQAAns(JCas jCas) throws AnalysisEngineProcessException {

		//         A1                  1_0                
		// Map<xmlファイル名-空欄ID, Map<空欄IDごとの質問文ID, Map<score_sofa_id, Answer>>>
		Map<String, Map<String, Map<Integer, CeAnswer>>> qa_ans = new TreeMap<String, Map<String, Map<Integer, CeAnswer>>>();

		try {
			Iterator<JCas> agCasIter = jCas.getViewIterator(AG_RESULT_VIEW_NAME_PREFIX);
			if(agCasIter == null) {
System.out.println("ag result view is null");				
				return qa_ans;
			}
			while(agCasIter.hasNext()) {
				JCas agView = agCasIter.next();
				String topicId	= "";
				String target_q_id = "";
				String target_num = "";
				// TOPIC IDの取得
				Iterator<TOP> sourceDocInfoFSIterator = agView.getJFSIndexRepository().getAllIndexedFS(SourceDocumentInformation.type);
				if (sourceDocInfoFSIterator.hasNext()) {
					SourceDocumentInformation sdi = (SourceDocumentInformation) sourceDocInfoFSIterator.next();
					topicId = sdi.getUri();
					String[] topics = topicId.split("-");
					int size = topics.length;
					
					if (size < 2) {
						System.err.println("topicid format error:"+topicId);
						continue;
					} else {
						target_q_id = topics[size-2];
						target_num = topics[size-1];
					}
				} else {
					System.err.println(agView.getViewName()+" has no SourceDocumentInformation!");
				}
				String answerStr = agView.getDocumentText();
				answerStr = answerStr.replaceAll("&#10;","");
				answerStr = answerStr.replaceAll("\n","");

				Iterator<TOP> acIter = agView.getJFSIndexRepository().getAllIndexedFS(AnswerCandidate.type);
				if (acIter.hasNext()) {
					AnswerCandidate ac = (AnswerCandidate)acIter.next();
					int rank = ac.getRank();
					double score = ac.getScore();
					int score_sofa_id = ac.getAddress();
					// String docId = ac.getDocId();
					CeAnswer ans = new CeAnswer(answerStr, rank, score);
					Map<String, Map<Integer, CeAnswer>> targetMap = null;
					Map<Integer, CeAnswer> ansMap = null;
					if (!qa_ans.containsKey(target_q_id)) {
						targetMap = new TreeMap<String, Map<Integer, CeAnswer>>();
						qa_ans.put(target_q_id, targetMap);
					}
					targetMap = qa_ans.get(target_q_id);
					
					if (!targetMap.containsKey(target_num)) {
						ansMap = new TreeMap<Integer, CeAnswer>();
						targetMap.put(target_num, ansMap);
					}
					ansMap = targetMap.get(target_num);
					ansMap.put(score_sofa_id, ans);
System.out.println("qa_ans("+target_q_id+", Map("+target_num+", Map("+score_sofa_id+",("+answerStr+":"+rank+":"+score+"))))");
				}
			}
		} catch(CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}
		return qa_ans;
	}
	
	
}
