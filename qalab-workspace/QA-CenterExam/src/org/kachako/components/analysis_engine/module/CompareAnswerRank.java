package org.kachako.components.analysis_engine.module;

import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;



public class CompareAnswerRank implements Comparator<Entry<Integer, CeAnswer>> {

	@Override
	public int compare(Entry<Integer, CeAnswer> o1, Entry<Integer, CeAnswer> o2) {
		Map.Entry<Integer, CeAnswer> e1 = o1;
		Map.Entry<Integer, CeAnswer> e2 = o2;

		int rank1 = ((CeAnswer)e1.getValue()).getRank();
		int rank2 = ((CeAnswer)e2.getValue()).getRank();

		if (rank1 > rank2)
			return 1;
		else if (rank1 == rank2)
			return 0;
		else
			return -1;

	}

}
