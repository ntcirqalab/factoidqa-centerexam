package org.kachako.components.analysis_engine.module;

import java.util.Map;
import java.util.TreeMap;

import org.kachako.types.centerexam.Question;

public class CeQuestion {

	private String questiontype;			// xml_center_analysisで追加
	private String answertype;				// from data
	private String q_line_org;				// from data
	private String q_line;					// from data
	private String knowledge;				// from data
	private String answernum;				// from data
	private Map<String, String> choice;	 	// from data	// n:m(ライン:カラム)みたいなのも入れるのでkeyはStringじゃないとダメ？
	private Map<String, String> ref_utext;	// from data
	private Map<String, String> QA_query;	// QuestoinAnalysisで追加
	private Map<String, String> sup_ans;	// QuestoinAnalysisで追加
	private double score;					// AnswerAnalysisで追加
	private Question question;				// original Question

	public CeQuestion() {
		questiontype = "";
		answertype	= "";
		q_line_org	= "";
		q_line		= "";
		knowledge	= "";
		answernum	= "";
		choice		= new TreeMap<String, String>();		
		ref_utext	= new TreeMap<String, String>();
		QA_query	= new TreeMap<String, String>();
		sup_ans		= new TreeMap<String, String>();
		score		= 0.0;
		question	= null;
	}

	public String getQuestionType() {
		return questiontype;
	}

	public void setQuestionType(String qtype) {
		this.questiontype = qtype;
	}

	public String getAnswerType() {
		return answertype;
	}

	public void setAnswerType(String atype) {
		this.answertype = atype;
	}

	public String getQLine() {
		return q_line;
	}
	public void setQLine(String q_line) {
		this.q_line = q_line;
	}

	public String getQLineOrg() {
		return q_line_org;
	}
	public void setQLineOrg(String q_line_org) {
		this.q_line_org = q_line_org;
	}

	public String getKnowlede() {
		return knowledge;
	}
	public void setKnowledge(String knowledge) {
		this.knowledge = knowledge;
	}

	public String getAnswernum() {
		return answernum;
	}
	public void setAnswernum(String answernum) {
		this.answernum = answernum;
	}


	public Map<String, String> getChoice() {
		return choice;
	}

	public String getChoice(String c_num) {
		if (choice.containsKey(c_num)) {
			return choice.get(c_num);
		}
		return "";
	}

	public void setChoice(String c_num, String value) {
		choice.put(c_num, value);
	}

	public Map<String, String> getQAQuery() {
		return QA_query;
	}
	public void setQAQuery(Map<String, String> QA_query) {
		this.QA_query = QA_query;
	}


	public Map<String, String> getSupAns() {
		return sup_ans;
	}

	public void setSupAns(Map<String, String> sup_ans) {
		this.sup_ans = sup_ans;
	}

	public Map<String, String> getRefUText() {
		return ref_utext;
	}

	public String getRefUText(String key) {
		if (ref_utext.containsKey(key))
			return ref_utext.get(key);
		return "";
	}

	public void setRefUText(String key, String value) {
		ref_utext.put(key, value);
	}

	public void setScore(double score) {
		this.score = score;
	}
	
	public double getScore() {
		return score;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public Question getQuestion() {
		return question;
	}
}
