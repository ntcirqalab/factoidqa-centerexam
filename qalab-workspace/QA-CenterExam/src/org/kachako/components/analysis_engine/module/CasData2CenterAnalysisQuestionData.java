package org.kachako.components.analysis_engine.module;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.tcas.Annotation;
import org.kachako.share.regex.RegexCache;
import org.kachako.types.centerexam.Adata;
import org.kachako.types.centerexam.AnsColumn;
import org.kachako.types.centerexam.Blank;
import org.kachako.types.centerexam.CNum;
import org.kachako.types.centerexam.Choice;
import org.kachako.types.centerexam.Data;
import org.kachako.types.centerexam.Exam;
import org.kachako.types.centerexam.Instruction;
import org.kachako.types.centerexam.LText;
import org.kachako.types.centerexam.Label;
import org.kachako.types.centerexam.Question;
import org.kachako.types.centerexam.Ref;
import org.kachako.types.centerexam.UText;

public class CasData2CenterAnalysisQuestionData {

	// 問題文の内容が年代順のパターン
	private static final String PAT_CHRONOLOGICAL_QS = "年代順|年代の古いもの|歴史的な流れ|時代順|正しく配列|年代の古い順|配列しているもの";
	// 「表」を使用する問題文のパターン
	private static final String PAT_TABLE_QS	= "次の年表";
	private final RegexCache regexCache	= new RegexCache();
	
	
	public void parseQuestionData(FSArray tagListFsArray, //FSArray endListFsArray,
			Map<Question, LinkedHashSet<Data>> maximalQsMap,
			Map<Question, Data> miniQsDataMap,
			Map<Question, List<Question>> miniQuestionMap,
			Map<Question, List<AnsColumn>> miniAnsColumnMap,
			Map<Question, List<Annotation>> minimalQsAnnotationMap,
			Map<Question, AnsColumn> minimalQsAnsColumnMap,
			List<Ref> refList,
			List<Annotation> blankList,
			List<Annotation> labelList,
			boolean yozemiFlag,
			StringBuilder selected) {

		if (tagListFsArray == null || tagListFsArray.size() <= 0) {
			return;
		}

		LinkedHashSet<Data> sectionDataHashSet	= new LinkedHashSet<Data>();
		List<Question> miniQsList				= new ArrayList<Question>();
		List<AnsColumn> miniAnsColumList		= new ArrayList<AnsColumn>();
		List<Annotation> annotationList			= new ArrayList<Annotation>();

		boolean ansColumnFlag	= false;
		Question currentMaximalQs	= null;
		Data currentData	= null;
		Question currentMinimalQs	= null;
		for(int i = 0; i < tagListFsArray.size(); i++) {
			//System.out.println("tagList : "+tagListFsArray.get(i).getType()+" endList : "+endListFsArray.get(i).getType());
			if (currentMinimalQs != null && currentMinimalQs.getEnd() <= ((Annotation)tagListFsArray.get(i)).getBegin()) {
				ansColumnFlag	= false;
			}
			if(tagListFsArray.get(i).getType().getName().equals(Exam.class.getName())){
				Annotation ann = (Annotation) tagListFsArray.get(i);
				String range = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("range_of_options"));
        		String num = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("num_of_options"));
        		if(range != null && num != null){
        			try {
						String ids[] = range.split(" ");
						int n = Integer.valueOf(num);
						for (int j = 0; j < ids.length; j++) {
							if(j < n)
								selected.append(ids[j] + " ");
							else
								break;
						}
					} catch (Exception e) {
					}
        		}
			}
			if (tagListFsArray.get(i).getType().toString().equals(Instruction.class.getName())) {
				// Instruction
				if(currentMinimalQs == null) {
					continue;
				}
				Instruction instruction 	= (Instruction)tagListFsArray.get(i);
				String instructionText	= instruction.getCoveredText();
				//if(!yozemiFlag && regexCache.find(PAT_IMAGE_QS,instructionText)) {
				//// 図を取扱うのは無視・・・
				//currentMinimalQs = null;
				//ansColumnFlag	= false;
				//continue;
				//}
				if (!yozemiFlag && regexCache.find(PAT_TABLE_QS,instructionText)) {
					// 表を取扱うのは無視・・・
					currentMinimalQs = null;
					ansColumnFlag	= false;
					continue;
				}
				annotationList.add(currentMinimalQs);
				miniQsList.add(currentMinimalQs);
				miniQsDataMap.put(currentMinimalQs, currentData);
			}
			if (tagListFsArray.get(i).getType().toString().equals(Question.class.getName())) {
				Question question 	= (Question)tagListFsArray.get(i);
				if (currentMinimalQs != null) {
					minimalQsAnnotationMap.put(currentMinimalQs, annotationList);
				}
				if (currentMaximalQs != null) {				
					miniQuestionMap.put(currentMaximalQs, miniQsList);
					miniAnsColumnMap.put(currentMaximalQs, miniAnsColumList);
				}
				annotationList	= new ArrayList<Annotation>();
				ansColumnFlag	= false;

				if(question.getMinimal() == null || question.getMinimal().equals("no")) {
					if(currentMaximalQs != null) {
						maximalQsMap.put(currentMaximalQs, sectionDataHashSet);
						sectionDataHashSet = new LinkedHashSet<Data>();
					}
					currentMaximalQs = question;
					currentMinimalQs = null;
					currentData		 = null;
					ansColumnFlag	 = false;
					miniQsList	= new ArrayList<Question>();
					miniAnsColumList	= new ArrayList<AnsColumn>();
					continue;
				}
				if (question.getMinimal().equals("yes")) {
					if (question.getKnowledge_type() == null || question.getAnswer_type() == null) {
						currentMinimalQs = null;
						ansColumnFlag	= false;
						continue;
					}
//System.out.println("anscol="+question.getAnscol());
					if (yozemiFlag) { 
						// 代ゼミ問題
						if (regexCache.find("KS", question.getKnowledge_type())) {
							//if(!regexCache.find("IC_T", question.getKnowledge_type())
							//&& !regexCache.find("IC_G", question.getKnowledge_type())) {
							if(!regexCache.find("IC_G", question.getKnowledge_type())) {
								currentMinimalQs = question;
							} else {
								currentMinimalQs = null;
								ansColumnFlag	= false;							
							}
						} else {
							currentMinimalQs = null;
							ansColumnFlag	= false;							
						}
					} else {
						if (regexCache.find("KS", question.getKnowledge_type())) {
							if (!regexCache.find("image", question.getAnswer_type())
									&& !regexCache.find("IC_T", question.getKnowledge_type())
									&& !regexCache.find("IC_G", question.getKnowledge_type())) {
								currentMinimalQs = question;
							} else {
								currentMinimalQs = null;
								ansColumnFlag	= false;	
							}
						} else {
							currentMinimalQs = null;
							ansColumnFlag	= false;
						}
					}
				}
				continue;
			}
			if (tagListFsArray.get(i).getType().toString().equals(Data.class.getName())) {
				Data data	= (Data)tagListFsArray.get(i);
//System.out.println("data type="+data.getTypes());
				//if(data.getTypes() != null && (data.getTypes().equals("text") || data.getTypes().equals("complex") || (yozemiFlag && data.getTypes().equals("image")))) {
				if (data.getTypes() != null && (data.getTypes().equals("text") || data.getTypes().equals("complex")
						|| (yozemiFlag && data.getTypes().equals("image"))
						|| (yozemiFlag && data.getTypes().equals("table")))) {
					if (ansColumnFlag) {
						if (currentMinimalQs != null) {
							if (currentMinimalQs.getBegin() <= data.getBegin() && currentMinimalQs.getEnd() >= data.getEnd()) {
								annotationList.add(data);
								//System.out.println("1: data="+data.getCoveredText());
								continue;
							}
						} else {
							//System.out.println("2: data="+data.getCoveredText());
							// questionタグがない場合のエラー処理 or 属性がKSでないquestion
							ansColumnFlag	= false;
							continue;
						}
					}
					if(annotationList != null && annotationList.size() > 0) {
						minimalQsAnnotationMap.put(currentMinimalQs, annotationList);
						miniQuestionMap.put(currentMaximalQs, miniQsList);
						miniAnsColumnMap.put(currentMaximalQs, miniAnsColumList);
					}

					annotationList	= new ArrayList<Annotation>();
					currentMinimalQs	= null;

					currentData	= data;
					sectionDataHashSet.add(currentData);
					ansColumnFlag	= false;
					//System.out.println("3: currentData="+currentData.getCoveredText());
				}
				continue;
			}
			if (tagListFsArray.get(i).getType().toString().equals(AnsColumn.class.getName())) {
				if (currentMinimalQs != null) {
					ansColumnFlag	= true;
					AnsColumn ansColumn = (AnsColumn)tagListFsArray.get(i);
					miniAnsColumList.add(ansColumn);
					minimalQsAnsColumnMap.put(currentMinimalQs, ansColumn);
				}
			}
			if (currentMinimalQs != null) {
				if (currentMinimalQs.getBegin() <= ((Annotation)tagListFsArray.get(i)).getBegin() 
						&& currentMinimalQs.getEnd() >= ((Annotation)tagListFsArray.get(i)).getEnd()) {
					//System.out.println("add:"+tagListFsArray.get(i).getType()+" : "+((Annotation)tagListFsArray.get(i)).getCoveredText());
					annotationList.add((Annotation)tagListFsArray.get(i));
				}
			}
			if(tagListFsArray.get(i).getType().getName().equals(Label.class.getName())) {
				labelList.add((Label) tagListFsArray.get(i));
			} else if(tagListFsArray.get(i).getType().getName().equals(Ref.class.getName())) {
				refList.add((Ref) tagListFsArray.get(i));
			} else if(tagListFsArray.get(i).getType().getName().equals(Blank.class.getName()) 
//				|| tagListFsArray.get(i).getType().getName().equals(UText.class.getName())) 
					){
				blankList.add((Annotation) tagListFsArray.get(i));
			}
		}

		if(annotationList != null && annotationList.size() > 0) {
			minimalQsAnnotationMap.put(currentMinimalQs, annotationList);
		}
		//if(currentMaximalQs != null && sectionDataHashSet.size() > 0) {
		if(currentMaximalQs != null) {
			if(sectionDataHashSet.size() <= 0) {
				sectionDataHashSet.add(null);
			}
			maximalQsMap.put(currentMaximalQs, sectionDataHashSet);
			miniQuestionMap.put(currentMaximalQs, miniQsList);
			miniAnsColumnMap.put(currentMaximalQs, miniAnsColumList);
		}
		return;
	}

	
	public Map<String, CeQuestion> makeCeQuestionData(Map<Question, List<Question>> miniQuestionMap, 
			Map<Question,List<Annotation>> minimalQsAnnotationMap, 
			Map<Question,Data> miniQsDataMap,
			Map<Question,AnsColumn> minimalQsAnsColumnMap,
			List<Annotation>labelList, List<Annotation>blankList, List<Ref> refList) {

		Map<String, CeQuestion> exam = new LinkedHashMap<String, CeQuestion>();

		for (Map.Entry<Question, List<Question>> entry : miniQuestionMap.entrySet()) {
			List<Question>	miniQuestionList = entry.getValue();
			if(miniQuestionList == null || miniQuestionList.size() <= 0) {
//				maximalNum++;
				continue;
			}
			for(Question miniQuestion : miniQuestionList) {
//System.out.println("miniQ:anscolid:"+miniQuestion.getAnscolumn_ids());	
System.out.println("miniQ:anscol:"+miniQuestion.getAnscol());				
				List<Annotation> annList = minimalQsAnnotationMap.get(miniQuestion);
				if(regexCache.find(PAT_CHRONOLOGICAL_QS, miniQuestion.getCoveredText())) {
					//System.out.println("年代順");
					//continue;
				}
				Data setumonData	= miniQsDataMap.get(miniQuestion);
				String miniQuestionText	= miniQuestion.getCoveredText().trim();
				String minimalId	= null;
				if (regexCache.find("^問(\\S+)\\s", miniQuestionText)) {
					minimalId	= regexCache.lastMatched(1);
					minimalId	= TextUtil.convertTwoByteToOneByte(minimalId);
				}
				if (minimalId == null) {
					continue;
				}
				CeQuestion q = new CeQuestion();
				
				String answerType = miniQuestion.getAnswer_type();			// answer_type
				String knowledgeType = miniQuestion.getKnowledge_type();	// knowledge_type
				q.setQuestion(miniQuestion);
				q.setKnowledge(knowledgeType);
				q.setAnswerType(answerType);

				// set default QuestionType and RefUText
//				String datastr = setumonData.getCoveredText();
				setDefaultQuestionType(annList, q, setumonData, labelList, blankList, refList);
				
				// AnsColumn
				AnsColumn ansColumn = minimalQsAnsColumnMap.get(miniQuestion);
				String ansColumnText = "";
				if (ansColumn != null)
					ansColumnText = ansColumn.getCoveredText().trim();
				q.setAnswernum(ansColumnText);

				Map<String, String> utextMap = new HashMap<String,String>();
				List<Choice> choiceList = new ArrayList<Choice>();
				List<CNum> cNumList = new ArrayList<CNum>();
				Instruction instruction = getSentencesQuestion(annList, setumonData, ansColumnText, utextMap, choiceList, refList, cNumList);
				String org = "";
				if (instruction != null) 
					org = instruction.getCoveredText().trim();

				q.setQLineOrg(org);
				// qlineとchoiceのrefのidに置き換える;
				String qline2 = replaceRefString(instruction, refList, null);

				// 下線の置き換え
				if(utextMap != null && utextMap.size() > 0) 
					qline2 = replaceUtext(utextMap, qline2, false);		// 下線部(4) → 下線部U4等に置き換える
				qline2 = replaceQuestionText(qline2);
				q.setQLine(qline2);
				
				for (int i = 0 ; i < choiceList.size(); i++) {
					String choiceStr = replaceRefString(choiceList.get(i), refList, cNumList.get(i));
					q.setChoice(Integer.toString(i+1), choiceStr);
				}
				
				if (q.getQuestionType() == null || q.getQuestionType().length() == 0) {
					// print STDERR "$id_q : $type\n";
					if (answerType.matches("^term.*-term.*")) {
						q.setQuestionType("factoid-comb");
					} else if (answerType.matches("^term.*")) {
						q.setQuestionType("factoid");
					} else if (answerType.contains("(symbol-TF)")) {
						q.setQuestionType("TF-comb");
					} else if(answerType.contains("o(symbol-")) {
						q.setQuestionType("order");
					} else if (answerType.contains("symbol-symbol") && 
							(qline2.contains("正しいものの組合わせ") ||
									qline2.contains("正しいものの組み合わせ") ||
									qline2.contains("正しいものの組合せ)"))) {
						q.setQuestionType("T-comb");
					}
				}
				if (q.getQuestionType().equals("blank")) {
					if (!(qline2.contains("入る") || qline2.contains("入れる"))) {
						q.setQuestionType("");
					} else if(qline2.contains("組み合わせ") || 
							qline2.contains("組合せ") || 
							qline2.contains("組合わせ")) { 
						q.setQuestionType("blank-comb");
					}
				}
				if (q.getQuestionType() == null || q.getQuestionType().length() == 0) {
					if (answerType.contains("sentence")) {
						q.setQuestionType("TF");
					}
				}		 
				if(!knowledgeType.equals("KS") && !knowledgeType.equals("KS,RT") && !knowledgeType.equals("RT,KS")) {
					q.setQuestionType("not_target");
				}
				exam.put(miniQuestion.getAnscol(), q);
			}
		}
		return exam;
	}
	
	
	private void setDefaultQuestionType(List<Annotation> annList, CeQuestion q, Data setumonData, 
			List<Annotation>labelList, List<Annotation>blankList, List<Ref> refList) {
		
		for(Annotation ann : annList) {
			if(ann.getType().toString().equals(Ref.class.getName())) {
				Ref	ref	= (Ref)ann;
				if(ref.getReference() == null) {
					continue;
				}
				String targetStr = ref.getTarget();	// U1とかL1が入っている
				String str = "";
				if(ref.getReference().getType().toString().equals(UText.class.getName())) {
					str = replaceBlankString(ref.getReference(), null, null, labelList);
				} else if(ref.getReference().getType().toString().equals(Blank.class.getName())) {
					q.setQuestionType("blank");
					str = replaceBlankString(setumonData, blankList, targetStr, labelList);	// TODO: 
				} else if(ref.getReference().getType().toString().equals(LText.class.getName())) {
					str = replaceBlankString(ref.getReference(), null, null, labelList);
				}
				str = str.replaceAll("\n","").replaceAll("^\\s+","").replaceAll("\\s+$","");
				q.setRefUText(targetStr, str);
			}
		}
	}
	

	/**
	 * blank-combでref_utextのテキスト中のキーワードをIDに変換する
	 * <label>は削除する
	 * TODO:ちゃんと位置をみないと相当だめ
	 * @param anno
	 * @param blankList
	 * @param targetId
	 * @return
	 */
	private String replaceBlankString(Annotation anno, List<Annotation> blankList, String targetId, List<Annotation>labelList) {
		
		if (anno == null)
			return "";
		
		String str = anno.getCoveredText();
		int begin = anno.getBegin();
		int end = anno.getEnd();

		List<Annotation> annotationList = new ArrayList<Annotation>();
		if (blankList != null)
			annotationList.addAll(blankList);
		if (labelList != null)
			annotationList.addAll(labelList);
		
		if (blankList != null) {
			Collections.sort(annotationList, new Comparator<Annotation>() {	
				public int compare(Annotation a1, Annotation a2) {
					int end1 = a1.getBegin();
					int end2 = a2.getBegin();
					if (end1 > end2)
						return 1;
					else if (end1 == end2) {
						if (a1.getType().toString().equals(Label.class.getName()))
							return -1;
						return 0;
					}else
						return -1;
				}
			});
			Collections.reverse(annotationList);
		}
		for (Annotation a : annotationList) {
			if (a.getBegin() >= begin && a.getEnd() <= end) {
				if (a.getType().toString().equals(Blank.class.getName())) {
					String idStr = ((Blank)a).getId();
					if (str.length() < (a.getEnd() - begin) || str.length() < (a.getBegin() - begin))
						continue;
					String tmp = str.substring(0, a.getBegin() - begin);
					if (idStr.equals(targetId)) 
						tmp = tmp + "|target|";
					else 
						tmp = tmp + "|"+idStr+"|";
					tmp = tmp + str.substring(a.getEnd() - begin);
					str = tmp;
				} else if (a.getType().toString().equals(Label.class.getName())) {
					String orgStr = a.getCoveredText();
					if (str.length() < (a.getEnd() - begin) || str.length() < (a.getBegin() - begin))
						continue;
					String nowStr = str.substring(a.getBegin() - begin, a.getEnd() - begin);
					if (orgStr.equals(nowStr)) {
						// Labelを削除
						String tmp = str.substring(0, a.getBegin() - begin);
						tmp = tmp + str.substring(a.getBegin() + orgStr.length() - begin);
						str = tmp;
					}
				}
			}
		}
		return str;
	}
	
	/**
	 * 
	 */
	private String replaceRefString(Annotation anno, List<Ref>refList, CNum cNum) {

		if (anno == null) 
			return "";

		String annoStr = anno.getCoveredText();
		int begin = anno.getBegin();
		int end = anno.getEnd();

		Collections.sort(refList, new Comparator<Annotation>() {	
			public int compare(Annotation a1, Annotation a2) {
				int end1 = a1.getBegin();
				int end2 = a2.getBegin();
				if (end1 > end2)
					return 1;
				else if (end1 == end2) {
					return 0;
				}else
					return -1;
			}
		});
		Collections.reverse(refList);
			
		for (Ref a : refList) {
			if (a.getBegin() >= begin && a.getEnd() <= end) {
				String idStr = a.getTarget();
				if (idStr == null)
					continue;
				if (annoStr.length() < (a.getEnd() - begin) || annoStr.length() < (a.getBegin() - begin))
					continue;
				String tmp = annoStr.substring(0, a.getBegin() - begin);
				tmp = tmp + idStr;
				tmp = tmp + annoStr.substring(a.getEnd() - begin);
				annoStr = tmp;
			}
		}
		
		annoStr = annoStr.replaceAll("—|−|－|―","-");
		annoStr = annoStr.replaceAll("\n"," ");
		annoStr = annoStr.replaceAll("[ ]", " ");
		if (cNum != null) {
			String cNumStr	= cNum.getCoveredText();
			annoStr	= annoStr.replaceAll(cNumStr, "");
		}
		annoStr = annoStr.replaceAll("^\\s+","");
		annoStr = annoStr.replaceAll("\\s+$","");	
		return annoStr.trim();
//		for(int i = 0; i < choiceAnnoList.size(); i++) {
//		String choice = choiceAnnoList.get(i).getCoveredText().trim();
//		CNum cNum	= cNumList.get(i);
//		String cNumStr	= cNum.getCoveredText();
//		choice	= choice.replaceAll(cNumStr, "");
//		choice = replaceRefString(choiceAnnoList.get(i), refList, choice);
//		choiceList.add(i, choice);
//	}		
		
	}
	
	
	/**
	 * 
	 * @param annList
	 * @param setumonData
	 * @param ansColumnText
	 * @param uTextMap		
	 * @param choiceList
	 * @return
	 */
	private Instruction getSentencesQuestion(List<Annotation> annList, Data setumonData, String ansColumnText,
			Map<String,String> uTextMap, List<Choice> choiceList, List<Ref> refList, List<CNum> cNumList) {
		
		Instruction instruction	= null;
		LinkedHashSet<Blank> blankList	=  new LinkedHashSet<Blank>();
//		List<CNum> cNumList	= new ArrayList<CNum>();
		List<Adata> adataList	= new ArrayList<Adata>();
//		List<Choice> choiceAnnoList = new ArrayList<Choice>();

		for(Annotation ann : annList) {
			if(ann.getType().toString().equals(Ref.class.getName())) {
				Ref	ref	= (Ref)ann;
				if(ref.getReference() == null) {
					continue;
				}
				if(ref.getReference().getType().toString().equals(UText.class.getName())) {
					// 下線部
					uTextMap.put(ref.getCoveredText(), ref.getTarget());
System.out.println("utextMap.put("+ref.getCoveredText()+","+ref.getTarget()+")");
				} else if(ref.getReference().getType().toString().equals(Blank.class.getName())) {
					blankList.add((Blank)ref.getReference());	// 空欄
				}
			} else if(ann.getType().toString().equals(Instruction.class.getName())) {
				instruction	= (Instruction)ann;
			} else if(ann.getType().toString().equals(Data.class.getName())) {
//				internalData	= (Data)ann;
			} else if(ann.getType().toString().equals(Choice.class.getName())) {
				choiceList.add((Choice)ann);
			} else if(ann.getType().toString().equals(CNum.class.getName())) {
				cNumList.add((CNum)ann);
			} else if(ann.getType().toString().equals(Adata.class.getName())) {
				adataList.add((Adata)ann);
			}
		}
		// Refは置き換え、cNumは削除する
//		for(int i = 0; i < choiceAnnoList.size(); i++) {
//			String choice = choiceAnnoList.get(i).getCoveredText().trim();
//			CNum cNum	= cNumList.get(i);
//			String cNumStr	= cNum.getCoveredText();
//			choice	= choice.replaceAll(cNumStr, "");
//			choice = replaceRefString(choiceAnnoList.get(i), refList, choice);
//			choiceList.add(i, choice);
//		}		
		return instruction;
	}
	
	/**
	 * 問題文の「下線部」部分を置き換える.
	 * 
	 * @param uTextMap
	 * @param targetSentence
	 * @return
	 */
	private String replaceUtext(Map<String,String> uTextMap, String targetSentence, boolean flag) {

		for (Map.Entry<String, String> entry : uTextMap.entrySet()) {
			String ref = entry.getKey();
			String val = entry.getValue().trim();

			if (!flag) {
//				if(regexCache.find("下線部[(]("+ref+")[)]に(関連して|関して)", targetSentence)
//						|| regexCache.find("下線部"+ref+"に(関連して|関して)", targetSentence)) {
//					continue;
//				}
				targetSentence	= targetSentence.replaceAll("下線部[(]("+ref+")[)]|下線部"+ref, "下線部"+val);
			} else {
				val = val.replaceFirst(ref, "");
				targetSentence	= targetSentence.replaceAll("下線部"+ref, "下線部"+val);
			}
		}
		return targetSentence;
	}
	
	
	/**
	 * 下線部の文字列を取得する.
	 * 
	 * @param ref
	 * @return
	 */
//	private String getUtext(Ref ref)
//	{
//		if(!ref.getReference().getType().toString().equals(UText.class.getName())) {
//			return null;
//		}
//		UText	 uText	= (UText)ref.getReference();
//		String uTextString	= uText.getCoveredText();
//		uTextString	= uTextString.substring(uTextString.indexOf(")")+1);
//		uTextString	= uTextString.replaceAll("[|「|」|]", "");
//		return uTextString;
//	}

	private String replaceQuestionText(String questionText)
	{
		questionText	= questionText.replaceAll("[\\(]", "（");
		questionText	= questionText.replaceAll("[\\)]", "）");
		questionText	= questionText.replaceAll("[　| ]","");
		questionText	= questionText.replaceAll("\n","");

		questionText	= TextUtil.convertTwoByteToOneByte(questionText);

		return questionText;
	}

}
