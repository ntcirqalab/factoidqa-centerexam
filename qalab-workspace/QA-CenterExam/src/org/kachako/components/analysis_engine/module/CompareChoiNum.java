package org.kachako.components.analysis_engine.module;

import java.util.Comparator;
import java.util.Map.Entry;

public class CompareChoiNum  implements Comparator<Entry<Integer, Double>> {

	public CompareChoiNum() {
		
	}

	public int compare(Entry<Integer,Double> o1, Entry<Integer,Double> o2) {
		Entry<Integer,Double> e1 = o1;
		Entry<Integer,Double> e2 = o2;

		double score1 = (double) e1.getValue();
		double score2 = (double) e2.getValue();
		
		if (score1 > score2)
			return 1;
		else if (score1 == score2)
			return 0;
		else
			return -1;
	}

}
