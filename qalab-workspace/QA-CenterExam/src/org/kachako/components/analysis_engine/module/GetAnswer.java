package org.kachako.components.analysis_engine.module;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.mapdb.DB;
import org.mapdb.DBMaker;

/* QAシステムの出力を使って、選択肢を選ぶ
 * 入力 : 問の情報が入ったリファレンス、QAシステムへの解候補群が入ったリファレンス
 * 出力 : 選択した選択肢の番号
 *
 * QAシステムの解候補と、選択肢の中の語や想定解をマッチングする際には、wikipediaリダイレクトの情報を用いる
 * 完全一致するものがない場合、部分一致でもOKとしている
 */
public class GetAnswer {

	private final static String DBNAMEW1 = "DB-WIKI-RED";

	private DB dbw;
	private ConcurrentNavigableMap<Integer,String> wdb;

	/**
	 * コンストラクタ
	 */
	public GetAnswer(String dbFolder) {

		dbw = DBMaker.newFileDB(new File(Paths.get(dbFolder).resolve(DBNAMEW1).toUri()))
				.closeOnJvmShutdown().readOnly().make();
		wdb = dbw.getTreeMap(DBNAMEW1);
	}


	/**
	 * 選択肢が単語、単語の組み合わせであるfactoid,blank,blank-combに対応
	 * QAシステムの解候補と、選択肢の語をマッチングさせて選択肢を選ぶ
	 * 各選択肢にスコアをつけて、スコアが最も高い選択肢(誤ったものを選ぶ場合には、スコアが低い選択肢)を選ぶ
	 * 選択肢が1単語の場合には、マッチした中で最上位の解候補とマッチした選択肢を選ぶ
	 * 解候補と選択肢の語がマッチした際、解候補の順位を反映させたスコアをつける(ここでは選択肢のなかで、
	 * 何番目にマッチしたか、のみを使用、選択肢が単語の組み合わせの時に対応させるため)
	 */
	public int getAnswerTerm(CeQuestion question, Map<String, Map<Integer, CeAnswer>> qa_ans) {

		Map<String, Map<String, String>> all_choices = new HashMap<String, Map<String, String>>();
		Map<String, String> choices = new HashMap<String, String>();
		Map<String, String> can_choi = new HashMap<String, String>();

		String  b_id        = "";
		String  b_id_old    = "";
		int     q_flag      = -1;
		String  answer_text = "";
		boolean not_flag    = false;

		Map<Integer, Double> choi_num = new HashMap<Integer, Double>();
		choi_num.put(1, 0.0);
		choi_num.put(2, 0.00001);
		choi_num.put(3, 0.00001);
		choi_num.put(4, 0.0);

		// 各選択肢らしさのスコアの初期値。2と3を優遇しておく
		String qtype = question.getQuestionType();
		if (qtype.equals("factoid") || qtype.equals("blank")) {
			choices =  getChoicesSimple(question);
			// test_out
			for (String choi : choices.keySet()) {
				System.out.println(choi + " : " + choices.get(choi));
			}
			// そうでないものを選ぶ場合。とりあえずフラグだけ立てておく…
			Matcher m = Pattern.compile("ないもの|誤っているもの|誤りを含む").matcher(question.getQLine());
			if (m.find()) {
				not_flag = true;
			}
		} else if(qtype.equals("factoid-comb") || qtype.equals("blank-comb")) {
			all_choices = getChoicesComb(question);
// test_out
for (String choi : all_choices.keySet()) {
	for (String text : all_choices.get(choi).keySet()) {
		System.out.println(choi + " : " + text + " : " + all_choices.get(choi).get(text));
	}
}
		}
		List<String> qa_ans_keys = new ArrayList<String>(qa_ans.keySet());
		Collections.sort(qa_ans_keys);
		
		for (String q_id : qa_ans_keys) {

			Pattern p = Pattern.compile("(B\\d+)_.+");
			Matcher m = p.matcher(q_id);
			if(m.find()) {
				b_id = m.group(1); // $1
			} else {
				b_id =  q_id;
			}

			if (qtype.equals("factoid-comb") || qtype.equals("blank-comb")) {

				if (all_choices.containsKey(b_id)) {
					choices = all_choices.get(b_id);
				} else {
					if (!b_id.equals(b_id_old)) {
						q_flag++;
					}
					choices =  all_choices.get(q_flag);
					b_id_old = b_id;
				}
			}
			if (choices == null) {
				System.out.println("getAnswerTerm: choices is null");
				continue;
			}
// test_out_2
for (String can : choices.keySet()) {
	System.out.println("can "+ can + choices.get(can));
}
			// 解答のランクでソートしたものを順次取得
			Map<Integer, CeAnswer> answerMap = qa_ans.get(q_id);
			List<Entry<Integer, CeAnswer>> entrylist = new ArrayList<Entry<Integer, CeAnswer>>(answerMap.entrySet());
			Collections.sort(entrylist, new CompareAnswerRank());
			Collections.reverse(entrylist);	// 高いものから並べる

			for (Entry<Integer, CeAnswer> entry : entrylist) {
				CeAnswer a = entry.getValue();
				System.out.println("ans : "+a.getText());
				answer_text = a.getText();
				answer_text = zen2han(answer_text);

				if (choices.containsKey(answer_text)) {
					if(can_choi.containsKey(b_id)) {
						String tmp = can_choi.get(b_id) + "-" + choices.get(answer_text);
						can_choi.put(b_id, tmp);
					} else{
						can_choi.put(b_id, choices.get(answer_text));
					}
					// choice = choices{answer_text};
					System.out.println(b_id+" "+answer_text+" : "+a.getRank()+" : "+a.getScore()+" : (choice : "+can_choi.get(b_id)+")");
					// last;
				} else if (wdb.containsKey(answer_text)) {
					String ans = wdb.get(answer_text);
					if (!ans.contains("Category")) {
						System.out.println("\t("+b_id+" "+answer_text+" -> "+ans);
						answer_text = wdb.get(answer_text);
						if (choices.containsKey(answer_text)) {
							if (can_choi.containsKey(b_id)) {
								String tmp = can_choi.get(b_id) + "-" + choices.get(answer_text);
								can_choi.put(b_id, tmp);
							} else {
								can_choi.put(b_id, choices.get(answer_text));
							}
							// can_choi{b_id} = choices{answer_text};
							// choice = choices{asnwer_text};
							System.out.println(b_id+" "+answer_text+" : "+a.getRank()+" : "+a.getScore()+" : (choice : "+can_choi.get(b_id)+")");
							// last;
						}
					}
				}
			}

			if (!can_choi.containsKey(b_id)) {

				for (Entry<Integer, CeAnswer> entry : entrylist) {
					CeAnswer a = entry.getValue();
					System.out.println("ans : "+a.getText());
					answer_text = a.getText();
					answer_text = zen2han(answer_text);

					for (String can_text : choices.keySet()) {

						if (can_text.contains(answer_text) || answer_text.contains(can_text)) {
							if(can_choi.containsKey(b_id)) {
								String tmp = can_choi.get(b_id) + "-" + choices.get(can_text);
								can_choi.put(b_id, tmp);
							} else{
								can_choi.put(b_id, choices.get(can_text));
							}
							// choice = choices.get(answer_text);
							System.out.println(b_id+" (p) "+answer_text+" : "+a.getRank()+" : "+a.score+" : (choice : "+can_choi.get(b_id)+")");
							// last;
						} else if (wdb.containsKey(answer_text)) {
							if (wdb.get(answer_text).contains("Category")) {
								System.out.println("\t("+b_id+" "+answer_text+" -> "+wdb.get(answer_text)+")");
								answer_text = wdb.get(answer_text);

								if (can_text.contains(answer_text) || answer_text.contains(can_text)) {
									if (can_choi.containsKey(b_id)) {
										String tmp = can_choi.get(b_id) + "-" + choices.get(can_text);
										can_choi.put(b_id, tmp);
									} else {
										can_choi.put(b_id, choices.get(can_text));
									}
									// can_choi{b_id} = choices{can_text};
									// choice = choices{can_text};
									System.out.println(b_id+" (p) "+answer_text+" : "+a.getRank()+" : "+a.score+" : (choice : "+can_choi.get(b_id)+")");
									// last;
								}
							}
						}
					}
				}
			}
		}
		
		// 各選択肢にスコアをつける
		for (String can : can_choi.keySet()) {
			System.out.println(can + " : " + can_choi.get(can));
			int c_score = 2;
			String choi_line = can_choi.get(can);

			while (choi_line.contains("-")) {
				int st = choi_line.indexOf("-");
				String c_l = choi_line.substring(0, st);
				choi_line = choi_line.substring(st+1);
				c_score /= 2;
				if (c_l.contains(":")){
					String [] c_nums = c_l.split(":");
					for (String c_n : c_nums) {
						int n = Integer.parseInt(c_n);
						double dd = choi_num.get(n) + c_score;
						choi_num.put(n, dd);
					}
				} else {
					int n = Integer.parseInt(c_l);
					double dd = choi_num.get(n) + c_score;
					choi_num.put(n, dd);
				}
			}
			c_score /= 2;

			if (choi_line.contains(":")) {
				String [] c_nums = choi_line.split(":");
				for (String  c_n : c_nums) {
					int n = Integer.parseInt(c_n);
					double dd = choi_num.get(n) + c_score;
					choi_num.put(n, dd);
				}
			} else {
				int n = Integer.parseInt(choi_line);
				double dd = choi_num.get(n) + c_score;
				choi_num.put(n, dd);
			}
			// choice = can_choi.get(can);
		}

		// スコアが最も高い選択肢を解答として選ぶ
		// (スコア最大値が複数の場合はJavaのシステムにお任せ)
		boolean flag = false;
		int     choice = 0;

		List<Entry<Integer,Double>> choiList = new ArrayList<Entry<Integer,Double>>(choi_num.entrySet());
		Collections.sort(choiList, new CompareChoiNum());
		if (!not_flag) 
			Collections.reverse(choiList);

		for (Entry<Integer,Double> choi : choiList) {
			int c_num = choi.getKey();
			if (!flag) {
				choice = c_num;
				question.setScore(choi_num.get(c_num));
			}
			flag = true;
			System.out.println(c_num + " : " + choi_num.get(c_num));
		}
		return choice;
	}


	/**
	 * 単語→選択肢番号　のハッシュを作る
	 */
	private Map<String, String> getChoicesSimple(CeQuestion question) {

		Map<String, String> choices = new HashMap<String, String>();
		List<String> can_text = new ArrayList<String>();

		for (String c_num : question.getChoice().keySet()) { // foreach my $c_num (keys %{$question->{'choice'}}) {
			String c_text = question.getChoice(c_num);
			can_text = getCanText(c_text);

			for (String text : can_text) {	//foreach my $text (@can_text){
				choices.put(text, c_num);
			}
	    }
	    return(choices);
	}

	/**
	 * 単語→選択肢番号(複数あり)　のハッシュを作る
	 */
	private Map<String, Map<String, String>> getChoicesComb(CeQuestion question) {

		Map<String, Map<String,String>> choices =  new HashMap<String, Map<String,String>>();
		
		Pattern p20 = Pattern.compile("(.+)-(.+)-(.+)");
		Pattern p30 = Pattern.compile("(.+)-(.+)");
		
		Pattern p1 = Pattern.compile("(B\\d+)-(.+)(B\\d+)-(.+)-(B\\d+)-(.+)");
		Pattern p2 = Pattern.compile("(B\\d+)-(.+)(B\\d+)-(.+)");
		Pattern p3 = Pattern.compile("(B\\d+)(.+)(B\\d+)(.+)(B\\d+)(.+)");
		Pattern p4 = Pattern.compile("(B\\d+)(.+)(B\\d+)(.+)");
		
		for (String c_num : question.getChoice().keySet()) { // foreach my $c_num (keys %{$question->{'choice'}}){
			if (c_num == null)
				continue;
			
			List<String> c_text = new ArrayList<String>();
			List<String> b_num = new ArrayList<String>();
			b_num.add("0");
			b_num.add("1");
			b_num.add("2");
			
			String text = question.getChoice(c_num);
			// List<String> c_text =  new ArrayList<String>();
			// System.out.println(text);
			text = text.replaceAll("\\?","-");
			text = text.replaceAll("—","-");
			text = text.replace("-+","-");

			Matcher m20 = p20.matcher(text);
			Matcher m30 = p30.matcher(text);
			// System.out.println(text);
			if(text.contains("B")) {
				Matcher m1 = p1.matcher(text);
				Matcher m2 = p2.matcher(text);
				Matcher m3 = p3.matcher(text);
				Matcher m4 = p4.matcher(text);
				if (m1.find()) {
					c_text.add(m1.group(2)); // $2;
					c_text.add(m1.group(4)); // $4;
					c_text.add(m1.group(6)); // $6;
					b_num.set(0, m1.group(1)); // $1;
					b_num.set(1, m1.group(3)); // $3;
					b_num.set(2, m1.group(5)); // $5;
System.out.println("m1: "+m2.group(1) +" : " + m2.group(2));
				} else if (m2.find()){
					c_text.add(m2.group(2)); // $2;
					c_text.add(m2.group(4)); // $4;
					b_num.set(0, m2.group(1)); // $1;
					b_num.set(1, m2.group(3)); // $3;
System.out.println("m2" +m2.group(1) +" : " + m2.group(2));
				} else if (m3.find()) {
					c_text.add(m1.group(2)); // $2;
					c_text.add(m1.group(4)); // $4;
					c_text.add(m1.group(6)); // $6;
					b_num.set(0, m1.group(1)); // $1;
					b_num.set(1, m1.group(3)); // $3;
					b_num.set(2, m1.group(5)); // $5;
System.out.println("m3" +m2.group(1) +" : " + m2.group(2));
			    } else if (m4.find()) {
			    	c_text.add(m2.group(2)); // $2;
					c_text.add(m2.group(4)); // $4;
					b_num.set(0, m2.group(1)); // $1;
					b_num.set(1, m2.group(3)); // $3;
System.out.println("m4: "+m2.group(1) +" : " + m2.group(2));
			    }
			} else if (m20.find()) {
				c_text.add(m20.group(1)); // $1;
				c_text.add(m20.group(2)); // $2;
				c_text.add(m20.group(3)); // $3;
			} else if (m30.find()) {
				c_text.add(m30.group(1)); // $1;
				c_text.add(m30.group(2)); // $2;
			}

			for(int i = 0; i < c_text.size(); i++){
				List<String> c_text_2 = getCanText(c_text.get(i));
				for(String can : c_text_2) {
					if (can == null) {
System.out.println("can is null : skip");
						continue;
					}
System.out.println("test b_num["+ i + "] : " + can + ": " + c_num);
					String choice = b_num.get(i);
					if (!choices.containsKey(choice)) {
						choices.put(choice, new HashMap<String, String>());
					}
					Map<String, String> choiMap = choices.get(choice);
					if (choices.get(choice).containsKey(can)) {
						if (choiMap.containsKey(can)) {
							String tmp = choices.get(choice).get(can);
							choices.get(choice).put(can, tmp + ":" + c_num);
						} else {
							choices.get(choice).put(can, c_num);
						}
					}
			    }
			}
		}
		return choices;
	}

	/**
	 * 選択肢の単語のカッコを処理したり、同義語をとる
	 */
	private List<String> getCanText(String c_text) {

		List<String> c_temp= new ArrayList<String>();
		String  c_line = "";

		c_text = c_text.replaceAll(" ","");
		c_text = c_text.replaceAll("　","");
		c_text = c_text.replaceAll("「|」|『|』","");
		c_text = zen2han(c_text);

		c_temp.add(c_text);
		c_line = c_text;

		Pattern pat = Pattern.compile("\\((.+)\\)");
		Matcher matcher = pat.matcher(c_text);
		if (matcher.find()) {	//	    if (c_text =~ /\((.+)\)/){
			String c_tmp_2 = matcher.group(1); // $1;	// マッチした箇所？
			String c_tmp_3 = c_text.substring(0, matcher.start()); // $`; // マッチ前
			String c_tmp_1 = c_text;

			c_tmp_1 = c_tmp_1.replaceAll("\\(.+\\)","");
			c_temp.add(c_tmp_1);
			c_line += ("," + c_tmp_1);

			c_tmp_1 = c_tmp_1.replaceAll(c_tmp_3, c_tmp_2);
			c_temp.add(c_tmp_1);
			c_line += ("," + c_tmp_1);
		}

		int  db_flag = 0;
		String text_temp = "";
		List<String> can = new ArrayList<String>();
		for (String text : c_temp) {
			text_temp = "";
			db_flag = 0;
			can.add(text);

			String wdbText = wdb.get(text);
			if (wdbText != null) {
				if ((!wdbText.contains("Category")) && (!c_line.contains(wdbText))) { // "!~" 含まない場合
					can.add(wdbText);
					db_flag = 1;
					// System.out.println(wdbText);
				}
			}

			if (text.contains("＝")) {
				text_temp = text;
				text = text.replaceAll("＝","・");

				if(!(db_flag == 1 && text.equals(wdb.get(text_temp)))){
					can.add(text);
				}
				if (wdb.containsKey(text)){
					String val = wdb.get(text);
					if (!val.contains("Category") && !c_line.contains(val)) {
						can.add(val);
					}
				}
			}
		}
	    return can;
	}

	/**
	 * get_answer_tf+get_answer_tf_underline
	 * (波線部の)正しい(間違った)選択肢を選ぶ問に対応
	 * 各選択肢にスコアをつけて、スコアが最も高い(低い)選択肢を選ぶ
	 * 質問文に対する想定解が解候補にあったら、解候補の順位の逆数をスコアに加える
	 */
	public int getAnswerTf(CeQuestion question, Map<String, Map<Integer, CeAnswer>> qa_ans) {
		Map<String, String> choices = question.getChoice();
		Map<String, String> sup_ans = question.getSupAns();
		
		int choice = 0;
		Map<Integer, Double> can_choi = new HashMap<Integer, Double>();
		can_choi.put(1, 0.0);
		can_choi.put(2, 0.0);
		can_choi.put(3, 0.0);
		can_choi.put(4, 0.0);

		String  c_text = "";
		int  	c_id   = 0;
		double  c_zen  = 0;
		double  c_kou  = 0;

		// 各選択肢に対する質問数を出しておく
		Map<Integer, Integer> q_num = new HashMap<Integer, Integer>();	// 質問数Map
		List<String> qakey = new ArrayList<String>(qa_ans.keySet());
		Collections.sort(qakey);
		for (String q_id : qakey) {
System.out.println("Qtype="+question.getQuestionType()+" qa_ans: qid="+q_id);			
			if (question.getQuestionType().equals("TF")){
				Pattern p = Pattern.compile("(.+)_(.+)");
				Matcher m = p.matcher(q_id);
				if (m.find()) {
//System.out.println("(q_num) hit:"+m.group(1)+"=>"+m.group(2));					
					q_num.put(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)));	// {$1} = $2;
				}
			} else if (question.getQuestionType().equals("TF-underline")) {
				q_num.put(Integer.parseInt(q_id), 0);
//System.out.println("(q_num) hit:"+q_id+"=> 0");					
			}
		}

		// ここから本番、想定解が取れているかどうかチェック
		// 想定解の順位の逆数がスコア
System.out.println("/// check start ///");			
		for (String q_id : qa_ans.keySet()) {
System.out.println("qid="+q_id);
			if (question.getQuestionType().equals("TF")) {
				Pattern p = Pattern.compile("(.+)_.+");
				Matcher m = p.matcher(q_id);
				if (m.find()) {
					String c_id_str = m.group(1);
					c_id = Integer.parseInt(c_id_str);
System.out.println("1:c_id="+c_id);
				}
				c_text = sup_ans.get(q_id);
			} else if(question.getQuestionType().equals("TF-underline")){
				Pattern p = Pattern.compile(".+\\s*:\\s*(.+)");
				Matcher m = p.matcher(choices.get(q_id));
				if (m.find()) {
					c_text = m.group(1); // $1;
					c_id = Integer.parseInt(q_id);
System.out.println("2:c_id="+c_id);
			    }
			}
			c_zen = can_choi.get(c_id);
System.out.println("c_zen="+c_zen+" c_text="+c_text);
			can_choi = compAnswer(qa_ans, q_id, c_id, c_text, "eq", can_choi);
			c_kou = can_choi.get(c_id);
System.out.println("c_kou="+c_kou);
			if(c_zen == c_kou) {
System.out.println("compAnswer 2");
				can_choi = compAnswer(qa_ans, q_id, c_id, c_text, "seiki", can_choi);
			}
	    }

		
		// スコアを質問数で割る
		for (int c_num : can_choi.keySet()) {
			double qnum = 0.0;
			double n = 0.0;
			if (q_num.containsKey(c_num))
				n = (q_num.get(c_num) + 1.0);
			if (can_choi.containsKey(c_num))
				qnum = can_choi.get(c_num) / n;
			can_choi.put(c_num, qnum);
		}
		
		// 各選択肢のスコアを質問数で割る
/*		
		for (int c_num : can_choi.keySet()) {
System.out.println("score c_num:"+c_num);			
			if (!q_num.containsKey(c_num)) {
				System.err.println("q_num("+c_num+") non exist?");
			}
			double qnum = (double)q_num.get(c_num);
			double nnum = can_choi.get(c_num) / (qnum + 1.0);
System.out.println("can_choi.get("+c_num+")="+can_choi.get(c_num)+"/"+(qnum+1));
			can_choi.put(c_num, nnum);
		}
*/
		// スコア順で並び替えて選択肢を選ぶ
		boolean not_flag = false;
		if (question.getQLine().matches("誤っている|誤りを含む")) {
			not_flag = true;
		}
		List<Entry<Integer, Double>> canChoiList = new ArrayList<Entry<Integer,Double>>(can_choi.entrySet());
		Collections.sort(canChoiList, new CompareChoiNum());
		if (!not_flag)
			Collections.reverse(canChoiList);
		
		boolean c_flag = false;
		for (Entry<Integer, Double> can_choi_num : canChoiList) {
			int c_num = can_choi_num.getKey();
			double score = can_choi_num.getValue();
			if (!c_flag) {
				choice = c_num;
				question.setScore(score);
			}
			c_flag = true;
System.out.println("getAnswerTf:"+c_num + " : " + can_choi.get(c_num));
		}
		return choice;
	}

	/**
	 * 正誤の組み合わせを選ぶ問
 	 * 質問文に対する想定解が取れていたら、想定解の逆数をその文の「正文らしさのスコア」とする
	 * 正文らしさのスコア」が閾値以上なら正文、閾値以下なら誤文と判断して正誤の組み合わせを選ぶ
	 */
	public int getAnswerTfComb(CeQuestion question, Map<String, Map<Integer, CeAnswer>> qa_ans) {

		Map<String, String> choices = question.getChoice();
		Map<String, String> sup_ans = question.getSupAns();

		Map<String, Integer> q_num = new HashMap<String, Integer>();

		// 各文章から作った質問数を出しておく（後で質問数で割る）
		List<String> q_id_list = new ArrayList<String>(qa_ans.keySet());
		Collections.sort(q_id_list);

		Pattern p = Pattern.compile("(.+)_(.+)");
		for(String q_id : q_id_list) {
			Matcher m = p.matcher(q_id);
			if(m.find()) {
				q_num.put(m.group(1), Integer.parseInt(m.group(2)));
			}
		}

//System.err.println("in");
		int c_id = 0;
		// 想定解が取れているかチェック
		// 想定解の順位の逆数が正誤のスコア
		Map<Integer, Double> can_choi = new HashMap<Integer, Double>();
		Pattern p2 = Pattern.compile("(.+)_.+");
		for (String q_id : qa_ans.keySet()) {
			Matcher m = p2.matcher(q_id);
			if (m.find()) {
				String c_id_str = m.group(1);
System.out.println("c_id_str:"+c_id_str);
				c_id_str = c_id_str.replaceAll("L|U|D","");
				c_id = Integer.parseInt(c_id_str);
				if(!can_choi.containsKey(c_id)) {
					can_choi.put(c_id, 0.0);
				}
			}
			String c_text = sup_ans.get(q_id);
			double c_zen = can_choi.get(c_id);

			can_choi = compAnswer(qa_ans, q_id, c_id, c_text, "eq", can_choi);
			double c_kou = can_choi.get(c_id);
			if(c_zen == c_kou){
				can_choi = compAnswer(qa_ans, q_id, c_id, c_text, "seiki", can_choi);
			}
		}

		int  choice = 0;
		String choi_text = "";
		// スコアを質問数で割る
		for (int c_num : can_choi.keySet()) {
			double qnum = 0.0;
			double n = 0.0;
			if (q_num.containsKey(c_num))
				n = (q_num.get(c_num) + 1);
			if (can_choi.containsKey(c_num))
				qnum = can_choi.get(c_num) / n;
			can_choi.put(c_num, qnum);
		}

		if (question.getQuestionType().equals("TF-comb")) {
			// 各文章の正誤を出して、選択肢を決める
			// 日本史の場合、３つの正誤の組み合わせを選ぶが、選択肢にない組み合わせがある、どうするか
			List<Integer> can_choi_list = new ArrayList<Integer>(can_choi.keySet());
			Collections.sort(can_choi_list);

			double th = 0.01; //正誤の組み合わせを選ぶ際の、正か誤かの閾値
			for (int c_num : can_choi_list) {
				System.out.println(c_num+" : "+can_choi.get(c_num));
				choi_text += c_num;
				if (can_choi.get(c_num) > th) {
					choi_text += "正";
				} else{
					choi_text += "誤";
				}
			}
System.out.println(choi_text);

			for (String c_num : choices.keySet()) {
				String tmp = c_num;
				tmp = tmp.replaceAll("-+", "");
				tmp = tmp.replace("　", "");
				tmp = tmp.replace(" ", "");
				//choices.put(c_num, tmp);

				System.out.println(c_num + " : " + tmp);
				if(choi_text.equals(tmp)) {
					choice = Integer.parseInt(c_num);
					question.setScore(can_choi.get(c_num));
				}
			}
		} else if (question.getQuestionType().equals("T-comb")) {
			int c_t_1 = 0;
			int c_t_2 = 0;
			int c_flag = 0;
			// 正しい文章の組み合わせを選ぶ
			// １文目or２文目+３文目or４文目の組み合わせが選択肢になっているようなので、それにあわせる
			List<Integer> can_choi_list = new ArrayList<Integer>(can_choi.keySet());
			Collections.sort(can_choi_list);
			for (int c_num : can_choi_list) {
				System.out.println(c_num+" : "+can_choi.get(c_num));
				c_flag++;

				if (c_flag == 1 || c_flag == 3) {
					c_t_1 = c_num;
				} else if (c_flag == 2 || c_flag == 4){
					c_t_2 =  c_num;

					if (can_choi.get(c_t_1) > can_choi.get(c_t_2)) {
						choi_text += c_t_1;
					} else {
						choi_text += c_t_2;
					}
				}
			}
			System.out.println(choi_text);
			for (String c_num : choices.keySet()) {
				String tmp = c_num;
				tmp = tmp.replaceAll("・", "");
				tmp = tmp.replace("　", "");
				tmp = tmp.replace(" ", "");

				System.out.println(c_num+" : " + tmp);
				if (choi_text.equals(tmp)){
					choice = Integer.parseInt(c_num);
					question.setScore(can_choi.get(c_num));
	  		    }
	  		}
		}
		return choice;
	}


	/**
	 *
	 */
	private Map<Integer, Double> compAnswer(Map<String, Map<Integer,CeAnswer>> qa_ans, String q_id, 
			int c_id, String c_text, String comp, Map<Integer, Double>can_choi) {

		if (c_text == null) {
			return can_choi;
		}
		
		String  answer_text = "";
		String  y_zen = "";
		double w_comp = 0.5;	// $compがeqでないときに掛ける重み

		Pattern p1 = Pattern.compile("(\\d+)(・|～)(\\d+)世紀$");
		Pattern p2 = Pattern.compile("(前)*(\\d+)世紀$");
		Pattern p3 = Pattern.compile("(\\d+)世紀(前|後)半");
		Pattern p4 = Pattern.compile("(\\d+)年代");

		Matcher m4 = p4.matcher(c_text);

		boolean y_flag = false;
		int y_small = 0;
		int y_big = 0;
		if (c_text.contains("世紀")) {
			y_flag = true;
			Matcher m1 = p1.matcher(c_text);
			Matcher m2 = p2.matcher(c_text);
			Matcher m3 = p3.matcher(c_text);
			if (m1.find()) {
				y_big = Integer.parseInt(m1.group(3));	//$3;
				y_small = Integer.parseInt(m1.group(1));	// $1;
				y_big *= 100;
				y_big--;
				y_small = (y_small-1) * 100;
System.out.println(q_id + " : " + c_text + " : " + y_small + " - " + y_big);
			} else if (m2.find()) {
				y_zen = m2.group(1);	// $1;
				y_big = Integer.parseInt(m2.group(2));	// $2;
				y_big *= 100;
				if (y_zen.equals("前")) {
					y_big *= (-1);
				}
				y_small =  y_big - 100;
				y_big--;
System.out.println(q_id + " : " + c_text + " : " + y_small + " - " + y_big);
			} else if (m3.find()) {
				y_big = Integer.parseInt(m3.group(1));	// $1;
				y_zen = m3.group(2);	// $2;
				if (y_zen.equals("前")) {
					y_big *= 100;
					y_small = y_big - 100;
					y_big = y_big - 51;
				} else {
					y_big *= 100;
					y_small = y_big - 50;
					y_big--;
				}
System.out.println(q_id + " : " + c_text + " : " + y_small + " - " + y_big);
			}
		} else if (m4.find()) {
			y_flag = true;
			y_small = Integer.parseInt(m4.group(1)); // $1;
			y_big = y_small + 9;
System.out.println(q_id + " : " + c_text + " : " + y_small + " - " + y_big);
		}
		List<String> can = getCanText(c_text);

System.out.println("<CanText>");		
for(String s : can) {		
System.out.println(q_id+" : "+s);
}
		Pattern p5 = Pattern.compile("(前)*(\\d+)年");

		Map<Integer, CeAnswer> answerMap = qa_ans.get(q_id);
		List<Entry<Integer, CeAnswer>> entrylist = new ArrayList<Entry<Integer, CeAnswer>>(answerMap.entrySet());
		Collections.sort(entrylist, new CompareAnswerRank());
//		foreach my $a_num (sort {$qa_ans->{$q_id}->{$a}->{'rank'} <=> $qa_ans->{$q_id}->{$b}->{'rank'}} keys %{$qa_ans->{$q_id}}) {
		for (Entry<Integer, CeAnswer> entry : entrylist) {
			CeAnswer a = entry.getValue();
			//print "ans : $qa_ans->{$q_id}->{$a_num}->{'text'}\n";
			answer_text = a.getText();
			answer_text = zen2han(answer_text);
			double rank = (double)a.getRank();
			double dr = ((double)1.0) / rank;
			double wdr = w_comp / rank;
System.out.println("answer="+answer_text+" rank="+rank+ " dr="+dr);
			Matcher m5 = p5.matcher(answer_text);
			if (y_flag && m5.find()) {
				y_zen = m5.group(1); // $1;
				answer_text = m5.group(2); // $2;
				int answer_num = Integer.parseInt(answer_text);
				if (y_zen.equals("前")) {
					answer_text = Integer.toString(Integer.parseInt(answer_text) * -1);
				}
				if (y_small <= answer_num && y_big >= answer_num) {
					double dcan = can_choi.get(c_id);
					can_choi.put(c_id, dcan + dr);
System.out.println("OK:"+q_id + " : " + answer_text + " : rank=" + rank);
					break; // ひとつ見つかったらとりあえずokにする
				}
			} else {
				boolean ok_flag = false;
				for (String c_word : can) {
					if (ok_flag) {
						break;
					} else if(comp.equals("eq")) {
						if (c_word.equals(answer_text)) {
							double dcan = can_choi.get(c_id);
							can_choi.put(c_id, dcan + dr);
							ok_flag = true;
System.out.println("OK:"+q_id + " : " + c_word + " : " + answer_text + " : rank=" + rank);
						} else if (wdb.containsKey(answer_text) && c_word.equals(wdb.get(answer_text))) {
							double dcan = can_choi.get(c_id);
							
							can_choi.put(c_id, dcan + dr);
							ok_flag = true;
System.out.println("OK:"+q_id + " : " + c_word + " : " + answer_text + " : " + wdb.get(answer_text)+" : w rank="+rank);
						}
					} else {
						if (c_word.contains(answer_text) || answer_text.contains(c_word)) {
							double dcan = can_choi.get(c_id);
							can_choi.put(c_id, dcan + wdr);
							ok_flag = true;
System.out.println("OK:"+q_id + " : " + c_word + " : " + answer_text + " : p rank="+ rank);
						} else if((wdb.containsKey(answer_text) && !wdb.get(answer_text).contains("Category")) &&
								(c_word.contains(wdb.get(answer_text)) || (wdb.get(answer_text).contains(c_text)))) {
							double dcan = can_choi.get(c_id);
							can_choi.put(c_id, dcan + wdr);
							ok_flag = true;
System.out.println("OK:"+q_id+" : "+c_word+" : "+answer_text+" : "+wdb.get(answer_text)+" : pw rank="+rank);
						}
					}
		    	}
//				if (ok_flag == false)
//					System.out.println("   no "+comp+" ans");
			}
    	}
    	return can_choi;
	}



	/**
	 * 年代順に並び替える問
	 * 最上位の「○年」という解候補の年代のみを使って並び替えを行って、同じ並びの選択肢を選ぶ
	 */
	public int getAnswerOrder(CeQuestion question, Map<String, Map<Integer, CeAnswer>> qa_ans) {
		String answer_text = "";
		Map<String, String> choices = question.getChoice();
		Map<String, Integer> can_choi = new HashMap<String, Integer>();

		for (String q_id : qa_ans.keySet()) {
			// とりあえず最上位の(●年)の年代のみを使う
			Map<Integer, CeAnswer> answerMap = qa_ans.get(q_id);
			List<Entry<Integer, CeAnswer>> entrylist = new ArrayList<Entry<Integer, CeAnswer>>(answerMap.entrySet());
			Collections.sort(entrylist, new CompareAnswerRank());
			for (Entry<Integer, CeAnswer> entry : entrylist) {
				CeAnswer a = entry.getValue();

				answer_text = a.getText();
System.out.println("getAnswerOrder:ans: "+answer_text);
				answer_text = zen2han(answer_text);

				if (answer_text.matches("年$")) {
					answer_text = answer_text.replace("年","");
					can_choi.put(q_id, Integer.parseInt(answer_text));
					break;
				}
			}
		}

		List<Entry<String, Integer>> canChoiList = new ArrayList<Entry<String,Integer>>(can_choi.entrySet());
		Collections.sort(canChoiList, new CompareCanChoi());
		Collections.reverse(canChoiList);
		
		String choi_text = "";
		for (Entry<String, Integer> e : canChoiList) { //each my $q_id (sort {$can_choi{$a} <=> $can_choi{$b}} keys %can_choi) {
			String q_id = e.getKey();
System.out.println("getAnswerOrder:canChoiList: "+ q_id+" : "+can_choi.get(q_id));
			choi_text += q_id;
		}

		int choice = 0;
System.out.println("getAnswerOrder:choi_text="+choi_text);
		for (String c_num  : choices.keySet()) {
			String tmp = choices.get(c_num);
			tmp = tmp.replaceAll("→", "");
			tmp = tmp.replaceAll("-", "");
			tmp = tmp.replaceAll("－", "");
			tmp = tmp.replaceAll("　", "");
			tmp = tmp.replaceAll(" ", "");

System.out.println("getAnswerOrder: "+c_num + " : " + tmp);
			if (choi_text.equals(tmp)) {
				choice = Integer.parseInt(c_num);
			}
	    }
	    return choice;
	}

	/**
	 * 全角数字、全角アルファベットを半角に変換
	 */
	private String zen2han(String sent) {
		StringBuffer sb = new StringBuffer(sent);
		for (int i = 0; i < sb.length(); i++) {
			char c = sb.charAt(i);
			if (c >= 'ａ' && c <= 'ｚ') {
				sb.setCharAt(i, (char) (c - 'ａ' + 'a'));
			} else if (c >= 'Ａ' && c <= 'Ｚ') {
				sb.setCharAt(i, (char) (c - 'Ａ' + 'A'));
			} else if (c >= '0' && c <= '9') {
				sb.setCharAt(i, (char) (c - '0' + '０'));
			}
		}
		return sb.toString();
	}

}
