package org.kachako.components.analysis_engine.module;


/* QAシステムの結果ファイルから読み込む情報
 * (このプログラムでは、結果ファイルdoc*をxmlファイルとして読み込んで解析している)
 * ($target_q_id:"xmlファイル名-空欄ID")
 * ($target_num:"空欄IDごとの質問文ID")
 * $qa_ans{q_id}->{a_num}->{$score_sofa_id}->{text}: 解候補の文字列
 * $qa_ans{q_id}->{a_num}->{$score_sofa_id}->{rank}: 解候補の順位
 * $qa_ans{q_id}->{a_num}->{$score_sofa_id}->{score}: 解候補のスコア(未使用)
 * 
 * Map<String target_q_id, Map<String target_num, Map<score_sofa_id,Answer>>>
 * Map<xmlファイル名-空欄ID, Map<空欄IDごとの質問文ID, Map<score_sofa_id, Answer>>>
 * 
 */
public class CeAnswer {

	String text;	// 階候補の文字列
	int rank;		// 階候補の順位
	double score;	// 階候補のスコア(未使用)
	
	public CeAnswer(String text, int rank, double score) {
		this.text = text;
		this.rank = rank;
		this.score = score;
	}
	
	public int getRank() {
		return rank;
	}
	
	public void setRank(int rank) {
		this.rank = rank;
	}

	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public double getScore() {
		return score;
	}
	
	public void setScore(double score) {
		this.score = score;
	}
	
}
