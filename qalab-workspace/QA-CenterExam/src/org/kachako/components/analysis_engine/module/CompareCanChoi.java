package org.kachako.components.analysis_engine.module;

import java.util.Comparator;
import java.util.Map.Entry;

public class CompareCanChoi implements Comparator<Entry<String, Integer>> {

	public CompareCanChoi() {
		
	}

	public int compare(Entry<String,Integer> o1, Entry<String,Integer> o2) {
		Entry<String,Integer> e1 = o1;
		Entry<String,Integer> e2 = o2;

		int score1 = (int) e1.getValue();
		int score2 = (int) e2.getValue();
		
		if (score1 > score2)
			return 1;
		else if (score1 == score2)
			return 0;
		else
			return -1;
	}

}
