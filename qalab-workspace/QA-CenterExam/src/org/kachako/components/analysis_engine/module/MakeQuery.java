package org.kachako.components.analysis_engine.module;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import com.cignoir.cabocha.Cabocha;
import com.cignoir.node.Chunk;
import com.cignoir.node.Sentence;
import com.cignoir.node.Token;


public class MakeQuery {

	private final static String DBNAMEW1 = "DB-WIKI-RED";
	private final static String DBNAMEW2 = "DB-WIKI-PERSON";
	private final static String DBNAMEY1 = "CENTER-YOGO-DB";
	private final static String DBNAMEY2 = "TOKYOSHOSEKI-YOGO-DB";

	private DB dbw1;
	private DB dbw2;
	private DB dby1;
	private DB dby2;
	private ConcurrentNavigableMap<Integer,String> wdb1;
	private ConcurrentNavigableMap<Integer,String> wdb2;
	private ConcurrentNavigableMap<Integer,String> ydb1;
	private ConcurrentNavigableMap<Integer,String> ydb2;

	private Cabocha cabocha;

	public MakeQuery(String dbFolder, String cabochaPath) {

		dbw1 = DBMaker.newFileDB(new File(Paths.get(dbFolder).resolve(DBNAMEW1).toUri()))
				.closeOnJvmShutdown().readOnly().make();
		dbw2 = DBMaker.newFileDB(new File(Paths.get(dbFolder).resolve(DBNAMEW2).toUri()))
				.closeOnJvmShutdown().readOnly().make();
		dby1 = DBMaker.newFileDB(new File(Paths.get(dbFolder).resolve(DBNAMEY1).toUri()))
				.closeOnJvmShutdown().readOnly().make();	// 開く際のモード("O_RDONLY"など)を指定する際に使用
		dby2 = DBMaker.newFileDB(new File(Paths.get(dbFolder).resolve(DBNAMEY2).toUri()))
				.closeOnJvmShutdown().readOnly().make();	// 開く際のモード("O_RDONLY"など)を指定する際に使用
		
		wdb1 = dbw1.getTreeMap(DBNAMEW1);
		wdb2 = dbw2.getTreeMap(DBNAMEW2);
		ydb1 = dby1.getTreeMap(DBNAMEY1);
		ydb2 = dby2.getTreeMap(DBNAMEY2);
		
		cabocha = new Cabocha(cabochaPath);
	}


	/**
	 * 選択肢が単語で、穴埋め以外の問
	 * q_line:「下線部○○について、△△について正しいものを1～4から選べ。」などの指示文から質問を生成
	 */
	public Map<String, String> makeQueryFactoid(CeQuestion question) {

		String query = question.getQLine();
		String  q_word	= "何";

		// System.out.println(query);
		query = query.replaceAll("^同じく", "");

		List<String[]> morpheme = useCabocha(query);
		boolean flag_1 = false;
		boolean flag_2 = false;

		Map<String, String> ref_utext = question.getRefUText();
		for (String u_id : ref_utext.keySet()) {
			String change	= "";
			String change_2	= "";
			if (ref_utext.get(u_id).matches("，|。|、|．")) {
				if (query.contains("下線部" + u_id)) {
					change = "下線部" + u_id;
				} else {
					change = u_id;
				}
				change_2= "「" + ref_utext.get(u_id) +  "」";
			} else {
				if (query.contains(u_id+"の")) {
					for(String[] mor : morpheme) {
						// System.err.println(morpheme.get(i)[0]);
						// System.err.println(morpheme.get(i)[1]);
						// System.err.println(morpheme.get(i)[2]);
						if (mor[0].equals("*")) {
							continue;
						} else if(mor[0].equals("下線")) {
							flag_1 = true;
							change += mor[0];
						} else if(flag_1) {
							change += mor[0];
							if (mor[0].equals("の")) {
								flag_2 = true;
								flag_1 = false;
							}
						} else if(flag_2) {
							if(mor[1].matches("名詞|記号") && !mor[2].contains("非自立")) {
								change += mor[0];
							} else {
								break;
							}
						}
					}
				} else {
					change = "下線部" + u_id;
				}
				change_2 = ref_utext.get(u_id);
			}
System.out.println("makeQueryFactoid: "+change+":"+change_2);
			query = query.replace(change, change_2);
		}

		int n = query.indexOf("に関連して，");
		if (n >= 0) {
			if (!query.matches("に関連して，(この|当時)")) {
				query = query.substring(n + "に関連して，".length());
			}
		}

		Pattern p = Pattern.compile("として(，)?(最も)?(正しい|誤って|誤った|適当な)|を，次の|。次の");
		Matcher m = p.matcher(query);
		
		if (m.find()) {
			query = query.substring(m.end());
		}
		// 途中にある場合
		if (query.contains("次の①～④のうちから，")) {
			query = query.replace("次の①～④のうちから，","");
		}

		if (question.getAnswerType().equals("term_location")) {
			q_word = "どこ";
		} else if (question.getAnswerType().equals("term_person")) {
			q_word = "誰";
		} else if (question.getAnswerType().equals("term_time")) {
			q_word = "いつ";
		}
		query = query.replaceAll("の名$","");
		query = query.replace("\\(.+?\\)","");

		if (!query.contains("組合せ|組み合わせ") && !query.matches("何か$|どれか$|だれか$|誰か$") && !query.contains("何と")) {
			query = query + "は" + q_word + "ですか。";
		}
		
		Map<String, String> querys = new HashMap<String, String>();
		querys.put("0", query);
		return(querys);
	}

	/**
	 * 空欄に入る語を選ぶ問(空欄の数は一つ)
	 * 空欄が入っている文から質問を作る(空欄が二か所以上あったら、空欄の箇所の数だけ質問を作る)
	 * 実際の処理は、サブルーチン blankQuery
	 * ここでは、質問文作成の際、疑問詞を決める為の情報を抽出している
	 */
	public Map<String, String> makeQueryBlank(CeQuestion question) {
		
		String q_line = question.getQLine();
		Map<String, String> ref_utext = question.getRefUText();
		boolean flag_1 = false;
		String focus_word = "";

//	=get_focus
		List<String[]> mor_qline = useCabocha(q_line);

		for (String[] mor : mor_qline) {
			// System.err.println(mor_qline.get(i)[0]);
			// System.err.println(mor_qline.get(i)[1]);
			// System.err.println(mor_qline.get(i)[2]);
			if(mor[0].equals("*")) {
				continue;
			} else if(mor[0].equals("入れる") || mor[0].equals("入る")) {
				flag_1 = true;
			} else if (flag_1) {
				if(mor[1].contains("名詞")) {
					focus_word += mor[0];
			    } else{
			    	flag_1 = false;
			    }
			}
		}
System.out.println("makeQueryBlank: "+ q_line + " : " + focus_word);
//	=cut

		Map<String, String> querys = null; // new HashMap<Integer, String>();
		// とりあえず、空欄が入っている文からそのまま質問文を作る
		List<String> refutexts = new ArrayList<String>(ref_utext.keySet());
		Collections.sort(refutexts);
		for (String u_id : refutexts) {
			if (u_id.contains("B")) {
				querys = blankQuery(u_id, ref_utext.get(u_id), question.getAnswerType(), focus_word);
			}
		}
		return querys;

	}

	/**
	 * 疑問詞を作る際に使う接尾語を決める
	 */
	private String getSetubi(String focus_word) {

		String q_word_setubi="";

	    if(focus_word.contains("宗派")){
	    	q_word_setubi="派";
	    } else if(focus_word.contains("言語")) {
	    	q_word_setubi="語";
	    } else if(focus_word.contains("宗教")) {
	    	q_word_setubi="教";
	    } else if(focus_word.contains("条約")) {
	    	q_word_setubi="条約";
	    } else if(focus_word.contains("王朝")) {
	    	q_word_setubi="朝";
	    	// 入れていいのかどうか？
	    }
	    return(q_word_setubi);
	}

	/**
	 * 空欄が入った文から質問文を作る
	 */
	private Map<String, String> blankQuery(String u_id, String text, String answertype, String focus_word) {

		String q_word = "何";
		String q_word_setubi = "";
		if (answertype.contains("term_location")) {
			q_word = "どこ";
		} else if (answertype.contains("term_person")) {
			q_word = "誰";
		} else if(answertype.contains("term_time")) {
			q_word = "いつ";
		} else {
			q_word_setubi = getSetubi(focus_word);
		}

		// System.err.println(u_id + " : " + q_word + " : " + q_word_setubi + " : " + focus_word);

		Map<String, String> querys = new HashMap<String, String>();
		String [] lines = text.split("。");
		int  q_num = 0;
		for (String sent : lines) {
			sent = sent.replaceAll(" ","");
			sent = sent.replaceAll("　","");	
			if (sent.contains("|target|")) {
				String query = sent;
				if (q_word_setubi.length() > 0) {
					if(sent.contains("|target|" + q_word_setubi)) {
						query = query.replace("|target|", q_word);
					} else {
						query = query.replace("|target|",q_word+q_word_setubi);
					}
				} else {
					query = query.replace("|target|", q_word);
				}
				query += "か。";
				query = query.replaceAll("^(また|しかし|そして|一方|他方|そこで|その後)，","");
				query = query.replaceAll("\\(.+?\\)", "");
				query = query.replaceAll("\\|.+?\\|", "あるもの");
		
				String q_id = u_id + "_" + q_num;
				q_num++;
				querys.put(q_id, query);
			}
		}
		return querys;
	}

	/**
	 * 空欄に入る語の組み合わせを選ぶ問
	 * 空欄の数は、2個以上
	 * 質問文作成の実際の処理は、サブルーチン blankQuery
	 * ここでは、質問文作成の際、疑問詞を決める為の情報を抽出している
	 */
	public Map<String, String> makeQueryBlankComb(CeQuestion question) {

		String q_line = question.getQLine();
		String a_type = question.getAnswerType();
		Map<String, String> ref_utext = question.getRefUText();
		List<String> answertype = new ArrayList<String>();
		Map<String, String> querys = new HashMap<String, String>();
		Map<String, String> new_querys = new HashMap<String, String>();
		int flag_1 = 0;
		int  b_num = 0;

		Pattern p1 = Pattern.compile("\\((.+?)\\)\\*(\\d+)");
		Matcher m1 = p1.matcher(a_type);
		Pattern p3 = Pattern.compile("(.+)-(.+)-(.+)");
		Matcher m3 = p3.matcher(a_type);
		Pattern p4 = Pattern.compile("(.+)-(.+)");
		Matcher m4 = p4.matcher(a_type);
		Pattern p5 = Pattern.compile("[B1234567890]");
		Pattern p6 = Pattern.compile("\\(.+\\)");
		Matcher m6 = p6.matcher(a_type);

		if(m1.find()) {
			String btype = m1.group(1); 				// $1;
			b_num = Integer.parseInt(m1.group(2));	// $2;
			for (int i = 0; i < b_num; i++) {
				answertype.add(btype);
			}
		} else if (m6.find()) {
			String  tmp_type = a_type;
			Pattern p2 = Pattern.compile("\\((.+?)\\)");
			Matcher m2 = p2.matcher(tmp_type);
			while(m2.find()) {
				answertype.add(m2.group(1));
				tmp_type = tmp_type.substring(m2.end()); // #';
				m2 = p2.matcher(tmp_type);
				b_num++;
			}
		} else if(m3.find()) {
			answertype.add(m3.group(1)); 
			answertype.add(m3.group(2)); 
			answertype.add(m3.group(3)); 
			b_num = 3;
		} else if(m4.find()) {
			answertype.add(m4.group(1)); 
			answertype.add(m4.group(2)); 
			b_num = 2;
		}
		System.out.println(a_type + " : " + answertype);

//=get_focus
		List<String[]> mor_qline = useCabocha(q_line);
		List<String> focus_word = new ArrayList<String>();
		int  j = 0;
		for (String[] mor : mor_qline) {
// System.out.println("mor[0]="+mor[0]);
// System.out.println("mor[1]="+mor[1]);
// System.out.println("mor[2]="+mor[2]);
			if (mor[0].equals("*")) {
				continue;
			} else if (mor[0].equals("入れる") || mor[0].equals("入る")) {
				flag_1 = 1;
			} else if (flag_1 == 1) {
				Matcher m5 = p5.matcher(mor[0]);
				if(mor[1].contains("名詞") && !m5.find()) {
					if (focus_word.size() == j)
						focus_word.add(mor[0]);
					else
						focus_word.set(j, focus_word.get(j) + mor[0]);
				} else {
					if(mor[0].equals("と")) {
						flag_1 = 1;
						j++;
					} else if(mor[0].equals("その")) {
						continue;
					} else {
						flag_1 = 2;
					}
				}
			} else if (flag_1 == 2) {
				if (mor[0].equals("と")) {
					flag_1 = 1;
					j++;
				} else if (mor[0].contains("組み合わせ") || mor[0].contains("組合せ") 
						|| mor[0].contains("組合わせ") || mor[0].contains("組み合せ")) {
					for (int k = 1; k < b_num; k++) {
						if (focus_word.size() <= k) {
							focus_word.add(focus_word.get(k-1));
						} else if (focus_word.get(k).equals("")) {
							focus_word.set(k, focus_word.get(k-1));
						}
					}
					flag_1 = 0;
				}
			}
		}
System.out.println("makeQueryBlankComb: "+"focus_word="+q_line + " : " + focus_word);
//=cut
		j = 0;
		// とりあえず、空欄が入っている文からそのまま質問文を作る
		List<String> u_id_list = new ArrayList<String>(ref_utext.keySet());
		Collections.sort(u_id_list);
		
		for (String u_id : u_id_list) {
			if(u_id.contains("B")) {
				String focusstr = "";
				String ansstr = "";
				if (j < focus_word.size())
					focusstr = focus_word.get(j);
				if (j < answertype.size())
					ansstr = answertype.get(j);
				new_querys = blankQuery(u_id, ref_utext.get(u_id), ansstr, focusstr);
				for (String q : new_querys.keySet()){
System.out.println("makeQueryBlankComb: " + q);
				}
				querys.putAll(new_querys);
				j++;
			}
		}
		return querys;
	}

	/**
	 * 選択肢の中から、波線部の内容が正しい(間違った)ものを選ぶ問
	 * 波線部の部分を疑問詞に置き換えて質問文を作成
	 */
	public Map<String, String> makeQueryTfUnderline(CeQuestion question) {

		Map<String, String> querys = new HashMap<String, String>();
		Map<String, String> choices = question.getChoice();
		String line = "";
		String  word = "";
		String  type = "";
		String  q_word = "何";

		Pattern p = Pattern.compile("(.+)\\s:\\s(.+)/");
		List<String> keylist = new ArrayList<String>(choices.keySet());
		Collections.sort(keylist);
		for (String key : keylist) {
			String choice_str = choices.get(key);
			Matcher m = p.matcher(choice_str);
			if (m.find()) {
			    line = m.group(1);
			    word = m.group(2);
			    // System.out.println(line + "\t" + word);
			}
			// System.out.println(word);
			word = normalize(word);
			line = normalize(line);
			type =  word2type(word);
			q_word = word2qword(word, type);
System.out.println("makeQueryTfUnderline: "+q_word);

			line = line.replaceAll(word, q_word);
			line = line.replaceAll("。", "か。");
			line = line.replaceAll("\\(.+?\\)", "");
			line = line.replaceAll("（.+?）", "");
			line = line.replaceAll("-", "～");
			querys.put(key,line);
	    }
	    return(querys);
	}

	/**
	 * make_query_tf and make_query_tf_comb
	 * 選択肢の中から正しい(間違った)ものを選ぶ問
	 * 2(3)つの文の正誤の組み合わせを選ぶ問質問文を作成すると同時に、想定解も保存しておく
	 *
	 * 選択肢などの文(平叙文)から、名詞などを疑問詞に置き換えて、置き換えた名詞を問う質問文を作る
	 * 例：
	 *「日本の首相は安倍晋三です」→
	 * 「どこの首相は安倍晋三ですか」(日本)
	 * 「日本の何は安倍晋三ですか」(首相)
	 * 「日本の首相は誰ですか」(安倍晋三)
	 * カッコの中が想定解(選択肢を選ぶ際に使用する)
	 */
	public void makeQueryTf(CeQuestion question, Map<String, String>sup_ans, Map<String, String> querys) {
		
		Map<String, String> choices = new HashMap<String, String>();

		if (question.getQuestionType().equals("TF")) {
			choices = question.getChoice();
		} else if (question.getQuestionType().equals("TF-comb") ||
				question.getQuestionType().equals("T-comb")) {
				// choices = question.getRefText();
			for (String key : question.getRefUText().keySet()){
				if (key.contains("L")) {
					choices.put(key, question.getRefUText(key));
				}
			}
		}

		// 手がかり語をとる
		String [] clues = getClue(question);
System.out.println("TF:"+clues[0]+" : "+clues[1]);
		makeInclueLine(clues, choices);

		// 質問文を作る
		List<String> keys = new ArrayList<String>(choices.keySet());
		Collections.sort(keys);
		Pattern p1 = Pattern.compile("\\(.+?\\)");
		Pattern p2 = Pattern.compile("（.+?）");
		for (String key : keys) {
			String line = choices.get(key);
			Matcher m1 = p1.matcher(line);
			if (m1.find()) {
				String t1 = line.substring(0, m1.start());
				String t2 = line.substring(m1.end());
				line = t1 + t2;
			}
			Matcher m2 = p2.matcher(line);
			if (m2.find()) {
				String t1 = line.substring(0, m2.start());
				String t2 = line.substring(m2.end());
				line = t1 + t2;
			}
			line = line.replaceAll("-","～");
			line = normalize(line);

			List<String[]>mor_choice = useCabocha(line);
			List<String> c_words= new ArrayList<String>();
			boolean w_flag = false;
			boolean kakko_flag = false;
			String word = "";

			// print "$choices{$key}\n";
			for (int i = 0 ; i < mor_choice.size(); i++) {
				String [] mor = mor_choice.get(i);
				if(mor[0].equals("*")) {
					continue;
				} else {
					// print "$mor_choice[$i]->[0]\t";
					// print "$mor_choice[$i]->[1]\t";
					// print "$mor_choice[$i]->[2]\n";
					String nextstr = "";
					if (i+1 < mor_choice.size()) {
						String [] nextar = mor_choice.get(i+1);
						if (nextar.length > 1)
							nextstr = nextar[1];
					}
					if (kakko_flag) {
						if (mor[1].contains("記号") && mor[2].contains("括弧閉")) {
							kakko_flag = false;
							word += mor[0];
							c_words.add(word);
							word = "";
						} else {
							word += mor[0];
						}
					} else if (w_flag) {
						String ran = "";
						if (i+2 < mor_choice.size()) {
							ran = mor_choice.get(i+2)[0];
						}
						if ((mor[1].contains("名詞") && mor[2].matches("一般|固有名詞|数|副詞可能|接尾|サ変接続|形容動詞語幹|ナイ形容動詞語幹"))
								|| (mor[1].contains("接頭詞") && mor[2].contains("名詞接続"))
								// || (mor[1].contains("助詞") && mor[2].contains("連体化/))
								|| (mor[1].contains("記号") && mor[2].contains("一般"))) {
							word = word + mor[0];
						} else if (mor[1].contains("助詞") && mor[2].contains("連体化") && ran.equals("乱")) {
							word = word + mor[0];
							// print "in\n";
						} else {
							w_flag = false;
							c_words.add(word);
							word = "";
						}
					} else if ((mor[1].contains("名詞") && mor[2].matches("一般|固有名詞|数|副詞可能|形容動詞語幹"))
							|| (mor[1].contains("接頭詞") && mor[2].contains("名詞接続")) 
							|| (mor[1].contains("接頭詞") && mor[2].contains("数接続"))) {
						word = mor[0];
						w_flag = true;
					} else if (mor[1].contains("名詞") && mor[2].contains("サ変接続") && nextstr.contains("名詞")) {
						word = mor[0];
						w_flag = true;
					} else if (mor[1].contains("記号") && mor[2].contains("括弧開")) {
						kakko_flag = true;
						word = word + mor[0];
					}
				}
			}
// print "c_words : @c_words c_words\n";
			String q_word = "何";
			String new_q_id = "";
//			String focus = "";

			int q_num = 0;
			for (String word2 : c_words) {
//System.out.println("MakeQueryTf:word2="+word2);				
				String org_word = word2;
				String wtype = word2type(word2);	// DBから単語のタイプを求める

				// print STDERR "$word : $type\n";
//System.out.println("wtype="+wtype);
				// とりあえず、typeが見つかったものだけ疑問詞と置き換える
				if (wtype != null && wtype.length() > 0) {
					new_q_id = key + "_" + q_num;
					q_word = word2qword(word2, wtype);
//System.out.println("1:new_q_id="+new_q_id);					
					//print STDERR "$q_word\n";
			
					String val = line.replace(org_word, q_word);
					val = val.replace("。","か。");
					// val = val.replaceAll("『|』","");
					val = val.replaceAll("\\(.+?\\)","");
					val = val.replaceAll("（.+?）","");
					querys.put(new_q_id, val);

					sup_ans.put(new_q_id, word2);
			
					// print STDERR "$new_q_id : $querys{$new_q_id} : $sup_ans{$new_q_id}\n";
					q_num++;
				}
			}
			// typeが見つからなかったら最後の名詞だけ置き換える
			if (q_num == 0) {
				String word2 = "";
				if (c_words.size() > 0)
					word2 = c_words.get(c_words.size() - 1);
				String word2_org = word2;
				word2 = word2.replaceAll("「|」|『|』","");
	
				// print "check : $new_q_id : $word\n";
				// print "$c_words[-1]\t $c_words[0]\n";
				new_q_id = key + "_" + q_num;
//System.out.println("2:new_q_id="+new_q_id);					
				q_word = word2qword(word2, "");
				//print STDERR "$q_word\n";
				word2_org = word2_org.replaceAll("\\?","\\\\?");
		
				String val = line.replaceAll(word2_org, q_word);
				val = val.replace("。","か。");
				//$querys{$new_q_id} =~ s/『|』//g;
				val = val.replaceAll("\\(.+?'\\)","");
				val = val.replaceAll("（.+?）","");
				querys.put(new_q_id, val);
		
				sup_ans.put(new_q_id, word2);
				//print "STDERR $new_q_id : $querys{$new_q_id} : $sup_ans{$new_q_id}\n";
			}
	    }
	    return;
	}

	public Map<String, String> makeSupAns(CeQuestion question) {

		Map<String, String> sup_ans = new HashMap<String, String>();
		Map<String, String> choices = new HashMap<String, String>();

		if (question.getQuestionType().equals("TF")) {
			choices = question.getChoice();
		} else if (question.getQuestionType().equals("TF-comb") ||
				question.getQuestionType().equals("T-comb")) {
			// choices = question.getRefText();
			for (String key : question.getRefUText().keySet()){
				if (key.contains("L")) {
					choices.put(key, question.getRefUText(key));
				}
			}
		}

		String [] clues = getClue(question);
		makeInclueLine(clues, choices);

		List<String> keys = new ArrayList<String>(choices.keySet());
		Collections.sort(keys);
		Pattern p1 = Pattern.compile("\\(.+?\\)");
		Pattern p2 = Pattern.compile("（.+?）");
		for (String key : keys) {
			String line = choices.get(key);
			Matcher m1 = p1.matcher(line);
			if (m1.find()) {
				String t1 = line.substring(0, m1.start());
				String t2 = line.substring(m1.end());
				line = t1 + t2;
			}
			Matcher m2 = p2.matcher(line);
			if (m2.find()) {
				String t1 = line.substring(0, m2.start());
				String t2 = line.substring(m2.end());
				line = t1 + t2;
			}
			line = line.replaceAll("-","～");
			line = normalize(line);

			List<String[]>mor_choice = useCabocha(line);
			List<String> c_words= new ArrayList<String>();
			boolean w_flag = false;
			boolean kakko_flag = false;
			int q_num = 0;
			String word = "";

			for (int i = 0 ; i < mor_choice.size(); i++) {
				String [] mor = mor_choice.get(i);
				if(mor[0].equals("*")) {
					continue;
				} else {
					if (kakko_flag) {
						if (mor[1].contains("記号") && mor[2].contains("括弧閉")) {
							kakko_flag = false;
							word += mor[0];
							c_words.add(word);
							word = "";
						} else {
							word += mor[0];
						}
					} else if (w_flag) {
						if ((mor[1].contains("名詞") && mor[2].matches("一般|固有名詞|数|副詞可能|接尾|サ変接続|形容動詞語幹|ナイ形容動詞語幹"))
								|| (mor[1].contains("接頭詞") && mor[2].contains("名詞接続"))
								// || (mor[1].contains("助詞") && mor[2].contains("連体化/))
								|| (mor[1].contains("記号") && mor[2].contains("一般"))) {
							word = word + mor[0];
						} else if (mor[1].contains("助詞") && mor[2].contains("連体化") && mor[0].contains("乱")) {
							word = word + mor[0];
						} else {
							w_flag = false;
							c_words.add(word);
							word = "";
						}
					} else if ((mor[1].contains("名詞") && mor[2].matches("一般|固有名詞|数|副詞可能|形容動詞語幹"))
							|| (mor[1].contains("接頭詞") && mor[2].contains("名詞接続")) 
							|| (mor[1].contains("接頭詞") && mor[2].contains("数接続"))) {
						word = mor[0];
						w_flag = true;
					} else if (mor[1].contains("名詞") && mor[2].contains("サ変接続") && 
							(i + 1) < mor_choice.size() && mor_choice.get(i+1)[1].contains("名詞")) {
						word = mor[0];
						w_flag = true;
					} else if (mor[1].contains("記号") && mor[2].contains("括弧開")) {
						kakko_flag = true;
						word = word + mor[0];
					}
				}
			}
			
			String new_q_id = "";
			for(String word2 : c_words) {
				String wtype = word2type(word);
				if(wtype != null && wtype.length() > 0) {
					new_q_id = key + "_" + q_num;
					sup_ans.put(new_q_id, word2);
					q_num++;
				}
			}
			// typeが見つかったものがなかったら、最後の名詞だけ置き換える
			if (q_num == 0 && c_words.size() > 0) {
				String word2 = c_words.get(c_words.size() - 1);
				word2 = word2.replaceAll("「|」|『|』","");
				new_q_id = key + "_" + q_num;
				sup_ans.put(new_q_id, word2);
			}
	    }
	    return sup_ans;
	}

	
	
	/**
	 * 手がかり語をとる
	 */
	private String [] getClue(CeQuestion query) {

		String [] clues = new String[2];	// [0]: clue [1]:c_type
		Map<String, String>utext = query.getRefUText();

		String line = normalize(query.getQLine());
		
		Pattern p1 = Pattern.compile("(\\d+世紀)に起こった");
		Pattern p2 = Pattern.compile("関連して，?(.+?)に(ついて|関して)");
		Pattern p3 = Pattern.compile("U\\d+の(時代|時期)(に|の)");
		Pattern p4 = Pattern.compile("U\\d+の(人物|皇帝|帝国|国|地域|王朝|島)(に|で)");
		Pattern p5 = Pattern.compile("U\\d+について");
		Pattern p6 = Pattern.compile("関連して，?この(時代|年|世紀)");
		Pattern p7 = Pattern.compile("関連して，?この");
		Pattern p8 = Pattern.compile("U\\d+に関連?して述べた");
		
		Matcher m1 = p1.matcher(line);
		Matcher m2 = p2.matcher(line);
		Matcher m3 = p3.matcher(line);
		Matcher m4 = p4.matcher(line); 
		Matcher m5 = p5.matcher(line); 
		Matcher m6 = p6.matcher(line); 
		Matcher m7 = p7.matcher(line); 
		Matcher m8 = p8.matcher(line); 

		clues[0] = "";
		clues[1] = "";
		
		if (m3.find()) {
			clues[1] = "time";
			for (String key : utext.keySet()) {
				if (key.contains("U")) 
					clues[0] =utext.get(key);
			}
		} else if (m1.find()) {
			clues[0] = m1.group(1);
			clues[1] = "time";
		} else if (m4.find() || m5.find()){
			clues[1] = "other";
			for (String key : utext.keySet()) {
				if (key.contains("U")) 
					clues[0] = utext.get(key);
			}
		} else if (m6.find()) {
			clues[1] = "time";
			for (String key : utext.keySet()) {
				if (key.contains("U")) 
					clues[0] = utext.get(key);
			}
		} else if (m7.find()) {
			clues[1] = "other";
			for (String key : utext.keySet()) {
				if (key.contains("U")) 
					clues[0] = utext.get(key);
			}
		} else if (m2.find()) {
			clues[0] = m2.group(1);
			clues[1] = "other";
		} else if (m8.find()) {
			clues[1] = "other";
			for (String key : utext.keySet()) {
				if (key.contains("U")) 
					clues[0] = utext.get(key);
			}
		}
		return clues;
	}

	/**
	 * 手がかり語が必要そうな文を判定して、必要そうなら手がかり語を追加
	 * @return
	 */
	private void makeInclueLine(String [] clues, Map<String, String> choices) {

		if (clues[0] == null || clues[0].length() == 0)
			return;
		
		// System.out.println(clues[0] + " : " + clues[1]);

		List<String> keys = new ArrayList<String>(choices.keySet());
		Collections.sort(keys);
		for (String  key : keys){
			String choiceValue = choices.get(key);
			if (!choiceValue.contains("。")) {
				continue;
			} else if (clues[1].equals("time")) {
				choices.put(key, clues[0]+"に，"+choiceValue);
			} else {
				String line = normalize(choiceValue);
				// print "$line\n";
				if (line.contains("この") || !line.matches("(は|が)")) {
					boolean syugo_flag = false;
					boolean kono_flag = false;
					String kono_line = "";
					// print STDERR "in\n";
		
					List<String []>mor_choice = useCabocha(line);
					for (String[] mor : mor_choice) {
						if (mor[0].equals("*"))  {
							continue;
						} else if (mor[0].equals("この")) {
							// print STDERR "in\n";
							kono_line = mor[0];
							// print STDERR "$kono_line\n";
							kono_flag = true;
						} else if (kono_flag && mor[1].contains("名詞")) {
							kono_line += mor[0];
							// print STDERR "$kono_line\n";
						} else if ((mor[0].equals("は") && mor[1].equals("助詞") && mor[2].equals("係助詞"))
								|| (mor[0].equals("が") && mor[1].equals("助詞") && mor[2].equals("格助詞"))) {
							syugo_flag = true;
							kono_flag = false;
						} else {
							kono_flag = false;
						}
					}
					if (kono_line.length() > 0) {
						// print STDERR "$kono_line\n";
						line.replaceAll(kono_line, clues[0]);
					} else if (syugo_flag == false){
						line = clues[0] + "は，" + line;
					}
					choices.put(key, line);
			    }
			}
	    }
	    return;
	}

	/**
	 * 単語からタイプを求める
	 */
	private String word2type(String word) {
		String word_2 = "";
		String  type = "";

		word = word.replaceAll("\\(.+?\\)","");
		word = word.replaceAll("「|」|『|』","");

		if (wdb1.containsKey(word)){
			word_2 =  word;
		}
		if (ydb1.containsKey(word)){
			type = ydb1.get(word);
		}
		if (type.equals("") && ydb2.containsKey(word)) {
			type = ydb2.get(word);
		}
		if (type.equals("") && wdb2.containsKey(word)) {
			type = "person";
		}

		if (type.equals("") && word_2.length() > 0) {
			if (ydb1.containsKey(word_2)) {
				type = ydb1.get(word_2);
			}
			if (type.equals(ydb2.containsKey(word_2))) {
				type = ydb2.get(word_2);
			}
			if (type.equals("") && wdb2.containsKey(word_2)) {
			    type = "person";
			}
		}
		return type;
	}

	/**
	 * 単語とタイプから疑問詞を決める
	 */
	private String word2qword(String word, String type) {

		String q_word = "何";
		Pattern setubi_1 = Pattern.compile("(民族|族|戦争|主義|法|教|派|美術|様式|建築|城|文明|文化|地帯|事件|革命|神殿|宣言|同盟)$");
		Pattern setubi_2 = Pattern.compile("(乱)$");
		Pattern setubi_3 = Pattern.compile("(川|山)$");
		Pattern setubi_4 = Pattern.compile("(朝|王国)$");
		Pattern setubi_5 = Pattern.compile("(人)$");

		Matcher setubi_m1 = setubi_1.matcher(word);
		Matcher setubi_m2 = setubi_2.matcher(word);
		Matcher setubi_m3 = setubi_3.matcher(word);
		Matcher setubi_m4 = setubi_4.matcher(word);
		Matcher setubi_m5 = setubi_5.matcher(word);
		
		if (type.contains("location") || type.contains("nationAndDynasty") || type.contains("geographicFeature")) {
			q_word = "どこ";
			if(setubi_m4.find()) {
				q_word += setubi_m4.group(1);
			}
		} else if (type.equals("person") || type.contains("family")) {
			q_word = "誰";
		} else if (type.contains("language")) {
			q_word = "何語";
		} else if(type.contains("time")) {
			q_word = "いつ";
		} else {
			if(setubi_m1.find()) {
				q_word = "何" + setubi_m1.group(1);
			} else if(setubi_m2.find()) {
				q_word= "何の" + setubi_m2.group(1);
			} else if(setubi_m3.find()) {
				q_word = "どこの" + setubi_m3.group(1);
			} else if(word.matches("\\d+年|\\d+世紀")){
				q_word = "いつ";
			} else if(setubi_m5.find()){
				q_word = "なに" + setubi_m5.group(1);
			}
		}
		// System.err.println(word + " : " + type + " : " + q_word);
		return q_word;
	}

	/**
	 * 出来事の順番を入れ替えて、正しい順番の選択肢を選ぶ問
	 * それぞれの出来事がいつ起こったのかを問う質問文を作る
	 */
	public Map<String, String> makeQueryOrder(CeQuestion query) {
		
		Map<String,String> querys = new HashMap<String, String>();
		Map<String, String> l_text = query.getRefUText();

		// それぞれの文について、「いつですか？」の質問を作るだけ
		List<String> l_ids = new ArrayList<String>(l_text.keySet());
		Collections.sort(l_ids);
		for (String l_id : l_ids) {
			if (l_id.contains("L")) {
				querys.put(l_id, l_text.get(l_id));
			}
		}
		String [] clues = getClue(query);

		// print STDERR "clue : $clue : $c_type\n";
		makeInclueLine(clues, querys);

		for (String l_id : querys.keySet()) {
			String q = querys.get(l_id);
System.out.println("makeQueryOrder: " + l_id + " : " + q);
			q = q.replaceAll("\\(.+?\\)","");
			q = q.replaceAll("（.+?）","");
			q = q.replaceAll("-", "～");
	
			if (q.contains("。")){
				q = q.replace("。", "のはいつですか。");
			} else{
				q = q + "はいつ起こりましたか。";
			}
			querys.put(l_id, q);
    	}
		return querys;
	}

	/**
	 * かぼちゃで形態素解析
	 */
	private List<String[]> useCabocha(String sent) {

		List<String[]> morpheme = new ArrayList<String[]>(); // my @morpheme=();
		
		sent = normalize(sent);
		try {
			Sentence sentence = cabocha.execute(sent);			
			List<Chunk> chunkList= sentence.getChunks();
			for (Chunk chunk : chunkList) {
				List<Token> tokens = chunk.getTokens();
				for (Token token : tokens) {
//					System.out.println("token:"+token.getBase()+ " pos:"+ token.getPos() + " ctype:"+ token.getCtype());
					String [] element = new String[3];
					element[0] = token.getBase();
					
					String [] pos = token.getPos().split("-");
					if (pos.length > 0)
						element[1] = pos[0];
					else 
						element[1] = "";
					if (pos.length > 1)
						element[2] = pos[1];
					morpheme.add(element);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return(morpheme);
	}

	/**
	 * 数字は半角に
     * かっこは全角に
     * ＝は全角に
	 */
	private String normalize(String sent) {

		sent = sent.replaceAll("１", "1");
		sent = sent.replaceAll("２", "2");
		sent = sent.replaceAll("３", "3");
		sent = sent.replaceAll("４", "4");
		sent = sent.replaceAll("５", "5");
		sent = sent.replaceAll("６", "6");
		sent = sent.replaceAll("７", "7");
		sent = sent.replaceAll("８", "8");
		sent = sent.replaceAll("９", "9");
		sent = sent.replaceAll("０", "0");
		sent = sent.replaceAll("\\(", "（");
		sent = sent.replaceAll("\\)", "）");
		sent = sent.replaceAll("=", "＝");
		sent = sent.replaceAll("＝", "・");

		return sent;
	}
}
