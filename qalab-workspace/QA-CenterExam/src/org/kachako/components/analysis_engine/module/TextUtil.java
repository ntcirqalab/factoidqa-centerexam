package org.kachako.components.analysis_engine.module;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 文字列処理のユーティリティクラス
 * 
 * @author JSA saitou
 *
 */
public class TextUtil
{

	/**
	 * 漢数字を半角アラビア数字に変換する.
	 * 
	 * @param str 漢数字を含む文字列
	 * @return 漢数字部分を半角アラビア数字に変換した文字列.
	 */
	public static String kanjiToArabicNum(String str)
	{
		final String regexNum = "[0-9]+";	// 数字列を表現する正規表現。
		final String regexKanNum[] = { "零|０", "一|１", "二|２", "三|３", "四|４", "五|５", "六|６", "七|７", "八|８", "九|９" };
		final String regexKanUnit[] = { "", "十", "百", "千", "万" }; // とりあえず「万」までで
		Matcher mch, mchN;
		StringBuffer strBuf = new StringBuffer();

		if(str != null && !str.equals("")) {
			// 数字を半角数字にする
			for(int i = 0; i < regexKanNum.length; i++) {
				str = str.replaceAll(regexKanNum[i], Integer.valueOf(i).toString());
			}
			// 数字の右に単位が付いていないものは、一の位と考えて「/数値/」という文字列に置き換える
			strBuf.setLength(0);
			mch = Pattern.compile("("+regexNum+")([^[0-9]"+regexKanUnit[1]+regexKanUnit[2]+regexKanUnit[3]+regexKanUnit[4]+"])").matcher(str);
			while(mch.find()) {
				mch.appendReplacement(strBuf, "/"+mch.group(1)+"/"+mch.group(2));
			}
			mch.appendTail(strBuf);
			str = strBuf.toString();
			// 数字の右に文字が無いものは、一の位と考えて「/数値/」という文字列に置き換える
			strBuf.setLength(0);
			mch = Pattern.compile("("+regexNum+")(\\s*)$").matcher(str);
			while(mch.find()) {
				mch.appendReplacement(strBuf, "/"+mch.group(1)+"/"+mch.group(2));
			}
			mch.appendTail(strBuf);
			str = strBuf.toString();
			// 数字の右に単位が付いているものは、その単位の数値と考えて「/数値/」という文字列に置き換える
			for(int i = 1; i < regexKanUnit.length; i++) {
				strBuf.setLength(0);
				mch = Pattern.compile("("+regexNum+")"+regexKanUnit[i]).matcher(str);
				while(mch.find()) {
					mch.appendReplacement(strBuf, "/"+Integer.valueOf(Integer.valueOf(mch.group(1))*(int)Math.pow(10, i)).toString()+"/");
				}
				mch.appendTail(strBuf);
				str = strBuf.toString();
			}
			// 残る「十」「百」「千」は、「(10)」「(100)」「(1000)」にする
			for(int i = 1; i < regexKanUnit.length-1; i++) {
				str = str.replaceAll(regexKanUnit[i], "/"+Integer.valueOf((int)Math.pow(10, i)).toString()+"/");
			}
			// 残る「万」は、「」(空)にする
			str = str.replaceAll(regexKanUnit[4], "");
			// 最後に数値を統合する
			strBuf.setLength(0);
			mch = Pattern.compile("(/"+regexNum+"/)+").matcher(str);
			while(mch.find()) {
				int num = 0;
				mchN = Pattern.compile("/("+regexNum+")/").matcher(mch.group());
				while(mchN.find()) {
					num += Integer.valueOf(mchN.group(1));
				}
				mch.appendReplacement(strBuf, Integer.valueOf(num).toString());
			}
			mch.appendTail(strBuf);
			str = strBuf.toString();
		}
		return str;
	}

	/**
	 * 全角英数字を半角英数字に変換する.
	 * 
	 * @param str 文字列
	 * @return 全角英数字を半角英数字に変換した文字列.
	 */
	public static String convertTwoByteToOneByte(String str)
	{
		int difference	= 'Ａ' - 'A';
		char[] cc = str.toCharArray();
		StringBuilder sb = new StringBuilder();
		for (char c : cc) {
			char newChar = c;
			if ((('Ａ' <= c) && (c <= 'Ｚ')) || (('ａ' <= c) && (c <= 'ｚ'))
					|| (('０' <= c) && (c <= '９'))) {
				// 変換対象のcharだった場合に全角文字と半角文字の差分を引く
				newChar = (char) (c - difference);
			}

			sb.append(newChar);
		}
		return sb.toString();
	}

	public  static String convertOneByteToTwoByte(String str)
	{
		int difference	= 'Ａ' - 'A';
		char[] cc = str.toCharArray();
		StringBuilder sb = new StringBuilder();
		for (char c : cc) {
			char newChar = c;
			if ((('A' <= c) && (c <= 'Z')) || (('a' <= c) && (c <= 'z'))
					|| (('0' <= c) && (c <= '9'))) {
				// 変換対象のcharだった場合に全角文字と半角文字の差分を足す
				newChar = (char) (c + difference);
			}

			sb.append(newChar);
		}
		return sb.toString();
	}

	/**
	 * 全角記号を半角記号に変換する.
	 * 
	 * @param str 文字列
	 * @return 全角記号を半角記号に変換した文字列.
	 */
	public static String convertTwoByteSignToOneByteSign(String str)
	{
		int difference	= 'Ａ' - 'A';
		char[] cc = str.toCharArray();
		StringBuilder sb = new StringBuilder();
		for (char c : cc) {
			char newChar = c;
			if (isTwoByteSign(c)) {
				// 変換対象のcharだった場合に全角文字と半角文字の差分を引く
				newChar = (char) (c - difference);
			}

			sb.append(newChar);
		}
		return sb.toString();
	}

	private static boolean isTwoByteSign(char pc)
	{
		char[] signs	= { '！',  '＃',  '＄',  '％',  '＆', '（', '）', '＊', '＋', '，', '−', '．', '／', '：', '；', '＜',  '＝',  '＞',  '？',  '＠',  '［',  '］',  '＾',  '＿',  '｛',  '｜',  '｝' };
		for(char c : signs) {
			if(c == pc) {
				return true;
			}
		}
		return false;
	}

//	private static boolean isOneByteSign(char pc)
//	{
//		char[] signs	= { '!' , '"', '#' , '$' ,'%' , '&' ,'\'' , '(' , ')' , '*' , '+' , ',' , '.' , '/' , ':' , ';' , '<' , '=' , '>' , '?' , '@' , '[' , '\\' , ']' , '^' , '_' , '`' , '{' , '|' , '}' , '~' ,'-' };
//		for(char c : signs) {
//			if(c == pc) {
//				return true;
//			}
//		}
//		return false;
//	}
}
