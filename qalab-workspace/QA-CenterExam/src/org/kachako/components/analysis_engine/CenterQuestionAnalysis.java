package org.kachako.components.analysis_engine;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.TOP;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.kachako.components.analysis_engine.module.CasData2CenterAnalysisQuestionData;
import org.kachako.components.analysis_engine.module.CeQuestion;
import org.kachako.components.analysis_engine.module.MakeQuery;
import org.kachako.types.centerexam.AnsColumn;
import org.kachako.types.centerexam.Data;
import org.kachako.types.centerexam.Question;
import org.kachako.types.centerexam.Ref;
import org.kachako.types.centerexam.TagList;
import org.kachako.types.qa.AnswerType;
import org.kachako.types.qa.Metadata;

public class CenterQuestionAnalysis extends JCasAnnotator_ImplBase {

//	// 問題文の内容が年代順のパターン
//	private static final String PAT_CHRONOLOGICAL_QS	= "年代順|年代の古いもの|歴史的な流れ|時代順|正しく配列|年代の古い順|配列しているもの";
//	// 「表」を使用する問題文のパターン
//	private static final String PAT_TABLE_QS	= "次の年表";
//	private final RegexCache regexCache	= new RegexCache();

	protected static final String TARGET_LANGUAGE_QUESTION_VIEW_NAME = "QuestionViewName";
	protected static final String ENGLISH_QUESTION_VIEW_NAME = "EnglishQuestionView";
	protected static final String CABOCHAPATH = "cabochaPath";
	protected static final String DBDIRECTORY = "dbDirectory";
	
	private String cabochaPath;
	private String dbDirectory;
	private String targetQuestionViewName;
	// private String englishQuestionViewName;
	
	protected final String LINE_SEPARATOR = "\n";
	
	
	private MakeQuery makeQuery;
	
	@Override
	public void initialize(UimaContext aContext) throws ResourceInitializationException
	{
		System.out.println("--------QuestionAnalysis initialize---------");
		super.initialize(aContext);

		cabochaPath	= (String)aContext.getConfigParameterValue(CABOCHAPATH);
		dbDirectory	= (String)aContext.getConfigParameterValue(DBDIRECTORY);
		targetQuestionViewName = (String)aContext.getConfigParameterValue(TARGET_LANGUAGE_QUESTION_VIEW_NAME);
		//englishQuestionViewName = "EnglishViewName";

		makeQuery = new MakeQuery(dbDirectory, cabochaPath);
	}
	

	private JCas findInputView(JCas aJCas, String viewName) {

		JCas targetCas = null;
		Iterator<JCas> vIter = null;
		try {
			vIter = aJCas.getViewIterator();
		} catch (CASException e1) {
			e1.printStackTrace();
		}
		while (vIter.hasNext()) { 
			JCas jcas = vIter.next();
			String name = jcas.getViewName();
			if (name.equals(viewName)) {
				targetCas = jcas;
				break;
			}
		}
		if (targetCas == null) {
			try {
				targetCas = aJCas.getView("_InitialView");
			} catch (CASException e) {
				e.printStackTrace();
			}
		}
		return targetCas;
	}
	
	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		System.out.println("--------QuestionAnalysis start---------");

		JCas inView = findInputView(jCas, "Text");
		
		String fname = null;
		// SourceDocumentInformation uriの取得　------------
		TypeSystem typeSystem = inView.getTypeSystem();
		Type type = typeSystem.getType(SourceDocumentInformation.class.getName());
		
		FSIterator<FeatureStructure> fsIterator = inView.getFSIndexRepository().getAllIndexedFS(type);
		if(fsIterator.hasNext()){
			SourceDocumentInformation srcDocInfo = (SourceDocumentInformation) fsIterator.next();
			try {
				String name = new File(new URI(srcDocInfo.getUri()).getPath()).getName();
				fname = name.substring(0, name.indexOf(".xml"));
			} catch (URISyntaxException e) {
			}
		}
System.out.println(" fname="+fname);	
		Iterator<TOP> tagListFSIterator = inView.getJFSIndexRepository().getAllIndexedFS(TagList.type);

		Map<String, CeQuestion>exam = null;
        StringBuilder selected = new StringBuilder();

		if (tagListFSIterator.hasNext() /*&& endListFSIterator.hasNext()*/) {
			TagList tagListFS	= (TagList)tagListFSIterator.next();
			FSArray tagListFsArray = tagListFS.getAnnotationList();

			// 各大問ごとに設問を仕分けするMAP
			Map<Question,LinkedHashSet<Data>> maximalQsMap	= new LinkedHashMap<Question,LinkedHashSet<Data>>();
			// 各小問の設問保持
			Map<Question,Data> miniQsDataMap	= new LinkedHashMap<Question,Data>();
			// 各大問の各小問のQuestion保持
			Map<Question, List<Question>> miniQuestionMap	= new LinkedHashMap<Question, List<Question>>();
			// 各大問の各小問のAnsColum保持
			Map<Question, List<AnsColumn>> miniAnsColumnMap	= new LinkedHashMap<Question, List<AnsColumn>>();
			// 各小問の内容を仕分けするMAP
			Map<Question,List<Annotation>> minimalQsAnnotationMap	= new LinkedHashMap<Question,List<Annotation>>();
			// 各小問のAnsColumn保持MAP
			Map<Question,AnsColumn> minimalQsAnsColumnMap	= new LinkedHashMap<Question,AnsColumn>();

			List<Ref> refList = new ArrayList<Ref>();
			List<Annotation> blankList = new ArrayList<Annotation>();
			List<Annotation> labelList = new ArrayList<Annotation>();
			
			boolean yozemiFlag = false;

			CasData2CenterAnalysisQuestionData paseCas = new CasData2CenterAnalysisQuestionData();
			paseCas.parseQuestionData(tagListFsArray, /*endListFsArray,*/ maximalQsMap,
										miniQsDataMap, miniQuestionMap, miniAnsColumnMap, minimalQsAnnotationMap, 
										minimalQsAnsColumnMap, 
										refList, blankList, labelList,
										yozemiFlag, selected);

System.out.println("maximalQsMap.size="+maximalQsMap.size());
System.out.println("miniQuestionMap.size="+miniQuestionMap.size());
System.out.println("minimalQsAnnotationMap.size="+minimalQsAnnotationMap.size());
System.out.println("miniQsDataMap.size="+miniQsDataMap.size());
System.out.println("minimalQsAnsColumnMap.size="+minimalQsAnsColumnMap.size());
			
			if (maximalQsMap != null) {
				exam = paseCas.makeCeQuestionData(miniQuestionMap, minimalQsAnnotationMap, miniQsDataMap, minimalQsAnsColumnMap,
						labelList, blankList, refList);
			}
		}
		List<String> keys = null;
		if (exam == null)
			keys = new ArrayList<String>();
		else
			keys = new ArrayList<String>(exam.keySet());
//		Collections.sort(keys);
		for (String key : keys) {
			CeQuestion q = exam.get(key);
			Map<String, String> querys = null;
			if (q.getQuestionType().equals("factoid"))
				querys = makeQuery.makeQueryFactoid(q);
			else if (q.getQuestionType().equals("blank"))
				querys = makeQuery.makeQueryBlank(q);
			else if (q.getQuestionType().equals("blank-comb"))
				querys = makeQuery.makeQueryBlankComb(q);
			else if (q.getQuestionType().contains("TF-underline"))
				querys = makeQuery.makeQueryTfUnderline(q);
			else if (q.getQuestionType().equals("order"))
				querys = makeQuery.makeQueryOrder(q);
			else if (q.getQuestionType().equals("TF") ||
					q.getQuestionType().equals("TF-comb") ||	
					q.getQuestionType().equals("T-comb")) {	
				querys = new HashMap<String, String>();
				Map<String, String> supans = new HashMap<String, String>();
				makeQuery.makeQueryTf(q, supans, querys);
				q.setSupAns(supans);
			}
			if (querys != null) {
				q.setQAQuery(querys);
			}
dumpCeQuestion(q, key, fname);
		}
		outputCas(inView, exam, fname);
		return;
	}

	
	private void outputCas(JCas jCas, Map<String, CeQuestion>exam, String fname) {

		if (exam == null) {
			System.out.println("exam is null");
			return;
		}
		//create Question and Answer views
		try {
			int casSuffix = 1;
			for (String qid : exam.keySet()) {
				CeQuestion q = exam.get(qid);
				Map<String, String> qaq = q.getQAQuery();
				for (String qakey : qaq.keySet()) {
					String qstring = qaq.get(qakey);

					String targetViewName = targetQuestionViewName+"."+Integer.toString(casSuffix);
System.out.println("create ViewName="+targetViewName);
					JCas targetView = jCas.createView(targetViewName);

					//add Metadata
					addMetadata(jCas.getView(targetViewName));
					//addMetadata(jCas.getView(englishViewName));
					
					setUpQuestionViews(qstring, qid, qakey, fname, targetView/*, englishView*/, q.getAnswerType());
					casSuffix++;
				}
			}
		} catch (CASException e) {
			e.printStackTrace();
		}

	}
	

	protected void setUpQuestionViews(String qstring, String qid, String qakey,
			String fname, JCas targetView/*, JCas englishView*/, String answerType) {
		
		//add Question FSs and create document of Question views
		addQuestion(qstring, fname, targetView);
		//addQuestion(qstring, fname, englishView);

		
		AnswerType answerTypeFS	= new AnswerType(targetView);
		answerTypeFS.setAnswerType(answerType);
		answerTypeFS.addToIndexes();
		
		
		//set document of Question views
		targetView.setDocumentText(qstring + LINE_SEPARATOR);
		//englishView.setDocumentText("");

		String topicid = fname + "-" + qid + "-" + qakey;
		
		//set URI of Question views
		SourceDocumentInformation targetSourceDocumentInformation = new SourceDocumentInformation(targetView);
		targetSourceDocumentInformation.addToIndexes(targetView);
		targetSourceDocumentInformation.setUri(topicid);
System.out.println("uri="+topicid);
		//SourceDocumentInformation englishSourceDocumentInformation = new SourceDocumentInformation(englishView);
		//englishSourceDocumentInformation.addToIndexes(englishView);
		//englishSourceDocumentInformation.setUri(topicid);
	}
	
	private void addQuestion(String qstring, String fname, JCas qView) {
		org.kachako.types.qa.Question questionFS = new org.kachako.types.qa.Question(qView);
		questionFS.addToIndexes(qView);
		
		//set span and add document text
		questionFS.setBegin(0);
		questionFS.setEnd(qstring.length()-1);
		
		//add Narrative Feature
		questionFS.setNarrative("");
	}
	
	
	protected void addMetadata(JCas targetJCas){
		
		Metadata metadataFS = new Metadata(targetJCas);
		metadataFS.setDescription("This is the topic file to be used in the NTCIR-8 ACLIA EN-JA and JA-JA formal evaluation");
		metadataFS.setVersion("1.0");
		metadataFS.setSourceLanguage("EN");
		metadataFS.setTargetLanguage("JA");
		metadataFS.setCorpus("Mainichi 2002-2005");
		metadataFS.addToIndexes();
	}
	
	private void dumpCeQuestion(CeQuestion q, String id, String fname) {

		System.out.println(fname+"-"+id);
		System.out.println("\tQA_query");
		Map<String, String> qaqs = q.getQAQuery();
		if (qaqs.size() > 0) {
			List<String>keys = new ArrayList<String>(qaqs.keySet());
			Collections.sort(keys);
			for (String key: keys) {
				System.out.println("\t\t"+key+" : "+qaqs.get(key));
			}
		}
		System.out.println("\tquestiontype");
		System.out.println("\t\t0 :"+q.getQuestionType());
		System.out.println("\tq_line");
		System.out.println("\t\t0 :"+q.getQLine());
		System.out.println("\t\torg :"+q.getQLineOrg());
		
		Map<String, String> supa = q.getSupAns();
		if (supa.size() > 0) {
			System.out.println("\tsup_ans");
			List<String>keys = new ArrayList<String>(supa.keySet());
			Collections.sort(keys);
			for (String key: keys) {
				System.out.println("\t\t"+key+" : "+supa.get(key));
			}
		}
		
		System.out.println("\tknowledge");
		System.out.println("\t\t0 : "+q.getKnowlede());
		System.out.println("\tanswernum");
		System.out.println("\t\t0 : "+q.getAnswernum());
		System.out.println("\tanswertype");
		System.out.println("\t\t0 : "+q.getAnswerType());
		Map<String, String> choices = q.getChoice();
		if (choices.size() > 0) {
			System.out.println("\tchoice");
			for (String key: choices.keySet()) {
				System.out.println("\t\t"+key+" : "+choices.get(key));
			}
		}
		Map<String, String> utexts = q.getRefUText();
		if (utexts.size() > 0) { 
			System.out.println("\tref_utext");
			for (String key: utexts.keySet()) {
				System.out.println("\t\t"+key+" : "+utexts.get(key));
			}
		}
	}
}
