package org.kachako.dbutil;

import java.io.File;

import org.mapdb.BTreeMap;
import org.mapdb.DB;
import org.mapdb.DBMaker;

public class mapdbtest {

	public static void main(String[] args) {
	
//		DB db = DBMaker.newFileDB(new File("DB/test-PERSON")).readOnly().make();
		DB db = DBMaker.newFileDB(new File("DB/test-PERSON")).make();
		BTreeMap<String, String> map = db.getTreeMap("test-person");
		
		if (map.containsKey("一青窈")) {
			String val = map.get("一青窈");
			System.out.println("exist val="+val);
		} else {
			map.put("一青窈", "Singer");
			
			db.commit();
		}
		if (!map.containsKey("stieve")) {
			map.put("stieve", "jobs");
			System.out.println("set ok");
		}
		if (map.containsKey("stieve")) {
			System.out.println("get ok");
		}
		db.commit();
		
		db.close();
			
			
		
//	DB db = DBMaker.newFileDB(new File("DB/DB-WIKI-PERSON")).readOnly().make();
//		
//	if (db.exists("一青窈")) {
//		String name = db.get("一青窈");
//		System.out.println("name="+name);
//	}
		
		
}

	
	
	
}
