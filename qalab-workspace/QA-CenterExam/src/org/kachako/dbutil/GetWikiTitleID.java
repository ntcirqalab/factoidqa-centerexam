package org.kachako.dbutil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.mapdb.BTreeMap;
import org.mapdb.DB;
import org.mapdb.DBMaker;

public class GetWikiTitleID {

	private final static String	DBFOLDER = "DB/";
	private final static String  DBFILE = "DB-WIKI-TITLE-ID";
	private final static String  DBFILE2 = "DB-WIKI-TITLE-ID-2";

	static DB wdb;
	static BTreeMap<String, String> wmap;
	static DB wdb2;
	static BTreeMap<String, String> wmap2;

	
	private final static String WIKI_FOLDER = "DB/src/wikipedia/wiki-100M-xml";
	
	
	public static void main(String[] args) {
		
		wdb = DBMaker.newFileDB(new File(DBFOLDER+DBFILE)).make();
		wmap = wdb.getTreeMap(DBFILE);
		wdb2 = DBMaker.newFileDB(new File(DBFOLDER+DBFILE2)).make();
		wmap2 = wdb2.getTreeMap(DBFILE2);

		File dir = new File(WIKI_FOLDER);
		File[] files = dir.listFiles();

//		Pattern p1 = Pattern.compile("<page>(.+?)</page>");
		Pattern p2 = Pattern.compile("<title>(.+?)</title>");
		Pattern p3 = Pattern.compile("<id>(.+?)</id>");
		
		for (File txtfile : files) {
			
			if (txtfile.isDirectory())
				continue;
			
			try {
System.out.println("file="+txtfile.toURI());
String docno = "";
String title = "";
				FileReader fr = new FileReader(txtfile);
				BufferedReader br = new BufferedReader(fr);
				String line;
				while ((line = br.readLine()) != null) {

					if (line.contains("</page>")) {
						title = "";
						docno = "";
					}
//					Matcher m2 = p2.matcher(page);
					Matcher m2 = p2.matcher(line);
					if (m2.find()) {
						title = m2.group(1);
					}
//					Matcher m3 = p3.matcher(page);
					Matcher m3 = p3.matcher(line);
					if (m3.find()) {
						docno = m3.group(1);
					}
				
					if (title.length() > 0 && docno.length() > 0) {
						System.out.println(title+" : "+docno);
						wmap.put(title, docno);
						wmap2.put(docno, title);
						title = "";
						docno = "";
					}
				}
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
				continue;
			}
		}
		wdb.commit();
		wdb.close();
		wdb2.commit();
		wdb2.close();
	}
}
