
package org.kachako.dbutil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.mapdb.BTreeMap;
import org.mapdb.DB;
import org.mapdb.DBMaker;

public class MakePersonDB {
	
	final static String	DBFOLDER = "DB/";
	final static String DBFILE = "DB-WIKI-PERSON";
	final static long N = Long.MAX_VALUE;

	final static String txtFile = "src/WikiPerson-seibotu-2.txt";
	static DB wdb;
	static BTreeMap<String, String> wmap;
	
	public static void main(String[] args) {

		wdb = DBMaker.newFileDB(new File(DBFOLDER+DBFILE)).make();
		wmap = wdb.getTreeMap(DBFILE);

		Pattern p1 = Pattern.compile("(\\S+)\\s+\\((.+)\\)");
		
		FileReader fr = null;
		try {
			fr = new FileReader(DBFOLDER+txtFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(fr);
		String line;
		try {
			while ((line = br.readLine()) != null) {
				line = line.trim();
			
				String term1 = "";
				String term2 = "";

				Matcher m = p1.matcher(line);
				if (m.find()) {
					term1 = m.group(1);
					term2 = m.group(2);
					
					if (!wmap.containsKey(term1)) {
						wmap.put(term1, term2);
System.out.println(term1+":"+term2);						
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}	
		wdb.commit();
		wdb.close();
	}
}