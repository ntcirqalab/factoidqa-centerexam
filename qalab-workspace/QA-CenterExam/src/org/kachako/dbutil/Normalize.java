package org.kachako.dbutil;

public class Normalize {

	public static String normalize(String sent) {
		
		sent = sent.replaceAll("１","1");
		sent = sent.replaceAll("２","2");
		sent = sent.replaceAll("３","3");
		sent = sent.replaceAll("４","4");
		sent = sent.replaceAll("５","5");
		sent = sent.replaceAll("６","6");
		sent = sent.replaceAll("７","7");
		sent = sent.replaceAll("８","8");
		sent = sent.replaceAll("９","9");
		sent = sent.replaceAll("０","0");	
		// かっこは全角に
		sent = sent.replaceAll("\\(","（");
		sent = sent.replaceAll("\\)","）");
		// ＝は全角に
		sent = sent.replaceAll("=","＝");
		sent = sent.replaceAll("＝","・");


		sent = sent.replaceAll("Ａ","A");
		sent = sent.replaceAll("ａ","a");
		sent = sent.replaceAll("Ｂ","B");
		sent = sent.replaceAll("ｂ","b");
		sent = sent.replaceAll("Ｃ","C");
		sent = sent.replaceAll("ｃ","c");
		sent = sent.replaceAll("Ｄ","D");
		sent = sent.replaceAll("ｄ","d");
		sent = sent.replaceAll("Ｅ","E");
		sent = sent.replaceAll("ｅ","e");
		sent = sent.replaceAll("Ｆ","F");
		sent = sent.replaceAll("ｆ","f");
		sent = sent.replaceAll("Ｇ","G");
		sent = sent.replaceAll("ｇ","g");
		sent = sent.replaceAll("Ｈ","H");
		sent = sent.replaceAll("ｈ","h");
		sent = sent.replaceAll("Ｉ","I");
		sent = sent.replaceAll("ｉ","i");
		sent = sent.replaceAll("Ｊ","J");
		sent = sent.replaceAll("ｊ","j");
		sent = sent.replaceAll("Ｋ","K");
		sent = sent.replaceAll("ｋ","k");
		sent = sent.replaceAll("Ｌ","L");
		sent = sent.replaceAll("ｌ","l");
		sent = sent.replaceAll("Ｍ","M");
		sent = sent.replaceAll("ｍ","m");
		sent = sent.replaceAll("Ｎ","N");
		sent = sent.replaceAll("ｎ","n");
		sent = sent.replaceAll("Ｏ","O");
		sent = sent.replaceAll("ｏ","o");
		sent = sent.replaceAll("Ｐ","P");
		sent = sent.replaceAll("ｐ","p");
		sent = sent.replaceAll("Ｑ","Q");
		sent = sent.replaceAll("ｑ","q");
		sent = sent.replaceAll("Ｒ","R");
		sent = sent.replaceAll("ｒ","r");
		sent = sent.replaceAll("Ｓ","S");
		sent = sent.replaceAll("ｓ","s");
		sent = sent.replaceAll("Ｔ","T");
		sent = sent.replaceAll("ｔ","t");
		sent = sent.replaceAll("Ｕ","U");
		sent = sent.replaceAll("ｕ","u");
		sent = sent.replaceAll("Ｖ","V");
		sent = sent.replaceAll("ｖ","v");
		sent = sent.replaceAll("Ｗ","W");
		sent = sent.replaceAll("ｗ","w");
		sent = sent.replaceAll("Ｘ","X");
		sent = sent.replaceAll("ｘ","x");
		sent = sent.replaceAll("Ｙ","Y");
		sent = sent.replaceAll("ｙ","y");
		sent = sent.replaceAll("Ｚ","Z");
		sent = sent.replaceAll("ｚ","z");

		    return sent;
	}



}
