package org.kachako.dbutil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.mapdb.BTreeMap;
import org.mapdb.DB;
import org.mapdb.DBMaker;

public class MakeRedDB {
	
	private final static String	DBFOLDER = "DB/";
	final static String DBFILE = "DB-WIKI-TITLE-ID-2";
	final static String DBFILE2 = "DB-WIKI-RED";
	final static String DBFILE3 = "DB-WIKI-CAT";
	
	static DB tdb;
	static DB rdb;
//	static DB cdb;
	static BTreeMap<String, String> tmap;
	static BTreeMap<String, String> rmap;
//	static BTreeMap<String, String> cmap;

	final static String txtFile = "src/edge/part-r-00000";
	
	public static void main(String[] args) {

		// -- DBMを開く --
		tdb = DBMaker.newFileDB(new File(DBFOLDER+DBFILE)).readOnly().make();
		rdb = DBMaker.newFileDB(new File(DBFOLDER+DBFILE2)).make();
//		cdb = DBMaker.newFileDB(new File(DBFOLDER+DBFILE3)).make();
		tmap = tdb.getTreeMap(DBFILE);
		rmap = rdb.getTreeMap(DBFILE2);
//		cmap = cdb.getTreeMap(DBFILE3);

		PrintWriter out_1 = null;
//		PrintWriter out_2 = null;
		try {
			out_1 = new PrintWriter(new BufferedWriter(new FileWriter(new File(DBFOLDER+"redirect.txt"))));
//			out_2 = new PrintWriter(new BufferedWriter(new FileWriter(new File(DBFOLDER+"category.txt"))));
		} catch (IOException e) {
			e.printStackTrace();
		}

		Pattern p1 = Pattern.compile("(\\S+)\\s+(.+)\\s+(hypernym|redirect|target)");
		
		FileReader fr = null;
		try {
			fr = new FileReader(DBFOLDER+txtFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(fr);
		String line;
		try {
			while ((line = br.readLine()) != null) {
				line = line.trim();
			
				String term1 = "";
				String term2 = "";
				Matcher m = p1.matcher(line);
//System.out.println(line);				
				if (m.find()) {
					String id1 = m.group(1);
					String id2 = m.group(2);
					String types = m.group(3);

					if (tmap.containsKey(id1)) {
						if (tmap.containsKey(id2)) {
							//print STDERR "$TDB{$id1}\t$TDB{$id2}\n";
							term1 = tmap.get(id1);
							term2 = tmap.get(id2);
							if (types.equals("target")) {
System.out.println(term1+":"+term2);				    
								rmap.put(term1, term2);
								out_1.println(term1 + " : " + term2);
							} else if (types.equals("hypernym")) {
//								 if (cmap.containsKey(term1)) {
//						    				String tmp = cmap.get(term1);
//				    				cmap.put(term1, tmp + "," + term2);
//				    			} else {
//				    				cmap.put(term1, term2);
//				    			}
//				    			out_2.println(term1 + " : " + cmap.get(term1));
							}
						} else 
							System.out.println(id2+"はみつかりません。");
					} else 
						System.out.println(id1+"は見つかりません");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
//		cdb.commit();
		rdb.commit();
	//	cdb.close();
		rdb.close();
		tdb.close();
	}
}