package org.kachako.dbutil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.mapdb.BTreeMap;
import org.mapdb.DB;
import org.mapdb.DBMaker;

public class TextbookGetwordDB {
	
	private final static String	DBFOLDER = "DB/";
	private final static String  DBFILE2 = "DB-WIKI-RED";
	private final static String  DBFILE = "TOKYOSHOSEKI-YOGO-DB";

	static DB rdb;
	static DB wdb;
	static BTreeMap<String, String> rmap;
	static BTreeMap<String, String> wmap;

	private final static String xmlDirectory = "src/textbook/annotation-book-rekishi-utf8/";

	public static void main(String[] args) {
	
		rdb = DBMaker.newFileDB(new File(DBFOLDER+DBFILE2)).readOnly().make();
		wdb = DBMaker.newFileDB(new File(DBFOLDER+DBFILE)).make();
		rmap = rdb.getTreeMap(DBFILE2);
		wmap = wdb.getTreeMap(DBFILE);

		Map<String, Map<String,Integer>> terms = new HashMap<String, Map<String,Integer>>();
		Map<String, Integer> types = new HashMap<String, Integer>();

		File dir = new File(DBFOLDER+xmlDirectory);
		File[] files = dir.listFiles();
	
		Pattern p0 = Pattern.compile("<(sectionTitle|pageNumber|chapterTitle|partTitle)>");		
		Pattern p1 = Pattern.compile("<([^/]+?)>(.+?)</(.+?)>");
		Pattern p2 = Pattern.compile("\\s");		
	
		for (File xml : files) {			
			if (xml.isDirectory())
			continue;
			System.out.println(xml);

//			FileReader fr;
			InputStreamReader fr;
			try {
//				fr = new FileReader(xml);
				fr = new InputStreamReader(new FileInputStream(xml), "UTF-8");
				BufferedReader br = new BufferedReader(fr);
				String line;
				while ((line = br.readLine()) != null) {
					line = line.trim();
					line = Normalize.normalize(line);
		
					Matcher m0 = p0.matcher(line);
					if (m0.find()) 
						line = line.substring(m0.end());
					int ks = line.indexOf("<topic>");
					if (ks >= 0)
						line = line.substring(ks+7); 
					ks = line.indexOf("<p>");
					if (ks >= 0)
						line = line.substring(ks+3); 

//					int last_start = 0;
					Matcher m = p1.matcher(line);
					while (m.find()) {
						String t_name  = m.group(1);
						String term    = m.group(2);
						String t_name2 = m.group(3);
						Matcher tm = p2.matcher(t_name);
						if (tm.find()) {
							t_name = t_name.substring(0, tm.start());
						}
//						last_start = m.end();
				
						if (t_name.equals(t_name2)) {
							if (!terms.containsKey(term)) {
								terms.put(term, new HashMap<String,Integer>());
							}
							Map<String, Integer> tmap = terms.get(term);
					
							if (tmap.containsKey(t_name)) {
								int n = tmap.get(t_name);
								tmap.put(t_name, n+1);
							} else {
								tmap.put(t_name, 1);
							}
//System.out.println("terms>"+term+","+t_name);
							if (types.containsKey(t_name)) {
								int n = types.get(t_name);
								types.put(t_name, n+1);
							} else {
								types.put(t_name, 1);
							}	
							if (rmap.containsKey(term) && !rmap.get(term).contains("Category")) {
								term = rmap.get(term);
								if (!terms.containsKey(term)) {
									terms.put(term, new HashMap<String, Integer>());
								}
								
								Map<String, Integer> tmap2 = terms.get(term);
						
								if (tmap2.containsKey(t_name)) {
									int n = tmap2.get(t_name);
									tmap2.put(t_name, n+1);
								} else {
									tmap2.put(t_name, 1);
								}
//System.out.println("terms>"+term+","+t_name);
								if (types.containsKey(t_name)) {
									int n = types.get(t_name);
									types.put(t_name, n+1);
								} else {
									types.put(t_name, 1);
								}
							}
						} else {
							System.out.println("一致しません "+t_name+" : "+term+" : "+t_name2);
						}
						// print "$p_line\n";
					}
				}
				br.close();
				fr.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		for (String type1 : types.keySet()) {
			for (String yougo : terms.keySet()) {
				if(terms.containsKey(yougo)) {
					Map<String, Integer> tmap = terms.get(yougo);
					if (tmap.containsKey(type1)) {
System.out.println("wmap put:"+yougo+"="+type1);						
						if(wmap.containsKey(yougo)) {
							String tmp = wmap.get(yougo);
							wmap.put(yougo,  tmp+","+type1);
						} else {
							wmap.put(yougo, type1);
						}
					}
				}
			}
		}
		wdb.commit();
		wdb.close();
		rdb.close();
	}
}
