

/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.TOP;


/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class EndList extends TOP {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(EndList.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected EndList() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public EndList(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public EndList(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: AnnotationList

  /** getter for AnnotationList - gets 
   * @generated */
  public FSArray getAnnotationList() {
    if (EndList_Type.featOkTst && ((EndList_Type)jcasType).casFeat_AnnotationList == null)
      jcasType.jcas.throwFeatMissing("AnnotationList", "org.kachako.types.centerexam.EndList");
    return (FSArray)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((EndList_Type)jcasType).casFeatCode_AnnotationList)));}
    
  /** setter for AnnotationList - sets  
   * @generated */
  public void setAnnotationList(FSArray v) {
    if (EndList_Type.featOkTst && ((EndList_Type)jcasType).casFeat_AnnotationList == null)
      jcasType.jcas.throwFeatMissing("AnnotationList", "org.kachako.types.centerexam.EndList");
    jcasType.ll_cas.ll_setRefValue(addr, ((EndList_Type)jcasType).casFeatCode_AnnotationList, jcasType.ll_cas.ll_getFSRef(v));}    
    
  /** indexed getter for AnnotationList - gets an indexed value - 
   * @generated */
  public CenterExamTag getAnnotationList(int i) {
    if (EndList_Type.featOkTst && ((EndList_Type)jcasType).casFeat_AnnotationList == null)
      jcasType.jcas.throwFeatMissing("AnnotationList", "org.kachako.types.centerexam.EndList");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((EndList_Type)jcasType).casFeatCode_AnnotationList), i);
    return (CenterExamTag)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((EndList_Type)jcasType).casFeatCode_AnnotationList), i)));}

  /** indexed setter for AnnotationList - sets an indexed value - 
   * @generated */
  public void setAnnotationList(int i, CenterExamTag v) { 
    if (EndList_Type.featOkTst && ((EndList_Type)jcasType).casFeat_AnnotationList == null)
      jcasType.jcas.throwFeatMissing("AnnotationList", "org.kachako.types.centerexam.EndList");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((EndList_Type)jcasType).casFeatCode_AnnotationList), i);
    jcasType.ll_cas.ll_setRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((EndList_Type)jcasType).casFeatCode_AnnotationList), i, jcasType.ll_cas.ll_getFSRef(v));}
  }

    