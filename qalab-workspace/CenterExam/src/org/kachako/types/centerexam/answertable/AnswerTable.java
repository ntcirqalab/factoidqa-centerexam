

/* First created by JCasGen Fri Mar 08 11:24:40 JST 2013 */
package org.kachako.types.centerexam.answertable;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Tue Aug 06 10:15:21 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/answerTableTypeSystem.xml
 * @generated */
public class AnswerTable extends AnswerTableTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(AnswerTable.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected AnswerTable() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public AnswerTable(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public AnswerTable(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public AnswerTable(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: String

  /** getter for String - gets 
   * @generated */
  public String getString() {
    if (AnswerTable_Type.featOkTst && ((AnswerTable_Type)jcasType).casFeat_String == null)
      jcasType.jcas.throwFeatMissing("String", "org.kachako.types.centerexam.answertable.AnswerTable");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnswerTable_Type)jcasType).casFeatCode_String);}
    
  /** setter for String - sets  
   * @generated */
  public void setString(String v) {
    if (AnswerTable_Type.featOkTst && ((AnswerTable_Type)jcasType).casFeat_String == null)
      jcasType.jcas.throwFeatMissing("String", "org.kachako.types.centerexam.answertable.AnswerTable");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnswerTable_Type)jcasType).casFeatCode_String, v);}    
   
    
  //*--------------*
  //* Feature: totalScore

  /** getter for totalScore - gets 
   * @generated */
  public String getTotalScore() {
    if (AnswerTable_Type.featOkTst && ((AnswerTable_Type)jcasType).casFeat_totalScore == null)
      jcasType.jcas.throwFeatMissing("totalScore", "org.kachako.types.centerexam.answertable.AnswerTable");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnswerTable_Type)jcasType).casFeatCode_totalScore);}
    
  /** setter for totalScore - sets  
   * @generated */
  public void setTotalScore(String v) {
    if (AnswerTable_Type.featOkTst && ((AnswerTable_Type)jcasType).casFeat_totalScore == null)
      jcasType.jcas.throwFeatMissing("totalScore", "org.kachako.types.centerexam.answertable.AnswerTable");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnswerTable_Type)jcasType).casFeatCode_totalScore, v);}    
   
    
  //*--------------*
  //* Feature: perfectScore

  /** getter for perfectScore - gets 
   * @generated */
  public String getPerfectScore() {
    if (AnswerTable_Type.featOkTst && ((AnswerTable_Type)jcasType).casFeat_perfectScore == null)
      jcasType.jcas.throwFeatMissing("perfectScore", "org.kachako.types.centerexam.answertable.AnswerTable");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnswerTable_Type)jcasType).casFeatCode_perfectScore);}
    
  /** setter for perfectScore - sets  
   * @generated */
  public void setPerfectScore(String v) {
    if (AnswerTable_Type.featOkTst && ((AnswerTable_Type)jcasType).casFeat_perfectScore == null)
      jcasType.jcas.throwFeatMissing("perfectScore", "org.kachako.types.centerexam.answertable.AnswerTable");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnswerTable_Type)jcasType).casFeatCode_perfectScore, v);}    
   
    
  //*--------------*
  //* Feature: questions

  /** getter for questions - gets 
   * @generated */
  public String getQuestions() {
    if (AnswerTable_Type.featOkTst && ((AnswerTable_Type)jcasType).casFeat_questions == null)
      jcasType.jcas.throwFeatMissing("questions", "org.kachako.types.centerexam.answertable.AnswerTable");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnswerTable_Type)jcasType).casFeatCode_questions);}
    
  /** setter for questions - sets  
   * @generated */
  public void setQuestions(String v) {
    if (AnswerTable_Type.featOkTst && ((AnswerTable_Type)jcasType).casFeat_questions == null)
      jcasType.jcas.throwFeatMissing("questions", "org.kachako.types.centerexam.answertable.AnswerTable");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnswerTable_Type)jcasType).casFeatCode_questions, v);}    
   
    
  //*--------------*
  //* Feature: correctAnswers

  /** getter for correctAnswers - gets 
   * @generated */
  public String getCorrectAnswers() {
    if (AnswerTable_Type.featOkTst && ((AnswerTable_Type)jcasType).casFeat_correctAnswers == null)
      jcasType.jcas.throwFeatMissing("correctAnswers", "org.kachako.types.centerexam.answertable.AnswerTable");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnswerTable_Type)jcasType).casFeatCode_correctAnswers);}
    
  /** setter for correctAnswers - sets  
   * @generated */
  public void setCorrectAnswers(String v) {
    if (AnswerTable_Type.featOkTst && ((AnswerTable_Type)jcasType).casFeat_correctAnswers == null)
      jcasType.jcas.throwFeatMissing("correctAnswers", "org.kachako.types.centerexam.answertable.AnswerTable");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnswerTable_Type)jcasType).casFeatCode_correctAnswers, v);}    
   
    
  //*--------------*
  //* Feature: accuracyRate

  /** getter for accuracyRate - gets 
   * @generated */
  public String getAccuracyRate() {
    if (AnswerTable_Type.featOkTst && ((AnswerTable_Type)jcasType).casFeat_accuracyRate == null)
      jcasType.jcas.throwFeatMissing("accuracyRate", "org.kachako.types.centerexam.answertable.AnswerTable");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnswerTable_Type)jcasType).casFeatCode_accuracyRate);}
    
  /** setter for accuracyRate - sets  
   * @generated */
  public void setAccuracyRate(String v) {
    if (AnswerTable_Type.featOkTst && ((AnswerTable_Type)jcasType).casFeat_accuracyRate == null)
      jcasType.jcas.throwFeatMissing("accuracyRate", "org.kachako.types.centerexam.answertable.AnswerTable");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnswerTable_Type)jcasType).casFeatCode_accuracyRate, v);}    
   
    
  //*--------------*
  //* Feature: file

  /** getter for file - gets 
   * @generated */
  public String getFile() {
    if (AnswerTable_Type.featOkTst && ((AnswerTable_Type)jcasType).casFeat_file == null)
      jcasType.jcas.throwFeatMissing("file", "org.kachako.types.centerexam.answertable.AnswerTable");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnswerTable_Type)jcasType).casFeatCode_file);}
    
  /** setter for file - sets  
   * @generated */
  public void setFile(String v) {
    if (AnswerTable_Type.featOkTst && ((AnswerTable_Type)jcasType).casFeat_file == null)
      jcasType.jcas.throwFeatMissing("file", "org.kachako.types.centerexam.answertable.AnswerTable");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnswerTable_Type)jcasType).casFeatCode_file, v);}    
   
    
  //*--------------*
  //* Feature: selected

  /** getter for selected - gets 
   * @generated */
  public String getSelected() {
    if (AnswerTable_Type.featOkTst && ((AnswerTable_Type)jcasType).casFeat_selected == null)
      jcasType.jcas.throwFeatMissing("selected", "org.kachako.types.centerexam.answertable.AnswerTable");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnswerTable_Type)jcasType).casFeatCode_selected);}
    
  /** setter for selected - sets  
   * @generated */
  public void setSelected(String v) {
    if (AnswerTable_Type.featOkTst && ((AnswerTable_Type)jcasType).casFeat_selected == null)
      jcasType.jcas.throwFeatMissing("selected", "org.kachako.types.centerexam.answertable.AnswerTable");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnswerTable_Type)jcasType).casFeatCode_selected, v);}    
   
    
  //*--------------*
  //* Feature: numOfOptions

  /** getter for numOfOptions - gets 
   * @generated */
  public String getNumOfOptions() {
    if (AnswerTable_Type.featOkTst && ((AnswerTable_Type)jcasType).casFeat_numOfOptions == null)
      jcasType.jcas.throwFeatMissing("numOfOptions", "org.kachako.types.centerexam.answertable.AnswerTable");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnswerTable_Type)jcasType).casFeatCode_numOfOptions);}
    
  /** setter for numOfOptions - sets  
   * @generated */
  public void setNumOfOptions(String v) {
    if (AnswerTable_Type.featOkTst && ((AnswerTable_Type)jcasType).casFeat_numOfOptions == null)
      jcasType.jcas.throwFeatMissing("numOfOptions", "org.kachako.types.centerexam.answertable.AnswerTable");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnswerTable_Type)jcasType).casFeatCode_numOfOptions, v);}    
   
    
  //*--------------*
  //* Feature: rangeOfOptions

  /** getter for rangeOfOptions - gets 
   * @generated */
  public String getRangeOfOptions() {
    if (AnswerTable_Type.featOkTst && ((AnswerTable_Type)jcasType).casFeat_rangeOfOptions == null)
      jcasType.jcas.throwFeatMissing("rangeOfOptions", "org.kachako.types.centerexam.answertable.AnswerTable");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnswerTable_Type)jcasType).casFeatCode_rangeOfOptions);}
    
  /** setter for rangeOfOptions - sets  
   * @generated */
  public void setRangeOfOptions(String v) {
    if (AnswerTable_Type.featOkTst && ((AnswerTable_Type)jcasType).casFeat_rangeOfOptions == null)
      jcasType.jcas.throwFeatMissing("rangeOfOptions", "org.kachako.types.centerexam.answertable.AnswerTable");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnswerTable_Type)jcasType).casFeatCode_rangeOfOptions, v);}    
  }

    