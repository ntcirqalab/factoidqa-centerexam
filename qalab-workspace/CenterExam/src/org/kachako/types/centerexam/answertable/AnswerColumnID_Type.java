
/* First created by JCasGen Fri Mar 08 11:24:40 JST 2013 */
package org.kachako.types.centerexam.answertable;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Tue Aug 06 10:15:21 JST 2013
 * @generated */
public class AnswerColumnID_Type extends AnswerTableTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (AnswerColumnID_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = AnswerColumnID_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new AnswerColumnID(addr, AnswerColumnID_Type.this);
  			   AnswerColumnID_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new AnswerColumnID(addr, AnswerColumnID_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = AnswerColumnID.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.answertable.AnswerColumnID");
 
  /** @generated */
  final Feature casFeat_String;
  /** @generated */
  final int     casFeatCode_String;
  /** @generated */ 
  public String getString(int addr) {
        if (featOkTst && casFeat_String == null)
      jcas.throwFeatMissing("String", "org.kachako.types.centerexam.answertable.AnswerColumnID");
    return ll_cas.ll_getStringValue(addr, casFeatCode_String);
  }
  /** @generated */    
  public void setString(int addr, String v) {
        if (featOkTst && casFeat_String == null)
      jcas.throwFeatMissing("String", "org.kachako.types.centerexam.answertable.AnswerColumnID");
    ll_cas.ll_setStringValue(addr, casFeatCode_String, v);}
    
  
 
  /** @generated */
  final Feature casFeat_TF;
  /** @generated */
  final int     casFeatCode_TF;
  /** @generated */ 
  public boolean getTF(int addr) {
        if (featOkTst && casFeat_TF == null)
      jcas.throwFeatMissing("TF", "org.kachako.types.centerexam.answertable.AnswerColumnID");
    return ll_cas.ll_getBooleanValue(addr, casFeatCode_TF);
  }
  /** @generated */    
  public void setTF(int addr, boolean v) {
        if (featOkTst && casFeat_TF == null)
      jcas.throwFeatMissing("TF", "org.kachako.types.centerexam.answertable.AnswerColumnID");
    ll_cas.ll_setBooleanValue(addr, casFeatCode_TF, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public AnswerColumnID_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_String = jcas.getRequiredFeatureDE(casType, "String", "uima.cas.String", featOkTst);
    casFeatCode_String  = (null == casFeat_String) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_String).getCode();

 
    casFeat_TF = jcas.getRequiredFeatureDE(casType, "TF", "uima.cas.Boolean", featOkTst);
    casFeatCode_TF  = (null == casFeat_TF) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_TF).getCode();

  }
}



    