

/* First created by JCasGen Fri Mar 08 11:24:40 JST 2013 */
package org.kachako.types.centerexam.answertable;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Tue Aug 06 10:15:21 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/answerTableTypeSystem.xml
 * @generated */
public class Answer extends AnswerTableTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Answer.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Answer() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Answer(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Answer(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Answer(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: systemAnswer

  /** getter for systemAnswer - gets 
   * @generated */
  public String getSystemAnswer() {
    if (Answer_Type.featOkTst && ((Answer_Type)jcasType).casFeat_systemAnswer == null)
      jcasType.jcas.throwFeatMissing("systemAnswer", "org.kachako.types.centerexam.answertable.Answer");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Answer_Type)jcasType).casFeatCode_systemAnswer);}
    
  /** setter for systemAnswer - sets  
   * @generated */
  public void setSystemAnswer(String v) {
    if (Answer_Type.featOkTst && ((Answer_Type)jcasType).casFeat_systemAnswer == null)
      jcasType.jcas.throwFeatMissing("systemAnswer", "org.kachako.types.centerexam.answertable.Answer");
    jcasType.ll_cas.ll_setStringValue(addr, ((Answer_Type)jcasType).casFeatCode_systemAnswer, v);}    
   
    
  //*--------------*
  //* Feature: String

  /** getter for String - gets 
   * @generated */
  public String getString() {
    if (Answer_Type.featOkTst && ((Answer_Type)jcasType).casFeat_String == null)
      jcasType.jcas.throwFeatMissing("String", "org.kachako.types.centerexam.answertable.Answer");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Answer_Type)jcasType).casFeatCode_String);}
    
  /** setter for String - sets  
   * @generated */
  public void setString(String v) {
    if (Answer_Type.featOkTst && ((Answer_Type)jcasType).casFeat_String == null)
      jcasType.jcas.throwFeatMissing("String", "org.kachako.types.centerexam.answertable.Answer");
    jcasType.ll_cas.ll_setStringValue(addr, ((Answer_Type)jcasType).casFeatCode_String, v);}    
   
    
  //*--------------*
  //* Feature: ansColumn

  /** getter for ansColumn - gets 
   * @generated */
  public String getAnsColumn() {
    if (Answer_Type.featOkTst && ((Answer_Type)jcasType).casFeat_ansColumn == null)
      jcasType.jcas.throwFeatMissing("ansColumn", "org.kachako.types.centerexam.answertable.Answer");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Answer_Type)jcasType).casFeatCode_ansColumn);}
    
  /** setter for ansColumn - sets  
   * @generated */
  public void setAnsColumn(String v) {
    if (Answer_Type.featOkTst && ((Answer_Type)jcasType).casFeat_ansColumn == null)
      jcasType.jcas.throwFeatMissing("ansColumn", "org.kachako.types.centerexam.answertable.Answer");
    jcasType.ll_cas.ll_setStringValue(addr, ((Answer_Type)jcasType).casFeatCode_ansColumn, v);}    
  }

    