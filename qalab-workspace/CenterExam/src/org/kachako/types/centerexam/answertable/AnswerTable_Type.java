
/* First created by JCasGen Fri Mar 08 11:24:40 JST 2013 */
package org.kachako.types.centerexam.answertable;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Tue Aug 06 10:15:22 JST 2013
 * @generated */
public class AnswerTable_Type extends AnswerTableTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (AnswerTable_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = AnswerTable_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new AnswerTable(addr, AnswerTable_Type.this);
  			   AnswerTable_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new AnswerTable(addr, AnswerTable_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = AnswerTable.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.answertable.AnswerTable");
 
  /** @generated */
  final Feature casFeat_String;
  /** @generated */
  final int     casFeatCode_String;
  /** @generated */ 
  public String getString(int addr) {
        if (featOkTst && casFeat_String == null)
      jcas.throwFeatMissing("String", "org.kachako.types.centerexam.answertable.AnswerTable");
    return ll_cas.ll_getStringValue(addr, casFeatCode_String);
  }
  /** @generated */    
  public void setString(int addr, String v) {
        if (featOkTst && casFeat_String == null)
      jcas.throwFeatMissing("String", "org.kachako.types.centerexam.answertable.AnswerTable");
    ll_cas.ll_setStringValue(addr, casFeatCode_String, v);}
    
  
 
  /** @generated */
  final Feature casFeat_totalScore;
  /** @generated */
  final int     casFeatCode_totalScore;
  /** @generated */ 
  public String getTotalScore(int addr) {
        if (featOkTst && casFeat_totalScore == null)
      jcas.throwFeatMissing("totalScore", "org.kachako.types.centerexam.answertable.AnswerTable");
    return ll_cas.ll_getStringValue(addr, casFeatCode_totalScore);
  }
  /** @generated */    
  public void setTotalScore(int addr, String v) {
        if (featOkTst && casFeat_totalScore == null)
      jcas.throwFeatMissing("totalScore", "org.kachako.types.centerexam.answertable.AnswerTable");
    ll_cas.ll_setStringValue(addr, casFeatCode_totalScore, v);}
    
  
 
  /** @generated */
  final Feature casFeat_perfectScore;
  /** @generated */
  final int     casFeatCode_perfectScore;
  /** @generated */ 
  public String getPerfectScore(int addr) {
        if (featOkTst && casFeat_perfectScore == null)
      jcas.throwFeatMissing("perfectScore", "org.kachako.types.centerexam.answertable.AnswerTable");
    return ll_cas.ll_getStringValue(addr, casFeatCode_perfectScore);
  }
  /** @generated */    
  public void setPerfectScore(int addr, String v) {
        if (featOkTst && casFeat_perfectScore == null)
      jcas.throwFeatMissing("perfectScore", "org.kachako.types.centerexam.answertable.AnswerTable");
    ll_cas.ll_setStringValue(addr, casFeatCode_perfectScore, v);}
    
  
 
  /** @generated */
  final Feature casFeat_questions;
  /** @generated */
  final int     casFeatCode_questions;
  /** @generated */ 
  public String getQuestions(int addr) {
        if (featOkTst && casFeat_questions == null)
      jcas.throwFeatMissing("questions", "org.kachako.types.centerexam.answertable.AnswerTable");
    return ll_cas.ll_getStringValue(addr, casFeatCode_questions);
  }
  /** @generated */    
  public void setQuestions(int addr, String v) {
        if (featOkTst && casFeat_questions == null)
      jcas.throwFeatMissing("questions", "org.kachako.types.centerexam.answertable.AnswerTable");
    ll_cas.ll_setStringValue(addr, casFeatCode_questions, v);}
    
  
 
  /** @generated */
  final Feature casFeat_correctAnswers;
  /** @generated */
  final int     casFeatCode_correctAnswers;
  /** @generated */ 
  public String getCorrectAnswers(int addr) {
        if (featOkTst && casFeat_correctAnswers == null)
      jcas.throwFeatMissing("correctAnswers", "org.kachako.types.centerexam.answertable.AnswerTable");
    return ll_cas.ll_getStringValue(addr, casFeatCode_correctAnswers);
  }
  /** @generated */    
  public void setCorrectAnswers(int addr, String v) {
        if (featOkTst && casFeat_correctAnswers == null)
      jcas.throwFeatMissing("correctAnswers", "org.kachako.types.centerexam.answertable.AnswerTable");
    ll_cas.ll_setStringValue(addr, casFeatCode_correctAnswers, v);}
    
  
 
  /** @generated */
  final Feature casFeat_accuracyRate;
  /** @generated */
  final int     casFeatCode_accuracyRate;
  /** @generated */ 
  public String getAccuracyRate(int addr) {
        if (featOkTst && casFeat_accuracyRate == null)
      jcas.throwFeatMissing("accuracyRate", "org.kachako.types.centerexam.answertable.AnswerTable");
    return ll_cas.ll_getStringValue(addr, casFeatCode_accuracyRate);
  }
  /** @generated */    
  public void setAccuracyRate(int addr, String v) {
        if (featOkTst && casFeat_accuracyRate == null)
      jcas.throwFeatMissing("accuracyRate", "org.kachako.types.centerexam.answertable.AnswerTable");
    ll_cas.ll_setStringValue(addr, casFeatCode_accuracyRate, v);}
    
  
 
  /** @generated */
  final Feature casFeat_file;
  /** @generated */
  final int     casFeatCode_file;
  /** @generated */ 
  public String getFile(int addr) {
        if (featOkTst && casFeat_file == null)
      jcas.throwFeatMissing("file", "org.kachako.types.centerexam.answertable.AnswerTable");
    return ll_cas.ll_getStringValue(addr, casFeatCode_file);
  }
  /** @generated */    
  public void setFile(int addr, String v) {
        if (featOkTst && casFeat_file == null)
      jcas.throwFeatMissing("file", "org.kachako.types.centerexam.answertable.AnswerTable");
    ll_cas.ll_setStringValue(addr, casFeatCode_file, v);}
    
  
 
  /** @generated */
  final Feature casFeat_selected;
  /** @generated */
  final int     casFeatCode_selected;
  /** @generated */ 
  public String getSelected(int addr) {
        if (featOkTst && casFeat_selected == null)
      jcas.throwFeatMissing("selected", "org.kachako.types.centerexam.answertable.AnswerTable");
    return ll_cas.ll_getStringValue(addr, casFeatCode_selected);
  }
  /** @generated */    
  public void setSelected(int addr, String v) {
        if (featOkTst && casFeat_selected == null)
      jcas.throwFeatMissing("selected", "org.kachako.types.centerexam.answertable.AnswerTable");
    ll_cas.ll_setStringValue(addr, casFeatCode_selected, v);}
    
  
 
  /** @generated */
  final Feature casFeat_numOfOptions;
  /** @generated */
  final int     casFeatCode_numOfOptions;
  /** @generated */ 
  public String getNumOfOptions(int addr) {
        if (featOkTst && casFeat_numOfOptions == null)
      jcas.throwFeatMissing("numOfOptions", "org.kachako.types.centerexam.answertable.AnswerTable");
    return ll_cas.ll_getStringValue(addr, casFeatCode_numOfOptions);
  }
  /** @generated */    
  public void setNumOfOptions(int addr, String v) {
        if (featOkTst && casFeat_numOfOptions == null)
      jcas.throwFeatMissing("numOfOptions", "org.kachako.types.centerexam.answertable.AnswerTable");
    ll_cas.ll_setStringValue(addr, casFeatCode_numOfOptions, v);}
    
  
 
  /** @generated */
  final Feature casFeat_rangeOfOptions;
  /** @generated */
  final int     casFeatCode_rangeOfOptions;
  /** @generated */ 
  public String getRangeOfOptions(int addr) {
        if (featOkTst && casFeat_rangeOfOptions == null)
      jcas.throwFeatMissing("rangeOfOptions", "org.kachako.types.centerexam.answertable.AnswerTable");
    return ll_cas.ll_getStringValue(addr, casFeatCode_rangeOfOptions);
  }
  /** @generated */    
  public void setRangeOfOptions(int addr, String v) {
        if (featOkTst && casFeat_rangeOfOptions == null)
      jcas.throwFeatMissing("rangeOfOptions", "org.kachako.types.centerexam.answertable.AnswerTable");
    ll_cas.ll_setStringValue(addr, casFeatCode_rangeOfOptions, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public AnswerTable_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_String = jcas.getRequiredFeatureDE(casType, "String", "uima.cas.String", featOkTst);
    casFeatCode_String  = (null == casFeat_String) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_String).getCode();

 
    casFeat_totalScore = jcas.getRequiredFeatureDE(casType, "totalScore", "uima.cas.String", featOkTst);
    casFeatCode_totalScore  = (null == casFeat_totalScore) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_totalScore).getCode();

 
    casFeat_perfectScore = jcas.getRequiredFeatureDE(casType, "perfectScore", "uima.cas.String", featOkTst);
    casFeatCode_perfectScore  = (null == casFeat_perfectScore) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_perfectScore).getCode();

 
    casFeat_questions = jcas.getRequiredFeatureDE(casType, "questions", "uima.cas.String", featOkTst);
    casFeatCode_questions  = (null == casFeat_questions) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_questions).getCode();

 
    casFeat_correctAnswers = jcas.getRequiredFeatureDE(casType, "correctAnswers", "uima.cas.String", featOkTst);
    casFeatCode_correctAnswers  = (null == casFeat_correctAnswers) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_correctAnswers).getCode();

 
    casFeat_accuracyRate = jcas.getRequiredFeatureDE(casType, "accuracyRate", "uima.cas.String", featOkTst);
    casFeatCode_accuracyRate  = (null == casFeat_accuracyRate) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_accuracyRate).getCode();

 
    casFeat_file = jcas.getRequiredFeatureDE(casType, "file", "uima.cas.String", featOkTst);
    casFeatCode_file  = (null == casFeat_file) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_file).getCode();

 
    casFeat_selected = jcas.getRequiredFeatureDE(casType, "selected", "uima.cas.String", featOkTst);
    casFeatCode_selected  = (null == casFeat_selected) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_selected).getCode();

 
    casFeat_numOfOptions = jcas.getRequiredFeatureDE(casType, "numOfOptions", "uima.cas.String", featOkTst);
    casFeatCode_numOfOptions  = (null == casFeat_numOfOptions) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_numOfOptions).getCode();

 
    casFeat_rangeOfOptions = jcas.getRequiredFeatureDE(casType, "rangeOfOptions", "uima.cas.String", featOkTst);
    casFeatCode_rangeOfOptions  = (null == casFeat_rangeOfOptions) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_rangeOfOptions).getCode();

  }
}



    