

/* First created by JCasGen Tue Aug 06 10:15:16 JST 2013 */
package org.kachako.types.centerexam.answertable;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Tue Aug 06 10:15:22 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/answerTableTypeSystem.xml
 * @generated */
public class Confidence extends AnswerTableTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Confidence.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Confidence() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Confidence(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Confidence(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Confidence(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: String

  /** getter for String - gets 
   * @generated */
  public String getString() {
    if (Confidence_Type.featOkTst && ((Confidence_Type)jcasType).casFeat_String == null)
      jcasType.jcas.throwFeatMissing("String", "org.kachako.types.centerexam.answertable.Confidence");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Confidence_Type)jcasType).casFeatCode_String);}
    
  /** setter for String - sets  
   * @generated */
  public void setString(String v) {
    if (Confidence_Type.featOkTst && ((Confidence_Type)jcasType).casFeat_String == null)
      jcasType.jcas.throwFeatMissing("String", "org.kachako.types.centerexam.answertable.Confidence");
    jcasType.ll_cas.ll_setStringValue(addr, ((Confidence_Type)jcasType).casFeatCode_String, v);}    
  }

    