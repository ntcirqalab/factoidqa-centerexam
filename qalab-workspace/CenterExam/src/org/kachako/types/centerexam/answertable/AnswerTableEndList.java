

/* First created by JCasGen Fri Mar 08 11:24:40 JST 2013 */
package org.kachako.types.centerexam.answertable;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.TOP;


/** 
 * Updated by JCasGen Tue Aug 06 10:15:22 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/answerTableTypeSystem.xml
 * @generated */
public class AnswerTableEndList extends TOP {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(AnswerTableEndList.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected AnswerTableEndList() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public AnswerTableEndList(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public AnswerTableEndList(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: EndList

  /** getter for EndList - gets 
   * @generated */
  public FSArray getEndList() {
    if (AnswerTableEndList_Type.featOkTst && ((AnswerTableEndList_Type)jcasType).casFeat_EndList == null)
      jcasType.jcas.throwFeatMissing("EndList", "org.kachako.types.centerexam.answertable.AnswerTableEndList");
    return (FSArray)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((AnswerTableEndList_Type)jcasType).casFeatCode_EndList)));}
    
  /** setter for EndList - sets  
   * @generated */
  public void setEndList(FSArray v) {
    if (AnswerTableEndList_Type.featOkTst && ((AnswerTableEndList_Type)jcasType).casFeat_EndList == null)
      jcasType.jcas.throwFeatMissing("EndList", "org.kachako.types.centerexam.answertable.AnswerTableEndList");
    jcasType.ll_cas.ll_setRefValue(addr, ((AnswerTableEndList_Type)jcasType).casFeatCode_EndList, jcasType.ll_cas.ll_getFSRef(v));}    
    
  /** indexed getter for EndList - gets an indexed value - 
   * @generated */
  public AnswerTableTag getEndList(int i) {
    if (AnswerTableEndList_Type.featOkTst && ((AnswerTableEndList_Type)jcasType).casFeat_EndList == null)
      jcasType.jcas.throwFeatMissing("EndList", "org.kachako.types.centerexam.answertable.AnswerTableEndList");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((AnswerTableEndList_Type)jcasType).casFeatCode_EndList), i);
    return (AnswerTableTag)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((AnswerTableEndList_Type)jcasType).casFeatCode_EndList), i)));}

  /** indexed setter for EndList - sets an indexed value - 
   * @generated */
  public void setEndList(int i, AnswerTableTag v) { 
    if (AnswerTableEndList_Type.featOkTst && ((AnswerTableEndList_Type)jcasType).casFeat_EndList == null)
      jcasType.jcas.throwFeatMissing("EndList", "org.kachako.types.centerexam.answertable.AnswerTableEndList");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((AnswerTableEndList_Type)jcasType).casFeatCode_EndList), i);
    jcasType.ll_cas.ll_setRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((AnswerTableEndList_Type)jcasType).casFeatCode_EndList), i, jcasType.ll_cas.ll_getFSRef(v));}
  }

    