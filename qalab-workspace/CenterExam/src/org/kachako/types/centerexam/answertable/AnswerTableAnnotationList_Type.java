
/* First created by JCasGen Fri Mar 08 11:24:40 JST 2013 */
package org.kachako.types.centerexam.answertable;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.cas.TOP_Type;

/** 
 * Updated by JCasGen Tue Aug 06 10:15:22 JST 2013
 * @generated */
public class AnswerTableAnnotationList_Type extends TOP_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (AnswerTableAnnotationList_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = AnswerTableAnnotationList_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new AnswerTableAnnotationList(addr, AnswerTableAnnotationList_Type.this);
  			   AnswerTableAnnotationList_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new AnswerTableAnnotationList(addr, AnswerTableAnnotationList_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = AnswerTableAnnotationList.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.answertable.AnswerTableAnnotationList");
 
  /** @generated */
  final Feature casFeat_BeginList;
  /** @generated */
  final int     casFeatCode_BeginList;
  /** @generated */ 
  public int getBeginList(int addr) {
        if (featOkTst && casFeat_BeginList == null)
      jcas.throwFeatMissing("BeginList", "org.kachako.types.centerexam.answertable.AnswerTableAnnotationList");
    return ll_cas.ll_getRefValue(addr, casFeatCode_BeginList);
  }
  /** @generated */    
  public void setBeginList(int addr, int v) {
        if (featOkTst && casFeat_BeginList == null)
      jcas.throwFeatMissing("BeginList", "org.kachako.types.centerexam.answertable.AnswerTableAnnotationList");
    ll_cas.ll_setRefValue(addr, casFeatCode_BeginList, v);}
    
   /** @generated */
  public int getBeginList(int addr, int i) {
        if (featOkTst && casFeat_BeginList == null)
      jcas.throwFeatMissing("BeginList", "org.kachako.types.centerexam.answertable.AnswerTableAnnotationList");
    if (lowLevelTypeChecks)
      return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_BeginList), i, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_BeginList), i);
  return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_BeginList), i);
  }
   
  /** @generated */ 
  public void setBeginList(int addr, int i, int v) {
        if (featOkTst && casFeat_BeginList == null)
      jcas.throwFeatMissing("BeginList", "org.kachako.types.centerexam.answertable.AnswerTableAnnotationList");
    if (lowLevelTypeChecks)
      ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_BeginList), i, v, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_BeginList), i);
    ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_BeginList), i, v);
  }
 



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public AnswerTableAnnotationList_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_BeginList = jcas.getRequiredFeatureDE(casType, "BeginList", "uima.cas.FSArray", featOkTst);
    casFeatCode_BeginList  = (null == casFeat_BeginList) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_BeginList).getCode();

  }
}



    