

/* First created by JCasGen Fri Mar 08 11:24:40 JST 2013 */
package org.kachako.types.centerexam.answertable;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Tue Aug 06 10:15:21 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/answerTableTypeSystem.xml
 * @generated */
public class AnswerColumnID extends AnswerTableTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(AnswerColumnID.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected AnswerColumnID() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public AnswerColumnID(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public AnswerColumnID(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public AnswerColumnID(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: String

  /** getter for String - gets 
   * @generated */
  public String getString() {
    if (AnswerColumnID_Type.featOkTst && ((AnswerColumnID_Type)jcasType).casFeat_String == null)
      jcasType.jcas.throwFeatMissing("String", "org.kachako.types.centerexam.answertable.AnswerColumnID");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnswerColumnID_Type)jcasType).casFeatCode_String);}
    
  /** setter for String - sets  
   * @generated */
  public void setString(String v) {
    if (AnswerColumnID_Type.featOkTst && ((AnswerColumnID_Type)jcasType).casFeat_String == null)
      jcasType.jcas.throwFeatMissing("String", "org.kachako.types.centerexam.answertable.AnswerColumnID");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnswerColumnID_Type)jcasType).casFeatCode_String, v);}    
   
    
  //*--------------*
  //* Feature: TF

  /** getter for TF - gets 
   * @generated */
  public boolean getTF() {
    if (AnswerColumnID_Type.featOkTst && ((AnswerColumnID_Type)jcasType).casFeat_TF == null)
      jcasType.jcas.throwFeatMissing("TF", "org.kachako.types.centerexam.answertable.AnswerColumnID");
    return jcasType.ll_cas.ll_getBooleanValue(addr, ((AnswerColumnID_Type)jcasType).casFeatCode_TF);}
    
  /** setter for TF - sets  
   * @generated */
  public void setTF(boolean v) {
    if (AnswerColumnID_Type.featOkTst && ((AnswerColumnID_Type)jcasType).casFeat_TF == null)
      jcasType.jcas.throwFeatMissing("TF", "org.kachako.types.centerexam.answertable.AnswerColumnID");
    jcasType.ll_cas.ll_setBooleanValue(addr, ((AnswerColumnID_Type)jcasType).casFeatCode_TF, v);}    
  }

    