
/* First created by JCasGen Fri Mar 08 11:24:40 JST 2013 */
package org.kachako.types.centerexam.answertable;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Tue Aug 06 10:15:22 JST 2013
 * @generated */
public class QuestionID_Type extends AnswerTableTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (QuestionID_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = QuestionID_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new QuestionID(addr, QuestionID_Type.this);
  			   QuestionID_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new QuestionID(addr, QuestionID_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = QuestionID.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.answertable.QuestionID");
 
  /** @generated */
  final Feature casFeat_String;
  /** @generated */
  final int     casFeatCode_String;
  /** @generated */ 
  public String getString(int addr) {
        if (featOkTst && casFeat_String == null)
      jcas.throwFeatMissing("String", "org.kachako.types.centerexam.answertable.QuestionID");
    return ll_cas.ll_getStringValue(addr, casFeatCode_String);
  }
  /** @generated */    
  public void setString(int addr, String v) {
        if (featOkTst && casFeat_String == null)
      jcas.throwFeatMissing("String", "org.kachako.types.centerexam.answertable.QuestionID");
    ll_cas.ll_setStringValue(addr, casFeatCode_String, v);}
    
  
 
  /** @generated */
  final Feature casFeat_parents_id;
  /** @generated */
  final int     casFeatCode_parents_id;
  /** @generated */ 
  public String getParents_id(int addr) {
        if (featOkTst && casFeat_parents_id == null)
      jcas.throwFeatMissing("parents_id", "org.kachako.types.centerexam.answertable.QuestionID");
    return ll_cas.ll_getStringValue(addr, casFeatCode_parents_id);
  }
  /** @generated */    
  public void setParents_id(int addr, String v) {
        if (featOkTst && casFeat_parents_id == null)
      jcas.throwFeatMissing("parents_id", "org.kachako.types.centerexam.answertable.QuestionID");
    ll_cas.ll_setStringValue(addr, casFeatCode_parents_id, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public QuestionID_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_String = jcas.getRequiredFeatureDE(casType, "String", "uima.cas.String", featOkTst);
    casFeatCode_String  = (null == casFeat_String) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_String).getCode();

 
    casFeat_parents_id = jcas.getRequiredFeatureDE(casType, "parents_id", "uima.cas.String", featOkTst);
    casFeatCode_parents_id  = (null == casFeat_parents_id) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_parents_id).getCode();

  }
}



    