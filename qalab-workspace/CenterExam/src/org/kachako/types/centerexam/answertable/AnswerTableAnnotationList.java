

/* First created by JCasGen Fri Mar 08 11:24:40 JST 2013 */
package org.kachako.types.centerexam.answertable;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.TOP;


/** 
 * Updated by JCasGen Tue Aug 06 10:15:22 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/answerTableTypeSystem.xml
 * @generated */
public class AnswerTableAnnotationList extends TOP {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(AnswerTableAnnotationList.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected AnswerTableAnnotationList() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public AnswerTableAnnotationList(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public AnswerTableAnnotationList(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: BeginList

  /** getter for BeginList - gets 
   * @generated */
  public FSArray getBeginList() {
    if (AnswerTableAnnotationList_Type.featOkTst && ((AnswerTableAnnotationList_Type)jcasType).casFeat_BeginList == null)
      jcasType.jcas.throwFeatMissing("BeginList", "org.kachako.types.centerexam.answertable.AnswerTableAnnotationList");
    return (FSArray)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((AnswerTableAnnotationList_Type)jcasType).casFeatCode_BeginList)));}
    
  /** setter for BeginList - sets  
   * @generated */
  public void setBeginList(FSArray v) {
    if (AnswerTableAnnotationList_Type.featOkTst && ((AnswerTableAnnotationList_Type)jcasType).casFeat_BeginList == null)
      jcasType.jcas.throwFeatMissing("BeginList", "org.kachako.types.centerexam.answertable.AnswerTableAnnotationList");
    jcasType.ll_cas.ll_setRefValue(addr, ((AnswerTableAnnotationList_Type)jcasType).casFeatCode_BeginList, jcasType.ll_cas.ll_getFSRef(v));}    
    
  /** indexed getter for BeginList - gets an indexed value - 
   * @generated */
  public AnswerTableTag getBeginList(int i) {
    if (AnswerTableAnnotationList_Type.featOkTst && ((AnswerTableAnnotationList_Type)jcasType).casFeat_BeginList == null)
      jcasType.jcas.throwFeatMissing("BeginList", "org.kachako.types.centerexam.answertable.AnswerTableAnnotationList");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((AnswerTableAnnotationList_Type)jcasType).casFeatCode_BeginList), i);
    return (AnswerTableTag)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((AnswerTableAnnotationList_Type)jcasType).casFeatCode_BeginList), i)));}

  /** indexed setter for BeginList - sets an indexed value - 
   * @generated */
  public void setBeginList(int i, AnswerTableTag v) { 
    if (AnswerTableAnnotationList_Type.featOkTst && ((AnswerTableAnnotationList_Type)jcasType).casFeat_BeginList == null)
      jcasType.jcas.throwFeatMissing("BeginList", "org.kachako.types.centerexam.answertable.AnswerTableAnnotationList");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((AnswerTableAnnotationList_Type)jcasType).casFeatCode_BeginList), i);
    jcasType.ll_cas.ll_setRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((AnswerTableAnnotationList_Type)jcasType).casFeatCode_BeginList), i, jcasType.ll_cas.ll_getFSRef(v));}
  }

    