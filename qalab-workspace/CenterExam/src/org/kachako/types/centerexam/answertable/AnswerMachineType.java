

/* First created by JCasGen Fri Mar 08 11:24:40 JST 2013 */
package org.kachako.types.centerexam.answertable;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Tue Aug 06 10:15:21 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/answerTableTypeSystem.xml
 * @generated */
public class AnswerMachineType extends AnswerTableTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(AnswerMachineType.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected AnswerMachineType() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public AnswerMachineType(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public AnswerMachineType(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public AnswerMachineType(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: componentID

  /** getter for componentID - gets 
   * @generated */
  public String getComponentID() {
    if (AnswerMachineType_Type.featOkTst && ((AnswerMachineType_Type)jcasType).casFeat_componentID == null)
      jcasType.jcas.throwFeatMissing("componentID", "org.kachako.types.centerexam.answertable.AnswerMachineType");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnswerMachineType_Type)jcasType).casFeatCode_componentID);}
    
  /** setter for componentID - sets  
   * @generated */
  public void setComponentID(String v) {
    if (AnswerMachineType_Type.featOkTst && ((AnswerMachineType_Type)jcasType).casFeat_componentID == null)
      jcasType.jcas.throwFeatMissing("componentID", "org.kachako.types.centerexam.answertable.AnswerMachineType");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnswerMachineType_Type)jcasType).casFeatCode_componentID, v);}    
   
    
  //*--------------*
  //* Feature: score

  /** getter for score - gets 
   * @generated */
  public String getScore() {
    if (AnswerMachineType_Type.featOkTst && ((AnswerMachineType_Type)jcasType).casFeat_score == null)
      jcasType.jcas.throwFeatMissing("score", "org.kachako.types.centerexam.answertable.AnswerMachineType");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnswerMachineType_Type)jcasType).casFeatCode_score);}
    
  /** setter for score - sets  
   * @generated */
  public void setScore(String v) {
    if (AnswerMachineType_Type.featOkTst && ((AnswerMachineType_Type)jcasType).casFeat_score == null)
      jcasType.jcas.throwFeatMissing("score", "org.kachako.types.centerexam.answertable.AnswerMachineType");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnswerMachineType_Type)jcasType).casFeatCode_score, v);}    
   
    
  //*--------------*
  //* Feature: rate

  /** getter for rate - gets 
   * @generated */
  public String getRate() {
    if (AnswerMachineType_Type.featOkTst && ((AnswerMachineType_Type)jcasType).casFeat_rate == null)
      jcasType.jcas.throwFeatMissing("rate", "org.kachako.types.centerexam.answertable.AnswerMachineType");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnswerMachineType_Type)jcasType).casFeatCode_rate);}
    
  /** setter for rate - sets  
   * @generated */
  public void setRate(String v) {
    if (AnswerMachineType_Type.featOkTst && ((AnswerMachineType_Type)jcasType).casFeat_rate == null)
      jcasType.jcas.throwFeatMissing("rate", "org.kachako.types.centerexam.answertable.AnswerMachineType");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnswerMachineType_Type)jcasType).casFeatCode_rate, v);}    
   
    
  //*--------------*
  //* Feature: numOfQuestion

  /** getter for numOfQuestion - gets 
   * @generated */
  public String getNumOfQuestion() {
    if (AnswerMachineType_Type.featOkTst && ((AnswerMachineType_Type)jcasType).casFeat_numOfQuestion == null)
      jcasType.jcas.throwFeatMissing("numOfQuestion", "org.kachako.types.centerexam.answertable.AnswerMachineType");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnswerMachineType_Type)jcasType).casFeatCode_numOfQuestion);}
    
  /** setter for numOfQuestion - sets  
   * @generated */
  public void setNumOfQuestion(String v) {
    if (AnswerMachineType_Type.featOkTst && ((AnswerMachineType_Type)jcasType).casFeat_numOfQuestion == null)
      jcasType.jcas.throwFeatMissing("numOfQuestion", "org.kachako.types.centerexam.answertable.AnswerMachineType");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnswerMachineType_Type)jcasType).casFeatCode_numOfQuestion, v);}    
   
    
  //*--------------*
  //* Feature: numOfCorrect

  /** getter for numOfCorrect - gets 
   * @generated */
  public String getNumOfCorrect() {
    if (AnswerMachineType_Type.featOkTst && ((AnswerMachineType_Type)jcasType).casFeat_numOfCorrect == null)
      jcasType.jcas.throwFeatMissing("numOfCorrect", "org.kachako.types.centerexam.answertable.AnswerMachineType");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnswerMachineType_Type)jcasType).casFeatCode_numOfCorrect);}
    
  /** setter for numOfCorrect - sets  
   * @generated */
  public void setNumOfCorrect(String v) {
    if (AnswerMachineType_Type.featOkTst && ((AnswerMachineType_Type)jcasType).casFeat_numOfCorrect == null)
      jcasType.jcas.throwFeatMissing("numOfCorrect", "org.kachako.types.centerexam.answertable.AnswerMachineType");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnswerMachineType_Type)jcasType).casFeatCode_numOfCorrect, v);}    
   
    
  //*--------------*
  //* Feature: perfectScore

  /** getter for perfectScore - gets 
   * @generated */
  public String getPerfectScore() {
    if (AnswerMachineType_Type.featOkTst && ((AnswerMachineType_Type)jcasType).casFeat_perfectScore == null)
      jcasType.jcas.throwFeatMissing("perfectScore", "org.kachako.types.centerexam.answertable.AnswerMachineType");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnswerMachineType_Type)jcasType).casFeatCode_perfectScore);}
    
  /** setter for perfectScore - sets  
   * @generated */
  public void setPerfectScore(String v) {
    if (AnswerMachineType_Type.featOkTst && ((AnswerMachineType_Type)jcasType).casFeat_perfectScore == null)
      jcasType.jcas.throwFeatMissing("perfectScore", "org.kachako.types.centerexam.answertable.AnswerMachineType");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnswerMachineType_Type)jcasType).casFeatCode_perfectScore, v);}    
  }

    