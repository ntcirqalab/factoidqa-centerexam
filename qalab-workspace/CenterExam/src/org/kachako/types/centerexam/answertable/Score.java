

/* First created by JCasGen Fri Mar 08 11:24:40 JST 2013 */
package org.kachako.types.centerexam.answertable;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Tue Aug 06 10:15:22 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/answerTableTypeSystem.xml
 * @generated */
public class Score extends AnswerTableTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Score.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Score() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Score(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Score(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Score(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: String

  /** getter for String - gets 
   * @generated */
  public String getString() {
    if (Score_Type.featOkTst && ((Score_Type)jcasType).casFeat_String == null)
      jcasType.jcas.throwFeatMissing("String", "org.kachako.types.centerexam.answertable.Score");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Score_Type)jcasType).casFeatCode_String);}
    
  /** setter for String - sets  
   * @generated */
  public void setString(String v) {
    if (Score_Type.featOkTst && ((Score_Type)jcasType).casFeat_String == null)
      jcasType.jcas.throwFeatMissing("String", "org.kachako.types.centerexam.answertable.Score");
    jcasType.ll_cas.ll_setStringValue(addr, ((Score_Type)jcasType).casFeatCode_String, v);}    
  }

    