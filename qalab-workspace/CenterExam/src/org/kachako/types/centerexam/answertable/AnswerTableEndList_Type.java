
/* First created by JCasGen Fri Mar 08 11:24:40 JST 2013 */
package org.kachako.types.centerexam.answertable;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.cas.TOP_Type;

/** 
 * Updated by JCasGen Tue Aug 06 10:15:22 JST 2013
 * @generated */
public class AnswerTableEndList_Type extends TOP_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (AnswerTableEndList_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = AnswerTableEndList_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new AnswerTableEndList(addr, AnswerTableEndList_Type.this);
  			   AnswerTableEndList_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new AnswerTableEndList(addr, AnswerTableEndList_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = AnswerTableEndList.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.answertable.AnswerTableEndList");
 
  /** @generated */
  final Feature casFeat_EndList;
  /** @generated */
  final int     casFeatCode_EndList;
  /** @generated */ 
  public int getEndList(int addr) {
        if (featOkTst && casFeat_EndList == null)
      jcas.throwFeatMissing("EndList", "org.kachako.types.centerexam.answertable.AnswerTableEndList");
    return ll_cas.ll_getRefValue(addr, casFeatCode_EndList);
  }
  /** @generated */    
  public void setEndList(int addr, int v) {
        if (featOkTst && casFeat_EndList == null)
      jcas.throwFeatMissing("EndList", "org.kachako.types.centerexam.answertable.AnswerTableEndList");
    ll_cas.ll_setRefValue(addr, casFeatCode_EndList, v);}
    
   /** @generated */
  public int getEndList(int addr, int i) {
        if (featOkTst && casFeat_EndList == null)
      jcas.throwFeatMissing("EndList", "org.kachako.types.centerexam.answertable.AnswerTableEndList");
    if (lowLevelTypeChecks)
      return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_EndList), i, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_EndList), i);
  return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_EndList), i);
  }
   
  /** @generated */ 
  public void setEndList(int addr, int i, int v) {
        if (featOkTst && casFeat_EndList == null)
      jcas.throwFeatMissing("EndList", "org.kachako.types.centerexam.answertable.AnswerTableEndList");
    if (lowLevelTypeChecks)
      ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_EndList), i, v, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_EndList), i);
    ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_EndList), i, v);
  }
 



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public AnswerTableEndList_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_EndList = jcas.getRequiredFeatureDE(casType, "EndList", "uima.cas.FSArray", featOkTst);
    casFeatCode_EndList  = (null == casFeat_EndList) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_EndList).getCode();

  }
}



    