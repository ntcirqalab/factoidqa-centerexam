
/* First created by JCasGen Tue May 28 13:13:48 JST 2013 */
package org.kachako.types.centerexam.answertable;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Tue Aug 06 10:15:22 JST 2013
 * @generated */
public class Option_Type extends AnswerTableTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Option_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Option_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Option(addr, Option_Type.this);
  			   Option_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Option(addr, Option_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Option.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.answertable.Option");
 
  /** @generated */
  final Feature casFeat_section_ID;
  /** @generated */
  final int     casFeatCode_section_ID;
  /** @generated */ 
  public String getSection_ID(int addr) {
        if (featOkTst && casFeat_section_ID == null)
      jcas.throwFeatMissing("section_ID", "org.kachako.types.centerexam.answertable.Option");
    return ll_cas.ll_getStringValue(addr, casFeatCode_section_ID);
  }
  /** @generated */    
  public void setSection_ID(int addr, String v) {
        if (featOkTst && casFeat_section_ID == null)
      jcas.throwFeatMissing("section_ID", "org.kachako.types.centerexam.answertable.Option");
    ll_cas.ll_setStringValue(addr, casFeatCode_section_ID, v);}
    
  
 
  /** @generated */
  final Feature casFeat_anscolumns;
  /** @generated */
  final int     casFeatCode_anscolumns;
  /** @generated */ 
  public String getAnscolumns(int addr) {
        if (featOkTst && casFeat_anscolumns == null)
      jcas.throwFeatMissing("anscolumns", "org.kachako.types.centerexam.answertable.Option");
    return ll_cas.ll_getStringValue(addr, casFeatCode_anscolumns);
  }
  /** @generated */    
  public void setAnscolumns(int addr, String v) {
        if (featOkTst && casFeat_anscolumns == null)
      jcas.throwFeatMissing("anscolumns", "org.kachako.types.centerexam.answertable.Option");
    ll_cas.ll_setStringValue(addr, casFeatCode_anscolumns, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Option_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_section_ID = jcas.getRequiredFeatureDE(casType, "section_ID", "uima.cas.String", featOkTst);
    casFeatCode_section_ID  = (null == casFeat_section_ID) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_section_ID).getCode();

 
    casFeat_anscolumns = jcas.getRequiredFeatureDE(casType, "anscolumns", "uima.cas.String", featOkTst);
    casFeatCode_anscolumns  = (null == casFeat_anscolumns) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_anscolumns).getCode();

  }
}



    