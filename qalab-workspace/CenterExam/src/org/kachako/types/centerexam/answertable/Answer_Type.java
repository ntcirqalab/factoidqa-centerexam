
/* First created by JCasGen Fri Mar 08 11:24:40 JST 2013 */
package org.kachako.types.centerexam.answertable;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Tue Aug 06 10:15:21 JST 2013
 * @generated */
public class Answer_Type extends AnswerTableTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Answer_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Answer_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Answer(addr, Answer_Type.this);
  			   Answer_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Answer(addr, Answer_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Answer.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.answertable.Answer");
 
  /** @generated */
  final Feature casFeat_systemAnswer;
  /** @generated */
  final int     casFeatCode_systemAnswer;
  /** @generated */ 
  public String getSystemAnswer(int addr) {
        if (featOkTst && casFeat_systemAnswer == null)
      jcas.throwFeatMissing("systemAnswer", "org.kachako.types.centerexam.answertable.Answer");
    return ll_cas.ll_getStringValue(addr, casFeatCode_systemAnswer);
  }
  /** @generated */    
  public void setSystemAnswer(int addr, String v) {
        if (featOkTst && casFeat_systemAnswer == null)
      jcas.throwFeatMissing("systemAnswer", "org.kachako.types.centerexam.answertable.Answer");
    ll_cas.ll_setStringValue(addr, casFeatCode_systemAnswer, v);}
    
  
 
  /** @generated */
  final Feature casFeat_String;
  /** @generated */
  final int     casFeatCode_String;
  /** @generated */ 
  public String getString(int addr) {
        if (featOkTst && casFeat_String == null)
      jcas.throwFeatMissing("String", "org.kachako.types.centerexam.answertable.Answer");
    return ll_cas.ll_getStringValue(addr, casFeatCode_String);
  }
  /** @generated */    
  public void setString(int addr, String v) {
        if (featOkTst && casFeat_String == null)
      jcas.throwFeatMissing("String", "org.kachako.types.centerexam.answertable.Answer");
    ll_cas.ll_setStringValue(addr, casFeatCode_String, v);}
    
  
 
  /** @generated */
  final Feature casFeat_ansColumn;
  /** @generated */
  final int     casFeatCode_ansColumn;
  /** @generated */ 
  public String getAnsColumn(int addr) {
        if (featOkTst && casFeat_ansColumn == null)
      jcas.throwFeatMissing("ansColumn", "org.kachako.types.centerexam.answertable.Answer");
    return ll_cas.ll_getStringValue(addr, casFeatCode_ansColumn);
  }
  /** @generated */    
  public void setAnsColumn(int addr, String v) {
        if (featOkTst && casFeat_ansColumn == null)
      jcas.throwFeatMissing("ansColumn", "org.kachako.types.centerexam.answertable.Answer");
    ll_cas.ll_setStringValue(addr, casFeatCode_ansColumn, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Answer_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_systemAnswer = jcas.getRequiredFeatureDE(casType, "systemAnswer", "uima.cas.String", featOkTst);
    casFeatCode_systemAnswer  = (null == casFeat_systemAnswer) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_systemAnswer).getCode();

 
    casFeat_String = jcas.getRequiredFeatureDE(casType, "String", "uima.cas.String", featOkTst);
    casFeatCode_String  = (null == casFeat_String) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_String).getCode();

 
    casFeat_ansColumn = jcas.getRequiredFeatureDE(casType, "ansColumn", "uima.cas.String", featOkTst);
    casFeatCode_ansColumn  = (null == casFeat_ansColumn) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_ansColumn).getCode();

  }
}



    