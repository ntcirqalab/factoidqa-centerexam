

/* First created by JCasGen Tue May 28 13:13:48 JST 2013 */
package org.kachako.types.centerexam.answertable;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Tue Aug 06 10:15:22 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/answerTableTypeSystem.xml
 * @generated */
public class Option extends AnswerTableTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Option.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Option() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Option(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Option(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Option(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: section_ID

  /** getter for section_ID - gets 
   * @generated */
  public String getSection_ID() {
    if (Option_Type.featOkTst && ((Option_Type)jcasType).casFeat_section_ID == null)
      jcasType.jcas.throwFeatMissing("section_ID", "org.kachako.types.centerexam.answertable.Option");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Option_Type)jcasType).casFeatCode_section_ID);}
    
  /** setter for section_ID - sets  
   * @generated */
  public void setSection_ID(String v) {
    if (Option_Type.featOkTst && ((Option_Type)jcasType).casFeat_section_ID == null)
      jcasType.jcas.throwFeatMissing("section_ID", "org.kachako.types.centerexam.answertable.Option");
    jcasType.ll_cas.ll_setStringValue(addr, ((Option_Type)jcasType).casFeatCode_section_ID, v);}    
   
    
  //*--------------*
  //* Feature: anscolumns

  /** getter for anscolumns - gets 
   * @generated */
  public String getAnscolumns() {
    if (Option_Type.featOkTst && ((Option_Type)jcasType).casFeat_anscolumns == null)
      jcasType.jcas.throwFeatMissing("anscolumns", "org.kachako.types.centerexam.answertable.Option");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Option_Type)jcasType).casFeatCode_anscolumns);}
    
  /** setter for anscolumns - sets  
   * @generated */
  public void setAnscolumns(String v) {
    if (Option_Type.featOkTst && ((Option_Type)jcasType).casFeat_anscolumns == null)
      jcasType.jcas.throwFeatMissing("anscolumns", "org.kachako.types.centerexam.answertable.Option");
    jcasType.ll_cas.ll_setStringValue(addr, ((Option_Type)jcasType).casFeatCode_anscolumns, v);}    
  }

    