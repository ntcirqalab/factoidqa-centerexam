

/* First created by JCasGen Wed Apr 03 12:36:17 JST 2013 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class Math extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Math.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Math() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Math(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Math(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Math(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: display

  /** getter for display - gets 
   * @generated */
  public String getDisplay() {
    if (Math_Type.featOkTst && ((Math_Type)jcasType).casFeat_display == null)
      jcasType.jcas.throwFeatMissing("display", "org.kachako.types.centerexam.Math");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Math_Type)jcasType).casFeatCode_display);}
    
  /** setter for display - sets  
   * @generated */
  public void setDisplay(String v) {
    if (Math_Type.featOkTst && ((Math_Type)jcasType).casFeat_display == null)
      jcasType.jcas.throwFeatMissing("display", "org.kachako.types.centerexam.Math");
    jcasType.ll_cas.ll_setStringValue(addr, ((Math_Type)jcasType).casFeatCode_display, v);}    
   
    
  //*--------------*
  //* Feature: xmlns

  /** getter for xmlns - gets 
   * @generated */
  public String getXmlns() {
    if (Math_Type.featOkTst && ((Math_Type)jcasType).casFeat_xmlns == null)
      jcasType.jcas.throwFeatMissing("xmlns", "org.kachako.types.centerexam.Math");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Math_Type)jcasType).casFeatCode_xmlns);}
    
  /** setter for xmlns - sets  
   * @generated */
  public void setXmlns(String v) {
    if (Math_Type.featOkTst && ((Math_Type)jcasType).casFeat_xmlns == null)
      jcasType.jcas.throwFeatMissing("xmlns", "org.kachako.types.centerexam.Math");
    jcasType.ll_cas.ll_setStringValue(addr, ((Math_Type)jcasType).casFeatCode_xmlns, v);}    
  }

    