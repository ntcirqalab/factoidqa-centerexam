
/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Wed Aug 21 14:13:40 JST 2013
 * @generated */
public class Choice_Type extends CenterExamTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Choice_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Choice_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Choice(addr, Choice_Type.this);
  			   Choice_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Choice(addr, Choice_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Choice.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.Choice");
 
  /** @generated */
  final Feature casFeat_ra;
  /** @generated */
  final int     casFeatCode_ra;
  /** @generated */ 
  public String getRa(int addr) {
        if (featOkTst && casFeat_ra == null)
      jcas.throwFeatMissing("ra", "org.kachako.types.centerexam.Choice");
    return ll_cas.ll_getStringValue(addr, casFeatCode_ra);
  }
  /** @generated */    
  public void setRa(int addr, String v) {
        if (featOkTst && casFeat_ra == null)
      jcas.throwFeatMissing("ra", "org.kachako.types.centerexam.Choice");
    ll_cas.ll_setStringValue(addr, casFeatCode_ra, v);}
    
  
 
  /** @generated */
  final Feature casFeat_comment;
  /** @generated */
  final int     casFeatCode_comment;
  /** @generated */ 
  public String getComment(int addr) {
        if (featOkTst && casFeat_comment == null)
      jcas.throwFeatMissing("comment", "org.kachako.types.centerexam.Choice");
    return ll_cas.ll_getStringValue(addr, casFeatCode_comment);
  }
  /** @generated */    
  public void setComment(int addr, String v) {
        if (featOkTst && casFeat_comment == null)
      jcas.throwFeatMissing("comment", "org.kachako.types.centerexam.Choice");
    ll_cas.ll_setStringValue(addr, casFeatCode_comment, v);}
    
  
 
  /** @generated */
  final Feature casFeat_ansnum;
  /** @generated */
  final int     casFeatCode_ansnum;
  /** @generated */ 
  public String getAnsnum(int addr) {
        if (featOkTst && casFeat_ansnum == null)
      jcas.throwFeatMissing("ansnum", "org.kachako.types.centerexam.Choice");
    return ll_cas.ll_getStringValue(addr, casFeatCode_ansnum);
  }
  /** @generated */    
  public void setAnsnum(int addr, String v) {
        if (featOkTst && casFeat_ansnum == null)
      jcas.throwFeatMissing("ansnum", "org.kachako.types.centerexam.Choice");
    ll_cas.ll_setStringValue(addr, casFeatCode_ansnum, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Choice_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_ra = jcas.getRequiredFeatureDE(casType, "ra", "uima.cas.String", featOkTst);
    casFeatCode_ra  = (null == casFeat_ra) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_ra).getCode();

 
    casFeat_comment = jcas.getRequiredFeatureDE(casType, "comment", "uima.cas.String", featOkTst);
    casFeatCode_comment  = (null == casFeat_comment) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_comment).getCode();

 
    casFeat_ansnum = jcas.getRequiredFeatureDE(casType, "ansnum", "uima.cas.String", featOkTst);
    casFeatCode_ansnum  = (null == casFeat_ansnum) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_ansnum).getCode();

  }
}



    