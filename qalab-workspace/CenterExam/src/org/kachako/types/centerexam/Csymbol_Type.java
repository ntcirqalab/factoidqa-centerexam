
/* First created by JCasGen Thu Apr 04 12:27:10 JST 2013 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Wed Aug 21 14:13:40 JST 2013
 * @generated */
public class Csymbol_Type extends CenterExamTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Csymbol_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Csymbol_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Csymbol(addr, Csymbol_Type.this);
  			   Csymbol_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Csymbol(addr, Csymbol_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Csymbol.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.Csymbol");
 
  /** @generated */
  final Feature casFeat_cd;
  /** @generated */
  final int     casFeatCode_cd;
  /** @generated */ 
  public String getCd(int addr) {
        if (featOkTst && casFeat_cd == null)
      jcas.throwFeatMissing("cd", "org.kachako.types.centerexam.Csymbol");
    return ll_cas.ll_getStringValue(addr, casFeatCode_cd);
  }
  /** @generated */    
  public void setCd(int addr, String v) {
        if (featOkTst && casFeat_cd == null)
      jcas.throwFeatMissing("cd", "org.kachako.types.centerexam.Csymbol");
    ll_cas.ll_setStringValue(addr, casFeatCode_cd, v);}
    
  
 
  /** @generated */
  final Feature casFeat_definitionURL;
  /** @generated */
  final int     casFeatCode_definitionURL;
  /** @generated */ 
  public String getDefinitionURL(int addr) {
        if (featOkTst && casFeat_definitionURL == null)
      jcas.throwFeatMissing("definitionURL", "org.kachako.types.centerexam.Csymbol");
    return ll_cas.ll_getStringValue(addr, casFeatCode_definitionURL);
  }
  /** @generated */    
  public void setDefinitionURL(int addr, String v) {
        if (featOkTst && casFeat_definitionURL == null)
      jcas.throwFeatMissing("definitionURL", "org.kachako.types.centerexam.Csymbol");
    ll_cas.ll_setStringValue(addr, casFeatCode_definitionURL, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Csymbol_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_cd = jcas.getRequiredFeatureDE(casType, "cd", "uima.cas.String", featOkTst);
    casFeatCode_cd  = (null == casFeat_cd) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_cd).getCode();

 
    casFeat_definitionURL = jcas.getRequiredFeatureDE(casType, "definitionURL", "uima.cas.String", featOkTst);
    casFeatCode_definitionURL  = (null == casFeat_definitionURL) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_definitionURL).getCode();

  }
}



    