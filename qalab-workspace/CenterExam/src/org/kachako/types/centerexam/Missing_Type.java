
/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * @generated */
public class Missing_Type extends CenterExamTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Missing_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Missing_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Missing(addr, Missing_Type.this);
  			   Missing_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Missing(addr, Missing_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Missing.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.Missing");
 
  /** @generated */
  final Feature casFeat_attributeString;
  /** @generated */
  final int     casFeatCode_attributeString;
  /** @generated */ 
  public String getAttributeString(int addr) {
        if (featOkTst && casFeat_attributeString == null)
      jcas.throwFeatMissing("attributeString", "org.kachako.types.centerexam.Missing");
    return ll_cas.ll_getStringValue(addr, casFeatCode_attributeString);
  }
  /** @generated */    
  public void setAttributeString(int addr, String v) {
        if (featOkTst && casFeat_attributeString == null)
      jcas.throwFeatMissing("attributeString", "org.kachako.types.centerexam.Missing");
    ll_cas.ll_setStringValue(addr, casFeatCode_attributeString, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Missing_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_attributeString = jcas.getRequiredFeatureDE(casType, "attributeString", "uima.cas.String", featOkTst);
    casFeatCode_attributeString  = (null == casFeat_attributeString) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_attributeString).getCode();

  }
}



    