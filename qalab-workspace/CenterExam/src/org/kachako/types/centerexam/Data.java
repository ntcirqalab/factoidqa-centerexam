

/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:40 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class Data extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Data.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Data() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Data(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Data(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Data(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: id

  /** getter for id - gets 
   * @generated */
  public String getId() {
    if (Data_Type.featOkTst && ((Data_Type)jcasType).casFeat_id == null)
      jcasType.jcas.throwFeatMissing("id", "org.kachako.types.centerexam.Data");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Data_Type)jcasType).casFeatCode_id);}
    
  /** setter for id - sets  
   * @generated */
  public void setId(String v) {
    if (Data_Type.featOkTst && ((Data_Type)jcasType).casFeat_id == null)
      jcasType.jcas.throwFeatMissing("id", "org.kachako.types.centerexam.Data");
    jcasType.ll_cas.ll_setStringValue(addr, ((Data_Type)jcasType).casFeatCode_id, v);}    
   
    
  //*--------------*
  //* Feature: types

  /** getter for types - gets 
   * @generated */
  public String getTypes() {
    if (Data_Type.featOkTst && ((Data_Type)jcasType).casFeat_types == null)
      jcasType.jcas.throwFeatMissing("types", "org.kachako.types.centerexam.Data");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Data_Type)jcasType).casFeatCode_types);}
    
  /** setter for types - sets  
   * @generated */
  public void setTypes(String v) {
    if (Data_Type.featOkTst && ((Data_Type)jcasType).casFeat_types == null)
      jcasType.jcas.throwFeatMissing("types", "org.kachako.types.centerexam.Data");
    jcasType.ll_cas.ll_setStringValue(addr, ((Data_Type)jcasType).casFeatCode_types, v);}    
  }

    