

/* First created by JCasGen Thu Apr 04 11:46:08 JST 2013 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class Mfenced extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Mfenced.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Mfenced() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Mfenced(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Mfenced(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Mfenced(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: separators

  /** getter for separators - gets 
   * @generated */
  public String getSeparators() {
    if (Mfenced_Type.featOkTst && ((Mfenced_Type)jcasType).casFeat_separators == null)
      jcasType.jcas.throwFeatMissing("separators", "org.kachako.types.centerexam.Mfenced");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Mfenced_Type)jcasType).casFeatCode_separators);}
    
  /** setter for separators - sets  
   * @generated */
  public void setSeparators(String v) {
    if (Mfenced_Type.featOkTst && ((Mfenced_Type)jcasType).casFeat_separators == null)
      jcasType.jcas.throwFeatMissing("separators", "org.kachako.types.centerexam.Mfenced");
    jcasType.ll_cas.ll_setStringValue(addr, ((Mfenced_Type)jcasType).casFeatCode_separators, v);}    
   
    
  //*--------------*
  //* Feature: open

  /** getter for open - gets 
   * @generated */
  public String getOpen() {
    if (Mfenced_Type.featOkTst && ((Mfenced_Type)jcasType).casFeat_open == null)
      jcasType.jcas.throwFeatMissing("open", "org.kachako.types.centerexam.Mfenced");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Mfenced_Type)jcasType).casFeatCode_open);}
    
  /** setter for open - sets  
   * @generated */
  public void setOpen(String v) {
    if (Mfenced_Type.featOkTst && ((Mfenced_Type)jcasType).casFeat_open == null)
      jcasType.jcas.throwFeatMissing("open", "org.kachako.types.centerexam.Mfenced");
    jcasType.ll_cas.ll_setStringValue(addr, ((Mfenced_Type)jcasType).casFeatCode_open, v);}    
   
    
  //*--------------*
  //* Feature: close

  /** getter for close - gets 
   * @generated */
  public String getClose() {
    if (Mfenced_Type.featOkTst && ((Mfenced_Type)jcasType).casFeat_close == null)
      jcasType.jcas.throwFeatMissing("close", "org.kachako.types.centerexam.Mfenced");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Mfenced_Type)jcasType).casFeatCode_close);}
    
  /** setter for close - sets  
   * @generated */
  public void setClose(String v) {
    if (Mfenced_Type.featOkTst && ((Mfenced_Type)jcasType).casFeat_close == null)
      jcasType.jcas.throwFeatMissing("close", "org.kachako.types.centerexam.Mfenced");
    jcasType.ll_cas.ll_setStringValue(addr, ((Mfenced_Type)jcasType).casFeatCode_close, v);}    
  }

    