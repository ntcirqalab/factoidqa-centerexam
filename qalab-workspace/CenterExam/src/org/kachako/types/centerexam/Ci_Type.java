
/* First created by JCasGen Thu Apr 04 11:46:07 JST 2013 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Wed Aug 21 14:13:40 JST 2013
 * @generated */
public class Ci_Type extends CenterExamTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Ci_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Ci_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Ci(addr, Ci_Type.this);
  			   Ci_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Ci(addr, Ci_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Ci.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.Ci");
 
  /** @generated */
  final Feature casFeat_ciType;
  /** @generated */
  final int     casFeatCode_ciType;
  /** @generated */ 
  public String getCiType(int addr) {
        if (featOkTst && casFeat_ciType == null)
      jcas.throwFeatMissing("ciType", "org.kachako.types.centerexam.Ci");
    return ll_cas.ll_getStringValue(addr, casFeatCode_ciType);
  }
  /** @generated */    
  public void setCiType(int addr, String v) {
        if (featOkTst && casFeat_ciType == null)
      jcas.throwFeatMissing("ciType", "org.kachako.types.centerexam.Ci");
    ll_cas.ll_setStringValue(addr, casFeatCode_ciType, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Ci_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_ciType = jcas.getRequiredFeatureDE(casType, "ciType", "uima.cas.String", featOkTst);
    casFeatCode_ciType  = (null == casFeat_ciType) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_ciType).getCode();

  }
}



    