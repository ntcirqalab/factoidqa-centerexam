
/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.cas.TOP_Type;

/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * @generated */
public class TagList_Type extends TOP_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (TagList_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = TagList_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new TagList(addr, TagList_Type.this);
  			   TagList_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new TagList(addr, TagList_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = TagList.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.TagList");
 
  /** @generated */
  final Feature casFeat_annotationList;
  /** @generated */
  final int     casFeatCode_annotationList;
  /** @generated */ 
  public int getAnnotationList(int addr) {
        if (featOkTst && casFeat_annotationList == null)
      jcas.throwFeatMissing("annotationList", "org.kachako.types.centerexam.TagList");
    return ll_cas.ll_getRefValue(addr, casFeatCode_annotationList);
  }
  /** @generated */    
  public void setAnnotationList(int addr, int v) {
        if (featOkTst && casFeat_annotationList == null)
      jcas.throwFeatMissing("annotationList", "org.kachako.types.centerexam.TagList");
    ll_cas.ll_setRefValue(addr, casFeatCode_annotationList, v);}
    
   /** @generated */
  public int getAnnotationList(int addr, int i) {
        if (featOkTst && casFeat_annotationList == null)
      jcas.throwFeatMissing("annotationList", "org.kachako.types.centerexam.TagList");
    if (lowLevelTypeChecks)
      return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_annotationList), i, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_annotationList), i);
  return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_annotationList), i);
  }
   
  /** @generated */ 
  public void setAnnotationList(int addr, int i, int v) {
        if (featOkTst && casFeat_annotationList == null)
      jcas.throwFeatMissing("annotationList", "org.kachako.types.centerexam.TagList");
    if (lowLevelTypeChecks)
      ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_annotationList), i, v, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_annotationList), i);
    ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_annotationList), i, v);
  }
 



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public TagList_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_annotationList = jcas.getRequiredFeatureDE(casType, "annotationList", "uima.cas.FSArray", featOkTst);
    casFeatCode_annotationList  = (null == casFeat_annotationList) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_annotationList).getCode();

  }
}



    