
/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * @generated */
public class Tbl_Type extends CenterExamTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Tbl_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Tbl_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Tbl(addr, Tbl_Type.this);
  			   Tbl_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Tbl(addr, Tbl_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Tbl.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.Tbl");
 
  /** @generated */
  final Feature casFeat_numOfCols;
  /** @generated */
  final int     casFeatCode_numOfCols;
  /** @generated */ 
  public int getNumOfCols(int addr) {
        if (featOkTst && casFeat_numOfCols == null)
      jcas.throwFeatMissing("numOfCols", "org.kachako.types.centerexam.Tbl");
    return ll_cas.ll_getIntValue(addr, casFeatCode_numOfCols);
  }
  /** @generated */    
  public void setNumOfCols(int addr, int v) {
        if (featOkTst && casFeat_numOfCols == null)
      jcas.throwFeatMissing("numOfCols", "org.kachako.types.centerexam.Tbl");
    ll_cas.ll_setIntValue(addr, casFeatCode_numOfCols, v);}
    
  
 
  /** @generated */
  final Feature casFeat_numOfRows;
  /** @generated */
  final int     casFeatCode_numOfRows;
  /** @generated */ 
  public int getNumOfRows(int addr) {
        if (featOkTst && casFeat_numOfRows == null)
      jcas.throwFeatMissing("numOfRows", "org.kachako.types.centerexam.Tbl");
    return ll_cas.ll_getIntValue(addr, casFeatCode_numOfRows);
  }
  /** @generated */    
  public void setNumOfRows(int addr, int v) {
        if (featOkTst && casFeat_numOfRows == null)
      jcas.throwFeatMissing("numOfRows", "org.kachako.types.centerexam.Tbl");
    ll_cas.ll_setIntValue(addr, casFeatCode_numOfRows, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Tbl_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_numOfCols = jcas.getRequiredFeatureDE(casType, "numOfCols", "uima.cas.Integer", featOkTst);
    casFeatCode_numOfCols  = (null == casFeat_numOfCols) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_numOfCols).getCode();

 
    casFeat_numOfRows = jcas.getRequiredFeatureDE(casType, "numOfRows", "uima.cas.Integer", featOkTst);
    casFeatCode_numOfRows  = (null == casFeat_numOfRows) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_numOfRows).getCode();

  }
}



    