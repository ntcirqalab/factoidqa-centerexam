
/* First created by JCasGen Thu Apr 04 11:46:08 JST 2013 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;

import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * @generated */
public class Mi_Type extends CenterExamTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Mi_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Mi_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Mi(addr, Mi_Type.this);
  			   Mi_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Mi(addr, Mi_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Mi.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.Mi");



  /** @generated */
  final Feature casFeat_mathvariant;
  /** @generated */
  final int     casFeatCode_mathvariant;
  /** @generated */ 
  public String getMathvariant(int addr) {
        if (featOkTst && casFeat_mathvariant == null)
      jcas.throwFeatMissing("mathvariant", "org.kachako.types.centerexam.Mi");
    return ll_cas.ll_getStringValue(addr, casFeatCode_mathvariant);
  }
  /** @generated */    
  public void setMathvariant(int addr, String v) {
        if (featOkTst && casFeat_mathvariant == null)
      jcas.throwFeatMissing("mathvariant", "org.kachako.types.centerexam.Mi");
    ll_cas.ll_setStringValue(addr, casFeatCode_mathvariant, v);}
    
  
 
  /** @generated */
  final Feature casFeat_anscolumn_id;
  /** @generated */
  final int     casFeatCode_anscolumn_id;
  /** @generated */ 
  public String getAnscolumn_id(int addr) {
        if (featOkTst && casFeat_anscolumn_id == null)
      jcas.throwFeatMissing("anscolumn_id", "org.kachako.types.centerexam.Mi");
    return ll_cas.ll_getStringValue(addr, casFeatCode_anscolumn_id);
  }
  /** @generated */    
  public void setAnscolumn_id(int addr, String v) {
        if (featOkTst && casFeat_anscolumn_id == null)
      jcas.throwFeatMissing("anscolumn_id", "org.kachako.types.centerexam.Mi");
    ll_cas.ll_setStringValue(addr, casFeatCode_anscolumn_id, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Mi_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_mathvariant = jcas.getRequiredFeatureDE(casType, "mathvariant", "uima.cas.String", featOkTst);
    casFeatCode_mathvariant  = (null == casFeat_mathvariant) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_mathvariant).getCode();

 
    casFeat_anscolumn_id = jcas.getRequiredFeatureDE(casType, "anscolumn_id", "uima.cas.String", featOkTst);
    casFeatCode_anscolumn_id  = (null == casFeat_anscolumn_id) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_anscolumn_id).getCode();

  }
}



    