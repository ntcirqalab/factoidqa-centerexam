
/* First created by JCasGen Wed Aug 21 14:13:41 JST 2013 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * @generated */
public class Adata_Type extends CenterExamTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Adata_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Adata_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Adata(addr, Adata_Type.this);
  			   Adata_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Adata(addr, Adata_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Adata.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.Adata");
 
  /** @generated */
  final Feature casFeat_id;
  /** @generated */
  final int     casFeatCode_id;
  /** @generated */ 
  public String getId(int addr) {
        if (featOkTst && casFeat_id == null)
      jcas.throwFeatMissing("id", "org.kachako.types.centerexam.Adata");
    return ll_cas.ll_getStringValue(addr, casFeatCode_id);
  }
  /** @generated */    
  public void setId(int addr, String v) {
        if (featOkTst && casFeat_id == null)
      jcas.throwFeatMissing("id", "org.kachako.types.centerexam.Adata");
    ll_cas.ll_setStringValue(addr, casFeatCode_id, v);}
    
  
 
  /** @generated */
  final Feature casFeat_adataType;
  /** @generated */
  final int     casFeatCode_adataType;
  /** @generated */ 
  public String getAdataType(int addr) {
        if (featOkTst && casFeat_adataType == null)
      jcas.throwFeatMissing("adataType", "org.kachako.types.centerexam.Adata");
    return ll_cas.ll_getStringValue(addr, casFeatCode_adataType);
  }
  /** @generated */    
  public void setAdataType(int addr, String v) {
        if (featOkTst && casFeat_adataType == null)
      jcas.throwFeatMissing("adataType", "org.kachako.types.centerexam.Adata");
    ll_cas.ll_setStringValue(addr, casFeatCode_adataType, v);}
    
  
 
  /** @generated */
  final Feature casFeat_field;
  /** @generated */
  final int     casFeatCode_field;
  /** @generated */ 
  public String getField(int addr) {
        if (featOkTst && casFeat_field == null)
      jcas.throwFeatMissing("field", "org.kachako.types.centerexam.Adata");
    return ll_cas.ll_getStringValue(addr, casFeatCode_field);
  }
  /** @generated */    
  public void setField(int addr, String v) {
        if (featOkTst && casFeat_field == null)
      jcas.throwFeatMissing("field", "org.kachako.types.centerexam.Adata");
    ll_cas.ll_setStringValue(addr, casFeatCode_field, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Adata_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_id = jcas.getRequiredFeatureDE(casType, "id", "uima.cas.String", featOkTst);
    casFeatCode_id  = (null == casFeat_id) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_id).getCode();

 
    casFeat_adataType = jcas.getRequiredFeatureDE(casType, "adataType", "uima.cas.String", featOkTst);
    casFeatCode_adataType  = (null == casFeat_adataType) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_adataType).getCode();

 
    casFeat_field = jcas.getRequiredFeatureDE(casType, "field", "uima.cas.String", featOkTst);
    casFeatCode_field  = (null == casFeat_field) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_field).getCode();

  }
}



    