

/* First created by JCasGen Thu Apr 04 11:46:08 JST 2013 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class Mo extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Mo.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Mo() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Mo(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Mo(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Mo(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
  //*--------------*
  //* Feature: stretchy

  /** getter for stretchy - gets 
   * @generated */
  public String getStretchy() {
    if (Mo_Type.featOkTst && ((Mo_Type)jcasType).casFeat_stretchy == null)
      jcasType.jcas.throwFeatMissing("stretchy", "org.kachako.types.centerexam.Mo");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Mo_Type)jcasType).casFeatCode_stretchy);}
    
  /** setter for stretchy - sets  
   * @generated */
  public void setStretchy(String v) {
    if (Mo_Type.featOkTst && ((Mo_Type)jcasType).casFeat_stretchy == null)
      jcasType.jcas.throwFeatMissing("stretchy", "org.kachako.types.centerexam.Mo");
    jcasType.ll_cas.ll_setStringValue(addr, ((Mo_Type)jcasType).casFeatCode_stretchy, v);}    
  }

    