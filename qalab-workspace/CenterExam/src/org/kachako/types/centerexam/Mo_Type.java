
/* First created by JCasGen Thu Apr 04 11:46:08 JST 2013 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;

import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * @generated */
public class Mo_Type extends CenterExamTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Mo_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Mo_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Mo(addr, Mo_Type.this);
  			   Mo_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Mo(addr, Mo_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Mo.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.Mo");



  /** @generated */
  final Feature casFeat_stretchy;
  /** @generated */
  final int     casFeatCode_stretchy;
  /** @generated */ 
  public String getStretchy(int addr) {
        if (featOkTst && casFeat_stretchy == null)
      jcas.throwFeatMissing("stretchy", "org.kachako.types.centerexam.Mo");
    return ll_cas.ll_getStringValue(addr, casFeatCode_stretchy);
  }
  /** @generated */    
  public void setStretchy(int addr, String v) {
        if (featOkTst && casFeat_stretchy == null)
      jcas.throwFeatMissing("stretchy", "org.kachako.types.centerexam.Mo");
    ll_cas.ll_setStringValue(addr, casFeatCode_stretchy, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Mo_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_stretchy = jcas.getRequiredFeatureDE(casType, "stretchy", "uima.cas.String", featOkTst);
    casFeatCode_stretchy  = (null == casFeat_stretchy) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_stretchy).getCode();

  }
}



    