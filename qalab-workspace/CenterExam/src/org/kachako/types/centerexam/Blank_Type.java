
/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Wed Aug 21 14:13:40 JST 2013
 * @generated */
public class Blank_Type extends CenterExamTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Blank_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Blank_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Blank(addr, Blank_Type.this);
  			   Blank_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Blank(addr, Blank_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Blank.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.Blank");
 
  /** @generated */
  final Feature casFeat_id;
  /** @generated */
  final int     casFeatCode_id;
  /** @generated */ 
  public String getId(int addr) {
        if (featOkTst && casFeat_id == null)
      jcas.throwFeatMissing("id", "org.kachako.types.centerexam.Blank");
    return ll_cas.ll_getStringValue(addr, casFeatCode_id);
  }
  /** @generated */    
  public void setId(int addr, String v) {
        if (featOkTst && casFeat_id == null)
      jcas.throwFeatMissing("id", "org.kachako.types.centerexam.Blank");
    ll_cas.ll_setStringValue(addr, casFeatCode_id, v);}
    
  
 
  /** @generated */
  final Feature casFeat_digits;
  /** @generated */
  final int     casFeatCode_digits;
  /** @generated */ 
  public String getDigits(int addr) {
        if (featOkTst && casFeat_digits == null)
      jcas.throwFeatMissing("digits", "org.kachako.types.centerexam.Blank");
    return ll_cas.ll_getStringValue(addr, casFeatCode_digits);
  }
  /** @generated */    
  public void setDigits(int addr, String v) {
        if (featOkTst && casFeat_digits == null)
      jcas.throwFeatMissing("digits", "org.kachako.types.centerexam.Blank");
    ll_cas.ll_setStringValue(addr, casFeatCode_digits, v);}
    
  
 
  /** @generated */
  final Feature casFeat_anscolumn_id;
  /** @generated */
  final int     casFeatCode_anscolumn_id;
  /** @generated */ 
  public String getAnscolumn_id(int addr) {
        if (featOkTst && casFeat_anscolumn_id == null)
      jcas.throwFeatMissing("anscolumn_id", "org.kachako.types.centerexam.Blank");
    return ll_cas.ll_getStringValue(addr, casFeatCode_anscolumn_id);
  }
  /** @generated */    
  public void setAnscolumn_id(int addr, String v) {
        if (featOkTst && casFeat_anscolumn_id == null)
      jcas.throwFeatMissing("anscolumn_id", "org.kachako.types.centerexam.Blank");
    ll_cas.ll_setStringValue(addr, casFeatCode_anscolumn_id, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Blank_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_id = jcas.getRequiredFeatureDE(casType, "id", "uima.cas.String", featOkTst);
    casFeatCode_id  = (null == casFeat_id) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_id).getCode();

 
    casFeat_digits = jcas.getRequiredFeatureDE(casType, "digits", "uima.cas.String", featOkTst);
    casFeatCode_digits  = (null == casFeat_digits) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_digits).getCode();

 
    casFeat_anscolumn_id = jcas.getRequiredFeatureDE(casType, "anscolumn_id", "uima.cas.String", featOkTst);
    casFeatCode_anscolumn_id  = (null == casFeat_anscolumn_id) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_anscolumn_id).getCode();

  }
}



    