

/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.TOP;


/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class TagList extends TOP {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(TagList.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected TagList() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public TagList(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public TagList(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: annotationList

  /** getter for annotationList - gets 
   * @generated */
  public FSArray getAnnotationList() {
    if (TagList_Type.featOkTst && ((TagList_Type)jcasType).casFeat_annotationList == null)
      jcasType.jcas.throwFeatMissing("annotationList", "org.kachako.types.centerexam.TagList");
    return (FSArray)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((TagList_Type)jcasType).casFeatCode_annotationList)));}
    
  /** setter for annotationList - sets  
   * @generated */
  public void setAnnotationList(FSArray v) {
    if (TagList_Type.featOkTst && ((TagList_Type)jcasType).casFeat_annotationList == null)
      jcasType.jcas.throwFeatMissing("annotationList", "org.kachako.types.centerexam.TagList");
    jcasType.ll_cas.ll_setRefValue(addr, ((TagList_Type)jcasType).casFeatCode_annotationList, jcasType.ll_cas.ll_getFSRef(v));}    
    
  /** indexed getter for annotationList - gets an indexed value - 
   * @generated */
  public CenterExamTag getAnnotationList(int i) {
    if (TagList_Type.featOkTst && ((TagList_Type)jcasType).casFeat_annotationList == null)
      jcasType.jcas.throwFeatMissing("annotationList", "org.kachako.types.centerexam.TagList");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((TagList_Type)jcasType).casFeatCode_annotationList), i);
    return (CenterExamTag)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((TagList_Type)jcasType).casFeatCode_annotationList), i)));}

  /** indexed setter for annotationList - sets an indexed value - 
   * @generated */
  public void setAnnotationList(int i, CenterExamTag v) { 
    if (TagList_Type.featOkTst && ((TagList_Type)jcasType).casFeat_annotationList == null)
      jcasType.jcas.throwFeatMissing("annotationList", "org.kachako.types.centerexam.TagList");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((TagList_Type)jcasType).casFeatCode_annotationList), i);
    jcasType.ll_cas.ll_setRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((TagList_Type)jcasType).casFeatCode_annotationList), i, jcasType.ll_cas.ll_getFSRef(v));}
  }

    