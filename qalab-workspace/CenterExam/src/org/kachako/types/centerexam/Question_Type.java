
/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * @generated */
public class Question_Type extends CenterExamTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Question_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Question_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Question(addr, Question_Type.this);
  			   Question_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Question(addr, Question_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Question.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.Question");
 
  /** @generated */
  final Feature casFeat_id;
  /** @generated */
  final int     casFeatCode_id;
  /** @generated */ 
  public String getId(int addr) {
        if (featOkTst && casFeat_id == null)
      jcas.throwFeatMissing("id", "org.kachako.types.centerexam.Question");
    return ll_cas.ll_getStringValue(addr, casFeatCode_id);
  }
  /** @generated */    
  public void setId(int addr, String v) {
        if (featOkTst && casFeat_id == null)
      jcas.throwFeatMissing("id", "org.kachako.types.centerexam.Question");
    ll_cas.ll_setStringValue(addr, casFeatCode_id, v);}
    
  
 
  /** @generated */
  final Feature casFeat_minimal;
  /** @generated */
  final int     casFeatCode_minimal;
  /** @generated */ 
  public String getMinimal(int addr) {
        if (featOkTst && casFeat_minimal == null)
      jcas.throwFeatMissing("minimal", "org.kachako.types.centerexam.Question");
    return ll_cas.ll_getStringValue(addr, casFeatCode_minimal);
  }
  /** @generated */    
  public void setMinimal(int addr, String v) {
        if (featOkTst && casFeat_minimal == null)
      jcas.throwFeatMissing("minimal", "org.kachako.types.centerexam.Question");
    ll_cas.ll_setStringValue(addr, casFeatCode_minimal, v);}
    
  
 
  /** @generated */
  final Feature casFeat_answer_style;
  /** @generated */
  final int     casFeatCode_answer_style;
  /** @generated */ 
  public String getAnswer_style(int addr) {
        if (featOkTst && casFeat_answer_style == null)
      jcas.throwFeatMissing("answer_style", "org.kachako.types.centerexam.Question");
    return ll_cas.ll_getStringValue(addr, casFeatCode_answer_style);
  }
  /** @generated */    
  public void setAnswer_style(int addr, String v) {
        if (featOkTst && casFeat_answer_style == null)
      jcas.throwFeatMissing("answer_style", "org.kachako.types.centerexam.Question");
    ll_cas.ll_setStringValue(addr, casFeatCode_answer_style, v);}
    
  
 
  /** @generated */
  final Feature casFeat_answer_type;
  /** @generated */
  final int     casFeatCode_answer_type;
  /** @generated */ 
  public String getAnswer_type(int addr) {
        if (featOkTst && casFeat_answer_type == null)
      jcas.throwFeatMissing("answer_type", "org.kachako.types.centerexam.Question");
    return ll_cas.ll_getStringValue(addr, casFeatCode_answer_type);
  }
  /** @generated */    
  public void setAnswer_type(int addr, String v) {
        if (featOkTst && casFeat_answer_type == null)
      jcas.throwFeatMissing("answer_type", "org.kachako.types.centerexam.Question");
    ll_cas.ll_setStringValue(addr, casFeatCode_answer_type, v);}
    
  
 
  /** @generated */
  final Feature casFeat_knowledge_type;
  /** @generated */
  final int     casFeatCode_knowledge_type;
  /** @generated */ 
  public String getKnowledge_type(int addr) {
        if (featOkTst && casFeat_knowledge_type == null)
      jcas.throwFeatMissing("knowledge_type", "org.kachako.types.centerexam.Question");
    return ll_cas.ll_getStringValue(addr, casFeatCode_knowledge_type);
  }
  /** @generated */    
  public void setKnowledge_type(int addr, String v) {
        if (featOkTst && casFeat_knowledge_type == null)
      jcas.throwFeatMissing("knowledge_type", "org.kachako.types.centerexam.Question");
    ll_cas.ll_setStringValue(addr, casFeatCode_knowledge_type, v);}
    
  
 
  /** @generated */
  final Feature casFeat_anscol;
  /** @generated */
  final int     casFeatCode_anscol;
  /** @generated */ 
  public String getAnscol(int addr) {
        if (featOkTst && casFeat_anscol == null)
      jcas.throwFeatMissing("anscol", "org.kachako.types.centerexam.Question");
    return ll_cas.ll_getStringValue(addr, casFeatCode_anscol);
  }
  /** @generated */    
  public void setAnscol(int addr, String v) {
        if (featOkTst && casFeat_anscol == null)
      jcas.throwFeatMissing("anscol", "org.kachako.types.centerexam.Question");
    ll_cas.ll_setStringValue(addr, casFeatCode_anscol, v);}
    
  
 
  /** @generated */
  final Feature casFeat_title;
  /** @generated */
  final int     casFeatCode_title;
  /** @generated */ 
  public String getTitle(int addr) {
        if (featOkTst && casFeat_title == null)
      jcas.throwFeatMissing("title", "org.kachako.types.centerexam.Question");
    return ll_cas.ll_getStringValue(addr, casFeatCode_title);
  }
  /** @generated */    
  public void setTitle(int addr, String v) {
        if (featOkTst && casFeat_title == null)
      jcas.throwFeatMissing("title", "org.kachako.types.centerexam.Question");
    ll_cas.ll_setStringValue(addr, casFeatCode_title, v);}
    
  
 
  /** @generated */
  final Feature casFeat_sectionId;
  /** @generated */
  final int     casFeatCode_sectionId;
  /** @generated */ 
  public String getSectionId(int addr) {
        if (featOkTst && casFeat_sectionId == null)
      jcas.throwFeatMissing("sectionId", "org.kachako.types.centerexam.Question");
    return ll_cas.ll_getStringValue(addr, casFeatCode_sectionId);
  }
  /** @generated */    
  public void setSectionId(int addr, String v) {
        if (featOkTst && casFeat_sectionId == null)
      jcas.throwFeatMissing("sectionId", "org.kachako.types.centerexam.Question");
    ll_cas.ll_setStringValue(addr, casFeatCode_sectionId, v);}
    
  
 
  /** @generated */
  final Feature casFeat_anscolumn_ids;
  /** @generated */
  final int     casFeatCode_anscolumn_ids;
  /** @generated */ 
  public String getAnscolumn_ids(int addr) {
        if (featOkTst && casFeat_anscolumn_ids == null)
      jcas.throwFeatMissing("anscolumn_ids", "org.kachako.types.centerexam.Question");
    return ll_cas.ll_getStringValue(addr, casFeatCode_anscolumn_ids);
  }
  /** @generated */    
  public void setAnscolumn_ids(int addr, String v) {
        if (featOkTst && casFeat_anscolumn_ids == null)
      jcas.throwFeatMissing("anscolumn_ids", "org.kachako.types.centerexam.Question");
    ll_cas.ll_setStringValue(addr, casFeatCode_anscolumn_ids, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Question_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_id = jcas.getRequiredFeatureDE(casType, "id", "uima.cas.String", featOkTst);
    casFeatCode_id  = (null == casFeat_id) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_id).getCode();

 
    casFeat_minimal = jcas.getRequiredFeatureDE(casType, "minimal", "uima.cas.String", featOkTst);
    casFeatCode_minimal  = (null == casFeat_minimal) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_minimal).getCode();

 
    casFeat_answer_style = jcas.getRequiredFeatureDE(casType, "answer_style", "uima.cas.String", featOkTst);
    casFeatCode_answer_style  = (null == casFeat_answer_style) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_answer_style).getCode();

 
    casFeat_answer_type = jcas.getRequiredFeatureDE(casType, "answer_type", "uima.cas.String", featOkTst);
    casFeatCode_answer_type  = (null == casFeat_answer_type) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_answer_type).getCode();

 
    casFeat_knowledge_type = jcas.getRequiredFeatureDE(casType, "knowledge_type", "uima.cas.String", featOkTst);
    casFeatCode_knowledge_type  = (null == casFeat_knowledge_type) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_knowledge_type).getCode();

 
    casFeat_anscol = jcas.getRequiredFeatureDE(casType, "anscol", "uima.cas.String", featOkTst);
    casFeatCode_anscol  = (null == casFeat_anscol) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_anscol).getCode();

 
    casFeat_title = jcas.getRequiredFeatureDE(casType, "title", "uima.cas.String", featOkTst);
    casFeatCode_title  = (null == casFeat_title) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_title).getCode();

 
    casFeat_sectionId = jcas.getRequiredFeatureDE(casType, "sectionId", "uima.cas.String", featOkTst);
    casFeatCode_sectionId  = (null == casFeat_sectionId) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_sectionId).getCode();

 
    casFeat_anscolumn_ids = jcas.getRequiredFeatureDE(casType, "anscolumn_ids", "uima.cas.String", featOkTst);
    casFeatCode_anscolumn_ids  = (null == casFeat_anscolumn_ids) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_anscolumn_ids).getCode();

  }
}



    