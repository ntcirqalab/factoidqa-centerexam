

/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class Garbled extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Garbled.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Garbled() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Garbled(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Garbled(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Garbled(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: correction

  /** getter for correction - gets 
   * @generated */
  public String getCorrection() {
    if (Garbled_Type.featOkTst && ((Garbled_Type)jcasType).casFeat_correction == null)
      jcasType.jcas.throwFeatMissing("correction", "org.kachako.types.centerexam.Garbled");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Garbled_Type)jcasType).casFeatCode_correction);}
    
  /** setter for correction - sets  
   * @generated */
  public void setCorrection(String v) {
    if (Garbled_Type.featOkTst && ((Garbled_Type)jcasType).casFeat_correction == null)
      jcasType.jcas.throwFeatMissing("correction", "org.kachako.types.centerexam.Garbled");
    jcasType.ll_cas.ll_setStringValue(addr, ((Garbled_Type)jcasType).casFeatCode_correction, v);}    
  }

    