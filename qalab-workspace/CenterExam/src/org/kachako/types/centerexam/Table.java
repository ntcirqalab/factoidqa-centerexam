

/* First created by JCasGen Thu Apr 04 12:52:50 JST 2013 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class Table extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Table.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Table() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Table(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Table(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Table(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: xmlns

  /** getter for xmlns - gets 
   * @generated */
  public String getXmlns() {
    if (Table_Type.featOkTst && ((Table_Type)jcasType).casFeat_xmlns == null)
      jcasType.jcas.throwFeatMissing("xmlns", "org.kachako.types.centerexam.Table");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Table_Type)jcasType).casFeatCode_xmlns);}
    
  /** setter for xmlns - sets  
   * @generated */
  public void setXmlns(String v) {
    if (Table_Type.featOkTst && ((Table_Type)jcasType).casFeat_xmlns == null)
      jcasType.jcas.throwFeatMissing("xmlns", "org.kachako.types.centerexam.Table");
    jcasType.ll_cas.ll_setStringValue(addr, ((Table_Type)jcasType).casFeatCode_xmlns, v);}    
  }

    