
/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.cas.TOP_Type;

/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * @generated */
public class EndList_Type extends TOP_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (EndList_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = EndList_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new EndList(addr, EndList_Type.this);
  			   EndList_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new EndList(addr, EndList_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = EndList.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.EndList");
 
  /** @generated */
  final Feature casFeat_AnnotationList;
  /** @generated */
  final int     casFeatCode_AnnotationList;
  /** @generated */ 
  public int getAnnotationList(int addr) {
        if (featOkTst && casFeat_AnnotationList == null)
      jcas.throwFeatMissing("AnnotationList", "org.kachako.types.centerexam.EndList");
    return ll_cas.ll_getRefValue(addr, casFeatCode_AnnotationList);
  }
  /** @generated */    
  public void setAnnotationList(int addr, int v) {
        if (featOkTst && casFeat_AnnotationList == null)
      jcas.throwFeatMissing("AnnotationList", "org.kachako.types.centerexam.EndList");
    ll_cas.ll_setRefValue(addr, casFeatCode_AnnotationList, v);}
    
   /** @generated */
  public int getAnnotationList(int addr, int i) {
        if (featOkTst && casFeat_AnnotationList == null)
      jcas.throwFeatMissing("AnnotationList", "org.kachako.types.centerexam.EndList");
    if (lowLevelTypeChecks)
      return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_AnnotationList), i, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_AnnotationList), i);
  return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_AnnotationList), i);
  }
   
  /** @generated */ 
  public void setAnnotationList(int addr, int i, int v) {
        if (featOkTst && casFeat_AnnotationList == null)
      jcas.throwFeatMissing("AnnotationList", "org.kachako.types.centerexam.EndList");
    if (lowLevelTypeChecks)
      ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_AnnotationList), i, v, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_AnnotationList), i);
    ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_AnnotationList), i, v);
  }
 



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public EndList_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_AnnotationList = jcas.getRequiredFeatureDE(casType, "AnnotationList", "uima.cas.FSArray", featOkTst);
    casFeatCode_AnnotationList  = (null == casFeat_AnnotationList) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_AnnotationList).getCode();

  }
}



    