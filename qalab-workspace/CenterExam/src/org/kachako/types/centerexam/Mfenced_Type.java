
/* First created by JCasGen Thu Apr 04 11:46:08 JST 2013 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * @generated */
public class Mfenced_Type extends CenterExamTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Mfenced_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Mfenced_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Mfenced(addr, Mfenced_Type.this);
  			   Mfenced_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Mfenced(addr, Mfenced_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Mfenced.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.Mfenced");
 
  /** @generated */
  final Feature casFeat_separators;
  /** @generated */
  final int     casFeatCode_separators;
  /** @generated */ 
  public String getSeparators(int addr) {
        if (featOkTst && casFeat_separators == null)
      jcas.throwFeatMissing("separators", "org.kachako.types.centerexam.Mfenced");
    return ll_cas.ll_getStringValue(addr, casFeatCode_separators);
  }
  /** @generated */    
  public void setSeparators(int addr, String v) {
        if (featOkTst && casFeat_separators == null)
      jcas.throwFeatMissing("separators", "org.kachako.types.centerexam.Mfenced");
    ll_cas.ll_setStringValue(addr, casFeatCode_separators, v);}
    
  
 
  /** @generated */
  final Feature casFeat_open;
  /** @generated */
  final int     casFeatCode_open;
  /** @generated */ 
  public String getOpen(int addr) {
        if (featOkTst && casFeat_open == null)
      jcas.throwFeatMissing("open", "org.kachako.types.centerexam.Mfenced");
    return ll_cas.ll_getStringValue(addr, casFeatCode_open);
  }
  /** @generated */    
  public void setOpen(int addr, String v) {
        if (featOkTst && casFeat_open == null)
      jcas.throwFeatMissing("open", "org.kachako.types.centerexam.Mfenced");
    ll_cas.ll_setStringValue(addr, casFeatCode_open, v);}
    
  
 
  /** @generated */
  final Feature casFeat_close;
  /** @generated */
  final int     casFeatCode_close;
  /** @generated */ 
  public String getClose(int addr) {
        if (featOkTst && casFeat_close == null)
      jcas.throwFeatMissing("close", "org.kachako.types.centerexam.Mfenced");
    return ll_cas.ll_getStringValue(addr, casFeatCode_close);
  }
  /** @generated */    
  public void setClose(int addr, String v) {
        if (featOkTst && casFeat_close == null)
      jcas.throwFeatMissing("close", "org.kachako.types.centerexam.Mfenced");
    ll_cas.ll_setStringValue(addr, casFeatCode_close, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Mfenced_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_separators = jcas.getRequiredFeatureDE(casType, "separators", "uima.cas.String", featOkTst);
    casFeatCode_separators  = (null == casFeat_separators) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_separators).getCode();

 
    casFeat_open = jcas.getRequiredFeatureDE(casType, "open", "uima.cas.String", featOkTst);
    casFeatCode_open  = (null == casFeat_open) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_open).getCode();

 
    casFeat_close = jcas.getRequiredFeatureDE(casType, "close", "uima.cas.String", featOkTst);
    casFeatCode_close  = (null == casFeat_close) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_close).getCode();

  }
}



    