

/* First created by JCasGen Wed Jun 19 10:11:54 JST 2013 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class Mspace extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Mspace.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Mspace() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Mspace(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Mspace(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Mspace(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: width

  /** getter for width - gets 
   * @generated */
  public String getWidth() {
    if (Mspace_Type.featOkTst && ((Mspace_Type)jcasType).casFeat_width == null)
      jcasType.jcas.throwFeatMissing("width", "org.kachako.types.centerexam.Mspace");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Mspace_Type)jcasType).casFeatCode_width);}
    
  /** setter for width - sets  
   * @generated */
  public void setWidth(String v) {
    if (Mspace_Type.featOkTst && ((Mspace_Type)jcasType).casFeat_width == null)
      jcasType.jcas.throwFeatMissing("width", "org.kachako.types.centerexam.Mspace");
    jcasType.ll_cas.ll_setStringValue(addr, ((Mspace_Type)jcasType).casFeatCode_width, v);}    
  }

    