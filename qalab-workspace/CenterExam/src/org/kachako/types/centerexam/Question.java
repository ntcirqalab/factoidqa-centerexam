

/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class Question extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Question.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Question() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Question(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Question(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Question(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: id

  /** getter for id - gets 
   * @generated */
  public String getId() {
    if (Question_Type.featOkTst && ((Question_Type)jcasType).casFeat_id == null)
      jcasType.jcas.throwFeatMissing("id", "org.kachako.types.centerexam.Question");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Question_Type)jcasType).casFeatCode_id);}
    
  /** setter for id - sets  
   * @generated */
  public void setId(String v) {
    if (Question_Type.featOkTst && ((Question_Type)jcasType).casFeat_id == null)
      jcasType.jcas.throwFeatMissing("id", "org.kachako.types.centerexam.Question");
    jcasType.ll_cas.ll_setStringValue(addr, ((Question_Type)jcasType).casFeatCode_id, v);}    
   
    
  //*--------------*
  //* Feature: minimal

  /** getter for minimal - gets 
   * @generated */
  public String getMinimal() {
    if (Question_Type.featOkTst && ((Question_Type)jcasType).casFeat_minimal == null)
      jcasType.jcas.throwFeatMissing("minimal", "org.kachako.types.centerexam.Question");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Question_Type)jcasType).casFeatCode_minimal);}
    
  /** setter for minimal - sets  
   * @generated */
  public void setMinimal(String v) {
    if (Question_Type.featOkTst && ((Question_Type)jcasType).casFeat_minimal == null)
      jcasType.jcas.throwFeatMissing("minimal", "org.kachako.types.centerexam.Question");
    jcasType.ll_cas.ll_setStringValue(addr, ((Question_Type)jcasType).casFeatCode_minimal, v);}    
   
    
  //*--------------*
  //* Feature: answer_style

  /** getter for answer_style - gets 
   * @generated */
  public String getAnswer_style() {
    if (Question_Type.featOkTst && ((Question_Type)jcasType).casFeat_answer_style == null)
      jcasType.jcas.throwFeatMissing("answer_style", "org.kachako.types.centerexam.Question");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Question_Type)jcasType).casFeatCode_answer_style);}
    
  /** setter for answer_style - sets  
   * @generated */
  public void setAnswer_style(String v) {
    if (Question_Type.featOkTst && ((Question_Type)jcasType).casFeat_answer_style == null)
      jcasType.jcas.throwFeatMissing("answer_style", "org.kachako.types.centerexam.Question");
    jcasType.ll_cas.ll_setStringValue(addr, ((Question_Type)jcasType).casFeatCode_answer_style, v);}    
   
    
  //*--------------*
  //* Feature: answer_type

  /** getter for answer_type - gets 
   * @generated */
  public String getAnswer_type() {
    if (Question_Type.featOkTst && ((Question_Type)jcasType).casFeat_answer_type == null)
      jcasType.jcas.throwFeatMissing("answer_type", "org.kachako.types.centerexam.Question");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Question_Type)jcasType).casFeatCode_answer_type);}
    
  /** setter for answer_type - sets  
   * @generated */
  public void setAnswer_type(String v) {
    if (Question_Type.featOkTst && ((Question_Type)jcasType).casFeat_answer_type == null)
      jcasType.jcas.throwFeatMissing("answer_type", "org.kachako.types.centerexam.Question");
    jcasType.ll_cas.ll_setStringValue(addr, ((Question_Type)jcasType).casFeatCode_answer_type, v);}    
   
    
  //*--------------*
  //* Feature: knowledge_type

  /** getter for knowledge_type - gets 
   * @generated */
  public String getKnowledge_type() {
    if (Question_Type.featOkTst && ((Question_Type)jcasType).casFeat_knowledge_type == null)
      jcasType.jcas.throwFeatMissing("knowledge_type", "org.kachako.types.centerexam.Question");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Question_Type)jcasType).casFeatCode_knowledge_type);}
    
  /** setter for knowledge_type - sets  
   * @generated */
  public void setKnowledge_type(String v) {
    if (Question_Type.featOkTst && ((Question_Type)jcasType).casFeat_knowledge_type == null)
      jcasType.jcas.throwFeatMissing("knowledge_type", "org.kachako.types.centerexam.Question");
    jcasType.ll_cas.ll_setStringValue(addr, ((Question_Type)jcasType).casFeatCode_knowledge_type, v);}    
   
    
  //*--------------*
  //* Feature: anscol

  /** getter for anscol - gets 
   * @generated */
  public String getAnscol() {
    if (Question_Type.featOkTst && ((Question_Type)jcasType).casFeat_anscol == null)
      jcasType.jcas.throwFeatMissing("anscol", "org.kachako.types.centerexam.Question");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Question_Type)jcasType).casFeatCode_anscol);}
    
  /** setter for anscol - sets  
   * @generated */
  public void setAnscol(String v) {
    if (Question_Type.featOkTst && ((Question_Type)jcasType).casFeat_anscol == null)
      jcasType.jcas.throwFeatMissing("anscol", "org.kachako.types.centerexam.Question");
    jcasType.ll_cas.ll_setStringValue(addr, ((Question_Type)jcasType).casFeatCode_anscol, v);}    
   
    
  //*--------------*
  //* Feature: title

  /** getter for title - gets 
   * @generated */
  public String getTitle() {
    if (Question_Type.featOkTst && ((Question_Type)jcasType).casFeat_title == null)
      jcasType.jcas.throwFeatMissing("title", "org.kachako.types.centerexam.Question");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Question_Type)jcasType).casFeatCode_title);}
    
  /** setter for title - sets  
   * @generated */
  public void setTitle(String v) {
    if (Question_Type.featOkTst && ((Question_Type)jcasType).casFeat_title == null)
      jcasType.jcas.throwFeatMissing("title", "org.kachako.types.centerexam.Question");
    jcasType.ll_cas.ll_setStringValue(addr, ((Question_Type)jcasType).casFeatCode_title, v);}    
   
    
  //*--------------*
  //* Feature: sectionId

  /** getter for sectionId - gets 
   * @generated */
  public String getSectionId() {
    if (Question_Type.featOkTst && ((Question_Type)jcasType).casFeat_sectionId == null)
      jcasType.jcas.throwFeatMissing("sectionId", "org.kachako.types.centerexam.Question");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Question_Type)jcasType).casFeatCode_sectionId);}
    
  /** setter for sectionId - sets  
   * @generated */
  public void setSectionId(String v) {
    if (Question_Type.featOkTst && ((Question_Type)jcasType).casFeat_sectionId == null)
      jcasType.jcas.throwFeatMissing("sectionId", "org.kachako.types.centerexam.Question");
    jcasType.ll_cas.ll_setStringValue(addr, ((Question_Type)jcasType).casFeatCode_sectionId, v);}    
   
    
  //*--------------*
  //* Feature: anscolumn_ids

  /** getter for anscolumn_ids - gets 
   * @generated */
  public String getAnscolumn_ids() {
    if (Question_Type.featOkTst && ((Question_Type)jcasType).casFeat_anscolumn_ids == null)
      jcasType.jcas.throwFeatMissing("anscolumn_ids", "org.kachako.types.centerexam.Question");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Question_Type)jcasType).casFeatCode_anscolumn_ids);}
    
  /** setter for anscolumn_ids - sets  
   * @generated */
  public void setAnscolumn_ids(String v) {
    if (Question_Type.featOkTst && ((Question_Type)jcasType).casFeat_anscolumn_ids == null)
      jcasType.jcas.throwFeatMissing("anscolumn_ids", "org.kachako.types.centerexam.Question");
    jcasType.ll_cas.ll_setStringValue(addr, ((Question_Type)jcasType).casFeatCode_anscolumn_ids, v);}    
  }

    