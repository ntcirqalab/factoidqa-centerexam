

/* First created by JCasGen Thu Apr 04 11:46:08 JST 2013 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class Mi extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Mi.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Mi() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Mi(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Mi(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Mi(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
  //*--------------*
  //* Feature: mathvariant

  /** getter for mathvariant - gets 
   * @generated */
  public String getMathvariant() {
    if (Mi_Type.featOkTst && ((Mi_Type)jcasType).casFeat_mathvariant == null)
      jcasType.jcas.throwFeatMissing("mathvariant", "org.kachako.types.centerexam.Mi");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Mi_Type)jcasType).casFeatCode_mathvariant);}
    
  /** setter for mathvariant - sets  
   * @generated */
  public void setMathvariant(String v) {
    if (Mi_Type.featOkTst && ((Mi_Type)jcasType).casFeat_mathvariant == null)
      jcasType.jcas.throwFeatMissing("mathvariant", "org.kachako.types.centerexam.Mi");
    jcasType.ll_cas.ll_setStringValue(addr, ((Mi_Type)jcasType).casFeatCode_mathvariant, v);}    
   
    
  //*--------------*
  //* Feature: anscolumn_id

  /** getter for anscolumn_id - gets 
   * @generated */
  public String getAnscolumn_id() {
    if (Mi_Type.featOkTst && ((Mi_Type)jcasType).casFeat_anscolumn_id == null)
      jcasType.jcas.throwFeatMissing("anscolumn_id", "org.kachako.types.centerexam.Mi");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Mi_Type)jcasType).casFeatCode_anscolumn_id);}
    
  /** setter for anscolumn_id - sets  
   * @generated */
  public void setAnscolumn_id(String v) {
    if (Mi_Type.featOkTst && ((Mi_Type)jcasType).casFeat_anscolumn_id == null)
      jcasType.jcas.throwFeatMissing("anscolumn_id", "org.kachako.types.centerexam.Mi");
    jcasType.ll_cas.ll_setStringValue(addr, ((Mi_Type)jcasType).casFeatCode_anscolumn_id, v);}    
  }

    