

/* First created by JCasGen Thu Apr 04 12:27:10 JST 2013 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:40 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class Csymbol extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Csymbol.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Csymbol() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Csymbol(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Csymbol(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Csymbol(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: cd

  /** getter for cd - gets 
   * @generated */
  public String getCd() {
    if (Csymbol_Type.featOkTst && ((Csymbol_Type)jcasType).casFeat_cd == null)
      jcasType.jcas.throwFeatMissing("cd", "org.kachako.types.centerexam.Csymbol");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Csymbol_Type)jcasType).casFeatCode_cd);}
    
  /** setter for cd - sets  
   * @generated */
  public void setCd(String v) {
    if (Csymbol_Type.featOkTst && ((Csymbol_Type)jcasType).casFeat_cd == null)
      jcasType.jcas.throwFeatMissing("cd", "org.kachako.types.centerexam.Csymbol");
    jcasType.ll_cas.ll_setStringValue(addr, ((Csymbol_Type)jcasType).casFeatCode_cd, v);}    
   
    
  //*--------------*
  //* Feature: definitionURL

  /** getter for definitionURL - gets 
   * @generated */
  public String getDefinitionURL() {
    if (Csymbol_Type.featOkTst && ((Csymbol_Type)jcasType).casFeat_definitionURL == null)
      jcasType.jcas.throwFeatMissing("definitionURL", "org.kachako.types.centerexam.Csymbol");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Csymbol_Type)jcasType).casFeatCode_definitionURL);}
    
  /** setter for definitionURL - sets  
   * @generated */
  public void setDefinitionURL(String v) {
    if (Csymbol_Type.featOkTst && ((Csymbol_Type)jcasType).casFeat_definitionURL == null)
      jcasType.jcas.throwFeatMissing("definitionURL", "org.kachako.types.centerexam.Csymbol");
    jcasType.ll_cas.ll_setStringValue(addr, ((Csymbol_Type)jcasType).casFeatCode_definitionURL, v);}    
  }

    