

/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class Ref extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Ref.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Ref() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Ref(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Ref(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Ref(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: comment

  /** getter for comment - gets 
   * @generated */
  public String getComment() {
    if (Ref_Type.featOkTst && ((Ref_Type)jcasType).casFeat_comment == null)
      jcasType.jcas.throwFeatMissing("comment", "org.kachako.types.centerexam.Ref");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Ref_Type)jcasType).casFeatCode_comment);}
    
  /** setter for comment - sets  
   * @generated */
  public void setComment(String v) {
    if (Ref_Type.featOkTst && ((Ref_Type)jcasType).casFeat_comment == null)
      jcasType.jcas.throwFeatMissing("comment", "org.kachako.types.centerexam.Ref");
    jcasType.ll_cas.ll_setStringValue(addr, ((Ref_Type)jcasType).casFeatCode_comment, v);}    
   
    
  //*--------------*
  //* Feature: target

  /** getter for target - gets 
   * @generated */
  public String getTarget() {
    if (Ref_Type.featOkTst && ((Ref_Type)jcasType).casFeat_target == null)
      jcasType.jcas.throwFeatMissing("target", "org.kachako.types.centerexam.Ref");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Ref_Type)jcasType).casFeatCode_target);}
    
  /** setter for target - sets  
   * @generated */
  public void setTarget(String v) {
    if (Ref_Type.featOkTst && ((Ref_Type)jcasType).casFeat_target == null)
      jcasType.jcas.throwFeatMissing("target", "org.kachako.types.centerexam.Ref");
    jcasType.ll_cas.ll_setStringValue(addr, ((Ref_Type)jcasType).casFeatCode_target, v);}    
   
    
  //*--------------*
  //* Feature: reference

  /** getter for reference - gets 
   * @generated */
  public Annotation getReference() {
    if (Ref_Type.featOkTst && ((Ref_Type)jcasType).casFeat_reference == null)
      jcasType.jcas.throwFeatMissing("reference", "org.kachako.types.centerexam.Ref");
    return (Annotation)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((Ref_Type)jcasType).casFeatCode_reference)));}
    
  /** setter for reference - sets  
   * @generated */
  public void setReference(Annotation v) {
    if (Ref_Type.featOkTst && ((Ref_Type)jcasType).casFeat_reference == null)
      jcasType.jcas.throwFeatMissing("reference", "org.kachako.types.centerexam.Ref");
    jcasType.ll_cas.ll_setRefValue(addr, ((Ref_Type)jcasType).casFeatCode_reference, jcasType.ll_cas.ll_getFSRef(v));}    
  }

    