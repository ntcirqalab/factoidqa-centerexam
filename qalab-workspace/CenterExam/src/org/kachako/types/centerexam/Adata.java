

/* First created by JCasGen Wed Aug 21 14:13:41 JST 2013 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class Adata extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Adata.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Adata() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Adata(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Adata(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Adata(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: id

  /** getter for id - gets 
   * @generated */
  public String getId() {
    if (Adata_Type.featOkTst && ((Adata_Type)jcasType).casFeat_id == null)
      jcasType.jcas.throwFeatMissing("id", "org.kachako.types.centerexam.Adata");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Adata_Type)jcasType).casFeatCode_id);}
    
  /** setter for id - sets  
   * @generated */
  public void setId(String v) {
    if (Adata_Type.featOkTst && ((Adata_Type)jcasType).casFeat_id == null)
      jcasType.jcas.throwFeatMissing("id", "org.kachako.types.centerexam.Adata");
    jcasType.ll_cas.ll_setStringValue(addr, ((Adata_Type)jcasType).casFeatCode_id, v);}    
   
    
  //*--------------*
  //* Feature: adataType

  /** getter for adataType - gets 
   * @generated */
  public String getAdataType() {
    if (Adata_Type.featOkTst && ((Adata_Type)jcasType).casFeat_adataType == null)
      jcasType.jcas.throwFeatMissing("adataType", "org.kachako.types.centerexam.Adata");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Adata_Type)jcasType).casFeatCode_adataType);}
    
  /** setter for adataType - sets  
   * @generated */
  public void setAdataType(String v) {
    if (Adata_Type.featOkTst && ((Adata_Type)jcasType).casFeat_adataType == null)
      jcasType.jcas.throwFeatMissing("adataType", "org.kachako.types.centerexam.Adata");
    jcasType.ll_cas.ll_setStringValue(addr, ((Adata_Type)jcasType).casFeatCode_adataType, v);}    
   
    
  //*--------------*
  //* Feature: field

  /** getter for field - gets 
   * @generated */
  public String getField() {
    if (Adata_Type.featOkTst && ((Adata_Type)jcasType).casFeat_field == null)
      jcasType.jcas.throwFeatMissing("field", "org.kachako.types.centerexam.Adata");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Adata_Type)jcasType).casFeatCode_field);}
    
  /** setter for field - sets  
   * @generated */
  public void setField(String v) {
    if (Adata_Type.featOkTst && ((Adata_Type)jcasType).casFeat_field == null)
      jcasType.jcas.throwFeatMissing("field", "org.kachako.types.centerexam.Adata");
    jcasType.ll_cas.ll_setStringValue(addr, ((Adata_Type)jcasType).casFeatCode_field, v);}    
  }

    