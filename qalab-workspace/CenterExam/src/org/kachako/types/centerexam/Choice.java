

/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:40 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class Choice extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Choice.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Choice() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Choice(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Choice(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Choice(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: ra

  /** getter for ra - gets 
   * @generated */
  public String getRa() {
    if (Choice_Type.featOkTst && ((Choice_Type)jcasType).casFeat_ra == null)
      jcasType.jcas.throwFeatMissing("ra", "org.kachako.types.centerexam.Choice");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Choice_Type)jcasType).casFeatCode_ra);}
    
  /** setter for ra - sets  
   * @generated */
  public void setRa(String v) {
    if (Choice_Type.featOkTst && ((Choice_Type)jcasType).casFeat_ra == null)
      jcasType.jcas.throwFeatMissing("ra", "org.kachako.types.centerexam.Choice");
    jcasType.ll_cas.ll_setStringValue(addr, ((Choice_Type)jcasType).casFeatCode_ra, v);}    
   
    
  //*--------------*
  //* Feature: comment

  /** getter for comment - gets 
   * @generated */
  public String getComment() {
    if (Choice_Type.featOkTst && ((Choice_Type)jcasType).casFeat_comment == null)
      jcasType.jcas.throwFeatMissing("comment", "org.kachako.types.centerexam.Choice");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Choice_Type)jcasType).casFeatCode_comment);}
    
  /** setter for comment - sets  
   * @generated */
  public void setComment(String v) {
    if (Choice_Type.featOkTst && ((Choice_Type)jcasType).casFeat_comment == null)
      jcasType.jcas.throwFeatMissing("comment", "org.kachako.types.centerexam.Choice");
    jcasType.ll_cas.ll_setStringValue(addr, ((Choice_Type)jcasType).casFeatCode_comment, v);}    
   
    
  //*--------------*
  //* Feature: ansnum

  /** getter for ansnum - gets 
   * @generated */
  public String getAnsnum() {
    if (Choice_Type.featOkTst && ((Choice_Type)jcasType).casFeat_ansnum == null)
      jcasType.jcas.throwFeatMissing("ansnum", "org.kachako.types.centerexam.Choice");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Choice_Type)jcasType).casFeatCode_ansnum);}
    
  /** setter for ansnum - sets  
   * @generated */
  public void setAnsnum(String v) {
    if (Choice_Type.featOkTst && ((Choice_Type)jcasType).casFeat_ansnum == null)
      jcasType.jcas.throwFeatMissing("ansnum", "org.kachako.types.centerexam.Choice");
    jcasType.ll_cas.ll_setStringValue(addr, ((Choice_Type)jcasType).casFeatCode_ansnum, v);}    
  }

    