

/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class Tbl extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Tbl.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Tbl() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Tbl(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Tbl(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Tbl(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: numOfCols

  /** getter for numOfCols - gets 
   * @generated */
  public int getNumOfCols() {
    if (Tbl_Type.featOkTst && ((Tbl_Type)jcasType).casFeat_numOfCols == null)
      jcasType.jcas.throwFeatMissing("numOfCols", "org.kachako.types.centerexam.Tbl");
    return jcasType.ll_cas.ll_getIntValue(addr, ((Tbl_Type)jcasType).casFeatCode_numOfCols);}
    
  /** setter for numOfCols - sets  
   * @generated */
  public void setNumOfCols(int v) {
    if (Tbl_Type.featOkTst && ((Tbl_Type)jcasType).casFeat_numOfCols == null)
      jcasType.jcas.throwFeatMissing("numOfCols", "org.kachako.types.centerexam.Tbl");
    jcasType.ll_cas.ll_setIntValue(addr, ((Tbl_Type)jcasType).casFeatCode_numOfCols, v);}    
   
    
  //*--------------*
  //* Feature: numOfRows

  /** getter for numOfRows - gets 
   * @generated */
  public int getNumOfRows() {
    if (Tbl_Type.featOkTst && ((Tbl_Type)jcasType).casFeat_numOfRows == null)
      jcasType.jcas.throwFeatMissing("numOfRows", "org.kachako.types.centerexam.Tbl");
    return jcasType.ll_cas.ll_getIntValue(addr, ((Tbl_Type)jcasType).casFeatCode_numOfRows);}
    
  /** setter for numOfRows - sets  
   * @generated */
  public void setNumOfRows(int v) {
    if (Tbl_Type.featOkTst && ((Tbl_Type)jcasType).casFeat_numOfRows == null)
      jcasType.jcas.throwFeatMissing("numOfRows", "org.kachako.types.centerexam.Tbl");
    jcasType.ll_cas.ll_setIntValue(addr, ((Tbl_Type)jcasType).casFeatCode_numOfRows, v);}    
  }

    