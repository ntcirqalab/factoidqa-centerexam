

/* First created by JCasGen Fri Apr 12 12:02:49 JST 2013 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class ScalarProduct extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(ScalarProduct.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected ScalarProduct() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public ScalarProduct(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public ScalarProduct(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public ScalarProduct(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
}

    