
/* First created by JCasGen Thu Apr 04 12:52:50 JST 2013 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * @generated */
public class Table_Type extends CenterExamTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Table_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Table_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Table(addr, Table_Type.this);
  			   Table_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Table(addr, Table_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Table.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.Table");
 
  /** @generated */
  final Feature casFeat_xmlns;
  /** @generated */
  final int     casFeatCode_xmlns;
  /** @generated */ 
  public String getXmlns(int addr) {
        if (featOkTst && casFeat_xmlns == null)
      jcas.throwFeatMissing("xmlns", "org.kachako.types.centerexam.Table");
    return ll_cas.ll_getStringValue(addr, casFeatCode_xmlns);
  }
  /** @generated */    
  public void setXmlns(int addr, String v) {
        if (featOkTst && casFeat_xmlns == null)
      jcas.throwFeatMissing("xmlns", "org.kachako.types.centerexam.Table");
    ll_cas.ll_setStringValue(addr, casFeatCode_xmlns, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Table_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_xmlns = jcas.getRequiredFeatureDE(casType, "xmlns", "uima.cas.String", featOkTst);
    casFeatCode_xmlns  = (null == casFeat_xmlns) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_xmlns).getCode();

  }
}



    