

/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class Missing extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Missing.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Missing() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Missing(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Missing(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Missing(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: attributeString

  /** getter for attributeString - gets 
   * @generated */
  public String getAttributeString() {
    if (Missing_Type.featOkTst && ((Missing_Type)jcasType).casFeat_attributeString == null)
      jcasType.jcas.throwFeatMissing("attributeString", "org.kachako.types.centerexam.Missing");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Missing_Type)jcasType).casFeatCode_attributeString);}
    
  /** setter for attributeString - sets  
   * @generated */
  public void setAttributeString(String v) {
    if (Missing_Type.featOkTst && ((Missing_Type)jcasType).casFeat_attributeString == null)
      jcasType.jcas.throwFeatMissing("attributeString", "org.kachako.types.centerexam.Missing");
    jcasType.ll_cas.ll_setStringValue(addr, ((Missing_Type)jcasType).casFeatCode_attributeString, v);}    
  }

    