

/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class Img extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Img.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Img() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Img(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Img(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Img(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: src

  /** getter for src - gets 
   * @generated */
  public String getSrc() {
    if (Img_Type.featOkTst && ((Img_Type)jcasType).casFeat_src == null)
      jcasType.jcas.throwFeatMissing("src", "org.kachako.types.centerexam.Img");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Img_Type)jcasType).casFeatCode_src);}
    
  /** setter for src - sets  
   * @generated */
  public void setSrc(String v) {
    if (Img_Type.featOkTst && ((Img_Type)jcasType).casFeat_src == null)
      jcasType.jcas.throwFeatMissing("src", "org.kachako.types.centerexam.Img");
    jcasType.ll_cas.ll_setStringValue(addr, ((Img_Type)jcasType).casFeatCode_src, v);}    
   
    
  //*--------------*
  //* Feature: comment

  /** getter for comment - gets 
   * @generated */
  public String getComment() {
    if (Img_Type.featOkTst && ((Img_Type)jcasType).casFeat_comment == null)
      jcasType.jcas.throwFeatMissing("comment", "org.kachako.types.centerexam.Img");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Img_Type)jcasType).casFeatCode_comment);}
    
  /** setter for comment - sets  
   * @generated */
  public void setComment(String v) {
    if (Img_Type.featOkTst && ((Img_Type)jcasType).casFeat_comment == null)
      jcasType.jcas.throwFeatMissing("comment", "org.kachako.types.centerexam.Img");
    jcasType.ll_cas.ll_setStringValue(addr, ((Img_Type)jcasType).casFeatCode_comment, v);}    
   
    
  //*--------------*
  //* Feature: corresponding_obj

  /** getter for corresponding_obj - gets 
   * @generated */
  public String getCorresponding_obj() {
    if (Img_Type.featOkTst && ((Img_Type)jcasType).casFeat_corresponding_obj == null)
      jcasType.jcas.throwFeatMissing("corresponding_obj", "org.kachako.types.centerexam.Img");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Img_Type)jcasType).casFeatCode_corresponding_obj);}
    
  /** setter for corresponding_obj - sets  
   * @generated */
  public void setCorresponding_obj(String v) {
    if (Img_Type.featOkTst && ((Img_Type)jcasType).casFeat_corresponding_obj == null)
      jcasType.jcas.throwFeatMissing("corresponding_obj", "org.kachako.types.centerexam.Img");
    jcasType.ll_cas.ll_setStringValue(addr, ((Img_Type)jcasType).casFeatCode_corresponding_obj, v);}    
  }

    