

/* First created by JCasGen Thu Apr 04 11:46:07 JST 2013 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:40 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class Ci extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Ci.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Ci() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Ci(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Ci(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Ci(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: ciType

  /** getter for ciType - gets 
   * @generated */
  public String getCiType() {
    if (Ci_Type.featOkTst && ((Ci_Type)jcasType).casFeat_ciType == null)
      jcasType.jcas.throwFeatMissing("ciType", "org.kachako.types.centerexam.Ci");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Ci_Type)jcasType).casFeatCode_ciType);}
    
  /** setter for ciType - sets  
   * @generated */
  public void setCiType(String v) {
    if (Ci_Type.featOkTst && ((Ci_Type)jcasType).casFeat_ciType == null)
      jcasType.jcas.throwFeatMissing("ciType", "org.kachako.types.centerexam.Ci");
    jcasType.ll_cas.ll_setStringValue(addr, ((Ci_Type)jcasType).casFeatCode_ciType, v);}    
  }

    