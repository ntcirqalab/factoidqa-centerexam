
/* First created by JCasGen Thu Apr 04 12:52:50 JST 2013 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * @generated */
public class Mstyle_Type extends CenterExamTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Mstyle_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Mstyle_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Mstyle(addr, Mstyle_Type.this);
  			   Mstyle_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Mstyle(addr, Mstyle_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Mstyle.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.Mstyle");
 
  /** @generated */
  final Feature casFeat_displaystyle;
  /** @generated */
  final int     casFeatCode_displaystyle;
  /** @generated */ 
  public String getDisplaystyle(int addr) {
        if (featOkTst && casFeat_displaystyle == null)
      jcas.throwFeatMissing("displaystyle", "org.kachako.types.centerexam.Mstyle");
    return ll_cas.ll_getStringValue(addr, casFeatCode_displaystyle);
  }
  /** @generated */    
  public void setDisplaystyle(int addr, String v) {
        if (featOkTst && casFeat_displaystyle == null)
      jcas.throwFeatMissing("displaystyle", "org.kachako.types.centerexam.Mstyle");
    ll_cas.ll_setStringValue(addr, casFeatCode_displaystyle, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Mstyle_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_displaystyle = jcas.getRequiredFeatureDE(casType, "displaystyle", "uima.cas.String", featOkTst);
    casFeatCode_displaystyle  = (null == casFeat_displaystyle) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_displaystyle).getCode();

  }
}



    