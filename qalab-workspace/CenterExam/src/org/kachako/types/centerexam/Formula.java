

/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class Formula extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Formula.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Formula() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Formula(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Formula(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Formula(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: comment

  /** getter for comment - gets 
   * @generated */
  public String getComment() {
    if (Formula_Type.featOkTst && ((Formula_Type)jcasType).casFeat_comment == null)
      jcasType.jcas.throwFeatMissing("comment", "org.kachako.types.centerexam.Formula");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Formula_Type)jcasType).casFeatCode_comment);}
    
  /** setter for comment - sets  
   * @generated */
  public void setComment(String v) {
    if (Formula_Type.featOkTst && ((Formula_Type)jcasType).casFeat_comment == null)
      jcasType.jcas.throwFeatMissing("comment", "org.kachako.types.centerexam.Formula");
    jcasType.ll_cas.ll_setStringValue(addr, ((Formula_Type)jcasType).casFeatCode_comment, v);}    
  }

    