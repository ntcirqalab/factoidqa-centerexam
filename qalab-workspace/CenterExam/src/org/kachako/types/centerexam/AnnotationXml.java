

/* First created by JCasGen Thu Apr 04 11:46:07 JST 2013 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:40 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class AnnotationXml extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(AnnotationXml.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected AnnotationXml() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public AnnotationXml(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public AnnotationXml(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public AnnotationXml(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: encoding

  /** getter for encoding - gets 
   * @generated */
  public String getEncoding() {
    if (AnnotationXml_Type.featOkTst && ((AnnotationXml_Type)jcasType).casFeat_encoding == null)
      jcasType.jcas.throwFeatMissing("encoding", "org.kachako.types.centerexam.AnnotationXml");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnnotationXml_Type)jcasType).casFeatCode_encoding);}
    
  /** setter for encoding - sets  
   * @generated */
  public void setEncoding(String v) {
    if (AnnotationXml_Type.featOkTst && ((AnnotationXml_Type)jcasType).casFeat_encoding == null)
      jcasType.jcas.throwFeatMissing("encoding", "org.kachako.types.centerexam.AnnotationXml");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnnotationXml_Type)jcasType).casFeatCode_encoding, v);}    
  }

    