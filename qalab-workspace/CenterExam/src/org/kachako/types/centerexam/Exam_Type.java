
/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * @generated */
public class Exam_Type extends CenterExamTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Exam_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Exam_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Exam(addr, Exam_Type.this);
  			   Exam_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Exam(addr, Exam_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Exam.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.Exam");
 
  /** @generated */
  final Feature casFeat_source;
  /** @generated */
  final int     casFeatCode_source;
  /** @generated */ 
  public String getSource(int addr) {
        if (featOkTst && casFeat_source == null)
      jcas.throwFeatMissing("source", "org.kachako.types.centerexam.Exam");
    return ll_cas.ll_getStringValue(addr, casFeatCode_source);
  }
  /** @generated */    
  public void setSource(int addr, String v) {
        if (featOkTst && casFeat_source == null)
      jcas.throwFeatMissing("source", "org.kachako.types.centerexam.Exam");
    ll_cas.ll_setStringValue(addr, casFeatCode_source, v);}
    
  
 
  /** @generated */
  final Feature casFeat_srcTxtURL;
  /** @generated */
  final int     casFeatCode_srcTxtURL;
  /** @generated */ 
  public String getSrcTxtURL(int addr) {
        if (featOkTst && casFeat_srcTxtURL == null)
      jcas.throwFeatMissing("srcTxtURL", "org.kachako.types.centerexam.Exam");
    return ll_cas.ll_getStringValue(addr, casFeatCode_srcTxtURL);
  }
  /** @generated */    
  public void setSrcTxtURL(int addr, String v) {
        if (featOkTst && casFeat_srcTxtURL == null)
      jcas.throwFeatMissing("srcTxtURL", "org.kachako.types.centerexam.Exam");
    ll_cas.ll_setStringValue(addr, casFeatCode_srcTxtURL, v);}
    
  
 
  /** @generated */
  final Feature casFeat_subject;
  /** @generated */
  final int     casFeatCode_subject;
  /** @generated */ 
  public String getSubject(int addr) {
        if (featOkTst && casFeat_subject == null)
      jcas.throwFeatMissing("subject", "org.kachako.types.centerexam.Exam");
    return ll_cas.ll_getStringValue(addr, casFeatCode_subject);
  }
  /** @generated */    
  public void setSubject(int addr, String v) {
        if (featOkTst && casFeat_subject == null)
      jcas.throwFeatMissing("subject", "org.kachako.types.centerexam.Exam");
    ll_cas.ll_setStringValue(addr, casFeatCode_subject, v);}
    
  
 
  /** @generated */
  final Feature casFeat_year;
  /** @generated */
  final int     casFeatCode_year;
  /** @generated */ 
  public int getYear(int addr) {
        if (featOkTst && casFeat_year == null)
      jcas.throwFeatMissing("year", "org.kachako.types.centerexam.Exam");
    return ll_cas.ll_getIntValue(addr, casFeatCode_year);
  }
  /** @generated */    
  public void setYear(int addr, int v) {
        if (featOkTst && casFeat_year == null)
      jcas.throwFeatMissing("year", "org.kachako.types.centerexam.Exam");
    ll_cas.ll_setIntValue(addr, casFeatCode_year, v);}
    
  
 
  /** @generated */
  final Feature casFeat_math;
  /** @generated */
  final int     casFeatCode_math;
  /** @generated */ 
  public boolean getMath(int addr) {
        if (featOkTst && casFeat_math == null)
      jcas.throwFeatMissing("math", "org.kachako.types.centerexam.Exam");
    return ll_cas.ll_getBooleanValue(addr, casFeatCode_math);
  }
  /** @generated */    
  public void setMath(int addr, boolean v) {
        if (featOkTst && casFeat_math == null)
      jcas.throwFeatMissing("math", "org.kachako.types.centerexam.Exam");
    ll_cas.ll_setBooleanValue(addr, casFeatCode_math, v);}
    
  
 
  /** @generated */
  final Feature casFeat_id;
  /** @generated */
  final int     casFeatCode_id;
  /** @generated */ 
  public String getId(int addr) {
        if (featOkTst && casFeat_id == null)
      jcas.throwFeatMissing("id", "org.kachako.types.centerexam.Exam");
    return ll_cas.ll_getStringValue(addr, casFeatCode_id);
  }
  /** @generated */    
  public void setId(int addr, String v) {
        if (featOkTst && casFeat_id == null)
      jcas.throwFeatMissing("id", "org.kachako.types.centerexam.Exam");
    ll_cas.ll_setStringValue(addr, casFeatCode_id, v);}
    
  
 
  /** @generated */
  final Feature casFeat_range_of_options;
  /** @generated */
  final int     casFeatCode_range_of_options;
  /** @generated */ 
  public String getRange_of_options(int addr) {
        if (featOkTst && casFeat_range_of_options == null)
      jcas.throwFeatMissing("range_of_options", "org.kachako.types.centerexam.Exam");
    return ll_cas.ll_getStringValue(addr, casFeatCode_range_of_options);
  }
  /** @generated */    
  public void setRange_of_options(int addr, String v) {
        if (featOkTst && casFeat_range_of_options == null)
      jcas.throwFeatMissing("range_of_options", "org.kachako.types.centerexam.Exam");
    ll_cas.ll_setStringValue(addr, casFeatCode_range_of_options, v);}
    
  
 
  /** @generated */
  final Feature casFeat_num_of_options;
  /** @generated */
  final int     casFeatCode_num_of_options;
  /** @generated */ 
  public String getNum_of_options(int addr) {
        if (featOkTst && casFeat_num_of_options == null)
      jcas.throwFeatMissing("num_of_options", "org.kachako.types.centerexam.Exam");
    return ll_cas.ll_getStringValue(addr, casFeatCode_num_of_options);
  }
  /** @generated */    
  public void setNum_of_options(int addr, String v) {
        if (featOkTst && casFeat_num_of_options == null)
      jcas.throwFeatMissing("num_of_options", "org.kachako.types.centerexam.Exam");
    ll_cas.ll_setStringValue(addr, casFeatCode_num_of_options, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Exam_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_source = jcas.getRequiredFeatureDE(casType, "source", "uima.cas.String", featOkTst);
    casFeatCode_source  = (null == casFeat_source) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_source).getCode();

 
    casFeat_srcTxtURL = jcas.getRequiredFeatureDE(casType, "srcTxtURL", "uima.cas.String", featOkTst);
    casFeatCode_srcTxtURL  = (null == casFeat_srcTxtURL) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_srcTxtURL).getCode();

 
    casFeat_subject = jcas.getRequiredFeatureDE(casType, "subject", "uima.cas.String", featOkTst);
    casFeatCode_subject  = (null == casFeat_subject) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_subject).getCode();

 
    casFeat_year = jcas.getRequiredFeatureDE(casType, "year", "uima.cas.Integer", featOkTst);
    casFeatCode_year  = (null == casFeat_year) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_year).getCode();

 
    casFeat_math = jcas.getRequiredFeatureDE(casType, "math", "uima.cas.Boolean", featOkTst);
    casFeatCode_math  = (null == casFeat_math) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_math).getCode();

 
    casFeat_id = jcas.getRequiredFeatureDE(casType, "id", "uima.cas.String", featOkTst);
    casFeatCode_id  = (null == casFeat_id) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_id).getCode();

 
    casFeat_range_of_options = jcas.getRequiredFeatureDE(casType, "range_of_options", "uima.cas.String", featOkTst);
    casFeatCode_range_of_options  = (null == casFeat_range_of_options) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_range_of_options).getCode();

 
    casFeat_num_of_options = jcas.getRequiredFeatureDE(casType, "num_of_options", "uima.cas.String", featOkTst);
    casFeatCode_num_of_options  = (null == casFeat_num_of_options) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_num_of_options).getCode();

  }
}



    