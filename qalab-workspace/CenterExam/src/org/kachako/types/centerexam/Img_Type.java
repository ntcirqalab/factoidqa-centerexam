
/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * @generated */
public class Img_Type extends CenterExamTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Img_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Img_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Img(addr, Img_Type.this);
  			   Img_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Img(addr, Img_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Img.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.Img");
 
  /** @generated */
  final Feature casFeat_src;
  /** @generated */
  final int     casFeatCode_src;
  /** @generated */ 
  public String getSrc(int addr) {
        if (featOkTst && casFeat_src == null)
      jcas.throwFeatMissing("src", "org.kachako.types.centerexam.Img");
    return ll_cas.ll_getStringValue(addr, casFeatCode_src);
  }
  /** @generated */    
  public void setSrc(int addr, String v) {
        if (featOkTst && casFeat_src == null)
      jcas.throwFeatMissing("src", "org.kachako.types.centerexam.Img");
    ll_cas.ll_setStringValue(addr, casFeatCode_src, v);}
    
  
 
  /** @generated */
  final Feature casFeat_comment;
  /** @generated */
  final int     casFeatCode_comment;
  /** @generated */ 
  public String getComment(int addr) {
        if (featOkTst && casFeat_comment == null)
      jcas.throwFeatMissing("comment", "org.kachako.types.centerexam.Img");
    return ll_cas.ll_getStringValue(addr, casFeatCode_comment);
  }
  /** @generated */    
  public void setComment(int addr, String v) {
        if (featOkTst && casFeat_comment == null)
      jcas.throwFeatMissing("comment", "org.kachako.types.centerexam.Img");
    ll_cas.ll_setStringValue(addr, casFeatCode_comment, v);}
    
  
 
  /** @generated */
  final Feature casFeat_corresponding_obj;
  /** @generated */
  final int     casFeatCode_corresponding_obj;
  /** @generated */ 
  public String getCorresponding_obj(int addr) {
        if (featOkTst && casFeat_corresponding_obj == null)
      jcas.throwFeatMissing("corresponding_obj", "org.kachako.types.centerexam.Img");
    return ll_cas.ll_getStringValue(addr, casFeatCode_corresponding_obj);
  }
  /** @generated */    
  public void setCorresponding_obj(int addr, String v) {
        if (featOkTst && casFeat_corresponding_obj == null)
      jcas.throwFeatMissing("corresponding_obj", "org.kachako.types.centerexam.Img");
    ll_cas.ll_setStringValue(addr, casFeatCode_corresponding_obj, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Img_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_src = jcas.getRequiredFeatureDE(casType, "src", "uima.cas.String", featOkTst);
    casFeatCode_src  = (null == casFeat_src) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_src).getCode();

 
    casFeat_comment = jcas.getRequiredFeatureDE(casType, "comment", "uima.cas.String", featOkTst);
    casFeatCode_comment  = (null == casFeat_comment) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_comment).getCode();

 
    casFeat_corresponding_obj = jcas.getRequiredFeatureDE(casType, "corresponding_obj", "uima.cas.String", featOkTst);
    casFeatCode_corresponding_obj  = (null == casFeat_corresponding_obj) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_corresponding_obj).getCode();

  }
}



    