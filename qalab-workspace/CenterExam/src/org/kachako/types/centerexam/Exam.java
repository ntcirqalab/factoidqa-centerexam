

/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class Exam extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Exam.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Exam() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Exam(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Exam(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Exam(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: source

  /** getter for source - gets 
   * @generated */
  public String getSource() {
    if (Exam_Type.featOkTst && ((Exam_Type)jcasType).casFeat_source == null)
      jcasType.jcas.throwFeatMissing("source", "org.kachako.types.centerexam.Exam");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Exam_Type)jcasType).casFeatCode_source);}
    
  /** setter for source - sets  
   * @generated */
  public void setSource(String v) {
    if (Exam_Type.featOkTst && ((Exam_Type)jcasType).casFeat_source == null)
      jcasType.jcas.throwFeatMissing("source", "org.kachako.types.centerexam.Exam");
    jcasType.ll_cas.ll_setStringValue(addr, ((Exam_Type)jcasType).casFeatCode_source, v);}    
   
    
  //*--------------*
  //* Feature: srcTxtURL

  /** getter for srcTxtURL - gets 
   * @generated */
  public String getSrcTxtURL() {
    if (Exam_Type.featOkTst && ((Exam_Type)jcasType).casFeat_srcTxtURL == null)
      jcasType.jcas.throwFeatMissing("srcTxtURL", "org.kachako.types.centerexam.Exam");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Exam_Type)jcasType).casFeatCode_srcTxtURL);}
    
  /** setter for srcTxtURL - sets  
   * @generated */
  public void setSrcTxtURL(String v) {
    if (Exam_Type.featOkTst && ((Exam_Type)jcasType).casFeat_srcTxtURL == null)
      jcasType.jcas.throwFeatMissing("srcTxtURL", "org.kachako.types.centerexam.Exam");
    jcasType.ll_cas.ll_setStringValue(addr, ((Exam_Type)jcasType).casFeatCode_srcTxtURL, v);}    
   
    
  //*--------------*
  //* Feature: subject

  /** getter for subject - gets 
   * @generated */
  public String getSubject() {
    if (Exam_Type.featOkTst && ((Exam_Type)jcasType).casFeat_subject == null)
      jcasType.jcas.throwFeatMissing("subject", "org.kachako.types.centerexam.Exam");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Exam_Type)jcasType).casFeatCode_subject);}
    
  /** setter for subject - sets  
   * @generated */
  public void setSubject(String v) {
    if (Exam_Type.featOkTst && ((Exam_Type)jcasType).casFeat_subject == null)
      jcasType.jcas.throwFeatMissing("subject", "org.kachako.types.centerexam.Exam");
    jcasType.ll_cas.ll_setStringValue(addr, ((Exam_Type)jcasType).casFeatCode_subject, v);}    
   
    
  //*--------------*
  //* Feature: year

  /** getter for year - gets 
   * @generated */
  public int getYear() {
    if (Exam_Type.featOkTst && ((Exam_Type)jcasType).casFeat_year == null)
      jcasType.jcas.throwFeatMissing("year", "org.kachako.types.centerexam.Exam");
    return jcasType.ll_cas.ll_getIntValue(addr, ((Exam_Type)jcasType).casFeatCode_year);}
    
  /** setter for year - sets  
   * @generated */
  public void setYear(int v) {
    if (Exam_Type.featOkTst && ((Exam_Type)jcasType).casFeat_year == null)
      jcasType.jcas.throwFeatMissing("year", "org.kachako.types.centerexam.Exam");
    jcasType.ll_cas.ll_setIntValue(addr, ((Exam_Type)jcasType).casFeatCode_year, v);}    
   
    
  //*--------------*
  //* Feature: math

  /** getter for math - gets 
   * @generated */
  public boolean getMath() {
    if (Exam_Type.featOkTst && ((Exam_Type)jcasType).casFeat_math == null)
      jcasType.jcas.throwFeatMissing("math", "org.kachako.types.centerexam.Exam");
    return jcasType.ll_cas.ll_getBooleanValue(addr, ((Exam_Type)jcasType).casFeatCode_math);}
    
  /** setter for math - sets  
   * @generated */
  public void setMath(boolean v) {
    if (Exam_Type.featOkTst && ((Exam_Type)jcasType).casFeat_math == null)
      jcasType.jcas.throwFeatMissing("math", "org.kachako.types.centerexam.Exam");
    jcasType.ll_cas.ll_setBooleanValue(addr, ((Exam_Type)jcasType).casFeatCode_math, v);}    
   
    
  //*--------------*
  //* Feature: id

  /** getter for id - gets 
   * @generated */
  public String getId() {
    if (Exam_Type.featOkTst && ((Exam_Type)jcasType).casFeat_id == null)
      jcasType.jcas.throwFeatMissing("id", "org.kachako.types.centerexam.Exam");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Exam_Type)jcasType).casFeatCode_id);}
    
  /** setter for id - sets  
   * @generated */
  public void setId(String v) {
    if (Exam_Type.featOkTst && ((Exam_Type)jcasType).casFeat_id == null)
      jcasType.jcas.throwFeatMissing("id", "org.kachako.types.centerexam.Exam");
    jcasType.ll_cas.ll_setStringValue(addr, ((Exam_Type)jcasType).casFeatCode_id, v);}    
   
    
  //*--------------*
  //* Feature: range_of_options

  /** getter for range_of_options - gets 
   * @generated */
  public String getRange_of_options() {
    if (Exam_Type.featOkTst && ((Exam_Type)jcasType).casFeat_range_of_options == null)
      jcasType.jcas.throwFeatMissing("range_of_options", "org.kachako.types.centerexam.Exam");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Exam_Type)jcasType).casFeatCode_range_of_options);}
    
  /** setter for range_of_options - sets  
   * @generated */
  public void setRange_of_options(String v) {
    if (Exam_Type.featOkTst && ((Exam_Type)jcasType).casFeat_range_of_options == null)
      jcasType.jcas.throwFeatMissing("range_of_options", "org.kachako.types.centerexam.Exam");
    jcasType.ll_cas.ll_setStringValue(addr, ((Exam_Type)jcasType).casFeatCode_range_of_options, v);}    
   
    
  //*--------------*
  //* Feature: num_of_options

  /** getter for num_of_options - gets 
   * @generated */
  public String getNum_of_options() {
    if (Exam_Type.featOkTst && ((Exam_Type)jcasType).casFeat_num_of_options == null)
      jcasType.jcas.throwFeatMissing("num_of_options", "org.kachako.types.centerexam.Exam");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Exam_Type)jcasType).casFeatCode_num_of_options);}
    
  /** setter for num_of_options - sets  
   * @generated */
  public void setNum_of_options(String v) {
    if (Exam_Type.featOkTst && ((Exam_Type)jcasType).casFeat_num_of_options == null)
      jcasType.jcas.throwFeatMissing("num_of_options", "org.kachako.types.centerexam.Exam");
    jcasType.ll_cas.ll_setStringValue(addr, ((Exam_Type)jcasType).casFeatCode_num_of_options, v);}    
  }

    