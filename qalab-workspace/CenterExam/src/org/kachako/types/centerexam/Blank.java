

/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:40 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class Blank extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Blank.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Blank() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Blank(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Blank(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Blank(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: id

  /** getter for id - gets 
   * @generated */
  public String getId() {
    if (Blank_Type.featOkTst && ((Blank_Type)jcasType).casFeat_id == null)
      jcasType.jcas.throwFeatMissing("id", "org.kachako.types.centerexam.Blank");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Blank_Type)jcasType).casFeatCode_id);}
    
  /** setter for id - sets  
   * @generated */
  public void setId(String v) {
    if (Blank_Type.featOkTst && ((Blank_Type)jcasType).casFeat_id == null)
      jcasType.jcas.throwFeatMissing("id", "org.kachako.types.centerexam.Blank");
    jcasType.ll_cas.ll_setStringValue(addr, ((Blank_Type)jcasType).casFeatCode_id, v);}    
   
    
  //*--------------*
  //* Feature: digits

  /** getter for digits - gets 
   * @generated */
  public String getDigits() {
    if (Blank_Type.featOkTst && ((Blank_Type)jcasType).casFeat_digits == null)
      jcasType.jcas.throwFeatMissing("digits", "org.kachako.types.centerexam.Blank");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Blank_Type)jcasType).casFeatCode_digits);}
    
  /** setter for digits - sets  
   * @generated */
  public void setDigits(String v) {
    if (Blank_Type.featOkTst && ((Blank_Type)jcasType).casFeat_digits == null)
      jcasType.jcas.throwFeatMissing("digits", "org.kachako.types.centerexam.Blank");
    jcasType.ll_cas.ll_setStringValue(addr, ((Blank_Type)jcasType).casFeatCode_digits, v);}    
   
    
  //*--------------*
  //* Feature: anscolumn_id

  /** getter for anscolumn_id - gets 
   * @generated */
  public String getAnscolumn_id() {
    if (Blank_Type.featOkTst && ((Blank_Type)jcasType).casFeat_anscolumn_id == null)
      jcasType.jcas.throwFeatMissing("anscolumn_id", "org.kachako.types.centerexam.Blank");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Blank_Type)jcasType).casFeatCode_anscolumn_id);}
    
  /** setter for anscolumn_id - sets  
   * @generated */
  public void setAnscolumn_id(String v) {
    if (Blank_Type.featOkTst && ((Blank_Type)jcasType).casFeat_anscolumn_id == null)
      jcasType.jcas.throwFeatMissing("anscolumn_id", "org.kachako.types.centerexam.Blank");
    jcasType.ll_cas.ll_setStringValue(addr, ((Blank_Type)jcasType).casFeatCode_anscolumn_id, v);}    
  }

    