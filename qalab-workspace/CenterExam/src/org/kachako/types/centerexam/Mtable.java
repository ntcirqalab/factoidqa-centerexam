

/* First created by JCasGen Thu Apr 04 11:46:08 JST 2013 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class Mtable extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Mtable.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Mtable() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Mtable(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Mtable(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Mtable(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: columnalign

  /** getter for columnalign - gets 
   * @generated */
  public String getColumnalign() {
    if (Mtable_Type.featOkTst && ((Mtable_Type)jcasType).casFeat_columnalign == null)
      jcasType.jcas.throwFeatMissing("columnalign", "org.kachako.types.centerexam.Mtable");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Mtable_Type)jcasType).casFeatCode_columnalign);}
    
  /** setter for columnalign - sets  
   * @generated */
  public void setColumnalign(String v) {
    if (Mtable_Type.featOkTst && ((Mtable_Type)jcasType).casFeat_columnalign == null)
      jcasType.jcas.throwFeatMissing("columnalign", "org.kachako.types.centerexam.Mtable");
    jcasType.ll_cas.ll_setStringValue(addr, ((Mtable_Type)jcasType).casFeatCode_columnalign, v);}    
  }

    