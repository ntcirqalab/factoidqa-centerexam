
/* First created by JCasGen Wed Apr 03 12:36:17 JST 2013 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * @generated */
public class Math_Type extends CenterExamTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Math_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Math_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Math(addr, Math_Type.this);
  			   Math_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Math(addr, Math_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Math.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.Math");
 
  /** @generated */
  final Feature casFeat_display;
  /** @generated */
  final int     casFeatCode_display;
  /** @generated */ 
  public String getDisplay(int addr) {
        if (featOkTst && casFeat_display == null)
      jcas.throwFeatMissing("display", "org.kachako.types.centerexam.Math");
    return ll_cas.ll_getStringValue(addr, casFeatCode_display);
  }
  /** @generated */    
  public void setDisplay(int addr, String v) {
        if (featOkTst && casFeat_display == null)
      jcas.throwFeatMissing("display", "org.kachako.types.centerexam.Math");
    ll_cas.ll_setStringValue(addr, casFeatCode_display, v);}
    
  
 
  /** @generated */
  final Feature casFeat_xmlns;
  /** @generated */
  final int     casFeatCode_xmlns;
  /** @generated */ 
  public String getXmlns(int addr) {
        if (featOkTst && casFeat_xmlns == null)
      jcas.throwFeatMissing("xmlns", "org.kachako.types.centerexam.Math");
    return ll_cas.ll_getStringValue(addr, casFeatCode_xmlns);
  }
  /** @generated */    
  public void setXmlns(int addr, String v) {
        if (featOkTst && casFeat_xmlns == null)
      jcas.throwFeatMissing("xmlns", "org.kachako.types.centerexam.Math");
    ll_cas.ll_setStringValue(addr, casFeatCode_xmlns, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Math_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_display = jcas.getRequiredFeatureDE(casType, "display", "uima.cas.String", featOkTst);
    casFeatCode_display  = (null == casFeat_display) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_display).getCode();

 
    casFeat_xmlns = jcas.getRequiredFeatureDE(casType, "xmlns", "uima.cas.String", featOkTst);
    casFeatCode_xmlns  = (null == casFeat_xmlns) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_xmlns).getCode();

  }
}



    