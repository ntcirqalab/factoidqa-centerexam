

/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Wed Aug 21 14:13:40 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class CenterExamTag extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(CenterExamTag.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected CenterExamTag() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public CenterExamTag(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public CenterExamTag(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public CenterExamTag(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
}

    