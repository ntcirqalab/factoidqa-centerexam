

/* First created by JCasGen Fri Jul 20 15:04:23 JST 2012 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Wed Aug 21 14:13:40 JST 2013
 * XML source: /Users/rg_takahashi/Documents/workspace/CenterExam/src/org/kachako/descriptors/centerexam/typesystem/examTypeSystem.xml
 * @generated */
public class AnsColumn extends CenterExamTag {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(AnsColumn.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected AnsColumn() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public AnsColumn(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public AnsColumn(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public AnsColumn(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: id

  /** getter for id - gets 
   * @generated */
  public String getId() {
    if (AnsColumn_Type.featOkTst && ((AnsColumn_Type)jcasType).casFeat_id == null)
      jcasType.jcas.throwFeatMissing("id", "org.kachako.types.centerexam.AnsColumn");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnsColumn_Type)jcasType).casFeatCode_id);}
    
  /** setter for id - sets  
   * @generated */
  public void setId(String v) {
    if (AnsColumn_Type.featOkTst && ((AnsColumn_Type)jcasType).casFeat_id == null)
      jcasType.jcas.throwFeatMissing("id", "org.kachako.types.centerexam.AnsColumn");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnsColumn_Type)jcasType).casFeatCode_id, v);}    
  }

    