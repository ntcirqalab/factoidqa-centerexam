
/* First created by JCasGen Thu Apr 04 11:46:08 JST 2013 */
package org.kachako.types.centerexam;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Wed Aug 21 14:13:41 JST 2013
 * @generated */
public class Mtable_Type extends CenterExamTag_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Mtable_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Mtable_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Mtable(addr, Mtable_Type.this);
  			   Mtable_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Mtable(addr, Mtable_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Mtable.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.centerexam.Mtable");
 
  /** @generated */
  final Feature casFeat_columnalign;
  /** @generated */
  final int     casFeatCode_columnalign;
  /** @generated */ 
  public String getColumnalign(int addr) {
        if (featOkTst && casFeat_columnalign == null)
      jcas.throwFeatMissing("columnalign", "org.kachako.types.centerexam.Mtable");
    return ll_cas.ll_getStringValue(addr, casFeatCode_columnalign);
  }
  /** @generated */    
  public void setColumnalign(int addr, String v) {
        if (featOkTst && casFeat_columnalign == null)
      jcas.throwFeatMissing("columnalign", "org.kachako.types.centerexam.Mtable");
    ll_cas.ll_setStringValue(addr, casFeatCode_columnalign, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Mtable_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_columnalign = jcas.getRequiredFeatureDE(casType, "columnalign", "uima.cas.String", featOkTst);
    casFeatCode_columnalign  = (null == casFeat_columnalign) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_columnalign).getCode();

  }
}



    