/*
 * An XML document type.
 * Localname: exam
 * Namespace: 
 * Java type: noNamespace.ExamDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.centerExam.impl;
/**
 * A document containing one exam(@) element.
 *
 * This is a complex type.
 */
public class ExamDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.ExamDocument
{
    private static final long serialVersionUID = 1L;
    
    public ExamDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName EXAM$0 = 
        new javax.xml.namespace.QName("", "exam");
    
    
    /**
     * Gets the "exam" element
     */
    public noNamespace.ExamDocument.Exam getExam()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.ExamDocument.Exam target = null;
            target = (noNamespace.ExamDocument.Exam)get_store().find_element_user(EXAM$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "exam" element
     */
    public void setExam(noNamespace.ExamDocument.Exam exam)
    {
        generatedSetterHelperImpl(exam, EXAM$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "exam" element
     */
    public noNamespace.ExamDocument.Exam addNewExam()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.ExamDocument.Exam target = null;
            target = (noNamespace.ExamDocument.Exam)get_store().add_element_user(EXAM$0);
            return target;
        }
    }
    /**
     * An XML exam(@).
     *
     * This is a complex type.
     */
    public static class ExamImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.ExamDocument.Exam
    {
        private static final long serialVersionUID = 1L;
        
        public ExamImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName TITLE$0 = 
            new javax.xml.namespace.QName("", "title");
        private static final javax.xml.namespace.QName INFO$2 = 
            new javax.xml.namespace.QName("", "info");
        private static final javax.xml.namespace.QName QUESTION$4 = 
            new javax.xml.namespace.QName("", "question");
        private static final javax.xml.namespace.QName CNUM$6 = 
            new javax.xml.namespace.QName("", "cNum");
        private static final javax.xml.namespace.QName LABEL$8 = 
            new javax.xml.namespace.QName("", "label");
        private static final javax.xml.namespace.QName GARBLED$10 = 
            new javax.xml.namespace.QName("", "garbled");
        private static final javax.xml.namespace.QName MISSING$12 = 
            new javax.xml.namespace.QName("", "missing");
        private static final javax.xml.namespace.QName CHOICE$14 = 
            new javax.xml.namespace.QName("", "choice");
        private static final javax.xml.namespace.QName CHOICES$16 = 
            new javax.xml.namespace.QName("", "choices");
        private static final javax.xml.namespace.QName BR$18 = 
            new javax.xml.namespace.QName("", "br");
        private static final javax.xml.namespace.QName REF$20 = 
            new javax.xml.namespace.QName("", "ref");
        private static final javax.xml.namespace.QName ANSCOLUMN$22 = 
            new javax.xml.namespace.QName("", "ansColumn");
        private static final javax.xml.namespace.QName SOURCE$24 = 
            new javax.xml.namespace.QName("", "source");
        private static final javax.xml.namespace.QName SRCTXTURL$26 = 
            new javax.xml.namespace.QName("", "srcTxtURL");
        private static final javax.xml.namespace.QName SUBJECT$28 = 
            new javax.xml.namespace.QName("", "subject");
        private static final javax.xml.namespace.QName YEAR$30 = 
            new javax.xml.namespace.QName("", "year");
        private static final javax.xml.namespace.QName SPACE$32 = 
            new javax.xml.namespace.QName("http://www.w3.org/XML/1998/namespace", "space");
        
        
        /**
         * Gets array of all "title" elements
         */
        public noNamespace.TitleDocument.Title[] getTitleArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(TITLE$0, targetList);
                noNamespace.TitleDocument.Title[] result = new noNamespace.TitleDocument.Title[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "title" element
         */
        public noNamespace.TitleDocument.Title getTitleArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.TitleDocument.Title target = null;
                target = (noNamespace.TitleDocument.Title)get_store().find_element_user(TITLE$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "title" element
         */
        public int sizeOfTitleArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(TITLE$0);
            }
        }
        
        /**
         * Sets array of all "title" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setTitleArray(noNamespace.TitleDocument.Title[] titleArray)
        {
            check_orphaned();
            arraySetterHelper(titleArray, TITLE$0);
        }
        
        /**
         * Sets ith "title" element
         */
        public void setTitleArray(int i, noNamespace.TitleDocument.Title title)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.TitleDocument.Title target = null;
                target = (noNamespace.TitleDocument.Title)get_store().find_element_user(TITLE$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(title);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "title" element
         */
        public noNamespace.TitleDocument.Title insertNewTitle(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.TitleDocument.Title target = null;
                target = (noNamespace.TitleDocument.Title)get_store().insert_element_user(TITLE$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "title" element
         */
        public noNamespace.TitleDocument.Title addNewTitle()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.TitleDocument.Title target = null;
                target = (noNamespace.TitleDocument.Title)get_store().add_element_user(TITLE$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "title" element
         */
        public void removeTitle(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(TITLE$0, i);
            }
        }
        
        /**
         * Gets array of all "info" elements
         */
        public noNamespace.InfoDocument.Info[] getInfoArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(INFO$2, targetList);
                noNamespace.InfoDocument.Info[] result = new noNamespace.InfoDocument.Info[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "info" element
         */
        public noNamespace.InfoDocument.Info getInfoArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.InfoDocument.Info target = null;
                target = (noNamespace.InfoDocument.Info)get_store().find_element_user(INFO$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "info" element
         */
        public int sizeOfInfoArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(INFO$2);
            }
        }
        
        /**
         * Sets array of all "info" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setInfoArray(noNamespace.InfoDocument.Info[] infoArray)
        {
            check_orphaned();
            arraySetterHelper(infoArray, INFO$2);
        }
        
        /**
         * Sets ith "info" element
         */
        public void setInfoArray(int i, noNamespace.InfoDocument.Info info)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.InfoDocument.Info target = null;
                target = (noNamespace.InfoDocument.Info)get_store().find_element_user(INFO$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(info);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "info" element
         */
        public noNamespace.InfoDocument.Info insertNewInfo(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.InfoDocument.Info target = null;
                target = (noNamespace.InfoDocument.Info)get_store().insert_element_user(INFO$2, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "info" element
         */
        public noNamespace.InfoDocument.Info addNewInfo()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.InfoDocument.Info target = null;
                target = (noNamespace.InfoDocument.Info)get_store().add_element_user(INFO$2);
                return target;
            }
        }
        
        /**
         * Removes the ith "info" element
         */
        public void removeInfo(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(INFO$2, i);
            }
        }
        
        /**
         * Gets array of all "question" elements
         */
        public noNamespace.QuestionDocument.Question[] getQuestionArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(QUESTION$4, targetList);
                noNamespace.QuestionDocument.Question[] result = new noNamespace.QuestionDocument.Question[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "question" element
         */
        public noNamespace.QuestionDocument.Question getQuestionArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuestionDocument.Question target = null;
                target = (noNamespace.QuestionDocument.Question)get_store().find_element_user(QUESTION$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "question" element
         */
        public int sizeOfQuestionArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(QUESTION$4);
            }
        }
        
        /**
         * Sets array of all "question" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setQuestionArray(noNamespace.QuestionDocument.Question[] questionArray)
        {
            check_orphaned();
            arraySetterHelper(questionArray, QUESTION$4);
        }
        
        /**
         * Sets ith "question" element
         */
        public void setQuestionArray(int i, noNamespace.QuestionDocument.Question question)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuestionDocument.Question target = null;
                target = (noNamespace.QuestionDocument.Question)get_store().find_element_user(QUESTION$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(question);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "question" element
         */
        public noNamespace.QuestionDocument.Question insertNewQuestion(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuestionDocument.Question target = null;
                target = (noNamespace.QuestionDocument.Question)get_store().insert_element_user(QUESTION$4, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "question" element
         */
        public noNamespace.QuestionDocument.Question addNewQuestion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuestionDocument.Question target = null;
                target = (noNamespace.QuestionDocument.Question)get_store().add_element_user(QUESTION$4);
                return target;
            }
        }
        
        /**
         * Removes the ith "question" element
         */
        public void removeQuestion(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(QUESTION$4, i);
            }
        }
        
        /**
         * Gets array of all "cNum" elements
         */
        public noNamespace.CNumDocument.CNum[] getCNumArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(CNUM$6, targetList);
                noNamespace.CNumDocument.CNum[] result = new noNamespace.CNumDocument.CNum[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "cNum" element
         */
        public noNamespace.CNumDocument.CNum getCNumArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CNumDocument.CNum target = null;
                target = (noNamespace.CNumDocument.CNum)get_store().find_element_user(CNUM$6, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "cNum" element
         */
        public int sizeOfCNumArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CNUM$6);
            }
        }
        
        /**
         * Sets array of all "cNum" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setCNumArray(noNamespace.CNumDocument.CNum[] cNumArray)
        {
            check_orphaned();
            arraySetterHelper(cNumArray, CNUM$6);
        }
        
        /**
         * Sets ith "cNum" element
         */
        public void setCNumArray(int i, noNamespace.CNumDocument.CNum cNum)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CNumDocument.CNum target = null;
                target = (noNamespace.CNumDocument.CNum)get_store().find_element_user(CNUM$6, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(cNum);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "cNum" element
         */
        public noNamespace.CNumDocument.CNum insertNewCNum(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CNumDocument.CNum target = null;
                target = (noNamespace.CNumDocument.CNum)get_store().insert_element_user(CNUM$6, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "cNum" element
         */
        public noNamespace.CNumDocument.CNum addNewCNum()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CNumDocument.CNum target = null;
                target = (noNamespace.CNumDocument.CNum)get_store().add_element_user(CNUM$6);
                return target;
            }
        }
        
        /**
         * Removes the ith "cNum" element
         */
        public void removeCNum(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CNUM$6, i);
            }
        }
        
        /**
         * Gets array of all "label" elements
         */
        public noNamespace.LabelDocument.Label[] getLabelArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(LABEL$8, targetList);
                noNamespace.LabelDocument.Label[] result = new noNamespace.LabelDocument.Label[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "label" element
         */
        public noNamespace.LabelDocument.Label getLabelArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().find_element_user(LABEL$8, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "label" element
         */
        public int sizeOfLabelArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(LABEL$8);
            }
        }
        
        /**
         * Sets array of all "label" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setLabelArray(noNamespace.LabelDocument.Label[] labelArray)
        {
            check_orphaned();
            arraySetterHelper(labelArray, LABEL$8);
        }
        
        /**
         * Sets ith "label" element
         */
        public void setLabelArray(int i, noNamespace.LabelDocument.Label label)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().find_element_user(LABEL$8, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(label);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "label" element
         */
        public noNamespace.LabelDocument.Label insertNewLabel(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().insert_element_user(LABEL$8, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "label" element
         */
        public noNamespace.LabelDocument.Label addNewLabel()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().add_element_user(LABEL$8);
                return target;
            }
        }
        
        /**
         * Removes the ith "label" element
         */
        public void removeLabel(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(LABEL$8, i);
            }
        }
        
        /**
         * Gets array of all "garbled" elements
         */
        public noNamespace.GarbledDocument.Garbled[] getGarbledArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(GARBLED$10, targetList);
                noNamespace.GarbledDocument.Garbled[] result = new noNamespace.GarbledDocument.Garbled[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled getGarbledArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().find_element_user(GARBLED$10, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "garbled" element
         */
        public int sizeOfGarbledArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(GARBLED$10);
            }
        }
        
        /**
         * Sets array of all "garbled" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setGarbledArray(noNamespace.GarbledDocument.Garbled[] garbledArray)
        {
            check_orphaned();
            arraySetterHelper(garbledArray, GARBLED$10);
        }
        
        /**
         * Sets ith "garbled" element
         */
        public void setGarbledArray(int i, noNamespace.GarbledDocument.Garbled garbled)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().find_element_user(GARBLED$10, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(garbled);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled insertNewGarbled(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().insert_element_user(GARBLED$10, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled addNewGarbled()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().add_element_user(GARBLED$10);
                return target;
            }
        }
        
        /**
         * Removes the ith "garbled" element
         */
        public void removeGarbled(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(GARBLED$10, i);
            }
        }
        
        /**
         * Gets array of all "missing" elements
         */
        public noNamespace.MissingDocument.Missing[] getMissingArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(MISSING$12, targetList);
                noNamespace.MissingDocument.Missing[] result = new noNamespace.MissingDocument.Missing[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "missing" element
         */
        public noNamespace.MissingDocument.Missing getMissingArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().find_element_user(MISSING$12, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "missing" element
         */
        public int sizeOfMissingArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(MISSING$12);
            }
        }
        
        /**
         * Sets array of all "missing" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setMissingArray(noNamespace.MissingDocument.Missing[] missingArray)
        {
            check_orphaned();
            arraySetterHelper(missingArray, MISSING$12);
        }
        
        /**
         * Sets ith "missing" element
         */
        public void setMissingArray(int i, noNamespace.MissingDocument.Missing missing)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().find_element_user(MISSING$12, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(missing);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "missing" element
         */
        public noNamespace.MissingDocument.Missing insertNewMissing(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().insert_element_user(MISSING$12, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "missing" element
         */
        public noNamespace.MissingDocument.Missing addNewMissing()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().add_element_user(MISSING$12);
                return target;
            }
        }
        
        /**
         * Removes the ith "missing" element
         */
        public void removeMissing(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(MISSING$12, i);
            }
        }
        
        /**
         * Gets array of all "choice" elements
         */
        public noNamespace.ChoiceDocument.Choice[] getChoiceArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(CHOICE$14, targetList);
                noNamespace.ChoiceDocument.Choice[] result = new noNamespace.ChoiceDocument.Choice[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "choice" element
         */
        public noNamespace.ChoiceDocument.Choice getChoiceArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoiceDocument.Choice target = null;
                target = (noNamespace.ChoiceDocument.Choice)get_store().find_element_user(CHOICE$14, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "choice" element
         */
        public int sizeOfChoiceArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CHOICE$14);
            }
        }
        
        /**
         * Sets array of all "choice" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setChoiceArray(noNamespace.ChoiceDocument.Choice[] choiceArray)
        {
            check_orphaned();
            arraySetterHelper(choiceArray, CHOICE$14);
        }
        
        /**
         * Sets ith "choice" element
         */
        public void setChoiceArray(int i, noNamespace.ChoiceDocument.Choice choice)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoiceDocument.Choice target = null;
                target = (noNamespace.ChoiceDocument.Choice)get_store().find_element_user(CHOICE$14, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(choice);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "choice" element
         */
        public noNamespace.ChoiceDocument.Choice insertNewChoice(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoiceDocument.Choice target = null;
                target = (noNamespace.ChoiceDocument.Choice)get_store().insert_element_user(CHOICE$14, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "choice" element
         */
        public noNamespace.ChoiceDocument.Choice addNewChoice()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoiceDocument.Choice target = null;
                target = (noNamespace.ChoiceDocument.Choice)get_store().add_element_user(CHOICE$14);
                return target;
            }
        }
        
        /**
         * Removes the ith "choice" element
         */
        public void removeChoice(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CHOICE$14, i);
            }
        }
        
        /**
         * Gets array of all "choices" elements
         */
        public noNamespace.ChoicesDocument.Choices[] getChoicesArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(CHOICES$16, targetList);
                noNamespace.ChoicesDocument.Choices[] result = new noNamespace.ChoicesDocument.Choices[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "choices" element
         */
        public noNamespace.ChoicesDocument.Choices getChoicesArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoicesDocument.Choices target = null;
                target = (noNamespace.ChoicesDocument.Choices)get_store().find_element_user(CHOICES$16, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "choices" element
         */
        public int sizeOfChoicesArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CHOICES$16);
            }
        }
        
        /**
         * Sets array of all "choices" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setChoicesArray(noNamespace.ChoicesDocument.Choices[] choicesArray)
        {
            check_orphaned();
            arraySetterHelper(choicesArray, CHOICES$16);
        }
        
        /**
         * Sets ith "choices" element
         */
        public void setChoicesArray(int i, noNamespace.ChoicesDocument.Choices choices)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoicesDocument.Choices target = null;
                target = (noNamespace.ChoicesDocument.Choices)get_store().find_element_user(CHOICES$16, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(choices);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "choices" element
         */
        public noNamespace.ChoicesDocument.Choices insertNewChoices(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoicesDocument.Choices target = null;
                target = (noNamespace.ChoicesDocument.Choices)get_store().insert_element_user(CHOICES$16, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "choices" element
         */
        public noNamespace.ChoicesDocument.Choices addNewChoices()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoicesDocument.Choices target = null;
                target = (noNamespace.ChoicesDocument.Choices)get_store().add_element_user(CHOICES$16);
                return target;
            }
        }
        
        /**
         * Removes the ith "choices" element
         */
        public void removeChoices(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CHOICES$16, i);
            }
        }
        
        /**
         * Gets array of all "br" elements
         */
        public noNamespace.BrDocument.Br[] getBrArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(BR$18, targetList);
                noNamespace.BrDocument.Br[] result = new noNamespace.BrDocument.Br[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "br" element
         */
        public noNamespace.BrDocument.Br getBrArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().find_element_user(BR$18, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "br" element
         */
        public int sizeOfBrArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(BR$18);
            }
        }
        
        /**
         * Sets array of all "br" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setBrArray(noNamespace.BrDocument.Br[] brArray)
        {
            check_orphaned();
            arraySetterHelper(brArray, BR$18);
        }
        
        /**
         * Sets ith "br" element
         */
        public void setBrArray(int i, noNamespace.BrDocument.Br br)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().find_element_user(BR$18, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(br);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "br" element
         */
        public noNamespace.BrDocument.Br insertNewBr(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().insert_element_user(BR$18, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "br" element
         */
        public noNamespace.BrDocument.Br addNewBr()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().add_element_user(BR$18);
                return target;
            }
        }
        
        /**
         * Removes the ith "br" element
         */
        public void removeBr(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(BR$18, i);
            }
        }
        
        /**
         * Gets array of all "ref" elements
         */
        public noNamespace.RefDocument.Ref[] getRefArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(REF$20, targetList);
                noNamespace.RefDocument.Ref[] result = new noNamespace.RefDocument.Ref[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "ref" element
         */
        public noNamespace.RefDocument.Ref getRefArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().find_element_user(REF$20, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "ref" element
         */
        public int sizeOfRefArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(REF$20);
            }
        }
        
        /**
         * Sets array of all "ref" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setRefArray(noNamespace.RefDocument.Ref[] refArray)
        {
            check_orphaned();
            arraySetterHelper(refArray, REF$20);
        }
        
        /**
         * Sets ith "ref" element
         */
        public void setRefArray(int i, noNamespace.RefDocument.Ref ref)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().find_element_user(REF$20, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(ref);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ref" element
         */
        public noNamespace.RefDocument.Ref insertNewRef(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().insert_element_user(REF$20, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ref" element
         */
        public noNamespace.RefDocument.Ref addNewRef()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().add_element_user(REF$20);
                return target;
            }
        }
        
        /**
         * Removes the ith "ref" element
         */
        public void removeRef(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(REF$20, i);
            }
        }
        
        /**
         * Gets array of all "ansColumn" elements
         */
        public noNamespace.AnsColumnDocument.AnsColumn[] getAnsColumnArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(ANSCOLUMN$22, targetList);
                noNamespace.AnsColumnDocument.AnsColumn[] result = new noNamespace.AnsColumnDocument.AnsColumn[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "ansColumn" element
         */
        public noNamespace.AnsColumnDocument.AnsColumn getAnsColumnArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.AnsColumnDocument.AnsColumn target = null;
                target = (noNamespace.AnsColumnDocument.AnsColumn)get_store().find_element_user(ANSCOLUMN$22, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "ansColumn" element
         */
        public int sizeOfAnsColumnArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ANSCOLUMN$22);
            }
        }
        
        /**
         * Sets array of all "ansColumn" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setAnsColumnArray(noNamespace.AnsColumnDocument.AnsColumn[] ansColumnArray)
        {
            check_orphaned();
            arraySetterHelper(ansColumnArray, ANSCOLUMN$22);
        }
        
        /**
         * Sets ith "ansColumn" element
         */
        public void setAnsColumnArray(int i, noNamespace.AnsColumnDocument.AnsColumn ansColumn)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.AnsColumnDocument.AnsColumn target = null;
                target = (noNamespace.AnsColumnDocument.AnsColumn)get_store().find_element_user(ANSCOLUMN$22, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(ansColumn);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ansColumn" element
         */
        public noNamespace.AnsColumnDocument.AnsColumn insertNewAnsColumn(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.AnsColumnDocument.AnsColumn target = null;
                target = (noNamespace.AnsColumnDocument.AnsColumn)get_store().insert_element_user(ANSCOLUMN$22, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ansColumn" element
         */
        public noNamespace.AnsColumnDocument.AnsColumn addNewAnsColumn()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.AnsColumnDocument.AnsColumn target = null;
                target = (noNamespace.AnsColumnDocument.AnsColumn)get_store().add_element_user(ANSCOLUMN$22);
                return target;
            }
        }
        
        /**
         * Removes the ith "ansColumn" element
         */
        public void removeAnsColumn(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ANSCOLUMN$22, i);
            }
        }
        
        /**
         * Gets the "source" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType getSource()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().find_attribute_user(SOURCE$24);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "source" attribute
         */
        public boolean isSetSource()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(SOURCE$24) != null;
            }
        }
        
        /**
         * Sets the "source" attribute
         */
        public void setSource(org.apache.xmlbeans.XmlAnySimpleType source)
        {
            generatedSetterHelperImpl(source, SOURCE$24, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "source" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType addNewSource()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().add_attribute_user(SOURCE$24);
                return target;
            }
        }
        
        /**
         * Unsets the "source" attribute
         */
        public void unsetSource()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(SOURCE$24);
            }
        }
        
        /**
         * Gets the "srcTxtURL" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType getSrcTxtURL()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().find_attribute_user(SRCTXTURL$26);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "srcTxtURL" attribute
         */
        public boolean isSetSrcTxtURL()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(SRCTXTURL$26) != null;
            }
        }
        
        /**
         * Sets the "srcTxtURL" attribute
         */
        public void setSrcTxtURL(org.apache.xmlbeans.XmlAnySimpleType srcTxtURL)
        {
            generatedSetterHelperImpl(srcTxtURL, SRCTXTURL$26, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "srcTxtURL" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType addNewSrcTxtURL()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().add_attribute_user(SRCTXTURL$26);
                return target;
            }
        }
        
        /**
         * Unsets the "srcTxtURL" attribute
         */
        public void unsetSrcTxtURL()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(SRCTXTURL$26);
            }
        }
        
        /**
         * Gets the "subject" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType getSubject()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().find_attribute_user(SUBJECT$28);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "subject" attribute
         */
        public void setSubject(org.apache.xmlbeans.XmlAnySimpleType subject)
        {
            generatedSetterHelperImpl(subject, SUBJECT$28, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "subject" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType addNewSubject()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().add_attribute_user(SUBJECT$28);
                return target;
            }
        }
        
        /**
         * Gets the "year" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType getYear()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().find_attribute_user(YEAR$30);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "year" attribute
         */
        public boolean isSetYear()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(YEAR$30) != null;
            }
        }
        
        /**
         * Sets the "year" attribute
         */
        public void setYear(org.apache.xmlbeans.XmlAnySimpleType year)
        {
            generatedSetterHelperImpl(year, YEAR$30, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "year" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType addNewYear()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().add_attribute_user(YEAR$30);
                return target;
            }
        }
        
        /**
         * Unsets the "year" attribute
         */
        public void unsetYear()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(YEAR$30);
            }
        }
        
        /**
         * Gets the "space" attribute
         */
        public org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space.Enum getSpace()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(SPACE$32);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_default_attribute_value(SPACE$32);
                }
                if (target == null)
                {
                    return null;
                }
                return (org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space.Enum)target.getEnumValue();
            }
        }
        
        /**
         * Gets (as xml) the "space" attribute
         */
        public org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space xgetSpace()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space target = null;
                target = (org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space)get_store().find_attribute_user(SPACE$32);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space)get_default_attribute_value(SPACE$32);
                }
                return target;
            }
        }
        
        /**
         * True if has "space" attribute
         */
        public boolean isSetSpace()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(SPACE$32) != null;
            }
        }
        
        /**
         * Sets the "space" attribute
         */
        public void setSpace(org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space.Enum space)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(SPACE$32);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(SPACE$32);
                }
                target.setEnumValue(space);
            }
        }
        
        /**
         * Sets (as xml) the "space" attribute
         */
        public void xsetSpace(org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space space)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space target = null;
                target = (org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space)get_store().find_attribute_user(SPACE$32);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space)get_store().add_attribute_user(SPACE$32);
                }
                target.set(space);
            }
        }
        
        /**
         * Unsets the "space" attribute
         */
        public void unsetSpace()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(SPACE$32);
            }
        }
    }
}
