/*
 * An XML document type.
 * Localname: answer_column
 * Namespace: 
 * Java type: AnswerColumnDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.answerTable.impl;

import org.kachako.components.centerexam.xmlschema.answerTable.AnswerColumnDocument;

/**
 * A document containing one answer_column(@) element.
 *
 * This is a complex type.
 */
public class AnswerColumnDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AnswerColumnDocument
{
    private static final long serialVersionUID = 1L;
    
    public AnswerColumnDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ANSWERCOLUMN$0 = 
        new javax.xml.namespace.QName("", "answer_column");
    
    
    /**
     * Gets the "answer_column" element
     */
    public java.lang.String getAnswerColumn()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ANSWERCOLUMN$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "answer_column" element
     */
    public org.apache.xmlbeans.XmlString xgetAnswerColumn()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ANSWERCOLUMN$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "answer_column" element
     */
    public void setAnswerColumn(java.lang.String answerColumn)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ANSWERCOLUMN$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ANSWERCOLUMN$0);
            }
            target.setStringValue(answerColumn);
        }
    }
    
    /**
     * Sets (as xml) the "answer_column" element
     */
    public void xsetAnswerColumn(org.apache.xmlbeans.XmlString answerColumn)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ANSWERCOLUMN$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(ANSWERCOLUMN$0);
            }
            target.set(answerColumn);
        }
    }
}
