/*
 * An XML document type.
 * Localname: anscolumn_ID
 * Namespace: 
 * Java type: AnscolumnIDDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.answerTable.impl;

import org.kachako.components.centerexam.xmlschema.answerTable.AnscolumnIDDocument;

/**
 * A document containing one anscolumn_ID(@) element.
 *
 * This is a complex type.
 */
public class AnscolumnIDDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AnscolumnIDDocument
{
    private static final long serialVersionUID = 1L;
    
    public AnscolumnIDDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ANSCOLUMNID$0 = 
        new javax.xml.namespace.QName("", "anscolumn_ID");
    
    
    /**
     * Gets the "anscolumn_ID" element
     */
    public java.lang.String getAnscolumnID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ANSCOLUMNID$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "anscolumn_ID" element
     */
    public org.apache.xmlbeans.XmlString xgetAnscolumnID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ANSCOLUMNID$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "anscolumn_ID" element
     */
    public void setAnscolumnID(java.lang.String anscolumnID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ANSCOLUMNID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ANSCOLUMNID$0);
            }
            target.setStringValue(anscolumnID);
        }
    }
    
    /**
     * Sets (as xml) the "anscolumn_ID" element
     */
    public void xsetAnscolumnID(org.apache.xmlbeans.XmlString anscolumnID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ANSCOLUMNID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(ANSCOLUMNID$0);
            }
            target.set(anscolumnID);
        }
    }
}
