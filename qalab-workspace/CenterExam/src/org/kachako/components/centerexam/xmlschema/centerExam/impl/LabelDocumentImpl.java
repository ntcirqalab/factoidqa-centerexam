/*
 * An XML document type.
 * Localname: label
 * Namespace: 
 * Java type: noNamespace.LabelDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.centerExam.impl;
/**
 * A document containing one label(@) element.
 *
 * This is a complex type.
 */
public class LabelDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.LabelDocument
{
    private static final long serialVersionUID = 1L;
    
    public LabelDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName LABEL$0 = 
        new javax.xml.namespace.QName("", "label");
    
    
    /**
     * Gets the "label" element
     */
    public noNamespace.LabelDocument.Label getLabel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.LabelDocument.Label target = null;
            target = (noNamespace.LabelDocument.Label)get_store().find_element_user(LABEL$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "label" element
     */
    public void setLabel(noNamespace.LabelDocument.Label label)
    {
        generatedSetterHelperImpl(label, LABEL$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "label" element
     */
    public noNamespace.LabelDocument.Label addNewLabel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.LabelDocument.Label target = null;
            target = (noNamespace.LabelDocument.Label)get_store().add_element_user(LABEL$0);
            return target;
        }
    }
    /**
     * An XML label(@).
     *
     * This is a complex type.
     */
    public static class LabelImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.LabelDocument.Label
    {
        private static final long serialVersionUID = 1L;
        
        public LabelImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName BR$0 = 
            new javax.xml.namespace.QName("", "br");
        private static final javax.xml.namespace.QName GARBLED$2 = 
            new javax.xml.namespace.QName("", "garbled");
        private static final javax.xml.namespace.QName MISSING$4 = 
            new javax.xml.namespace.QName("", "missing");
        
        
        /**
         * Gets array of all "br" elements
         */
        public noNamespace.BrDocument.Br[] getBrArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(BR$0, targetList);
                noNamespace.BrDocument.Br[] result = new noNamespace.BrDocument.Br[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "br" element
         */
        public noNamespace.BrDocument.Br getBrArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().find_element_user(BR$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "br" element
         */
        public int sizeOfBrArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(BR$0);
            }
        }
        
        /**
         * Sets array of all "br" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setBrArray(noNamespace.BrDocument.Br[] brArray)
        {
            check_orphaned();
            arraySetterHelper(brArray, BR$0);
        }
        
        /**
         * Sets ith "br" element
         */
        public void setBrArray(int i, noNamespace.BrDocument.Br br)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().find_element_user(BR$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(br);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "br" element
         */
        public noNamespace.BrDocument.Br insertNewBr(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().insert_element_user(BR$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "br" element
         */
        public noNamespace.BrDocument.Br addNewBr()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().add_element_user(BR$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "br" element
         */
        public void removeBr(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(BR$0, i);
            }
        }
        
        /**
         * Gets array of all "garbled" elements
         */
        public noNamespace.GarbledDocument.Garbled[] getGarbledArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(GARBLED$2, targetList);
                noNamespace.GarbledDocument.Garbled[] result = new noNamespace.GarbledDocument.Garbled[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled getGarbledArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().find_element_user(GARBLED$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "garbled" element
         */
        public int sizeOfGarbledArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(GARBLED$2);
            }
        }
        
        /**
         * Sets array of all "garbled" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setGarbledArray(noNamespace.GarbledDocument.Garbled[] garbledArray)
        {
            check_orphaned();
            arraySetterHelper(garbledArray, GARBLED$2);
        }
        
        /**
         * Sets ith "garbled" element
         */
        public void setGarbledArray(int i, noNamespace.GarbledDocument.Garbled garbled)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().find_element_user(GARBLED$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(garbled);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled insertNewGarbled(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().insert_element_user(GARBLED$2, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled addNewGarbled()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().add_element_user(GARBLED$2);
                return target;
            }
        }
        
        /**
         * Removes the ith "garbled" element
         */
        public void removeGarbled(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(GARBLED$2, i);
            }
        }
        
        /**
         * Gets array of all "missing" elements
         */
        public noNamespace.MissingDocument.Missing[] getMissingArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(MISSING$4, targetList);
                noNamespace.MissingDocument.Missing[] result = new noNamespace.MissingDocument.Missing[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "missing" element
         */
        public noNamespace.MissingDocument.Missing getMissingArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().find_element_user(MISSING$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "missing" element
         */
        public int sizeOfMissingArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(MISSING$4);
            }
        }
        
        /**
         * Sets array of all "missing" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setMissingArray(noNamespace.MissingDocument.Missing[] missingArray)
        {
            check_orphaned();
            arraySetterHelper(missingArray, MISSING$4);
        }
        
        /**
         * Sets ith "missing" element
         */
        public void setMissingArray(int i, noNamespace.MissingDocument.Missing missing)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().find_element_user(MISSING$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(missing);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "missing" element
         */
        public noNamespace.MissingDocument.Missing insertNewMissing(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().insert_element_user(MISSING$4, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "missing" element
         */
        public noNamespace.MissingDocument.Missing addNewMissing()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().add_element_user(MISSING$4);
                return target;
            }
        }
        
        /**
         * Removes the ith "missing" element
         */
        public void removeMissing(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(MISSING$4, i);
            }
        }
    }
}
