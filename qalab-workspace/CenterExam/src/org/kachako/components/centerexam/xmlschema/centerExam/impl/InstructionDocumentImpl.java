/*
 * An XML document type.
 * Localname: instruction
 * Namespace: 
 * Java type: noNamespace.InstructionDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.centerExam.impl;
/**
 * A document containing one instruction(@) element.
 *
 * This is a complex type.
 */
public class InstructionDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.InstructionDocument
{
    private static final long serialVersionUID = 1L;
    
    public InstructionDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INSTRUCTION$0 = 
        new javax.xml.namespace.QName("", "instruction");
    
    
    /**
     * Gets the "instruction" element
     */
    public noNamespace.InstructionDocument.Instruction getInstruction()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.InstructionDocument.Instruction target = null;
            target = (noNamespace.InstructionDocument.Instruction)get_store().find_element_user(INSTRUCTION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "instruction" element
     */
    public void setInstruction(noNamespace.InstructionDocument.Instruction instruction)
    {
        generatedSetterHelperImpl(instruction, INSTRUCTION$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "instruction" element
     */
    public noNamespace.InstructionDocument.Instruction addNewInstruction()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.InstructionDocument.Instruction target = null;
            target = (noNamespace.InstructionDocument.Instruction)get_store().add_element_user(INSTRUCTION$0);
            return target;
        }
    }
    /**
     * An XML instruction(@).
     *
     * This is a complex type.
     */
    public static class InstructionImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.InstructionDocument.Instruction
    {
        private static final long serialVersionUID = 1L;
        
        public InstructionImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName GARBLED$0 = 
            new javax.xml.namespace.QName("", "garbled");
        private static final javax.xml.namespace.QName REF$2 = 
            new javax.xml.namespace.QName("", "ref");
        private static final javax.xml.namespace.QName MISSING$4 = 
            new javax.xml.namespace.QName("", "missing");
        private static final javax.xml.namespace.QName LABEL$6 = 
            new javax.xml.namespace.QName("", "label");
        private static final javax.xml.namespace.QName ANSCOLUMN$8 = 
            new javax.xml.namespace.QName("", "ansColumn");
        private static final javax.xml.namespace.QName LTEXT$10 = 
            new javax.xml.namespace.QName("", "lText");
        private static final javax.xml.namespace.QName UTEXT$12 = 
            new javax.xml.namespace.QName("", "uText");
        private static final javax.xml.namespace.QName BR$14 = 
            new javax.xml.namespace.QName("", "br");
        private static final javax.xml.namespace.QName BLANK$16 = 
            new javax.xml.namespace.QName("", "blank");
        private static final javax.xml.namespace.QName FORMULA$18 = 
            new javax.xml.namespace.QName("", "formula");
        private static final javax.xml.namespace.QName QUOTE$20 = 
            new javax.xml.namespace.QName("", "quote");
        private static final javax.xml.namespace.QName SOURCE$22 = 
            new javax.xml.namespace.QName("", "source");
        private static final javax.xml.namespace.QName IMG$24 = 
            new javax.xml.namespace.QName("", "img");
        
        
        /**
         * Gets array of all "garbled" elements
         */
        public noNamespace.GarbledDocument.Garbled[] getGarbledArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(GARBLED$0, targetList);
                noNamespace.GarbledDocument.Garbled[] result = new noNamespace.GarbledDocument.Garbled[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled getGarbledArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().find_element_user(GARBLED$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "garbled" element
         */
        public int sizeOfGarbledArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(GARBLED$0);
            }
        }
        
        /**
         * Sets array of all "garbled" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setGarbledArray(noNamespace.GarbledDocument.Garbled[] garbledArray)
        {
            check_orphaned();
            arraySetterHelper(garbledArray, GARBLED$0);
        }
        
        /**
         * Sets ith "garbled" element
         */
        public void setGarbledArray(int i, noNamespace.GarbledDocument.Garbled garbled)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().find_element_user(GARBLED$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(garbled);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled insertNewGarbled(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().insert_element_user(GARBLED$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled addNewGarbled()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().add_element_user(GARBLED$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "garbled" element
         */
        public void removeGarbled(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(GARBLED$0, i);
            }
        }
        
        /**
         * Gets array of all "ref" elements
         */
        public noNamespace.RefDocument.Ref[] getRefArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(REF$2, targetList);
                noNamespace.RefDocument.Ref[] result = new noNamespace.RefDocument.Ref[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "ref" element
         */
        public noNamespace.RefDocument.Ref getRefArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().find_element_user(REF$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "ref" element
         */
        public int sizeOfRefArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(REF$2);
            }
        }
        
        /**
         * Sets array of all "ref" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setRefArray(noNamespace.RefDocument.Ref[] refArray)
        {
            check_orphaned();
            arraySetterHelper(refArray, REF$2);
        }
        
        /**
         * Sets ith "ref" element
         */
        public void setRefArray(int i, noNamespace.RefDocument.Ref ref)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().find_element_user(REF$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(ref);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ref" element
         */
        public noNamespace.RefDocument.Ref insertNewRef(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().insert_element_user(REF$2, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ref" element
         */
        public noNamespace.RefDocument.Ref addNewRef()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().add_element_user(REF$2);
                return target;
            }
        }
        
        /**
         * Removes the ith "ref" element
         */
        public void removeRef(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(REF$2, i);
            }
        }
        
        /**
         * Gets array of all "missing" elements
         */
        public noNamespace.MissingDocument.Missing[] getMissingArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(MISSING$4, targetList);
                noNamespace.MissingDocument.Missing[] result = new noNamespace.MissingDocument.Missing[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "missing" element
         */
        public noNamespace.MissingDocument.Missing getMissingArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().find_element_user(MISSING$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "missing" element
         */
        public int sizeOfMissingArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(MISSING$4);
            }
        }
        
        /**
         * Sets array of all "missing" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setMissingArray(noNamespace.MissingDocument.Missing[] missingArray)
        {
            check_orphaned();
            arraySetterHelper(missingArray, MISSING$4);
        }
        
        /**
         * Sets ith "missing" element
         */
        public void setMissingArray(int i, noNamespace.MissingDocument.Missing missing)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().find_element_user(MISSING$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(missing);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "missing" element
         */
        public noNamespace.MissingDocument.Missing insertNewMissing(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().insert_element_user(MISSING$4, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "missing" element
         */
        public noNamespace.MissingDocument.Missing addNewMissing()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().add_element_user(MISSING$4);
                return target;
            }
        }
        
        /**
         * Removes the ith "missing" element
         */
        public void removeMissing(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(MISSING$4, i);
            }
        }
        
        /**
         * Gets array of all "label" elements
         */
        public noNamespace.LabelDocument.Label[] getLabelArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(LABEL$6, targetList);
                noNamespace.LabelDocument.Label[] result = new noNamespace.LabelDocument.Label[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "label" element
         */
        public noNamespace.LabelDocument.Label getLabelArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().find_element_user(LABEL$6, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "label" element
         */
        public int sizeOfLabelArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(LABEL$6);
            }
        }
        
        /**
         * Sets array of all "label" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setLabelArray(noNamespace.LabelDocument.Label[] labelArray)
        {
            check_orphaned();
            arraySetterHelper(labelArray, LABEL$6);
        }
        
        /**
         * Sets ith "label" element
         */
        public void setLabelArray(int i, noNamespace.LabelDocument.Label label)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().find_element_user(LABEL$6, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(label);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "label" element
         */
        public noNamespace.LabelDocument.Label insertNewLabel(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().insert_element_user(LABEL$6, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "label" element
         */
        public noNamespace.LabelDocument.Label addNewLabel()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().add_element_user(LABEL$6);
                return target;
            }
        }
        
        /**
         * Removes the ith "label" element
         */
        public void removeLabel(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(LABEL$6, i);
            }
        }
        
        /**
         * Gets array of all "ansColumn" elements
         */
        public noNamespace.AnsColumnDocument.AnsColumn[] getAnsColumnArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(ANSCOLUMN$8, targetList);
                noNamespace.AnsColumnDocument.AnsColumn[] result = new noNamespace.AnsColumnDocument.AnsColumn[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "ansColumn" element
         */
        public noNamespace.AnsColumnDocument.AnsColumn getAnsColumnArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.AnsColumnDocument.AnsColumn target = null;
                target = (noNamespace.AnsColumnDocument.AnsColumn)get_store().find_element_user(ANSCOLUMN$8, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "ansColumn" element
         */
        public int sizeOfAnsColumnArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ANSCOLUMN$8);
            }
        }
        
        /**
         * Sets array of all "ansColumn" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setAnsColumnArray(noNamespace.AnsColumnDocument.AnsColumn[] ansColumnArray)
        {
            check_orphaned();
            arraySetterHelper(ansColumnArray, ANSCOLUMN$8);
        }
        
        /**
         * Sets ith "ansColumn" element
         */
        public void setAnsColumnArray(int i, noNamespace.AnsColumnDocument.AnsColumn ansColumn)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.AnsColumnDocument.AnsColumn target = null;
                target = (noNamespace.AnsColumnDocument.AnsColumn)get_store().find_element_user(ANSCOLUMN$8, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(ansColumn);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ansColumn" element
         */
        public noNamespace.AnsColumnDocument.AnsColumn insertNewAnsColumn(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.AnsColumnDocument.AnsColumn target = null;
                target = (noNamespace.AnsColumnDocument.AnsColumn)get_store().insert_element_user(ANSCOLUMN$8, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ansColumn" element
         */
        public noNamespace.AnsColumnDocument.AnsColumn addNewAnsColumn()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.AnsColumnDocument.AnsColumn target = null;
                target = (noNamespace.AnsColumnDocument.AnsColumn)get_store().add_element_user(ANSCOLUMN$8);
                return target;
            }
        }
        
        /**
         * Removes the ith "ansColumn" element
         */
        public void removeAnsColumn(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ANSCOLUMN$8, i);
            }
        }
        
        /**
         * Gets array of all "lText" elements
         */
        public noNamespace.LTextDocument.LText[] getLTextArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(LTEXT$10, targetList);
                noNamespace.LTextDocument.LText[] result = new noNamespace.LTextDocument.LText[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "lText" element
         */
        public noNamespace.LTextDocument.LText getLTextArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LTextDocument.LText target = null;
                target = (noNamespace.LTextDocument.LText)get_store().find_element_user(LTEXT$10, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "lText" element
         */
        public int sizeOfLTextArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(LTEXT$10);
            }
        }
        
        /**
         * Sets array of all "lText" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setLTextArray(noNamespace.LTextDocument.LText[] lTextArray)
        {
            check_orphaned();
            arraySetterHelper(lTextArray, LTEXT$10);
        }
        
        /**
         * Sets ith "lText" element
         */
        public void setLTextArray(int i, noNamespace.LTextDocument.LText lText)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LTextDocument.LText target = null;
                target = (noNamespace.LTextDocument.LText)get_store().find_element_user(LTEXT$10, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(lText);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "lText" element
         */
        public noNamespace.LTextDocument.LText insertNewLText(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LTextDocument.LText target = null;
                target = (noNamespace.LTextDocument.LText)get_store().insert_element_user(LTEXT$10, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "lText" element
         */
        public noNamespace.LTextDocument.LText addNewLText()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LTextDocument.LText target = null;
                target = (noNamespace.LTextDocument.LText)get_store().add_element_user(LTEXT$10);
                return target;
            }
        }
        
        /**
         * Removes the ith "lText" element
         */
        public void removeLText(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(LTEXT$10, i);
            }
        }
        
        /**
         * Gets array of all "uText" elements
         */
        public noNamespace.UTextDocument.UText[] getUTextArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(UTEXT$12, targetList);
                noNamespace.UTextDocument.UText[] result = new noNamespace.UTextDocument.UText[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "uText" element
         */
        public noNamespace.UTextDocument.UText getUTextArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.UTextDocument.UText target = null;
                target = (noNamespace.UTextDocument.UText)get_store().find_element_user(UTEXT$12, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "uText" element
         */
        public int sizeOfUTextArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(UTEXT$12);
            }
        }
        
        /**
         * Sets array of all "uText" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setUTextArray(noNamespace.UTextDocument.UText[] uTextArray)
        {
            check_orphaned();
            arraySetterHelper(uTextArray, UTEXT$12);
        }
        
        /**
         * Sets ith "uText" element
         */
        public void setUTextArray(int i, noNamespace.UTextDocument.UText uText)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.UTextDocument.UText target = null;
                target = (noNamespace.UTextDocument.UText)get_store().find_element_user(UTEXT$12, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(uText);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "uText" element
         */
        public noNamespace.UTextDocument.UText insertNewUText(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.UTextDocument.UText target = null;
                target = (noNamespace.UTextDocument.UText)get_store().insert_element_user(UTEXT$12, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "uText" element
         */
        public noNamespace.UTextDocument.UText addNewUText()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.UTextDocument.UText target = null;
                target = (noNamespace.UTextDocument.UText)get_store().add_element_user(UTEXT$12);
                return target;
            }
        }
        
        /**
         * Removes the ith "uText" element
         */
        public void removeUText(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(UTEXT$12, i);
            }
        }
        
        /**
         * Gets array of all "br" elements
         */
        public noNamespace.BrDocument.Br[] getBrArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(BR$14, targetList);
                noNamespace.BrDocument.Br[] result = new noNamespace.BrDocument.Br[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "br" element
         */
        public noNamespace.BrDocument.Br getBrArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().find_element_user(BR$14, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "br" element
         */
        public int sizeOfBrArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(BR$14);
            }
        }
        
        /**
         * Sets array of all "br" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setBrArray(noNamespace.BrDocument.Br[] brArray)
        {
            check_orphaned();
            arraySetterHelper(brArray, BR$14);
        }
        
        /**
         * Sets ith "br" element
         */
        public void setBrArray(int i, noNamespace.BrDocument.Br br)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().find_element_user(BR$14, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(br);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "br" element
         */
        public noNamespace.BrDocument.Br insertNewBr(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().insert_element_user(BR$14, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "br" element
         */
        public noNamespace.BrDocument.Br addNewBr()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().add_element_user(BR$14);
                return target;
            }
        }
        
        /**
         * Removes the ith "br" element
         */
        public void removeBr(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(BR$14, i);
            }
        }
        
        /**
         * Gets array of all "blank" elements
         */
        public noNamespace.BlankDocument.Blank[] getBlankArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(BLANK$16, targetList);
                noNamespace.BlankDocument.Blank[] result = new noNamespace.BlankDocument.Blank[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "blank" element
         */
        public noNamespace.BlankDocument.Blank getBlankArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BlankDocument.Blank target = null;
                target = (noNamespace.BlankDocument.Blank)get_store().find_element_user(BLANK$16, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "blank" element
         */
        public int sizeOfBlankArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(BLANK$16);
            }
        }
        
        /**
         * Sets array of all "blank" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setBlankArray(noNamespace.BlankDocument.Blank[] blankArray)
        {
            check_orphaned();
            arraySetterHelper(blankArray, BLANK$16);
        }
        
        /**
         * Sets ith "blank" element
         */
        public void setBlankArray(int i, noNamespace.BlankDocument.Blank blank)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BlankDocument.Blank target = null;
                target = (noNamespace.BlankDocument.Blank)get_store().find_element_user(BLANK$16, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(blank);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "blank" element
         */
        public noNamespace.BlankDocument.Blank insertNewBlank(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BlankDocument.Blank target = null;
                target = (noNamespace.BlankDocument.Blank)get_store().insert_element_user(BLANK$16, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "blank" element
         */
        public noNamespace.BlankDocument.Blank addNewBlank()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BlankDocument.Blank target = null;
                target = (noNamespace.BlankDocument.Blank)get_store().add_element_user(BLANK$16);
                return target;
            }
        }
        
        /**
         * Removes the ith "blank" element
         */
        public void removeBlank(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(BLANK$16, i);
            }
        }
        
        /**
         * Gets array of all "formula" elements
         */
        public noNamespace.FormulaDocument.Formula[] getFormulaArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(FORMULA$18, targetList);
                noNamespace.FormulaDocument.Formula[] result = new noNamespace.FormulaDocument.Formula[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "formula" element
         */
        public noNamespace.FormulaDocument.Formula getFormulaArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.FormulaDocument.Formula target = null;
                target = (noNamespace.FormulaDocument.Formula)get_store().find_element_user(FORMULA$18, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "formula" element
         */
        public int sizeOfFormulaArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(FORMULA$18);
            }
        }
        
        /**
         * Sets array of all "formula" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setFormulaArray(noNamespace.FormulaDocument.Formula[] formulaArray)
        {
            check_orphaned();
            arraySetterHelper(formulaArray, FORMULA$18);
        }
        
        /**
         * Sets ith "formula" element
         */
        public void setFormulaArray(int i, noNamespace.FormulaDocument.Formula formula)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.FormulaDocument.Formula target = null;
                target = (noNamespace.FormulaDocument.Formula)get_store().find_element_user(FORMULA$18, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(formula);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "formula" element
         */
        public noNamespace.FormulaDocument.Formula insertNewFormula(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.FormulaDocument.Formula target = null;
                target = (noNamespace.FormulaDocument.Formula)get_store().insert_element_user(FORMULA$18, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "formula" element
         */
        public noNamespace.FormulaDocument.Formula addNewFormula()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.FormulaDocument.Formula target = null;
                target = (noNamespace.FormulaDocument.Formula)get_store().add_element_user(FORMULA$18);
                return target;
            }
        }
        
        /**
         * Removes the ith "formula" element
         */
        public void removeFormula(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(FORMULA$18, i);
            }
        }
        
        /**
         * Gets array of all "quote" elements
         */
        public noNamespace.QuoteDocument.Quote[] getQuoteArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(QUOTE$20, targetList);
                noNamespace.QuoteDocument.Quote[] result = new noNamespace.QuoteDocument.Quote[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "quote" element
         */
        public noNamespace.QuoteDocument.Quote getQuoteArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuoteDocument.Quote target = null;
                target = (noNamespace.QuoteDocument.Quote)get_store().find_element_user(QUOTE$20, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "quote" element
         */
        public int sizeOfQuoteArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(QUOTE$20);
            }
        }
        
        /**
         * Sets array of all "quote" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setQuoteArray(noNamespace.QuoteDocument.Quote[] quoteArray)
        {
            check_orphaned();
            arraySetterHelper(quoteArray, QUOTE$20);
        }
        
        /**
         * Sets ith "quote" element
         */
        public void setQuoteArray(int i, noNamespace.QuoteDocument.Quote quote)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuoteDocument.Quote target = null;
                target = (noNamespace.QuoteDocument.Quote)get_store().find_element_user(QUOTE$20, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(quote);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "quote" element
         */
        public noNamespace.QuoteDocument.Quote insertNewQuote(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuoteDocument.Quote target = null;
                target = (noNamespace.QuoteDocument.Quote)get_store().insert_element_user(QUOTE$20, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "quote" element
         */
        public noNamespace.QuoteDocument.Quote addNewQuote()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuoteDocument.Quote target = null;
                target = (noNamespace.QuoteDocument.Quote)get_store().add_element_user(QUOTE$20);
                return target;
            }
        }
        
        /**
         * Removes the ith "quote" element
         */
        public void removeQuote(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(QUOTE$20, i);
            }
        }
        
        /**
         * Gets array of all "source" elements
         */
        public noNamespace.SourceDocument.Source[] getSourceArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(SOURCE$22, targetList);
                noNamespace.SourceDocument.Source[] result = new noNamespace.SourceDocument.Source[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "source" element
         */
        public noNamespace.SourceDocument.Source getSourceArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.SourceDocument.Source target = null;
                target = (noNamespace.SourceDocument.Source)get_store().find_element_user(SOURCE$22, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "source" element
         */
        public int sizeOfSourceArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(SOURCE$22);
            }
        }
        
        /**
         * Sets array of all "source" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setSourceArray(noNamespace.SourceDocument.Source[] sourceArray)
        {
            check_orphaned();
            arraySetterHelper(sourceArray, SOURCE$22);
        }
        
        /**
         * Sets ith "source" element
         */
        public void setSourceArray(int i, noNamespace.SourceDocument.Source source)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.SourceDocument.Source target = null;
                target = (noNamespace.SourceDocument.Source)get_store().find_element_user(SOURCE$22, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(source);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "source" element
         */
        public noNamespace.SourceDocument.Source insertNewSource(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.SourceDocument.Source target = null;
                target = (noNamespace.SourceDocument.Source)get_store().insert_element_user(SOURCE$22, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "source" element
         */
        public noNamespace.SourceDocument.Source addNewSource()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.SourceDocument.Source target = null;
                target = (noNamespace.SourceDocument.Source)get_store().add_element_user(SOURCE$22);
                return target;
            }
        }
        
        /**
         * Removes the ith "source" element
         */
        public void removeSource(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(SOURCE$22, i);
            }
        }
        
        /**
         * Gets array of all "img" elements
         */
        public noNamespace.ImgDocument.Img[] getImgArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(IMG$24, targetList);
                noNamespace.ImgDocument.Img[] result = new noNamespace.ImgDocument.Img[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "img" element
         */
        public noNamespace.ImgDocument.Img getImgArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ImgDocument.Img target = null;
                target = (noNamespace.ImgDocument.Img)get_store().find_element_user(IMG$24, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "img" element
         */
        public int sizeOfImgArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IMG$24);
            }
        }
        
        /**
         * Sets array of all "img" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setImgArray(noNamespace.ImgDocument.Img[] imgArray)
        {
            check_orphaned();
            arraySetterHelper(imgArray, IMG$24);
        }
        
        /**
         * Sets ith "img" element
         */
        public void setImgArray(int i, noNamespace.ImgDocument.Img img)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ImgDocument.Img target = null;
                target = (noNamespace.ImgDocument.Img)get_store().find_element_user(IMG$24, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(img);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "img" element
         */
        public noNamespace.ImgDocument.Img insertNewImg(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ImgDocument.Img target = null;
                target = (noNamespace.ImgDocument.Img)get_store().insert_element_user(IMG$24, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "img" element
         */
        public noNamespace.ImgDocument.Img addNewImg()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ImgDocument.Img target = null;
                target = (noNamespace.ImgDocument.Img)get_store().add_element_user(IMG$24);
                return target;
            }
        }
        
        /**
         * Removes the ith "img" element
         */
        public void removeImg(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IMG$24, i);
            }
        }
    }
}
