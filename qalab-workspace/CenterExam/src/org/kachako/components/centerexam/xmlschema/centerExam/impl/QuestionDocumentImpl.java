/*
 * An XML document type.
 * Localname: question
 * Namespace: 
 * Java type: noNamespace.QuestionDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.centerExam.impl;
/**
 * A document containing one question(@) element.
 *
 * This is a complex type.
 */
public class QuestionDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.QuestionDocument
{
    private static final long serialVersionUID = 1L;
    
    public QuestionDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName QUESTION$0 = 
        new javax.xml.namespace.QName("", "question");
    
    
    /**
     * Gets the "question" element
     */
    public noNamespace.QuestionDocument.Question getQuestion()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.QuestionDocument.Question target = null;
            target = (noNamespace.QuestionDocument.Question)get_store().find_element_user(QUESTION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "question" element
     */
    public void setQuestion(noNamespace.QuestionDocument.Question question)
    {
        generatedSetterHelperImpl(question, QUESTION$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "question" element
     */
    public noNamespace.QuestionDocument.Question addNewQuestion()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.QuestionDocument.Question target = null;
            target = (noNamespace.QuestionDocument.Question)get_store().add_element_user(QUESTION$0);
            return target;
        }
    }
    /**
     * An XML question(@).
     *
     * This is a complex type.
     */
    public static class QuestionImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.QuestionDocument.Question
    {
        private static final long serialVersionUID = 1L;
        
        public QuestionImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName GARBLED$0 = 
            new javax.xml.namespace.QName("", "garbled");
        private static final javax.xml.namespace.QName LABEL$2 = 
            new javax.xml.namespace.QName("", "label");
        private static final javax.xml.namespace.QName INSTRUCTION$4 = 
            new javax.xml.namespace.QName("", "instruction");
        private static final javax.xml.namespace.QName DATA$6 = 
            new javax.xml.namespace.QName("", "data");
        private static final javax.xml.namespace.QName QUESTION$8 = 
            new javax.xml.namespace.QName("", "question");
        private static final javax.xml.namespace.QName ANSCOLUMN$10 = 
            new javax.xml.namespace.QName("", "ansColumn");
        private static final javax.xml.namespace.QName CHOICE$12 = 
            new javax.xml.namespace.QName("", "choice");
        private static final javax.xml.namespace.QName IMG$14 = 
            new javax.xml.namespace.QName("", "img");
        private static final javax.xml.namespace.QName TBL$16 = 
            new javax.xml.namespace.QName("", "tbl");
        private static final javax.xml.namespace.QName CHOICES$18 = 
            new javax.xml.namespace.QName("", "choices");
        private static final javax.xml.namespace.QName CNUM$20 = 
            new javax.xml.namespace.QName("", "cNum");
        private static final javax.xml.namespace.QName MISSING$22 = 
            new javax.xml.namespace.QName("", "missing");
        private static final javax.xml.namespace.QName REF$24 = 
            new javax.xml.namespace.QName("", "ref");
        private static final javax.xml.namespace.QName BR$26 = 
            new javax.xml.namespace.QName("", "br");
        private static final javax.xml.namespace.QName FORMULA$28 = 
            new javax.xml.namespace.QName("", "formula");
        private static final javax.xml.namespace.QName QUOTE$30 = 
            new javax.xml.namespace.QName("", "quote");
        private static final javax.xml.namespace.QName SOURCE$32 = 
            new javax.xml.namespace.QName("", "source");
        private static final javax.xml.namespace.QName LTEXT$34 = 
            new javax.xml.namespace.QName("", "lText");
        private static final javax.xml.namespace.QName ID$36 = 
            new javax.xml.namespace.QName("", "id");
        private static final javax.xml.namespace.QName MINIMAL$38 = 
            new javax.xml.namespace.QName("", "minimal");
        private static final javax.xml.namespace.QName ANSWERSTYLE$40 = 
            new javax.xml.namespace.QName("", "answer_style");
        private static final javax.xml.namespace.QName ANSWERTYPE$42 = 
            new javax.xml.namespace.QName("", "answer_type");
        private static final javax.xml.namespace.QName KNOWLEDGETYPE$44 = 
            new javax.xml.namespace.QName("", "knowledge_type");
        private static final javax.xml.namespace.QName ANSCOL$46 = 
            new javax.xml.namespace.QName("", "anscol");
        private static final javax.xml.namespace.QName TITLE$48 = 
            new javax.xml.namespace.QName("", "title");
        private static final javax.xml.namespace.QName SECTIONID$50 = 
            new javax.xml.namespace.QName("", "sectionId");
        
        
        /**
         * Gets array of all "garbled" elements
         */
        public noNamespace.GarbledDocument.Garbled[] getGarbledArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(GARBLED$0, targetList);
                noNamespace.GarbledDocument.Garbled[] result = new noNamespace.GarbledDocument.Garbled[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled getGarbledArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().find_element_user(GARBLED$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "garbled" element
         */
        public int sizeOfGarbledArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(GARBLED$0);
            }
        }
        
        /**
         * Sets array of all "garbled" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setGarbledArray(noNamespace.GarbledDocument.Garbled[] garbledArray)
        {
            check_orphaned();
            arraySetterHelper(garbledArray, GARBLED$0);
        }
        
        /**
         * Sets ith "garbled" element
         */
        public void setGarbledArray(int i, noNamespace.GarbledDocument.Garbled garbled)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().find_element_user(GARBLED$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(garbled);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled insertNewGarbled(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().insert_element_user(GARBLED$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled addNewGarbled()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().add_element_user(GARBLED$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "garbled" element
         */
        public void removeGarbled(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(GARBLED$0, i);
            }
        }
        
        /**
         * Gets array of all "label" elements
         */
        public noNamespace.LabelDocument.Label[] getLabelArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(LABEL$2, targetList);
                noNamespace.LabelDocument.Label[] result = new noNamespace.LabelDocument.Label[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "label" element
         */
        public noNamespace.LabelDocument.Label getLabelArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().find_element_user(LABEL$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "label" element
         */
        public int sizeOfLabelArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(LABEL$2);
            }
        }
        
        /**
         * Sets array of all "label" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setLabelArray(noNamespace.LabelDocument.Label[] labelArray)
        {
            check_orphaned();
            arraySetterHelper(labelArray, LABEL$2);
        }
        
        /**
         * Sets ith "label" element
         */
        public void setLabelArray(int i, noNamespace.LabelDocument.Label label)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().find_element_user(LABEL$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(label);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "label" element
         */
        public noNamespace.LabelDocument.Label insertNewLabel(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().insert_element_user(LABEL$2, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "label" element
         */
        public noNamespace.LabelDocument.Label addNewLabel()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().add_element_user(LABEL$2);
                return target;
            }
        }
        
        /**
         * Removes the ith "label" element
         */
        public void removeLabel(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(LABEL$2, i);
            }
        }
        
        /**
         * Gets array of all "instruction" elements
         */
        public noNamespace.InstructionDocument.Instruction[] getInstructionArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(INSTRUCTION$4, targetList);
                noNamespace.InstructionDocument.Instruction[] result = new noNamespace.InstructionDocument.Instruction[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "instruction" element
         */
        public noNamespace.InstructionDocument.Instruction getInstructionArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.InstructionDocument.Instruction target = null;
                target = (noNamespace.InstructionDocument.Instruction)get_store().find_element_user(INSTRUCTION$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "instruction" element
         */
        public int sizeOfInstructionArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(INSTRUCTION$4);
            }
        }
        
        /**
         * Sets array of all "instruction" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setInstructionArray(noNamespace.InstructionDocument.Instruction[] instructionArray)
        {
            check_orphaned();
            arraySetterHelper(instructionArray, INSTRUCTION$4);
        }
        
        /**
         * Sets ith "instruction" element
         */
        public void setInstructionArray(int i, noNamespace.InstructionDocument.Instruction instruction)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.InstructionDocument.Instruction target = null;
                target = (noNamespace.InstructionDocument.Instruction)get_store().find_element_user(INSTRUCTION$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(instruction);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "instruction" element
         */
        public noNamespace.InstructionDocument.Instruction insertNewInstruction(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.InstructionDocument.Instruction target = null;
                target = (noNamespace.InstructionDocument.Instruction)get_store().insert_element_user(INSTRUCTION$4, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "instruction" element
         */
        public noNamespace.InstructionDocument.Instruction addNewInstruction()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.InstructionDocument.Instruction target = null;
                target = (noNamespace.InstructionDocument.Instruction)get_store().add_element_user(INSTRUCTION$4);
                return target;
            }
        }
        
        /**
         * Removes the ith "instruction" element
         */
        public void removeInstruction(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(INSTRUCTION$4, i);
            }
        }
        
        /**
         * Gets array of all "data" elements
         */
        public noNamespace.DataDocument.Data[] getDataArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(DATA$6, targetList);
                noNamespace.DataDocument.Data[] result = new noNamespace.DataDocument.Data[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "data" element
         */
        public noNamespace.DataDocument.Data getDataArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.DataDocument.Data target = null;
                target = (noNamespace.DataDocument.Data)get_store().find_element_user(DATA$6, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "data" element
         */
        public int sizeOfDataArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(DATA$6);
            }
        }
        
        /**
         * Sets array of all "data" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setDataArray(noNamespace.DataDocument.Data[] dataArray)
        {
            check_orphaned();
            arraySetterHelper(dataArray, DATA$6);
        }
        
        /**
         * Sets ith "data" element
         */
        public void setDataArray(int i, noNamespace.DataDocument.Data data)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.DataDocument.Data target = null;
                target = (noNamespace.DataDocument.Data)get_store().find_element_user(DATA$6, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(data);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "data" element
         */
        public noNamespace.DataDocument.Data insertNewData(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.DataDocument.Data target = null;
                target = (noNamespace.DataDocument.Data)get_store().insert_element_user(DATA$6, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "data" element
         */
        public noNamespace.DataDocument.Data addNewData()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.DataDocument.Data target = null;
                target = (noNamespace.DataDocument.Data)get_store().add_element_user(DATA$6);
                return target;
            }
        }
        
        /**
         * Removes the ith "data" element
         */
        public void removeData(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(DATA$6, i);
            }
        }
        
        /**
         * Gets array of all "question" elements
         */
        public noNamespace.QuestionDocument.Question[] getQuestionArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(QUESTION$8, targetList);
                noNamespace.QuestionDocument.Question[] result = new noNamespace.QuestionDocument.Question[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "question" element
         */
        public noNamespace.QuestionDocument.Question getQuestionArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuestionDocument.Question target = null;
                target = (noNamespace.QuestionDocument.Question)get_store().find_element_user(QUESTION$8, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "question" element
         */
        public int sizeOfQuestionArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(QUESTION$8);
            }
        }
        
        /**
         * Sets array of all "question" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setQuestionArray(noNamespace.QuestionDocument.Question[] questionArray)
        {
            check_orphaned();
            arraySetterHelper(questionArray, QUESTION$8);
        }
        
        /**
         * Sets ith "question" element
         */
        public void setQuestionArray(int i, noNamespace.QuestionDocument.Question question)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuestionDocument.Question target = null;
                target = (noNamespace.QuestionDocument.Question)get_store().find_element_user(QUESTION$8, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(question);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "question" element
         */
        public noNamespace.QuestionDocument.Question insertNewQuestion(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuestionDocument.Question target = null;
                target = (noNamespace.QuestionDocument.Question)get_store().insert_element_user(QUESTION$8, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "question" element
         */
        public noNamespace.QuestionDocument.Question addNewQuestion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuestionDocument.Question target = null;
                target = (noNamespace.QuestionDocument.Question)get_store().add_element_user(QUESTION$8);
                return target;
            }
        }
        
        /**
         * Removes the ith "question" element
         */
        public void removeQuestion(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(QUESTION$8, i);
            }
        }
        
        /**
         * Gets array of all "ansColumn" elements
         */
        public noNamespace.AnsColumnDocument.AnsColumn[] getAnsColumnArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(ANSCOLUMN$10, targetList);
                noNamespace.AnsColumnDocument.AnsColumn[] result = new noNamespace.AnsColumnDocument.AnsColumn[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "ansColumn" element
         */
        public noNamespace.AnsColumnDocument.AnsColumn getAnsColumnArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.AnsColumnDocument.AnsColumn target = null;
                target = (noNamespace.AnsColumnDocument.AnsColumn)get_store().find_element_user(ANSCOLUMN$10, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "ansColumn" element
         */
        public int sizeOfAnsColumnArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ANSCOLUMN$10);
            }
        }
        
        /**
         * Sets array of all "ansColumn" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setAnsColumnArray(noNamespace.AnsColumnDocument.AnsColumn[] ansColumnArray)
        {
            check_orphaned();
            arraySetterHelper(ansColumnArray, ANSCOLUMN$10);
        }
        
        /**
         * Sets ith "ansColumn" element
         */
        public void setAnsColumnArray(int i, noNamespace.AnsColumnDocument.AnsColumn ansColumn)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.AnsColumnDocument.AnsColumn target = null;
                target = (noNamespace.AnsColumnDocument.AnsColumn)get_store().find_element_user(ANSCOLUMN$10, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(ansColumn);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ansColumn" element
         */
        public noNamespace.AnsColumnDocument.AnsColumn insertNewAnsColumn(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.AnsColumnDocument.AnsColumn target = null;
                target = (noNamespace.AnsColumnDocument.AnsColumn)get_store().insert_element_user(ANSCOLUMN$10, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ansColumn" element
         */
        public noNamespace.AnsColumnDocument.AnsColumn addNewAnsColumn()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.AnsColumnDocument.AnsColumn target = null;
                target = (noNamespace.AnsColumnDocument.AnsColumn)get_store().add_element_user(ANSCOLUMN$10);
                return target;
            }
        }
        
        /**
         * Removes the ith "ansColumn" element
         */
        public void removeAnsColumn(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ANSCOLUMN$10, i);
            }
        }
        
        /**
         * Gets array of all "choice" elements
         */
        public noNamespace.ChoiceDocument.Choice[] getChoiceArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(CHOICE$12, targetList);
                noNamespace.ChoiceDocument.Choice[] result = new noNamespace.ChoiceDocument.Choice[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "choice" element
         */
        public noNamespace.ChoiceDocument.Choice getChoiceArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoiceDocument.Choice target = null;
                target = (noNamespace.ChoiceDocument.Choice)get_store().find_element_user(CHOICE$12, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "choice" element
         */
        public int sizeOfChoiceArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CHOICE$12);
            }
        }
        
        /**
         * Sets array of all "choice" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setChoiceArray(noNamespace.ChoiceDocument.Choice[] choiceArray)
        {
            check_orphaned();
            arraySetterHelper(choiceArray, CHOICE$12);
        }
        
        /**
         * Sets ith "choice" element
         */
        public void setChoiceArray(int i, noNamespace.ChoiceDocument.Choice choice)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoiceDocument.Choice target = null;
                target = (noNamespace.ChoiceDocument.Choice)get_store().find_element_user(CHOICE$12, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(choice);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "choice" element
         */
        public noNamespace.ChoiceDocument.Choice insertNewChoice(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoiceDocument.Choice target = null;
                target = (noNamespace.ChoiceDocument.Choice)get_store().insert_element_user(CHOICE$12, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "choice" element
         */
        public noNamespace.ChoiceDocument.Choice addNewChoice()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoiceDocument.Choice target = null;
                target = (noNamespace.ChoiceDocument.Choice)get_store().add_element_user(CHOICE$12);
                return target;
            }
        }
        
        /**
         * Removes the ith "choice" element
         */
        public void removeChoice(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CHOICE$12, i);
            }
        }
        
        /**
         * Gets array of all "img" elements
         */
        public noNamespace.ImgDocument.Img[] getImgArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(IMG$14, targetList);
                noNamespace.ImgDocument.Img[] result = new noNamespace.ImgDocument.Img[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "img" element
         */
        public noNamespace.ImgDocument.Img getImgArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ImgDocument.Img target = null;
                target = (noNamespace.ImgDocument.Img)get_store().find_element_user(IMG$14, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "img" element
         */
        public int sizeOfImgArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IMG$14);
            }
        }
        
        /**
         * Sets array of all "img" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setImgArray(noNamespace.ImgDocument.Img[] imgArray)
        {
            check_orphaned();
            arraySetterHelper(imgArray, IMG$14);
        }
        
        /**
         * Sets ith "img" element
         */
        public void setImgArray(int i, noNamespace.ImgDocument.Img img)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ImgDocument.Img target = null;
                target = (noNamespace.ImgDocument.Img)get_store().find_element_user(IMG$14, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(img);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "img" element
         */
        public noNamespace.ImgDocument.Img insertNewImg(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ImgDocument.Img target = null;
                target = (noNamespace.ImgDocument.Img)get_store().insert_element_user(IMG$14, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "img" element
         */
        public noNamespace.ImgDocument.Img addNewImg()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ImgDocument.Img target = null;
                target = (noNamespace.ImgDocument.Img)get_store().add_element_user(IMG$14);
                return target;
            }
        }
        
        /**
         * Removes the ith "img" element
         */
        public void removeImg(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IMG$14, i);
            }
        }
        
        /**
         * Gets array of all "tbl" elements
         */
        public noNamespace.TblDocument.Tbl[] getTblArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(TBL$16, targetList);
                noNamespace.TblDocument.Tbl[] result = new noNamespace.TblDocument.Tbl[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "tbl" element
         */
        public noNamespace.TblDocument.Tbl getTblArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.TblDocument.Tbl target = null;
                target = (noNamespace.TblDocument.Tbl)get_store().find_element_user(TBL$16, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "tbl" element
         */
        public int sizeOfTblArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(TBL$16);
            }
        }
        
        /**
         * Sets array of all "tbl" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setTblArray(noNamespace.TblDocument.Tbl[] tblArray)
        {
            check_orphaned();
            arraySetterHelper(tblArray, TBL$16);
        }
        
        /**
         * Sets ith "tbl" element
         */
        public void setTblArray(int i, noNamespace.TblDocument.Tbl tbl)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.TblDocument.Tbl target = null;
                target = (noNamespace.TblDocument.Tbl)get_store().find_element_user(TBL$16, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(tbl);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "tbl" element
         */
        public noNamespace.TblDocument.Tbl insertNewTbl(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.TblDocument.Tbl target = null;
                target = (noNamespace.TblDocument.Tbl)get_store().insert_element_user(TBL$16, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "tbl" element
         */
        public noNamespace.TblDocument.Tbl addNewTbl()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.TblDocument.Tbl target = null;
                target = (noNamespace.TblDocument.Tbl)get_store().add_element_user(TBL$16);
                return target;
            }
        }
        
        /**
         * Removes the ith "tbl" element
         */
        public void removeTbl(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(TBL$16, i);
            }
        }
        
        /**
         * Gets array of all "choices" elements
         */
        public noNamespace.ChoicesDocument.Choices[] getChoicesArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(CHOICES$18, targetList);
                noNamespace.ChoicesDocument.Choices[] result = new noNamespace.ChoicesDocument.Choices[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "choices" element
         */
        public noNamespace.ChoicesDocument.Choices getChoicesArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoicesDocument.Choices target = null;
                target = (noNamespace.ChoicesDocument.Choices)get_store().find_element_user(CHOICES$18, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "choices" element
         */
        public int sizeOfChoicesArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CHOICES$18);
            }
        }
        
        /**
         * Sets array of all "choices" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setChoicesArray(noNamespace.ChoicesDocument.Choices[] choicesArray)
        {
            check_orphaned();
            arraySetterHelper(choicesArray, CHOICES$18);
        }
        
        /**
         * Sets ith "choices" element
         */
        public void setChoicesArray(int i, noNamespace.ChoicesDocument.Choices choices)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoicesDocument.Choices target = null;
                target = (noNamespace.ChoicesDocument.Choices)get_store().find_element_user(CHOICES$18, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(choices);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "choices" element
         */
        public noNamespace.ChoicesDocument.Choices insertNewChoices(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoicesDocument.Choices target = null;
                target = (noNamespace.ChoicesDocument.Choices)get_store().insert_element_user(CHOICES$18, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "choices" element
         */
        public noNamespace.ChoicesDocument.Choices addNewChoices()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoicesDocument.Choices target = null;
                target = (noNamespace.ChoicesDocument.Choices)get_store().add_element_user(CHOICES$18);
                return target;
            }
        }
        
        /**
         * Removes the ith "choices" element
         */
        public void removeChoices(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CHOICES$18, i);
            }
        }
        
        /**
         * Gets array of all "cNum" elements
         */
        public noNamespace.CNumDocument.CNum[] getCNumArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(CNUM$20, targetList);
                noNamespace.CNumDocument.CNum[] result = new noNamespace.CNumDocument.CNum[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "cNum" element
         */
        public noNamespace.CNumDocument.CNum getCNumArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CNumDocument.CNum target = null;
                target = (noNamespace.CNumDocument.CNum)get_store().find_element_user(CNUM$20, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "cNum" element
         */
        public int sizeOfCNumArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CNUM$20);
            }
        }
        
        /**
         * Sets array of all "cNum" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setCNumArray(noNamespace.CNumDocument.CNum[] cNumArray)
        {
            check_orphaned();
            arraySetterHelper(cNumArray, CNUM$20);
        }
        
        /**
         * Sets ith "cNum" element
         */
        public void setCNumArray(int i, noNamespace.CNumDocument.CNum cNum)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CNumDocument.CNum target = null;
                target = (noNamespace.CNumDocument.CNum)get_store().find_element_user(CNUM$20, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(cNum);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "cNum" element
         */
        public noNamespace.CNumDocument.CNum insertNewCNum(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CNumDocument.CNum target = null;
                target = (noNamespace.CNumDocument.CNum)get_store().insert_element_user(CNUM$20, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "cNum" element
         */
        public noNamespace.CNumDocument.CNum addNewCNum()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CNumDocument.CNum target = null;
                target = (noNamespace.CNumDocument.CNum)get_store().add_element_user(CNUM$20);
                return target;
            }
        }
        
        /**
         * Removes the ith "cNum" element
         */
        public void removeCNum(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CNUM$20, i);
            }
        }
        
        /**
         * Gets array of all "missing" elements
         */
        public noNamespace.MissingDocument.Missing[] getMissingArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(MISSING$22, targetList);
                noNamespace.MissingDocument.Missing[] result = new noNamespace.MissingDocument.Missing[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "missing" element
         */
        public noNamespace.MissingDocument.Missing getMissingArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().find_element_user(MISSING$22, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "missing" element
         */
        public int sizeOfMissingArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(MISSING$22);
            }
        }
        
        /**
         * Sets array of all "missing" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setMissingArray(noNamespace.MissingDocument.Missing[] missingArray)
        {
            check_orphaned();
            arraySetterHelper(missingArray, MISSING$22);
        }
        
        /**
         * Sets ith "missing" element
         */
        public void setMissingArray(int i, noNamespace.MissingDocument.Missing missing)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().find_element_user(MISSING$22, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(missing);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "missing" element
         */
        public noNamespace.MissingDocument.Missing insertNewMissing(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().insert_element_user(MISSING$22, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "missing" element
         */
        public noNamespace.MissingDocument.Missing addNewMissing()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().add_element_user(MISSING$22);
                return target;
            }
        }
        
        /**
         * Removes the ith "missing" element
         */
        public void removeMissing(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(MISSING$22, i);
            }
        }
        
        /**
         * Gets array of all "ref" elements
         */
        public noNamespace.RefDocument.Ref[] getRefArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(REF$24, targetList);
                noNamespace.RefDocument.Ref[] result = new noNamespace.RefDocument.Ref[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "ref" element
         */
        public noNamespace.RefDocument.Ref getRefArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().find_element_user(REF$24, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "ref" element
         */
        public int sizeOfRefArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(REF$24);
            }
        }
        
        /**
         * Sets array of all "ref" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setRefArray(noNamespace.RefDocument.Ref[] refArray)
        {
            check_orphaned();
            arraySetterHelper(refArray, REF$24);
        }
        
        /**
         * Sets ith "ref" element
         */
        public void setRefArray(int i, noNamespace.RefDocument.Ref ref)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().find_element_user(REF$24, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(ref);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ref" element
         */
        public noNamespace.RefDocument.Ref insertNewRef(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().insert_element_user(REF$24, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ref" element
         */
        public noNamespace.RefDocument.Ref addNewRef()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().add_element_user(REF$24);
                return target;
            }
        }
        
        /**
         * Removes the ith "ref" element
         */
        public void removeRef(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(REF$24, i);
            }
        }
        
        /**
         * Gets array of all "br" elements
         */
        public noNamespace.BrDocument.Br[] getBrArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(BR$26, targetList);
                noNamespace.BrDocument.Br[] result = new noNamespace.BrDocument.Br[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "br" element
         */
        public noNamespace.BrDocument.Br getBrArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().find_element_user(BR$26, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "br" element
         */
        public int sizeOfBrArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(BR$26);
            }
        }
        
        /**
         * Sets array of all "br" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setBrArray(noNamespace.BrDocument.Br[] brArray)
        {
            check_orphaned();
            arraySetterHelper(brArray, BR$26);
        }
        
        /**
         * Sets ith "br" element
         */
        public void setBrArray(int i, noNamespace.BrDocument.Br br)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().find_element_user(BR$26, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(br);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "br" element
         */
        public noNamespace.BrDocument.Br insertNewBr(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().insert_element_user(BR$26, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "br" element
         */
        public noNamespace.BrDocument.Br addNewBr()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().add_element_user(BR$26);
                return target;
            }
        }
        
        /**
         * Removes the ith "br" element
         */
        public void removeBr(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(BR$26, i);
            }
        }
        
        /**
         * Gets array of all "formula" elements
         */
        public noNamespace.FormulaDocument.Formula[] getFormulaArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(FORMULA$28, targetList);
                noNamespace.FormulaDocument.Formula[] result = new noNamespace.FormulaDocument.Formula[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "formula" element
         */
        public noNamespace.FormulaDocument.Formula getFormulaArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.FormulaDocument.Formula target = null;
                target = (noNamespace.FormulaDocument.Formula)get_store().find_element_user(FORMULA$28, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "formula" element
         */
        public int sizeOfFormulaArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(FORMULA$28);
            }
        }
        
        /**
         * Sets array of all "formula" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setFormulaArray(noNamespace.FormulaDocument.Formula[] formulaArray)
        {
            check_orphaned();
            arraySetterHelper(formulaArray, FORMULA$28);
        }
        
        /**
         * Sets ith "formula" element
         */
        public void setFormulaArray(int i, noNamespace.FormulaDocument.Formula formula)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.FormulaDocument.Formula target = null;
                target = (noNamespace.FormulaDocument.Formula)get_store().find_element_user(FORMULA$28, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(formula);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "formula" element
         */
        public noNamespace.FormulaDocument.Formula insertNewFormula(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.FormulaDocument.Formula target = null;
                target = (noNamespace.FormulaDocument.Formula)get_store().insert_element_user(FORMULA$28, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "formula" element
         */
        public noNamespace.FormulaDocument.Formula addNewFormula()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.FormulaDocument.Formula target = null;
                target = (noNamespace.FormulaDocument.Formula)get_store().add_element_user(FORMULA$28);
                return target;
            }
        }
        
        /**
         * Removes the ith "formula" element
         */
        public void removeFormula(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(FORMULA$28, i);
            }
        }
        
        /**
         * Gets array of all "quote" elements
         */
        public noNamespace.QuoteDocument.Quote[] getQuoteArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(QUOTE$30, targetList);
                noNamespace.QuoteDocument.Quote[] result = new noNamespace.QuoteDocument.Quote[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "quote" element
         */
        public noNamespace.QuoteDocument.Quote getQuoteArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuoteDocument.Quote target = null;
                target = (noNamespace.QuoteDocument.Quote)get_store().find_element_user(QUOTE$30, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "quote" element
         */
        public int sizeOfQuoteArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(QUOTE$30);
            }
        }
        
        /**
         * Sets array of all "quote" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setQuoteArray(noNamespace.QuoteDocument.Quote[] quoteArray)
        {
            check_orphaned();
            arraySetterHelper(quoteArray, QUOTE$30);
        }
        
        /**
         * Sets ith "quote" element
         */
        public void setQuoteArray(int i, noNamespace.QuoteDocument.Quote quote)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuoteDocument.Quote target = null;
                target = (noNamespace.QuoteDocument.Quote)get_store().find_element_user(QUOTE$30, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(quote);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "quote" element
         */
        public noNamespace.QuoteDocument.Quote insertNewQuote(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuoteDocument.Quote target = null;
                target = (noNamespace.QuoteDocument.Quote)get_store().insert_element_user(QUOTE$30, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "quote" element
         */
        public noNamespace.QuoteDocument.Quote addNewQuote()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuoteDocument.Quote target = null;
                target = (noNamespace.QuoteDocument.Quote)get_store().add_element_user(QUOTE$30);
                return target;
            }
        }
        
        /**
         * Removes the ith "quote" element
         */
        public void removeQuote(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(QUOTE$30, i);
            }
        }
        
        /**
         * Gets array of all "source" elements
         */
        public noNamespace.SourceDocument.Source[] getSourceArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(SOURCE$32, targetList);
                noNamespace.SourceDocument.Source[] result = new noNamespace.SourceDocument.Source[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "source" element
         */
        public noNamespace.SourceDocument.Source getSourceArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.SourceDocument.Source target = null;
                target = (noNamespace.SourceDocument.Source)get_store().find_element_user(SOURCE$32, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "source" element
         */
        public int sizeOfSourceArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(SOURCE$32);
            }
        }
        
        /**
         * Sets array of all "source" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setSourceArray(noNamespace.SourceDocument.Source[] sourceArray)
        {
            check_orphaned();
            arraySetterHelper(sourceArray, SOURCE$32);
        }
        
        /**
         * Sets ith "source" element
         */
        public void setSourceArray(int i, noNamespace.SourceDocument.Source source)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.SourceDocument.Source target = null;
                target = (noNamespace.SourceDocument.Source)get_store().find_element_user(SOURCE$32, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(source);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "source" element
         */
        public noNamespace.SourceDocument.Source insertNewSource(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.SourceDocument.Source target = null;
                target = (noNamespace.SourceDocument.Source)get_store().insert_element_user(SOURCE$32, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "source" element
         */
        public noNamespace.SourceDocument.Source addNewSource()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.SourceDocument.Source target = null;
                target = (noNamespace.SourceDocument.Source)get_store().add_element_user(SOURCE$32);
                return target;
            }
        }
        
        /**
         * Removes the ith "source" element
         */
        public void removeSource(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(SOURCE$32, i);
            }
        }
        
        /**
         * Gets array of all "lText" elements
         */
        public noNamespace.LTextDocument.LText[] getLTextArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(LTEXT$34, targetList);
                noNamespace.LTextDocument.LText[] result = new noNamespace.LTextDocument.LText[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "lText" element
         */
        public noNamespace.LTextDocument.LText getLTextArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LTextDocument.LText target = null;
                target = (noNamespace.LTextDocument.LText)get_store().find_element_user(LTEXT$34, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "lText" element
         */
        public int sizeOfLTextArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(LTEXT$34);
            }
        }
        
        /**
         * Sets array of all "lText" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setLTextArray(noNamespace.LTextDocument.LText[] lTextArray)
        {
            check_orphaned();
            arraySetterHelper(lTextArray, LTEXT$34);
        }
        
        /**
         * Sets ith "lText" element
         */
        public void setLTextArray(int i, noNamespace.LTextDocument.LText lText)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LTextDocument.LText target = null;
                target = (noNamespace.LTextDocument.LText)get_store().find_element_user(LTEXT$34, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(lText);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "lText" element
         */
        public noNamespace.LTextDocument.LText insertNewLText(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LTextDocument.LText target = null;
                target = (noNamespace.LTextDocument.LText)get_store().insert_element_user(LTEXT$34, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "lText" element
         */
        public noNamespace.LTextDocument.LText addNewLText()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LTextDocument.LText target = null;
                target = (noNamespace.LTextDocument.LText)get_store().add_element_user(LTEXT$34);
                return target;
            }
        }
        
        /**
         * Removes the ith "lText" element
         */
        public void removeLText(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(LTEXT$34, i);
            }
        }
        
        /**
         * Gets the "id" attribute
         */
        public java.lang.String getId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(ID$36);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "id" attribute
         */
        public org.apache.xmlbeans.XmlID xgetId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlID target = null;
                target = (org.apache.xmlbeans.XmlID)get_store().find_attribute_user(ID$36);
                return target;
            }
        }
        
        /**
         * Sets the "id" attribute
         */
        public void setId(java.lang.String id)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(ID$36);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(ID$36);
                }
                target.setStringValue(id);
            }
        }
        
        /**
         * Sets (as xml) the "id" attribute
         */
        public void xsetId(org.apache.xmlbeans.XmlID id)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlID target = null;
                target = (org.apache.xmlbeans.XmlID)get_store().find_attribute_user(ID$36);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlID)get_store().add_attribute_user(ID$36);
                }
                target.set(id);
            }
        }
        
        /**
         * Gets the "minimal" attribute
         */
        public noNamespace.QuestionDocument.Question.Minimal.Enum getMinimal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(MINIMAL$38);
                if (target == null)
                {
                    return null;
                }
                return (noNamespace.QuestionDocument.Question.Minimal.Enum)target.getEnumValue();
            }
        }
        
        /**
         * Gets (as xml) the "minimal" attribute
         */
        public noNamespace.QuestionDocument.Question.Minimal xgetMinimal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuestionDocument.Question.Minimal target = null;
                target = (noNamespace.QuestionDocument.Question.Minimal)get_store().find_attribute_user(MINIMAL$38);
                return target;
            }
        }
        
        /**
         * Sets the "minimal" attribute
         */
        public void setMinimal(noNamespace.QuestionDocument.Question.Minimal.Enum minimal)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(MINIMAL$38);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(MINIMAL$38);
                }
                target.setEnumValue(minimal);
            }
        }
        
        /**
         * Sets (as xml) the "minimal" attribute
         */
        public void xsetMinimal(noNamespace.QuestionDocument.Question.Minimal minimal)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuestionDocument.Question.Minimal target = null;
                target = (noNamespace.QuestionDocument.Question.Minimal)get_store().find_attribute_user(MINIMAL$38);
                if (target == null)
                {
                    target = (noNamespace.QuestionDocument.Question.Minimal)get_store().add_attribute_user(MINIMAL$38);
                }
                target.set(minimal);
            }
        }
        
        /**
         * Gets the "answer_style" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType getAnswerStyle()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().find_attribute_user(ANSWERSTYLE$40);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "answer_style" attribute
         */
        public boolean isSetAnswerStyle()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(ANSWERSTYLE$40) != null;
            }
        }
        
        /**
         * Sets the "answer_style" attribute
         */
        public void setAnswerStyle(org.apache.xmlbeans.XmlAnySimpleType answerStyle)
        {
            generatedSetterHelperImpl(answerStyle, ANSWERSTYLE$40, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "answer_style" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType addNewAnswerStyle()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().add_attribute_user(ANSWERSTYLE$40);
                return target;
            }
        }
        
        /**
         * Unsets the "answer_style" attribute
         */
        public void unsetAnswerStyle()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(ANSWERSTYLE$40);
            }
        }
        
        /**
         * Gets the "answer_type" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType getAnswerType()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().find_attribute_user(ANSWERTYPE$42);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "answer_type" attribute
         */
        public boolean isSetAnswerType()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(ANSWERTYPE$42) != null;
            }
        }
        
        /**
         * Sets the "answer_type" attribute
         */
        public void setAnswerType(org.apache.xmlbeans.XmlAnySimpleType answerType)
        {
            generatedSetterHelperImpl(answerType, ANSWERTYPE$42, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "answer_type" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType addNewAnswerType()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().add_attribute_user(ANSWERTYPE$42);
                return target;
            }
        }
        
        /**
         * Unsets the "answer_type" attribute
         */
        public void unsetAnswerType()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(ANSWERTYPE$42);
            }
        }
        
        /**
         * Gets the "knowledge_type" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType getKnowledgeType()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().find_attribute_user(KNOWLEDGETYPE$44);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "knowledge_type" attribute
         */
        public boolean isSetKnowledgeType()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(KNOWLEDGETYPE$44) != null;
            }
        }
        
        /**
         * Sets the "knowledge_type" attribute
         */
        public void setKnowledgeType(org.apache.xmlbeans.XmlAnySimpleType knowledgeType)
        {
            generatedSetterHelperImpl(knowledgeType, KNOWLEDGETYPE$44, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "knowledge_type" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType addNewKnowledgeType()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().add_attribute_user(KNOWLEDGETYPE$44);
                return target;
            }
        }
        
        /**
         * Unsets the "knowledge_type" attribute
         */
        public void unsetKnowledgeType()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(KNOWLEDGETYPE$44);
            }
        }
        
        /**
         * Gets the "anscol" attribute
         */
        public java.util.List getAnscol()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(ANSCOL$46);
                if (target == null)
                {
                    return null;
                }
                return target.getListValue();
            }
        }
        
        /**
         * Gets (as xml) the "anscol" attribute
         */
        public org.apache.xmlbeans.XmlIDREFS xgetAnscol()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlIDREFS target = null;
                target = (org.apache.xmlbeans.XmlIDREFS)get_store().find_attribute_user(ANSCOL$46);
                return target;
            }
        }
        
        /**
         * True if has "anscol" attribute
         */
        public boolean isSetAnscol()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(ANSCOL$46) != null;
            }
        }
        
        /**
         * Sets the "anscol" attribute
         */
        public void setAnscol(java.util.List anscol)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(ANSCOL$46);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(ANSCOL$46);
                }
                target.setListValue(anscol);
            }
        }
        
        /**
         * Sets (as xml) the "anscol" attribute
         */
        public void xsetAnscol(org.apache.xmlbeans.XmlIDREFS anscol)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlIDREFS target = null;
                target = (org.apache.xmlbeans.XmlIDREFS)get_store().find_attribute_user(ANSCOL$46);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlIDREFS)get_store().add_attribute_user(ANSCOL$46);
                }
                target.set(anscol);
            }
        }
        
        /**
         * Unsets the "anscol" attribute
         */
        public void unsetAnscol()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(ANSCOL$46);
            }
        }
        
        /**
         * Gets the "title" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType getTitle()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().find_attribute_user(TITLE$48);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "title" attribute
         */
        public boolean isSetTitle()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(TITLE$48) != null;
            }
        }
        
        /**
         * Sets the "title" attribute
         */
        public void setTitle(org.apache.xmlbeans.XmlAnySimpleType title)
        {
            generatedSetterHelperImpl(title, TITLE$48, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "title" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType addNewTitle()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().add_attribute_user(TITLE$48);
                return target;
            }
        }
        
        /**
         * Unsets the "title" attribute
         */
        public void unsetTitle()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(TITLE$48);
            }
        }
        
        /**
         * Gets the "sectionId" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType getSectionId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().find_attribute_user(SECTIONID$50);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "sectionId" attribute
         */
        public boolean isSetSectionId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(SECTIONID$50) != null;
            }
        }
        
        /**
         * Sets the "sectionId" attribute
         */
        public void setSectionId(org.apache.xmlbeans.XmlAnySimpleType sectionId)
        {
            generatedSetterHelperImpl(sectionId, SECTIONID$50, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "sectionId" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType addNewSectionId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().add_attribute_user(SECTIONID$50);
                return target;
            }
        }
        
        /**
         * Unsets the "sectionId" attribute
         */
        public void unsetSectionId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(SECTIONID$50);
            }
        }
        /**
         * An XML minimal(@).
         *
         * This is an atomic type that is a restriction of noNamespace.QuestionDocument$Question$Minimal.
         */
        public static class MinimalImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements noNamespace.QuestionDocument.Question.Minimal
        {
            private static final long serialVersionUID = 1L;
            
            public MinimalImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MinimalImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
    }
}
