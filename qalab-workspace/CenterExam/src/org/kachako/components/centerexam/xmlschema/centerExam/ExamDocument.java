/*
 * An XML document type.
 * Localname: exam
 * Namespace: 
 * Java type: noNamespace.ExamDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.centerExam;


/**
 * A document containing one exam(@) element.
 *
 * This is a complex type.
 */
public interface ExamDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ExamDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s9AD9431611720D569E6CDA08F9EDE660").resolveHandle("examdc18doctype");
    
    /**
     * Gets the "exam" element
     */
    noNamespace.ExamDocument.Exam getExam();
    
    /**
     * Sets the "exam" element
     */
    void setExam(noNamespace.ExamDocument.Exam exam);
    
    /**
     * Appends and returns a new empty "exam" element
     */
    noNamespace.ExamDocument.Exam addNewExam();
    
    /**
     * An XML exam(@).
     *
     * This is a complex type.
     */
    public interface Exam extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Exam.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s9AD9431611720D569E6CDA08F9EDE660").resolveHandle("examd70delemtype");
        
        /**
         * Gets array of all "title" elements
         */
        noNamespace.TitleDocument.Title[] getTitleArray();
        
        /**
         * Gets ith "title" element
         */
        noNamespace.TitleDocument.Title getTitleArray(int i);
        
        /**
         * Returns number of "title" element
         */
        int sizeOfTitleArray();
        
        /**
         * Sets array of all "title" element
         */
        void setTitleArray(noNamespace.TitleDocument.Title[] titleArray);
        
        /**
         * Sets ith "title" element
         */
        void setTitleArray(int i, noNamespace.TitleDocument.Title title);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "title" element
         */
        noNamespace.TitleDocument.Title insertNewTitle(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "title" element
         */
        noNamespace.TitleDocument.Title addNewTitle();
        
        /**
         * Removes the ith "title" element
         */
        void removeTitle(int i);
        
        /**
         * Gets array of all "info" elements
         */
        noNamespace.InfoDocument.Info[] getInfoArray();
        
        /**
         * Gets ith "info" element
         */
        noNamespace.InfoDocument.Info getInfoArray(int i);
        
        /**
         * Returns number of "info" element
         */
        int sizeOfInfoArray();
        
        /**
         * Sets array of all "info" element
         */
        void setInfoArray(noNamespace.InfoDocument.Info[] infoArray);
        
        /**
         * Sets ith "info" element
         */
        void setInfoArray(int i, noNamespace.InfoDocument.Info info);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "info" element
         */
        noNamespace.InfoDocument.Info insertNewInfo(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "info" element
         */
        noNamespace.InfoDocument.Info addNewInfo();
        
        /**
         * Removes the ith "info" element
         */
        void removeInfo(int i);
        
        /**
         * Gets array of all "question" elements
         */
        noNamespace.QuestionDocument.Question[] getQuestionArray();
        
        /**
         * Gets ith "question" element
         */
        noNamespace.QuestionDocument.Question getQuestionArray(int i);
        
        /**
         * Returns number of "question" element
         */
        int sizeOfQuestionArray();
        
        /**
         * Sets array of all "question" element
         */
        void setQuestionArray(noNamespace.QuestionDocument.Question[] questionArray);
        
        /**
         * Sets ith "question" element
         */
        void setQuestionArray(int i, noNamespace.QuestionDocument.Question question);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "question" element
         */
        noNamespace.QuestionDocument.Question insertNewQuestion(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "question" element
         */
        noNamespace.QuestionDocument.Question addNewQuestion();
        
        /**
         * Removes the ith "question" element
         */
        void removeQuestion(int i);
        
        /**
         * Gets array of all "cNum" elements
         */
        noNamespace.CNumDocument.CNum[] getCNumArray();
        
        /**
         * Gets ith "cNum" element
         */
        noNamespace.CNumDocument.CNum getCNumArray(int i);
        
        /**
         * Returns number of "cNum" element
         */
        int sizeOfCNumArray();
        
        /**
         * Sets array of all "cNum" element
         */
        void setCNumArray(noNamespace.CNumDocument.CNum[] cNumArray);
        
        /**
         * Sets ith "cNum" element
         */
        void setCNumArray(int i, noNamespace.CNumDocument.CNum cNum);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "cNum" element
         */
        noNamespace.CNumDocument.CNum insertNewCNum(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "cNum" element
         */
        noNamespace.CNumDocument.CNum addNewCNum();
        
        /**
         * Removes the ith "cNum" element
         */
        void removeCNum(int i);
        
        /**
         * Gets array of all "label" elements
         */
        noNamespace.LabelDocument.Label[] getLabelArray();
        
        /**
         * Gets ith "label" element
         */
        noNamespace.LabelDocument.Label getLabelArray(int i);
        
        /**
         * Returns number of "label" element
         */
        int sizeOfLabelArray();
        
        /**
         * Sets array of all "label" element
         */
        void setLabelArray(noNamespace.LabelDocument.Label[] labelArray);
        
        /**
         * Sets ith "label" element
         */
        void setLabelArray(int i, noNamespace.LabelDocument.Label label);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "label" element
         */
        noNamespace.LabelDocument.Label insertNewLabel(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "label" element
         */
        noNamespace.LabelDocument.Label addNewLabel();
        
        /**
         * Removes the ith "label" element
         */
        void removeLabel(int i);
        
        /**
         * Gets array of all "garbled" elements
         */
        noNamespace.GarbledDocument.Garbled[] getGarbledArray();
        
        /**
         * Gets ith "garbled" element
         */
        noNamespace.GarbledDocument.Garbled getGarbledArray(int i);
        
        /**
         * Returns number of "garbled" element
         */
        int sizeOfGarbledArray();
        
        /**
         * Sets array of all "garbled" element
         */
        void setGarbledArray(noNamespace.GarbledDocument.Garbled[] garbledArray);
        
        /**
         * Sets ith "garbled" element
         */
        void setGarbledArray(int i, noNamespace.GarbledDocument.Garbled garbled);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "garbled" element
         */
        noNamespace.GarbledDocument.Garbled insertNewGarbled(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "garbled" element
         */
        noNamespace.GarbledDocument.Garbled addNewGarbled();
        
        /**
         * Removes the ith "garbled" element
         */
        void removeGarbled(int i);
        
        /**
         * Gets array of all "missing" elements
         */
        noNamespace.MissingDocument.Missing[] getMissingArray();
        
        /**
         * Gets ith "missing" element
         */
        noNamespace.MissingDocument.Missing getMissingArray(int i);
        
        /**
         * Returns number of "missing" element
         */
        int sizeOfMissingArray();
        
        /**
         * Sets array of all "missing" element
         */
        void setMissingArray(noNamespace.MissingDocument.Missing[] missingArray);
        
        /**
         * Sets ith "missing" element
         */
        void setMissingArray(int i, noNamespace.MissingDocument.Missing missing);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "missing" element
         */
        noNamespace.MissingDocument.Missing insertNewMissing(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "missing" element
         */
        noNamespace.MissingDocument.Missing addNewMissing();
        
        /**
         * Removes the ith "missing" element
         */
        void removeMissing(int i);
        
        /**
         * Gets array of all "choice" elements
         */
        noNamespace.ChoiceDocument.Choice[] getChoiceArray();
        
        /**
         * Gets ith "choice" element
         */
        noNamespace.ChoiceDocument.Choice getChoiceArray(int i);
        
        /**
         * Returns number of "choice" element
         */
        int sizeOfChoiceArray();
        
        /**
         * Sets array of all "choice" element
         */
        void setChoiceArray(noNamespace.ChoiceDocument.Choice[] choiceArray);
        
        /**
         * Sets ith "choice" element
         */
        void setChoiceArray(int i, noNamespace.ChoiceDocument.Choice choice);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "choice" element
         */
        noNamespace.ChoiceDocument.Choice insertNewChoice(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "choice" element
         */
        noNamespace.ChoiceDocument.Choice addNewChoice();
        
        /**
         * Removes the ith "choice" element
         */
        void removeChoice(int i);
        
        /**
         * Gets array of all "choices" elements
         */
        noNamespace.ChoicesDocument.Choices[] getChoicesArray();
        
        /**
         * Gets ith "choices" element
         */
        noNamespace.ChoicesDocument.Choices getChoicesArray(int i);
        
        /**
         * Returns number of "choices" element
         */
        int sizeOfChoicesArray();
        
        /**
         * Sets array of all "choices" element
         */
        void setChoicesArray(noNamespace.ChoicesDocument.Choices[] choicesArray);
        
        /**
         * Sets ith "choices" element
         */
        void setChoicesArray(int i, noNamespace.ChoicesDocument.Choices choices);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "choices" element
         */
        noNamespace.ChoicesDocument.Choices insertNewChoices(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "choices" element
         */
        noNamespace.ChoicesDocument.Choices addNewChoices();
        
        /**
         * Removes the ith "choices" element
         */
        void removeChoices(int i);
        
        /**
         * Gets array of all "br" elements
         */
        noNamespace.BrDocument.Br[] getBrArray();
        
        /**
         * Gets ith "br" element
         */
        noNamespace.BrDocument.Br getBrArray(int i);
        
        /**
         * Returns number of "br" element
         */
        int sizeOfBrArray();
        
        /**
         * Sets array of all "br" element
         */
        void setBrArray(noNamespace.BrDocument.Br[] brArray);
        
        /**
         * Sets ith "br" element
         */
        void setBrArray(int i, noNamespace.BrDocument.Br br);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "br" element
         */
        noNamespace.BrDocument.Br insertNewBr(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "br" element
         */
        noNamespace.BrDocument.Br addNewBr();
        
        /**
         * Removes the ith "br" element
         */
        void removeBr(int i);
        
        /**
         * Gets array of all "ref" elements
         */
        noNamespace.RefDocument.Ref[] getRefArray();
        
        /**
         * Gets ith "ref" element
         */
        noNamespace.RefDocument.Ref getRefArray(int i);
        
        /**
         * Returns number of "ref" element
         */
        int sizeOfRefArray();
        
        /**
         * Sets array of all "ref" element
         */
        void setRefArray(noNamespace.RefDocument.Ref[] refArray);
        
        /**
         * Sets ith "ref" element
         */
        void setRefArray(int i, noNamespace.RefDocument.Ref ref);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ref" element
         */
        noNamespace.RefDocument.Ref insertNewRef(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ref" element
         */
        noNamespace.RefDocument.Ref addNewRef();
        
        /**
         * Removes the ith "ref" element
         */
        void removeRef(int i);
        
        /**
         * Gets array of all "ansColumn" elements
         */
        noNamespace.AnsColumnDocument.AnsColumn[] getAnsColumnArray();
        
        /**
         * Gets ith "ansColumn" element
         */
        noNamespace.AnsColumnDocument.AnsColumn getAnsColumnArray(int i);
        
        /**
         * Returns number of "ansColumn" element
         */
        int sizeOfAnsColumnArray();
        
        /**
         * Sets array of all "ansColumn" element
         */
        void setAnsColumnArray(noNamespace.AnsColumnDocument.AnsColumn[] ansColumnArray);
        
        /**
         * Sets ith "ansColumn" element
         */
        void setAnsColumnArray(int i, noNamespace.AnsColumnDocument.AnsColumn ansColumn);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ansColumn" element
         */
        noNamespace.AnsColumnDocument.AnsColumn insertNewAnsColumn(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ansColumn" element
         */
        noNamespace.AnsColumnDocument.AnsColumn addNewAnsColumn();
        
        /**
         * Removes the ith "ansColumn" element
         */
        void removeAnsColumn(int i);
        
        /**
         * Gets the "source" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType getSource();
        
        /**
         * True if has "source" attribute
         */
        boolean isSetSource();
        
        /**
         * Sets the "source" attribute
         */
        void setSource(org.apache.xmlbeans.XmlAnySimpleType source);
        
        /**
         * Appends and returns a new empty "source" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType addNewSource();
        
        /**
         * Unsets the "source" attribute
         */
        void unsetSource();
        
        /**
         * Gets the "srcTxtURL" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType getSrcTxtURL();
        
        /**
         * True if has "srcTxtURL" attribute
         */
        boolean isSetSrcTxtURL();
        
        /**
         * Sets the "srcTxtURL" attribute
         */
        void setSrcTxtURL(org.apache.xmlbeans.XmlAnySimpleType srcTxtURL);
        
        /**
         * Appends and returns a new empty "srcTxtURL" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType addNewSrcTxtURL();
        
        /**
         * Unsets the "srcTxtURL" attribute
         */
        void unsetSrcTxtURL();
        
        /**
         * Gets the "subject" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType getSubject();
        
        /**
         * Sets the "subject" attribute
         */
        void setSubject(org.apache.xmlbeans.XmlAnySimpleType subject);
        
        /**
         * Appends and returns a new empty "subject" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType addNewSubject();
        
        /**
         * Gets the "year" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType getYear();
        
        /**
         * True if has "year" attribute
         */
        boolean isSetYear();
        
        /**
         * Sets the "year" attribute
         */
        void setYear(org.apache.xmlbeans.XmlAnySimpleType year);
        
        /**
         * Appends and returns a new empty "year" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType addNewYear();
        
        /**
         * Unsets the "year" attribute
         */
        void unsetYear();
        
        /**
         * Gets the "space" attribute
         */
        org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space.Enum getSpace();
        
        /**
         * Gets (as xml) the "space" attribute
         */
        org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space xgetSpace();
        
        /**
         * True if has "space" attribute
         */
        boolean isSetSpace();
        
        /**
         * Sets the "space" attribute
         */
        void setSpace(org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space.Enum space);
        
        /**
         * Sets (as xml) the "space" attribute
         */
        void xsetSpace(org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space space);
        
        /**
         * Unsets the "space" attribute
         */
        void unsetSpace();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static noNamespace.ExamDocument.Exam newInstance() {
              return (noNamespace.ExamDocument.Exam) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static noNamespace.ExamDocument.Exam newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (noNamespace.ExamDocument.Exam) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static noNamespace.ExamDocument newInstance() {
          return (noNamespace.ExamDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static noNamespace.ExamDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (noNamespace.ExamDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static noNamespace.ExamDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.ExamDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static noNamespace.ExamDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.ExamDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static noNamespace.ExamDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.ExamDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static noNamespace.ExamDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.ExamDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static noNamespace.ExamDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.ExamDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static noNamespace.ExamDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.ExamDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static noNamespace.ExamDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.ExamDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static noNamespace.ExamDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.ExamDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static noNamespace.ExamDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.ExamDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static noNamespace.ExamDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.ExamDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static noNamespace.ExamDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.ExamDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static noNamespace.ExamDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.ExamDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static noNamespace.ExamDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.ExamDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static noNamespace.ExamDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.ExamDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static noNamespace.ExamDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (noNamespace.ExamDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static noNamespace.ExamDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (noNamespace.ExamDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
