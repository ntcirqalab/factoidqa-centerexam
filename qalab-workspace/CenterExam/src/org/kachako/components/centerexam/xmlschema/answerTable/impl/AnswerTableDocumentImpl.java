/*
 * An XML document type.
 * Localname: answerTable
 * Namespace: 
 * Java type: AnswerTableDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.answerTable.impl;

import org.kachako.components.centerexam.xmlschema.answerTable.AnswerTableDocument;
import org.kachako.components.centerexam.xmlschema.answerTable.DataDocument;

/**
 * A document containing one answerTable(@) element.
 *
 * This is a complex type.
 */
public class AnswerTableDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AnswerTableDocument
{
    private static final long serialVersionUID = 1L;
    
    public AnswerTableDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ANSWERTABLE$0 = 
        new javax.xml.namespace.QName("", "answerTable");
    
    
    /**
     * Gets the "answerTable" element
     */
    public AnswerTableDocument.AnswerTable getAnswerTable()
    {
        synchronized (monitor())
        {
            check_orphaned();
            AnswerTableDocument.AnswerTable target = null;
            target = (AnswerTableDocument.AnswerTable)get_store().find_element_user(ANSWERTABLE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "answerTable" element
     */
    public void setAnswerTable(AnswerTableDocument.AnswerTable answerTable)
    {
        generatedSetterHelperImpl(answerTable, ANSWERTABLE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "answerTable" element
     */
    public AnswerTableDocument.AnswerTable addNewAnswerTable()
    {
        synchronized (monitor())
        {
            check_orphaned();
            AnswerTableDocument.AnswerTable target = null;
            target = (AnswerTableDocument.AnswerTable)get_store().add_element_user(ANSWERTABLE$0);
            return target;
        }
    }
    /**
     * An XML answerTable(@).
     *
     * This is a complex type.
     */
    public static class AnswerTableImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AnswerTableDocument.AnswerTable
    {
        private static final long serialVersionUID = 1L;
        
        public AnswerTableImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName DATA$0 = 
            new javax.xml.namespace.QName("", "data");
        private static final javax.xml.namespace.QName FILENAME$2 = 
            new javax.xml.namespace.QName("", "filename");
        private static final javax.xml.namespace.QName SPACE$4 = 
            new javax.xml.namespace.QName("http://www.w3.org/XML/1998/namespace", "space");
        
        
        /**
         * Gets array of all "data" elements
         */
        public DataDocument.Data[] getDataArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(DATA$0, targetList);
                DataDocument.Data[] result = new DataDocument.Data[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "data" element
         */
        public DataDocument.Data getDataArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                DataDocument.Data target = null;
                target = (DataDocument.Data)get_store().find_element_user(DATA$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "data" element
         */
        public int sizeOfDataArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(DATA$0);
            }
        }
        
        /**
         * Sets array of all "data" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setDataArray(DataDocument.Data[] dataArray)
        {
            check_orphaned();
            arraySetterHelper(dataArray, DATA$0);
        }
        
        /**
         * Sets ith "data" element
         */
        public void setDataArray(int i, DataDocument.Data data)
        {
            synchronized (monitor())
            {
                check_orphaned();
                DataDocument.Data target = null;
                target = (DataDocument.Data)get_store().find_element_user(DATA$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(data);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "data" element
         */
        public DataDocument.Data insertNewData(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                DataDocument.Data target = null;
                target = (DataDocument.Data)get_store().insert_element_user(DATA$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "data" element
         */
        public DataDocument.Data addNewData()
        {
            synchronized (monitor())
            {
                check_orphaned();
                DataDocument.Data target = null;
                target = (DataDocument.Data)get_store().add_element_user(DATA$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "data" element
         */
        public void removeData(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(DATA$0, i);
            }
        }
        
        /**
         * Gets the "filename" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType getFilename()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().find_attribute_user(FILENAME$2);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "filename" attribute
         */
        public boolean isSetFilename()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(FILENAME$2) != null;
            }
        }
        
        /**
         * Sets the "filename" attribute
         */
        public void setFilename(org.apache.xmlbeans.XmlAnySimpleType filename)
        {
            generatedSetterHelperImpl(filename, FILENAME$2, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "filename" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType addNewFilename()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().add_attribute_user(FILENAME$2);
                return target;
            }
        }
        
        /**
         * Unsets the "filename" attribute
         */
        public void unsetFilename()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(FILENAME$2);
            }
        }
        
        /**
         * Gets the "space" attribute
         */
        public org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space.Enum getSpace()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(SPACE$4);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_default_attribute_value(SPACE$4);
                }
                if (target == null)
                {
                    return null;
                }
                return (org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space.Enum)target.getEnumValue();
            }
        }
        
        /**
         * Gets (as xml) the "space" attribute
         */
        public org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space xgetSpace()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space target = null;
                target = (org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space)get_store().find_attribute_user(SPACE$4);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space)get_default_attribute_value(SPACE$4);
                }
                return target;
            }
        }
        
        /**
         * True if has "space" attribute
         */
        public boolean isSetSpace()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(SPACE$4) != null;
            }
        }
        
        /**
         * Sets the "space" attribute
         */
        public void setSpace(org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space.Enum space)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(SPACE$4);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(SPACE$4);
                }
                target.setEnumValue(space);
            }
        }
        
        /**
         * Sets (as xml) the "space" attribute
         */
        public void xsetSpace(org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space space)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space target = null;
                target = (org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space)get_store().find_attribute_user(SPACE$4);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute.Space)get_store().add_attribute_user(SPACE$4);
                }
                target.set(space);
            }
        }
        
        /**
         * Unsets the "space" attribute
         */
        public void unsetSpace()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(SPACE$4);
            }
        }
    }
}
