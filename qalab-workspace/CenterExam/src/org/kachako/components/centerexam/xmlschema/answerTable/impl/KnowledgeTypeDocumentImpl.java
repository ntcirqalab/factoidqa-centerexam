/*
 * An XML document type.
 * Localname: knowledge_type
 * Namespace: 
 * Java type: KnowledgeTypeDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.answerTable.impl;

import org.kachako.components.centerexam.xmlschema.answerTable.KnowledgeTypeDocument;

/**
 * A document containing one knowledge_type(@) element.
 *
 * This is a complex type.
 */
public class KnowledgeTypeDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements KnowledgeTypeDocument
{
    private static final long serialVersionUID = 1L;
    
    public KnowledgeTypeDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName KNOWLEDGETYPE$0 = 
        new javax.xml.namespace.QName("", "knowledge_type");
    
    
    /**
     * Gets the "knowledge_type" element
     */
    public java.lang.String getKnowledgeType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(KNOWLEDGETYPE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "knowledge_type" element
     */
    public org.apache.xmlbeans.XmlString xgetKnowledgeType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(KNOWLEDGETYPE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "knowledge_type" element
     */
    public void setKnowledgeType(java.lang.String knowledgeType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(KNOWLEDGETYPE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(KNOWLEDGETYPE$0);
            }
            target.setStringValue(knowledgeType);
        }
    }
    
    /**
     * Sets (as xml) the "knowledge_type" element
     */
    public void xsetKnowledgeType(org.apache.xmlbeans.XmlString knowledgeType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(KNOWLEDGETYPE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(KNOWLEDGETYPE$0);
            }
            target.set(knowledgeType);
        }
    }
}
