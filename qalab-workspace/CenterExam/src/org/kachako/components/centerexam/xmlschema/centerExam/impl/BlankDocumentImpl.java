/*
 * An XML document type.
 * Localname: blank
 * Namespace: 
 * Java type: noNamespace.BlankDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.centerExam.impl;
/**
 * A document containing one blank(@) element.
 *
 * This is a complex type.
 */
public class BlankDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.BlankDocument
{
    private static final long serialVersionUID = 1L;
    
    public BlankDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName BLANK$0 = 
        new javax.xml.namespace.QName("", "blank");
    
    
    /**
     * Gets the "blank" element
     */
    public noNamespace.BlankDocument.Blank getBlank()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.BlankDocument.Blank target = null;
            target = (noNamespace.BlankDocument.Blank)get_store().find_element_user(BLANK$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "blank" element
     */
    public void setBlank(noNamespace.BlankDocument.Blank blank)
    {
        generatedSetterHelperImpl(blank, BLANK$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "blank" element
     */
    public noNamespace.BlankDocument.Blank addNewBlank()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.BlankDocument.Blank target = null;
            target = (noNamespace.BlankDocument.Blank)get_store().add_element_user(BLANK$0);
            return target;
        }
    }
    /**
     * An XML blank(@).
     *
     * This is a complex type.
     */
    public static class BlankImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.BlankDocument.Blank
    {
        private static final long serialVersionUID = 1L;
        
        public BlankImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName GARBLED$0 = 
            new javax.xml.namespace.QName("", "garbled");
        private static final javax.xml.namespace.QName LABEL$2 = 
            new javax.xml.namespace.QName("", "label");
        private static final javax.xml.namespace.QName ANSCOLUMN$4 = 
            new javax.xml.namespace.QName("", "ansColumn");
        private static final javax.xml.namespace.QName MISSING$6 = 
            new javax.xml.namespace.QName("", "missing");
        private static final javax.xml.namespace.QName BR$8 = 
            new javax.xml.namespace.QName("", "br");
        private static final javax.xml.namespace.QName ID$10 = 
            new javax.xml.namespace.QName("", "id");
        private static final javax.xml.namespace.QName DIGITS$12 = 
            new javax.xml.namespace.QName("", "digits");
        
        
        /**
         * Gets array of all "garbled" elements
         */
        public noNamespace.GarbledDocument.Garbled[] getGarbledArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(GARBLED$0, targetList);
                noNamespace.GarbledDocument.Garbled[] result = new noNamespace.GarbledDocument.Garbled[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled getGarbledArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().find_element_user(GARBLED$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "garbled" element
         */
        public int sizeOfGarbledArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(GARBLED$0);
            }
        }
        
        /**
         * Sets array of all "garbled" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setGarbledArray(noNamespace.GarbledDocument.Garbled[] garbledArray)
        {
            check_orphaned();
            arraySetterHelper(garbledArray, GARBLED$0);
        }
        
        /**
         * Sets ith "garbled" element
         */
        public void setGarbledArray(int i, noNamespace.GarbledDocument.Garbled garbled)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().find_element_user(GARBLED$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(garbled);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled insertNewGarbled(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().insert_element_user(GARBLED$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled addNewGarbled()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().add_element_user(GARBLED$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "garbled" element
         */
        public void removeGarbled(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(GARBLED$0, i);
            }
        }
        
        /**
         * Gets array of all "label" elements
         */
        public noNamespace.LabelDocument.Label[] getLabelArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(LABEL$2, targetList);
                noNamespace.LabelDocument.Label[] result = new noNamespace.LabelDocument.Label[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "label" element
         */
        public noNamespace.LabelDocument.Label getLabelArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().find_element_user(LABEL$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "label" element
         */
        public int sizeOfLabelArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(LABEL$2);
            }
        }
        
        /**
         * Sets array of all "label" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setLabelArray(noNamespace.LabelDocument.Label[] labelArray)
        {
            check_orphaned();
            arraySetterHelper(labelArray, LABEL$2);
        }
        
        /**
         * Sets ith "label" element
         */
        public void setLabelArray(int i, noNamespace.LabelDocument.Label label)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().find_element_user(LABEL$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(label);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "label" element
         */
        public noNamespace.LabelDocument.Label insertNewLabel(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().insert_element_user(LABEL$2, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "label" element
         */
        public noNamespace.LabelDocument.Label addNewLabel()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().add_element_user(LABEL$2);
                return target;
            }
        }
        
        /**
         * Removes the ith "label" element
         */
        public void removeLabel(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(LABEL$2, i);
            }
        }
        
        /**
         * Gets array of all "ansColumn" elements
         */
        public noNamespace.AnsColumnDocument.AnsColumn[] getAnsColumnArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(ANSCOLUMN$4, targetList);
                noNamespace.AnsColumnDocument.AnsColumn[] result = new noNamespace.AnsColumnDocument.AnsColumn[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "ansColumn" element
         */
        public noNamespace.AnsColumnDocument.AnsColumn getAnsColumnArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.AnsColumnDocument.AnsColumn target = null;
                target = (noNamespace.AnsColumnDocument.AnsColumn)get_store().find_element_user(ANSCOLUMN$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "ansColumn" element
         */
        public int sizeOfAnsColumnArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ANSCOLUMN$4);
            }
        }
        
        /**
         * Sets array of all "ansColumn" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setAnsColumnArray(noNamespace.AnsColumnDocument.AnsColumn[] ansColumnArray)
        {
            check_orphaned();
            arraySetterHelper(ansColumnArray, ANSCOLUMN$4);
        }
        
        /**
         * Sets ith "ansColumn" element
         */
        public void setAnsColumnArray(int i, noNamespace.AnsColumnDocument.AnsColumn ansColumn)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.AnsColumnDocument.AnsColumn target = null;
                target = (noNamespace.AnsColumnDocument.AnsColumn)get_store().find_element_user(ANSCOLUMN$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(ansColumn);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ansColumn" element
         */
        public noNamespace.AnsColumnDocument.AnsColumn insertNewAnsColumn(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.AnsColumnDocument.AnsColumn target = null;
                target = (noNamespace.AnsColumnDocument.AnsColumn)get_store().insert_element_user(ANSCOLUMN$4, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ansColumn" element
         */
        public noNamespace.AnsColumnDocument.AnsColumn addNewAnsColumn()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.AnsColumnDocument.AnsColumn target = null;
                target = (noNamespace.AnsColumnDocument.AnsColumn)get_store().add_element_user(ANSCOLUMN$4);
                return target;
            }
        }
        
        /**
         * Removes the ith "ansColumn" element
         */
        public void removeAnsColumn(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ANSCOLUMN$4, i);
            }
        }
        
        /**
         * Gets array of all "missing" elements
         */
        public noNamespace.MissingDocument.Missing[] getMissingArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(MISSING$6, targetList);
                noNamespace.MissingDocument.Missing[] result = new noNamespace.MissingDocument.Missing[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "missing" element
         */
        public noNamespace.MissingDocument.Missing getMissingArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().find_element_user(MISSING$6, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "missing" element
         */
        public int sizeOfMissingArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(MISSING$6);
            }
        }
        
        /**
         * Sets array of all "missing" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setMissingArray(noNamespace.MissingDocument.Missing[] missingArray)
        {
            check_orphaned();
            arraySetterHelper(missingArray, MISSING$6);
        }
        
        /**
         * Sets ith "missing" element
         */
        public void setMissingArray(int i, noNamespace.MissingDocument.Missing missing)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().find_element_user(MISSING$6, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(missing);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "missing" element
         */
        public noNamespace.MissingDocument.Missing insertNewMissing(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().insert_element_user(MISSING$6, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "missing" element
         */
        public noNamespace.MissingDocument.Missing addNewMissing()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().add_element_user(MISSING$6);
                return target;
            }
        }
        
        /**
         * Removes the ith "missing" element
         */
        public void removeMissing(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(MISSING$6, i);
            }
        }
        
        /**
         * Gets array of all "br" elements
         */
        public noNamespace.BrDocument.Br[] getBrArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(BR$8, targetList);
                noNamespace.BrDocument.Br[] result = new noNamespace.BrDocument.Br[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "br" element
         */
        public noNamespace.BrDocument.Br getBrArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().find_element_user(BR$8, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "br" element
         */
        public int sizeOfBrArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(BR$8);
            }
        }
        
        /**
         * Sets array of all "br" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setBrArray(noNamespace.BrDocument.Br[] brArray)
        {
            check_orphaned();
            arraySetterHelper(brArray, BR$8);
        }
        
        /**
         * Sets ith "br" element
         */
        public void setBrArray(int i, noNamespace.BrDocument.Br br)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().find_element_user(BR$8, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(br);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "br" element
         */
        public noNamespace.BrDocument.Br insertNewBr(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().insert_element_user(BR$8, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "br" element
         */
        public noNamespace.BrDocument.Br addNewBr()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().add_element_user(BR$8);
                return target;
            }
        }
        
        /**
         * Removes the ith "br" element
         */
        public void removeBr(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(BR$8, i);
            }
        }
        
        /**
         * Gets the "id" attribute
         */
        public java.lang.String getId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(ID$10);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "id" attribute
         */
        public org.apache.xmlbeans.XmlID xgetId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlID target = null;
                target = (org.apache.xmlbeans.XmlID)get_store().find_attribute_user(ID$10);
                return target;
            }
        }
        
        /**
         * Sets the "id" attribute
         */
        public void setId(java.lang.String id)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(ID$10);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(ID$10);
                }
                target.setStringValue(id);
            }
        }
        
        /**
         * Sets (as xml) the "id" attribute
         */
        public void xsetId(org.apache.xmlbeans.XmlID id)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlID target = null;
                target = (org.apache.xmlbeans.XmlID)get_store().find_attribute_user(ID$10);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlID)get_store().add_attribute_user(ID$10);
                }
                target.set(id);
            }
        }
        
        /**
         * Gets the "digits" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType getDigits()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().find_attribute_user(DIGITS$12);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "digits" attribute
         */
        public boolean isSetDigits()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(DIGITS$12) != null;
            }
        }
        
        /**
         * Sets the "digits" attribute
         */
        public void setDigits(org.apache.xmlbeans.XmlAnySimpleType digits)
        {
            generatedSetterHelperImpl(digits, DIGITS$12, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "digits" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType addNewDigits()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().add_attribute_user(DIGITS$12);
                return target;
            }
        }
        
        /**
         * Unsets the "digits" attribute
         */
        public void unsetDigits()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(DIGITS$12);
            }
        }
    }
}
