/*
 * An XML document type.
 * Localname: answer_type
 * Namespace: 
 * Java type: AnswerTypeDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.answerTable.impl;

import org.kachako.components.centerexam.xmlschema.answerTable.AnswerTypeDocument;

/**
 * A document containing one answer_type(@) element.
 *
 * This is a complex type.
 */
public class AnswerTypeDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AnswerTypeDocument
{
    private static final long serialVersionUID = 1L;
    
    public AnswerTypeDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ANSWERTYPE$0 = 
        new javax.xml.namespace.QName("", "answer_type");
    
    
    /**
     * Gets the "answer_type" element
     */
    public java.lang.String getAnswerType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ANSWERTYPE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "answer_type" element
     */
    public org.apache.xmlbeans.XmlString xgetAnswerType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ANSWERTYPE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "answer_type" element
     */
    public void setAnswerType(java.lang.String answerType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ANSWERTYPE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ANSWERTYPE$0);
            }
            target.setStringValue(answerType);
        }
    }
    
    /**
     * Sets (as xml) the "answer_type" element
     */
    public void xsetAnswerType(org.apache.xmlbeans.XmlString answerType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ANSWERTYPE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(ANSWERTYPE$0);
            }
            target.set(answerType);
        }
    }
}
