/*
 * An XML document type.
 * Localname: blank
 * Namespace: 
 * Java type: noNamespace.BlankDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.centerExam;


/**
 * A document containing one blank(@) element.
 *
 * This is a complex type.
 */
public interface BlankDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(BlankDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s9AD9431611720D569E6CDA08F9EDE660").resolveHandle("blankec7bdoctype");
    
    /**
     * Gets the "blank" element
     */
    noNamespace.BlankDocument.Blank getBlank();
    
    /**
     * Sets the "blank" element
     */
    void setBlank(noNamespace.BlankDocument.Blank blank);
    
    /**
     * Appends and returns a new empty "blank" element
     */
    noNamespace.BlankDocument.Blank addNewBlank();
    
    /**
     * An XML blank(@).
     *
     * This is a complex type.
     */
    public interface Blank extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Blank.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s9AD9431611720D569E6CDA08F9EDE660").resolveHandle("blankf84belemtype");
        
        /**
         * Gets array of all "garbled" elements
         */
        noNamespace.GarbledDocument.Garbled[] getGarbledArray();
        
        /**
         * Gets ith "garbled" element
         */
        noNamespace.GarbledDocument.Garbled getGarbledArray(int i);
        
        /**
         * Returns number of "garbled" element
         */
        int sizeOfGarbledArray();
        
        /**
         * Sets array of all "garbled" element
         */
        void setGarbledArray(noNamespace.GarbledDocument.Garbled[] garbledArray);
        
        /**
         * Sets ith "garbled" element
         */
        void setGarbledArray(int i, noNamespace.GarbledDocument.Garbled garbled);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "garbled" element
         */
        noNamespace.GarbledDocument.Garbled insertNewGarbled(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "garbled" element
         */
        noNamespace.GarbledDocument.Garbled addNewGarbled();
        
        /**
         * Removes the ith "garbled" element
         */
        void removeGarbled(int i);
        
        /**
         * Gets array of all "label" elements
         */
        noNamespace.LabelDocument.Label[] getLabelArray();
        
        /**
         * Gets ith "label" element
         */
        noNamespace.LabelDocument.Label getLabelArray(int i);
        
        /**
         * Returns number of "label" element
         */
        int sizeOfLabelArray();
        
        /**
         * Sets array of all "label" element
         */
        void setLabelArray(noNamespace.LabelDocument.Label[] labelArray);
        
        /**
         * Sets ith "label" element
         */
        void setLabelArray(int i, noNamespace.LabelDocument.Label label);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "label" element
         */
        noNamespace.LabelDocument.Label insertNewLabel(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "label" element
         */
        noNamespace.LabelDocument.Label addNewLabel();
        
        /**
         * Removes the ith "label" element
         */
        void removeLabel(int i);
        
        /**
         * Gets array of all "ansColumn" elements
         */
        noNamespace.AnsColumnDocument.AnsColumn[] getAnsColumnArray();
        
        /**
         * Gets ith "ansColumn" element
         */
        noNamespace.AnsColumnDocument.AnsColumn getAnsColumnArray(int i);
        
        /**
         * Returns number of "ansColumn" element
         */
        int sizeOfAnsColumnArray();
        
        /**
         * Sets array of all "ansColumn" element
         */
        void setAnsColumnArray(noNamespace.AnsColumnDocument.AnsColumn[] ansColumnArray);
        
        /**
         * Sets ith "ansColumn" element
         */
        void setAnsColumnArray(int i, noNamespace.AnsColumnDocument.AnsColumn ansColumn);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ansColumn" element
         */
        noNamespace.AnsColumnDocument.AnsColumn insertNewAnsColumn(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ansColumn" element
         */
        noNamespace.AnsColumnDocument.AnsColumn addNewAnsColumn();
        
        /**
         * Removes the ith "ansColumn" element
         */
        void removeAnsColumn(int i);
        
        /**
         * Gets array of all "missing" elements
         */
        noNamespace.MissingDocument.Missing[] getMissingArray();
        
        /**
         * Gets ith "missing" element
         */
        noNamespace.MissingDocument.Missing getMissingArray(int i);
        
        /**
         * Returns number of "missing" element
         */
        int sizeOfMissingArray();
        
        /**
         * Sets array of all "missing" element
         */
        void setMissingArray(noNamespace.MissingDocument.Missing[] missingArray);
        
        /**
         * Sets ith "missing" element
         */
        void setMissingArray(int i, noNamespace.MissingDocument.Missing missing);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "missing" element
         */
        noNamespace.MissingDocument.Missing insertNewMissing(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "missing" element
         */
        noNamespace.MissingDocument.Missing addNewMissing();
        
        /**
         * Removes the ith "missing" element
         */
        void removeMissing(int i);
        
        /**
         * Gets array of all "br" elements
         */
        noNamespace.BrDocument.Br[] getBrArray();
        
        /**
         * Gets ith "br" element
         */
        noNamespace.BrDocument.Br getBrArray(int i);
        
        /**
         * Returns number of "br" element
         */
        int sizeOfBrArray();
        
        /**
         * Sets array of all "br" element
         */
        void setBrArray(noNamespace.BrDocument.Br[] brArray);
        
        /**
         * Sets ith "br" element
         */
        void setBrArray(int i, noNamespace.BrDocument.Br br);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "br" element
         */
        noNamespace.BrDocument.Br insertNewBr(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "br" element
         */
        noNamespace.BrDocument.Br addNewBr();
        
        /**
         * Removes the ith "br" element
         */
        void removeBr(int i);
        
        /**
         * Gets the "id" attribute
         */
        java.lang.String getId();
        
        /**
         * Gets (as xml) the "id" attribute
         */
        org.apache.xmlbeans.XmlID xgetId();
        
        /**
         * Sets the "id" attribute
         */
        void setId(java.lang.String id);
        
        /**
         * Sets (as xml) the "id" attribute
         */
        void xsetId(org.apache.xmlbeans.XmlID id);
        
        /**
         * Gets the "digits" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType getDigits();
        
        /**
         * True if has "digits" attribute
         */
        boolean isSetDigits();
        
        /**
         * Sets the "digits" attribute
         */
        void setDigits(org.apache.xmlbeans.XmlAnySimpleType digits);
        
        /**
         * Appends and returns a new empty "digits" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType addNewDigits();
        
        /**
         * Unsets the "digits" attribute
         */
        void unsetDigits();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static noNamespace.BlankDocument.Blank newInstance() {
              return (noNamespace.BlankDocument.Blank) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static noNamespace.BlankDocument.Blank newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (noNamespace.BlankDocument.Blank) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static noNamespace.BlankDocument newInstance() {
          return (noNamespace.BlankDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static noNamespace.BlankDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (noNamespace.BlankDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static noNamespace.BlankDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.BlankDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static noNamespace.BlankDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.BlankDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static noNamespace.BlankDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.BlankDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static noNamespace.BlankDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.BlankDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static noNamespace.BlankDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.BlankDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static noNamespace.BlankDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.BlankDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static noNamespace.BlankDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.BlankDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static noNamespace.BlankDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.BlankDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static noNamespace.BlankDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.BlankDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static noNamespace.BlankDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.BlankDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static noNamespace.BlankDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.BlankDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static noNamespace.BlankDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.BlankDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static noNamespace.BlankDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.BlankDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static noNamespace.BlankDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.BlankDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static noNamespace.BlankDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (noNamespace.BlankDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static noNamespace.BlankDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (noNamespace.BlankDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
