/*
 * An XML document type.
 * Localname: score
 * Namespace: 
 * Java type: ScoreDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.answerTable.impl;

import org.kachako.components.centerexam.xmlschema.answerTable.ScoreDocument;



/**
 * A document containing one score(@) element.
 *
 * This is a complex type.
 */
public class ScoreDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements ScoreDocument
{
    private static final long serialVersionUID = 1L;
    
    public ScoreDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SCORE$0 = 
        new javax.xml.namespace.QName("", "score");
    
    
    /**
     * Gets the "score" element
     */
    public java.lang.String getScore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCORE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "score" element
     */
    public org.apache.xmlbeans.XmlString xgetScore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SCORE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "score" element
     */
    public void setScore(java.lang.String score)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCORE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SCORE$0);
            }
            target.setStringValue(score);
        }
    }
    
    /**
     * Sets (as xml) the "score" element
     */
    public void xsetScore(org.apache.xmlbeans.XmlString score)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SCORE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(SCORE$0);
            }
            target.set(score);
        }
    }
}
