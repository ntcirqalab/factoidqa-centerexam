/*
 * An XML document type.
 * Localname: data
 * Namespace: 
 * Java type: DataDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.answerTable.impl;

import org.kachako.components.centerexam.xmlschema.answerTable.DataDocument;

/**
 * A document containing one data(@) element.
 *
 * This is a complex type.
 */
public class DataDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements DataDocument
{
    private static final long serialVersionUID = 1L;
    
    public DataDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DATA$0 = 
        new javax.xml.namespace.QName("", "data");
    
    
    /**
     * Gets the "data" element
     */
    public DataDocument.Data getData()
    {
        synchronized (monitor())
        {
            check_orphaned();
            DataDocument.Data target = null;
            target = (DataDocument.Data)get_store().find_element_user(DATA$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "data" element
     */
    public void setData(DataDocument.Data data)
    {
        generatedSetterHelperImpl(data, DATA$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "data" element
     */
    public DataDocument.Data addNewData()
    {
        synchronized (monitor())
        {
            check_orphaned();
            DataDocument.Data target = null;
            target = (DataDocument.Data)get_store().add_element_user(DATA$0);
            return target;
        }
    }
    /**
     * An XML data(@).
     *
     * This is a complex type.
     */
    public static class DataImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements DataDocument.Data
    {
        private static final long serialVersionUID = 1L;
        
        public DataImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName SECTION$0 = 
            new javax.xml.namespace.QName("", "section");
        private static final javax.xml.namespace.QName QUESTION$2 = 
            new javax.xml.namespace.QName("", "question");
        private static final javax.xml.namespace.QName ANSWERCOLUMN$4 = 
            new javax.xml.namespace.QName("", "answer_column");
        private static final javax.xml.namespace.QName ANSWER$6 = 
            new javax.xml.namespace.QName("", "answer");
        private static final javax.xml.namespace.QName SCORE$8 = 
            new javax.xml.namespace.QName("", "score");
        private static final javax.xml.namespace.QName ANSWERSTYLE$10 = 
            new javax.xml.namespace.QName("", "answer_style");
        private static final javax.xml.namespace.QName ANSWERTYPE$12 = 
            new javax.xml.namespace.QName("", "answer_type");
        private static final javax.xml.namespace.QName KNOWLEDGETYPE$14 = 
            new javax.xml.namespace.QName("", "knowledge_type");
        private static final javax.xml.namespace.QName QUESTIONID$16 = 
            new javax.xml.namespace.QName("", "question_ID");
        private static final javax.xml.namespace.QName ANSCOLUMNID$18 = 
            new javax.xml.namespace.QName("", "anscolumn_ID");
        private static final javax.xml.namespace.QName PROCESSLOG$20 = 
            new javax.xml.namespace.QName("", "process_log");
        
        
        /**
         * Gets array of all "section" elements
         */
        public java.lang.String[] getSectionArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(SECTION$0, targetList);
                java.lang.String[] result = new java.lang.String[targetList.size()];
                for (int i = 0, len = targetList.size() ; i < len ; i++)
                    result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
                return result;
            }
        }
        
        /**
         * Gets ith "section" element
         */
        public java.lang.String getSectionArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SECTION$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) array of all "section" elements
         */
        public org.apache.xmlbeans.XmlString[] xgetSectionArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(SECTION$0, targetList);
                org.apache.xmlbeans.XmlString[] result = new org.apache.xmlbeans.XmlString[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets (as xml) ith "section" element
         */
        public org.apache.xmlbeans.XmlString xgetSectionArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SECTION$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "section" element
         */
        public int sizeOfSectionArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(SECTION$0);
            }
        }
        
        /**
         * Sets array of all "section" element
         */
        public void setSectionArray(java.lang.String[] sectionArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(sectionArray, SECTION$0);
            }
        }
        
        /**
         * Sets ith "section" element
         */
        public void setSectionArray(int i, java.lang.String section)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SECTION$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.setStringValue(section);
            }
        }
        
        /**
         * Sets (as xml) array of all "section" element
         */
        public void xsetSectionArray(org.apache.xmlbeans.XmlString[]sectionArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(sectionArray, SECTION$0);
            }
        }
        
        /**
         * Sets (as xml) ith "section" element
         */
        public void xsetSectionArray(int i, org.apache.xmlbeans.XmlString section)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SECTION$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(section);
            }
        }
        
        /**
         * Inserts the value as the ith "section" element
         */
        public void insertSection(int i, java.lang.String section)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = 
                    (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(SECTION$0, i);
                target.setStringValue(section);
            }
        }
        
        /**
         * Appends the value as the last "section" element
         */
        public void addSection(java.lang.String section)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SECTION$0);
                target.setStringValue(section);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "section" element
         */
        public org.apache.xmlbeans.XmlString insertNewSection(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().insert_element_user(SECTION$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "section" element
         */
        public org.apache.xmlbeans.XmlString addNewSection()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(SECTION$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "section" element
         */
        public void removeSection(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(SECTION$0, i);
            }
        }
        
        /**
         * Gets array of all "question" elements
         */
        public java.lang.String[] getQuestionArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(QUESTION$2, targetList);
                java.lang.String[] result = new java.lang.String[targetList.size()];
                for (int i = 0, len = targetList.size() ; i < len ; i++)
                    result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
                return result;
            }
        }
        
        /**
         * Gets ith "question" element
         */
        public java.lang.String getQuestionArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUESTION$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) array of all "question" elements
         */
        public org.apache.xmlbeans.XmlString[] xgetQuestionArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(QUESTION$2, targetList);
                org.apache.xmlbeans.XmlString[] result = new org.apache.xmlbeans.XmlString[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets (as xml) ith "question" element
         */
        public org.apache.xmlbeans.XmlString xgetQuestionArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(QUESTION$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "question" element
         */
        public int sizeOfQuestionArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(QUESTION$2);
            }
        }
        
        /**
         * Sets array of all "question" element
         */
        public void setQuestionArray(java.lang.String[] questionArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(questionArray, QUESTION$2);
            }
        }
        
        /**
         * Sets ith "question" element
         */
        public void setQuestionArray(int i, java.lang.String question)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUESTION$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.setStringValue(question);
            }
        }
        
        /**
         * Sets (as xml) array of all "question" element
         */
        public void xsetQuestionArray(org.apache.xmlbeans.XmlString[]questionArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(questionArray, QUESTION$2);
            }
        }
        
        /**
         * Sets (as xml) ith "question" element
         */
        public void xsetQuestionArray(int i, org.apache.xmlbeans.XmlString question)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(QUESTION$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(question);
            }
        }
        
        /**
         * Inserts the value as the ith "question" element
         */
        public void insertQuestion(int i, java.lang.String question)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = 
                    (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(QUESTION$2, i);
                target.setStringValue(question);
            }
        }
        
        /**
         * Appends the value as the last "question" element
         */
        public void addQuestion(java.lang.String question)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(QUESTION$2);
                target.setStringValue(question);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "question" element
         */
        public org.apache.xmlbeans.XmlString insertNewQuestion(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().insert_element_user(QUESTION$2, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "question" element
         */
        public org.apache.xmlbeans.XmlString addNewQuestion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(QUESTION$2);
                return target;
            }
        }
        
        /**
         * Removes the ith "question" element
         */
        public void removeQuestion(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(QUESTION$2, i);
            }
        }
        
        /**
         * Gets array of all "answer_column" elements
         */
        public java.lang.String[] getAnswerColumnArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(ANSWERCOLUMN$4, targetList);
                java.lang.String[] result = new java.lang.String[targetList.size()];
                for (int i = 0, len = targetList.size() ; i < len ; i++)
                    result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
                return result;
            }
        }
        
        /**
         * Gets ith "answer_column" element
         */
        public java.lang.String getAnswerColumnArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ANSWERCOLUMN$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) array of all "answer_column" elements
         */
        public org.apache.xmlbeans.XmlString[] xgetAnswerColumnArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(ANSWERCOLUMN$4, targetList);
                org.apache.xmlbeans.XmlString[] result = new org.apache.xmlbeans.XmlString[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets (as xml) ith "answer_column" element
         */
        public org.apache.xmlbeans.XmlString xgetAnswerColumnArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ANSWERCOLUMN$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "answer_column" element
         */
        public int sizeOfAnswerColumnArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ANSWERCOLUMN$4);
            }
        }
        
        /**
         * Sets array of all "answer_column" element
         */
        public void setAnswerColumnArray(java.lang.String[] answerColumnArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(answerColumnArray, ANSWERCOLUMN$4);
            }
        }
        
        /**
         * Sets ith "answer_column" element
         */
        public void setAnswerColumnArray(int i, java.lang.String answerColumn)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ANSWERCOLUMN$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.setStringValue(answerColumn);
            }
        }
        
        /**
         * Sets (as xml) array of all "answer_column" element
         */
        public void xsetAnswerColumnArray(org.apache.xmlbeans.XmlString[]answerColumnArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(answerColumnArray, ANSWERCOLUMN$4);
            }
        }
        
        /**
         * Sets (as xml) ith "answer_column" element
         */
        public void xsetAnswerColumnArray(int i, org.apache.xmlbeans.XmlString answerColumn)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ANSWERCOLUMN$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(answerColumn);
            }
        }
        
        /**
         * Inserts the value as the ith "answer_column" element
         */
        public void insertAnswerColumn(int i, java.lang.String answerColumn)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = 
                    (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(ANSWERCOLUMN$4, i);
                target.setStringValue(answerColumn);
            }
        }
        
        /**
         * Appends the value as the last "answer_column" element
         */
        public void addAnswerColumn(java.lang.String answerColumn)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ANSWERCOLUMN$4);
                target.setStringValue(answerColumn);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "answer_column" element
         */
        public org.apache.xmlbeans.XmlString insertNewAnswerColumn(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().insert_element_user(ANSWERCOLUMN$4, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "answer_column" element
         */
        public org.apache.xmlbeans.XmlString addNewAnswerColumn()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(ANSWERCOLUMN$4);
                return target;
            }
        }
        
        /**
         * Removes the ith "answer_column" element
         */
        public void removeAnswerColumn(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ANSWERCOLUMN$4, i);
            }
        }
        
        /**
         * Gets array of all "answer" elements
         */
        public java.lang.String[] getAnswerArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(ANSWER$6, targetList);
                java.lang.String[] result = new java.lang.String[targetList.size()];
                for (int i = 0, len = targetList.size() ; i < len ; i++)
                    result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
                return result;
            }
        }
        
        /**
         * Gets ith "answer" element
         */
        public java.lang.String getAnswerArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ANSWER$6, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) array of all "answer" elements
         */
        public org.apache.xmlbeans.XmlString[] xgetAnswerArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(ANSWER$6, targetList);
                org.apache.xmlbeans.XmlString[] result = new org.apache.xmlbeans.XmlString[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets (as xml) ith "answer" element
         */
        public org.apache.xmlbeans.XmlString xgetAnswerArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ANSWER$6, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "answer" element
         */
        public int sizeOfAnswerArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ANSWER$6);
            }
        }
        
        /**
         * Sets array of all "answer" element
         */
        public void setAnswerArray(java.lang.String[] answerArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(answerArray, ANSWER$6);
            }
        }
        
        /**
         * Sets ith "answer" element
         */
        public void setAnswerArray(int i, java.lang.String answer)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ANSWER$6, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.setStringValue(answer);
            }
        }
        
        /**
         * Sets (as xml) array of all "answer" element
         */
        public void xsetAnswerArray(org.apache.xmlbeans.XmlString[]answerArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(answerArray, ANSWER$6);
            }
        }
        
        /**
         * Sets (as xml) ith "answer" element
         */
        public void xsetAnswerArray(int i, org.apache.xmlbeans.XmlString answer)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ANSWER$6, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(answer);
            }
        }
        
        /**
         * Inserts the value as the ith "answer" element
         */
        public void insertAnswer(int i, java.lang.String answer)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = 
                    (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(ANSWER$6, i);
                target.setStringValue(answer);
            }
        }
        
        /**
         * Appends the value as the last "answer" element
         */
        public void addAnswer(java.lang.String answer)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ANSWER$6);
                target.setStringValue(answer);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "answer" element
         */
        public org.apache.xmlbeans.XmlString insertNewAnswer(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().insert_element_user(ANSWER$6, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "answer" element
         */
        public org.apache.xmlbeans.XmlString addNewAnswer()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(ANSWER$6);
                return target;
            }
        }
        
        /**
         * Removes the ith "answer" element
         */
        public void removeAnswer(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ANSWER$6, i);
            }
        }
        
        /**
         * Gets array of all "score" elements
         */
        public java.lang.String[] getScoreArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(SCORE$8, targetList);
                java.lang.String[] result = new java.lang.String[targetList.size()];
                for (int i = 0, len = targetList.size() ; i < len ; i++)
                    result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
                return result;
            }
        }
        
        /**
         * Gets ith "score" element
         */
        public java.lang.String getScoreArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCORE$8, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) array of all "score" elements
         */
        public org.apache.xmlbeans.XmlString[] xgetScoreArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(SCORE$8, targetList);
                org.apache.xmlbeans.XmlString[] result = new org.apache.xmlbeans.XmlString[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets (as xml) ith "score" element
         */
        public org.apache.xmlbeans.XmlString xgetScoreArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SCORE$8, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "score" element
         */
        public int sizeOfScoreArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(SCORE$8);
            }
        }
        
        /**
         * Sets array of all "score" element
         */
        public void setScoreArray(java.lang.String[] scoreArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(scoreArray, SCORE$8);
            }
        }
        
        /**
         * Sets ith "score" element
         */
        public void setScoreArray(int i, java.lang.String score)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCORE$8, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.setStringValue(score);
            }
        }
        
        /**
         * Sets (as xml) array of all "score" element
         */
        public void xsetScoreArray(org.apache.xmlbeans.XmlString[]scoreArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(scoreArray, SCORE$8);
            }
        }
        
        /**
         * Sets (as xml) ith "score" element
         */
        public void xsetScoreArray(int i, org.apache.xmlbeans.XmlString score)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SCORE$8, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(score);
            }
        }
        
        /**
         * Inserts the value as the ith "score" element
         */
        public void insertScore(int i, java.lang.String score)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = 
                    (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(SCORE$8, i);
                target.setStringValue(score);
            }
        }
        
        /**
         * Appends the value as the last "score" element
         */
        public void addScore(java.lang.String score)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SCORE$8);
                target.setStringValue(score);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "score" element
         */
        public org.apache.xmlbeans.XmlString insertNewScore(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().insert_element_user(SCORE$8, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "score" element
         */
        public org.apache.xmlbeans.XmlString addNewScore()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(SCORE$8);
                return target;
            }
        }
        
        /**
         * Removes the ith "score" element
         */
        public void removeScore(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(SCORE$8, i);
            }
        }
        
        /**
         * Gets array of all "answer_style" elements
         */
        public java.lang.String[] getAnswerStyleArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(ANSWERSTYLE$10, targetList);
                java.lang.String[] result = new java.lang.String[targetList.size()];
                for (int i = 0, len = targetList.size() ; i < len ; i++)
                    result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
                return result;
            }
        }
        
        /**
         * Gets ith "answer_style" element
         */
        public java.lang.String getAnswerStyleArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ANSWERSTYLE$10, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) array of all "answer_style" elements
         */
        public org.apache.xmlbeans.XmlString[] xgetAnswerStyleArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(ANSWERSTYLE$10, targetList);
                org.apache.xmlbeans.XmlString[] result = new org.apache.xmlbeans.XmlString[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets (as xml) ith "answer_style" element
         */
        public org.apache.xmlbeans.XmlString xgetAnswerStyleArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ANSWERSTYLE$10, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "answer_style" element
         */
        public int sizeOfAnswerStyleArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ANSWERSTYLE$10);
            }
        }
        
        /**
         * Sets array of all "answer_style" element
         */
        public void setAnswerStyleArray(java.lang.String[] answerStyleArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(answerStyleArray, ANSWERSTYLE$10);
            }
        }
        
        /**
         * Sets ith "answer_style" element
         */
        public void setAnswerStyleArray(int i, java.lang.String answerStyle)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ANSWERSTYLE$10, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.setStringValue(answerStyle);
            }
        }
        
        /**
         * Sets (as xml) array of all "answer_style" element
         */
        public void xsetAnswerStyleArray(org.apache.xmlbeans.XmlString[]answerStyleArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(answerStyleArray, ANSWERSTYLE$10);
            }
        }
        
        /**
         * Sets (as xml) ith "answer_style" element
         */
        public void xsetAnswerStyleArray(int i, org.apache.xmlbeans.XmlString answerStyle)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ANSWERSTYLE$10, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(answerStyle);
            }
        }
        
        /**
         * Inserts the value as the ith "answer_style" element
         */
        public void insertAnswerStyle(int i, java.lang.String answerStyle)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = 
                    (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(ANSWERSTYLE$10, i);
                target.setStringValue(answerStyle);
            }
        }
        
        /**
         * Appends the value as the last "answer_style" element
         */
        public void addAnswerStyle(java.lang.String answerStyle)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ANSWERSTYLE$10);
                target.setStringValue(answerStyle);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "answer_style" element
         */
        public org.apache.xmlbeans.XmlString insertNewAnswerStyle(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().insert_element_user(ANSWERSTYLE$10, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "answer_style" element
         */
        public org.apache.xmlbeans.XmlString addNewAnswerStyle()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(ANSWERSTYLE$10);
                return target;
            }
        }
        
        /**
         * Removes the ith "answer_style" element
         */
        public void removeAnswerStyle(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ANSWERSTYLE$10, i);
            }
        }
        
        /**
         * Gets array of all "answer_type" elements
         */
        public java.lang.String[] getAnswerTypeArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(ANSWERTYPE$12, targetList);
                java.lang.String[] result = new java.lang.String[targetList.size()];
                for (int i = 0, len = targetList.size() ; i < len ; i++)
                    result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
                return result;
            }
        }
        
        /**
         * Gets ith "answer_type" element
         */
        public java.lang.String getAnswerTypeArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ANSWERTYPE$12, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) array of all "answer_type" elements
         */
        public org.apache.xmlbeans.XmlString[] xgetAnswerTypeArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(ANSWERTYPE$12, targetList);
                org.apache.xmlbeans.XmlString[] result = new org.apache.xmlbeans.XmlString[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets (as xml) ith "answer_type" element
         */
        public org.apache.xmlbeans.XmlString xgetAnswerTypeArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ANSWERTYPE$12, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "answer_type" element
         */
        public int sizeOfAnswerTypeArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ANSWERTYPE$12);
            }
        }
        
        /**
         * Sets array of all "answer_type" element
         */
        public void setAnswerTypeArray(java.lang.String[] answerTypeArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(answerTypeArray, ANSWERTYPE$12);
            }
        }
        
        /**
         * Sets ith "answer_type" element
         */
        public void setAnswerTypeArray(int i, java.lang.String answerType)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ANSWERTYPE$12, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.setStringValue(answerType);
            }
        }
        
        /**
         * Sets (as xml) array of all "answer_type" element
         */
        public void xsetAnswerTypeArray(org.apache.xmlbeans.XmlString[]answerTypeArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(answerTypeArray, ANSWERTYPE$12);
            }
        }
        
        /**
         * Sets (as xml) ith "answer_type" element
         */
        public void xsetAnswerTypeArray(int i, org.apache.xmlbeans.XmlString answerType)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ANSWERTYPE$12, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(answerType);
            }
        }
        
        /**
         * Inserts the value as the ith "answer_type" element
         */
        public void insertAnswerType(int i, java.lang.String answerType)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = 
                    (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(ANSWERTYPE$12, i);
                target.setStringValue(answerType);
            }
        }
        
        /**
         * Appends the value as the last "answer_type" element
         */
        public void addAnswerType(java.lang.String answerType)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ANSWERTYPE$12);
                target.setStringValue(answerType);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "answer_type" element
         */
        public org.apache.xmlbeans.XmlString insertNewAnswerType(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().insert_element_user(ANSWERTYPE$12, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "answer_type" element
         */
        public org.apache.xmlbeans.XmlString addNewAnswerType()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(ANSWERTYPE$12);
                return target;
            }
        }
        
        /**
         * Removes the ith "answer_type" element
         */
        public void removeAnswerType(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ANSWERTYPE$12, i);
            }
        }
        
        /**
         * Gets array of all "knowledge_type" elements
         */
        public java.lang.String[] getKnowledgeTypeArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(KNOWLEDGETYPE$14, targetList);
                java.lang.String[] result = new java.lang.String[targetList.size()];
                for (int i = 0, len = targetList.size() ; i < len ; i++)
                    result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
                return result;
            }
        }
        
        /**
         * Gets ith "knowledge_type" element
         */
        public java.lang.String getKnowledgeTypeArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(KNOWLEDGETYPE$14, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) array of all "knowledge_type" elements
         */
        public org.apache.xmlbeans.XmlString[] xgetKnowledgeTypeArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(KNOWLEDGETYPE$14, targetList);
                org.apache.xmlbeans.XmlString[] result = new org.apache.xmlbeans.XmlString[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets (as xml) ith "knowledge_type" element
         */
        public org.apache.xmlbeans.XmlString xgetKnowledgeTypeArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(KNOWLEDGETYPE$14, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "knowledge_type" element
         */
        public int sizeOfKnowledgeTypeArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(KNOWLEDGETYPE$14);
            }
        }
        
        /**
         * Sets array of all "knowledge_type" element
         */
        public void setKnowledgeTypeArray(java.lang.String[] knowledgeTypeArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(knowledgeTypeArray, KNOWLEDGETYPE$14);
            }
        }
        
        /**
         * Sets ith "knowledge_type" element
         */
        public void setKnowledgeTypeArray(int i, java.lang.String knowledgeType)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(KNOWLEDGETYPE$14, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.setStringValue(knowledgeType);
            }
        }
        
        /**
         * Sets (as xml) array of all "knowledge_type" element
         */
        public void xsetKnowledgeTypeArray(org.apache.xmlbeans.XmlString[]knowledgeTypeArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(knowledgeTypeArray, KNOWLEDGETYPE$14);
            }
        }
        
        /**
         * Sets (as xml) ith "knowledge_type" element
         */
        public void xsetKnowledgeTypeArray(int i, org.apache.xmlbeans.XmlString knowledgeType)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(KNOWLEDGETYPE$14, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(knowledgeType);
            }
        }
        
        /**
         * Inserts the value as the ith "knowledge_type" element
         */
        public void insertKnowledgeType(int i, java.lang.String knowledgeType)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = 
                    (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(KNOWLEDGETYPE$14, i);
                target.setStringValue(knowledgeType);
            }
        }
        
        /**
         * Appends the value as the last "knowledge_type" element
         */
        public void addKnowledgeType(java.lang.String knowledgeType)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(KNOWLEDGETYPE$14);
                target.setStringValue(knowledgeType);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "knowledge_type" element
         */
        public org.apache.xmlbeans.XmlString insertNewKnowledgeType(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().insert_element_user(KNOWLEDGETYPE$14, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "knowledge_type" element
         */
        public org.apache.xmlbeans.XmlString addNewKnowledgeType()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(KNOWLEDGETYPE$14);
                return target;
            }
        }
        
        /**
         * Removes the ith "knowledge_type" element
         */
        public void removeKnowledgeType(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(KNOWLEDGETYPE$14, i);
            }
        }
        
        /**
         * Gets array of all "question_ID" elements
         */
        public java.lang.String[] getQuestionIDArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(QUESTIONID$16, targetList);
                java.lang.String[] result = new java.lang.String[targetList.size()];
                for (int i = 0, len = targetList.size() ; i < len ; i++)
                    result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
                return result;
            }
        }
        
        /**
         * Gets ith "question_ID" element
         */
        public java.lang.String getQuestionIDArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUESTIONID$16, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) array of all "question_ID" elements
         */
        public org.apache.xmlbeans.XmlString[] xgetQuestionIDArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(QUESTIONID$16, targetList);
                org.apache.xmlbeans.XmlString[] result = new org.apache.xmlbeans.XmlString[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets (as xml) ith "question_ID" element
         */
        public org.apache.xmlbeans.XmlString xgetQuestionIDArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(QUESTIONID$16, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "question_ID" element
         */
        public int sizeOfQuestionIDArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(QUESTIONID$16);
            }
        }
        
        /**
         * Sets array of all "question_ID" element
         */
        public void setQuestionIDArray(java.lang.String[] questionIDArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(questionIDArray, QUESTIONID$16);
            }
        }
        
        /**
         * Sets ith "question_ID" element
         */
        public void setQuestionIDArray(int i, java.lang.String questionID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUESTIONID$16, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.setStringValue(questionID);
            }
        }
        
        /**
         * Sets (as xml) array of all "question_ID" element
         */
        public void xsetQuestionIDArray(org.apache.xmlbeans.XmlString[]questionIDArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(questionIDArray, QUESTIONID$16);
            }
        }
        
        /**
         * Sets (as xml) ith "question_ID" element
         */
        public void xsetQuestionIDArray(int i, org.apache.xmlbeans.XmlString questionID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(QUESTIONID$16, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(questionID);
            }
        }
        
        /**
         * Inserts the value as the ith "question_ID" element
         */
        public void insertQuestionID(int i, java.lang.String questionID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = 
                    (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(QUESTIONID$16, i);
                target.setStringValue(questionID);
            }
        }
        
        /**
         * Appends the value as the last "question_ID" element
         */
        public void addQuestionID(java.lang.String questionID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(QUESTIONID$16);
                target.setStringValue(questionID);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "question_ID" element
         */
        public org.apache.xmlbeans.XmlString insertNewQuestionID(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().insert_element_user(QUESTIONID$16, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "question_ID" element
         */
        public org.apache.xmlbeans.XmlString addNewQuestionID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(QUESTIONID$16);
                return target;
            }
        }
        
        /**
         * Removes the ith "question_ID" element
         */
        public void removeQuestionID(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(QUESTIONID$16, i);
            }
        }
        
        /**
         * Gets array of all "anscolumn_ID" elements
         */
        public java.lang.String[] getAnscolumnIDArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(ANSCOLUMNID$18, targetList);
                java.lang.String[] result = new java.lang.String[targetList.size()];
                for (int i = 0, len = targetList.size() ; i < len ; i++)
                    result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
                return result;
            }
        }
        
        /**
         * Gets ith "anscolumn_ID" element
         */
        public java.lang.String getAnscolumnIDArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ANSCOLUMNID$18, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) array of all "anscolumn_ID" elements
         */
        public org.apache.xmlbeans.XmlString[] xgetAnscolumnIDArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(ANSCOLUMNID$18, targetList);
                org.apache.xmlbeans.XmlString[] result = new org.apache.xmlbeans.XmlString[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets (as xml) ith "anscolumn_ID" element
         */
        public org.apache.xmlbeans.XmlString xgetAnscolumnIDArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ANSCOLUMNID$18, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "anscolumn_ID" element
         */
        public int sizeOfAnscolumnIDArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ANSCOLUMNID$18);
            }
        }
        
        /**
         * Sets array of all "anscolumn_ID" element
         */
        public void setAnscolumnIDArray(java.lang.String[] anscolumnIDArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(anscolumnIDArray, ANSCOLUMNID$18);
            }
        }
        
        /**
         * Sets ith "anscolumn_ID" element
         */
        public void setAnscolumnIDArray(int i, java.lang.String anscolumnID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ANSCOLUMNID$18, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.setStringValue(anscolumnID);
            }
        }
        
        /**
         * Sets (as xml) array of all "anscolumn_ID" element
         */
        public void xsetAnscolumnIDArray(org.apache.xmlbeans.XmlString[]anscolumnIDArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(anscolumnIDArray, ANSCOLUMNID$18);
            }
        }
        
        /**
         * Sets (as xml) ith "anscolumn_ID" element
         */
        public void xsetAnscolumnIDArray(int i, org.apache.xmlbeans.XmlString anscolumnID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ANSCOLUMNID$18, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(anscolumnID);
            }
        }
        
        /**
         * Inserts the value as the ith "anscolumn_ID" element
         */
        public void insertAnscolumnID(int i, java.lang.String anscolumnID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = 
                    (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(ANSCOLUMNID$18, i);
                target.setStringValue(anscolumnID);
            }
        }
        
        /**
         * Appends the value as the last "anscolumn_ID" element
         */
        public void addAnscolumnID(java.lang.String anscolumnID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ANSCOLUMNID$18);
                target.setStringValue(anscolumnID);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "anscolumn_ID" element
         */
        public org.apache.xmlbeans.XmlString insertNewAnscolumnID(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().insert_element_user(ANSCOLUMNID$18, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "anscolumn_ID" element
         */
        public org.apache.xmlbeans.XmlString addNewAnscolumnID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(ANSCOLUMNID$18);
                return target;
            }
        }
        
        /**
         * Removes the ith "anscolumn_ID" element
         */
        public void removeAnscolumnID(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ANSCOLUMNID$18, i);
            }
        }
        
        /**
         * Gets array of all "process_log" elements
         */
        public java.lang.String[] getProcessLogArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(PROCESSLOG$20, targetList);
                java.lang.String[] result = new java.lang.String[targetList.size()];
                for (int i = 0, len = targetList.size() ; i < len ; i++)
                    result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
                return result;
            }
        }
        
        /**
         * Gets ith "process_log" element
         */
        public java.lang.String getProcessLogArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PROCESSLOG$20, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) array of all "process_log" elements
         */
        public org.apache.xmlbeans.XmlString[] xgetProcessLogArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(PROCESSLOG$20, targetList);
                org.apache.xmlbeans.XmlString[] result = new org.apache.xmlbeans.XmlString[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets (as xml) ith "process_log" element
         */
        public org.apache.xmlbeans.XmlString xgetProcessLogArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PROCESSLOG$20, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "process_log" element
         */
        public int sizeOfProcessLogArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PROCESSLOG$20);
            }
        }
        
        /**
         * Sets array of all "process_log" element
         */
        public void setProcessLogArray(java.lang.String[] processLogArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(processLogArray, PROCESSLOG$20);
            }
        }
        
        /**
         * Sets ith "process_log" element
         */
        public void setProcessLogArray(int i, java.lang.String processLog)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PROCESSLOG$20, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.setStringValue(processLog);
            }
        }
        
        /**
         * Sets (as xml) array of all "process_log" element
         */
        public void xsetProcessLogArray(org.apache.xmlbeans.XmlString[]processLogArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(processLogArray, PROCESSLOG$20);
            }
        }
        
        /**
         * Sets (as xml) ith "process_log" element
         */
        public void xsetProcessLogArray(int i, org.apache.xmlbeans.XmlString processLog)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PROCESSLOG$20, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(processLog);
            }
        }
        
        /**
         * Inserts the value as the ith "process_log" element
         */
        public void insertProcessLog(int i, java.lang.String processLog)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = 
                    (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(PROCESSLOG$20, i);
                target.setStringValue(processLog);
            }
        }
        
        /**
         * Appends the value as the last "process_log" element
         */
        public void addProcessLog(java.lang.String processLog)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PROCESSLOG$20);
                target.setStringValue(processLog);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "process_log" element
         */
        public org.apache.xmlbeans.XmlString insertNewProcessLog(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().insert_element_user(PROCESSLOG$20, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "process_log" element
         */
        public org.apache.xmlbeans.XmlString addNewProcessLog()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(PROCESSLOG$20);
                return target;
            }
        }
        
        /**
         * Removes the ith "process_log" element
         */
        public void removeProcessLog(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PROCESSLOG$20, i);
            }
        }
    }
}
