/*
 * An XML document type.
 * Localname: img
 * Namespace: 
 * Java type: noNamespace.ImgDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.centerExam.impl;
/**
 * A document containing one img(@) element.
 *
 * This is a complex type.
 */
public class ImgDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.ImgDocument
{
    private static final long serialVersionUID = 1L;
    
    public ImgDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IMG$0 = 
        new javax.xml.namespace.QName("", "img");
    
    
    /**
     * Gets the "img" element
     */
    public noNamespace.ImgDocument.Img getImg()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.ImgDocument.Img target = null;
            target = (noNamespace.ImgDocument.Img)get_store().find_element_user(IMG$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "img" element
     */
    public void setImg(noNamespace.ImgDocument.Img img)
    {
        generatedSetterHelperImpl(img, IMG$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "img" element
     */
    public noNamespace.ImgDocument.Img addNewImg()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.ImgDocument.Img target = null;
            target = (noNamespace.ImgDocument.Img)get_store().add_element_user(IMG$0);
            return target;
        }
    }
    /**
     * An XML img(@).
     *
     * This is a complex type.
     */
    public static class ImgImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.ImgDocument.Img
    {
        private static final long serialVersionUID = 1L;
        
        public ImgImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName SRC$0 = 
            new javax.xml.namespace.QName("", "src");
        private static final javax.xml.namespace.QName COMMENT$2 = 
            new javax.xml.namespace.QName("", "comment");
        
        
        /**
         * Gets the "src" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType getSrc()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().find_attribute_user(SRC$0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "src" attribute
         */
        public void setSrc(org.apache.xmlbeans.XmlAnySimpleType src)
        {
            generatedSetterHelperImpl(src, SRC$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "src" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType addNewSrc()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().add_attribute_user(SRC$0);
                return target;
            }
        }
        
        /**
         * Gets the "comment" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType getComment()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().find_attribute_user(COMMENT$2);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "comment" attribute
         */
        public boolean isSetComment()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(COMMENT$2) != null;
            }
        }
        
        /**
         * Sets the "comment" attribute
         */
        public void setComment(org.apache.xmlbeans.XmlAnySimpleType comment)
        {
            generatedSetterHelperImpl(comment, COMMENT$2, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "comment" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType addNewComment()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().add_attribute_user(COMMENT$2);
                return target;
            }
        }
        
        /**
         * Unsets the "comment" attribute
         */
        public void unsetComment()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(COMMENT$2);
            }
        }
    }
}
