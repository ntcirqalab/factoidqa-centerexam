/*
 * An XML document type.
 * Localname: question_ID
 * Namespace: 
 * Java type: QuestionIDDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.answerTable.impl;

import org.kachako.components.centerexam.xmlschema.answerTable.QuestionIDDocument;

/**
 * A document containing one question_ID(@) element.
 *
 * This is a complex type.
 */
public class QuestionIDDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements QuestionIDDocument
{
    private static final long serialVersionUID = 1L;
    
    public QuestionIDDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName QUESTIONID$0 = 
        new javax.xml.namespace.QName("", "question_ID");
    
    
    /**
     * Gets the "question_ID" element
     */
    public java.lang.String getQuestionID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUESTIONID$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "question_ID" element
     */
    public org.apache.xmlbeans.XmlString xgetQuestionID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(QUESTIONID$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "question_ID" element
     */
    public void setQuestionID(java.lang.String questionID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUESTIONID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(QUESTIONID$0);
            }
            target.setStringValue(questionID);
        }
    }
    
    /**
     * Sets (as xml) the "question_ID" element
     */
    public void xsetQuestionID(org.apache.xmlbeans.XmlString questionID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(QUESTIONID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(QUESTIONID$0);
            }
            target.set(questionID);
        }
    }
}
