/*
 * An XML document type.
 * Localname: answer_style
 * Namespace: 
 * Java type: AnswerStyleDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.answerTable.impl;

import org.kachako.components.centerexam.xmlschema.answerTable.AnswerStyleDocument;

/**
 * A document containing one answer_style(@) element.
 *
 * This is a complex type.
 */
public class AnswerStyleDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AnswerStyleDocument
{
    private static final long serialVersionUID = 1L;
    
    public AnswerStyleDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ANSWERSTYLE$0 = 
        new javax.xml.namespace.QName("", "answer_style");
    
    
    /**
     * Gets the "answer_style" element
     */
    public java.lang.String getAnswerStyle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ANSWERSTYLE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "answer_style" element
     */
    public org.apache.xmlbeans.XmlString xgetAnswerStyle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ANSWERSTYLE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "answer_style" element
     */
    public void setAnswerStyle(java.lang.String answerStyle)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ANSWERSTYLE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ANSWERSTYLE$0);
            }
            target.setStringValue(answerStyle);
        }
    }
    
    /**
     * Sets (as xml) the "answer_style" element
     */
    public void xsetAnswerStyle(org.apache.xmlbeans.XmlString answerStyle)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ANSWERSTYLE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(ANSWERSTYLE$0);
            }
            target.set(answerStyle);
        }
    }
}
