/*
 * An XML document type.
 * Localname: br
 * Namespace: 
 * Java type: noNamespace.BrDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.centerExam.impl;
/**
 * A document containing one br(@) element.
 *
 * This is a complex type.
 */
public class BrDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.BrDocument
{
    private static final long serialVersionUID = 1L;
    
    public BrDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName BR$0 = 
        new javax.xml.namespace.QName("", "br");
    
    
    /**
     * Gets the "br" element
     */
    public noNamespace.BrDocument.Br getBr()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.BrDocument.Br target = null;
            target = (noNamespace.BrDocument.Br)get_store().find_element_user(BR$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "br" element
     */
    public void setBr(noNamespace.BrDocument.Br br)
    {
        generatedSetterHelperImpl(br, BR$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "br" element
     */
    public noNamespace.BrDocument.Br addNewBr()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.BrDocument.Br target = null;
            target = (noNamespace.BrDocument.Br)get_store().add_element_user(BR$0);
            return target;
        }
    }
    /**
     * An XML br(@).
     *
     * This is a complex type.
     */
    public static class BrImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.BrDocument.Br
    {
        private static final long serialVersionUID = 1L;
        
        public BrImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        
    }
}
