/*
 * An XML document type.
 * Localname: data
 * Namespace: 
 * Java type: noNamespace.DataDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.centerExam;


/**
 * A document containing one data(@) element.
 *
 * This is a complex type.
 */
public interface DataDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(DataDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s9AD9431611720D569E6CDA08F9EDE660").resolveHandle("data13a3doctype");
    
    /**
     * Gets the "data" element
     */
    noNamespace.DataDocument.Data getData();
    
    /**
     * Sets the "data" element
     */
    void setData(noNamespace.DataDocument.Data data);
    
    /**
     * Appends and returns a new empty "data" element
     */
    noNamespace.DataDocument.Data addNewData();
    
    /**
     * An XML data(@).
     *
     * This is a complex type.
     */
    public interface Data extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Data.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s9AD9431611720D569E6CDA08F9EDE660").resolveHandle("data54adelemtype");
        
        /**
         * Gets array of all "garbled" elements
         */
        noNamespace.GarbledDocument.Garbled[] getGarbledArray();
        
        /**
         * Gets ith "garbled" element
         */
        noNamespace.GarbledDocument.Garbled getGarbledArray(int i);
        
        /**
         * Returns number of "garbled" element
         */
        int sizeOfGarbledArray();
        
        /**
         * Sets array of all "garbled" element
         */
        void setGarbledArray(noNamespace.GarbledDocument.Garbled[] garbledArray);
        
        /**
         * Sets ith "garbled" element
         */
        void setGarbledArray(int i, noNamespace.GarbledDocument.Garbled garbled);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "garbled" element
         */
        noNamespace.GarbledDocument.Garbled insertNewGarbled(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "garbled" element
         */
        noNamespace.GarbledDocument.Garbled addNewGarbled();
        
        /**
         * Removes the ith "garbled" element
         */
        void removeGarbled(int i);
        
        /**
         * Gets array of all "blank" elements
         */
        noNamespace.BlankDocument.Blank[] getBlankArray();
        
        /**
         * Gets ith "blank" element
         */
        noNamespace.BlankDocument.Blank getBlankArray(int i);
        
        /**
         * Returns number of "blank" element
         */
        int sizeOfBlankArray();
        
        /**
         * Sets array of all "blank" element
         */
        void setBlankArray(noNamespace.BlankDocument.Blank[] blankArray);
        
        /**
         * Sets ith "blank" element
         */
        void setBlankArray(int i, noNamespace.BlankDocument.Blank blank);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "blank" element
         */
        noNamespace.BlankDocument.Blank insertNewBlank(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "blank" element
         */
        noNamespace.BlankDocument.Blank addNewBlank();
        
        /**
         * Removes the ith "blank" element
         */
        void removeBlank(int i);
        
        /**
         * Gets array of all "img" elements
         */
        noNamespace.ImgDocument.Img[] getImgArray();
        
        /**
         * Gets ith "img" element
         */
        noNamespace.ImgDocument.Img getImgArray(int i);
        
        /**
         * Returns number of "img" element
         */
        int sizeOfImgArray();
        
        /**
         * Sets array of all "img" element
         */
        void setImgArray(noNamespace.ImgDocument.Img[] imgArray);
        
        /**
         * Sets ith "img" element
         */
        void setImgArray(int i, noNamespace.ImgDocument.Img img);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "img" element
         */
        noNamespace.ImgDocument.Img insertNewImg(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "img" element
         */
        noNamespace.ImgDocument.Img addNewImg();
        
        /**
         * Removes the ith "img" element
         */
        void removeImg(int i);
        
        /**
         * Gets array of all "lText" elements
         */
        noNamespace.LTextDocument.LText[] getLTextArray();
        
        /**
         * Gets ith "lText" element
         */
        noNamespace.LTextDocument.LText getLTextArray(int i);
        
        /**
         * Returns number of "lText" element
         */
        int sizeOfLTextArray();
        
        /**
         * Sets array of all "lText" element
         */
        void setLTextArray(noNamespace.LTextDocument.LText[] lTextArray);
        
        /**
         * Sets ith "lText" element
         */
        void setLTextArray(int i, noNamespace.LTextDocument.LText lText);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "lText" element
         */
        noNamespace.LTextDocument.LText insertNewLText(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "lText" element
         */
        noNamespace.LTextDocument.LText addNewLText();
        
        /**
         * Removes the ith "lText" element
         */
        void removeLText(int i);
        
        /**
         * Gets array of all "tbl" elements
         */
        noNamespace.TblDocument.Tbl[] getTblArray();
        
        /**
         * Gets ith "tbl" element
         */
        noNamespace.TblDocument.Tbl getTblArray(int i);
        
        /**
         * Returns number of "tbl" element
         */
        int sizeOfTblArray();
        
        /**
         * Sets array of all "tbl" element
         */
        void setTblArray(noNamespace.TblDocument.Tbl[] tblArray);
        
        /**
         * Sets ith "tbl" element
         */
        void setTblArray(int i, noNamespace.TblDocument.Tbl tbl);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "tbl" element
         */
        noNamespace.TblDocument.Tbl insertNewTbl(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "tbl" element
         */
        noNamespace.TblDocument.Tbl addNewTbl();
        
        /**
         * Removes the ith "tbl" element
         */
        void removeTbl(int i);
        
        /**
         * Gets array of all "label" elements
         */
        noNamespace.LabelDocument.Label[] getLabelArray();
        
        /**
         * Gets ith "label" element
         */
        noNamespace.LabelDocument.Label getLabelArray(int i);
        
        /**
         * Returns number of "label" element
         */
        int sizeOfLabelArray();
        
        /**
         * Sets array of all "label" element
         */
        void setLabelArray(noNamespace.LabelDocument.Label[] labelArray);
        
        /**
         * Sets ith "label" element
         */
        void setLabelArray(int i, noNamespace.LabelDocument.Label label);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "label" element
         */
        noNamespace.LabelDocument.Label insertNewLabel(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "label" element
         */
        noNamespace.LabelDocument.Label addNewLabel();
        
        /**
         * Removes the ith "label" element
         */
        void removeLabel(int i);
        
        /**
         * Gets array of all "uText" elements
         */
        noNamespace.UTextDocument.UText[] getUTextArray();
        
        /**
         * Gets ith "uText" element
         */
        noNamespace.UTextDocument.UText getUTextArray(int i);
        
        /**
         * Returns number of "uText" element
         */
        int sizeOfUTextArray();
        
        /**
         * Sets array of all "uText" element
         */
        void setUTextArray(noNamespace.UTextDocument.UText[] uTextArray);
        
        /**
         * Sets ith "uText" element
         */
        void setUTextArray(int i, noNamespace.UTextDocument.UText uText);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "uText" element
         */
        noNamespace.UTextDocument.UText insertNewUText(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "uText" element
         */
        noNamespace.UTextDocument.UText addNewUText();
        
        /**
         * Removes the ith "uText" element
         */
        void removeUText(int i);
        
        /**
         * Gets array of all "ansColumn" elements
         */
        noNamespace.AnsColumnDocument.AnsColumn[] getAnsColumnArray();
        
        /**
         * Gets ith "ansColumn" element
         */
        noNamespace.AnsColumnDocument.AnsColumn getAnsColumnArray(int i);
        
        /**
         * Returns number of "ansColumn" element
         */
        int sizeOfAnsColumnArray();
        
        /**
         * Sets array of all "ansColumn" element
         */
        void setAnsColumnArray(noNamespace.AnsColumnDocument.AnsColumn[] ansColumnArray);
        
        /**
         * Sets ith "ansColumn" element
         */
        void setAnsColumnArray(int i, noNamespace.AnsColumnDocument.AnsColumn ansColumn);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ansColumn" element
         */
        noNamespace.AnsColumnDocument.AnsColumn insertNewAnsColumn(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ansColumn" element
         */
        noNamespace.AnsColumnDocument.AnsColumn addNewAnsColumn();
        
        /**
         * Removes the ith "ansColumn" element
         */
        void removeAnsColumn(int i);
        
        /**
         * Gets array of all "source" elements
         */
        noNamespace.SourceDocument.Source[] getSourceArray();
        
        /**
         * Gets ith "source" element
         */
        noNamespace.SourceDocument.Source getSourceArray(int i);
        
        /**
         * Returns number of "source" element
         */
        int sizeOfSourceArray();
        
        /**
         * Sets array of all "source" element
         */
        void setSourceArray(noNamespace.SourceDocument.Source[] sourceArray);
        
        /**
         * Sets ith "source" element
         */
        void setSourceArray(int i, noNamespace.SourceDocument.Source source);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "source" element
         */
        noNamespace.SourceDocument.Source insertNewSource(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "source" element
         */
        noNamespace.SourceDocument.Source addNewSource();
        
        /**
         * Removes the ith "source" element
         */
        void removeSource(int i);
        
        /**
         * Gets array of all "note" elements
         */
        noNamespace.NoteDocument.Note[] getNoteArray();
        
        /**
         * Gets ith "note" element
         */
        noNamespace.NoteDocument.Note getNoteArray(int i);
        
        /**
         * Returns number of "note" element
         */
        int sizeOfNoteArray();
        
        /**
         * Sets array of all "note" element
         */
        void setNoteArray(noNamespace.NoteDocument.Note[] noteArray);
        
        /**
         * Sets ith "note" element
         */
        void setNoteArray(int i, noNamespace.NoteDocument.Note note);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "note" element
         */
        noNamespace.NoteDocument.Note insertNewNote(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "note" element
         */
        noNamespace.NoteDocument.Note addNewNote();
        
        /**
         * Removes the ith "note" element
         */
        void removeNote(int i);
        
        /**
         * Gets array of all "quote" elements
         */
        noNamespace.QuoteDocument.Quote[] getQuoteArray();
        
        /**
         * Gets ith "quote" element
         */
        noNamespace.QuoteDocument.Quote getQuoteArray(int i);
        
        /**
         * Returns number of "quote" element
         */
        int sizeOfQuoteArray();
        
        /**
         * Sets array of all "quote" element
         */
        void setQuoteArray(noNamespace.QuoteDocument.Quote[] quoteArray);
        
        /**
         * Sets ith "quote" element
         */
        void setQuoteArray(int i, noNamespace.QuoteDocument.Quote quote);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "quote" element
         */
        noNamespace.QuoteDocument.Quote insertNewQuote(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "quote" element
         */
        noNamespace.QuoteDocument.Quote addNewQuote();
        
        /**
         * Removes the ith "quote" element
         */
        void removeQuote(int i);
        
        /**
         * Gets array of all "choice" elements
         */
        noNamespace.ChoiceDocument.Choice[] getChoiceArray();
        
        /**
         * Gets ith "choice" element
         */
        noNamespace.ChoiceDocument.Choice getChoiceArray(int i);
        
        /**
         * Returns number of "choice" element
         */
        int sizeOfChoiceArray();
        
        /**
         * Sets array of all "choice" element
         */
        void setChoiceArray(noNamespace.ChoiceDocument.Choice[] choiceArray);
        
        /**
         * Sets ith "choice" element
         */
        void setChoiceArray(int i, noNamespace.ChoiceDocument.Choice choice);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "choice" element
         */
        noNamespace.ChoiceDocument.Choice insertNewChoice(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "choice" element
         */
        noNamespace.ChoiceDocument.Choice addNewChoice();
        
        /**
         * Removes the ith "choice" element
         */
        void removeChoice(int i);
        
        /**
         * Gets array of all "cNum" elements
         */
        noNamespace.CNumDocument.CNum[] getCNumArray();
        
        /**
         * Gets ith "cNum" element
         */
        noNamespace.CNumDocument.CNum getCNumArray(int i);
        
        /**
         * Returns number of "cNum" element
         */
        int sizeOfCNumArray();
        
        /**
         * Sets array of all "cNum" element
         */
        void setCNumArray(noNamespace.CNumDocument.CNum[] cNumArray);
        
        /**
         * Sets ith "cNum" element
         */
        void setCNumArray(int i, noNamespace.CNumDocument.CNum cNum);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "cNum" element
         */
        noNamespace.CNumDocument.CNum insertNewCNum(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "cNum" element
         */
        noNamespace.CNumDocument.CNum addNewCNum();
        
        /**
         * Removes the ith "cNum" element
         */
        void removeCNum(int i);
        
        /**
         * Gets array of all "missing" elements
         */
        noNamespace.MissingDocument.Missing[] getMissingArray();
        
        /**
         * Gets ith "missing" element
         */
        noNamespace.MissingDocument.Missing getMissingArray(int i);
        
        /**
         * Returns number of "missing" element
         */
        int sizeOfMissingArray();
        
        /**
         * Sets array of all "missing" element
         */
        void setMissingArray(noNamespace.MissingDocument.Missing[] missingArray);
        
        /**
         * Sets ith "missing" element
         */
        void setMissingArray(int i, noNamespace.MissingDocument.Missing missing);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "missing" element
         */
        noNamespace.MissingDocument.Missing insertNewMissing(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "missing" element
         */
        noNamespace.MissingDocument.Missing addNewMissing();
        
        /**
         * Removes the ith "missing" element
         */
        void removeMissing(int i);
        
        /**
         * Gets array of all "ref" elements
         */
        noNamespace.RefDocument.Ref[] getRefArray();
        
        /**
         * Gets ith "ref" element
         */
        noNamespace.RefDocument.Ref getRefArray(int i);
        
        /**
         * Returns number of "ref" element
         */
        int sizeOfRefArray();
        
        /**
         * Sets array of all "ref" element
         */
        void setRefArray(noNamespace.RefDocument.Ref[] refArray);
        
        /**
         * Sets ith "ref" element
         */
        void setRefArray(int i, noNamespace.RefDocument.Ref ref);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ref" element
         */
        noNamespace.RefDocument.Ref insertNewRef(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ref" element
         */
        noNamespace.RefDocument.Ref addNewRef();
        
        /**
         * Removes the ith "ref" element
         */
        void removeRef(int i);
        
        /**
         * Gets array of all "data" elements
         */
        noNamespace.DataDocument.Data[] getDataArray();
        
        /**
         * Gets ith "data" element
         */
        noNamespace.DataDocument.Data getDataArray(int i);
        
        /**
         * Returns number of "data" element
         */
        int sizeOfDataArray();
        
        /**
         * Sets array of all "data" element
         */
        void setDataArray(noNamespace.DataDocument.Data[] dataArray);
        
        /**
         * Sets ith "data" element
         */
        void setDataArray(int i, noNamespace.DataDocument.Data data);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "data" element
         */
        noNamespace.DataDocument.Data insertNewData(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "data" element
         */
        noNamespace.DataDocument.Data addNewData();
        
        /**
         * Removes the ith "data" element
         */
        void removeData(int i);
        
        /**
         * Gets array of all "br" elements
         */
        noNamespace.BrDocument.Br[] getBrArray();
        
        /**
         * Gets ith "br" element
         */
        noNamespace.BrDocument.Br getBrArray(int i);
        
        /**
         * Returns number of "br" element
         */
        int sizeOfBrArray();
        
        /**
         * Sets array of all "br" element
         */
        void setBrArray(noNamespace.BrDocument.Br[] brArray);
        
        /**
         * Sets ith "br" element
         */
        void setBrArray(int i, noNamespace.BrDocument.Br br);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "br" element
         */
        noNamespace.BrDocument.Br insertNewBr(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "br" element
         */
        noNamespace.BrDocument.Br addNewBr();
        
        /**
         * Removes the ith "br" element
         */
        void removeBr(int i);
        
        /**
         * Gets array of all "choices" elements
         */
        noNamespace.ChoicesDocument.Choices[] getChoicesArray();
        
        /**
         * Gets ith "choices" element
         */
        noNamespace.ChoicesDocument.Choices getChoicesArray(int i);
        
        /**
         * Returns number of "choices" element
         */
        int sizeOfChoicesArray();
        
        /**
         * Sets array of all "choices" element
         */
        void setChoicesArray(noNamespace.ChoicesDocument.Choices[] choicesArray);
        
        /**
         * Sets ith "choices" element
         */
        void setChoicesArray(int i, noNamespace.ChoicesDocument.Choices choices);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "choices" element
         */
        noNamespace.ChoicesDocument.Choices insertNewChoices(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "choices" element
         */
        noNamespace.ChoicesDocument.Choices addNewChoices();
        
        /**
         * Removes the ith "choices" element
         */
        void removeChoices(int i);
        
        /**
         * Gets array of all "formula" elements
         */
        noNamespace.FormulaDocument.Formula[] getFormulaArray();
        
        /**
         * Gets ith "formula" element
         */
        noNamespace.FormulaDocument.Formula getFormulaArray(int i);
        
        /**
         * Returns number of "formula" element
         */
        int sizeOfFormulaArray();
        
        /**
         * Sets array of all "formula" element
         */
        void setFormulaArray(noNamespace.FormulaDocument.Formula[] formulaArray);
        
        /**
         * Sets ith "formula" element
         */
        void setFormulaArray(int i, noNamespace.FormulaDocument.Formula formula);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "formula" element
         */
        noNamespace.FormulaDocument.Formula insertNewFormula(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "formula" element
         */
        noNamespace.FormulaDocument.Formula addNewFormula();
        
        /**
         * Removes the ith "formula" element
         */
        void removeFormula(int i);
        
        /**
         * Gets array of all "row" elements
         */
        noNamespace.RowDocument.Row[] getRowArray();
        
        /**
         * Gets ith "row" element
         */
        noNamespace.RowDocument.Row getRowArray(int i);
        
        /**
         * Returns number of "row" element
         */
        int sizeOfRowArray();
        
        /**
         * Sets array of all "row" element
         */
        void setRowArray(noNamespace.RowDocument.Row[] rowArray);
        
        /**
         * Sets ith "row" element
         */
        void setRowArray(int i, noNamespace.RowDocument.Row row);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "row" element
         */
        noNamespace.RowDocument.Row insertNewRow(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "row" element
         */
        noNamespace.RowDocument.Row addNewRow();
        
        /**
         * Removes the ith "row" element
         */
        void removeRow(int i);
        
        /**
         * Gets array of all "cell" elements
         */
        noNamespace.CellDocument.Cell[] getCellArray();
        
        /**
         * Gets ith "cell" element
         */
        noNamespace.CellDocument.Cell getCellArray(int i);
        
        /**
         * Returns number of "cell" element
         */
        int sizeOfCellArray();
        
        /**
         * Sets array of all "cell" element
         */
        void setCellArray(noNamespace.CellDocument.Cell[] cellArray);
        
        /**
         * Sets ith "cell" element
         */
        void setCellArray(int i, noNamespace.CellDocument.Cell cell);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "cell" element
         */
        noNamespace.CellDocument.Cell insertNewCell(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "cell" element
         */
        noNamespace.CellDocument.Cell addNewCell();
        
        /**
         * Removes the ith "cell" element
         */
        void removeCell(int i);
        
        /**
         * Gets the "id" attribute
         */
        java.lang.String getId();
        
        /**
         * Gets (as xml) the "id" attribute
         */
        org.apache.xmlbeans.XmlID xgetId();
        
        /**
         * Sets the "id" attribute
         */
        void setId(java.lang.String id);
        
        /**
         * Sets (as xml) the "id" attribute
         */
        void xsetId(org.apache.xmlbeans.XmlID id);
        
        /**
         * Gets the "type" attribute
         */
        noNamespace.DataDocument.Data.Type.Enum getType();
        
        /**
         * Gets (as xml) the "type" attribute
         */
        noNamespace.DataDocument.Data.Type xgetType();
        
        /**
         * Sets the "type" attribute
         */
        void setType(noNamespace.DataDocument.Data.Type.Enum type);
        
        /**
         * Sets (as xml) the "type" attribute
         */
        void xsetType(noNamespace.DataDocument.Data.Type type);
        
        /**
         * An XML type(@).
         *
         * This is an atomic type that is a restriction of noNamespace.DataDocument$Data$Type.
         */
        public interface Type extends org.apache.xmlbeans.XmlToken
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Type.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s9AD9431611720D569E6CDA08F9EDE660").resolveHandle("type1887attrtype");
            
            org.apache.xmlbeans.StringEnumAbstractBase enumValue();
            void set(org.apache.xmlbeans.StringEnumAbstractBase e);
            
            static final Enum COMPLEX = Enum.forString("complex");
            static final Enum TEXT = Enum.forString("text");
            static final Enum TABLE = Enum.forString("table");
            static final Enum IMAGE = Enum.forString("image");
            
            static final int INT_COMPLEX = Enum.INT_COMPLEX;
            static final int INT_TEXT = Enum.INT_TEXT;
            static final int INT_TABLE = Enum.INT_TABLE;
            static final int INT_IMAGE = Enum.INT_IMAGE;
            
            /**
             * Enumeration value class for noNamespace.DataDocument$Data$Type.
             * These enum values can be used as follows:
             * <pre>
             * enum.toString(); // returns the string value of the enum
             * enum.intValue(); // returns an int value, useful for switches
             * // e.g., case Enum.INT_COMPLEX
             * Enum.forString(s); // returns the enum value for a string
             * Enum.forInt(i); // returns the enum value for an int
             * </pre>
             * Enumeration objects are immutable singleton objects that
             * can be compared using == object equality. They have no
             * public constructor. See the constants defined within this
             * class for all the valid values.
             */
            static final class Enum extends org.apache.xmlbeans.StringEnumAbstractBase
            {
                /**
                 * Returns the enum value for a string, or null if none.
                 */
                public static Enum forString(java.lang.String s)
                    { return (Enum)table.forString(s); }
                /**
                 * Returns the enum value corresponding to an int, or null if none.
                 */
                public static Enum forInt(int i)
                    { return (Enum)table.forInt(i); }
                
                private Enum(java.lang.String s, int i)
                    { super(s, i); }
                
                static final int INT_COMPLEX = 1;
                static final int INT_TEXT = 2;
                static final int INT_TABLE = 3;
                static final int INT_IMAGE = 4;
                
                public static final org.apache.xmlbeans.StringEnumAbstractBase.Table table =
                    new org.apache.xmlbeans.StringEnumAbstractBase.Table
                (
                    new Enum[]
                    {
                      new Enum("complex", INT_COMPLEX),
                      new Enum("text", INT_TEXT),
                      new Enum("table", INT_TABLE),
                      new Enum("image", INT_IMAGE),
                    }
                );
                private static final long serialVersionUID = 1L;
                private java.lang.Object readResolve() { return forInt(intValue()); } 
            }
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static noNamespace.DataDocument.Data.Type newValue(java.lang.Object obj) {
                  return (noNamespace.DataDocument.Data.Type) type.newValue( obj ); }
                
                public static noNamespace.DataDocument.Data.Type newInstance() {
                  return (noNamespace.DataDocument.Data.Type) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static noNamespace.DataDocument.Data.Type newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (noNamespace.DataDocument.Data.Type) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static noNamespace.DataDocument.Data newInstance() {
              return (noNamespace.DataDocument.Data) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static noNamespace.DataDocument.Data newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (noNamespace.DataDocument.Data) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static noNamespace.DataDocument newInstance() {
          return (noNamespace.DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static noNamespace.DataDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (noNamespace.DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static noNamespace.DataDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static noNamespace.DataDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static noNamespace.DataDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static noNamespace.DataDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static noNamespace.DataDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static noNamespace.DataDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static noNamespace.DataDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static noNamespace.DataDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static noNamespace.DataDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static noNamespace.DataDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static noNamespace.DataDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static noNamespace.DataDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static noNamespace.DataDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static noNamespace.DataDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static noNamespace.DataDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (noNamespace.DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static noNamespace.DataDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (noNamespace.DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
