/*
 * An XML document type.
 * Localname: answer
 * Namespace: 
 * Java type: AnswerDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.answerTable.impl;

import org.kachako.components.centerexam.xmlschema.answerTable.AnswerDocument;

/**
 * A document containing one answer(@) element.
 *
 * This is a complex type.
 */
public class AnswerDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AnswerDocument
{
    private static final long serialVersionUID = 1L;
    
    public AnswerDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ANSWER$0 = 
        new javax.xml.namespace.QName("", "answer");
    
    
    /**
     * Gets the "answer" element
     */
    public java.lang.String getAnswer()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ANSWER$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "answer" element
     */
    public org.apache.xmlbeans.XmlString xgetAnswer()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ANSWER$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "answer" element
     */
    public void setAnswer(java.lang.String answer)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ANSWER$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ANSWER$0);
            }
            target.setStringValue(answer);
        }
    }
    
    /**
     * Sets (as xml) the "answer" element
     */
    public void xsetAnswer(org.apache.xmlbeans.XmlString answer)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ANSWER$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(ANSWER$0);
            }
            target.set(answer);
        }
    }
}
