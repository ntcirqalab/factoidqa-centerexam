/*
 * An XML document type.
 * Localname: choices
 * Namespace: 
 * Java type: noNamespace.ChoicesDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.centerExam.impl;
/**
 * A document containing one choices(@) element.
 *
 * This is a complex type.
 */
public class ChoicesDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.ChoicesDocument
{
    private static final long serialVersionUID = 1L;
    
    public ChoicesDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CHOICES$0 = 
        new javax.xml.namespace.QName("", "choices");
    
    
    /**
     * Gets the "choices" element
     */
    public noNamespace.ChoicesDocument.Choices getChoices()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.ChoicesDocument.Choices target = null;
            target = (noNamespace.ChoicesDocument.Choices)get_store().find_element_user(CHOICES$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "choices" element
     */
    public void setChoices(noNamespace.ChoicesDocument.Choices choices)
    {
        generatedSetterHelperImpl(choices, CHOICES$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "choices" element
     */
    public noNamespace.ChoicesDocument.Choices addNewChoices()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.ChoicesDocument.Choices target = null;
            target = (noNamespace.ChoicesDocument.Choices)get_store().add_element_user(CHOICES$0);
            return target;
        }
    }
    /**
     * An XML choices(@).
     *
     * This is a complex type.
     */
    public static class ChoicesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.ChoicesDocument.Choices
    {
        private static final long serialVersionUID = 1L;
        
        public ChoicesImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName GARBLED$0 = 
            new javax.xml.namespace.QName("", "garbled");
        private static final javax.xml.namespace.QName CNUM$2 = 
            new javax.xml.namespace.QName("", "cNum");
        private static final javax.xml.namespace.QName REF$4 = 
            new javax.xml.namespace.QName("", "ref");
        private static final javax.xml.namespace.QName IMG$6 = 
            new javax.xml.namespace.QName("", "img");
        private static final javax.xml.namespace.QName TBL$8 = 
            new javax.xml.namespace.QName("", "tbl");
        private static final javax.xml.namespace.QName ROW$10 = 
            new javax.xml.namespace.QName("", "row");
        private static final javax.xml.namespace.QName CELL$12 = 
            new javax.xml.namespace.QName("", "cell");
        private static final javax.xml.namespace.QName UTEXT$14 = 
            new javax.xml.namespace.QName("", "uText");
        private static final javax.xml.namespace.QName LTEXT$16 = 
            new javax.xml.namespace.QName("", "lText");
        private static final javax.xml.namespace.QName CHOICE$18 = 
            new javax.xml.namespace.QName("", "choice");
        private static final javax.xml.namespace.QName MISSING$20 = 
            new javax.xml.namespace.QName("", "missing");
        private static final javax.xml.namespace.QName BR$22 = 
            new javax.xml.namespace.QName("", "br");
        private static final javax.xml.namespace.QName LABEL$24 = 
            new javax.xml.namespace.QName("", "label");
        private static final javax.xml.namespace.QName FORMULA$26 = 
            new javax.xml.namespace.QName("", "formula");
        private static final javax.xml.namespace.QName QUOTE$28 = 
            new javax.xml.namespace.QName("", "quote");
        private static final javax.xml.namespace.QName SOURCE$30 = 
            new javax.xml.namespace.QName("", "source");
        private static final javax.xml.namespace.QName COMMENT$32 = 
            new javax.xml.namespace.QName("", "comment");
        private static final javax.xml.namespace.QName ANSCOL$34 = 
            new javax.xml.namespace.QName("", "anscol");
        
        
        /**
         * Gets array of all "garbled" elements
         */
        public noNamespace.GarbledDocument.Garbled[] getGarbledArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(GARBLED$0, targetList);
                noNamespace.GarbledDocument.Garbled[] result = new noNamespace.GarbledDocument.Garbled[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled getGarbledArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().find_element_user(GARBLED$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "garbled" element
         */
        public int sizeOfGarbledArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(GARBLED$0);
            }
        }
        
        /**
         * Sets array of all "garbled" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setGarbledArray(noNamespace.GarbledDocument.Garbled[] garbledArray)
        {
            check_orphaned();
            arraySetterHelper(garbledArray, GARBLED$0);
        }
        
        /**
         * Sets ith "garbled" element
         */
        public void setGarbledArray(int i, noNamespace.GarbledDocument.Garbled garbled)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().find_element_user(GARBLED$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(garbled);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled insertNewGarbled(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().insert_element_user(GARBLED$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled addNewGarbled()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().add_element_user(GARBLED$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "garbled" element
         */
        public void removeGarbled(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(GARBLED$0, i);
            }
        }
        
        /**
         * Gets array of all "cNum" elements
         */
        public noNamespace.CNumDocument.CNum[] getCNumArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(CNUM$2, targetList);
                noNamespace.CNumDocument.CNum[] result = new noNamespace.CNumDocument.CNum[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "cNum" element
         */
        public noNamespace.CNumDocument.CNum getCNumArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CNumDocument.CNum target = null;
                target = (noNamespace.CNumDocument.CNum)get_store().find_element_user(CNUM$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "cNum" element
         */
        public int sizeOfCNumArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CNUM$2);
            }
        }
        
        /**
         * Sets array of all "cNum" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setCNumArray(noNamespace.CNumDocument.CNum[] cNumArray)
        {
            check_orphaned();
            arraySetterHelper(cNumArray, CNUM$2);
        }
        
        /**
         * Sets ith "cNum" element
         */
        public void setCNumArray(int i, noNamespace.CNumDocument.CNum cNum)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CNumDocument.CNum target = null;
                target = (noNamespace.CNumDocument.CNum)get_store().find_element_user(CNUM$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(cNum);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "cNum" element
         */
        public noNamespace.CNumDocument.CNum insertNewCNum(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CNumDocument.CNum target = null;
                target = (noNamespace.CNumDocument.CNum)get_store().insert_element_user(CNUM$2, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "cNum" element
         */
        public noNamespace.CNumDocument.CNum addNewCNum()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CNumDocument.CNum target = null;
                target = (noNamespace.CNumDocument.CNum)get_store().add_element_user(CNUM$2);
                return target;
            }
        }
        
        /**
         * Removes the ith "cNum" element
         */
        public void removeCNum(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CNUM$2, i);
            }
        }
        
        /**
         * Gets array of all "ref" elements
         */
        public noNamespace.RefDocument.Ref[] getRefArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(REF$4, targetList);
                noNamespace.RefDocument.Ref[] result = new noNamespace.RefDocument.Ref[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "ref" element
         */
        public noNamespace.RefDocument.Ref getRefArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().find_element_user(REF$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "ref" element
         */
        public int sizeOfRefArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(REF$4);
            }
        }
        
        /**
         * Sets array of all "ref" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setRefArray(noNamespace.RefDocument.Ref[] refArray)
        {
            check_orphaned();
            arraySetterHelper(refArray, REF$4);
        }
        
        /**
         * Sets ith "ref" element
         */
        public void setRefArray(int i, noNamespace.RefDocument.Ref ref)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().find_element_user(REF$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(ref);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ref" element
         */
        public noNamespace.RefDocument.Ref insertNewRef(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().insert_element_user(REF$4, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ref" element
         */
        public noNamespace.RefDocument.Ref addNewRef()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().add_element_user(REF$4);
                return target;
            }
        }
        
        /**
         * Removes the ith "ref" element
         */
        public void removeRef(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(REF$4, i);
            }
        }
        
        /**
         * Gets array of all "img" elements
         */
        public noNamespace.ImgDocument.Img[] getImgArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(IMG$6, targetList);
                noNamespace.ImgDocument.Img[] result = new noNamespace.ImgDocument.Img[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "img" element
         */
        public noNamespace.ImgDocument.Img getImgArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ImgDocument.Img target = null;
                target = (noNamespace.ImgDocument.Img)get_store().find_element_user(IMG$6, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "img" element
         */
        public int sizeOfImgArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IMG$6);
            }
        }
        
        /**
         * Sets array of all "img" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setImgArray(noNamespace.ImgDocument.Img[] imgArray)
        {
            check_orphaned();
            arraySetterHelper(imgArray, IMG$6);
        }
        
        /**
         * Sets ith "img" element
         */
        public void setImgArray(int i, noNamespace.ImgDocument.Img img)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ImgDocument.Img target = null;
                target = (noNamespace.ImgDocument.Img)get_store().find_element_user(IMG$6, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(img);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "img" element
         */
        public noNamespace.ImgDocument.Img insertNewImg(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ImgDocument.Img target = null;
                target = (noNamespace.ImgDocument.Img)get_store().insert_element_user(IMG$6, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "img" element
         */
        public noNamespace.ImgDocument.Img addNewImg()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ImgDocument.Img target = null;
                target = (noNamespace.ImgDocument.Img)get_store().add_element_user(IMG$6);
                return target;
            }
        }
        
        /**
         * Removes the ith "img" element
         */
        public void removeImg(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IMG$6, i);
            }
        }
        
        /**
         * Gets array of all "tbl" elements
         */
        public noNamespace.TblDocument.Tbl[] getTblArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(TBL$8, targetList);
                noNamespace.TblDocument.Tbl[] result = new noNamespace.TblDocument.Tbl[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "tbl" element
         */
        public noNamespace.TblDocument.Tbl getTblArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.TblDocument.Tbl target = null;
                target = (noNamespace.TblDocument.Tbl)get_store().find_element_user(TBL$8, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "tbl" element
         */
        public int sizeOfTblArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(TBL$8);
            }
        }
        
        /**
         * Sets array of all "tbl" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setTblArray(noNamespace.TblDocument.Tbl[] tblArray)
        {
            check_orphaned();
            arraySetterHelper(tblArray, TBL$8);
        }
        
        /**
         * Sets ith "tbl" element
         */
        public void setTblArray(int i, noNamespace.TblDocument.Tbl tbl)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.TblDocument.Tbl target = null;
                target = (noNamespace.TblDocument.Tbl)get_store().find_element_user(TBL$8, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(tbl);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "tbl" element
         */
        public noNamespace.TblDocument.Tbl insertNewTbl(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.TblDocument.Tbl target = null;
                target = (noNamespace.TblDocument.Tbl)get_store().insert_element_user(TBL$8, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "tbl" element
         */
        public noNamespace.TblDocument.Tbl addNewTbl()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.TblDocument.Tbl target = null;
                target = (noNamespace.TblDocument.Tbl)get_store().add_element_user(TBL$8);
                return target;
            }
        }
        
        /**
         * Removes the ith "tbl" element
         */
        public void removeTbl(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(TBL$8, i);
            }
        }
        
        /**
         * Gets array of all "row" elements
         */
        public noNamespace.RowDocument.Row[] getRowArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(ROW$10, targetList);
                noNamespace.RowDocument.Row[] result = new noNamespace.RowDocument.Row[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "row" element
         */
        public noNamespace.RowDocument.Row getRowArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RowDocument.Row target = null;
                target = (noNamespace.RowDocument.Row)get_store().find_element_user(ROW$10, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "row" element
         */
        public int sizeOfRowArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ROW$10);
            }
        }
        
        /**
         * Sets array of all "row" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setRowArray(noNamespace.RowDocument.Row[] rowArray)
        {
            check_orphaned();
            arraySetterHelper(rowArray, ROW$10);
        }
        
        /**
         * Sets ith "row" element
         */
        public void setRowArray(int i, noNamespace.RowDocument.Row row)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RowDocument.Row target = null;
                target = (noNamespace.RowDocument.Row)get_store().find_element_user(ROW$10, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(row);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "row" element
         */
        public noNamespace.RowDocument.Row insertNewRow(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RowDocument.Row target = null;
                target = (noNamespace.RowDocument.Row)get_store().insert_element_user(ROW$10, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "row" element
         */
        public noNamespace.RowDocument.Row addNewRow()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RowDocument.Row target = null;
                target = (noNamespace.RowDocument.Row)get_store().add_element_user(ROW$10);
                return target;
            }
        }
        
        /**
         * Removes the ith "row" element
         */
        public void removeRow(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ROW$10, i);
            }
        }
        
        /**
         * Gets array of all "cell" elements
         */
        public noNamespace.CellDocument.Cell[] getCellArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(CELL$12, targetList);
                noNamespace.CellDocument.Cell[] result = new noNamespace.CellDocument.Cell[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "cell" element
         */
        public noNamespace.CellDocument.Cell getCellArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CellDocument.Cell target = null;
                target = (noNamespace.CellDocument.Cell)get_store().find_element_user(CELL$12, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "cell" element
         */
        public int sizeOfCellArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CELL$12);
            }
        }
        
        /**
         * Sets array of all "cell" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setCellArray(noNamespace.CellDocument.Cell[] cellArray)
        {
            check_orphaned();
            arraySetterHelper(cellArray, CELL$12);
        }
        
        /**
         * Sets ith "cell" element
         */
        public void setCellArray(int i, noNamespace.CellDocument.Cell cell)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CellDocument.Cell target = null;
                target = (noNamespace.CellDocument.Cell)get_store().find_element_user(CELL$12, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(cell);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "cell" element
         */
        public noNamespace.CellDocument.Cell insertNewCell(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CellDocument.Cell target = null;
                target = (noNamespace.CellDocument.Cell)get_store().insert_element_user(CELL$12, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "cell" element
         */
        public noNamespace.CellDocument.Cell addNewCell()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CellDocument.Cell target = null;
                target = (noNamespace.CellDocument.Cell)get_store().add_element_user(CELL$12);
                return target;
            }
        }
        
        /**
         * Removes the ith "cell" element
         */
        public void removeCell(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CELL$12, i);
            }
        }
        
        /**
         * Gets array of all "uText" elements
         */
        public noNamespace.UTextDocument.UText[] getUTextArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(UTEXT$14, targetList);
                noNamespace.UTextDocument.UText[] result = new noNamespace.UTextDocument.UText[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "uText" element
         */
        public noNamespace.UTextDocument.UText getUTextArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.UTextDocument.UText target = null;
                target = (noNamespace.UTextDocument.UText)get_store().find_element_user(UTEXT$14, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "uText" element
         */
        public int sizeOfUTextArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(UTEXT$14);
            }
        }
        
        /**
         * Sets array of all "uText" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setUTextArray(noNamespace.UTextDocument.UText[] uTextArray)
        {
            check_orphaned();
            arraySetterHelper(uTextArray, UTEXT$14);
        }
        
        /**
         * Sets ith "uText" element
         */
        public void setUTextArray(int i, noNamespace.UTextDocument.UText uText)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.UTextDocument.UText target = null;
                target = (noNamespace.UTextDocument.UText)get_store().find_element_user(UTEXT$14, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(uText);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "uText" element
         */
        public noNamespace.UTextDocument.UText insertNewUText(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.UTextDocument.UText target = null;
                target = (noNamespace.UTextDocument.UText)get_store().insert_element_user(UTEXT$14, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "uText" element
         */
        public noNamespace.UTextDocument.UText addNewUText()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.UTextDocument.UText target = null;
                target = (noNamespace.UTextDocument.UText)get_store().add_element_user(UTEXT$14);
                return target;
            }
        }
        
        /**
         * Removes the ith "uText" element
         */
        public void removeUText(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(UTEXT$14, i);
            }
        }
        
        /**
         * Gets array of all "lText" elements
         */
        public noNamespace.LTextDocument.LText[] getLTextArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(LTEXT$16, targetList);
                noNamespace.LTextDocument.LText[] result = new noNamespace.LTextDocument.LText[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "lText" element
         */
        public noNamespace.LTextDocument.LText getLTextArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LTextDocument.LText target = null;
                target = (noNamespace.LTextDocument.LText)get_store().find_element_user(LTEXT$16, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "lText" element
         */
        public int sizeOfLTextArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(LTEXT$16);
            }
        }
        
        /**
         * Sets array of all "lText" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setLTextArray(noNamespace.LTextDocument.LText[] lTextArray)
        {
            check_orphaned();
            arraySetterHelper(lTextArray, LTEXT$16);
        }
        
        /**
         * Sets ith "lText" element
         */
        public void setLTextArray(int i, noNamespace.LTextDocument.LText lText)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LTextDocument.LText target = null;
                target = (noNamespace.LTextDocument.LText)get_store().find_element_user(LTEXT$16, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(lText);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "lText" element
         */
        public noNamespace.LTextDocument.LText insertNewLText(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LTextDocument.LText target = null;
                target = (noNamespace.LTextDocument.LText)get_store().insert_element_user(LTEXT$16, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "lText" element
         */
        public noNamespace.LTextDocument.LText addNewLText()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LTextDocument.LText target = null;
                target = (noNamespace.LTextDocument.LText)get_store().add_element_user(LTEXT$16);
                return target;
            }
        }
        
        /**
         * Removes the ith "lText" element
         */
        public void removeLText(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(LTEXT$16, i);
            }
        }
        
        /**
         * Gets array of all "choice" elements
         */
        public noNamespace.ChoiceDocument.Choice[] getChoiceArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(CHOICE$18, targetList);
                noNamespace.ChoiceDocument.Choice[] result = new noNamespace.ChoiceDocument.Choice[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "choice" element
         */
        public noNamespace.ChoiceDocument.Choice getChoiceArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoiceDocument.Choice target = null;
                target = (noNamespace.ChoiceDocument.Choice)get_store().find_element_user(CHOICE$18, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "choice" element
         */
        public int sizeOfChoiceArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CHOICE$18);
            }
        }
        
        /**
         * Sets array of all "choice" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setChoiceArray(noNamespace.ChoiceDocument.Choice[] choiceArray)
        {
            check_orphaned();
            arraySetterHelper(choiceArray, CHOICE$18);
        }
        
        /**
         * Sets ith "choice" element
         */
        public void setChoiceArray(int i, noNamespace.ChoiceDocument.Choice choice)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoiceDocument.Choice target = null;
                target = (noNamespace.ChoiceDocument.Choice)get_store().find_element_user(CHOICE$18, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(choice);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "choice" element
         */
        public noNamespace.ChoiceDocument.Choice insertNewChoice(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoiceDocument.Choice target = null;
                target = (noNamespace.ChoiceDocument.Choice)get_store().insert_element_user(CHOICE$18, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "choice" element
         */
        public noNamespace.ChoiceDocument.Choice addNewChoice()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoiceDocument.Choice target = null;
                target = (noNamespace.ChoiceDocument.Choice)get_store().add_element_user(CHOICE$18);
                return target;
            }
        }
        
        /**
         * Removes the ith "choice" element
         */
        public void removeChoice(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CHOICE$18, i);
            }
        }
        
        /**
         * Gets array of all "missing" elements
         */
        public noNamespace.MissingDocument.Missing[] getMissingArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(MISSING$20, targetList);
                noNamespace.MissingDocument.Missing[] result = new noNamespace.MissingDocument.Missing[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "missing" element
         */
        public noNamespace.MissingDocument.Missing getMissingArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().find_element_user(MISSING$20, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "missing" element
         */
        public int sizeOfMissingArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(MISSING$20);
            }
        }
        
        /**
         * Sets array of all "missing" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setMissingArray(noNamespace.MissingDocument.Missing[] missingArray)
        {
            check_orphaned();
            arraySetterHelper(missingArray, MISSING$20);
        }
        
        /**
         * Sets ith "missing" element
         */
        public void setMissingArray(int i, noNamespace.MissingDocument.Missing missing)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().find_element_user(MISSING$20, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(missing);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "missing" element
         */
        public noNamespace.MissingDocument.Missing insertNewMissing(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().insert_element_user(MISSING$20, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "missing" element
         */
        public noNamespace.MissingDocument.Missing addNewMissing()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().add_element_user(MISSING$20);
                return target;
            }
        }
        
        /**
         * Removes the ith "missing" element
         */
        public void removeMissing(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(MISSING$20, i);
            }
        }
        
        /**
         * Gets array of all "br" elements
         */
        public noNamespace.BrDocument.Br[] getBrArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(BR$22, targetList);
                noNamespace.BrDocument.Br[] result = new noNamespace.BrDocument.Br[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "br" element
         */
        public noNamespace.BrDocument.Br getBrArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().find_element_user(BR$22, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "br" element
         */
        public int sizeOfBrArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(BR$22);
            }
        }
        
        /**
         * Sets array of all "br" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setBrArray(noNamespace.BrDocument.Br[] brArray)
        {
            check_orphaned();
            arraySetterHelper(brArray, BR$22);
        }
        
        /**
         * Sets ith "br" element
         */
        public void setBrArray(int i, noNamespace.BrDocument.Br br)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().find_element_user(BR$22, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(br);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "br" element
         */
        public noNamespace.BrDocument.Br insertNewBr(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().insert_element_user(BR$22, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "br" element
         */
        public noNamespace.BrDocument.Br addNewBr()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().add_element_user(BR$22);
                return target;
            }
        }
        
        /**
         * Removes the ith "br" element
         */
        public void removeBr(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(BR$22, i);
            }
        }
        
        /**
         * Gets array of all "label" elements
         */
        public noNamespace.LabelDocument.Label[] getLabelArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(LABEL$24, targetList);
                noNamespace.LabelDocument.Label[] result = new noNamespace.LabelDocument.Label[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "label" element
         */
        public noNamespace.LabelDocument.Label getLabelArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().find_element_user(LABEL$24, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "label" element
         */
        public int sizeOfLabelArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(LABEL$24);
            }
        }
        
        /**
         * Sets array of all "label" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setLabelArray(noNamespace.LabelDocument.Label[] labelArray)
        {
            check_orphaned();
            arraySetterHelper(labelArray, LABEL$24);
        }
        
        /**
         * Sets ith "label" element
         */
        public void setLabelArray(int i, noNamespace.LabelDocument.Label label)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().find_element_user(LABEL$24, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(label);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "label" element
         */
        public noNamespace.LabelDocument.Label insertNewLabel(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().insert_element_user(LABEL$24, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "label" element
         */
        public noNamespace.LabelDocument.Label addNewLabel()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().add_element_user(LABEL$24);
                return target;
            }
        }
        
        /**
         * Removes the ith "label" element
         */
        public void removeLabel(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(LABEL$24, i);
            }
        }
        
        /**
         * Gets array of all "formula" elements
         */
        public noNamespace.FormulaDocument.Formula[] getFormulaArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(FORMULA$26, targetList);
                noNamespace.FormulaDocument.Formula[] result = new noNamespace.FormulaDocument.Formula[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "formula" element
         */
        public noNamespace.FormulaDocument.Formula getFormulaArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.FormulaDocument.Formula target = null;
                target = (noNamespace.FormulaDocument.Formula)get_store().find_element_user(FORMULA$26, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "formula" element
         */
        public int sizeOfFormulaArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(FORMULA$26);
            }
        }
        
        /**
         * Sets array of all "formula" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setFormulaArray(noNamespace.FormulaDocument.Formula[] formulaArray)
        {
            check_orphaned();
            arraySetterHelper(formulaArray, FORMULA$26);
        }
        
        /**
         * Sets ith "formula" element
         */
        public void setFormulaArray(int i, noNamespace.FormulaDocument.Formula formula)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.FormulaDocument.Formula target = null;
                target = (noNamespace.FormulaDocument.Formula)get_store().find_element_user(FORMULA$26, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(formula);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "formula" element
         */
        public noNamespace.FormulaDocument.Formula insertNewFormula(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.FormulaDocument.Formula target = null;
                target = (noNamespace.FormulaDocument.Formula)get_store().insert_element_user(FORMULA$26, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "formula" element
         */
        public noNamespace.FormulaDocument.Formula addNewFormula()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.FormulaDocument.Formula target = null;
                target = (noNamespace.FormulaDocument.Formula)get_store().add_element_user(FORMULA$26);
                return target;
            }
        }
        
        /**
         * Removes the ith "formula" element
         */
        public void removeFormula(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(FORMULA$26, i);
            }
        }
        
        /**
         * Gets array of all "quote" elements
         */
        public noNamespace.QuoteDocument.Quote[] getQuoteArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(QUOTE$28, targetList);
                noNamespace.QuoteDocument.Quote[] result = new noNamespace.QuoteDocument.Quote[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "quote" element
         */
        public noNamespace.QuoteDocument.Quote getQuoteArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuoteDocument.Quote target = null;
                target = (noNamespace.QuoteDocument.Quote)get_store().find_element_user(QUOTE$28, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "quote" element
         */
        public int sizeOfQuoteArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(QUOTE$28);
            }
        }
        
        /**
         * Sets array of all "quote" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setQuoteArray(noNamespace.QuoteDocument.Quote[] quoteArray)
        {
            check_orphaned();
            arraySetterHelper(quoteArray, QUOTE$28);
        }
        
        /**
         * Sets ith "quote" element
         */
        public void setQuoteArray(int i, noNamespace.QuoteDocument.Quote quote)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuoteDocument.Quote target = null;
                target = (noNamespace.QuoteDocument.Quote)get_store().find_element_user(QUOTE$28, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(quote);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "quote" element
         */
        public noNamespace.QuoteDocument.Quote insertNewQuote(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuoteDocument.Quote target = null;
                target = (noNamespace.QuoteDocument.Quote)get_store().insert_element_user(QUOTE$28, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "quote" element
         */
        public noNamespace.QuoteDocument.Quote addNewQuote()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuoteDocument.Quote target = null;
                target = (noNamespace.QuoteDocument.Quote)get_store().add_element_user(QUOTE$28);
                return target;
            }
        }
        
        /**
         * Removes the ith "quote" element
         */
        public void removeQuote(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(QUOTE$28, i);
            }
        }
        
        /**
         * Gets array of all "source" elements
         */
        public noNamespace.SourceDocument.Source[] getSourceArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(SOURCE$30, targetList);
                noNamespace.SourceDocument.Source[] result = new noNamespace.SourceDocument.Source[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "source" element
         */
        public noNamespace.SourceDocument.Source getSourceArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.SourceDocument.Source target = null;
                target = (noNamespace.SourceDocument.Source)get_store().find_element_user(SOURCE$30, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "source" element
         */
        public int sizeOfSourceArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(SOURCE$30);
            }
        }
        
        /**
         * Sets array of all "source" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setSourceArray(noNamespace.SourceDocument.Source[] sourceArray)
        {
            check_orphaned();
            arraySetterHelper(sourceArray, SOURCE$30);
        }
        
        /**
         * Sets ith "source" element
         */
        public void setSourceArray(int i, noNamespace.SourceDocument.Source source)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.SourceDocument.Source target = null;
                target = (noNamespace.SourceDocument.Source)get_store().find_element_user(SOURCE$30, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(source);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "source" element
         */
        public noNamespace.SourceDocument.Source insertNewSource(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.SourceDocument.Source target = null;
                target = (noNamespace.SourceDocument.Source)get_store().insert_element_user(SOURCE$30, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "source" element
         */
        public noNamespace.SourceDocument.Source addNewSource()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.SourceDocument.Source target = null;
                target = (noNamespace.SourceDocument.Source)get_store().add_element_user(SOURCE$30);
                return target;
            }
        }
        
        /**
         * Removes the ith "source" element
         */
        public void removeSource(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(SOURCE$30, i);
            }
        }
        
        /**
         * Gets the "comment" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType getComment()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().find_attribute_user(COMMENT$32);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "comment" attribute
         */
        public boolean isSetComment()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(COMMENT$32) != null;
            }
        }
        
        /**
         * Sets the "comment" attribute
         */
        public void setComment(org.apache.xmlbeans.XmlAnySimpleType comment)
        {
            generatedSetterHelperImpl(comment, COMMENT$32, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "comment" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType addNewComment()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().add_attribute_user(COMMENT$32);
                return target;
            }
        }
        
        /**
         * Unsets the "comment" attribute
         */
        public void unsetComment()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(COMMENT$32);
            }
        }
        
        /**
         * Gets the "anscol" attribute
         */
        public java.util.List getAnscol()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(ANSCOL$34);
                if (target == null)
                {
                    return null;
                }
                return target.getListValue();
            }
        }
        
        /**
         * Gets (as xml) the "anscol" attribute
         */
        public org.apache.xmlbeans.XmlIDREFS xgetAnscol()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlIDREFS target = null;
                target = (org.apache.xmlbeans.XmlIDREFS)get_store().find_attribute_user(ANSCOL$34);
                return target;
            }
        }
        
        /**
         * True if has "anscol" attribute
         */
        public boolean isSetAnscol()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(ANSCOL$34) != null;
            }
        }
        
        /**
         * Sets the "anscol" attribute
         */
        public void setAnscol(java.util.List anscol)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(ANSCOL$34);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(ANSCOL$34);
                }
                target.setListValue(anscol);
            }
        }
        
        /**
         * Sets (as xml) the "anscol" attribute
         */
        public void xsetAnscol(org.apache.xmlbeans.XmlIDREFS anscol)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlIDREFS target = null;
                target = (org.apache.xmlbeans.XmlIDREFS)get_store().find_attribute_user(ANSCOL$34);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlIDREFS)get_store().add_attribute_user(ANSCOL$34);
                }
                target.set(anscol);
            }
        }
        
        /**
         * Unsets the "anscol" attribute
         */
        public void unsetAnscol()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(ANSCOL$34);
            }
        }
    }
}
