/*
 * An XML document type.
 * Localname: question
 * Namespace: 
 * Java type: QuestionDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.answerTable.impl;

import org.kachako.components.centerexam.xmlschema.answerTable.QuestionDocument;

/**
 * A document containing one question(@) element.
 *
 * This is a complex type.
 */
public class QuestionDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements QuestionDocument
{
    private static final long serialVersionUID = 1L;
    
    public QuestionDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName QUESTION$0 = 
        new javax.xml.namespace.QName("", "question");
    
    
    /**
     * Gets the "question" element
     */
    public java.lang.String getQuestion()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUESTION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "question" element
     */
    public org.apache.xmlbeans.XmlString xgetQuestion()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(QUESTION$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "question" element
     */
    public void setQuestion(java.lang.String question)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUESTION$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(QUESTION$0);
            }
            target.setStringValue(question);
        }
    }
    
    /**
     * Sets (as xml) the "question" element
     */
    public void xsetQuestion(org.apache.xmlbeans.XmlString question)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(QUESTION$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(QUESTION$0);
            }
            target.set(question);
        }
    }
}
