/*
 * An XML document type.
 * Localname: ref
 * Namespace: 
 * Java type: noNamespace.RefDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.centerExam;


/**
 * A document containing one ref(@) element.
 *
 * This is a complex type.
 */
public interface RefDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(RefDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s9AD9431611720D569E6CDA08F9EDE660").resolveHandle("refb17adoctype");
    
    /**
     * Gets the "ref" element
     */
    noNamespace.RefDocument.Ref getRef();
    
    /**
     * Sets the "ref" element
     */
    void setRef(noNamespace.RefDocument.Ref ref);
    
    /**
     * Appends and returns a new empty "ref" element
     */
    noNamespace.RefDocument.Ref addNewRef();
    
    /**
     * An XML ref(@).
     *
     * This is a complex type.
     */
    public interface Ref extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Ref.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s9AD9431611720D569E6CDA08F9EDE660").resolveHandle("refab49elemtype");
        
        /**
         * Gets array of all "garbled" elements
         */
        noNamespace.GarbledDocument.Garbled[] getGarbledArray();
        
        /**
         * Gets ith "garbled" element
         */
        noNamespace.GarbledDocument.Garbled getGarbledArray(int i);
        
        /**
         * Returns number of "garbled" element
         */
        int sizeOfGarbledArray();
        
        /**
         * Sets array of all "garbled" element
         */
        void setGarbledArray(noNamespace.GarbledDocument.Garbled[] garbledArray);
        
        /**
         * Sets ith "garbled" element
         */
        void setGarbledArray(int i, noNamespace.GarbledDocument.Garbled garbled);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "garbled" element
         */
        noNamespace.GarbledDocument.Garbled insertNewGarbled(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "garbled" element
         */
        noNamespace.GarbledDocument.Garbled addNewGarbled();
        
        /**
         * Removes the ith "garbled" element
         */
        void removeGarbled(int i);
        
        /**
         * Gets array of all "missing" elements
         */
        noNamespace.MissingDocument.Missing[] getMissingArray();
        
        /**
         * Gets ith "missing" element
         */
        noNamespace.MissingDocument.Missing getMissingArray(int i);
        
        /**
         * Returns number of "missing" element
         */
        int sizeOfMissingArray();
        
        /**
         * Sets array of all "missing" element
         */
        void setMissingArray(noNamespace.MissingDocument.Missing[] missingArray);
        
        /**
         * Sets ith "missing" element
         */
        void setMissingArray(int i, noNamespace.MissingDocument.Missing missing);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "missing" element
         */
        noNamespace.MissingDocument.Missing insertNewMissing(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "missing" element
         */
        noNamespace.MissingDocument.Missing addNewMissing();
        
        /**
         * Removes the ith "missing" element
         */
        void removeMissing(int i);
        
        /**
         * Gets array of all "br" elements
         */
        noNamespace.BrDocument.Br[] getBrArray();
        
        /**
         * Gets ith "br" element
         */
        noNamespace.BrDocument.Br getBrArray(int i);
        
        /**
         * Returns number of "br" element
         */
        int sizeOfBrArray();
        
        /**
         * Sets array of all "br" element
         */
        void setBrArray(noNamespace.BrDocument.Br[] brArray);
        
        /**
         * Sets ith "br" element
         */
        void setBrArray(int i, noNamespace.BrDocument.Br br);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "br" element
         */
        noNamespace.BrDocument.Br insertNewBr(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "br" element
         */
        noNamespace.BrDocument.Br addNewBr();
        
        /**
         * Removes the ith "br" element
         */
        void removeBr(int i);
        
        /**
         * Gets array of all "formula" elements
         */
        noNamespace.FormulaDocument.Formula[] getFormulaArray();
        
        /**
         * Gets ith "formula" element
         */
        noNamespace.FormulaDocument.Formula getFormulaArray(int i);
        
        /**
         * Returns number of "formula" element
         */
        int sizeOfFormulaArray();
        
        /**
         * Sets array of all "formula" element
         */
        void setFormulaArray(noNamespace.FormulaDocument.Formula[] formulaArray);
        
        /**
         * Sets ith "formula" element
         */
        void setFormulaArray(int i, noNamespace.FormulaDocument.Formula formula);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "formula" element
         */
        noNamespace.FormulaDocument.Formula insertNewFormula(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "formula" element
         */
        noNamespace.FormulaDocument.Formula addNewFormula();
        
        /**
         * Removes the ith "formula" element
         */
        void removeFormula(int i);
        
        /**
         * Gets array of all "uText" elements
         */
        noNamespace.UTextDocument.UText[] getUTextArray();
        
        /**
         * Gets ith "uText" element
         */
        noNamespace.UTextDocument.UText getUTextArray(int i);
        
        /**
         * Returns number of "uText" element
         */
        int sizeOfUTextArray();
        
        /**
         * Sets array of all "uText" element
         */
        void setUTextArray(noNamespace.UTextDocument.UText[] uTextArray);
        
        /**
         * Sets ith "uText" element
         */
        void setUTextArray(int i, noNamespace.UTextDocument.UText uText);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "uText" element
         */
        noNamespace.UTextDocument.UText insertNewUText(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "uText" element
         */
        noNamespace.UTextDocument.UText addNewUText();
        
        /**
         * Removes the ith "uText" element
         */
        void removeUText(int i);
        
        /**
         * Gets array of all "lText" elements
         */
        noNamespace.LTextDocument.LText[] getLTextArray();
        
        /**
         * Gets ith "lText" element
         */
        noNamespace.LTextDocument.LText getLTextArray(int i);
        
        /**
         * Returns number of "lText" element
         */
        int sizeOfLTextArray();
        
        /**
         * Sets array of all "lText" element
         */
        void setLTextArray(noNamespace.LTextDocument.LText[] lTextArray);
        
        /**
         * Sets ith "lText" element
         */
        void setLTextArray(int i, noNamespace.LTextDocument.LText lText);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "lText" element
         */
        noNamespace.LTextDocument.LText insertNewLText(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "lText" element
         */
        noNamespace.LTextDocument.LText addNewLText();
        
        /**
         * Removes the ith "lText" element
         */
        void removeLText(int i);
        
        /**
         * Gets the "comment" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType getComment();
        
        /**
         * True if has "comment" attribute
         */
        boolean isSetComment();
        
        /**
         * Sets the "comment" attribute
         */
        void setComment(org.apache.xmlbeans.XmlAnySimpleType comment);
        
        /**
         * Appends and returns a new empty "comment" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType addNewComment();
        
        /**
         * Unsets the "comment" attribute
         */
        void unsetComment();
        
        /**
         * Gets the "target" attribute
         */
        java.lang.String getTarget();
        
        /**
         * Gets (as xml) the "target" attribute
         */
        org.apache.xmlbeans.XmlIDREF xgetTarget();
        
        /**
         * True if has "target" attribute
         */
        boolean isSetTarget();
        
        /**
         * Sets the "target" attribute
         */
        void setTarget(java.lang.String target);
        
        /**
         * Sets (as xml) the "target" attribute
         */
        void xsetTarget(org.apache.xmlbeans.XmlIDREF target);
        
        /**
         * Unsets the "target" attribute
         */
        void unsetTarget();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static noNamespace.RefDocument.Ref newInstance() {
              return (noNamespace.RefDocument.Ref) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static noNamespace.RefDocument.Ref newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (noNamespace.RefDocument.Ref) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static noNamespace.RefDocument newInstance() {
          return (noNamespace.RefDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static noNamespace.RefDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (noNamespace.RefDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static noNamespace.RefDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.RefDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static noNamespace.RefDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.RefDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static noNamespace.RefDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.RefDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static noNamespace.RefDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.RefDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static noNamespace.RefDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.RefDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static noNamespace.RefDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.RefDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static noNamespace.RefDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.RefDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static noNamespace.RefDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.RefDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static noNamespace.RefDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.RefDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static noNamespace.RefDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.RefDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static noNamespace.RefDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.RefDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static noNamespace.RefDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.RefDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static noNamespace.RefDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.RefDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static noNamespace.RefDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.RefDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static noNamespace.RefDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (noNamespace.RefDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static noNamespace.RefDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (noNamespace.RefDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
