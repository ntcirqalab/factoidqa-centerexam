/*
 * An XML document type.
 * Localname: formula
 * Namespace: 
 * Java type: noNamespace.FormulaDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.centerExam.impl;
/**
 * A document containing one formula(@) element.
 *
 * This is a complex type.
 */
public class FormulaDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.FormulaDocument
{
    private static final long serialVersionUID = 1L;
    
    public FormulaDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName FORMULA$0 = 
        new javax.xml.namespace.QName("", "formula");
    
    
    /**
     * Gets the "formula" element
     */
    public noNamespace.FormulaDocument.Formula getFormula()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.FormulaDocument.Formula target = null;
            target = (noNamespace.FormulaDocument.Formula)get_store().find_element_user(FORMULA$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "formula" element
     */
    public void setFormula(noNamespace.FormulaDocument.Formula formula)
    {
        generatedSetterHelperImpl(formula, FORMULA$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "formula" element
     */
    public noNamespace.FormulaDocument.Formula addNewFormula()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.FormulaDocument.Formula target = null;
            target = (noNamespace.FormulaDocument.Formula)get_store().add_element_user(FORMULA$0);
            return target;
        }
    }
    /**
     * An XML formula(@).
     *
     * This is a complex type.
     */
    public static class FormulaImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.FormulaDocument.Formula
    {
        private static final long serialVersionUID = 1L;
        
        public FormulaImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName COMMENT$0 = 
            new javax.xml.namespace.QName("", "comment");
        
        
        /**
         * Gets the "comment" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType getComment()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().find_attribute_user(COMMENT$0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "comment" attribute
         */
        public boolean isSetComment()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(COMMENT$0) != null;
            }
        }
        
        /**
         * Sets the "comment" attribute
         */
        public void setComment(org.apache.xmlbeans.XmlAnySimpleType comment)
        {
            generatedSetterHelperImpl(comment, COMMENT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "comment" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType addNewComment()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().add_attribute_user(COMMENT$0);
                return target;
            }
        }
        
        /**
         * Unsets the "comment" attribute
         */
        public void unsetComment()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(COMMENT$0);
            }
        }
    }
}
