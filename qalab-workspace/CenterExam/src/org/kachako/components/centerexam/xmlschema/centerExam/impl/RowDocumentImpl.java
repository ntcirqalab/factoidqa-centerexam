/*
 * An XML document type.
 * Localname: row
 * Namespace: 
 * Java type: noNamespace.RowDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.centerExam.impl;
/**
 * A document containing one row(@) element.
 *
 * This is a complex type.
 */
public class RowDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.RowDocument
{
    private static final long serialVersionUID = 1L;
    
    public RowDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ROW$0 = 
        new javax.xml.namespace.QName("", "row");
    
    
    /**
     * Gets the "row" element
     */
    public noNamespace.RowDocument.Row getRow()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.RowDocument.Row target = null;
            target = (noNamespace.RowDocument.Row)get_store().find_element_user(ROW$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "row" element
     */
    public void setRow(noNamespace.RowDocument.Row row)
    {
        generatedSetterHelperImpl(row, ROW$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "row" element
     */
    public noNamespace.RowDocument.Row addNewRow()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.RowDocument.Row target = null;
            target = (noNamespace.RowDocument.Row)get_store().add_element_user(ROW$0);
            return target;
        }
    }
    /**
     * An XML row(@).
     *
     * This is a complex type.
     */
    public static class RowImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.RowDocument.Row
    {
        private static final long serialVersionUID = 1L;
        
        public RowImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName CELL$0 = 
            new javax.xml.namespace.QName("", "cell");
        private static final javax.xml.namespace.QName LABEL$2 = 
            new javax.xml.namespace.QName("", "label");
        private static final javax.xml.namespace.QName CHOICE$4 = 
            new javax.xml.namespace.QName("", "choice");
        private static final javax.xml.namespace.QName BR$6 = 
            new javax.xml.namespace.QName("", "br");
        private static final javax.xml.namespace.QName BLANK$8 = 
            new javax.xml.namespace.QName("", "blank");
        private static final javax.xml.namespace.QName REF$10 = 
            new javax.xml.namespace.QName("", "ref");
        private static final javax.xml.namespace.QName CNUM$12 = 
            new javax.xml.namespace.QName("", "cNum");
        private static final javax.xml.namespace.QName UTEXT$14 = 
            new javax.xml.namespace.QName("", "uText");
        private static final javax.xml.namespace.QName CHOICES$16 = 
            new javax.xml.namespace.QName("", "choices");
        private static final javax.xml.namespace.QName FORMULA$18 = 
            new javax.xml.namespace.QName("", "formula");
        private static final javax.xml.namespace.QName QUOTE$20 = 
            new javax.xml.namespace.QName("", "quote");
        private static final javax.xml.namespace.QName SOURCE$22 = 
            new javax.xml.namespace.QName("", "source");
        
        
        /**
         * Gets array of all "cell" elements
         */
        public noNamespace.CellDocument.Cell[] getCellArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(CELL$0, targetList);
                noNamespace.CellDocument.Cell[] result = new noNamespace.CellDocument.Cell[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "cell" element
         */
        public noNamespace.CellDocument.Cell getCellArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CellDocument.Cell target = null;
                target = (noNamespace.CellDocument.Cell)get_store().find_element_user(CELL$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "cell" element
         */
        public int sizeOfCellArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CELL$0);
            }
        }
        
        /**
         * Sets array of all "cell" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setCellArray(noNamespace.CellDocument.Cell[] cellArray)
        {
            check_orphaned();
            arraySetterHelper(cellArray, CELL$0);
        }
        
        /**
         * Sets ith "cell" element
         */
        public void setCellArray(int i, noNamespace.CellDocument.Cell cell)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CellDocument.Cell target = null;
                target = (noNamespace.CellDocument.Cell)get_store().find_element_user(CELL$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(cell);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "cell" element
         */
        public noNamespace.CellDocument.Cell insertNewCell(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CellDocument.Cell target = null;
                target = (noNamespace.CellDocument.Cell)get_store().insert_element_user(CELL$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "cell" element
         */
        public noNamespace.CellDocument.Cell addNewCell()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CellDocument.Cell target = null;
                target = (noNamespace.CellDocument.Cell)get_store().add_element_user(CELL$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "cell" element
         */
        public void removeCell(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CELL$0, i);
            }
        }
        
        /**
         * Gets array of all "label" elements
         */
        public noNamespace.LabelDocument.Label[] getLabelArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(LABEL$2, targetList);
                noNamespace.LabelDocument.Label[] result = new noNamespace.LabelDocument.Label[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "label" element
         */
        public noNamespace.LabelDocument.Label getLabelArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().find_element_user(LABEL$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "label" element
         */
        public int sizeOfLabelArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(LABEL$2);
            }
        }
        
        /**
         * Sets array of all "label" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setLabelArray(noNamespace.LabelDocument.Label[] labelArray)
        {
            check_orphaned();
            arraySetterHelper(labelArray, LABEL$2);
        }
        
        /**
         * Sets ith "label" element
         */
        public void setLabelArray(int i, noNamespace.LabelDocument.Label label)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().find_element_user(LABEL$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(label);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "label" element
         */
        public noNamespace.LabelDocument.Label insertNewLabel(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().insert_element_user(LABEL$2, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "label" element
         */
        public noNamespace.LabelDocument.Label addNewLabel()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().add_element_user(LABEL$2);
                return target;
            }
        }
        
        /**
         * Removes the ith "label" element
         */
        public void removeLabel(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(LABEL$2, i);
            }
        }
        
        /**
         * Gets array of all "choice" elements
         */
        public noNamespace.ChoiceDocument.Choice[] getChoiceArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(CHOICE$4, targetList);
                noNamespace.ChoiceDocument.Choice[] result = new noNamespace.ChoiceDocument.Choice[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "choice" element
         */
        public noNamespace.ChoiceDocument.Choice getChoiceArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoiceDocument.Choice target = null;
                target = (noNamespace.ChoiceDocument.Choice)get_store().find_element_user(CHOICE$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "choice" element
         */
        public int sizeOfChoiceArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CHOICE$4);
            }
        }
        
        /**
         * Sets array of all "choice" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setChoiceArray(noNamespace.ChoiceDocument.Choice[] choiceArray)
        {
            check_orphaned();
            arraySetterHelper(choiceArray, CHOICE$4);
        }
        
        /**
         * Sets ith "choice" element
         */
        public void setChoiceArray(int i, noNamespace.ChoiceDocument.Choice choice)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoiceDocument.Choice target = null;
                target = (noNamespace.ChoiceDocument.Choice)get_store().find_element_user(CHOICE$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(choice);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "choice" element
         */
        public noNamespace.ChoiceDocument.Choice insertNewChoice(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoiceDocument.Choice target = null;
                target = (noNamespace.ChoiceDocument.Choice)get_store().insert_element_user(CHOICE$4, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "choice" element
         */
        public noNamespace.ChoiceDocument.Choice addNewChoice()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoiceDocument.Choice target = null;
                target = (noNamespace.ChoiceDocument.Choice)get_store().add_element_user(CHOICE$4);
                return target;
            }
        }
        
        /**
         * Removes the ith "choice" element
         */
        public void removeChoice(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CHOICE$4, i);
            }
        }
        
        /**
         * Gets array of all "br" elements
         */
        public noNamespace.BrDocument.Br[] getBrArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(BR$6, targetList);
                noNamespace.BrDocument.Br[] result = new noNamespace.BrDocument.Br[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "br" element
         */
        public noNamespace.BrDocument.Br getBrArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().find_element_user(BR$6, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "br" element
         */
        public int sizeOfBrArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(BR$6);
            }
        }
        
        /**
         * Sets array of all "br" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setBrArray(noNamespace.BrDocument.Br[] brArray)
        {
            check_orphaned();
            arraySetterHelper(brArray, BR$6);
        }
        
        /**
         * Sets ith "br" element
         */
        public void setBrArray(int i, noNamespace.BrDocument.Br br)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().find_element_user(BR$6, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(br);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "br" element
         */
        public noNamespace.BrDocument.Br insertNewBr(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().insert_element_user(BR$6, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "br" element
         */
        public noNamespace.BrDocument.Br addNewBr()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().add_element_user(BR$6);
                return target;
            }
        }
        
        /**
         * Removes the ith "br" element
         */
        public void removeBr(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(BR$6, i);
            }
        }
        
        /**
         * Gets array of all "blank" elements
         */
        public noNamespace.BlankDocument.Blank[] getBlankArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(BLANK$8, targetList);
                noNamespace.BlankDocument.Blank[] result = new noNamespace.BlankDocument.Blank[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "blank" element
         */
        public noNamespace.BlankDocument.Blank getBlankArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BlankDocument.Blank target = null;
                target = (noNamespace.BlankDocument.Blank)get_store().find_element_user(BLANK$8, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "blank" element
         */
        public int sizeOfBlankArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(BLANK$8);
            }
        }
        
        /**
         * Sets array of all "blank" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setBlankArray(noNamespace.BlankDocument.Blank[] blankArray)
        {
            check_orphaned();
            arraySetterHelper(blankArray, BLANK$8);
        }
        
        /**
         * Sets ith "blank" element
         */
        public void setBlankArray(int i, noNamespace.BlankDocument.Blank blank)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BlankDocument.Blank target = null;
                target = (noNamespace.BlankDocument.Blank)get_store().find_element_user(BLANK$8, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(blank);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "blank" element
         */
        public noNamespace.BlankDocument.Blank insertNewBlank(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BlankDocument.Blank target = null;
                target = (noNamespace.BlankDocument.Blank)get_store().insert_element_user(BLANK$8, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "blank" element
         */
        public noNamespace.BlankDocument.Blank addNewBlank()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BlankDocument.Blank target = null;
                target = (noNamespace.BlankDocument.Blank)get_store().add_element_user(BLANK$8);
                return target;
            }
        }
        
        /**
         * Removes the ith "blank" element
         */
        public void removeBlank(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(BLANK$8, i);
            }
        }
        
        /**
         * Gets array of all "ref" elements
         */
        public noNamespace.RefDocument.Ref[] getRefArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(REF$10, targetList);
                noNamespace.RefDocument.Ref[] result = new noNamespace.RefDocument.Ref[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "ref" element
         */
        public noNamespace.RefDocument.Ref getRefArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().find_element_user(REF$10, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "ref" element
         */
        public int sizeOfRefArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(REF$10);
            }
        }
        
        /**
         * Sets array of all "ref" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setRefArray(noNamespace.RefDocument.Ref[] refArray)
        {
            check_orphaned();
            arraySetterHelper(refArray, REF$10);
        }
        
        /**
         * Sets ith "ref" element
         */
        public void setRefArray(int i, noNamespace.RefDocument.Ref ref)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().find_element_user(REF$10, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(ref);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ref" element
         */
        public noNamespace.RefDocument.Ref insertNewRef(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().insert_element_user(REF$10, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ref" element
         */
        public noNamespace.RefDocument.Ref addNewRef()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().add_element_user(REF$10);
                return target;
            }
        }
        
        /**
         * Removes the ith "ref" element
         */
        public void removeRef(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(REF$10, i);
            }
        }
        
        /**
         * Gets array of all "cNum" elements
         */
        public noNamespace.CNumDocument.CNum[] getCNumArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(CNUM$12, targetList);
                noNamespace.CNumDocument.CNum[] result = new noNamespace.CNumDocument.CNum[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "cNum" element
         */
        public noNamespace.CNumDocument.CNum getCNumArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CNumDocument.CNum target = null;
                target = (noNamespace.CNumDocument.CNum)get_store().find_element_user(CNUM$12, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "cNum" element
         */
        public int sizeOfCNumArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CNUM$12);
            }
        }
        
        /**
         * Sets array of all "cNum" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setCNumArray(noNamespace.CNumDocument.CNum[] cNumArray)
        {
            check_orphaned();
            arraySetterHelper(cNumArray, CNUM$12);
        }
        
        /**
         * Sets ith "cNum" element
         */
        public void setCNumArray(int i, noNamespace.CNumDocument.CNum cNum)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CNumDocument.CNum target = null;
                target = (noNamespace.CNumDocument.CNum)get_store().find_element_user(CNUM$12, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(cNum);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "cNum" element
         */
        public noNamespace.CNumDocument.CNum insertNewCNum(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CNumDocument.CNum target = null;
                target = (noNamespace.CNumDocument.CNum)get_store().insert_element_user(CNUM$12, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "cNum" element
         */
        public noNamespace.CNumDocument.CNum addNewCNum()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.CNumDocument.CNum target = null;
                target = (noNamespace.CNumDocument.CNum)get_store().add_element_user(CNUM$12);
                return target;
            }
        }
        
        /**
         * Removes the ith "cNum" element
         */
        public void removeCNum(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CNUM$12, i);
            }
        }
        
        /**
         * Gets array of all "uText" elements
         */
        public noNamespace.UTextDocument.UText[] getUTextArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(UTEXT$14, targetList);
                noNamespace.UTextDocument.UText[] result = new noNamespace.UTextDocument.UText[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "uText" element
         */
        public noNamespace.UTextDocument.UText getUTextArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.UTextDocument.UText target = null;
                target = (noNamespace.UTextDocument.UText)get_store().find_element_user(UTEXT$14, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "uText" element
         */
        public int sizeOfUTextArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(UTEXT$14);
            }
        }
        
        /**
         * Sets array of all "uText" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setUTextArray(noNamespace.UTextDocument.UText[] uTextArray)
        {
            check_orphaned();
            arraySetterHelper(uTextArray, UTEXT$14);
        }
        
        /**
         * Sets ith "uText" element
         */
        public void setUTextArray(int i, noNamespace.UTextDocument.UText uText)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.UTextDocument.UText target = null;
                target = (noNamespace.UTextDocument.UText)get_store().find_element_user(UTEXT$14, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(uText);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "uText" element
         */
        public noNamespace.UTextDocument.UText insertNewUText(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.UTextDocument.UText target = null;
                target = (noNamespace.UTextDocument.UText)get_store().insert_element_user(UTEXT$14, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "uText" element
         */
        public noNamespace.UTextDocument.UText addNewUText()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.UTextDocument.UText target = null;
                target = (noNamespace.UTextDocument.UText)get_store().add_element_user(UTEXT$14);
                return target;
            }
        }
        
        /**
         * Removes the ith "uText" element
         */
        public void removeUText(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(UTEXT$14, i);
            }
        }
        
        /**
         * Gets array of all "choices" elements
         */
        public noNamespace.ChoicesDocument.Choices[] getChoicesArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(CHOICES$16, targetList);
                noNamespace.ChoicesDocument.Choices[] result = new noNamespace.ChoicesDocument.Choices[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "choices" element
         */
        public noNamespace.ChoicesDocument.Choices getChoicesArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoicesDocument.Choices target = null;
                target = (noNamespace.ChoicesDocument.Choices)get_store().find_element_user(CHOICES$16, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "choices" element
         */
        public int sizeOfChoicesArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CHOICES$16);
            }
        }
        
        /**
         * Sets array of all "choices" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setChoicesArray(noNamespace.ChoicesDocument.Choices[] choicesArray)
        {
            check_orphaned();
            arraySetterHelper(choicesArray, CHOICES$16);
        }
        
        /**
         * Sets ith "choices" element
         */
        public void setChoicesArray(int i, noNamespace.ChoicesDocument.Choices choices)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoicesDocument.Choices target = null;
                target = (noNamespace.ChoicesDocument.Choices)get_store().find_element_user(CHOICES$16, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(choices);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "choices" element
         */
        public noNamespace.ChoicesDocument.Choices insertNewChoices(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoicesDocument.Choices target = null;
                target = (noNamespace.ChoicesDocument.Choices)get_store().insert_element_user(CHOICES$16, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "choices" element
         */
        public noNamespace.ChoicesDocument.Choices addNewChoices()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.ChoicesDocument.Choices target = null;
                target = (noNamespace.ChoicesDocument.Choices)get_store().add_element_user(CHOICES$16);
                return target;
            }
        }
        
        /**
         * Removes the ith "choices" element
         */
        public void removeChoices(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CHOICES$16, i);
            }
        }
        
        /**
         * Gets array of all "formula" elements
         */
        public noNamespace.FormulaDocument.Formula[] getFormulaArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(FORMULA$18, targetList);
                noNamespace.FormulaDocument.Formula[] result = new noNamespace.FormulaDocument.Formula[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "formula" element
         */
        public noNamespace.FormulaDocument.Formula getFormulaArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.FormulaDocument.Formula target = null;
                target = (noNamespace.FormulaDocument.Formula)get_store().find_element_user(FORMULA$18, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "formula" element
         */
        public int sizeOfFormulaArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(FORMULA$18);
            }
        }
        
        /**
         * Sets array of all "formula" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setFormulaArray(noNamespace.FormulaDocument.Formula[] formulaArray)
        {
            check_orphaned();
            arraySetterHelper(formulaArray, FORMULA$18);
        }
        
        /**
         * Sets ith "formula" element
         */
        public void setFormulaArray(int i, noNamespace.FormulaDocument.Formula formula)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.FormulaDocument.Formula target = null;
                target = (noNamespace.FormulaDocument.Formula)get_store().find_element_user(FORMULA$18, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(formula);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "formula" element
         */
        public noNamespace.FormulaDocument.Formula insertNewFormula(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.FormulaDocument.Formula target = null;
                target = (noNamespace.FormulaDocument.Formula)get_store().insert_element_user(FORMULA$18, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "formula" element
         */
        public noNamespace.FormulaDocument.Formula addNewFormula()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.FormulaDocument.Formula target = null;
                target = (noNamespace.FormulaDocument.Formula)get_store().add_element_user(FORMULA$18);
                return target;
            }
        }
        
        /**
         * Removes the ith "formula" element
         */
        public void removeFormula(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(FORMULA$18, i);
            }
        }
        
        /**
         * Gets array of all "quote" elements
         */
        public noNamespace.QuoteDocument.Quote[] getQuoteArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(QUOTE$20, targetList);
                noNamespace.QuoteDocument.Quote[] result = new noNamespace.QuoteDocument.Quote[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "quote" element
         */
        public noNamespace.QuoteDocument.Quote getQuoteArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuoteDocument.Quote target = null;
                target = (noNamespace.QuoteDocument.Quote)get_store().find_element_user(QUOTE$20, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "quote" element
         */
        public int sizeOfQuoteArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(QUOTE$20);
            }
        }
        
        /**
         * Sets array of all "quote" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setQuoteArray(noNamespace.QuoteDocument.Quote[] quoteArray)
        {
            check_orphaned();
            arraySetterHelper(quoteArray, QUOTE$20);
        }
        
        /**
         * Sets ith "quote" element
         */
        public void setQuoteArray(int i, noNamespace.QuoteDocument.Quote quote)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuoteDocument.Quote target = null;
                target = (noNamespace.QuoteDocument.Quote)get_store().find_element_user(QUOTE$20, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(quote);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "quote" element
         */
        public noNamespace.QuoteDocument.Quote insertNewQuote(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuoteDocument.Quote target = null;
                target = (noNamespace.QuoteDocument.Quote)get_store().insert_element_user(QUOTE$20, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "quote" element
         */
        public noNamespace.QuoteDocument.Quote addNewQuote()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.QuoteDocument.Quote target = null;
                target = (noNamespace.QuoteDocument.Quote)get_store().add_element_user(QUOTE$20);
                return target;
            }
        }
        
        /**
         * Removes the ith "quote" element
         */
        public void removeQuote(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(QUOTE$20, i);
            }
        }
        
        /**
         * Gets array of all "source" elements
         */
        public noNamespace.SourceDocument.Source[] getSourceArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(SOURCE$22, targetList);
                noNamespace.SourceDocument.Source[] result = new noNamespace.SourceDocument.Source[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "source" element
         */
        public noNamespace.SourceDocument.Source getSourceArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.SourceDocument.Source target = null;
                target = (noNamespace.SourceDocument.Source)get_store().find_element_user(SOURCE$22, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "source" element
         */
        public int sizeOfSourceArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(SOURCE$22);
            }
        }
        
        /**
         * Sets array of all "source" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setSourceArray(noNamespace.SourceDocument.Source[] sourceArray)
        {
            check_orphaned();
            arraySetterHelper(sourceArray, SOURCE$22);
        }
        
        /**
         * Sets ith "source" element
         */
        public void setSourceArray(int i, noNamespace.SourceDocument.Source source)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.SourceDocument.Source target = null;
                target = (noNamespace.SourceDocument.Source)get_store().find_element_user(SOURCE$22, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(source);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "source" element
         */
        public noNamespace.SourceDocument.Source insertNewSource(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.SourceDocument.Source target = null;
                target = (noNamespace.SourceDocument.Source)get_store().insert_element_user(SOURCE$22, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "source" element
         */
        public noNamespace.SourceDocument.Source addNewSource()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.SourceDocument.Source target = null;
                target = (noNamespace.SourceDocument.Source)get_store().add_element_user(SOURCE$22);
                return target;
            }
        }
        
        /**
         * Removes the ith "source" element
         */
        public void removeSource(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(SOURCE$22, i);
            }
        }
    }
}
