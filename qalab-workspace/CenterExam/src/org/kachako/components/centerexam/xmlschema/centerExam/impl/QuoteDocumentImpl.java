/*
 * An XML document type.
 * Localname: quote
 * Namespace: 
 * Java type: noNamespace.QuoteDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.centerExam.impl;
/**
 * A document containing one quote(@) element.
 *
 * This is a complex type.
 */
public class QuoteDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.QuoteDocument
{
    private static final long serialVersionUID = 1L;
    
    public QuoteDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName QUOTE$0 = 
        new javax.xml.namespace.QName("", "quote");
    
    
    /**
     * Gets the "quote" element
     */
    public noNamespace.QuoteDocument.Quote getQuote()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.QuoteDocument.Quote target = null;
            target = (noNamespace.QuoteDocument.Quote)get_store().find_element_user(QUOTE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "quote" element
     */
    public void setQuote(noNamespace.QuoteDocument.Quote quote)
    {
        generatedSetterHelperImpl(quote, QUOTE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "quote" element
     */
    public noNamespace.QuoteDocument.Quote addNewQuote()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.QuoteDocument.Quote target = null;
            target = (noNamespace.QuoteDocument.Quote)get_store().add_element_user(QUOTE$0);
            return target;
        }
    }
    /**
     * An XML quote(@).
     *
     * This is a complex type.
     */
    public static class QuoteImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.QuoteDocument.Quote
    {
        private static final long serialVersionUID = 1L;
        
        public QuoteImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName GARBLED$0 = 
            new javax.xml.namespace.QName("", "garbled");
        private static final javax.xml.namespace.QName MISSING$2 = 
            new javax.xml.namespace.QName("", "missing");
        private static final javax.xml.namespace.QName BR$4 = 
            new javax.xml.namespace.QName("", "br");
        private static final javax.xml.namespace.QName UTEXT$6 = 
            new javax.xml.namespace.QName("", "uText");
        private static final javax.xml.namespace.QName LTEXT$8 = 
            new javax.xml.namespace.QName("", "lText");
        private static final javax.xml.namespace.QName REF$10 = 
            new javax.xml.namespace.QName("", "ref");
        private static final javax.xml.namespace.QName LABEL$12 = 
            new javax.xml.namespace.QName("", "label");
        private static final javax.xml.namespace.QName BLANK$14 = 
            new javax.xml.namespace.QName("", "blank");
        private static final javax.xml.namespace.QName NOTE$16 = 
            new javax.xml.namespace.QName("", "note");
        private static final javax.xml.namespace.QName SOURCE$18 = 
            new javax.xml.namespace.QName("", "source");
        
        
        /**
         * Gets array of all "garbled" elements
         */
        public noNamespace.GarbledDocument.Garbled[] getGarbledArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(GARBLED$0, targetList);
                noNamespace.GarbledDocument.Garbled[] result = new noNamespace.GarbledDocument.Garbled[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled getGarbledArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().find_element_user(GARBLED$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "garbled" element
         */
        public int sizeOfGarbledArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(GARBLED$0);
            }
        }
        
        /**
         * Sets array of all "garbled" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setGarbledArray(noNamespace.GarbledDocument.Garbled[] garbledArray)
        {
            check_orphaned();
            arraySetterHelper(garbledArray, GARBLED$0);
        }
        
        /**
         * Sets ith "garbled" element
         */
        public void setGarbledArray(int i, noNamespace.GarbledDocument.Garbled garbled)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().find_element_user(GARBLED$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(garbled);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled insertNewGarbled(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().insert_element_user(GARBLED$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled addNewGarbled()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().add_element_user(GARBLED$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "garbled" element
         */
        public void removeGarbled(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(GARBLED$0, i);
            }
        }
        
        /**
         * Gets array of all "missing" elements
         */
        public noNamespace.MissingDocument.Missing[] getMissingArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(MISSING$2, targetList);
                noNamespace.MissingDocument.Missing[] result = new noNamespace.MissingDocument.Missing[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "missing" element
         */
        public noNamespace.MissingDocument.Missing getMissingArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().find_element_user(MISSING$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "missing" element
         */
        public int sizeOfMissingArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(MISSING$2);
            }
        }
        
        /**
         * Sets array of all "missing" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setMissingArray(noNamespace.MissingDocument.Missing[] missingArray)
        {
            check_orphaned();
            arraySetterHelper(missingArray, MISSING$2);
        }
        
        /**
         * Sets ith "missing" element
         */
        public void setMissingArray(int i, noNamespace.MissingDocument.Missing missing)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().find_element_user(MISSING$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(missing);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "missing" element
         */
        public noNamespace.MissingDocument.Missing insertNewMissing(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().insert_element_user(MISSING$2, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "missing" element
         */
        public noNamespace.MissingDocument.Missing addNewMissing()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().add_element_user(MISSING$2);
                return target;
            }
        }
        
        /**
         * Removes the ith "missing" element
         */
        public void removeMissing(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(MISSING$2, i);
            }
        }
        
        /**
         * Gets array of all "br" elements
         */
        public noNamespace.BrDocument.Br[] getBrArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(BR$4, targetList);
                noNamespace.BrDocument.Br[] result = new noNamespace.BrDocument.Br[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "br" element
         */
        public noNamespace.BrDocument.Br getBrArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().find_element_user(BR$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "br" element
         */
        public int sizeOfBrArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(BR$4);
            }
        }
        
        /**
         * Sets array of all "br" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setBrArray(noNamespace.BrDocument.Br[] brArray)
        {
            check_orphaned();
            arraySetterHelper(brArray, BR$4);
        }
        
        /**
         * Sets ith "br" element
         */
        public void setBrArray(int i, noNamespace.BrDocument.Br br)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().find_element_user(BR$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(br);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "br" element
         */
        public noNamespace.BrDocument.Br insertNewBr(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().insert_element_user(BR$4, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "br" element
         */
        public noNamespace.BrDocument.Br addNewBr()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().add_element_user(BR$4);
                return target;
            }
        }
        
        /**
         * Removes the ith "br" element
         */
        public void removeBr(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(BR$4, i);
            }
        }
        
        /**
         * Gets array of all "uText" elements
         */
        public noNamespace.UTextDocument.UText[] getUTextArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(UTEXT$6, targetList);
                noNamespace.UTextDocument.UText[] result = new noNamespace.UTextDocument.UText[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "uText" element
         */
        public noNamespace.UTextDocument.UText getUTextArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.UTextDocument.UText target = null;
                target = (noNamespace.UTextDocument.UText)get_store().find_element_user(UTEXT$6, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "uText" element
         */
        public int sizeOfUTextArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(UTEXT$6);
            }
        }
        
        /**
         * Sets array of all "uText" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setUTextArray(noNamespace.UTextDocument.UText[] uTextArray)
        {
            check_orphaned();
            arraySetterHelper(uTextArray, UTEXT$6);
        }
        
        /**
         * Sets ith "uText" element
         */
        public void setUTextArray(int i, noNamespace.UTextDocument.UText uText)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.UTextDocument.UText target = null;
                target = (noNamespace.UTextDocument.UText)get_store().find_element_user(UTEXT$6, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(uText);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "uText" element
         */
        public noNamespace.UTextDocument.UText insertNewUText(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.UTextDocument.UText target = null;
                target = (noNamespace.UTextDocument.UText)get_store().insert_element_user(UTEXT$6, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "uText" element
         */
        public noNamespace.UTextDocument.UText addNewUText()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.UTextDocument.UText target = null;
                target = (noNamespace.UTextDocument.UText)get_store().add_element_user(UTEXT$6);
                return target;
            }
        }
        
        /**
         * Removes the ith "uText" element
         */
        public void removeUText(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(UTEXT$6, i);
            }
        }
        
        /**
         * Gets array of all "lText" elements
         */
        public noNamespace.LTextDocument.LText[] getLTextArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(LTEXT$8, targetList);
                noNamespace.LTextDocument.LText[] result = new noNamespace.LTextDocument.LText[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "lText" element
         */
        public noNamespace.LTextDocument.LText getLTextArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LTextDocument.LText target = null;
                target = (noNamespace.LTextDocument.LText)get_store().find_element_user(LTEXT$8, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "lText" element
         */
        public int sizeOfLTextArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(LTEXT$8);
            }
        }
        
        /**
         * Sets array of all "lText" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setLTextArray(noNamespace.LTextDocument.LText[] lTextArray)
        {
            check_orphaned();
            arraySetterHelper(lTextArray, LTEXT$8);
        }
        
        /**
         * Sets ith "lText" element
         */
        public void setLTextArray(int i, noNamespace.LTextDocument.LText lText)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LTextDocument.LText target = null;
                target = (noNamespace.LTextDocument.LText)get_store().find_element_user(LTEXT$8, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(lText);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "lText" element
         */
        public noNamespace.LTextDocument.LText insertNewLText(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LTextDocument.LText target = null;
                target = (noNamespace.LTextDocument.LText)get_store().insert_element_user(LTEXT$8, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "lText" element
         */
        public noNamespace.LTextDocument.LText addNewLText()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LTextDocument.LText target = null;
                target = (noNamespace.LTextDocument.LText)get_store().add_element_user(LTEXT$8);
                return target;
            }
        }
        
        /**
         * Removes the ith "lText" element
         */
        public void removeLText(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(LTEXT$8, i);
            }
        }
        
        /**
         * Gets array of all "ref" elements
         */
        public noNamespace.RefDocument.Ref[] getRefArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(REF$10, targetList);
                noNamespace.RefDocument.Ref[] result = new noNamespace.RefDocument.Ref[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "ref" element
         */
        public noNamespace.RefDocument.Ref getRefArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().find_element_user(REF$10, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "ref" element
         */
        public int sizeOfRefArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(REF$10);
            }
        }
        
        /**
         * Sets array of all "ref" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setRefArray(noNamespace.RefDocument.Ref[] refArray)
        {
            check_orphaned();
            arraySetterHelper(refArray, REF$10);
        }
        
        /**
         * Sets ith "ref" element
         */
        public void setRefArray(int i, noNamespace.RefDocument.Ref ref)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().find_element_user(REF$10, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(ref);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ref" element
         */
        public noNamespace.RefDocument.Ref insertNewRef(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().insert_element_user(REF$10, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ref" element
         */
        public noNamespace.RefDocument.Ref addNewRef()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.RefDocument.Ref target = null;
                target = (noNamespace.RefDocument.Ref)get_store().add_element_user(REF$10);
                return target;
            }
        }
        
        /**
         * Removes the ith "ref" element
         */
        public void removeRef(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(REF$10, i);
            }
        }
        
        /**
         * Gets array of all "label" elements
         */
        public noNamespace.LabelDocument.Label[] getLabelArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(LABEL$12, targetList);
                noNamespace.LabelDocument.Label[] result = new noNamespace.LabelDocument.Label[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "label" element
         */
        public noNamespace.LabelDocument.Label getLabelArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().find_element_user(LABEL$12, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "label" element
         */
        public int sizeOfLabelArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(LABEL$12);
            }
        }
        
        /**
         * Sets array of all "label" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setLabelArray(noNamespace.LabelDocument.Label[] labelArray)
        {
            check_orphaned();
            arraySetterHelper(labelArray, LABEL$12);
        }
        
        /**
         * Sets ith "label" element
         */
        public void setLabelArray(int i, noNamespace.LabelDocument.Label label)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().find_element_user(LABEL$12, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(label);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "label" element
         */
        public noNamespace.LabelDocument.Label insertNewLabel(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().insert_element_user(LABEL$12, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "label" element
         */
        public noNamespace.LabelDocument.Label addNewLabel()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LabelDocument.Label target = null;
                target = (noNamespace.LabelDocument.Label)get_store().add_element_user(LABEL$12);
                return target;
            }
        }
        
        /**
         * Removes the ith "label" element
         */
        public void removeLabel(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(LABEL$12, i);
            }
        }
        
        /**
         * Gets array of all "blank" elements
         */
        public noNamespace.BlankDocument.Blank[] getBlankArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(BLANK$14, targetList);
                noNamespace.BlankDocument.Blank[] result = new noNamespace.BlankDocument.Blank[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "blank" element
         */
        public noNamespace.BlankDocument.Blank getBlankArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BlankDocument.Blank target = null;
                target = (noNamespace.BlankDocument.Blank)get_store().find_element_user(BLANK$14, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "blank" element
         */
        public int sizeOfBlankArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(BLANK$14);
            }
        }
        
        /**
         * Sets array of all "blank" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setBlankArray(noNamespace.BlankDocument.Blank[] blankArray)
        {
            check_orphaned();
            arraySetterHelper(blankArray, BLANK$14);
        }
        
        /**
         * Sets ith "blank" element
         */
        public void setBlankArray(int i, noNamespace.BlankDocument.Blank blank)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BlankDocument.Blank target = null;
                target = (noNamespace.BlankDocument.Blank)get_store().find_element_user(BLANK$14, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(blank);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "blank" element
         */
        public noNamespace.BlankDocument.Blank insertNewBlank(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BlankDocument.Blank target = null;
                target = (noNamespace.BlankDocument.Blank)get_store().insert_element_user(BLANK$14, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "blank" element
         */
        public noNamespace.BlankDocument.Blank addNewBlank()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BlankDocument.Blank target = null;
                target = (noNamespace.BlankDocument.Blank)get_store().add_element_user(BLANK$14);
                return target;
            }
        }
        
        /**
         * Removes the ith "blank" element
         */
        public void removeBlank(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(BLANK$14, i);
            }
        }
        
        /**
         * Gets array of all "note" elements
         */
        public noNamespace.NoteDocument.Note[] getNoteArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(NOTE$16, targetList);
                noNamespace.NoteDocument.Note[] result = new noNamespace.NoteDocument.Note[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "note" element
         */
        public noNamespace.NoteDocument.Note getNoteArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.NoteDocument.Note target = null;
                target = (noNamespace.NoteDocument.Note)get_store().find_element_user(NOTE$16, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "note" element
         */
        public int sizeOfNoteArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(NOTE$16);
            }
        }
        
        /**
         * Sets array of all "note" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setNoteArray(noNamespace.NoteDocument.Note[] noteArray)
        {
            check_orphaned();
            arraySetterHelper(noteArray, NOTE$16);
        }
        
        /**
         * Sets ith "note" element
         */
        public void setNoteArray(int i, noNamespace.NoteDocument.Note note)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.NoteDocument.Note target = null;
                target = (noNamespace.NoteDocument.Note)get_store().find_element_user(NOTE$16, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(note);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "note" element
         */
        public noNamespace.NoteDocument.Note insertNewNote(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.NoteDocument.Note target = null;
                target = (noNamespace.NoteDocument.Note)get_store().insert_element_user(NOTE$16, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "note" element
         */
        public noNamespace.NoteDocument.Note addNewNote()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.NoteDocument.Note target = null;
                target = (noNamespace.NoteDocument.Note)get_store().add_element_user(NOTE$16);
                return target;
            }
        }
        
        /**
         * Removes the ith "note" element
         */
        public void removeNote(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(NOTE$16, i);
            }
        }
        
        /**
         * Gets array of all "source" elements
         */
        public noNamespace.SourceDocument.Source[] getSourceArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(SOURCE$18, targetList);
                noNamespace.SourceDocument.Source[] result = new noNamespace.SourceDocument.Source[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "source" element
         */
        public noNamespace.SourceDocument.Source getSourceArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.SourceDocument.Source target = null;
                target = (noNamespace.SourceDocument.Source)get_store().find_element_user(SOURCE$18, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "source" element
         */
        public int sizeOfSourceArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(SOURCE$18);
            }
        }
        
        /**
         * Sets array of all "source" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setSourceArray(noNamespace.SourceDocument.Source[] sourceArray)
        {
            check_orphaned();
            arraySetterHelper(sourceArray, SOURCE$18);
        }
        
        /**
         * Sets ith "source" element
         */
        public void setSourceArray(int i, noNamespace.SourceDocument.Source source)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.SourceDocument.Source target = null;
                target = (noNamespace.SourceDocument.Source)get_store().find_element_user(SOURCE$18, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(source);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "source" element
         */
        public noNamespace.SourceDocument.Source insertNewSource(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.SourceDocument.Source target = null;
                target = (noNamespace.SourceDocument.Source)get_store().insert_element_user(SOURCE$18, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "source" element
         */
        public noNamespace.SourceDocument.Source addNewSource()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.SourceDocument.Source target = null;
                target = (noNamespace.SourceDocument.Source)get_store().add_element_user(SOURCE$18);
                return target;
            }
        }
        
        /**
         * Removes the ith "source" element
         */
        public void removeSource(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(SOURCE$18, i);
            }
        }
    }
}
