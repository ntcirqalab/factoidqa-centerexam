/*
 * An XML document type.
 * Localname: data
 * Namespace: 
 * Java type: DataDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.answerTable;


/**
 * A document containing one data(@) element.
 *
 * This is a complex type.
 */
public interface DataDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(DataDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s1AB2768DE360239AA90F628A92D47304").resolveHandle("data13a3doctype");
    
    /**
     * Gets the "data" element
     */
    DataDocument.Data getData();
    
    /**
     * Sets the "data" element
     */
    void setData(DataDocument.Data data);
    
    /**
     * Appends and returns a new empty "data" element
     */
    DataDocument.Data addNewData();
    
    /**
     * An XML data(@).
     *
     * This is a complex type.
     */
    public interface Data extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Data.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s1AB2768DE360239AA90F628A92D47304").resolveHandle("data54adelemtype");
        
        /**
         * Gets array of all "section" elements
         */
        java.lang.String[] getSectionArray();
        
        /**
         * Gets ith "section" element
         */
        java.lang.String getSectionArray(int i);
        
        /**
         * Gets (as xml) array of all "section" elements
         */
        org.apache.xmlbeans.XmlString[] xgetSectionArray();
        
        /**
         * Gets (as xml) ith "section" element
         */
        org.apache.xmlbeans.XmlString xgetSectionArray(int i);
        
        /**
         * Returns number of "section" element
         */
        int sizeOfSectionArray();
        
        /**
         * Sets array of all "section" element
         */
        void setSectionArray(java.lang.String[] sectionArray);
        
        /**
         * Sets ith "section" element
         */
        void setSectionArray(int i, java.lang.String section);
        
        /**
         * Sets (as xml) array of all "section" element
         */
        void xsetSectionArray(org.apache.xmlbeans.XmlString[] sectionArray);
        
        /**
         * Sets (as xml) ith "section" element
         */
        void xsetSectionArray(int i, org.apache.xmlbeans.XmlString section);
        
        /**
         * Inserts the value as the ith "section" element
         */
        void insertSection(int i, java.lang.String section);
        
        /**
         * Appends the value as the last "section" element
         */
        void addSection(java.lang.String section);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "section" element
         */
        org.apache.xmlbeans.XmlString insertNewSection(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "section" element
         */
        org.apache.xmlbeans.XmlString addNewSection();
        
        /**
         * Removes the ith "section" element
         */
        void removeSection(int i);
        
        /**
         * Gets array of all "question" elements
         */
        java.lang.String[] getQuestionArray();
        
        /**
         * Gets ith "question" element
         */
        java.lang.String getQuestionArray(int i);
        
        /**
         * Gets (as xml) array of all "question" elements
         */
        org.apache.xmlbeans.XmlString[] xgetQuestionArray();
        
        /**
         * Gets (as xml) ith "question" element
         */
        org.apache.xmlbeans.XmlString xgetQuestionArray(int i);
        
        /**
         * Returns number of "question" element
         */
        int sizeOfQuestionArray();
        
        /**
         * Sets array of all "question" element
         */
        void setQuestionArray(java.lang.String[] questionArray);
        
        /**
         * Sets ith "question" element
         */
        void setQuestionArray(int i, java.lang.String question);
        
        /**
         * Sets (as xml) array of all "question" element
         */
        void xsetQuestionArray(org.apache.xmlbeans.XmlString[] questionArray);
        
        /**
         * Sets (as xml) ith "question" element
         */
        void xsetQuestionArray(int i, org.apache.xmlbeans.XmlString question);
        
        /**
         * Inserts the value as the ith "question" element
         */
        void insertQuestion(int i, java.lang.String question);
        
        /**
         * Appends the value as the last "question" element
         */
        void addQuestion(java.lang.String question);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "question" element
         */
        org.apache.xmlbeans.XmlString insertNewQuestion(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "question" element
         */
        org.apache.xmlbeans.XmlString addNewQuestion();
        
        /**
         * Removes the ith "question" element
         */
        void removeQuestion(int i);
        
        /**
         * Gets array of all "answer_column" elements
         */
        java.lang.String[] getAnswerColumnArray();
        
        /**
         * Gets ith "answer_column" element
         */
        java.lang.String getAnswerColumnArray(int i);
        
        /**
         * Gets (as xml) array of all "answer_column" elements
         */
        org.apache.xmlbeans.XmlString[] xgetAnswerColumnArray();
        
        /**
         * Gets (as xml) ith "answer_column" element
         */
        org.apache.xmlbeans.XmlString xgetAnswerColumnArray(int i);
        
        /**
         * Returns number of "answer_column" element
         */
        int sizeOfAnswerColumnArray();
        
        /**
         * Sets array of all "answer_column" element
         */
        void setAnswerColumnArray(java.lang.String[] answerColumnArray);
        
        /**
         * Sets ith "answer_column" element
         */
        void setAnswerColumnArray(int i, java.lang.String answerColumn);
        
        /**
         * Sets (as xml) array of all "answer_column" element
         */
        void xsetAnswerColumnArray(org.apache.xmlbeans.XmlString[] answerColumnArray);
        
        /**
         * Sets (as xml) ith "answer_column" element
         */
        void xsetAnswerColumnArray(int i, org.apache.xmlbeans.XmlString answerColumn);
        
        /**
         * Inserts the value as the ith "answer_column" element
         */
        void insertAnswerColumn(int i, java.lang.String answerColumn);
        
        /**
         * Appends the value as the last "answer_column" element
         */
        void addAnswerColumn(java.lang.String answerColumn);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "answer_column" element
         */
        org.apache.xmlbeans.XmlString insertNewAnswerColumn(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "answer_column" element
         */
        org.apache.xmlbeans.XmlString addNewAnswerColumn();
        
        /**
         * Removes the ith "answer_column" element
         */
        void removeAnswerColumn(int i);
        
        /**
         * Gets array of all "answer" elements
         */
        java.lang.String[] getAnswerArray();
        
        /**
         * Gets ith "answer" element
         */
        java.lang.String getAnswerArray(int i);
        
        /**
         * Gets (as xml) array of all "answer" elements
         */
        org.apache.xmlbeans.XmlString[] xgetAnswerArray();
        
        /**
         * Gets (as xml) ith "answer" element
         */
        org.apache.xmlbeans.XmlString xgetAnswerArray(int i);
        
        /**
         * Returns number of "answer" element
         */
        int sizeOfAnswerArray();
        
        /**
         * Sets array of all "answer" element
         */
        void setAnswerArray(java.lang.String[] answerArray);
        
        /**
         * Sets ith "answer" element
         */
        void setAnswerArray(int i, java.lang.String answer);
        
        /**
         * Sets (as xml) array of all "answer" element
         */
        void xsetAnswerArray(org.apache.xmlbeans.XmlString[] answerArray);
        
        /**
         * Sets (as xml) ith "answer" element
         */
        void xsetAnswerArray(int i, org.apache.xmlbeans.XmlString answer);
        
        /**
         * Inserts the value as the ith "answer" element
         */
        void insertAnswer(int i, java.lang.String answer);
        
        /**
         * Appends the value as the last "answer" element
         */
        void addAnswer(java.lang.String answer);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "answer" element
         */
        org.apache.xmlbeans.XmlString insertNewAnswer(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "answer" element
         */
        org.apache.xmlbeans.XmlString addNewAnswer();
        
        /**
         * Removes the ith "answer" element
         */
        void removeAnswer(int i);
        
        /**
         * Gets array of all "score" elements
         */
        java.lang.String[] getScoreArray();
        
        /**
         * Gets ith "score" element
         */
        java.lang.String getScoreArray(int i);
        
        /**
         * Gets (as xml) array of all "score" elements
         */
        org.apache.xmlbeans.XmlString[] xgetScoreArray();
        
        /**
         * Gets (as xml) ith "score" element
         */
        org.apache.xmlbeans.XmlString xgetScoreArray(int i);
        
        /**
         * Returns number of "score" element
         */
        int sizeOfScoreArray();
        
        /**
         * Sets array of all "score" element
         */
        void setScoreArray(java.lang.String[] scoreArray);
        
        /**
         * Sets ith "score" element
         */
        void setScoreArray(int i, java.lang.String score);
        
        /**
         * Sets (as xml) array of all "score" element
         */
        void xsetScoreArray(org.apache.xmlbeans.XmlString[] scoreArray);
        
        /**
         * Sets (as xml) ith "score" element
         */
        void xsetScoreArray(int i, org.apache.xmlbeans.XmlString score);
        
        /**
         * Inserts the value as the ith "score" element
         */
        void insertScore(int i, java.lang.String score);
        
        /**
         * Appends the value as the last "score" element
         */
        void addScore(java.lang.String score);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "score" element
         */
        org.apache.xmlbeans.XmlString insertNewScore(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "score" element
         */
        org.apache.xmlbeans.XmlString addNewScore();
        
        /**
         * Removes the ith "score" element
         */
        void removeScore(int i);
        
        /**
         * Gets array of all "answer_style" elements
         */
        java.lang.String[] getAnswerStyleArray();
        
        /**
         * Gets ith "answer_style" element
         */
        java.lang.String getAnswerStyleArray(int i);
        
        /**
         * Gets (as xml) array of all "answer_style" elements
         */
        org.apache.xmlbeans.XmlString[] xgetAnswerStyleArray();
        
        /**
         * Gets (as xml) ith "answer_style" element
         */
        org.apache.xmlbeans.XmlString xgetAnswerStyleArray(int i);
        
        /**
         * Returns number of "answer_style" element
         */
        int sizeOfAnswerStyleArray();
        
        /**
         * Sets array of all "answer_style" element
         */
        void setAnswerStyleArray(java.lang.String[] answerStyleArray);
        
        /**
         * Sets ith "answer_style" element
         */
        void setAnswerStyleArray(int i, java.lang.String answerStyle);
        
        /**
         * Sets (as xml) array of all "answer_style" element
         */
        void xsetAnswerStyleArray(org.apache.xmlbeans.XmlString[] answerStyleArray);
        
        /**
         * Sets (as xml) ith "answer_style" element
         */
        void xsetAnswerStyleArray(int i, org.apache.xmlbeans.XmlString answerStyle);
        
        /**
         * Inserts the value as the ith "answer_style" element
         */
        void insertAnswerStyle(int i, java.lang.String answerStyle);
        
        /**
         * Appends the value as the last "answer_style" element
         */
        void addAnswerStyle(java.lang.String answerStyle);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "answer_style" element
         */
        org.apache.xmlbeans.XmlString insertNewAnswerStyle(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "answer_style" element
         */
        org.apache.xmlbeans.XmlString addNewAnswerStyle();
        
        /**
         * Removes the ith "answer_style" element
         */
        void removeAnswerStyle(int i);
        
        /**
         * Gets array of all "answer_type" elements
         */
        java.lang.String[] getAnswerTypeArray();
        
        /**
         * Gets ith "answer_type" element
         */
        java.lang.String getAnswerTypeArray(int i);
        
        /**
         * Gets (as xml) array of all "answer_type" elements
         */
        org.apache.xmlbeans.XmlString[] xgetAnswerTypeArray();
        
        /**
         * Gets (as xml) ith "answer_type" element
         */
        org.apache.xmlbeans.XmlString xgetAnswerTypeArray(int i);
        
        /**
         * Returns number of "answer_type" element
         */
        int sizeOfAnswerTypeArray();
        
        /**
         * Sets array of all "answer_type" element
         */
        void setAnswerTypeArray(java.lang.String[] answerTypeArray);
        
        /**
         * Sets ith "answer_type" element
         */
        void setAnswerTypeArray(int i, java.lang.String answerType);
        
        /**
         * Sets (as xml) array of all "answer_type" element
         */
        void xsetAnswerTypeArray(org.apache.xmlbeans.XmlString[] answerTypeArray);
        
        /**
         * Sets (as xml) ith "answer_type" element
         */
        void xsetAnswerTypeArray(int i, org.apache.xmlbeans.XmlString answerType);
        
        /**
         * Inserts the value as the ith "answer_type" element
         */
        void insertAnswerType(int i, java.lang.String answerType);
        
        /**
         * Appends the value as the last "answer_type" element
         */
        void addAnswerType(java.lang.String answerType);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "answer_type" element
         */
        org.apache.xmlbeans.XmlString insertNewAnswerType(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "answer_type" element
         */
        org.apache.xmlbeans.XmlString addNewAnswerType();
        
        /**
         * Removes the ith "answer_type" element
         */
        void removeAnswerType(int i);
        
        /**
         * Gets array of all "knowledge_type" elements
         */
        java.lang.String[] getKnowledgeTypeArray();
        
        /**
         * Gets ith "knowledge_type" element
         */
        java.lang.String getKnowledgeTypeArray(int i);
        
        /**
         * Gets (as xml) array of all "knowledge_type" elements
         */
        org.apache.xmlbeans.XmlString[] xgetKnowledgeTypeArray();
        
        /**
         * Gets (as xml) ith "knowledge_type" element
         */
        org.apache.xmlbeans.XmlString xgetKnowledgeTypeArray(int i);
        
        /**
         * Returns number of "knowledge_type" element
         */
        int sizeOfKnowledgeTypeArray();
        
        /**
         * Sets array of all "knowledge_type" element
         */
        void setKnowledgeTypeArray(java.lang.String[] knowledgeTypeArray);
        
        /**
         * Sets ith "knowledge_type" element
         */
        void setKnowledgeTypeArray(int i, java.lang.String knowledgeType);
        
        /**
         * Sets (as xml) array of all "knowledge_type" element
         */
        void xsetKnowledgeTypeArray(org.apache.xmlbeans.XmlString[] knowledgeTypeArray);
        
        /**
         * Sets (as xml) ith "knowledge_type" element
         */
        void xsetKnowledgeTypeArray(int i, org.apache.xmlbeans.XmlString knowledgeType);
        
        /**
         * Inserts the value as the ith "knowledge_type" element
         */
        void insertKnowledgeType(int i, java.lang.String knowledgeType);
        
        /**
         * Appends the value as the last "knowledge_type" element
         */
        void addKnowledgeType(java.lang.String knowledgeType);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "knowledge_type" element
         */
        org.apache.xmlbeans.XmlString insertNewKnowledgeType(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "knowledge_type" element
         */
        org.apache.xmlbeans.XmlString addNewKnowledgeType();
        
        /**
         * Removes the ith "knowledge_type" element
         */
        void removeKnowledgeType(int i);
        
        /**
         * Gets array of all "question_ID" elements
         */
        java.lang.String[] getQuestionIDArray();
        
        /**
         * Gets ith "question_ID" element
         */
        java.lang.String getQuestionIDArray(int i);
        
        /**
         * Gets (as xml) array of all "question_ID" elements
         */
        org.apache.xmlbeans.XmlString[] xgetQuestionIDArray();
        
        /**
         * Gets (as xml) ith "question_ID" element
         */
        org.apache.xmlbeans.XmlString xgetQuestionIDArray(int i);
        
        /**
         * Returns number of "question_ID" element
         */
        int sizeOfQuestionIDArray();
        
        /**
         * Sets array of all "question_ID" element
         */
        void setQuestionIDArray(java.lang.String[] questionIDArray);
        
        /**
         * Sets ith "question_ID" element
         */
        void setQuestionIDArray(int i, java.lang.String questionID);
        
        /**
         * Sets (as xml) array of all "question_ID" element
         */
        void xsetQuestionIDArray(org.apache.xmlbeans.XmlString[] questionIDArray);
        
        /**
         * Sets (as xml) ith "question_ID" element
         */
        void xsetQuestionIDArray(int i, org.apache.xmlbeans.XmlString questionID);
        
        /**
         * Inserts the value as the ith "question_ID" element
         */
        void insertQuestionID(int i, java.lang.String questionID);
        
        /**
         * Appends the value as the last "question_ID" element
         */
        void addQuestionID(java.lang.String questionID);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "question_ID" element
         */
        org.apache.xmlbeans.XmlString insertNewQuestionID(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "question_ID" element
         */
        org.apache.xmlbeans.XmlString addNewQuestionID();
        
        /**
         * Removes the ith "question_ID" element
         */
        void removeQuestionID(int i);
        
        /**
         * Gets array of all "anscolumn_ID" elements
         */
        java.lang.String[] getAnscolumnIDArray();
        
        /**
         * Gets ith "anscolumn_ID" element
         */
        java.lang.String getAnscolumnIDArray(int i);
        
        /**
         * Gets (as xml) array of all "anscolumn_ID" elements
         */
        org.apache.xmlbeans.XmlString[] xgetAnscolumnIDArray();
        
        /**
         * Gets (as xml) ith "anscolumn_ID" element
         */
        org.apache.xmlbeans.XmlString xgetAnscolumnIDArray(int i);
        
        /**
         * Returns number of "anscolumn_ID" element
         */
        int sizeOfAnscolumnIDArray();
        
        /**
         * Sets array of all "anscolumn_ID" element
         */
        void setAnscolumnIDArray(java.lang.String[] anscolumnIDArray);
        
        /**
         * Sets ith "anscolumn_ID" element
         */
        void setAnscolumnIDArray(int i, java.lang.String anscolumnID);
        
        /**
         * Sets (as xml) array of all "anscolumn_ID" element
         */
        void xsetAnscolumnIDArray(org.apache.xmlbeans.XmlString[] anscolumnIDArray);
        
        /**
         * Sets (as xml) ith "anscolumn_ID" element
         */
        void xsetAnscolumnIDArray(int i, org.apache.xmlbeans.XmlString anscolumnID);
        
        /**
         * Inserts the value as the ith "anscolumn_ID" element
         */
        void insertAnscolumnID(int i, java.lang.String anscolumnID);
        
        /**
         * Appends the value as the last "anscolumn_ID" element
         */
        void addAnscolumnID(java.lang.String anscolumnID);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "anscolumn_ID" element
         */
        org.apache.xmlbeans.XmlString insertNewAnscolumnID(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "anscolumn_ID" element
         */
        org.apache.xmlbeans.XmlString addNewAnscolumnID();
        
        /**
         * Removes the ith "anscolumn_ID" element
         */
        void removeAnscolumnID(int i);
        
        /**
         * Gets array of all "process_log" elements
         */
        java.lang.String[] getProcessLogArray();
        
        /**
         * Gets ith "process_log" element
         */
        java.lang.String getProcessLogArray(int i);
        
        /**
         * Gets (as xml) array of all "process_log" elements
         */
        org.apache.xmlbeans.XmlString[] xgetProcessLogArray();
        
        /**
         * Gets (as xml) ith "process_log" element
         */
        org.apache.xmlbeans.XmlString xgetProcessLogArray(int i);
        
        /**
         * Returns number of "process_log" element
         */
        int sizeOfProcessLogArray();
        
        /**
         * Sets array of all "process_log" element
         */
        void setProcessLogArray(java.lang.String[] processLogArray);
        
        /**
         * Sets ith "process_log" element
         */
        void setProcessLogArray(int i, java.lang.String processLog);
        
        /**
         * Sets (as xml) array of all "process_log" element
         */
        void xsetProcessLogArray(org.apache.xmlbeans.XmlString[] processLogArray);
        
        /**
         * Sets (as xml) ith "process_log" element
         */
        void xsetProcessLogArray(int i, org.apache.xmlbeans.XmlString processLog);
        
        /**
         * Inserts the value as the ith "process_log" element
         */
        void insertProcessLog(int i, java.lang.String processLog);
        
        /**
         * Appends the value as the last "process_log" element
         */
        void addProcessLog(java.lang.String processLog);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "process_log" element
         */
        org.apache.xmlbeans.XmlString insertNewProcessLog(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "process_log" element
         */
        org.apache.xmlbeans.XmlString addNewProcessLog();
        
        /**
         * Removes the ith "process_log" element
         */
        void removeProcessLog(int i);
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static DataDocument.Data newInstance() {
              return (DataDocument.Data) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static DataDocument.Data newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (DataDocument.Data) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static DataDocument newInstance() {
          return (DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static DataDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static DataDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static DataDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static DataDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static DataDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static DataDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static DataDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static DataDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static DataDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static DataDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static DataDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static DataDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static DataDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static DataDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static DataDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static DataDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static DataDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (DataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
