/*
 * An XML document type.
 * Localname: tbl
 * Namespace: 
 * Java type: noNamespace.TblDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.centerExam;


/**
 * A document containing one tbl(@) element.
 *
 * This is a complex type.
 */
public interface TblDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(TblDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s9AD9431611720D569E6CDA08F9EDE660").resolveHandle("tblb8a5doctype");
    
    /**
     * Gets the "tbl" element
     */
    noNamespace.TblDocument.Tbl getTbl();
    
    /**
     * Sets the "tbl" element
     */
    void setTbl(noNamespace.TblDocument.Tbl tbl);
    
    /**
     * Appends and returns a new empty "tbl" element
     */
    noNamespace.TblDocument.Tbl addNewTbl();
    
    /**
     * An XML tbl(@).
     *
     * This is a complex type.
     */
    public interface Tbl extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Tbl.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s9AD9431611720D569E6CDA08F9EDE660").resolveHandle("tbl6d5felemtype");
        
        /**
         * Gets array of all "row" elements
         */
        noNamespace.RowDocument.Row[] getRowArray();
        
        /**
         * Gets ith "row" element
         */
        noNamespace.RowDocument.Row getRowArray(int i);
        
        /**
         * Returns number of "row" element
         */
        int sizeOfRowArray();
        
        /**
         * Sets array of all "row" element
         */
        void setRowArray(noNamespace.RowDocument.Row[] rowArray);
        
        /**
         * Sets ith "row" element
         */
        void setRowArray(int i, noNamespace.RowDocument.Row row);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "row" element
         */
        noNamespace.RowDocument.Row insertNewRow(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "row" element
         */
        noNamespace.RowDocument.Row addNewRow();
        
        /**
         * Removes the ith "row" element
         */
        void removeRow(int i);
        
        /**
         * Gets array of all "label" elements
         */
        noNamespace.LabelDocument.Label[] getLabelArray();
        
        /**
         * Gets ith "label" element
         */
        noNamespace.LabelDocument.Label getLabelArray(int i);
        
        /**
         * Returns number of "label" element
         */
        int sizeOfLabelArray();
        
        /**
         * Sets array of all "label" element
         */
        void setLabelArray(noNamespace.LabelDocument.Label[] labelArray);
        
        /**
         * Sets ith "label" element
         */
        void setLabelArray(int i, noNamespace.LabelDocument.Label label);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "label" element
         */
        noNamespace.LabelDocument.Label insertNewLabel(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "label" element
         */
        noNamespace.LabelDocument.Label addNewLabel();
        
        /**
         * Removes the ith "label" element
         */
        void removeLabel(int i);
        
        /**
         * Gets array of all "choice" elements
         */
        noNamespace.ChoiceDocument.Choice[] getChoiceArray();
        
        /**
         * Gets ith "choice" element
         */
        noNamespace.ChoiceDocument.Choice getChoiceArray(int i);
        
        /**
         * Returns number of "choice" element
         */
        int sizeOfChoiceArray();
        
        /**
         * Sets array of all "choice" element
         */
        void setChoiceArray(noNamespace.ChoiceDocument.Choice[] choiceArray);
        
        /**
         * Sets ith "choice" element
         */
        void setChoiceArray(int i, noNamespace.ChoiceDocument.Choice choice);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "choice" element
         */
        noNamespace.ChoiceDocument.Choice insertNewChoice(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "choice" element
         */
        noNamespace.ChoiceDocument.Choice addNewChoice();
        
        /**
         * Removes the ith "choice" element
         */
        void removeChoice(int i);
        
        /**
         * Gets array of all "cNum" elements
         */
        noNamespace.CNumDocument.CNum[] getCNumArray();
        
        /**
         * Gets ith "cNum" element
         */
        noNamespace.CNumDocument.CNum getCNumArray(int i);
        
        /**
         * Returns number of "cNum" element
         */
        int sizeOfCNumArray();
        
        /**
         * Sets array of all "cNum" element
         */
        void setCNumArray(noNamespace.CNumDocument.CNum[] cNumArray);
        
        /**
         * Sets ith "cNum" element
         */
        void setCNumArray(int i, noNamespace.CNumDocument.CNum cNum);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "cNum" element
         */
        noNamespace.CNumDocument.CNum insertNewCNum(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "cNum" element
         */
        noNamespace.CNumDocument.CNum addNewCNum();
        
        /**
         * Removes the ith "cNum" element
         */
        void removeCNum(int i);
        
        /**
         * Gets array of all "br" elements
         */
        noNamespace.BrDocument.Br[] getBrArray();
        
        /**
         * Gets ith "br" element
         */
        noNamespace.BrDocument.Br getBrArray(int i);
        
        /**
         * Returns number of "br" element
         */
        int sizeOfBrArray();
        
        /**
         * Sets array of all "br" element
         */
        void setBrArray(noNamespace.BrDocument.Br[] brArray);
        
        /**
         * Sets ith "br" element
         */
        void setBrArray(int i, noNamespace.BrDocument.Br br);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "br" element
         */
        noNamespace.BrDocument.Br insertNewBr(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "br" element
         */
        noNamespace.BrDocument.Br addNewBr();
        
        /**
         * Removes the ith "br" element
         */
        void removeBr(int i);
        
        /**
         * Gets array of all "cell" elements
         */
        noNamespace.CellDocument.Cell[] getCellArray();
        
        /**
         * Gets ith "cell" element
         */
        noNamespace.CellDocument.Cell getCellArray(int i);
        
        /**
         * Returns number of "cell" element
         */
        int sizeOfCellArray();
        
        /**
         * Sets array of all "cell" element
         */
        void setCellArray(noNamespace.CellDocument.Cell[] cellArray);
        
        /**
         * Sets ith "cell" element
         */
        void setCellArray(int i, noNamespace.CellDocument.Cell cell);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "cell" element
         */
        noNamespace.CellDocument.Cell insertNewCell(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "cell" element
         */
        noNamespace.CellDocument.Cell addNewCell();
        
        /**
         * Removes the ith "cell" element
         */
        void removeCell(int i);
        
        /**
         * Gets array of all "blank" elements
         */
        noNamespace.BlankDocument.Blank[] getBlankArray();
        
        /**
         * Gets ith "blank" element
         */
        noNamespace.BlankDocument.Blank getBlankArray(int i);
        
        /**
         * Returns number of "blank" element
         */
        int sizeOfBlankArray();
        
        /**
         * Sets array of all "blank" element
         */
        void setBlankArray(noNamespace.BlankDocument.Blank[] blankArray);
        
        /**
         * Sets ith "blank" element
         */
        void setBlankArray(int i, noNamespace.BlankDocument.Blank blank);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "blank" element
         */
        noNamespace.BlankDocument.Blank insertNewBlank(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "blank" element
         */
        noNamespace.BlankDocument.Blank addNewBlank();
        
        /**
         * Removes the ith "blank" element
         */
        void removeBlank(int i);
        
        /**
         * Gets array of all "ref" elements
         */
        noNamespace.RefDocument.Ref[] getRefArray();
        
        /**
         * Gets ith "ref" element
         */
        noNamespace.RefDocument.Ref getRefArray(int i);
        
        /**
         * Returns number of "ref" element
         */
        int sizeOfRefArray();
        
        /**
         * Sets array of all "ref" element
         */
        void setRefArray(noNamespace.RefDocument.Ref[] refArray);
        
        /**
         * Sets ith "ref" element
         */
        void setRefArray(int i, noNamespace.RefDocument.Ref ref);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ref" element
         */
        noNamespace.RefDocument.Ref insertNewRef(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ref" element
         */
        noNamespace.RefDocument.Ref addNewRef();
        
        /**
         * Removes the ith "ref" element
         */
        void removeRef(int i);
        
        /**
         * Gets array of all "uText" elements
         */
        noNamespace.UTextDocument.UText[] getUTextArray();
        
        /**
         * Gets ith "uText" element
         */
        noNamespace.UTextDocument.UText getUTextArray(int i);
        
        /**
         * Returns number of "uText" element
         */
        int sizeOfUTextArray();
        
        /**
         * Sets array of all "uText" element
         */
        void setUTextArray(noNamespace.UTextDocument.UText[] uTextArray);
        
        /**
         * Sets ith "uText" element
         */
        void setUTextArray(int i, noNamespace.UTextDocument.UText uText);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "uText" element
         */
        noNamespace.UTextDocument.UText insertNewUText(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "uText" element
         */
        noNamespace.UTextDocument.UText addNewUText();
        
        /**
         * Removes the ith "uText" element
         */
        void removeUText(int i);
        
        /**
         * Gets array of all "choices" elements
         */
        noNamespace.ChoicesDocument.Choices[] getChoicesArray();
        
        /**
         * Gets ith "choices" element
         */
        noNamespace.ChoicesDocument.Choices getChoicesArray(int i);
        
        /**
         * Returns number of "choices" element
         */
        int sizeOfChoicesArray();
        
        /**
         * Sets array of all "choices" element
         */
        void setChoicesArray(noNamespace.ChoicesDocument.Choices[] choicesArray);
        
        /**
         * Sets ith "choices" element
         */
        void setChoicesArray(int i, noNamespace.ChoicesDocument.Choices choices);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "choices" element
         */
        noNamespace.ChoicesDocument.Choices insertNewChoices(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "choices" element
         */
        noNamespace.ChoicesDocument.Choices addNewChoices();
        
        /**
         * Removes the ith "choices" element
         */
        void removeChoices(int i);
        
        /**
         * Gets array of all "formula" elements
         */
        noNamespace.FormulaDocument.Formula[] getFormulaArray();
        
        /**
         * Gets ith "formula" element
         */
        noNamespace.FormulaDocument.Formula getFormulaArray(int i);
        
        /**
         * Returns number of "formula" element
         */
        int sizeOfFormulaArray();
        
        /**
         * Sets array of all "formula" element
         */
        void setFormulaArray(noNamespace.FormulaDocument.Formula[] formulaArray);
        
        /**
         * Sets ith "formula" element
         */
        void setFormulaArray(int i, noNamespace.FormulaDocument.Formula formula);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "formula" element
         */
        noNamespace.FormulaDocument.Formula insertNewFormula(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "formula" element
         */
        noNamespace.FormulaDocument.Formula addNewFormula();
        
        /**
         * Removes the ith "formula" element
         */
        void removeFormula(int i);
        
        /**
         * Gets array of all "quote" elements
         */
        noNamespace.QuoteDocument.Quote[] getQuoteArray();
        
        /**
         * Gets ith "quote" element
         */
        noNamespace.QuoteDocument.Quote getQuoteArray(int i);
        
        /**
         * Returns number of "quote" element
         */
        int sizeOfQuoteArray();
        
        /**
         * Sets array of all "quote" element
         */
        void setQuoteArray(noNamespace.QuoteDocument.Quote[] quoteArray);
        
        /**
         * Sets ith "quote" element
         */
        void setQuoteArray(int i, noNamespace.QuoteDocument.Quote quote);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "quote" element
         */
        noNamespace.QuoteDocument.Quote insertNewQuote(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "quote" element
         */
        noNamespace.QuoteDocument.Quote addNewQuote();
        
        /**
         * Removes the ith "quote" element
         */
        void removeQuote(int i);
        
        /**
         * Gets array of all "source" elements
         */
        noNamespace.SourceDocument.Source[] getSourceArray();
        
        /**
         * Gets ith "source" element
         */
        noNamespace.SourceDocument.Source getSourceArray(int i);
        
        /**
         * Returns number of "source" element
         */
        int sizeOfSourceArray();
        
        /**
         * Sets array of all "source" element
         */
        void setSourceArray(noNamespace.SourceDocument.Source[] sourceArray);
        
        /**
         * Sets ith "source" element
         */
        void setSourceArray(int i, noNamespace.SourceDocument.Source source);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "source" element
         */
        noNamespace.SourceDocument.Source insertNewSource(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "source" element
         */
        noNamespace.SourceDocument.Source addNewSource();
        
        /**
         * Removes the ith "source" element
         */
        void removeSource(int i);
        
        /**
         * Gets the "numOfCols" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType getNumOfCols();
        
        /**
         * True if has "numOfCols" attribute
         */
        boolean isSetNumOfCols();
        
        /**
         * Sets the "numOfCols" attribute
         */
        void setNumOfCols(org.apache.xmlbeans.XmlAnySimpleType numOfCols);
        
        /**
         * Appends and returns a new empty "numOfCols" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType addNewNumOfCols();
        
        /**
         * Unsets the "numOfCols" attribute
         */
        void unsetNumOfCols();
        
        /**
         * Gets the "numOfRows" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType getNumOfRows();
        
        /**
         * True if has "numOfRows" attribute
         */
        boolean isSetNumOfRows();
        
        /**
         * Sets the "numOfRows" attribute
         */
        void setNumOfRows(org.apache.xmlbeans.XmlAnySimpleType numOfRows);
        
        /**
         * Appends and returns a new empty "numOfRows" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType addNewNumOfRows();
        
        /**
         * Unsets the "numOfRows" attribute
         */
        void unsetNumOfRows();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static noNamespace.TblDocument.Tbl newInstance() {
              return (noNamespace.TblDocument.Tbl) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static noNamespace.TblDocument.Tbl newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (noNamespace.TblDocument.Tbl) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static noNamespace.TblDocument newInstance() {
          return (noNamespace.TblDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static noNamespace.TblDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (noNamespace.TblDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static noNamespace.TblDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.TblDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static noNamespace.TblDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.TblDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static noNamespace.TblDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.TblDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static noNamespace.TblDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.TblDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static noNamespace.TblDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.TblDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static noNamespace.TblDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.TblDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static noNamespace.TblDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.TblDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static noNamespace.TblDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.TblDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static noNamespace.TblDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.TblDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static noNamespace.TblDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.TblDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static noNamespace.TblDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.TblDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static noNamespace.TblDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.TblDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static noNamespace.TblDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.TblDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static noNamespace.TblDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.TblDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static noNamespace.TblDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (noNamespace.TblDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static noNamespace.TblDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (noNamespace.TblDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
