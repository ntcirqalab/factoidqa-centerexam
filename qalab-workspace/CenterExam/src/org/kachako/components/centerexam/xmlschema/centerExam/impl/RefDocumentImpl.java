/*
 * An XML document type.
 * Localname: ref
 * Namespace: 
 * Java type: noNamespace.RefDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.centerExam.impl;
/**
 * A document containing one ref(@) element.
 *
 * This is a complex type.
 */
public class RefDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.RefDocument
{
    private static final long serialVersionUID = 1L;
    
    public RefDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName REF$0 = 
        new javax.xml.namespace.QName("", "ref");
    
    
    /**
     * Gets the "ref" element
     */
    public noNamespace.RefDocument.Ref getRef()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.RefDocument.Ref target = null;
            target = (noNamespace.RefDocument.Ref)get_store().find_element_user(REF$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ref" element
     */
    public void setRef(noNamespace.RefDocument.Ref ref)
    {
        generatedSetterHelperImpl(ref, REF$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "ref" element
     */
    public noNamespace.RefDocument.Ref addNewRef()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.RefDocument.Ref target = null;
            target = (noNamespace.RefDocument.Ref)get_store().add_element_user(REF$0);
            return target;
        }
    }
    /**
     * An XML ref(@).
     *
     * This is a complex type.
     */
    public static class RefImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.RefDocument.Ref
    {
        private static final long serialVersionUID = 1L;
        
        public RefImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName GARBLED$0 = 
            new javax.xml.namespace.QName("", "garbled");
        private static final javax.xml.namespace.QName MISSING$2 = 
            new javax.xml.namespace.QName("", "missing");
        private static final javax.xml.namespace.QName BR$4 = 
            new javax.xml.namespace.QName("", "br");
        private static final javax.xml.namespace.QName FORMULA$6 = 
            new javax.xml.namespace.QName("", "formula");
        private static final javax.xml.namespace.QName UTEXT$8 = 
            new javax.xml.namespace.QName("", "uText");
        private static final javax.xml.namespace.QName LTEXT$10 = 
            new javax.xml.namespace.QName("", "lText");
        private static final javax.xml.namespace.QName COMMENT$12 = 
            new javax.xml.namespace.QName("", "comment");
        private static final javax.xml.namespace.QName TARGET$14 = 
            new javax.xml.namespace.QName("", "target");
        
        
        /**
         * Gets array of all "garbled" elements
         */
        public noNamespace.GarbledDocument.Garbled[] getGarbledArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(GARBLED$0, targetList);
                noNamespace.GarbledDocument.Garbled[] result = new noNamespace.GarbledDocument.Garbled[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled getGarbledArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().find_element_user(GARBLED$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "garbled" element
         */
        public int sizeOfGarbledArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(GARBLED$0);
            }
        }
        
        /**
         * Sets array of all "garbled" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setGarbledArray(noNamespace.GarbledDocument.Garbled[] garbledArray)
        {
            check_orphaned();
            arraySetterHelper(garbledArray, GARBLED$0);
        }
        
        /**
         * Sets ith "garbled" element
         */
        public void setGarbledArray(int i, noNamespace.GarbledDocument.Garbled garbled)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().find_element_user(GARBLED$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(garbled);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled insertNewGarbled(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().insert_element_user(GARBLED$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "garbled" element
         */
        public noNamespace.GarbledDocument.Garbled addNewGarbled()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.GarbledDocument.Garbled target = null;
                target = (noNamespace.GarbledDocument.Garbled)get_store().add_element_user(GARBLED$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "garbled" element
         */
        public void removeGarbled(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(GARBLED$0, i);
            }
        }
        
        /**
         * Gets array of all "missing" elements
         */
        public noNamespace.MissingDocument.Missing[] getMissingArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(MISSING$2, targetList);
                noNamespace.MissingDocument.Missing[] result = new noNamespace.MissingDocument.Missing[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "missing" element
         */
        public noNamespace.MissingDocument.Missing getMissingArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().find_element_user(MISSING$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "missing" element
         */
        public int sizeOfMissingArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(MISSING$2);
            }
        }
        
        /**
         * Sets array of all "missing" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setMissingArray(noNamespace.MissingDocument.Missing[] missingArray)
        {
            check_orphaned();
            arraySetterHelper(missingArray, MISSING$2);
        }
        
        /**
         * Sets ith "missing" element
         */
        public void setMissingArray(int i, noNamespace.MissingDocument.Missing missing)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().find_element_user(MISSING$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(missing);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "missing" element
         */
        public noNamespace.MissingDocument.Missing insertNewMissing(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().insert_element_user(MISSING$2, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "missing" element
         */
        public noNamespace.MissingDocument.Missing addNewMissing()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.MissingDocument.Missing target = null;
                target = (noNamespace.MissingDocument.Missing)get_store().add_element_user(MISSING$2);
                return target;
            }
        }
        
        /**
         * Removes the ith "missing" element
         */
        public void removeMissing(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(MISSING$2, i);
            }
        }
        
        /**
         * Gets array of all "br" elements
         */
        public noNamespace.BrDocument.Br[] getBrArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(BR$4, targetList);
                noNamespace.BrDocument.Br[] result = new noNamespace.BrDocument.Br[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "br" element
         */
        public noNamespace.BrDocument.Br getBrArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().find_element_user(BR$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "br" element
         */
        public int sizeOfBrArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(BR$4);
            }
        }
        
        /**
         * Sets array of all "br" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setBrArray(noNamespace.BrDocument.Br[] brArray)
        {
            check_orphaned();
            arraySetterHelper(brArray, BR$4);
        }
        
        /**
         * Sets ith "br" element
         */
        public void setBrArray(int i, noNamespace.BrDocument.Br br)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().find_element_user(BR$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(br);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "br" element
         */
        public noNamespace.BrDocument.Br insertNewBr(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().insert_element_user(BR$4, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "br" element
         */
        public noNamespace.BrDocument.Br addNewBr()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.BrDocument.Br target = null;
                target = (noNamespace.BrDocument.Br)get_store().add_element_user(BR$4);
                return target;
            }
        }
        
        /**
         * Removes the ith "br" element
         */
        public void removeBr(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(BR$4, i);
            }
        }
        
        /**
         * Gets array of all "formula" elements
         */
        public noNamespace.FormulaDocument.Formula[] getFormulaArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(FORMULA$6, targetList);
                noNamespace.FormulaDocument.Formula[] result = new noNamespace.FormulaDocument.Formula[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "formula" element
         */
        public noNamespace.FormulaDocument.Formula getFormulaArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.FormulaDocument.Formula target = null;
                target = (noNamespace.FormulaDocument.Formula)get_store().find_element_user(FORMULA$6, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "formula" element
         */
        public int sizeOfFormulaArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(FORMULA$6);
            }
        }
        
        /**
         * Sets array of all "formula" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setFormulaArray(noNamespace.FormulaDocument.Formula[] formulaArray)
        {
            check_orphaned();
            arraySetterHelper(formulaArray, FORMULA$6);
        }
        
        /**
         * Sets ith "formula" element
         */
        public void setFormulaArray(int i, noNamespace.FormulaDocument.Formula formula)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.FormulaDocument.Formula target = null;
                target = (noNamespace.FormulaDocument.Formula)get_store().find_element_user(FORMULA$6, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(formula);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "formula" element
         */
        public noNamespace.FormulaDocument.Formula insertNewFormula(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.FormulaDocument.Formula target = null;
                target = (noNamespace.FormulaDocument.Formula)get_store().insert_element_user(FORMULA$6, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "formula" element
         */
        public noNamespace.FormulaDocument.Formula addNewFormula()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.FormulaDocument.Formula target = null;
                target = (noNamespace.FormulaDocument.Formula)get_store().add_element_user(FORMULA$6);
                return target;
            }
        }
        
        /**
         * Removes the ith "formula" element
         */
        public void removeFormula(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(FORMULA$6, i);
            }
        }
        
        /**
         * Gets array of all "uText" elements
         */
        public noNamespace.UTextDocument.UText[] getUTextArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(UTEXT$8, targetList);
                noNamespace.UTextDocument.UText[] result = new noNamespace.UTextDocument.UText[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "uText" element
         */
        public noNamespace.UTextDocument.UText getUTextArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.UTextDocument.UText target = null;
                target = (noNamespace.UTextDocument.UText)get_store().find_element_user(UTEXT$8, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "uText" element
         */
        public int sizeOfUTextArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(UTEXT$8);
            }
        }
        
        /**
         * Sets array of all "uText" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setUTextArray(noNamespace.UTextDocument.UText[] uTextArray)
        {
            check_orphaned();
            arraySetterHelper(uTextArray, UTEXT$8);
        }
        
        /**
         * Sets ith "uText" element
         */
        public void setUTextArray(int i, noNamespace.UTextDocument.UText uText)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.UTextDocument.UText target = null;
                target = (noNamespace.UTextDocument.UText)get_store().find_element_user(UTEXT$8, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(uText);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "uText" element
         */
        public noNamespace.UTextDocument.UText insertNewUText(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.UTextDocument.UText target = null;
                target = (noNamespace.UTextDocument.UText)get_store().insert_element_user(UTEXT$8, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "uText" element
         */
        public noNamespace.UTextDocument.UText addNewUText()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.UTextDocument.UText target = null;
                target = (noNamespace.UTextDocument.UText)get_store().add_element_user(UTEXT$8);
                return target;
            }
        }
        
        /**
         * Removes the ith "uText" element
         */
        public void removeUText(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(UTEXT$8, i);
            }
        }
        
        /**
         * Gets array of all "lText" elements
         */
        public noNamespace.LTextDocument.LText[] getLTextArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(LTEXT$10, targetList);
                noNamespace.LTextDocument.LText[] result = new noNamespace.LTextDocument.LText[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "lText" element
         */
        public noNamespace.LTextDocument.LText getLTextArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LTextDocument.LText target = null;
                target = (noNamespace.LTextDocument.LText)get_store().find_element_user(LTEXT$10, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "lText" element
         */
        public int sizeOfLTextArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(LTEXT$10);
            }
        }
        
        /**
         * Sets array of all "lText" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setLTextArray(noNamespace.LTextDocument.LText[] lTextArray)
        {
            check_orphaned();
            arraySetterHelper(lTextArray, LTEXT$10);
        }
        
        /**
         * Sets ith "lText" element
         */
        public void setLTextArray(int i, noNamespace.LTextDocument.LText lText)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LTextDocument.LText target = null;
                target = (noNamespace.LTextDocument.LText)get_store().find_element_user(LTEXT$10, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(lText);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "lText" element
         */
        public noNamespace.LTextDocument.LText insertNewLText(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LTextDocument.LText target = null;
                target = (noNamespace.LTextDocument.LText)get_store().insert_element_user(LTEXT$10, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "lText" element
         */
        public noNamespace.LTextDocument.LText addNewLText()
        {
            synchronized (monitor())
            {
                check_orphaned();
                noNamespace.LTextDocument.LText target = null;
                target = (noNamespace.LTextDocument.LText)get_store().add_element_user(LTEXT$10);
                return target;
            }
        }
        
        /**
         * Removes the ith "lText" element
         */
        public void removeLText(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(LTEXT$10, i);
            }
        }
        
        /**
         * Gets the "comment" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType getComment()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().find_attribute_user(COMMENT$12);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "comment" attribute
         */
        public boolean isSetComment()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(COMMENT$12) != null;
            }
        }
        
        /**
         * Sets the "comment" attribute
         */
        public void setComment(org.apache.xmlbeans.XmlAnySimpleType comment)
        {
            generatedSetterHelperImpl(comment, COMMENT$12, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "comment" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType addNewComment()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().add_attribute_user(COMMENT$12);
                return target;
            }
        }
        
        /**
         * Unsets the "comment" attribute
         */
        public void unsetComment()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(COMMENT$12);
            }
        }
        
        /**
         * Gets the "target" attribute
         */
        public java.lang.String getTarget()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(TARGET$14);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "target" attribute
         */
        public org.apache.xmlbeans.XmlIDREF xgetTarget()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlIDREF target = null;
                target = (org.apache.xmlbeans.XmlIDREF)get_store().find_attribute_user(TARGET$14);
                return target;
            }
        }
        
        /**
         * True if has "target" attribute
         */
        public boolean isSetTarget()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(TARGET$14) != null;
            }
        }
        
        /**
         * Sets the "target" attribute
         */
        public void setTarget(java.lang.String targetValue)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(TARGET$14);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(TARGET$14);
                }
                target.setStringValue(targetValue);
            }
        }
        
        /**
         * Sets (as xml) the "target" attribute
         */
        public void xsetTarget(org.apache.xmlbeans.XmlIDREF targetValue)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlIDREF target = null;
                target = (org.apache.xmlbeans.XmlIDREF)get_store().find_attribute_user(TARGET$14);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlIDREF)get_store().add_attribute_user(TARGET$14);
                }
                target.set(targetValue);
            }
        }
        
        /**
         * Unsets the "target" attribute
         */
        public void unsetTarget()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(TARGET$14);
            }
        }
    }
}
