/*
 * An XML document type.
 * Localname: question
 * Namespace: 
 * Java type: noNamespace.QuestionDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.centerExam;


/**
 * A document containing one question(@) element.
 *
 * This is a complex type.
 */
public interface QuestionDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(QuestionDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s9AD9431611720D569E6CDA08F9EDE660").resolveHandle("questionbb5fdoctype");
    
    /**
     * Gets the "question" element
     */
    noNamespace.QuestionDocument.Question getQuestion();
    
    /**
     * Sets the "question" element
     */
    void setQuestion(noNamespace.QuestionDocument.Question question);
    
    /**
     * Appends and returns a new empty "question" element
     */
    noNamespace.QuestionDocument.Question addNewQuestion();
    
    /**
     * An XML question(@).
     *
     * This is a complex type.
     */
    public interface Question extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Question.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s9AD9431611720D569E6CDA08F9EDE660").resolveHandle("question61adelemtype");
        
        /**
         * Gets array of all "garbled" elements
         */
        noNamespace.GarbledDocument.Garbled[] getGarbledArray();
        
        /**
         * Gets ith "garbled" element
         */
        noNamespace.GarbledDocument.Garbled getGarbledArray(int i);
        
        /**
         * Returns number of "garbled" element
         */
        int sizeOfGarbledArray();
        
        /**
         * Sets array of all "garbled" element
         */
        void setGarbledArray(noNamespace.GarbledDocument.Garbled[] garbledArray);
        
        /**
         * Sets ith "garbled" element
         */
        void setGarbledArray(int i, noNamespace.GarbledDocument.Garbled garbled);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "garbled" element
         */
        noNamespace.GarbledDocument.Garbled insertNewGarbled(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "garbled" element
         */
        noNamespace.GarbledDocument.Garbled addNewGarbled();
        
        /**
         * Removes the ith "garbled" element
         */
        void removeGarbled(int i);
        
        /**
         * Gets array of all "label" elements
         */
        noNamespace.LabelDocument.Label[] getLabelArray();
        
        /**
         * Gets ith "label" element
         */
        noNamespace.LabelDocument.Label getLabelArray(int i);
        
        /**
         * Returns number of "label" element
         */
        int sizeOfLabelArray();
        
        /**
         * Sets array of all "label" element
         */
        void setLabelArray(noNamespace.LabelDocument.Label[] labelArray);
        
        /**
         * Sets ith "label" element
         */
        void setLabelArray(int i, noNamespace.LabelDocument.Label label);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "label" element
         */
        noNamespace.LabelDocument.Label insertNewLabel(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "label" element
         */
        noNamespace.LabelDocument.Label addNewLabel();
        
        /**
         * Removes the ith "label" element
         */
        void removeLabel(int i);
        
        /**
         * Gets array of all "instruction" elements
         */
        noNamespace.InstructionDocument.Instruction[] getInstructionArray();
        
        /**
         * Gets ith "instruction" element
         */
        noNamespace.InstructionDocument.Instruction getInstructionArray(int i);
        
        /**
         * Returns number of "instruction" element
         */
        int sizeOfInstructionArray();
        
        /**
         * Sets array of all "instruction" element
         */
        void setInstructionArray(noNamespace.InstructionDocument.Instruction[] instructionArray);
        
        /**
         * Sets ith "instruction" element
         */
        void setInstructionArray(int i, noNamespace.InstructionDocument.Instruction instruction);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "instruction" element
         */
        noNamespace.InstructionDocument.Instruction insertNewInstruction(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "instruction" element
         */
        noNamespace.InstructionDocument.Instruction addNewInstruction();
        
        /**
         * Removes the ith "instruction" element
         */
        void removeInstruction(int i);
        
        /**
         * Gets array of all "data" elements
         */
        noNamespace.DataDocument.Data[] getDataArray();
        
        /**
         * Gets ith "data" element
         */
        noNamespace.DataDocument.Data getDataArray(int i);
        
        /**
         * Returns number of "data" element
         */
        int sizeOfDataArray();
        
        /**
         * Sets array of all "data" element
         */
        void setDataArray(noNamespace.DataDocument.Data[] dataArray);
        
        /**
         * Sets ith "data" element
         */
        void setDataArray(int i, noNamespace.DataDocument.Data data);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "data" element
         */
        noNamespace.DataDocument.Data insertNewData(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "data" element
         */
        noNamespace.DataDocument.Data addNewData();
        
        /**
         * Removes the ith "data" element
         */
        void removeData(int i);
        
        /**
         * Gets array of all "question" elements
         */
        noNamespace.QuestionDocument.Question[] getQuestionArray();
        
        /**
         * Gets ith "question" element
         */
        noNamespace.QuestionDocument.Question getQuestionArray(int i);
        
        /**
         * Returns number of "question" element
         */
        int sizeOfQuestionArray();
        
        /**
         * Sets array of all "question" element
         */
        void setQuestionArray(noNamespace.QuestionDocument.Question[] questionArray);
        
        /**
         * Sets ith "question" element
         */
        void setQuestionArray(int i, noNamespace.QuestionDocument.Question question);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "question" element
         */
        noNamespace.QuestionDocument.Question insertNewQuestion(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "question" element
         */
        noNamespace.QuestionDocument.Question addNewQuestion();
        
        /**
         * Removes the ith "question" element
         */
        void removeQuestion(int i);
        
        /**
         * Gets array of all "ansColumn" elements
         */
        noNamespace.AnsColumnDocument.AnsColumn[] getAnsColumnArray();
        
        /**
         * Gets ith "ansColumn" element
         */
        noNamespace.AnsColumnDocument.AnsColumn getAnsColumnArray(int i);
        
        /**
         * Returns number of "ansColumn" element
         */
        int sizeOfAnsColumnArray();
        
        /**
         * Sets array of all "ansColumn" element
         */
        void setAnsColumnArray(noNamespace.AnsColumnDocument.AnsColumn[] ansColumnArray);
        
        /**
         * Sets ith "ansColumn" element
         */
        void setAnsColumnArray(int i, noNamespace.AnsColumnDocument.AnsColumn ansColumn);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ansColumn" element
         */
        noNamespace.AnsColumnDocument.AnsColumn insertNewAnsColumn(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ansColumn" element
         */
        noNamespace.AnsColumnDocument.AnsColumn addNewAnsColumn();
        
        /**
         * Removes the ith "ansColumn" element
         */
        void removeAnsColumn(int i);
        
        /**
         * Gets array of all "choice" elements
         */
        noNamespace.ChoiceDocument.Choice[] getChoiceArray();
        
        /**
         * Gets ith "choice" element
         */
        noNamespace.ChoiceDocument.Choice getChoiceArray(int i);
        
        /**
         * Returns number of "choice" element
         */
        int sizeOfChoiceArray();
        
        /**
         * Sets array of all "choice" element
         */
        void setChoiceArray(noNamespace.ChoiceDocument.Choice[] choiceArray);
        
        /**
         * Sets ith "choice" element
         */
        void setChoiceArray(int i, noNamespace.ChoiceDocument.Choice choice);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "choice" element
         */
        noNamespace.ChoiceDocument.Choice insertNewChoice(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "choice" element
         */
        noNamespace.ChoiceDocument.Choice addNewChoice();
        
        /**
         * Removes the ith "choice" element
         */
        void removeChoice(int i);
        
        /**
         * Gets array of all "img" elements
         */
        noNamespace.ImgDocument.Img[] getImgArray();
        
        /**
         * Gets ith "img" element
         */
        noNamespace.ImgDocument.Img getImgArray(int i);
        
        /**
         * Returns number of "img" element
         */
        int sizeOfImgArray();
        
        /**
         * Sets array of all "img" element
         */
        void setImgArray(noNamespace.ImgDocument.Img[] imgArray);
        
        /**
         * Sets ith "img" element
         */
        void setImgArray(int i, noNamespace.ImgDocument.Img img);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "img" element
         */
        noNamespace.ImgDocument.Img insertNewImg(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "img" element
         */
        noNamespace.ImgDocument.Img addNewImg();
        
        /**
         * Removes the ith "img" element
         */
        void removeImg(int i);
        
        /**
         * Gets array of all "tbl" elements
         */
        noNamespace.TblDocument.Tbl[] getTblArray();
        
        /**
         * Gets ith "tbl" element
         */
        noNamespace.TblDocument.Tbl getTblArray(int i);
        
        /**
         * Returns number of "tbl" element
         */
        int sizeOfTblArray();
        
        /**
         * Sets array of all "tbl" element
         */
        void setTblArray(noNamespace.TblDocument.Tbl[] tblArray);
        
        /**
         * Sets ith "tbl" element
         */
        void setTblArray(int i, noNamespace.TblDocument.Tbl tbl);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "tbl" element
         */
        noNamespace.TblDocument.Tbl insertNewTbl(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "tbl" element
         */
        noNamespace.TblDocument.Tbl addNewTbl();
        
        /**
         * Removes the ith "tbl" element
         */
        void removeTbl(int i);
        
        /**
         * Gets array of all "choices" elements
         */
        noNamespace.ChoicesDocument.Choices[] getChoicesArray();
        
        /**
         * Gets ith "choices" element
         */
        noNamespace.ChoicesDocument.Choices getChoicesArray(int i);
        
        /**
         * Returns number of "choices" element
         */
        int sizeOfChoicesArray();
        
        /**
         * Sets array of all "choices" element
         */
        void setChoicesArray(noNamespace.ChoicesDocument.Choices[] choicesArray);
        
        /**
         * Sets ith "choices" element
         */
        void setChoicesArray(int i, noNamespace.ChoicesDocument.Choices choices);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "choices" element
         */
        noNamespace.ChoicesDocument.Choices insertNewChoices(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "choices" element
         */
        noNamespace.ChoicesDocument.Choices addNewChoices();
        
        /**
         * Removes the ith "choices" element
         */
        void removeChoices(int i);
        
        /**
         * Gets array of all "cNum" elements
         */
        noNamespace.CNumDocument.CNum[] getCNumArray();
        
        /**
         * Gets ith "cNum" element
         */
        noNamespace.CNumDocument.CNum getCNumArray(int i);
        
        /**
         * Returns number of "cNum" element
         */
        int sizeOfCNumArray();
        
        /**
         * Sets array of all "cNum" element
         */
        void setCNumArray(noNamespace.CNumDocument.CNum[] cNumArray);
        
        /**
         * Sets ith "cNum" element
         */
        void setCNumArray(int i, noNamespace.CNumDocument.CNum cNum);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "cNum" element
         */
        noNamespace.CNumDocument.CNum insertNewCNum(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "cNum" element
         */
        noNamespace.CNumDocument.CNum addNewCNum();
        
        /**
         * Removes the ith "cNum" element
         */
        void removeCNum(int i);
        
        /**
         * Gets array of all "missing" elements
         */
        noNamespace.MissingDocument.Missing[] getMissingArray();
        
        /**
         * Gets ith "missing" element
         */
        noNamespace.MissingDocument.Missing getMissingArray(int i);
        
        /**
         * Returns number of "missing" element
         */
        int sizeOfMissingArray();
        
        /**
         * Sets array of all "missing" element
         */
        void setMissingArray(noNamespace.MissingDocument.Missing[] missingArray);
        
        /**
         * Sets ith "missing" element
         */
        void setMissingArray(int i, noNamespace.MissingDocument.Missing missing);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "missing" element
         */
        noNamespace.MissingDocument.Missing insertNewMissing(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "missing" element
         */
        noNamespace.MissingDocument.Missing addNewMissing();
        
        /**
         * Removes the ith "missing" element
         */
        void removeMissing(int i);
        
        /**
         * Gets array of all "ref" elements
         */
        noNamespace.RefDocument.Ref[] getRefArray();
        
        /**
         * Gets ith "ref" element
         */
        noNamespace.RefDocument.Ref getRefArray(int i);
        
        /**
         * Returns number of "ref" element
         */
        int sizeOfRefArray();
        
        /**
         * Sets array of all "ref" element
         */
        void setRefArray(noNamespace.RefDocument.Ref[] refArray);
        
        /**
         * Sets ith "ref" element
         */
        void setRefArray(int i, noNamespace.RefDocument.Ref ref);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ref" element
         */
        noNamespace.RefDocument.Ref insertNewRef(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ref" element
         */
        noNamespace.RefDocument.Ref addNewRef();
        
        /**
         * Removes the ith "ref" element
         */
        void removeRef(int i);
        
        /**
         * Gets array of all "br" elements
         */
        noNamespace.BrDocument.Br[] getBrArray();
        
        /**
         * Gets ith "br" element
         */
        noNamespace.BrDocument.Br getBrArray(int i);
        
        /**
         * Returns number of "br" element
         */
        int sizeOfBrArray();
        
        /**
         * Sets array of all "br" element
         */
        void setBrArray(noNamespace.BrDocument.Br[] brArray);
        
        /**
         * Sets ith "br" element
         */
        void setBrArray(int i, noNamespace.BrDocument.Br br);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "br" element
         */
        noNamespace.BrDocument.Br insertNewBr(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "br" element
         */
        noNamespace.BrDocument.Br addNewBr();
        
        /**
         * Removes the ith "br" element
         */
        void removeBr(int i);
        
        /**
         * Gets array of all "formula" elements
         */
        noNamespace.FormulaDocument.Formula[] getFormulaArray();
        
        /**
         * Gets ith "formula" element
         */
        noNamespace.FormulaDocument.Formula getFormulaArray(int i);
        
        /**
         * Returns number of "formula" element
         */
        int sizeOfFormulaArray();
        
        /**
         * Sets array of all "formula" element
         */
        void setFormulaArray(noNamespace.FormulaDocument.Formula[] formulaArray);
        
        /**
         * Sets ith "formula" element
         */
        void setFormulaArray(int i, noNamespace.FormulaDocument.Formula formula);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "formula" element
         */
        noNamespace.FormulaDocument.Formula insertNewFormula(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "formula" element
         */
        noNamespace.FormulaDocument.Formula addNewFormula();
        
        /**
         * Removes the ith "formula" element
         */
        void removeFormula(int i);
        
        /**
         * Gets array of all "quote" elements
         */
        noNamespace.QuoteDocument.Quote[] getQuoteArray();
        
        /**
         * Gets ith "quote" element
         */
        noNamespace.QuoteDocument.Quote getQuoteArray(int i);
        
        /**
         * Returns number of "quote" element
         */
        int sizeOfQuoteArray();
        
        /**
         * Sets array of all "quote" element
         */
        void setQuoteArray(noNamespace.QuoteDocument.Quote[] quoteArray);
        
        /**
         * Sets ith "quote" element
         */
        void setQuoteArray(int i, noNamespace.QuoteDocument.Quote quote);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "quote" element
         */
        noNamespace.QuoteDocument.Quote insertNewQuote(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "quote" element
         */
        noNamespace.QuoteDocument.Quote addNewQuote();
        
        /**
         * Removes the ith "quote" element
         */
        void removeQuote(int i);
        
        /**
         * Gets array of all "source" elements
         */
        noNamespace.SourceDocument.Source[] getSourceArray();
        
        /**
         * Gets ith "source" element
         */
        noNamespace.SourceDocument.Source getSourceArray(int i);
        
        /**
         * Returns number of "source" element
         */
        int sizeOfSourceArray();
        
        /**
         * Sets array of all "source" element
         */
        void setSourceArray(noNamespace.SourceDocument.Source[] sourceArray);
        
        /**
         * Sets ith "source" element
         */
        void setSourceArray(int i, noNamespace.SourceDocument.Source source);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "source" element
         */
        noNamespace.SourceDocument.Source insertNewSource(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "source" element
         */
        noNamespace.SourceDocument.Source addNewSource();
        
        /**
         * Removes the ith "source" element
         */
        void removeSource(int i);
        
        /**
         * Gets array of all "lText" elements
         */
        noNamespace.LTextDocument.LText[] getLTextArray();
        
        /**
         * Gets ith "lText" element
         */
        noNamespace.LTextDocument.LText getLTextArray(int i);
        
        /**
         * Returns number of "lText" element
         */
        int sizeOfLTextArray();
        
        /**
         * Sets array of all "lText" element
         */
        void setLTextArray(noNamespace.LTextDocument.LText[] lTextArray);
        
        /**
         * Sets ith "lText" element
         */
        void setLTextArray(int i, noNamespace.LTextDocument.LText lText);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "lText" element
         */
        noNamespace.LTextDocument.LText insertNewLText(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "lText" element
         */
        noNamespace.LTextDocument.LText addNewLText();
        
        /**
         * Removes the ith "lText" element
         */
        void removeLText(int i);
        
        /**
         * Gets the "id" attribute
         */
        java.lang.String getId();
        
        /**
         * Gets (as xml) the "id" attribute
         */
        org.apache.xmlbeans.XmlID xgetId();
        
        /**
         * Sets the "id" attribute
         */
        void setId(java.lang.String id);
        
        /**
         * Sets (as xml) the "id" attribute
         */
        void xsetId(org.apache.xmlbeans.XmlID id);
        
        /**
         * Gets the "minimal" attribute
         */
        noNamespace.QuestionDocument.Question.Minimal.Enum getMinimal();
        
        /**
         * Gets (as xml) the "minimal" attribute
         */
        noNamespace.QuestionDocument.Question.Minimal xgetMinimal();
        
        /**
         * Sets the "minimal" attribute
         */
        void setMinimal(noNamespace.QuestionDocument.Question.Minimal.Enum minimal);
        
        /**
         * Sets (as xml) the "minimal" attribute
         */
        void xsetMinimal(noNamespace.QuestionDocument.Question.Minimal minimal);
        
        /**
         * Gets the "answer_style" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType getAnswerStyle();
        
        /**
         * True if has "answer_style" attribute
         */
        boolean isSetAnswerStyle();
        
        /**
         * Sets the "answer_style" attribute
         */
        void setAnswerStyle(org.apache.xmlbeans.XmlAnySimpleType answerStyle);
        
        /**
         * Appends and returns a new empty "answer_style" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType addNewAnswerStyle();
        
        /**
         * Unsets the "answer_style" attribute
         */
        void unsetAnswerStyle();
        
        /**
         * Gets the "answer_type" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType getAnswerType();
        
        /**
         * True if has "answer_type" attribute
         */
        boolean isSetAnswerType();
        
        /**
         * Sets the "answer_type" attribute
         */
        void setAnswerType(org.apache.xmlbeans.XmlAnySimpleType answerType);
        
        /**
         * Appends and returns a new empty "answer_type" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType addNewAnswerType();
        
        /**
         * Unsets the "answer_type" attribute
         */
        void unsetAnswerType();
        
        /**
         * Gets the "knowledge_type" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType getKnowledgeType();
        
        /**
         * True if has "knowledge_type" attribute
         */
        boolean isSetKnowledgeType();
        
        /**
         * Sets the "knowledge_type" attribute
         */
        void setKnowledgeType(org.apache.xmlbeans.XmlAnySimpleType knowledgeType);
        
        /**
         * Appends and returns a new empty "knowledge_type" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType addNewKnowledgeType();
        
        /**
         * Unsets the "knowledge_type" attribute
         */
        void unsetKnowledgeType();
        
        /**
         * Gets the "anscol" attribute
         */
        java.util.List getAnscol();
        
        /**
         * Gets (as xml) the "anscol" attribute
         */
        org.apache.xmlbeans.XmlIDREFS xgetAnscol();
        
        /**
         * True if has "anscol" attribute
         */
        boolean isSetAnscol();
        
        /**
         * Sets the "anscol" attribute
         */
        void setAnscol(java.util.List anscol);
        
        /**
         * Sets (as xml) the "anscol" attribute
         */
        void xsetAnscol(org.apache.xmlbeans.XmlIDREFS anscol);
        
        /**
         * Unsets the "anscol" attribute
         */
        void unsetAnscol();
        
        /**
         * Gets the "title" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType getTitle();
        
        /**
         * True if has "title" attribute
         */
        boolean isSetTitle();
        
        /**
         * Sets the "title" attribute
         */
        void setTitle(org.apache.xmlbeans.XmlAnySimpleType title);
        
        /**
         * Appends and returns a new empty "title" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType addNewTitle();
        
        /**
         * Unsets the "title" attribute
         */
        void unsetTitle();
        
        /**
         * Gets the "sectionId" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType getSectionId();
        
        /**
         * True if has "sectionId" attribute
         */
        boolean isSetSectionId();
        
        /**
         * Sets the "sectionId" attribute
         */
        void setSectionId(org.apache.xmlbeans.XmlAnySimpleType sectionId);
        
        /**
         * Appends and returns a new empty "sectionId" attribute
         */
        org.apache.xmlbeans.XmlAnySimpleType addNewSectionId();
        
        /**
         * Unsets the "sectionId" attribute
         */
        void unsetSectionId();
        
        /**
         * An XML minimal(@).
         *
         * This is an atomic type that is a restriction of noNamespace.QuestionDocument$Question$Minimal.
         */
        public interface Minimal extends org.apache.xmlbeans.XmlToken
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Minimal.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s9AD9431611720D569E6CDA08F9EDE660").resolveHandle("minimalb2f6attrtype");
            
            org.apache.xmlbeans.StringEnumAbstractBase enumValue();
            void set(org.apache.xmlbeans.StringEnumAbstractBase e);
            
            static final Enum NULL = Enum.forString("null");
            static final Enum YES = Enum.forString("yes");
            static final Enum NO = Enum.forString("no");
            
            static final int INT_NULL = Enum.INT_NULL;
            static final int INT_YES = Enum.INT_YES;
            static final int INT_NO = Enum.INT_NO;
            
            /**
             * Enumeration value class for noNamespace.QuestionDocument$Question$Minimal.
             * These enum values can be used as follows:
             * <pre>
             * enum.toString(); // returns the string value of the enum
             * enum.intValue(); // returns an int value, useful for switches
             * // e.g., case Enum.INT_NULL
             * Enum.forString(s); // returns the enum value for a string
             * Enum.forInt(i); // returns the enum value for an int
             * </pre>
             * Enumeration objects are immutable singleton objects that
             * can be compared using == object equality. They have no
             * public constructor. See the constants defined within this
             * class for all the valid values.
             */
            static final class Enum extends org.apache.xmlbeans.StringEnumAbstractBase
            {
                /**
                 * Returns the enum value for a string, or null if none.
                 */
                public static Enum forString(java.lang.String s)
                    { return (Enum)table.forString(s); }
                /**
                 * Returns the enum value corresponding to an int, or null if none.
                 */
                public static Enum forInt(int i)
                    { return (Enum)table.forInt(i); }
                
                private Enum(java.lang.String s, int i)
                    { super(s, i); }
                
                static final int INT_NULL = 1;
                static final int INT_YES = 2;
                static final int INT_NO = 3;
                
                public static final org.apache.xmlbeans.StringEnumAbstractBase.Table table =
                    new org.apache.xmlbeans.StringEnumAbstractBase.Table
                (
                    new Enum[]
                    {
                      new Enum("null", INT_NULL),
                      new Enum("yes", INT_YES),
                      new Enum("no", INT_NO),
                    }
                );
                private static final long serialVersionUID = 1L;
                private java.lang.Object readResolve() { return forInt(intValue()); } 
            }
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static noNamespace.QuestionDocument.Question.Minimal newValue(java.lang.Object obj) {
                  return (noNamespace.QuestionDocument.Question.Minimal) type.newValue( obj ); }
                
                public static noNamespace.QuestionDocument.Question.Minimal newInstance() {
                  return (noNamespace.QuestionDocument.Question.Minimal) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static noNamespace.QuestionDocument.Question.Minimal newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (noNamespace.QuestionDocument.Question.Minimal) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static noNamespace.QuestionDocument.Question newInstance() {
              return (noNamespace.QuestionDocument.Question) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static noNamespace.QuestionDocument.Question newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (noNamespace.QuestionDocument.Question) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static noNamespace.QuestionDocument newInstance() {
          return (noNamespace.QuestionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static noNamespace.QuestionDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (noNamespace.QuestionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static noNamespace.QuestionDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.QuestionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static noNamespace.QuestionDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.QuestionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static noNamespace.QuestionDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.QuestionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static noNamespace.QuestionDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.QuestionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static noNamespace.QuestionDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.QuestionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static noNamespace.QuestionDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.QuestionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static noNamespace.QuestionDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.QuestionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static noNamespace.QuestionDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.QuestionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static noNamespace.QuestionDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.QuestionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static noNamespace.QuestionDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.QuestionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static noNamespace.QuestionDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.QuestionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static noNamespace.QuestionDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.QuestionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static noNamespace.QuestionDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.QuestionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static noNamespace.QuestionDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.QuestionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static noNamespace.QuestionDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (noNamespace.QuestionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static noNamespace.QuestionDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (noNamespace.QuestionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
