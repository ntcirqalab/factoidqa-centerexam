/*
 * An XML document type.
 * Localname: source
 * Namespace: 
 * Java type: noNamespace.SourceDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.centerExam;


/**
 * A document containing one source(@) element.
 *
 * This is a complex type.
 */
public interface SourceDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(SourceDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s9AD9431611720D569E6CDA08F9EDE660").resolveHandle("source71d4doctype");
    
    /**
     * Gets the "source" element
     */
    noNamespace.SourceDocument.Source getSource();
    
    /**
     * Sets the "source" element
     */
    void setSource(noNamespace.SourceDocument.Source source);
    
    /**
     * Appends and returns a new empty "source" element
     */
    noNamespace.SourceDocument.Source addNewSource();
    
    /**
     * An XML source(@).
     *
     * This is a complex type.
     */
    public interface Source extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Source.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s9AD9431611720D569E6CDA08F9EDE660").resolveHandle("source510delemtype");
        
        /**
         * Gets array of all "garbled" elements
         */
        noNamespace.GarbledDocument.Garbled[] getGarbledArray();
        
        /**
         * Gets ith "garbled" element
         */
        noNamespace.GarbledDocument.Garbled getGarbledArray(int i);
        
        /**
         * Returns number of "garbled" element
         */
        int sizeOfGarbledArray();
        
        /**
         * Sets array of all "garbled" element
         */
        void setGarbledArray(noNamespace.GarbledDocument.Garbled[] garbledArray);
        
        /**
         * Sets ith "garbled" element
         */
        void setGarbledArray(int i, noNamespace.GarbledDocument.Garbled garbled);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "garbled" element
         */
        noNamespace.GarbledDocument.Garbled insertNewGarbled(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "garbled" element
         */
        noNamespace.GarbledDocument.Garbled addNewGarbled();
        
        /**
         * Removes the ith "garbled" element
         */
        void removeGarbled(int i);
        
        /**
         * Gets array of all "missing" elements
         */
        noNamespace.MissingDocument.Missing[] getMissingArray();
        
        /**
         * Gets ith "missing" element
         */
        noNamespace.MissingDocument.Missing getMissingArray(int i);
        
        /**
         * Returns number of "missing" element
         */
        int sizeOfMissingArray();
        
        /**
         * Sets array of all "missing" element
         */
        void setMissingArray(noNamespace.MissingDocument.Missing[] missingArray);
        
        /**
         * Sets ith "missing" element
         */
        void setMissingArray(int i, noNamespace.MissingDocument.Missing missing);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "missing" element
         */
        noNamespace.MissingDocument.Missing insertNewMissing(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "missing" element
         */
        noNamespace.MissingDocument.Missing addNewMissing();
        
        /**
         * Removes the ith "missing" element
         */
        void removeMissing(int i);
        
        /**
         * Gets array of all "br" elements
         */
        noNamespace.BrDocument.Br[] getBrArray();
        
        /**
         * Gets ith "br" element
         */
        noNamespace.BrDocument.Br getBrArray(int i);
        
        /**
         * Returns number of "br" element
         */
        int sizeOfBrArray();
        
        /**
         * Sets array of all "br" element
         */
        void setBrArray(noNamespace.BrDocument.Br[] brArray);
        
        /**
         * Sets ith "br" element
         */
        void setBrArray(int i, noNamespace.BrDocument.Br br);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "br" element
         */
        noNamespace.BrDocument.Br insertNewBr(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "br" element
         */
        noNamespace.BrDocument.Br addNewBr();
        
        /**
         * Removes the ith "br" element
         */
        void removeBr(int i);
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static noNamespace.SourceDocument.Source newInstance() {
              return (noNamespace.SourceDocument.Source) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static noNamespace.SourceDocument.Source newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (noNamespace.SourceDocument.Source) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static noNamespace.SourceDocument newInstance() {
          return (noNamespace.SourceDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static noNamespace.SourceDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (noNamespace.SourceDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static noNamespace.SourceDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.SourceDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static noNamespace.SourceDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.SourceDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static noNamespace.SourceDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.SourceDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static noNamespace.SourceDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.SourceDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static noNamespace.SourceDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.SourceDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static noNamespace.SourceDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.SourceDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static noNamespace.SourceDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.SourceDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static noNamespace.SourceDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.SourceDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static noNamespace.SourceDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.SourceDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static noNamespace.SourceDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (noNamespace.SourceDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static noNamespace.SourceDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.SourceDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static noNamespace.SourceDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.SourceDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static noNamespace.SourceDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.SourceDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static noNamespace.SourceDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (noNamespace.SourceDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static noNamespace.SourceDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (noNamespace.SourceDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static noNamespace.SourceDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (noNamespace.SourceDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
