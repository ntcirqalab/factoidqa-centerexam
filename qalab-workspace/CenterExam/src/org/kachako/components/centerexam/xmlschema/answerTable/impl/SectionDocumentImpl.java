/*
 * An XML document type.
 * Localname: section
 * Namespace: 
 * Java type: SectionDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.answerTable.impl;

import org.kachako.components.centerexam.xmlschema.answerTable.SectionDocument;

/**
 * A document containing one section(@) element.
 *
 * This is a complex type.
 */
public class SectionDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements SectionDocument
{
    private static final long serialVersionUID = 1L;
    
    public SectionDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SECTION$0 = 
        new javax.xml.namespace.QName("", "section");
    
    
    /**
     * Gets the "section" element
     */
    public java.lang.String getSection()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SECTION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "section" element
     */
    public org.apache.xmlbeans.XmlString xgetSection()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SECTION$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "section" element
     */
    public void setSection(java.lang.String section)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SECTION$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SECTION$0);
            }
            target.setStringValue(section);
        }
    }
    
    /**
     * Sets (as xml) the "section" element
     */
    public void xsetSection(org.apache.xmlbeans.XmlString section)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(SECTION$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(SECTION$0);
            }
            target.set(section);
        }
    }
}
