/*
 * An XML document type.
 * Localname: missing
 * Namespace: 
 * Java type: noNamespace.MissingDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.centerExam.impl;
/**
 * A document containing one missing(@) element.
 *
 * This is a complex type.
 */
public class MissingDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.MissingDocument
{
    private static final long serialVersionUID = 1L;
    
    public MissingDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName MISSING$0 = 
        new javax.xml.namespace.QName("", "missing");
    
    
    /**
     * Gets the "missing" element
     */
    public noNamespace.MissingDocument.Missing getMissing()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.MissingDocument.Missing target = null;
            target = (noNamespace.MissingDocument.Missing)get_store().find_element_user(MISSING$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "missing" element
     */
    public void setMissing(noNamespace.MissingDocument.Missing missing)
    {
        generatedSetterHelperImpl(missing, MISSING$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "missing" element
     */
    public noNamespace.MissingDocument.Missing addNewMissing()
    {
        synchronized (monitor())
        {
            check_orphaned();
            noNamespace.MissingDocument.Missing target = null;
            target = (noNamespace.MissingDocument.Missing)get_store().add_element_user(MISSING$0);
            return target;
        }
    }
    /**
     * An XML missing(@).
     *
     * This is a complex type.
     */
    public static class MissingImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements noNamespace.MissingDocument.Missing
    {
        private static final long serialVersionUID = 1L;
        
        public MissingImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName STRING$0 = 
            new javax.xml.namespace.QName("", "string");
        
        
        /**
         * Gets the "string" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType getString()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().find_attribute_user(STRING$0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "string" attribute
         */
        public boolean isSetString()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(STRING$0) != null;
            }
        }
        
        /**
         * Sets the "string" attribute
         */
        public void setString(org.apache.xmlbeans.XmlAnySimpleType string)
        {
            generatedSetterHelperImpl(string, STRING$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "string" attribute
         */
        public org.apache.xmlbeans.XmlAnySimpleType addNewString()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlAnySimpleType target = null;
                target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().add_attribute_user(STRING$0);
                return target;
            }
        }
        
        /**
         * Unsets the "string" attribute
         */
        public void unsetString()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(STRING$0);
            }
        }
    }
}
