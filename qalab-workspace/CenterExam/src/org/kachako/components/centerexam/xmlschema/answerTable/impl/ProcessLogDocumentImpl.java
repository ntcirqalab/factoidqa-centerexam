/*
 * An XML document type.
 * Localname: process_log
 * Namespace: 
 * Java type: ProcessLogDocument
 *
 * Automatically generated - do not modify.
 */
package org.kachako.components.centerexam.xmlschema.answerTable.impl;

import org.kachako.components.centerexam.xmlschema.answerTable.ProcessLogDocument;

/**
 * A document containing one process_log(@) element.
 *
 * This is a complex type.
 */
public class ProcessLogDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements ProcessLogDocument
{
    private static final long serialVersionUID = 1L;
    
    public ProcessLogDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PROCESSLOG$0 = 
        new javax.xml.namespace.QName("", "process_log");
    
    
    /**
     * Gets the "process_log" element
     */
    public java.lang.String getProcessLog()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PROCESSLOG$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "process_log" element
     */
    public org.apache.xmlbeans.XmlString xgetProcessLog()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PROCESSLOG$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "process_log" element
     */
    public void setProcessLog(java.lang.String processLog)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PROCESSLOG$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PROCESSLOG$0);
            }
            target.setStringValue(processLog);
        }
    }
    
    /**
     * Sets (as xml) the "process_log" element
     */
    public void xsetProcessLog(org.apache.xmlbeans.XmlString processLog)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PROCESSLOG$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(PROCESSLOG$0);
            }
            target.set(processLog);
        }
    }
}
