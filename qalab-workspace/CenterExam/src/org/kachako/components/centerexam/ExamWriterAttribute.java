package org.kachako.components.centerexam;

import java.io.File;

import org.apache.uima.jcas.tcas.Annotation;
import org.apache.xmlbeans.XmlCursor;
import org.kachako.types.centerexam.Math;

public class ExamWriterAttribute {
 
		
	void setAttribute(XmlCursor cursor, String tag, Annotation ann, String imageDir){				
		
		if(tag.equals("exam")){

			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("source")) != null) cursor.insertAttributeWithValue("source", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("source")));
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("srcTxtURL")) != null) cursor.insertAttributeWithValue("srcTxtURL", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("srcTxtURL")));
			cursor.insertAttributeWithValue("subject", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("subject")));
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("year")) != null) cursor.insertAttributeWithValue("year", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("year")));
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("range_of_options")) != null) cursor.insertAttributeWithValue("range_of_options", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("range_of_options")));
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("num_of_options")) != null) cursor.insertAttributeWithValue("num_of_options", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("num_of_options")));

		}
		
		else if(tag.equals("ansColumn")){
			
			cursor.insertAttributeWithValue("id", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("id")));
			
		}
		
		else if(tag.equals("blank")){
			
			cursor.insertAttributeWithValue("id", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("id")));
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("digits")) != null) cursor.insertAttributeWithValue("digits", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("digits")));
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("anscolumn_id")) != null) cursor.insertAttributeWithValue("anscolumn_id", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("anscolumn_id")));
			
		}
		
		else if(tag.equals("cell")){
			
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("types")) != null) cursor.insertAttributeWithValue("type", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("types")));
			
		}
		
		else if(tag.equals("choice")){
			
			cursor.insertAttributeWithValue("comment", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("comment")));
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("ra")) != null) cursor.insertAttributeWithValue("ra", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("ra")));
			else cursor.insertAttributeWithValue("ra", "no");
			cursor.insertAttributeWithValue("ansnum", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("ansnum")));
			
		}
		
		else if(tag.equals("choices")){
			
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("anscol")) != null) cursor.insertAttributeWithValue("anscol", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("anscol")));
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("comment")) != null) cursor.insertAttributeWithValue("comment", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("comment")));
			
		}
		
		else if(tag.equals("data")){
			
			cursor.insertAttributeWithValue("id", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("id")));
			cursor.insertAttributeWithValue("type", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("types")));
			
		}
		
		else if(tag.equals("formula")){
			
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("comment")) != null) cursor.insertAttributeWithValue("comment", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("comment")));
			
		}
		
		else if(tag.equals("img")){

		    String featureValueAsString = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("src"));
		    String imageUrl = "file://" + imageDir + File.separator + featureValueAsString;
		    cursor.insertAttributeWithValue("src", imageUrl);
            if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("comment")) != null) cursor.insertAttributeWithValue("comment", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("comment")));
            if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("corresponding_obj")) != null) cursor.insertAttributeWithValue("corresponding_obj", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("corresponding_obj")));
		}
		
		else if(tag.equals("lText")){
			
			cursor.insertAttributeWithValue("id", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("id")));
			
		}
		
		else if(tag.equals("missing")){
			
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("attributeString")) != null) cursor.insertAttributeWithValue("string", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("attributeString")));
			
		}
		
		else if(tag.equals("note")){
			
			cursor.insertAttributeWithValue("id", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("id")));
			
		}
		
		else if(tag.equals("question")){
			
			cursor.insertAttributeWithValue("id", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("id")));
			cursor.insertAttributeWithValue("minimal", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("minimal")));
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("answer_style")) != null) cursor.insertAttributeWithValue("answer_style", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("answer_style")));
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("answer_type")) != null) cursor.insertAttributeWithValue("answer_type", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("answer_type")));
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("knowledge_type")) != null) cursor.insertAttributeWithValue("knowledge_type", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("knowledge_type")));
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("anscol")) != null) cursor.insertAttributeWithValue("anscol", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("anscol")));
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("title")) != null) cursor.insertAttributeWithValue("title", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("title")));
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("sectionId")) != null) cursor.insertAttributeWithValue("sectionId", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("sectionId")));
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("anscolumn_ids")) != null) cursor.insertAttributeWithValue("anscolumn_ids", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("anscolumn_ids")));
		}
		
		else if(tag.equals("ref")){
			
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("comment")) != null) cursor.insertAttributeWithValue("comment", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("comment")));
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("target")) != null) cursor.insertAttributeWithValue("target", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("target")));
			
		}
		
		else if(tag.equals("tbl")){
			
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("numOfCols")) != null) cursor.insertAttributeWithValue("numOfCols", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("numOfCols")));
			if(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("numOfRows")) != null) cursor.insertAttributeWithValue("numOfRows", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("numOfRows")));
			
		}
		
		else if(tag.equals("uText")){
			
			cursor.insertAttributeWithValue("id", ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("id")));
			
		}
		
		else if(tag.equals("math")){
			String attr = ((Math)ann).getXmlns();
			if(attr != null){
//				cursor.insertNamespace("m", attr);
			}
			attr = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("display"));
			if(attr != null){
				cursor.insertAttributeWithValue("display", attr);
			}
			
		}
		
		else if(tag.equals("annotation-xml")){
			
			String attr = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("encoding"));
			if(attr != null){
				cursor.insertAttributeWithValue("encoding", attr);
			}
			
		}
		
		else if(tag.equals("mi")){
			
			String attr = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("mathvariant"));
			if(attr != null){
				cursor.insertAttributeWithValue("mathvariant", attr);
			}
			attr = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("anscolumn_id"));
			if(attr != null){
				cursor.insertAttributeWithValue("anscolumn_id", attr);
			}
			
		}

		else if(tag.equals("ci")){
			
			String attr = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("ciType"));
			if(attr != null){
				cursor.insertAttributeWithValue("citype", attr);
			}
			
		}

		else if(tag.equals("mo")){
			
			String attr = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("stretchy"));
			if(attr != null){
				cursor.insertAttributeWithValue("stretchy", attr);
			}
			
		}
		
		else if(tag.equals("mfenced")){
			
			String attr = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("separators"));
			if(attr != null){
				cursor.insertAttributeWithValue("separators", attr);
			}
			
			attr = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("open"));
			if(attr != null){
				cursor.insertAttributeWithValue("open", attr);
			}
			
			attr = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("close"));
			if(attr != null){
				cursor.insertAttributeWithValue("close", attr);
			}
			
		}
		
		else if(tag.equals("mtable")){
			
			String attr = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("columnalign"));
			if(attr != null){
				cursor.insertAttributeWithValue("columnalign", attr);
			}
			
		}

		else if(tag.equals("csymbol")){
			
			String attr = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("cd"));
			if(attr != null){
				cursor.insertAttributeWithValue("cd", attr);
			}
			
			attr = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("definitionURL"));
			
		}

		else if(tag.equals("table")){
			
			String attr = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("xmlns"));
			if(attr != null){
				cursor.insertAttribute("xmlns", attr);
			}
			
		}
		

		else if(tag.equals("mstyle")){
			
			String attr = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("displaystyle"));
			if(attr != null){
				cursor.insertAttributeWithValue("displaystyle", attr);
			}
			
		}
		
		else if(tag.equals("mspace")){
			
			String attr = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("width"));
			if(attr != null){
				cursor.insertAttribute("width", attr);
			}
		}
		
		else if(tag.equals("adata")){
			
			String attr = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("id"));
			if(attr != null){
				cursor.insertAttributeWithValue("id", attr);
			}
			attr = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("adataType"));
			if(attr != null){
				cursor.insertAttributeWithValue("type", attr);
			}
			attr = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("field"));
			if(attr != null){
				cursor.insertAttributeWithValue("field", attr);
			}
		}
		
	}

}
