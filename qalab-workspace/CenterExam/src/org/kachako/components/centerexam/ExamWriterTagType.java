package org.kachako.components.centerexam;

import org.apache.uima.cas.Type;
import org.kachako.types.centerexam.*;
import org.kachako.types.centerexam.Math;

public class ExamWriterTagType {
			
	String getBeginTagType(Type annotationType){
		
		String tag = "";
		String typeName = annotationType.toString();
		
		
		if(typeName.equals(Exam.class.getName())){
			tag = "exam";
		}
		else if(typeName.equals(AnsColumn.class.getName())){
			tag = "ansColumn";
		}
		else if(typeName.equals(Blank.class.getName())){
			tag = "blank";
		}
		else if(typeName.equals(Br.class.getName())){
			tag = "br";
		}
		else if(typeName.equals(Cell.class.getName())){
			tag = "cell";
		}
		else if(typeName.equals(Choice.class.getName())){
			tag = "choice";
		}
		else if(typeName.equals(Choices.class.getName())){
			tag = "choices";
		}
		else if(typeName.equals(CNum.class.getName())){
			tag = "cNum";
		}
		else if(typeName.equals(Data.class.getName())){
			tag = "data";
		}
		else if(typeName.equals(Formula.class.getName())){
			tag = "formula";
		}
		else if(typeName.equals(Garbled.class.getName())){
			tag = "garbled";
		}
		else if(typeName.equals(Img.class.getName())){
			tag = "img";
		}
		else if(typeName.equals(Info.class.getName())){
			tag = "info";
		}
		else if(typeName.equals(Instruction.class.getName())){
			tag = "instruction";
		}
		else if(typeName.equals(Label.class.getName())){
			tag = "label";
		}
		else if(typeName.equals(LText.class.getName())){
			tag = "lText";
		}
		else if(typeName.equals(Missing.class.getName())){
			tag = "missing";
		}
		else if(typeName.equals(Note.class.getName())){
			tag = "note";
		}
		else if(typeName.equals(Question.class.getName())){
			tag = "question";
		}
		else if(typeName.equals(Quote.class.getName())){
			tag = "quote";
		}
		else if(typeName.equals(Ref.class.getName())){
			tag = "ref";
		}
		else if(typeName.equals(Row.class.getName())){
			tag = "row";
		}
		else if(typeName.equals(Source.class.getName())){
			tag = "source";
		}
		else if(typeName.equals(Tbl.class.getName())){
			tag = "tbl";
		}
		else if(typeName.equals(Title.class.getName())){
			tag = "title";
		}
		else if(typeName.equals(UText.class.getName())){
			tag = "uText";
		}
		else if(typeName.equals(Caption.class.getName())){
			tag = "caption";
		}
		else if(typeName.equals(Math.class.getName())){
			tag = "math";
		}
		else if(typeName.equals(Semantics.class.getName())){
			tag = "semantics";
		}
		else if(typeName.equals(AnnotationXml.class.getName())){
			tag = "annotation-xml";
		}
		else if(typeName.equals(Mrow.class.getName())){
			tag = "mrow";
		}
		else if(typeName.equals(Mi.class.getName())){
			tag = "mi";
		}
		else if(typeName.equals(Ci.class.getName())){
			tag = "ci";
		}
		else if(typeName.equals(Msub.class.getName())){
			tag = "msub";
		}
		else if(typeName.equals(Apply.class.getName())){
			tag = "apply";
		}
		else if(typeName.equals(Selector.class.getName())){
			tag = "selector";
		}
		else if(typeName.equals(Mo.class.getName())){
			tag = "mo";
		}
		else if(typeName.equals(Mover.class.getName())){
			tag = "mover";
		}
		else if(typeName.equals(Mn.class.getName())){
			tag = "mn";
		}
		else if(typeName.equals(Cn.class.getName())){
			tag = "cn";
		}
		else if(typeName.equals(Mfenced.class.getName())){
			tag = "mfenced";
		}
		else if(typeName.equals(Msup.class.getName())){
			tag = "msup";
		}
		else if(typeName.equals(Vector.class.getName())){
			tag = "vector";
		}
		else if(typeName.equals(Mtable.class.getName())){
			tag = "mtable";
		}
		else if(typeName.equals(Mtr.class.getName())){
			tag = "mtr";
		}
		else if(typeName.equals(Mtd.class.getName())){
			tag = "mtd";
		}
		else if(typeName.equals(Mtext.class.getName())){
			tag = "mtext";
		}
		else if(typeName.equals(Piecewise.class.getName())){
			tag = "piecewise";
		}
		else if(typeName.equals(Piece.class.getName())){
			tag = "piece";
		}
		else if(typeName.equals(Otherwise.class.getName())){
			tag = "othrewise";
		}
		else if(typeName.equals(Plus.class.getName())){
			tag = "plus";
		}
		else if(typeName.equals(Minus.class.getName())){
			tag = "minus";
		}
		else if(typeName.equals(Times.class.getName())){
			tag = "times";
		}
		else if(typeName.equals(Divide.class.getName())){
			tag = "divide";
		}
		else if(typeName.equals(Power.class.getName())){
			tag = "power";
		}
		else if(typeName.equals(Root.class.getName())){
			tag = "root";
		}
		else if(typeName.equals(Abs.class.getName())){
			tag = "abs";
		}
		else if(typeName.equals(Conjugate.class.getName())){
			tag = "conjugate";
		}
		else if(typeName.equals(Arg.class.getName())){
			tag = "arg";
		}
		else if(typeName.equals(And.class.getName())){
			tag = "and";
		}
		else if(typeName.equals(Or.class.getName())){
			tag = "or";
		}
		else if(typeName.equals(Not.class.getName())){
			tag = "not";
		}
		else if(typeName.equals(ImPlies.class.getName())){
			tag = "implies";
		}
		else if(typeName.equals(Forall.class.getName())){
			tag = "forall";
		}
		else if(typeName.equals(Eq.class.getName())){
			tag = "eq";
		}
		else if(typeName.equals(Neq.class.getName())){
			tag = "neq";
		}
		else if(typeName.equals(Gt.class.getName())){
			tag = "gt";
		}
		else if(typeName.equals(Lt.class.getName())){
			tag = "lt";
		}
		else if(typeName.equals(Geq.class.getName())){
			tag = "geq";
		}
		else if(typeName.equals(Leq.class.getName())){
			tag = "leq";
		}
		else if(typeName.equals(Equivalent.class.getName())){
			tag = "equivalent";
		}
		else if(typeName.equals(Factorof.class.getName())){
			tag = "factorof";
		}
		else if(typeName.equals(Diff.class.getName())){
			tag = "diff";
		}
		else if(typeName.equals(Int.class.getName())){
			tag = "int";
		}
		else if(typeName.equals(Set.class.getName())){
			tag = "set";
		}
		else if(typeName.equals(List.class.getName())){
			tag = "list";
		}
		else if(typeName.equals(Union.class.getName())){
			tag = "union";
		}
		else if(typeName.equals(Intersect.class.getName())){
			tag = "intersect";
		}
		else if(typeName.equals(Prsubset.class.getName())){
			tag = "prsubset";
		}
		else if(typeName.equals(Sin.class.getName())){
			tag = "sin";
		}
		else if(typeName.equals(Cos.class.getName())){
			tag = "cos";
		}
		else if(typeName.equals(Tan.class.getName())){
			tag = "tan";
		}
		else if(typeName.equals(Log.class.getName())){
			tag = "log";
		}
		else if(typeName.equals(Integers.class.getName())){
			tag = "integers";
		}
		else if(typeName.equals(Naturalnumbers.class.getName())){
			tag = "naturalnumbers";
		}
		else if(typeName.equals(Imaginaryi.class.getName())){
			tag = "imaginaryi";
		}
		else if(typeName.equals(Pi.class.getName())){
			tag = "pi";
		}
		else if(typeName.equals(Csymbol.class.getName())){
			tag = "csymbol";
		}
		else if(typeName.equals(Mmultiscripts.class.getName())){
			tag = "mmultiscripts";
		}
		else if(typeName.equals(Mprescripts.class.getName())){
			tag = "mprescripts";
		}
		else if(typeName.equals(None.class.getName())){
			tag = "none";
		}
		else if(typeName.equals(Bvar.class.getName())){
			tag = "bvar";
		}
		else if(typeName.equals(Condition.class.getName())){
			tag = "condition";
		}
		else if(typeName.equals(Table.class.getName())){
			tag = "table";
		}
		else if(typeName.equals(Thead.class.getName())){
			tag = "thead";
		}
		else if(typeName.equals(Tbody.class.getName())){
			tag = "tbody";
		}
		else if(typeName.equals(Tr.class.getName())){
			tag = "tr";
		}
		else if(typeName.equals(Th.class.getName())){
			tag = "th";
		}
		else if(typeName.equals(Td.class.getName())){
			tag = "td";
		}
		else if(typeName.equals(Mfrac.class.getName())){
			tag = "mfrac";
		}
		else if(typeName.equals(Munderorver.class.getName())){
			tag = "munderover";
		}
		else if(typeName.equals(Msubsup.class.getName())){
			tag = "msubsup";
		}
		else if(typeName.equals(Msqrt.class.getName())){
			tag = "msqrt";
		}
		else if(typeName.equals(Mroot.class.getName())){
			tag = "mroot";
		}
		else if(typeName.equals(Mstyle.class.getName())){
			tag = "mstyle";
		}
		else if(typeName.equals(ScalarProduct.class.getName())){
			tag = "scalarproduct";
		}
		else if(typeName.equals(In.class.getName())){
			tag = "in";
		}
		else if(typeName.equals(LowLimit.class.getName())){
			tag = "lowlimit";
		}
		else if(typeName.equals(UpLimit.class.getName())){
			tag = "uplimit";
		}
		else if(typeName.equals(LogBase.class.getName())){
			tag = "logbase";
		}
		else if(typeName.equals(Card.class.getName())){
			tag = "card";
		}
		else if(typeName.equals(Degree.class.getName())){
			tag = "degree";
		}
		else if(typeName.equals(Sum.class.getName())){
			tag = "sum";
		}
		else if(typeName.equals(Product.class.getName())){
			tag = "product";
		}
		else if(typeName.equals(Mspace.class.getName())){
			tag = "mspace";
		}
		else if(typeName.equals(Adata.class.getName())){
			tag = "adata";
		}
		else if(typeName.equals(OkuriL.class.getName())){
			tag = "okuriL";
		}
		else if(typeName.equals(Okuri.class.getName())){
			tag = "okuri";
		}
		else if(typeName.equals(Sub.class.getName())){
			tag = "sub";
		}
		else if(typeName.equals(Sup.class.getName())){
			tag = "sup";
		}
		else if(typeName.equals(Kaeri.class.getName())){
			tag = "kaeri";
		}
		
		return tag;
	}

}
