package org.kachako.components.centerexam.evaluation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.AbstractMap.SimpleEntry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Arrays;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.StringArray;
import org.apache.uima.jcas.cas.TOP;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.kachako.comparable.analysis_engine.ComparisonScoreResult;
import org.kachako.comparable.analysis_engine.TotalResultCalculator;
import org.kachako.composition.cwf.ComparableUtils;
import org.kachako.types.centerexam.answertable.*;
import org.kachako.types.comparable.AnnotationGroup;
import org.kachako.types.comparable.AnnotationGroupCluster;
import org.kachako.types.comparable.ComparisonSet;
import org.kachako.types.comparable.MatchedPair;

public class CenterExamMatchEvaluation4Kachako extends JCasAnnotator_ImplBase{
	
	static final String NORMAL = "normal";			//通常
	static final String COMPLETION = "completion";	//未解答を１で補完
	static final String EXCLUTION = "exclusion";	//統計に含めない
	static final String PARAM_MODE = "Mode";
	
	final String RANDOM_ORDER = "\\|";
	final String MULTIPLE_CORRECT ="\\|\\|";
	final String EXACT_MATCH_ORDER =",";
	
	 int mode;	//0:通常 1:未解答を１で補完 2:統計に含めない
	
	@Override
	public void initialize(UimaContext aContext)
			throws ResourceInitializationException {
		
		System.out.println("------- CenterExamMatchEvaluation(Kachako) initialize -------");
		
		if(aContext.getConfigParameterValue(PARAM_MODE) != null){		
			String param_mode = aContext.getConfigParameterValue(PARAM_MODE).toString().trim().toLowerCase();
			if(param_mode.equals(NORMAL)){
				mode = 0;
				System.out.println("CenterExamMatch Normal mode");
			}
			else if(param_mode.equals(COMPLETION)){
				mode = 1;
				System.out.println("CenterExamMatch Completion mode");
			}
			else if(param_mode.equals(EXCLUTION)){
				mode = 2;
				System.out.println("CenterExamMatch Exclusion mode");
			}
			else{
				mode = 0;
				param_mode = aContext.getConfigParameterValue(PARAM_MODE).toString().trim();
				System.err.println(param_mode + " mode is Unsupported !");
				System.out.println("CenterExamMatch Normal mode");
			}
		}
		else{
			mode = 0;
			System.out.println("CenterExamMatch Normal mode");
		}
		super.initialize(aContext);
	}
	
	@Override
	 public void process(JCas pjcas) throws AnalysisEngineProcessException {

		System.out.println("------- CenterExamMatchEvaluation(Kachako)  process -------");
		JCas jcas = null;
		try {
			jcas = pjcas.getView("KachakoComparison");
		} catch (CASException e) {
			System.out.println("no KachakoComparision View");
			e.printStackTrace();
			return;
		}
		
		Pattern randomPattern = Pattern.compile(RANDOM_ORDER);
		Pattern multiplePattern = Pattern.compile(MULTIPLE_CORRECT);
		Pattern exactPattern = Pattern.compile(EXACT_MATCH_ORDER);

		ComparisonSet comparisonSet = loopAnnotationGroupCluster(jcas);
		if (comparisonSet == null) {
			System.out.println("comparisonSet is null");
			return;
		}
		TypeSystem typeSystem = jcas.getTypeSystem();
//		FSIterator<FeatureStructure> comparisonSetIt = jcas.getFSIndexRepository().getAllIndexedFS(typeSystem.getType(ComparisonSet.class.getName()));
		
//		if ( ! comparisonSetIt.hasNext() ) return;
//		ComparisonSet comparisonSet = (ComparisonSet) comparisonSetIt.next();
		
		SortedAnnotations goldSortedAnnotations, testSortedAnnotations;
//	    String typeString = comparisonSet.getTypes().get(0);
///		String typeString = AnswerTableTag.class.getName();
		Type typeToBeCompared = typeSystem.getType(AnswerTableTag.class.getName());
//System.out.println("Check ExamMach");
//System.out.println(comparisonSet.getGoldAnnotations() == null && comparisonSet.getTestAnnotations() == null);	      

		String goldName = "";
		String testName = "";
		if ( comparisonSet.getGoldAnnotations() == null && comparisonSet.getTestAnnotations() == null ) {

			AnnotationGroup goldAnnotationGroup = comparisonSet.getGoldAnnotationGroup();
			AnnotationGroup testAnnotationGroup = comparisonSet.getTestAnnotationGroup();
			
			goldSortedAnnotations = new SortedAnnotations(goldAnnotationGroup, typeToBeCompared, typeSystem);
			testSortedAnnotations = new SortedAnnotations(testAnnotationGroup, typeToBeCompared, typeSystem);
			
			FSArray goldAnnotations = new FSArray(jcas, goldSortedAnnotations.size());
			goldAnnotations.addToIndexes();
			
			for (int i = 0; i < goldSortedAnnotations.size(); i++) {
				goldName = goldSortedAnnotations.get(i).getType().getName();
				Annotation gold = (Annotation)goldSortedAnnotations.get(i);
				goldAnnotations.set(i, gold);
			}
			comparisonSet.setGoldAnnotations(goldAnnotations);
			
			FSArray testAnnotations = new FSArray(jcas, testSortedAnnotations.size());
			testAnnotations.addToIndexes();
			for (int i = 0; i < testSortedAnnotations.size(); i++) {
				testName = testSortedAnnotations.get(i).getType().getName();
				Annotation test = (Annotation)testSortedAnnotations.get(i);
				testAnnotations.set(i, test);
			}
			comparisonSet.setTestAnnotations(testAnnotations);

		} else {
			goldSortedAnnotations = new SortedAnnotations(comparisonSet.getGoldAnnotations(), typeToBeCompared, typeSystem);
			testSortedAnnotations = new SortedAnnotations(comparisonSet.getTestAnnotations(), typeToBeCompared, typeSystem);	    	
		}

		ArrayList<MatchedPair> matchedPairs = new ArrayList<MatchedPair>();
		String componentID = null;
		
		HashMap<String, String> goldScoreMap = new HashMap<String, String>();
		HashMap<String, String> goldAnswerMap = new HashMap<String, String>();
		HashMap<String, Annotation> goldMap = new HashMap<String, Annotation>();
		HashMap<String, String> testAnswerMap = new HashMap<String, String>();
		HashMap<String, Annotation> testAnswerColumIDMap = new HashMap<String, Annotation>();
		HashMap<String, Annotation> testMap = new HashMap<String, Annotation>();
		HashMap<String, Integer> scoreMap = new HashMap<String, Integer>();
		
		ArrayList<String> goldAnswerColumnIDList = new ArrayList<String>();
		ArrayList<String> testAnswerColumnIDList = new ArrayList<String>();
		
		String goldKey = null;
		String testKey = null;
		String answer = null;
		String score = null;
		String perfectScore = null;
		String selected = null;
		
		TreeSet<String> skipQuestion = new TreeSet<String>();
		
		int numOfQuestion = 0;
		int totalScore = 0;
		int correctAnswer = 0;
		
		AnswerMachineType answerMachineType = null;
		Annotation tmpGoldAnnotation = null;
		Annotation dataType = null;
		Annotation ansColumn_IDType = null;
		
System.out.println("componentID: "+comparisonSet.getGoldAnnotationGroup().getComponentID() +"/"+ comparisonSet.getTestAnnotationGroup().getComponentID());
System.out.println("collectionReaderGenerated: "+comparisonSet.getTestAnnotationGroup().getCollectionReaderGenerated() );
		
		if ( comparisonSet.getGoldAnnotationGroup().getCollectionReaderGenerated() )
			componentID = comparisonSet.getTestAnnotationGroup().getComponentID();
		else if( comparisonSet.getTestAnnotationGroup().getCollectionReaderGenerated() ) {
			componentID = comparisonSet.getGoldAnnotationGroup().getComponentID();
			goldSortedAnnotations = new SortedAnnotations(comparisonSet.getTestAnnotations(), typeToBeCompared, typeSystem);
			testSortedAnnotations = new SortedAnnotations(comparisonSet.getGoldAnnotations(), typeToBeCompared, typeSystem);
		} else {
			FSArray array = new FSArray(jcas, 0);
			array.addToIndexes();
			comparisonSet.setMatchedAnnotations(array);
System.out.println("setMatchedAnnotations -> return");			
			return;
		}
//	    componentID = comparisonSet.getGoldAnnotationGroup().getComponentID();

		
		if (testSortedAnnotations.size() > 0) {
			Annotation ann = (Annotation) testSortedAnnotations.get(0);
			if (ann.getType().getName().equals(AnswerTable.class.getName())) {
				selected = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("selected"));
			}
		}
		
System.out.println("selected="+selected);
		
		if (selected != null && selected.trim().length() > 0) {
			ArrayList<String> ids = new ArrayList<String>();
			for(String id : selected.split(" ")){
				ids.add(id);
System.out.println("ids="+ids);
			}
			for (int i = 0; i < goldSortedAnnotations.size(); i++) {
				if (goldSortedAnnotations.get(i).getType().getName().equals(Option.class.getName())) {
					Option option = (Option) goldSortedAnnotations.get(i);
System.out.println("goldSortedAnnotations:" + option.getSection_ID() + ": " + option.getAnscolumns());
					if (!ids.contains(option.getSection_ID()) && option.getAnscolumns() != null && option.getAnscolumns().trim().length() > 0) {
						for (String id : option.getAnscolumns().split(" ")){
							skipQuestion.add(id);
						}
					}
				} else if(goldSortedAnnotations.get(i).getType().getName().equals(DataAT.class.getName()))
					break;
			}
		}
//	      System.out.println(skipQuestion);
		int tmp_i = 0;
		
System.out.println("goldSortedAnnotations.size()="+goldSortedAnnotations.size());
		for (int i = 0; i < goldSortedAnnotations.size(); i++) {
			goldName = goldSortedAnnotations.get(i).getType().getName();
			
			if(goldName.equals(DataAT.class.getName()))
				tmp_i = i;
			
			if(goldName.equals(DataAT.class.getName()) && goldKey != null && !skipQuestion.contains(goldKey)){
				
				Matcher randomMatcher = randomPattern.matcher(goldKey);
				Matcher multipleMatcher = multiplePattern.matcher(goldKey);
				Matcher exactMatcher = exactPattern.matcher(goldKey);
				
				tmpGoldAnnotation = (Annotation) goldSortedAnnotations.get(i);
				
				if(score == null)
					score = "0";
				
System.out.println(goldKey);    			  
				if(exactMatcher.find()){
					String IDs[] = goldKey.split(EXACT_MATCH_ORDER);
System.out.println("Match exact: " + Arrays.toString(IDs));
					for (int j = 0; j < IDs.length; j++) {
						goldAnswerColumnIDList.add(IDs[j].trim());
					}
					setGoldMaps(goldSortedAnnotations, goldScoreMap, goldAnswerMap,
							goldMap, answer, score, i, IDs);
					scoreMap.put(IDs[0], Integer.valueOf(score.split(EXACT_MATCH_ORDER)[0]));
				}
				else if(multipleMatcher.find()){
					String IDs[] = goldKey.split(MULTIPLE_CORRECT);
//System.out.println("Match multiple: " + Arrays.toString(IDs));  
					for (int j = 0; j < IDs.length; j++) {
						goldAnswerColumnIDList.add(IDs[j].trim());
					}
					setGoldMaps(goldSortedAnnotations, goldScoreMap, goldAnswerMap,
							goldMap, answer, score, i, IDs);
					setScoreMap(scoreMap, IDs, score.split(MULTIPLE_CORRECT));
				}
				else if(randomMatcher.find()){
					String[] IDs = goldKey.split(RANDOM_ORDER);
//System.out.println("Match random:" + Arrays.toString(IDs));
//System.out.println(score + "→" + Arrays.toString(score.split(RANDOM_ORDER)));
					for (int j = 0; j < IDs.length; j++) {
						goldAnswerColumnIDList.add(IDs[j].trim());
					}
					setGoldMaps(goldSortedAnnotations, goldScoreMap, goldAnswerMap,
							goldMap, answer, score, i, IDs);
					setScoreMap(scoreMap, IDs, score.split(MULTIPLE_CORRECT));
					//scoreMap.put(IDs[0], Integer.valueOf(score.split(RANDOM_ORDER)[0]));
				}
				else{
//System.out.println("No matched :" + goldKey);
					goldAnswerColumnIDList.add(goldKey);
					goldAnswerMap.put(goldKey, answer);
					goldScoreMap.put(goldKey, score);
					goldMap.put(goldKey, (Annotation) goldSortedAnnotations.get(i));
					scoreMap.put(goldKey, Integer.valueOf(score));
				}
			}
			
			if(goldName.equals(AnswerColumnID.class.getName())){
				goldKey = goldSortedAnnotations.get(i).getFeatureValueAsString(goldSortedAnnotations.get(i).getType().getFeatureByBaseName("String"));
			}
			if(goldName.equals(Answer.class.getName())){
				answer = goldSortedAnnotations.get(i).getFeatureValueAsString(goldSortedAnnotations.get(i).getType().getFeatureByBaseName("String"));
			}
			if(goldName.equals(Score.class.getName())){
				score = goldSortedAnnotations.get(i).getFeatureValueAsString(goldSortedAnnotations.get(i).getType().getFeatureByBaseName("String"));
				if(score == null || score.trim().length() == 0)
					score = "0";
				else
					score = score.trim();
			}	    	    
		}
		
System.out.println(goldKey + " / " + score);
		if(goldKey != null && !skipQuestion.contains(goldKey)){
			
			Matcher randomMatcher = randomPattern.matcher(goldKey);
			Matcher multipleMatcher = multiplePattern.matcher(goldKey);
			Matcher exactMatcher = exactPattern.matcher(goldKey);
			
System.out.println(exactMatcher.find() + " " + multipleMatcher.find() + " " + randomMatcher.find());
			
			if(exactMatcher.find()){
				String IDs[] = goldKey.split(EXACT_MATCH_ORDER);
				for (int j = 0; j < IDs.length; j++) {
					goldAnswerColumnIDList.add(IDs[j].trim());
				}
				setGoldMaps(goldSortedAnnotations, goldScoreMap, goldAnswerMap,
						goldMap, answer, score, tmp_i, IDs);
				scoreMap.put(IDs[0], Integer.valueOf(score.split(EXACT_MATCH_ORDER)[0]));
			}
			else if(multipleMatcher.find()){
				String IDs[] = goldKey.split(MULTIPLE_CORRECT);
				for (int j = 0; j < IDs.length; j++) {
					goldAnswerColumnIDList.add(IDs[j].trim());
				}
				setGoldMaps(goldSortedAnnotations, goldScoreMap, goldAnswerMap,
						goldMap, answer, score, tmp_i, IDs);
				setScoreMap(scoreMap, IDs, score.split(MULTIPLE_CORRECT));
			}
			else if(randomMatcher.find()){
				String IDs[] = goldKey.split(RANDOM_ORDER);
//System.out.println("Randam Matched: ids=" + Arrays.toString(IDs) + " socres=" + Arrays.toString(score.split(RANDOM_ORDER)));
				for (int j = 0; j < IDs.length; j++) {
					goldAnswerColumnIDList.add(IDs[j].trim());
				}
				setGoldMaps(goldSortedAnnotations, goldScoreMap, goldAnswerMap,
						  goldMap, answer, score, tmp_i, IDs);
				setScoreMap(scoreMap, IDs, score.split(RANDOM_ORDER));
			}
			else{
				goldAnswerMap.put(goldKey, answer);
				goldScoreMap.put(goldKey, score);
				goldAnswerColumnIDList.add(goldKey);
				scoreMap.put(goldKey, Integer.valueOf(score));
				if( tmpGoldAnnotation != null)
					goldMap.put(goldKey, tmpGoldAnnotation);
			}
		}
//		  numOfQuestion = goldAnswerMap.size();
		numOfQuestion = goldAnswerColumnIDList.size();
		
		for (int i = 0; i < testSortedAnnotations.size(); i++){
			testName = testSortedAnnotations.get(i).getType().getName();
			
			if(testName.equals(DataAT.class.getName()) && testKey != null){

				dataType = (Annotation) testSortedAnnotations.get(i);
				setTestMaps(testKey, answer, testAnswerMap, testMap, testAnswerColumnIDList, testAnswerColumIDMap, dataType, ansColumn_IDType);
				
			}
			else if(testName.equals(AnswerColumnID.class.getName())){
				testKey = testSortedAnnotations.get(i).getFeatureValueAsString(testSortedAnnotations.get(i).getType().getFeatureByBaseName("String"));
				ansColumn_IDType = (Annotation) testSortedAnnotations.get(i);
//	    		  System.out.println("test key: " + testKey);
			}
			else if(testName.equals(Answer.class.getName())){
				answer = testSortedAnnotations.get(i).getFeatureValueAsString(testSortedAnnotations.get(i).getType().getFeatureByBaseName("String"));
				
			}
			else if(testName.equals(AnswerMachineType.class.getName()))
				answerMachineType = (AnswerMachineType) testSortedAnnotations.get(i);  
		}
//System.out.println(testKey + ": " + answer);
		setTestMaps(testKey, answer, testAnswerMap, testMap, testAnswerColumnIDList, testAnswerColumIDMap, dataType, ansColumn_IDType);
		  
		TreeSet<String> idSet = new TreeSet<String>();
		for(String id : testAnswerColumnIDList){
			if(!goldAnswerColumnIDList.contains(id))
				idSet.add(id);
		}
		testAnswerColumnIDList.removeAll(idSet);
		  
		if(mode == 1){
			testAnswerColumnIDList = goldAnswerColumnIDList;
			for (int i = 0; i < testAnswerColumnIDList.size(); i++) {
				testKey = testAnswerColumnIDList.get(i);
				if(testAnswerMap.get(testKey) == null || testAnswerMap.get(testKey).length() == 0 || testAnswerMap.get(testKey).equals(" "))
					testAnswerMap.put(testKey, "1");
			}
		}
		else if(mode == 2){
			numOfQuestion = testAnswerColumnIDList.size();
			for (int i = 0; i < testAnswerColumnIDList.size(); i++) {
				String key = testAnswerColumnIDList.get(i);
				if( testAnswerMap.get(key) == null || testAnswerMap.get(key).trim().length() == 0 || testAnswerMap.get(key).equals(" "))
					numOfQuestion--;
			}
		}		  		  
		if(mode != 2)
			perfectScore = calcPerfectScore(scoreMap);
		else
			perfectScore = calcPerfectScore(scoreMap, testAnswerColumnIDList);
		
System.out.println(new TreeSet(scoreMap.keySet()));
System.out.println(scoreMap.size() + "(得点" +  perfectScore +  "): " + scoreMap);
System.out.println(comparisonSet.getGoldAnnotationGroup().getComponentID() +"/"+ comparisonSet.getTestAnnotationGroup().getComponentID());
System.out.println(goldAnswerColumnIDList.size() +":"+goldAnswerColumnIDList);
System.out.println(testAnswerColumnIDList.size() +":"+testAnswerColumnIDList);
System.out.println(goldAnswerMap);
System.out.println(testAnswerMap);
System.out.println(goldScoreMap);
		
		// 採点
		for (int i = 0; i < testAnswerColumnIDList.size(); i++) {
			testKey = testAnswerColumnIDList.get(i);
			
//			  if(testKey != null)
//	    		  System.out.println(testKey + ": " + goldAnswerMap.get(testKey) +"/"+ testAnswerMap.get(testKey));
			
			if(testKey != null && testAnswerMap.get(testKey) != null && goldAnswerMap.get(testKey) != null){
				String goldAnswer = goldAnswerMap.get(testKey);
				String testAnswer = testAnswerMap.get(testKey);
				Matcher randomMatcher = randomPattern.matcher(goldAnswer);
				Matcher multipleMatcher = multiplePattern.matcher(goldAnswer);
				Matcher exactMatcher = exactPattern.matcher(goldAnswer);
				
//System.out.println(i + ", " + testKey + ": " + testAnswer + "/" + goldAnswer);
				
				//一つの解答欄に対して、正答の可能性が複数ある場合の採点
				if(multipleMatcher.find()){
//System.out.println("Match Multiple");
//System.out.println("testKey, testAnswer: " + testKey + ", " + testAnswer);
//System.out.println("goldAnswer: " + goldAnswer);
					ArrayList<String> answers = new ArrayList<String>(Arrays.asList(goldAnswer.split(MULTIPLE_CORRECT)));
//System.out.println("answers: " + answers);
//System.out.println("i: "+i);
					while(testAnswer.split(EXACT_MATCH_ORDER).length < answers.size()){
						testAnswer = testAnswer + "," + testAnswerMap.get(testAnswerColumnIDList.get(++i));
						System.out.println("i: "+i);
					}
//System.out.println("testAnswer: " + testAnswer);
					for(String ans : answers){
//System.out.println("ans: " + ans);
						if(testAnswer.equals(ans)){
							MatchedPair matchedPair = new MatchedPair(jcas);
							matchedPair.addToIndexes();
							matchedPair.setGoldAnnotation(goldMap.get(testKey));
							matchedPair.setTestAnnotation(testMap.get(testKey));
							matchedPairs.add(matchedPair);
							if(goldScoreMap.get(testKey) != null)
								totalScore += Integer.valueOf(goldScoreMap.get(testKey));
//System.out.println(totalScore + "(+" + goldScoreMap.get(testKey) + ")");
							AnswerColumnID answerColumnID = (AnswerColumnID) testAnswerColumIDMap.get(testKey);
							if(answerColumnID != null){
								answerColumnID.setTF(true);
							}
							correctAnswer += ans.split(EXACT_MATCH_ORDER).length;
							break;
						}
					}
				}
				
				// 複数の解答欄にすべて正解を入れないと点数がつかない場合の採点
				else if(exactMatcher.find()){
//System.out.println("Match exact");
//System.out.print(testKey + " ");
					String answers[] = goldAnswer.split(EXACT_MATCH_ORDER);
					boolean check = true;
					for(int j=0; j<answers.length; j++){
						if(testAnswer.equals(answers[j])){
							AnswerColumnID answerColumnID = null;
							if(i < testAnswerColumnIDList.size())
								answerColumnID = (AnswerColumnID) testAnswerColumIDMap.get(testAnswerColumnIDList.get(i));
							if(answerColumnID != null){
								answerColumnID.setTF(true);
							}
							correctAnswer++;
							i++;
							if(i < testAnswerColumnIDList.size())
								testAnswer = testAnswerMap.get(testAnswerColumnIDList.get(i));
						}
						else{
							i++;
							if(i < testAnswerColumnIDList.size())
								testAnswer = testAnswerMap.get(testAnswerColumnIDList.get(i));
							check = false;
						}
					}
					if(check){
						MatchedPair matchedPair = new MatchedPair(jcas);
						matchedPair.addToIndexes();
						matchedPair.setGoldAnnotation(goldMap.get(testKey));
						matchedPair.setTestAnnotation(testMap.get(testKey));
						matchedPairs.add(matchedPair);
						if(goldScoreMap.get(testKey) != null)
							totalScore += Integer.valueOf(goldScoreMap.get(testKey));
//System.out.println(totalScore + "(+" + goldScoreMap.get(testKey) + ")");
						AnswerColumnID answerColumnID = (AnswerColumnID) testAnswerColumIDMap.get(testKey);
						if(answerColumnID != null){
							answerColumnID.setTF(true);
						}
					}
					i--;
				}
				
				
				// 答えが順不同の場合の採点
				else if(randomMatcher.find()){
//System.out.println("Match Random");
					String answers[] = goldAnswer.split(RANDOM_ORDER);
					String scores[] = goldScoreMap.get(testKey).split(RANDOM_ORDER);
					HashMap<String, Boolean> usedMap = new HashMap<String, Boolean>();
					for(int j=0; j<answers.length; j++){
						usedMap.put(answers[j], false);
					}
					for(int j=0; j<answers.length; j++){
//System.out.print(testKey + " ");
						for(int k=0; k<answers.length; k++){
							if(testAnswer.equals(answers[k]) && !usedMap.get(answers[k])){
								MatchedPair matchedPair = new MatchedPair(jcas);
								matchedPair.addToIndexes();
								matchedPair.setGoldAnnotation(goldMap.get(testKey));
								matchedPair.setTestAnnotation(testMap.get(testKey));
								matchedPairs.add(matchedPair);
								if(k < scores.length && scores[k] != null)
									totalScore += Integer.valueOf(scores[k]);
								else if(scores[0] != null)
									totalScore += Integer.valueOf(scores[0]);
//System.out.println(totalScore + "(+" + goldScoreMap.get(testKey) + ")");
								AnswerColumnID answerColumnID = (AnswerColumnID) testAnswerColumIDMap.get(testKey);
								if(answerColumnID != null){
									answerColumnID.setTF(true);
								}
								correctAnswer++;
								usedMap.remove(answers[k]);
								usedMap.put(answers[k], true);
								break;
							} 
						}
						i++;
						if(i < testAnswerColumnIDList.size())
							testAnswer = testAnswerMap.get(testAnswerColumnIDList.get(i));
					}
					i--;	  
				}
				
				else if(testAnswer.equals(goldAnswer)){
//System.out.println("No Matched");
//System.out.println(testKey);
					MatchedPair matchedPair = new MatchedPair(jcas);
					matchedPair.addToIndexes();
					matchedPair.setGoldAnnotation(goldMap.get(testKey));
					matchedPair.setTestAnnotation(testMap.get(testKey));
					matchedPairs.add(matchedPair);
					if(goldScoreMap.get(testKey) != null && goldScoreMap.get(testKey) != "")
						totalScore += Integer.valueOf(goldScoreMap.get(testKey));
//System.out.println(totalScore + "(+" + goldScoreMap.get(testKey) + ")");
					AnswerColumnID answerColumnID = (AnswerColumnID) testAnswerColumIDMap.get(testKey);
					if(answerColumnID != null)
						answerColumnID.setTF(true);
					correctAnswer++;
				}
			}
//	    	  System.out.println("correctAnswer:" + correctAnswer);
		}

		FSArray array = new FSArray(jcas, matchedPairs.size());
		array.addToIndexes();
		
		for (int i = 0; i < matchedPairs.size(); i++) {
			MatchedPair pair = matchedPairs.get(i);
			array.set(i, pair);
		}
		
		comparisonSet.setMatchedAnnotations(array);
		
if (answerMachineType == null)
System.out.println("answerMachineType is null");
//		if(answerMachineType != null && comparisonSet.getGoldAnnotationGroup().getComponentID().equals("Exam")){
		if(answerMachineType != null && comparisonSet.getGoldAnnotationGroup().getComponentID().contains("ExamReader")){
			
			answerMachineType.setComponentID(componentID);
			
			answerMachineType.setScore(String.valueOf(totalScore));
			double rate = (double)correctAnswer / numOfQuestion;
			if(numOfQuestion <= 0)
				answerMachineType.setRate("0.0000");
			else{
				BigDecimal bd = new BigDecimal(rate);
				answerMachineType.setRate(String.valueOf(bd.setScale(4, RoundingMode.HALF_UP)));
			}
			answerMachineType.setBegin(0);
			answerMachineType.setEnd(0);
			answerMachineType.setNumOfCorrect(String.valueOf(correctAnswer));
			answerMachineType.setNumOfQuestion(String.valueOf(numOfQuestion));
			answerMachineType.setPerfectScore(String.valueOf(perfectScore));
		}
	}

	
	private void setTestMaps(String key, String answer,
			HashMap<String, String> answerMap,
			HashMap<String, Annotation> testMap, ArrayList<String> testAnswerColumnIDList, HashMap<String, Annotation> testAnswerColumIDMap,
			Annotation dataType, Annotation anscolum_idType) {
		
		if(key == null) return;
		
		String keys[];
		String answers[];
		Pattern exactPattern = Pattern.compile(EXACT_MATCH_ORDER);
		Pattern multiplePattern = Pattern.compile(MULTIPLE_CORRECT);
		Pattern randomPattern = Pattern.compile(RANDOM_ORDER);
		
		Matcher exactMatcher = exactPattern.matcher(key);
		Matcher multipleMatcher = multiplePattern.matcher(key);
		Matcher randomMatcher =randomPattern.matcher(key);
		
		if(exactMatcher.find()){
			keys = key.split(EXACT_MATCH_ORDER);
			answers = answer.split(EXACT_MATCH_ORDER);
		}
		else if(multipleMatcher.find()){
			keys = key.split(MULTIPLE_CORRECT);
			answers = answer.split(MULTIPLE_CORRECT);
		}
		else if(randomMatcher.find()){
			keys = key.split(RANDOM_ORDER);
			answers = answer.split(RANDOM_ORDER);
		}
		else{
			keys = null;
			answers = null;
		}
		if(keys == null || answers == null){
			answerMap.put(key.trim(), answer.trim());
			testAnswerColumnIDList.add(key.trim());
			testMap.put(key.trim(), dataType);
			testAnswerColumIDMap.put(key.trim(), anscolum_idType);
		}
		else{
			for (int i = 0; i < keys.length; i++) {
				if(i < answers.length){
					answerMap.put(keys[i].trim(), answers[i].trim());
					testAnswerColumnIDList.add(keys[i].trim());
				}
				else{
					answerMap.put(keys[i].trim(), answers[0].trim());
					testAnswerColumnIDList.add(keys[i].trim());
				}
				testMap.put(keys[i].trim(), dataType);
				testAnswerColumIDMap.put(keys[i].trim(), anscolum_idType);
			}
		}
	}

	private void setGoldMaps(SortedAnnotations goldSortedAnnotations,
			HashMap<String, String> goldScoreMap,
			HashMap<String, String> goldAnswerMap,
			HashMap<String, Annotation> goldMap, String answer, String score,
			int i, String[] IDs) {
		for(int j=0; j<IDs.length; j++){
//			  numOfQuestion++;
			goldAnswerMap.put(IDs[j], answer);
			goldScoreMap.put(IDs[j], score);
			goldMap.put(IDs[j], (Annotation) goldSortedAnnotations.get(i));
		}
	}
	
	private void setScoreMap(HashMap<String, Integer>scoreMap, String[] ids, String[] scores){
		for (int i = 0; i < ids.length; i++) {
			if(i < scores.length){
				scoreMap.put(ids[i], Integer.valueOf(scores[i]));
			}
			else{
				scoreMap.put(ids[i], Integer.valueOf(scores[0]));
			}
		}
	}
	
	private String calcPerfectScore(HashMap<String, Integer> scoreMap){
		return calcPerfectScore(scoreMap, null);
	}
	
	private String calcPerfectScore(HashMap<String, Integer> scoreMap, ArrayList<String> answerColumnIDList){
		int perfectScore = 0;
		if(answerColumnIDList == null){
			Iterator<Integer> i = scoreMap.values().iterator();
			while(i.hasNext()){
				perfectScore += i.next();
			}
		}
		else{
			for (int i = 0; i < answerColumnIDList.size(); i++) {
				String key = answerColumnIDList.get(i);
				if(scoreMap.containsKey(key) && scoreMap.get(key) != null)
					perfectScore += scoreMap.get(key);
			}
		}
		return String.valueOf(perfectScore);
	}

/*	
	private SortedAnnotations getSortedAnnotations(AnnotationGroup goldGroup,
			Type typeToBeCompared,
			HashMap<AnnotationGroup, SortedAnnotations> groupToSortedMap,
			TypeSystem typeSystem) {
		SortedAnnotations sortedAnnotations;
		sortedAnnotations = groupToSortedMap.get(goldGroup);
		if (sortedAnnotations == null) {
			sortedAnnotations = new SortedAnnotations(goldGroup,
					typeToBeCompared, typeSystem);
			groupToSortedMap.put(goldGroup, sortedAnnotations);
		}
		return sortedAnnotations;
	}
*/	
	
	//----------------------------------------------------------------------------//
	//--- Inner Class --//
	public static class ComparisionScoreResultRPF_Ehr implements ComparisonScoreResult {

		private static final long serialVersionUID = 6225283981298221450L;
		private int labeledMatch;
		private String htmlString;
		private int numberOfQuestion = 0;
		private double totalRate;
		private int totalScore = 0;
		private final static String[] tmpLabels = {//input data
				  									//output data
												"RATE","SCORE","NOC", "NOQ" };
		
		public ComparisionScoreResultRPF_Ehr(ComparisonSet c) {
			super();
			
			FSArray goldAnnotations = c.getGoldAnnotations();
			FSArray testAnnotations = c.getTestAnnotations();
			FSArray matchedAnnotations = c.getMatchedAnnotations();
      	
			labeledMatch = matchedAnnotations.size() + c.getMatchCountAdjustment();
			
			List<Annotation> goldAnn = new ArrayList<Annotation>();
			List<Annotation> testAnn = new ArrayList<Annotation>();
			
			for (int i = 0; i < goldAnnotations.size(); i++) {	
				goldAnn.add((Annotation) goldAnnotations.get(i));
			}
			for (int i = 0; i < testAnnotations.size(); i++) {
				testAnn.add((Annotation) testAnnotations.get(i));
			}
			
			numberOfQuestion = calcNumOfQuestion(goldAnn);	
			totalScore = calcTotalScore(testAnn);
		}
		
		public ComparisionScoreResultRPF_Ehr(int labeledMatch, int totalNumberOfQuestion, double totalRate) {
			
			this.labeledMatch = labeledMatch;
			this.numberOfQuestion = totalNumberOfQuestion;
			this.totalRate = totalRate;
		}
		
		@Override
		public ArrayList<String> getLabels() {

			ArrayList<String> labels = new ArrayList<String>();
			
			for(String s : tmpLabels){
				if(labels.indexOf(s) == -1)
					labels.add(s);
				else
					System.err.println("Make sure label is unique.  [" + s + "]");
			}
			return labels;
		}
		
		public ArrayList<String> getDisplayLabels(){

			ArrayList<String> labels = new ArrayList<String>();
			
			for(String s : tmpLabels){
				if(labels.indexOf(s) == -1)
					labels.add(s);
				else
					System.err.println("Make sure label is unique.  [" + s + "]");
			}
			return labels;
		}
		
		public ArrayList<String> getTotalDisplayLabels(){
			
			ArrayList<String> labels = new ArrayList<String>();
			
			for(String s : tmpLabels){
				if(labels.indexOf(s) == -1)
					labels.add(s);
				else
					System.err.println("Make sure label is unique.  [" + s + "]");
			}
			return labels;
		}
		
		@Override
		public ArrayList<String> getTotalLabels() {

			ArrayList<String> labels = new ArrayList<String>();
			
			for(String s : tmpLabels){
				if(labels.indexOf(s) == -1)
					labels.add(s);
				else
					System.err.println("Make sure label is unique.  [" + s + "]");
			}
			return labels;
		}
		
		public Class getLabelClass(String label){
			
			if(label == "NOQ")
				return Integer.class;
			else if(label == "NOC")
				return Integer.class;
			else if(label == "RATE")
				return Double.class;
			else if(label == "SCORE")
				return Integer.class;
			else
				System.err.println("NO SUCH LABEL - [" + label + "]");
						
			return null;
		}

		@Override
		public String getScore(String label) {
			
			if(label == "NOQ")
				return String.valueOf(numberOfQuestion);
			else if(label == "NOC")
				return String.valueOf(this.labeledMatch);
			else if(label == "RATE")
				return String.valueOf(calcRate(this.labeledMatch, numberOfQuestion));
			else if(label == "SCORE")
				return String.valueOf(totalScore);
			else
				System.err.println("NO SUCH LABEL - [" + label + "]");
			
			return null;
		}
	
		
		public String getHTMLString() {
			if (htmlString == null) {
				int correct = this.labeledMatch;
				int question = numberOfQuestion;
				htmlString = new String(correct + "/" + question);			
			}
			return htmlString;
		}
		
		@Override
		public String getTotalHTMLString() {
			if (htmlString == null) {

				int correct = this.labeledMatch;
				int question = numberOfQuestion;
				
				htmlString = new String(correct + "/" + question);			
			}
			return htmlString;
		}

		//private methods -------------------------------------------------- 
		
		private int calcNumOfQuestion(List<Annotation> goldAnnotations){
			
			int count = 0;
				
			if(goldAnnotations.isEmpty()){
				return count;
			}
			for (int i = 0; i < goldAnnotations.size(); i++) {
				if(goldAnnotations.get(i).getType().toString().equals(AnswerColumn.class.getName()))
					count++;
			}
			return count;
		}
		
		private double calcRate(int labeledMatch,int numOfQuestion) {
			return (double)labeledMatch / (double)numOfQuestion * 100;
		}
		
		private int calcTotalScore(List<Annotation> annotations) {
			
			int score = 0;
			
			for (int i = 0; i < annotations.size(); i++) {
				if(annotations.get(i).getType().toString().equals(AnswerMachineType.class.getName()))
					score = Integer.valueOf(((AnswerMachineType) annotations.get(i)).getScore());
			}
			return score;
		}
		
		public double getTotalRate() {
			return totalRate;
		}
	}
	
	
	public static class ComparisionScoreResultTotal_Ehr implements TotalResultCalculator {
		
		public ComparisionScoreResultTotal_Ehr(){
			
		}
		
		@Override
		public ComparisonScoreResult calculate(Map<URL, ComparisonScoreResult> map) {
			
			int labeled = 0, question = 0;
			double rate = 0.0;
            Iterator<ComparisonScoreResult> iterator = map.values().iterator();
            while (iterator.hasNext()) {
              ComparisonScoreResult result = iterator.next();
              
              question += Integer.parseInt(result.getScore("NOQ"));
              labeled += Integer.parseInt(result.getScore("NOC"));
              rate += Double.parseDouble(result.getScore("RATE"));
            }
            ComparisionScoreResultRPF_Ehr totalResult = new ComparisionScoreResultRPF_Ehr(labeled,question,rate);
            
            return totalResult;
		}
	}
	
	
	/**
	 * KachakoではComparisonSetはコンポーネント内で作成することになったので作成処理を追加
	 * TODO: リターン値がnullになるのでAnnotationGroupClusterは入っていない？
	 * @param jcas	JCas
	 * @return		AnnotationGroupCluster の数
	 */
	public ComparisonSet loopAnnotationGroupCluster(JCas jcas) {

		try {
			Type type = jcas.getTypeSystem().getType(AnnotationGroupCluster.class.getName());
			FSIterator<TOP> iterator = jcas.getJFSIndexRepository().getAllIndexedFS(type);
			if (!iterator.hasNext()) {
System.out.println("no AnnotationGroupCluster");				
				return null;
			}
			AnnotationGroupCluster cluster = (AnnotationGroupCluster)iterator.next();
			FSArray annotationGroups = cluster.getAnnotationGroups();
			return comparisonSetFunction(annotationGroups, jcas);
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * annotationGroups から、 ComparisonSet を作成し、
	 * goldAnnotationGroup と testAnnotagionGroup を設定して、
	 *	をコールする。
	 */
	public ComparisonSet comparisonSetFunction(FSArray annotationGroups, JCas jcas) {

		AnnotationGroup gold = null;
		AnnotationGroup test = null;
		
		if (annotationGroups == null || annotationGroups.size() < 2) {
			return null;
		}
System.out.println("<<find goldAnnotationGroup>>");
		gold = findAnnotationGroup(annotationGroups, 0);
System.out.println("gold.getComponentID()="+gold.getComponentID());
System.out.println("gold.getAnnotations().size()="+gold.getAnnotations().size());
		
		
System.out.println("<<find testdAnnotationGroup>>");
		test = findAnnotationGroup(annotationGroups, 1);
System.out.println("test.getComponentID()="+test.getComponentID());
System.out.println("test.getAnnotations().size()="+test.getAnnotations().size());

		if (gold.getCollectionReaderGenerated()) {
			System.out.println("gold is CollectionReaderGenerated");
		}
		
		StringArray typesStringArray = new StringArray(jcas, 1);
		Type type = gold.getType();
		typesStringArray.set(0, type.getName());
		ComparisonSet cs = new ComparisonSet(jcas);

		cs.addToIndexes();
		cs.setGoldAnnotationGroup(gold);
		cs.setTestAnnotationGroup(test);
		cs.setTypes(typesStringArray);
		cs.setSuperTypes(typesStringArray);
		cs.setComponentID(test.getComponentID());

		List<Type> typesList = new ArrayList<Type>();
		typesList.add(type);
		List <SimpleEntry<AnnotationGroup, List<Type>>> groupsList = new ArrayList<SimpleEntry<AnnotationGroup, List<Type>>>();
		groupsList.add(new SimpleEntry<AnnotationGroup, List<Type>>(gold, typesList));
		groupsList.add(new SimpleEntry<AnnotationGroup, List<Type>>(test, typesList));
		createDerivingAnnotationGroups(cs, groupsList, jcas);

		return cs;
	}

	
	
	private AnnotationGroup findAnnotationGroup(FSArray annotationGroups, int pos) {

		AnnotationGroup ag = null;
		int cnt = 0;
		for (int i = 0; i < annotationGroups.size(); i++) {
			ag = (AnnotationGroup)annotationGroups.get(i);
			StringArray types = ag.getTypes();
			for (int j = 0; j < types.size(); j++) {
				String typeName = types.get(j);
System.out.println("annotationGroup["+i+"] type["+j+"]:"+typeName);
				if (typeName.equals(AnswerTableTag.class.getName())) {
					if (cnt == pos)
						return ag;
					cnt++;
				}
			}
		}
		return ag;
	}
	
	
	
	private void createDerivingAnnotationGroups(ComparisonSet annotationGroup, List<SimpleEntry<AnnotationGroup, List<Type>>> groups, JCas originalCas) {
		List<TOP> list = new ArrayList<TOP>();
		
		FSArray fsArray = new FSArray(originalCas, groups.size());
fsArray.addToIndexes(originalCas);
		annotationGroup.setDerivingAnnotationGroupTypes(fsArray);
System.out.println("groups.size()="+groups.size());
		for (int i = 0; i < groups.size(); i++) {
			List<Type> types = groups.get(i).getValue();
			StringArray typesArray = new StringArray(originalCas, types.size());
typesArray.addToIndexes(originalCas);
			fsArray.set(i, typesArray);

System.out.println("types.size()="+types.size());
			for (int j = 0; j < types.size(); j++) {
				Type type = types.get(j);
				typesArray.set(j, new String(type.getName()));
System.out.println("types="+type.getName());
			}
			AnnotationGroup group = groups.get(i).getKey();
			list.add(group);
		}
		FSArray	fsarray = ComparableUtils.setList2FSArray(originalCas, list);
fsarray.addToIndexes(originalCas);
		
	}
	
}
