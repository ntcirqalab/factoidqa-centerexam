/**
 *
 */
package org.kachako.components.centerexam.evaluation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.TOP;
import org.apache.uima.jcas.tcas.Annotation;
import org.kachako.types.comparable.AnnotationGroup;


/**
 * Annnotation をソートする。
 *
 * @author tkoba
 *
 */
public class SortedAnnotations {
	/**
	 * ソートする変数
	 */
	private ArrayList<TOP> sortedAnnotations = new ArrayList<TOP>();

	/**
	 * sortedAnnotations を文字列に変換して返す。
	 *
	 */
	@Override
	public String toString() {
		return sortedAnnotations.toString();
	}

	/**
	 *
	 * fsArray を sortedAnnotations にセットする。
	 * タイプが同じものでなければセットしない。
	 * Annotation用の Comparator をコールする。
	 *
	 * @param fsArray				ソートする、Annotation
	 * @param typeToBeCompared		タイプ
	 * @param typeSystem			typeToBeCompared の子クラス
	 */
	public SortedAnnotations(FSArray fsArray, Type typeToBeCompared, TypeSystem typeSystem )
	{
		for (int i = 0; i < fsArray.size(); i++) {
			TOP top = (TOP)fsArray.get(i);

			if ( top == null ) continue;

			/*
			 *  getType() が typeToBeCompared か typeBeCompared の子クラスかどうか
			 *
			 */
			if ( top.getType().equals(typeToBeCompared) || typeSystem.subsumes(typeToBeCompared, top.getType() ) )
			{
				sortedAnnotations.add(top);
			}
		}
		Collections.sort(sortedAnnotations, annotationComparator);
	}

	/**
	 *
	 * group を sortedAnnotations にセットする。
	 * タイプが同じものでなければセットしない。
	 * Annotation用の Comparator をコールする。
	 *
	 * @param group
	 * @param typeToBeCompared
	 * @param typeSystem
	 */
	public SortedAnnotations(AnnotationGroup group, Type typeToBeCompared, TypeSystem typeSystem ) {

		FSArray fa = group.getAnnotations();
		for (int i = 0; i < fa.size(); i++) {
			TOP top = (TOP) fa.get(i);
			if ( top == null ) {
				continue;
			}

			if ( top.getType().equals(typeToBeCompared) || typeSystem.subsumes(typeToBeCompared, top.getType() ) ) {
				sortedAnnotations.add(top);
			}
		}

		Collections.sort(sortedAnnotations, annotationComparator);
	}

	/**
	 * sortedAnnotations に格納されている index 番目を返す。
	 *
	 * @param index
	 * @return
	 */
	public TOP get(int index) {
		return this.sortedAnnotations.get(index);
	}

	/**
	 *
	 * sortedAnnotaions に引数の e をセットする。
	 *
	 * @param e		TOP
	 * @return		true 成功、false 失敗
	 */
	public boolean add(TOP e) {
		return this.sortedAnnotations.add(e);
	}

	/**
	 * sortedAnnotations内に o が存在するかを返す。
	 *
	 * @param o	指定 Object
	 * @return	true 存在する。false 存在しない。
	 */
	public boolean contains(Object o) {
		return this.sortedAnnotations.contains(o);
	}

	/**
	 *
	 * sortedAnnotations が空かどうかを返す。
	 *
	 * @return	true 空。false 空ではない。
	 */
	public boolean isEmpty() {
		return this.sortedAnnotations.isEmpty();
	}

	/**
	 *
	 * sortedAnnotations から、TOPの Iterator を返す。
	 *
	 * @return		Iterator<TOP> を返す。
	 */
	public Iterator<TOP> iterator() {
		return this.sortedAnnotations.iterator();
	}

	/**
	 *
	 * 指定された引数と同じ内容の要素を先頭から検索する。
	 *
	 * @param o	検索 Object
	 * @return	要素のインデックス。 要素が含まれない場合 -1 を返す。
	 */
	public int indexOf(Object o) {
		return this.sortedAnnotations.indexOf(o);
	}

	/**
	 *
	 * sortedAnnotations のサイズを返す。
	 *
	 * @return		sortedAnnotations.size()
	 */
	public int size() {
		return this.sortedAnnotations.size();
	}

	/**
	 *
	 * 比較関数
	 *
	 */
	public static Comparator<TOP> annotationComparator = new Comparator<TOP>() {
		public int compare(TOP o1, TOP o2) {
			synchronized (this) {

				if ( (o1 instanceof Annotation) && (o2 instanceof Annotation) ) {
					Annotation a1 = (Annotation) o1;
					Annotation a2 = (Annotation) o2;

					int begin1 = a1.getBegin();
					int begin2 = a2.getBegin();
					int end1 = a1.getEnd();
					int end2 = a2.getEnd();

					Type type1 = a1.getType();
					Type type2 = a2.getType();

					if ( begin1 != begin2 ) return begin1 - begin2;
					else if ( end1 != end2 ) return - end1 + end2;
					else if ( type1 == null || type2 == null ) {
						System.out.println("Illegal phenomena: type is null. " + Thread.currentThread().getStackTrace()[1]);
						return -1;
					} else if ( type1 != type2 )
						return type1.getShortName().compareTo(type2.getShortName());
					else
						return a1.getAddress() - a2.getAddress(); // really?
				} else if ( ! (o1 instanceof Annotation) ) {
					return -1;
				} else if ( ! (o2 instanceof Annotation) ) {
					return 1;
				} else {
					return o1.getAddress() - o2.getAddress(); // really???
				}
			}
		}
	};
}

