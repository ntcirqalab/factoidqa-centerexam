package org.kachako.components.centerexam.answer_machine;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Random;
import java.util.TreeSet;
import java.util.regex.Pattern;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.impl.DefaultAnnotationComparator;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.kachako.components.centerexam.answertable.AnswerTableReader;
import org.kachako.types.centerexam.Exam;
import org.kachako.types.centerexam.Mi;
import org.kachako.types.centerexam.Question;
import org.kachako.types.centerexam.TagList;

import com.ibm.icu.text.Transliterator;

public class RandomAnswerForMath extends JCasAnnotator_ImplBase {
	
	final static String PARAM_DEFANS = "DefaultAnswer";
	static String DEFAULT_ANSWER;
	AnswerMachine am;
	
	@Override
	public void initialize(UimaContext aContext) throws ResourceInitializationException {
		// TODO Auto-generated method stub
		super.initialize(aContext);
		DEFAULT_ANSWER = ((String)aContext.getConfigParameterValue(PARAM_DEFANS));
		if(DEFAULT_ANSWER == null || DEFAULT_ANSWER.trim().length() == 0)
			am = new AnswerMachine();
		else
			am = new AnswerMachine(DEFAULT_ANSWER.trim());
	}
	
	
	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		// TODO Auto-generated method stub
		
		File answerTableFile = createRandomAnswerXMLFile(jcas);
		AnswerTableReader.readAnswerTable(jcas, answerTableFile);
			
		
	}


	public File createRandomAnswerXMLFile(JCas jcas) {
		
		String fileName = null;

		ArrayList<Annotation> fsAnnotationList = new ArrayList<Annotation>();
		ArrayList<Type> fsTypeList = new ArrayList<Type>(); 
		 
	    TypeSystem typeSystem = jcas.getTypeSystem();
	    
	    Type type = typeSystem.getType(SourceDocumentInformation.class.getName());
	    FSIterator<FeatureStructure> fsIterator = jcas.getFSIndexRepository().getAllIndexedFS(type);
	    if(fsIterator.hasNext()){
	    	SourceDocumentInformation srcDocInfo = (SourceDocumentInformation) fsIterator.next();
	    	try {
				String name = new File(new URI(srcDocInfo.getUri()).getPath()).getName();
				fileName = name.substring(0, name.indexOf(".xml"));
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
			}
	    }
	    
	    type = typeSystem.getType(TagList.class.getName());
	    fsIterator = jcas.getFSIndexRepository().getAllIndexedFS(type);
		TagList tagList = (TagList)fsIterator.next();
		FSArray fsArray = tagList.getAnnotationList();
		TreeSet<String> skipQuestion = new TreeSet<String>();
        StringBuilder selected = new StringBuilder();
			
		//get  Begin,End
		for(int i=0;i<fsArray.size();i++){
			if(fsArray.get(i).getType().getName().equals(Exam.class.getName())){
				Annotation ann = (Annotation) fsArray.get(i);
				String range = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("range_of_options"));
        		String num = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("num_of_options"));
        		if(range != null && num != null){
        			try {
						String ids[] = range.split(" ");
						int n = Integer.valueOf(num);
						for (int j = 0; j < ids.length; j++) {
							if(j < n)
								selected.append(ids[j] + " ");
							else
								skipQuestion.add(ids[j]);
						}
					} catch (Exception e) {
						
					}
        		}
//        		System.out.println(range);
//        		System.out.println(num);
//        		System.out.println(selected);
//        		System.out.println(skipQuestion);
			}
			else if( fsArray.get(i).getType().toString().equals(Question.class.getName()) ){
				fsTypeList.add(fsArray.get(i).getType());
				fsAnnotationList.add((Annotation) fsArray.get(i));
			}
        }
		
	    XmlObject newXml = XmlObject.Factory.newInstance();
        XmlCursor cursor = newXml.newCursor();
        
        cursor.push();
        cursor.toNextToken();
        
        int index = 0;
        int minimalIndex = 0;
        String qestionID = "";
        String ansStyle = null;
        String ansType = null;
        String knowledge = null;
        String mini = null;
        String checkId = null;
            
        //Insert XML tags
		cursor.beginElement("answerTable");
		if(selected.toString().length() > 0)
			cursor.insertAttributeWithValue("selected", selected.toString().trim());
		if(fileName != null)
			cursor.insertAttributeWithValue("filename", fileName);
		
        while(index < fsTypeList.size()){
        	
        	Type fsType = fsTypeList.get(index);
        	
        	if(fsType.toString().equals(Question.class.getName())){
        		
        		Question question = (Question)fsAnnotationList.get(index);
        		mini = fsAnnotationList.get(index).getFeatureValueAsString(fsTypeList.get(index).getFeatureByBaseName("minimal"));
        	
        		if(mini == null || mini.equals("no")){
        			minimalIndex=0;
        			checkId = fsAnnotationList.get(index).getFeatureValueAsString(fsTypeList.get(index).getFeatureByBaseName("id"));
            	}
        		else if(mini.equals("yes")){
        			minimalIndex++;
        			qestionID = fsAnnotationList.get(index).getFeatureValueAsString(fsTypeList.get(index).getFeatureByBaseName("id"));
        		}
        		ansStyle = question.getAnswer_style();
        		ansType = question.getAnswer_type();
        		knowledge = question.getKnowledge_type();
        		String anscolumn_ids = fsAnnotationList.get(index).getFeatureValueAsString(fsAnnotationList.get(index).getType().getFeatureByBaseName("anscolumn_ids"));
        	
//System.out.println(ansColumn);

        		if(anscolumn_ids != null && !skipQuestion.contains(checkId)){
        			String[] ids = anscolumn_ids.split(",");
        			
        			for (int i = 0; i < ids.length; i++) {
        				
        				String ansColumn = ids[i];
        				
        				cursor.beginElement("data");
        		
        				cursor.beginElement("section");
        				cursor.insertChars("第" + ansColumn.charAt(0) + "問");
        				cursor.toNextToken();
        		
        				cursor.beginElement("question");
        				cursor.insertChars(String.valueOf(minimalIndex));
        				cursor.toNextToken();
        		
        				cursor.beginElement("answer_column");
        				cursor.insertChars(String.valueOf(ansColumn.charAt(1)));
        				cursor.toNextToken();
        		
        				cursor.beginElement("answer");
        				cursor.insertAttributeWithValue("system", "no");
        				cursor.insertChars(am.gen());
        				cursor.toNextToken();
        		
//        				cursor.beginElement("score");
//        				cursor.insertChars("");
//        				cursor.toNextToken();
        		
        				cursor.beginElement("answer_style");
        				if(ansStyle != null){
        					cursor.insertChars(ansStyle);
        					ansStyle = null;
        				}
        				else
        					cursor.insertChars(" ");
        					cursor.toNextToken();
        		
        				cursor.beginElement("answer_type");
        					if(ansType != null){
        						cursor.insertChars(ansType);
        						ansType = null;
        					}
        					else
        						cursor.insertChars(" ");
        					cursor.toNextToken();
        		
        					cursor.beginElement("knowledge_type");
        					if(knowledge != null){
        						cursor.insertChars(knowledge);
        						knowledge = null;
        					}
        					else
        						cursor.insertChars(" ");
        					cursor.toNextToken();
        		
        					cursor.beginElement("question_ID");
        					cursor.insertChars(qestionID);
        					cursor.toNextToken();
        		
        					cursor.beginElement("anscolumn_ID");
        					cursor.insertChars(ansColumn);
        					cursor.toNextToken();
        		
        					cursor.beginElement("process_log");
        					cursor.insertChars(" ");
        					cursor.toNextToken();
        		
        					cursor.toNextToken();
        			}
        		}
        	}
        	index++;
        }

        //save XML file
       try {
    	   File tmpFile = File.createTempFile("RAT", ".xml");
    	   
    	   cursor.pop();
    	   cursor.save(tmpFile);
    	   cursor.dispose();
//System.out.println(tmpFile);
    	   return tmpFile;
    	   
    	   
       } catch (IOException e) {
    	   // TODO Auto-generated catch block
    	   e.printStackTrace();
		}
        
       return null;
       
	}

	// inner Class
	public class AnswerMachineException extends Exception{
		public String errMsg;
		public AnswerMachineException(String msg) {
			this.errMsg = msg;
		}
	}
	
	private class AnswerMachine extends RandomAnswerMachine{
		
		private Random random = new Random();
		private int range = 10;
		private final int lower = 0;
		private int mode = 0; // 0:記号なし 1:記号(+,-,±)あり
		
		AnswerMachine(){
			super();
			random = new Random();
		}
		
		AnswerMachine(String defaultAnswer){
			super(defaultAnswer);
			random = new Random();
		}
		
		AnswerMachine(int mode){
			setDefaultAnswer(null);
			setMode(mode);
			setRange();
			random = new Random();
		}
		
		@Override
		String gen(int lower, int range){
			if(getDefualtAnswer() != null)
				return getDefualtAnswer();
			String answer = genAnswer(lower, range);
			switch(mode){
				case 0:
					while(getPreAnswer().equals(answer)){
						answer = genAnswer(lower, range);
					}
					setPreAnswer(answer);
					return genAnswer(lower, range);
				case 1:
					while(getPreAnswer().equals(answer)){
						answer = genAnswer(lower, range);
					}
					setPreAnswer(answer);
					int n = Integer.valueOf(answer);
					switch (n) {
						case 10:
							return "+";
						case 11:
							return "-";
						case 12:
							return "±";
						default:
							return String.valueOf(n);
					}
				default:
					setPreAnswer(answer);
					return answer;
				
			}
		}
		
		String gen(){
			return gen(this.lower, this.range);
		}
		
		String gen(int range){
			return gen(this.lower, this.range);
		}
		
		private String genAnswer(int lower, int range){
			return String.valueOf(this.random.nextInt(range) + lower);
		}
		
		private void setMode(int mode){
			if(mode > 1)
				mode = 0;
			this.mode = mode;
		}
		
		private void setRange(){
			switch (mode) {
				case 0:
					this.range = 10;
					break;
				case 1:
					this.range = 13;
					break;
				default:
					this.range = 10;
					break;
			}
		}
		
	}
}
