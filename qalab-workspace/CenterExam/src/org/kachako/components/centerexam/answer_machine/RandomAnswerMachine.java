package org.kachako.components.centerexam.answer_machine;

public abstract class RandomAnswerMachine {
	
	private String preAnswer = "";		// 解答機が出力した１つ前の解答を記憶
	private String defAnswer = null;	// null 以外のときはこれを gen() で返す
	private String[] symbol = null;
	
	RandomAnswerMachine(){
		setDefaultAnswer(null);
		setSymbol(null);
	}
	
	RandomAnswerMachine(String defaultAnswer){
		setDefaultAnswer(defaultAnswer);
	}
	
	RandomAnswerMachine(String[] symbol){
		setSymbol(symbol);
	}
	
	// 解答の生成　戻り値は lower ~ range の範囲内で String に変換したもの
	abstract String gen(int lower, int range);
	
	public void setDefaultAnswer(String defaultAnswer){
		this.defAnswer = defaultAnswer;
	}
	
	public String getDefualtAnswer(){
		return this.defAnswer;
	}
	
	public void clearDefAnswer(){
		this.defAnswer = null;
	}
	
	public void setPreAnswer(String preAnswer){
		this.preAnswer = preAnswer;
	}
	
	public String getPreAnswer(){
		return this.preAnswer;
	}
	
	public void clearPreAnswer(){
		this.preAnswer = "";
	}
	
	public void setSymbol(String[] symbol){
		this.symbol = symbol;
	}
	
	public String[] getSymbol(){
		return this.symbol;
	}
	
	public void clearSymbol(){
		this.symbol = null;
	}
	
	// 初期化
	public void reset(){
		clearPreAnswer();
		clearDefAnswer();
		clearSymbol();
	}

}
