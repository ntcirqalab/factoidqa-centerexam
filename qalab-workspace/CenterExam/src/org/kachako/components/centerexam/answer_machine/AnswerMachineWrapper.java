package org.kachako.components.centerexam.answer_machine;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.kachako.components.centerexam.ExamWriterMultiple;
import org.kachako.components.centerexam.ExamWriterMultiple.ExamXmlWriter;
import org.kachako.components.centerexam.answertable.AnswerTableReader;

public class AnswerMachineWrapper extends JCasAnnotator_ImplBase {
	
	public static final String PARAM_COMMAND_NAME_WITH_OPTIONS = "CommandNameWithOptions";
	public static final String PARAM_BASE_DIRECTORY = "BaseDirectory";
    public static final String PARAM_ENVIRONMENT_VARIABLES = "EnvironmentVariables";
	
	String inputFileName;
	String outputDir;
	String outputXmlFileName;

	private String[] commands;
	private String baseDirectoryName;
	private String[] environmentVariables;
	
	
	@Override
	public void initialize(UimaContext aContext)
			throws ResourceInitializationException {
		// TODO Auto-generated method stub
		super.initialize(aContext);
		
		commands = (String[])aContext.getConfigParameterValue(PARAM_COMMAND_NAME_WITH_OPTIONS);
	    baseDirectoryName = (String)aContext.getConfigParameterValue(PARAM_BASE_DIRECTORY);
	    environmentVariables = (String[])aContext.getConfigParameterValue(PARAM_ENVIRONMENT_VARIABLES);

	    examXmlWriter = new ExamXmlWriter();
	}
	
	ExamXmlWriter examXmlWriter;
	
	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		// TODO Auto-generated method stub
			ArrayList<String> commandList = new ArrayList<String>();
			File inputFile;
			File answerTableFile;
//			XMLInputSource in;
			FSIterator<Annotation> fsIte = jcas.getAnnotationIndex(SourceDocumentInformation.type).iterator(); 	
			try {
				String examFileName = "";
				if(fsIte.hasNext()){
			    	SourceDocumentInformation sourceDocInfo = (SourceDocumentInformation) fsIte.next();
			    	examFileName = new File(new URI(sourceDocInfo.getUri()).getPath()).getName();
				}
				if(examFileName != null && examFileName.length() > 0){
					inputFile = File.createTempFile(examFileName + ".exam.", ".xml");
					answerTableFile = File.createTempFile(examFileName + ".answer.", ".xml");
				}
				else{
					inputFile = File.createTempFile("exam.", ".xml");
					answerTableFile = File.createTempFile("answer.", ".xml");
				}
//				System.out.println(inputFile.getAbsolutePath() + " -> " + answerTableFile.getAbsolutePath());
//				ExamWriter.writeCenterExamXML(jcas, inputFile);
				examXmlWriter.writeCenterExamXML(jcas, inputFile);
//				ExamWriterMultiple.writeCenterExamXML(jcas, inputFile);
				setCommands(commandList, commands, inputFile, answerTableFile);
//				ProcessBuilder processBuilder = new ProcessBuilder(commands[0], inputFile.toString(), answerTableFile.toString());
				ProcessBuilder processBuilder = new ProcessBuilder(commandList);
				
				processBuilder.directory(new File(baseDirectoryName));
			    processBuilder.environment().put("PATH", "/usr/local/bin");
			    setEnvironments(processBuilder);
			   
				Process process;
				try {
					process = processBuilder.start();
					
					final InputStream errorStream = process.getErrorStream();
					new Thread() {
						@Override
						public void run() {
							BufferedReader reader = new BufferedReader(new InputStreamReader(errorStream));
							String line;
							try {
								while (null != (line = reader.readLine())) {
									 System.err.println(line);
								}
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}.start();

					final InputStream outputStream = process.getInputStream();
					new Thread() {
						@Override
						public void run() {
							BufferedReader reader = new BufferedReader(new InputStreamReader(outputStream));
							String line;
							try {
								while (null != (line = reader.readLine())) {
									 System.out.println(line);
								}
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}.start();
					
					process.waitFor();
					
					AnswerTableReader.readAnswerTable(jcas, answerTableFile);
										
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}								     
	}     
	
	public void setEnvironments(ProcessBuilder processBuilder) {

	    Map<String,String> env = processBuilder.environment();
	    
	    if ( environmentVariables != null ) {
	      for (int i = 0; i < environmentVariables.length; i+=2) {
	        String varName = environmentVariables[i];
	        String value = environmentVariables[i + 1];
	        env.put(varName, value);
	      }
	    }
	    
	  }
	
	public void setCommands(ArrayList<String> commandList, String[] commands, File input, File output){
		commandList.addAll(Arrays.asList(commands));
		commandList.add(input.toString());
		commandList.add(output.toString());
		System.out.println(commandList);
	}
	
}
