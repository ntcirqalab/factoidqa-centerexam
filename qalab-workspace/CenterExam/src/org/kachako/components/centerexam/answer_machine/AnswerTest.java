package org.kachako.components.centerexam.answer_machine;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.kachako.components.centerexam.answertable.AnswerTableReader;
import org.kachako.types.centerexam.answertable.AnswerTable;

public class AnswerTest extends JCasAnnotator_ImplBase {
	
	public static final String PARAM_INPUTDIR = "InputDirectory";
	public String INPUT_DIRECTORY;
	
	public HashMap<String, File> answerTableMap;
	
	@Override
	public void initialize(UimaContext aContext) throws ResourceInitializationException {
		super.initialize(aContext);
		
		INPUT_DIRECTORY = aContext.getConfigParameterValue(PARAM_INPUTDIR).toString().trim();
		answerTableMap = new HashMap<String, File>();
		
		File directory = new File(INPUT_DIRECTORY);
		
		if(directory.exists() || directory.isDirectory()){
			File file[] = directory.listFiles();
			for (int i = 0; i < file.length; i++) {
				File f = file[i];
				if(f.isFile() && f.canRead() && f.getPath().endsWith(".xml")){
					answerTableMap.put(f.getName(), f);
				}
			}
		}
	}

	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		// TODO Auto-generated method stub
		
		FSIterator<Annotation> fsIte = jcas.getAnnotationIndex(SourceDocumentInformation.type).iterator();
	    if(!fsIte.hasNext()) return;
	       
	    SourceDocumentInformation srcDocInfo = (SourceDocumentInformation) fsIte.next();
	    try {
	        File file = answerTableMap.get(new File(new URI(srcDocInfo.getUri()).getPath()).getName());
			if(file != null){
				AnswerTableReader.readAnswerTable(jcas, file);
			}
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
