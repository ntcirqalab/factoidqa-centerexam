package org.kachako.components.centerexam.answer_machine;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.kachako.components.centerexam.answertable.AnswerTableReader;
import org.kachako.types.centerexam.AnsColumn;
import org.kachako.types.centerexam.Exam;
import org.kachako.types.centerexam.Question;
import org.kachako.types.centerexam.TagList;

public class RandomAnswer2 extends JCasAnnotator_ImplBase {
		
	final static String PARAM_DEFANS = "DefaultAnswer";
	static String DEFAULT_ANSWER;
	
	@Override
	public void initialize(UimaContext aContext) throws ResourceInitializationException {
		// TODO Auto-generated method stub
		super.initialize(aContext);
		DEFAULT_ANSWER = (String)aContext.getConfigParameterValue(PARAM_DEFANS);
		if(DEFAULT_ANSWER == null || DEFAULT_ANSWER.trim().length() == 0)
			DEFAULT_ANSWER = "1";
		DEFAULT_ANSWER = DEFAULT_ANSWER.trim();
	}
	
	
	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		// TODO Auto-generated method stub
		
		File answerTableFile = createRandomAnswerXMLFile(jcas);
		AnswerTableReader.readAnswerTable(jcas, answerTableFile);
			
		
	}


	public File createRandomAnswerXMLFile(JCas jcas) {
		
		String fileName = null;

		ArrayList<Annotation> fsAnnotationList = new ArrayList<Annotation>();
		ArrayList<Type> fsTypeList = new ArrayList<Type>(); 
		 
	    TypeSystem typeSystem = jcas.getTypeSystem();
	    Type type = typeSystem.getType(SourceDocumentInformation.class.getName());
	    
	    FSIterator<FeatureStructure> fsIterator = jcas.getFSIndexRepository().getAllIndexedFS(type);
	    if(fsIterator.hasNext()){
	    	SourceDocumentInformation srcDocInfo = (SourceDocumentInformation) fsIterator.next();
	    	try {
				String name = new File(new URI(srcDocInfo.getUri()).getPath()).getName();
				fileName = name.substring(0, name.indexOf(".xml"));
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
			}
	    }
	    
	    type = typeSystem.getType(TagList.class.getName());	    
	    
	    fsIterator = jcas.getFSIndexRepository().getAllIndexedFS(type);
		TagList tagList = (TagList)fsIterator.next();
		FSArray fsArray = tagList.getAnnotationList();
		
//		TreeSet<String> skipQuestion = new TreeSet<String>();
        StringBuilder selected = new StringBuilder();
		
		//get  Begin,End
		for(int i=0;i<fsArray.size();i++){
			if(fsArray.get(i).getType().getName().equals(Exam.class.getName())){
				Annotation ann = (Annotation) fsArray.get(i);
				String range = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("range_of_options"));
        		String num = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("num_of_options"));
        		if(range != null && num != null){
        			try {
						String ids[] = range.split(" ");
						int n = Integer.valueOf(num);
						for (int j = 0; j < ids.length; j++) {
							if(j < n)
								selected.append(ids[j] + " ");
							else
								break;
						}
					} catch (Exception e) {
						
					}
        		}
//        		System.out.println(range);
//        		System.out.println(num);
//        		System.out.println(selected);
//        		System.out.println(skipQuestion);
			}
			else if( fsArray.get(i).getType().toString().equals(Question.class.getName()) ){
				fsTypeList.add(fsArray.get(i).getType());
				fsAnnotationList.add((Annotation) fsArray.get(i));
			}
        }
		
	    XmlObject newXml = XmlObject.Factory.newInstance();
        XmlCursor cursor = newXml.newCursor();
        
        cursor.push();
        cursor.toNextToken();
        
        int index = 0;
        int largeIndex = 0;
        int minimalIndex = 0;
        int answerColumn = 0;
        String largeString = "";
        String qestionID = "";
        String ansStyle = null;
        String ansType = null;
        String knowledge = null;
        String mini = null;
        String anscol = null;
        String checkId = null;
        
            
        //Insert XML tags
		cursor.beginElement("answerTable");
		if(selected.toString().trim().length() > 0)
			cursor.insertAttributeWithValue("selected", selected.toString().trim());
		if(fileName != null)
			cursor.insertAttributeWithValue("filename", fileName);
		
        while(index < fsTypeList.size()){

        	if(fsTypeList.get(index).getName().equals(Question.class.getName())){
        		
        		mini = fsAnnotationList.get(index).getFeatureValueAsString(fsTypeList.get(index).getFeatureByBaseName("minimal"));
        		if(mini == null || mini.equals("no")){
        			
        			minimalIndex=0;
        			largeIndex++;
        			largeString = "第" + String.valueOf(largeIndex) + "問";
        			checkId = fsAnnotationList.get(index).getFeatureValueAsString(fsTypeList.get(index).getFeatureByBaseName("id"));
            
        		}
        		else if(mini.equals("yes")){
        			minimalIndex++;
        		}
        		
        		qestionID = fsAnnotationList.get(index).getFeatureValueAsString(fsTypeList.get(index).getFeatureByBaseName("id"));
        		ansStyle = fsAnnotationList.get(index).getFeatureValueAsString(fsTypeList.get(index).getFeatureByBaseName("answer_style"));
        		ansType = fsAnnotationList.get(index).getFeatureValueAsString(fsTypeList.get(index).getFeatureByBaseName("answer_type"));
        		knowledge = fsAnnotationList.get(index).getFeatureValueAsString(fsTypeList.get(index).getFeatureByBaseName("knowledge_type"));
        		anscol = fsAnnotationList.get(index).getFeatureValueAsString(fsTypeList.get(index).getFeatureByBaseName("anscol"));

        		if(anscol != null){
        			
        			String ids[] = anscol.split(" ");
        		
        			for(int i = 0; i<ids.length; i++){
        			
        			answerColumn++;
        		
        			cursor.beginElement("data");
        		
        			cursor.beginElement("section");
        			cursor.insertChars(largeString);
        			cursor.toNextToken();
        		
        			cursor.beginElement("question");
        			cursor.insertChars(String.valueOf(minimalIndex));
        			cursor.toNextToken();
        		
        			cursor.beginElement("answer_column");
        			cursor.insertChars(String.valueOf(answerColumn));
        			cursor.toNextToken();
        		
        			cursor.beginElement("answer");
        			cursor.insertAttributeWithValue("system", "no");
        			cursor.insertChars(DEFAULT_ANSWER);
//        			if(index % 2 == 0)
//        				cursor.insertChars("2");
        			cursor.toNextToken();
        		
//        		cursor.beginElement("score");
//        		cursor.insertChars("");
//        		cursor.toNextToken();
        		
        			cursor.beginElement("answer_style");
        			if(ansStyle != null)
        				cursor.insertChars(ansStyle);
        			else
        				cursor.insertChars(" ");
        			cursor.toNextToken();
        		
        			cursor.beginElement("answer_type");
        			if(ansType != null)
        				cursor.insertChars(ansType);
        			else
        				cursor.insertChars(" ");
        			cursor.toNextToken();
        		
        			cursor.beginElement("knowledge_type");
        			if(knowledge != null)
        				cursor.insertChars(knowledge);
        			else
        				cursor.insertChars(" ");
        			cursor.toNextToken();
        		
        			cursor.beginElement("question_ID");
        			cursor.insertChars(qestionID);
        			cursor.toNextToken();
        		
        			cursor.beginElement("anscolumn_ID");
        			cursor.insertChars(ids[i]);
        			cursor.toNextToken();
        		
        			cursor.beginElement("process_log");
        			cursor.insertChars("Random Answer Machine");
        			cursor.toNextToken();
        		
        			cursor.toNextToken();
        			}
        		}
        	}
        	
        	index++;
        }

        //save XML file
       try {

    	   File tmpFile = File.createTempFile("RAT", ".xml");
    	   
    	   cursor.pop();
    	   cursor.save(tmpFile);
    	   cursor.dispose();
           
    	   return tmpFile;
    	   
    	   
       } catch (IOException e) {
    	   // TODO Auto-generated catch block
    	   e.printStackTrace();
		}
        
       return null;
       
	}

}
