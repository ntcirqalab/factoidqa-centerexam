package org.kachako.components.centerexam;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;
import java.util.TreeSet;

import javax.xml.namespace.QName;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.xmlbeans.*;
import org.apache.xmlbeans.XmlCursor.TokenType;
import org.kachako.components.centerexam.ExamWriterTagType;
import org.kachako.components.centerexam.ExamWriterAttribute;
import org.kachako.components.centerexam.answertable.AnswerTableGetter;
import org.kachako.components.centerexam.SaveResult;
import org.kachako.types.centerexam.answertable.Answer;
import org.kachako.types.centerexam.answertable.AnswerColumn;
import org.kachako.types.centerexam.answertable.AnswerColumnID;
import org.kachako.types.centerexam.answertable.AnswerMachineType;
import org.kachako.types.centerexam.answertable.AnswerTableAnnotationList;
import org.kachako.types.centerexam.answertable.DataAT;
import org.kachako.types.centerexam.answertable.ProcessLog;
import org.kachako.types.centerexam.CenterExamTag;
import org.kachako.types.centerexam.Math;
import org.kachako.types.centerexam.EndList;
import org.kachako.types.centerexam.TagList;
import org.kachako.types.centerexam.Title;
import org.kachako.types.comparable.AnnotationGroup;
import org.kachako.types.comparable.ComparisonSet;

public class ExamWriterMultiple4Kachako extends JCasAnnotator_ImplBase{
	
	public static final String PARAM_OUTPUTDIR = "OutputDirectory";
	public static final String PARAM_IMAGEDIR = "ImageDirectory";
	public static final String PARAM_LIBRARYDIR = "VisualizerLibrayDirectory";
	public static final String PARAM_ADDCSS = "AddCSS";
	public static final String PARAM_LAUNCH_BROWSER = "LaunchBrowser";
		
	static String XML_OUTPUTDIR;
	static String IMAGE_DIR;
	static String LIBRARY_DIR;
	static String SUFFIX;
	static boolean ADD_CSS;
	static boolean LAUNCH_BROWSER;
	static boolean MATH;

	HashMap<String, Integer> scoreMap;
	HashMap<String, Integer> totalScoreMap;
	HashMap<String, Integer> numOfQuestionMap;
	
	@Override
	public void initialize(UimaContext aContext) throws ResourceInitializationException {
		super.initialize(aContext);
					
		System.out.println("--------ExamWriterMultiple(Kachako) initialize---------");
		XML_OUTPUTDIR = aContext.getConfigParameterValue(PARAM_OUTPUTDIR).toString();
		IMAGE_DIR = aContext.getConfigParameterValue(PARAM_IMAGEDIR).toString();
		LIBRARY_DIR = aContext.getConfigParameterValue(PARAM_LIBRARYDIR).toString();
		ADD_CSS = (Boolean) aContext.getConfigParameterValue(PARAM_ADDCSS);
		LAUNCH_BROWSER = (Boolean) aContext.getConfigParameterValue(PARAM_LAUNCH_BROWSER);
			
		scoreMap = new HashMap<String, Integer>();
		totalScoreMap = new HashMap<String, Integer>();
		numOfQuestionMap = new HashMap<String, Integer>();
		examXmlWriter = new ExamXmlWriter();
	}

	private ExamXmlWriter examXmlWriter;
		 
	@Override
	public void process(JCas pjcas) throws AnalysisEngineProcessException {

		System.out.println("--------ExamWriterMultiple(Kachako) start---------");
		JCas jcas = null;
		JCas compjcas = null;
		try {
			jcas = pjcas.getView("Text");
		} catch (CASException e) {
			System.out.println("no Text View");
			e.printStackTrace();
			return;
		}
		try {
			compjcas = pjcas.getView("KachakoComparison");
		} catch (CASException e) {
			System.out.println("no KachakoComparision View");
			e.printStackTrace();
			return;
		}

System.out.println("ExamWriterMultiple:XML_OUTPUTDIR="+XML_OUTPUTDIR);		
		File inputFile = createXmlFile(jcas, XML_OUTPUTDIR);
		if(inputFile == null) {
			System.out.println("ExamWriterMultiple:createXmlFile:inputFile is null");
			return;
		}
System.out.println("ExamWriterMultiple:createXmlFile:inputFile="+inputFile.getPath());		
		try {
			examXmlWriter.writeCenterExamXML(jcas, compjcas, inputFile, true);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
		
	@Override
	public void collectionProcessComplete() throws AnalysisEngineProcessException {
			
System.out.println("--------ExamWriterMultiple(Kachako) collectionProcessComplete---------");
		super.collectionProcessComplete();
						
		//jqueryのコピー
		String srcFilePath = LIBRARY_DIR + File.separator + "jquery-1.7.2.min.js";
		File srcFile = new File(srcFilePath);
		File destFile = new File(XML_OUTPUTDIR + File.separator + "html", srcFile.getName());
		
		//javascriptのコピー
		srcFilePath = LIBRARY_DIR + File.separator +"replace-answer-type.js";
		File jsAnswerSrcFile = new File(srcFilePath);
		File jsAnswerDestFile = new File(XML_OUTPUTDIR + File.separator + "html", jsAnswerSrcFile.getName());
		
		srcFilePath = LIBRARY_DIR + File.separator +"replace-knowledge-type.js";
		File jsKnowledgeSrcFile = new File(srcFilePath);
		File jsKnowledgeDestFile = new File(XML_OUTPUTDIR + File.separator + "html", jsKnowledgeSrcFile.getName());
		
		Desktop desk = Desktop.getDesktop();
			
		try {
			File indexHtml = examXmlWriter.createIndexHtml(XML_OUTPUTDIR + File.separator +"html");
				
			copyFile(srcFile, destFile);
			copyFile(jsAnswerSrcFile, jsAnswerDestFile);
			copyFile(jsKnowledgeSrcFile, jsKnowledgeDestFile);
				
			if (MATH) {
				if (ADD_CSS) {
					File xslSrcFile = new File(LIBRARY_DIR + File.separator + "torobo-htmlForMathAddCSS.xsl");
					File xslDestFile = new File(XML_OUTPUTDIR + File.separator + "html", "torobo-htmlForMathAddCSS.xsl");
					copyFile(xslSrcFile, xslDestFile);
					xslDestFile = new File(XML_OUTPUTDIR + File.separator + "xml", "torobo-htmlForMathAddCSS.xsl");
					copyFile(xslSrcFile, xslDestFile);
				} else {
					File xslSrcFile = new File(LIBRARY_DIR + File.separator + "torobo-htmlForMath.xsl");
					File xslDestFile = new File(XML_OUTPUTDIR + File.separator + "html", "torobo-htmlForMath.xsl");
					copyFile(xslSrcFile, xslDestFile);
					xslDestFile = new File(XML_OUTPUTDIR + File.separator + "xml", "torobo-htmlForMath.xsl");
					copyFile(xslSrcFile, xslDestFile);
				}
			}
			URI uri = indexHtml.toURI();

			srcFilePath = LIBRARY_DIR + File.separator + "exam.css";
			File cssSrcFile = new File(srcFilePath);
			File cssDestFile = new File(XML_OUTPUTDIR + File.separator + "html", cssSrcFile.getName());
			copyFile(cssSrcFile, cssDestFile);

			if(LAUNCH_BROWSER)
				desk.browse(uri);

		} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}			


	public static class ExamXmlWriter {		
		
		// html ファイルの作成
		private File createIndexHtml(String dir) throws IOException {
			
			File indexFile = new File(dir, "index.html");
			PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(indexFile), "UTF-8")));
					
			pw.println("<html>");
			pw.println("<head>");
			pw.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />");
			pw.print("<title>");
			pw.print("出力結果一覧");
			pw.println("</title>");
			pw.println("</head>");
			pw.println("<body>");

			pw.println("<div style=\"margin:1em 3% 1em 3%;\">");
			
			pw.println("<h1>" + "出力結果一覧" + "<br /></h1>");
			pw.println("<hr>");
			
			pw.println("<table border=\"2px\" bodercoler=\"black\" Cellspacing=\"0\">");
			pw.println("<tr>");
			pw.println("<th rowspan=2>");
			pw.println("試験名（ファイル名）");
			pw.println("</th>");
			for (int i = 0; i < componentList.size(); i++) {
				pw.println("<th colspan=3>");
				pw.println(componentList.get(i));
				pw.println("</th>");
			}
			pw.println("</tr>");
			pw.println("<tr>");
			for (int i = 0; i < componentList.size(); i++) {
				pw.println("<th>正答率</th>");
				pw.println("<th>得点</th>");
				pw.println("<th>正答数/問題数</th>");
			}
			pw.println("</tr>");
						
			for (int i = 0; i < htmlFiles.size(); i++) {
				
				File tmp = htmlFiles.get(i);
				pw.println("<tr align=\"right\">");
				pw.println("<td><a href=\"" + tmp.getName() + "\">" + titleMap.get(tmp.getName()) + "</a>"
						+ "(" + tmp.getName() + ")</td>");
				for (int j = 0; j < componentList.size(); j++) {
					String key = tmp.getName() + componentList.get(j);
					String saveRate = saveResult.getRate(key);
					if(saveResult.getRate(key) == null){
						saveRate = "0.0000"; 
					}
					BigDecimal bd = new BigDecimal(Double.valueOf(saveRate)*100);
					pw.println("<td><font color=red>" + bd.setScale(2, RoundingMode.HALF_UP) + "%</font></td>");
					pw.println("<td><font color=blue>" + saveResult.getScore(key) + "</font></td>");
					pw.println("<td>" + saveResult.getNumOfCorrect(key) +"/"+ saveResult.getNumOfQuestion(key) + "</td>");
				}
			}
			pw.println("</table>");
			pw.println("</div>");
			pw.println("</body>");
			pw.println("</html>");
			
			pw.close();
			
			return indexFile;
		}
		
		private static boolean checkEndTag(String tag){
			if(tag.equals("img") || tag.equals("br") || tag.equals("missing")
					|| tag.equals("formula") || tag.equals("uText") || tag.equals("blank")
					|| tag.equals("ansColumn") || tag.equals("selector") || tag.equals("vector")
					|| tag.equals("plus") || tag.equals("minus") || tag.equals("times")
					|| tag.equals("divide") || tag.equals("power") || tag.equals("root")
					|| tag.equals("abs") || tag.equals("conjugate") || tag.equals("arg")
					|| tag.equals("and") || tag.equals("or") || tag.equals("not")
					|| tag.equals("iplies") || tag.equals("forall") || tag.equals("eq")
					|| tag.equals("neq") || tag.equals("gt") || tag.equals("lt")
					|| tag.equals("geq") || tag.equals("let") || tag.equals("equvalent")
					|| tag.equals("factorof") || tag.equals("diff") || tag.equals("int")
					|| tag.equals("set") || tag.equals("list") || tag.equals("union")
					|| tag.equals("intersect") || tag.equals("prsubset") || tag.equals("sum")
					|| tag.equals("product") || tag.equals("sin") || tag.equals("cos")
					|| tag.equals("tan") || tag.equals("log") || tag.equals("integers")
					|| tag.equals("naturalnumbers") || tag.equals("imaginalyi") || tag.equals("pi")
					|| tag.equals("mprescripts") || tag.equals("none") || tag.equals("scalarproduct")
					|| tag.equals("in") || tag.equals("card") || tag.equals("product")
					|| tag.equals("mspace"))
				return true;
			else
				return false;
		}


		public void writeCenterExamXML(JCas jcas, JCas compjcas, File outputFile) {
			try {
				writeCenterExamXML(jcas, compjcas, outputFile, false);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		HashMap<String, String> titleMap = new HashMap<String, String>();
		List<File> htmlFiles = new ArrayList<File>();
		List<String> componentList = new ArrayList<String>();
		SaveResult saveResult = new SaveResult();
		int numberOfCorrect;

		
		// 問題構造 xml の生成
		// extend が True: 解答結果と採点を追加して出力
		//          False: 問題構造のみ出力
		public void writeCenterExamXML(JCas jcas, JCas compjcas, File outputFile, boolean extend) throws IOException {

			HashMap<String,String> ansColumnIDArrayList = new HashMap<String,String>();
			HashMap<String,String> answerArrayList = new HashMap<String,String>();
			HashMap<String,String> scoreArrayList = new HashMap<String,String>();
			HashMap<String,String> processLogArrayList = new HashMap<String,String>();
			
			boolean flag = false;
			boolean bl = false;
			
			if (extend) {
				AnswerTableGetter ansGet = new AnswerTableGetter();
				try {
System.out.println("解答結果と採点を追加");
					ansGet.getAnswerData(jcas, ansColumnIDArrayList, answerArrayList, scoreArrayList, processLogArrayList);
				} catch (XmlException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			
			// 開始位置、終了位置の取得
			List<Integer> fsBeginList = new ArrayList<Integer>();
			List<Integer> fsEndList = new ArrayList<Integer>();
			List<Type> fsTypeList = new ArrayList<Type>();
			List<Annotation> fsAnnotationList = new ArrayList<Annotation>();
			List<Annotation> fsTypeEndList = new ArrayList<Annotation>();
			
			TypeSystem typeSystem = jcas.getTypeSystem();
			Type type = typeSystem.getType(CenterExamTag.class.getName());
			
			type = typeSystem.getType(TagList.class.getName()); 
			FSIterator<FeatureStructure> fsAnn = jcas.getFSIndexRepository().getAllIndexedFS(type);

			TagList tagList = (TagList)fsAnn.next();
			FSArray fsArray = tagList.getAnnotationList();
System.out.println("TagList.size="+fsArray.size());	
			type = typeSystem.getType(EndList.class.getName());
			fsAnn = jcas.getFSIndexRepository().getAllIndexedFS(type);

			EndList endList = (EndList)fsAnn.next();
			FSArray fsArrayEnd = endList.getAnnotationList();
System.out.println("EndList.size="+fsArray.size());	

			int titleBegin = 0, titleEnd = 0;
			
			for (int i = 0; i < fsArray.size(); i++) {
				
				Annotation anno = (Annotation)fsArray.get(i);
				Annotation annoEnd = (Annotation)fsArrayEnd.get(i);
				
				fsAnnotationList.add(anno);
				fsTypeList.add(anno.getType());
				fsBeginList.add(anno.getBegin());
				fsEndList.add(annoEnd.getEnd());
				fsTypeEndList.add(annoEnd);
				
				if(anno.getType().getName().equals(Title.class.getName())){
					titleBegin = anno.getBegin();
					titleEnd = anno.getEnd();
				}
System.out.println("fsArray.get("+i+").getType().getShortName()="+anno.getType().getName());
				if(anno.getType().getShortName().equals("Exam")){
					MATH = (anno.getBooleanValue(anno.getType().getFeatureByBaseName("math")));
					if(MATH)
						SUFFIX = ".xhtml";
			    	else
			    		SUFFIX = ".html";
				}
			}
			
			Collections.sort(fsEndList);
						
			XmlObject newXml = XmlObject.Factory.newInstance();
			XmlCursor cursor = newXml.newCursor();
			String xmlString = jcas.getDocumentText();
			cursor.push();
			cursor.toNextToken();
			
			if (extend) {
				//insert PROCINST (xml-stylesheet)
				String target = "xml-stylesheet";
				String text = null;
				String xslfile = null;
				if (MATH && ADD_CSS)
					xslfile = "torobo-htmlForMathAddCSS.xsl";
				else if (MATH)
					xslfile = "torobo-htmlForMath.xsl";
				else if (ADD_CSS)
					xslfile = "torobo-htmlAddCSS.xsl";
				else
					xslfile = "torobo-html.xsl";
				if (MATH)
					text = "href=\"" + xslfile + "\" type=\"text"+ File.separator +"xsl\"";
				else
					text = "href=\"" + LIBRARY_DIR + File.separator + xslfile + "\" type=\"text"+ File.separator +"xsl\"";
				
				cursor.insertProcInst(target, text);
				cursor.toNextToken();
				cursor.insertChars("\n");
			}
	        
			int beginIndex = 0;
			int endIndex = 0;
	        
			String insertString = "";
			ExamWriterTagType tagType = new ExamWriterTagType();
			ExamWriterAttribute attr = new ExamWriterAttribute();
			
			int count = 0;
			
			Stack<Annotation> annStack = new Stack<Annotation>();
			
			Annotation buf = null;
			int typeIndex = 0;
			boolean removeFlag = false;

			// 問題構造の各タグを挿入
			while (endIndex < fsEndList.size() && count < xmlString.length()) {
				
				if(fsBeginList.size() == beginIndex){
					beginIndex = fsBeginList.size() - 1;
					flag = true;
				}
				
				// 開始タグと次の開始タグもしくは終了タグまでのテキストを挿入
				if(fsBeginList.get(beginIndex) < fsEndList.get(endIndex) && !flag){
	        		insertString = tagType.getBeginTagType(fsTypeList.get(beginIndex));
	        		if(insertString.equals(""))
	        			System.out.println("WriterParseError: " + fsTypeList.get(beginIndex).getShortName() + " is not defined");
	        		
	        		endIndex = checkToNextToken(fsTypeEndList, cursor, count, annStack, typeIndex);
	        		
	        		// 開始タグ挿入
	        		if(!insertString.equals("")) {
	        			
	        			if(insertString.equals("math"))
	        				cursor.beginElement(new QName( ((Math)fsAnnotationList.get(beginIndex)).getXmlns(), insertString, "m"));
	        			else
	        				cursor.beginElement(insertString);
	        			
	        			attr.setAttribute(cursor, insertString, fsAnnotationList.get(beginIndex), IMAGE_DIR);
	        			annStack.push(fsAnnotationList.get(beginIndex));
	        			typeIndex = checkToNextToken(fsTypeEndList, cursor, count, annStack, typeIndex);
	        		}
	        		
	        		// 次の開始タグもしくは終了タグまでのテキストを挿入
	        		if(beginIndex + 1 < fsBeginList.size()){
	        			if(ADD_CSS && removeFlag){
	        				insertString = xmlString.substring(7, fsBeginList.get(beginIndex + 1));
	        				cursor.insertChars(insertString);
	        				count += insertString.length() + 7;
	        				removeFlag = false;
	        			}
	        			
	        			else if(fsBeginList.get(beginIndex + 1) <= fsEndList.get(endIndex) && fsBeginList.get(beginIndex + 1) - fsBeginList.get(beginIndex) > 0){
	        				insertString = xmlString.substring(fsBeginList.get(beginIndex), fsBeginList.get(beginIndex + 1));
	        				cursor.insertChars(insertString);
	        				count += insertString.length();
	        				
	        			}
	        			
	        			else if(fsEndList.get(endIndex) < fsBeginList.get(beginIndex + 1) && fsEndList.get(endIndex) - fsBeginList.get(beginIndex) > 0){
	        			
	        				insertString = xmlString.substring(fsBeginList.get(beginIndex), fsEndList.get(endIndex));
	        				cursor.insertChars(insertString);
	        				count += insertString.length();
	        				
	        			}
	        		}
	        		
	        		beginIndex++;

	        		typeIndex = checkToNextToken(fsTypeEndList, cursor, count, annStack, typeIndex);
	        	}
	        	
	        	
	        	// テキスト挿入
	        	else if(fsEndList.get(endIndex) < fsBeginList.get(beginIndex) || flag){
	        		
	        		typeIndex = checkToNextToken(fsTypeEndList, cursor, count, annStack, typeIndex);
	        		
	        		if(endIndex + 1 < fsEndList.size()){
	        			if(fsEndList.get(endIndex + 1) <= fsBeginList.get(beginIndex) || flag){
	        			
	        				insertString = xmlString.substring(fsEndList.get(endIndex), fsEndList.get(endIndex + 1));
	        				cursor.insertChars(insertString);
	        				count += insertString.length();
	        				
	        			}
	        			else if(fsBeginList.get(beginIndex) < fsEndList.get(endIndex + 1) && fsBeginList.get(beginIndex) - fsEndList.get(endIndex) > 0){
	        				{
	        				
	        					insertString = xmlString.substring(fsEndList.get(endIndex), fsBeginList.get(beginIndex));
	        					cursor.insertChars(insertString);
	        					count += insertString.length();
	        				}
	        			}
	        		}
	        		
        			endIndex++;	
        			
        			typeIndex = checkToNextToken(fsTypeEndList, cursor, count, annStack, typeIndex);
	        	}
	        	
	        	// 終了タグ挿入
	        	else{
	        		insertString = tagType.getBeginTagType(fsTypeList.get(beginIndex));
	        		
	        		typeIndex = checkToNextToken(fsTypeEndList, cursor, count, annStack, typeIndex);
	        		
	        		if(checkEndTag(insertString)){
	        			while(!bl){	
        					if(!annStack.isEmpty()){
        						buf = annStack.pop();
        						if(count == buf.getEnd() && fsTypeEndList.get(typeIndex).getType().getName().equals(buf.getType().getName())){
        							cursor.toNextToken();
        							typeIndex++;		
        						}
        						else{
        							annStack.push(buf);
        							bl = true;
        						}	
        					}
        					else{
        						bl = true;
        					}
        				}
        				bl = false;
        				
	        			cursor.beginElement(insertString);
	        			attr.setAttribute(cursor, insertString, fsAnnotationList.get(beginIndex), IMAGE_DIR);
	        			
	        			annStack.push(fsAnnotationList.get(beginIndex));
	        			
	        			typeIndex = checkToNextToken(fsTypeEndList, cursor, count, annStack, typeIndex);
	        			typeIndex = checkToNextToken(fsTypeEndList, cursor, count, annStack, typeIndex);
	        			
	        			if(endIndex + 1 < fsEndList.size() && beginIndex + 1 < fsBeginList.size()){
	        				
	        				if(fsEndList.get(endIndex + 1) <= fsBeginList.get(beginIndex + 1) && fsEndList.get(endIndex + 1) - fsEndList.get(endIndex) > 0){
	        					
	        					insertString = xmlString.substring(fsEndList.get(endIndex), fsEndList.get(endIndex + 1));
	        					cursor.insertChars(insertString);
	        					count += insertString.length();
	       					}
	        				else if(fsBeginList.get(beginIndex + 1) < fsEndList.get(endIndex + 1) && fsBeginList.get(beginIndex + 1) - fsEndList.get(endIndex) > 0){
		        					
	        					insertString = xmlString.substring(fsEndList.get(endIndex), fsBeginList.get(beginIndex +1));
	        					cursor.insertChars(insertString);
	        					count += insertString.length();
	        				}
		        		}
	        			
	        			if(endIndex + 1 < fsEndList.size() && beginIndex + 1 == fsBeginList.size()){
	        				
	        				insertString = xmlString.substring(fsEndList.get(endIndex), fsEndList.get(endIndex + 1));
        					cursor.insertChars(insertString);
        					count += insertString.length();
	        			
	        			}
	        			beginIndex++;
	        		}
	        		else{

	        			typeIndex = checkToNextToken(fsTypeEndList, cursor, count, annStack, typeIndex);
	        		}
	        		
	        		typeIndex = checkToNextToken(fsTypeEndList, cursor, count, annStack, typeIndex);
	        		
	        		
	        		if(endIndex + 1 < fsEndList.size() && flag){
	        			insertString = xmlString.substring(fsEndList.get(endIndex), fsEndList.get(endIndex + 1));
	        			cursor.insertChars(insertString);
	        			count += insertString.length();
	        		}
	        		
	        		endIndex++;

	        		typeIndex = checkToNextToken(fsTypeEndList, cursor, count, annStack, typeIndex);
	        	}
	        }
		    
		    //score の集計
		    if(extend){

		    	cursor.pop();
		    	cursor.push();
	        		    	
		    	String keyName = outputFile.getName() + SUFFIX;
		    	String title = xmlString.substring(titleBegin, titleEnd);
		    	
		    	titleMap.put(keyName, title);
		    	
		    	replaceRefId(cursor);
	            insertResult(compjcas, cursor, outputFile);
		    }
		    
		    
	       // xml ファイルの保存
	       cursor.pop();
	       try {
	    	   XmlOptions xmlOpt = new XmlOptions();
		       XmlOptionCharEscapeMap xmlOptionCharEscapeMap = new XmlOptionCharEscapeMap();
		       xmlOptionCharEscapeMap.addMapping('\"', XmlOptionCharEscapeMap.PREDEF_ENTITY);
		       xmlOptionCharEscapeMap.addMapping('\'', XmlOptionCharEscapeMap.PREDEF_ENTITY);
		       xmlOptionCharEscapeMap.addMapping('<', XmlOptionCharEscapeMap.PREDEF_ENTITY);
		       xmlOptionCharEscapeMap.addMapping('>', XmlOptionCharEscapeMap.PREDEF_ENTITY);
		       xmlOptionCharEscapeMap.addMapping('&', XmlOptionCharEscapeMap.PREDEF_ENTITY);
		       
		       xmlOpt.setSaveSubstituteCharacters(xmlOptionCharEscapeMap);
		       
	    	   cursor.save(outputFile, xmlOpt);
	    	   
	    	   if(MATH)
	    		   replaceFile(outputFile);
	    	   
	       } catch (IOException e) {
	    	   e.printStackTrace();
	       } catch (XmlException e) {
	    	   e.printStackTrace();
	       }
	       
	       cursor.dispose();

	       // html ファイルの生成と保存
	       if(extend){ 
	    	   
	    	   String XSLT_PATH = null;
	    	   
	    	   
	    	   if(MATH && ADD_CSS)
	    		   XSLT_PATH = LIBRARY_DIR + File.separator + "torobo-htmlForMathAddCSS.xsl";
	    	   else if(MATH)
	    		   XSLT_PATH = LIBRARY_DIR + File.separator + "torobo-htmlForMath.xsl";
	    	   else if(ADD_CSS)
	    		   XSLT_PATH = LIBRARY_DIR + File.separator + "torobo-htmlAddCSS.xsl";
	    	   else
	    		   XSLT_PATH = LIBRARY_DIR + File.separator + "torobo-html.xsl";
	    	   
	    	   String prefix = "file:///";
	    	   if(XSLT_PATH.matches("[A-Z]:.*")){
	    		   XSLT_PATH = prefix + XSLT_PATH;
	    	   }
	    	   
	       
	    	   TransformerFactory trFactory = TransformerFactory.newInstance();
	    	   Transformer transformer;
	    	   
	    	   String fileName = outputFile.getName() + SUFFIX;
	    	   File outputDir = new File(XML_OUTPUTDIR + File.separator +"html");
	    	   if(!outputDir.isDirectory())
	    		   outputDir.mkdir();
	    	   
	    	   File htmlOut = new File(outputDir, fileName);
	    	   try {
	    		   if(MATH && !ADD_CSS){
	    			   copyFile(outputFile, htmlOut);
	    		   }
	    		   else{
	    			   transformer = trFactory.newTransformer(new StreamSource(XSLT_PATH));
	    			   StreamSource ss = new StreamSource(outputFile);
	    			   StreamResult sr = new StreamResult(htmlOut);
	    			   transformer.transform(ss, sr);
	    		   }
	    		   if(MATH && ADD_CSS){
	    			   replaceXhtml(htmlOut);
	    		   }
	    		   else if(!MATH){
	    			   replaceHmtlEscape(htmlOut);
	    		   }
	    		   htmlFiles.add(htmlOut);
	    		   
	    	   } catch (TransformerConfigurationException e) {
	    		   e.printStackTrace();
	    	   } catch (TransformerException e) {
	    		   e.printStackTrace();
	    	   } catch (java.lang.StackOverflowError e) {
	    		   e.printStackTrace();
	    	   } catch (Exception e) {
				e.printStackTrace();
	    	   }
	       } 
	        
		}

		// 採点結果の挿入
		private void insertResult(JCas jcas, XmlCursor cursor, File outputFile){
					
			String score = null;
			String rate = null;
			String numOfQuestion = null;
			String numOfCorrect = null;
			String component = null;
			String perfectScore = null;

System.out.println("insertresult:"+outputFile);
			ArrayList<String> componentsNameList = new ArrayList<String>();
					
			TypeSystem typeSystem = jcas.getTypeSystem();
			Type type = typeSystem.getType(ComparisonSet.class.getName());
					
			FSIterator<FeatureStructure> fsIteratorOrig = jcas.getFSIndexRepository().getAllIndexedFS(type);
			
			TreeSet<ComparisonSet> treeSet = new TreeSet<ComparisonSet>(new Comparator<ComparisonSet>() {
				@Override
				public int compare(ComparisonSet o1, ComparisonSet o2) {
					if (o1 == o2) return 0;
					if (o1.equals(o2)) return 0;
					if (o1.getGoldAnnotationGroup().equals(o2.getGoldAnnotationGroup())
							&& o1.getTestAnnotationGroup().equals(o2.getTestAnnotationGroup()))
						return 0;
					return o1.hashCode() - o2.hashCode();
				}
			});
			while(fsIteratorOrig.hasNext()){
				ComparisonSet comparisonSet = (ComparisonSet) fsIteratorOrig.next();
System.out.println("<< comparisonSet.add");				
				treeSet.add(comparisonSet);
			}
System.out.println("treeSet.size="+treeSet.size());	
					
			int count = 0;
		
			// 各解答コンポーネントの結果を追加
			Iterator<ComparisonSet> fsIterator = treeSet.iterator();
			while(fsIterator.hasNext()){
				
				ComparisonSet comparisonSet = (ComparisonSet) fsIterator.next();
				if (!(comparisonSet instanceof ComparisonSet)) {
System.out.println("not compasionSet continue");					
					continue;
				}
				
//				AnnotationGroup annotationGroup = (AnnotationGroup) comparisonSet.getDerivingAnnotationGroups().getNthElement(0);
//				comparisonSet.setTypes(annotationGroup.getTypes());
						
				AnnotationGroup goldAnnotationGroup = comparisonSet.getGoldAnnotationGroup();
				AnnotationGroup testAnnotationGroup = comparisonSet.getTestAnnotationGroup();
		
if (goldAnnotationGroup.getCollectionReaderGenerated())
System.out.println("goldAnnotationGroup.getCollectionReaderGenerated()");
if (!(goldAnnotationGroup instanceof ComparisonSet))
System.out.println("!(goldAnnotationGroup instanceof ComparisonSet)");
if (!(testAnnotationGroup instanceof ComparisonSet))
System.out.println("!(testAnnotationGroup instanceof ComparisonSet)");
if (testAnnotationGroup != goldAnnotationGroup)
System.out.println("testAnnotationGroup != goldAnnotationGroup");
else
System.out.println("testAnnotationGroup == goldAnnotationGroup");
				if( goldAnnotationGroup.getCollectionReaderGenerated() 
						&& !(goldAnnotationGroup instanceof ComparisonSet)
						&& !(testAnnotationGroup instanceof ComparisonSet)
						&& testAnnotationGroup != goldAnnotationGroup) {
System.out.println("all ok");					
					String components = goldAnnotationGroup.getComponentID()+"/"+testAnnotationGroup.getComponentID();
					if( componentsNameList.contains(components) )
						continue;
					componentsNameList.add(components);
					
					component = testAnnotationGroup.getComponentID();
					count++;
					FSArray goldAnnotations = comparisonSet.getGoldAnnotations();
					FSArray testAnnotations = comparisonSet.getTestAnnotations();
							
					if (testAnnotations == null)
						continue;
		
					HashMap<String, String> answerMap = new HashMap<String, String>();
					HashMap<String, String> processLogMap = new HashMap<String, String>();
							
					Stack<String> tokenTypeStack = new Stack<String>();
							
					String ansColumnIds = null;
					String key = null;
					String answerAT = null;
					String processLog = null;
					String id = null;
							
					TreeSet<String> answerSet = new TreeSet<String>();
							
					for (int i=0; i<testAnnotations.size(); i++) {
						Annotation ann = (Annotation) testAnnotations.get(i);

						if(ann.getType().toString().equals(AnswerColumn.class.getName())){
						}
						else if(ann.getType().toString().equals(DataAT.class.getName())){
							if(key != null && answerAT != null){
								String[] keys = ExamReader.split(key);
								String[] answers = ExamReader.split(answerAT);
								for (int j = 0; j < keys.length; j++) {
									if(j < answers.length)
										answerMap.put(keys[j], answers[j]);
									else
										answerMap.put(keys[j], answers[0]);
									processLogMap.put(keys[j], processLog);
								}
							}
							key = null;
							answerAT = null;
						}
						else if (ann.getType().toString().equals(Answer.class.getName())) {
							answerAT = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("String"));
						}
						else if(ann.getType().toString().equals(AnswerColumnID.class.getName())){
							key = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("String"));
						}
						else if(ann.getType().toString().equals(ProcessLog.class.getName())){
							processLog = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("String"));
						}
						else if(ann.getType().toString().equals(AnswerMachineType.class.getName())){
							AnswerMachineType answerMachineType = (AnswerMachineType) ann;
		
							score = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("score"));
							rate = answerMachineType.getRate();
							numOfQuestion = answerMachineType.getNumOfQuestion();
							numOfCorrect = answerMachineType.getNumOfCorrect();
							perfectScore = answerMachineType.getPerfectScore();
						}
					}
					
					if (key != null && answerAT != null) {
						String[] keys = ExamReader.split(key);
						String[] answers = ExamReader.split(answerAT);
						for (int j = 0; j < keys.length; j++) {
							if (j < answers.length)
								answerMap.put(keys[j], answers[j]);
							else
								answerMap.put(keys[j], answers[0]);
							processLogMap.put(keys[j], processLog);
						}
					}
					
					// 数学用の前処理：解答結果を入れるべき箇所に result_table タグを挿入
					HashMap<String, String> goldAnswerMap = new HashMap<String, String>();
					if (MATH) {
						String ans = null;
						
						for (int i = 0; i < goldAnnotations.size(); i++) {
							Annotation ann = (Annotation) goldAnnotations.get(i);
							if(ann.getType().getName().equals(Answer.class.getName()))
								ans = ((Answer) ann).getString();
							else if(ann.getType().getName().equals(AnswerColumnID.class.getName())){
								String[] answers = ans.split(",");
								String[] ids = ((AnswerColumnID) ann).getString().split(",");
								for (int j = 0; j < ids.length; j++) {
									if (j < answers.length)
										goldAnswerMap.put(ids[j], answers[j]);
								}
							}
						}
						
						String ids = null;
						cursor.toStartDoc();
								
						while(cursor.hasNextToken()){
							cursor.toNextToken();
									
							switch (cursor.currentTokenType().intValue()) {

							case TokenType.INT_START:
								tokenTypeStack.push(cursor.getName().toString());
								
								if (cursor.getName().toString().equals("question")) {
									if (ids != null) {
										cursor.toPrevToken();
										while (cursor.currentTokenType().intValue() == TokenType.INT_ATTR) {
											cursor.toPrevToken();
										}
										cursor.beginElement("result_table");
										cursor.insertChars(" ");
										toNextEndToken(cursor);
										cursor.toNextToken();
										ids = null;
									}
									if (cursor.getAttributeText(QName.valueOf("anscolumn_ids")) != null)
										ids = cursor.getAttributeText(QName.valueOf("anscolumn_ids"));
								}
								break;

							case TokenType.INT_END:
								if (!tokenTypeStack.empty()) {
									String tokenName = tokenTypeStack.pop();
									if(tokenName.equals("question")){
										if(ids != null){
											cursor.beginElement("result_table");
											cursor.insertChars(" ");
											toNextEndToken(cursor);
											ids = null;
										}
									}
								}
								break;
								
							default:
								break;
							}
						}
					}
					cursor.pop();
					cursor.push();
					
					// 解答結果を挿入
					while(cursor.hasNextToken()){
					
						cursor.toNextToken();
					
						// 数学用の処理
						if (MATH) {
							switch (cursor.currentTokenType().intValue()) {
							
							case TokenType.INT_START:
								
								tokenTypeStack.push(cursor.getName().toString());
						
								if (cursor.getName().toString().equals("question")) {
									if (cursor.getAttributeText(QName.valueOf("anscolumn_ids")) != null) {
										ansColumnIds = cursor.getAttributeText(QName.valueOf("anscolumn_ids"));
									}
								} else if (cursor.getName().toString().equals("result_table")) {
									if (ansColumnIds != null) {
										String[] ids = ansColumnIds.split(",");
										cursor.toNextToken();
										cursor.beginElement("result_tr");
										cursor.insertAttributeWithValue("id", component);
										cursor.beginElement("result_th");
										cursor.insertChars(component);
										toNextEndToken(cursor);
										for (int i = 0; i < ids.length; i++) {
											cursor.beginElement("result_td");
											if(answerMap.get(ids[i]) != null && answerMap.get(ids[i]).equals(goldAnswerMap.get(ids[i])))
												cursor.insertAttributeWithValue("ra", "yes");
											else
												cursor.insertAttributeWithValue("ra", "no");
											cursor.insertChars(answerMap.get(ids[i]));
											toNextEndToken(cursor);
										}
										toNextEndToken(cursor);
										ansColumnIds = null;
									}
								}
								break;
								
							default:
								break;
							}
						} else {	// 数学以外の処理
							switch (cursor.currentTokenType().intValue()) {
						
							case TokenType.INT_START:
							
								tokenTypeStack.push(cursor.getName().toString());
								
								if (cursor.getName().toString().equals("choices")) {
									answerSet.clear();
									if (cursor.getAttributeText(QName.valueOf("anscol")) != null) {
										String[] ids = cursor.getAttributeText(QName.valueOf("anscol")).split(" ");
										id = ids[0];
										for (int j = 0; j < ids.length; j++) {
											if(answerMap.get(ids[j]) != null)
												answerSet.add(answerMap.get(ids[j]));
										}
									} else
										id = null;
									cursor.toNextToken();
									cursor.insertAttributeWithValue("component" + String.valueOf(count), component);
								} else if(cursor.getName().toString().equals("choice")) {
									
									if (cursor.getAttributeText(QName.valueOf("ra")).equals("yes")) {
										if (cursor.getAttributeText(QName.valueOf("ansnum")) != null
												&& answerSet.contains(cursor.getAttributeText(QName.valueOf("ansnum")))) {
											cursor.toNextToken();
											cursor.insertAttributeWithValue("ra" + String.valueOf(count), "systemYes");
											cursor.insertAttributeWithValue("component" + String.valueOf(count), component);
										} else {
											cursor.toNextToken();
											cursor.insertAttributeWithValue("ra" + String.valueOf(count), "yes");
											cursor.insertAttributeWithValue("component" + String.valueOf(count), component);
										}
									} else {
										if (cursor.getAttributeText(QName.valueOf("ansnum")) != null
												&& answerSet.contains(cursor.getAttributeText(QName.valueOf("ansnum")))){
											cursor.toNextToken();
											cursor.insertAttributeWithValue("ra" + String.valueOf(count), "systemNo");
											cursor.insertAttributeWithValue("component" + String.valueOf(count), component);
										} else {
											cursor.toNextToken();
											cursor.insertAttributeWithValue("ra" + String.valueOf(count), "no");
											cursor.insertAttributeWithValue("component" + String.valueOf(count), component);
										}
									}
								}
								break;
											
							case TokenType.INT_END:
								if ( !tokenTypeStack.empty() ) {
									String tokenName = tokenTypeStack.pop();
									if ( tokenName.equals("choices") ) {
										cursor.toNextToken();
										cursor.beginElement("process_log");
										cursor.insertAttributeWithValue("id", component);
										if (id != null)
											cursor.insertChars(processLogMap.get(id));
										cursor.toNextToken();
									}
								}
								break;
							}
						}
					}
					cursor.pop();
					cursor.push();
					cursor.toFirstChild();
					cursor.toNextToken();		    	
							
					cursor.insertAttributeWithValue("component" + String.valueOf(count), component);
					cursor.insertAttributeWithValue("score" + String.valueOf(count), score);
					cursor.insertAttributeWithValue("rate" + String.valueOf(count), rate);
					cursor.insertAttributeWithValue("numOfCorrect" + String.valueOf(count), numOfCorrect);
					cursor.insertAttributeWithValue("numOfQuestion" + String.valueOf(count), numOfQuestion);
					cursor.insertAttributeWithValue("perfectScore" + String.valueOf(count), perfectScore);
							
					File directory = new File(XML_OUTPUTDIR + File.separator + component);
					if (!directory.exists())
						directory.mkdir();
					
					// 各解答機コンポーネントの正答表を保存	
					File ansTableFile = new File(directory, outputFile.getName());
					try {
						if (testAnnotations.size() > 0)
							org.kachako.components.centerexam.answertable.AnswerTableWriter.writeAnswerTable(
									testAnnotations, ansTableFile, String.valueOf(perfectScore), score,rate, numOfCorrect, numOfQuestion);
						// index.html 用に各解答機コンポーネントの結果を saveResult に保存
						saveResult.setResult(outputFile.getName() + SUFFIX + component, score, rate, numOfCorrect, numOfQuestion, perfectScore);
						if (componentList.size() == 0 || !componentList.contains(component) )
							componentList.add(component);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			cursor.pop();
			cursor.push();
			cursor.toFirstChild();
			cursor.toNextToken();
			cursor.insertAttributeWithValue("numOfComponent", String.valueOf(count));
			numberOfCorrect = count;
		
			HashMap<String, String> answerMap = new HashMap<String, String>();
			type = typeSystem.getType(AnswerTableAnnotationList.class.getName()); 
			FSIterator<?> fsAnn = jcas.getFSIndexRepository().getAllIndexedFS(type);
			String anscolumn_ids = null;
			
			if(MATH && fsAnn.hasNext()){
				AnswerTableAnnotationList atAnnList = (AnswerTableAnnotationList) fsAnn.next();
				FSArray fsArray = atAnnList.getBeginList();
		//		ArrayList<Annotation> annList = new ArrayList<Annotation>();
				String answer = null;
				for (int i = 0; i < fsArray.size(); i++) {
					Annotation ann = (Annotation) fsArray.get(i);
					String annName = ann.getType().getName();
					if(annName.equals(Answer.class.getName())){
						answer = ((Answer) ann).getString();
					}
					if(annName.equals(AnswerColumnID.class.getName())){
						String[] answers = answer.split(",");
						String[] ids = ((AnswerColumnID) ann).getString().split(",");
						for (int j = 0; j < ids.length; j++) {
							if(j < answers.length)
								answerMap.put(ids[j], answers[j]);
						}
					}
				}
			}
			cursor.toStartDoc();
			boolean flag = false;
		
			while(cursor.hasNextToken()){
				cursor.toNextToken();
				
				if(MATH){
					
					switch (cursor.currentTokenType().intValue()) {
						
					case TokenType.INT_START:
						
						if(cursor.getName().toString().equals("exam")){
							flag = true;
						}
								
						else if(cursor.getName().toString().equals("question")){
						    anscolumn_ids = cursor.getAttributeText(QName.valueOf("anscolumn_ids"));
						}
						
						else if(cursor.getName().toString().equals("result_table")){
		//					System.out.println(anscolumn_ids);
							if(anscolumn_ids != null && !anscolumn_ids.equals("")){
								String ids[] = anscolumn_ids.split(",");
								cursor.toNextToken();
								cursor.beginElement("result_tr");
								cursor.insertAttributeWithValue("id", "correct");
								cursor.beginElement("result_th");
								cursor.insertChars("解答欄");
								toNextEndToken(cursor);
								for (int i = 0; i < ids.length; i++) {
									cursor.beginElement("result_td");
									cursor.insertChars(ids[i].substring(1));
									toNextEndToken(cursor);
								}
								toNextEndToken(cursor);
								cursor.beginElement("result_tr");
								cursor.insertAttributeWithValue("id", "correct");
								cursor.beginElement("result_th");
								cursor.insertChars("正答");
								toNextEndToken(cursor);
								for (int i = 0; i < ids.length; i++) {
									cursor.beginElement("result_td");
									cursor.insertChars(answerMap.get(ids[i]));
									toNextEndToken(cursor);
								}
								anscolumn_ids = null; 
							}
							else{
								anscolumn_ids = null;
							}
						}
						
						break;
						
						case TokenType.INT_TEXT:
						
							if(ADD_CSS && flag){
								cursor.removeChars(cursor.getChars().length());
								flag = false;
							}
								
							break;
									
						default:
							break;
					}	
				}	
				
				else{
					switch( cursor.currentTokenType().intValue() ){
					
					case TokenType.INT_START:		
						if(cursor.getName().toString().equals("exam")){
							flag = true;
						}
						else if(cursor.getName().toString().equals("choices")){
							cursor.toNextToken();
							cursor.insertAttributeWithValue("components", String.valueOf(count));
						}
					case TokenType.INT_TEXT:
						
						if(ADD_CSS && flag){
							cursor.removeChars(cursor.getChars().length());
							flag = false;
						}
						
						break;
						
					default:
						break;
					}
				}
			}
		}
	}
		
	// math タグの xmlns の後に余計な :m が挿入されるので差し替え
	private static void replaceFile(File file) {
		
		try {
			StringBuilder sb = new StringBuilder();
			String line = null;
			String replaceText = null;
				
			BufferedReader br = new BufferedReader(new FileReader(file));
			while( (line = br.readLine()) != null ){
				sb.append(line + "\r\n");
			}
			br.close();
				
			replaceText = sb.toString().replaceAll("s:m", "s");
			replaceText = replaceText.replaceAll("m:m", "m");
			replaceText = replaceText.replaceAll("</mmultiscripts><none/>", "<none/></mmultiscripts>");
			FileWriter fw = new FileWriter(file);
			fw.write(replaceText);
			fw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		
	private static void replaceXhtml(File file){
			
		try {
			StringBuilder sb = new StringBuilder();
			String line = null;
			String replaceText = null;
				
			BufferedReader br = new BufferedReader(new FileReader(file));
			while( (line = br.readLine()) != null ){
				sb.append(line + "\r\n");
			}
			br.close();
				
			// m:math 等の名前空間 m: を削除
			replaceText = sb.toString().replaceAll("m:", "");
			FileWriter fw = new FileWriter(file);
			fw.write(replaceText);
			fw.close();
				
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

		
	// htmlエスケープを変換
	private static void replaceHmtlEscape(File in){
			
		try {
			StringBuilder sb = new StringBuilder();
			String line = null;
			String replaceText = null;
				
			BufferedReader br = new BufferedReader(new FileReader(in));
			while( (line = br.readLine()) != null ){
				sb.append(line + "\r\n");
			}
			br.close();
				
			replaceText = sb.toString().replaceAll("&amp;", "&");
			replaceText = replaceText.replaceAll("&lt;", "<");
			replaceText = replaceText.replaceAll("&gt;", ">");
			replaceText = replaceText.replaceAll("&quot;", "\"");
				
				
			FileWriter fw = new FileWriter(in);
			fw.write(replaceText);
			fw.close();
				
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// cursor を次の token に進めるかの判定
	public static int checkToNextToken(List<Annotation> fsTypeEndList, XmlCursor cursor, int count, Stack<Annotation> annStack, int typeIndex) {
		Annotation buf;
		if (!annStack.isEmpty()) {
			buf = annStack.pop();
			if(count == buf.getEnd()  && fsTypeEndList.get(typeIndex).getType().getName().equals(buf.getType().getName())){
				cursor.toNextToken();
				typeIndex++;
			} else if (count > buf.getEnd()) {
				cursor.toNextToken();
				typeIndex++;
			} else{
				annStack.push(buf);
			}
		}
		return typeIndex;
	}

	// 空の xml ファイルの生成
	private static File createXmlFile(JCas jcas, String outputDirName) {
		//get XML file names
		FSIterator<Annotation> fsIte = jcas.getAnnotationIndex(SourceDocumentInformation.type).iterator();
		if(!fsIte.hasNext()) {
			System.out.println("SourceDocumentInformation is null");
			return null;
		}
		
		SourceDocumentInformation sourceDocInfo = (SourceDocumentInformation) fsIte.next();
		String outputXmlFileName;
		try {
			outputXmlFileName = new URI(sourceDocInfo.getUri()).getPath();
			File fileName = new File(outputXmlFileName);
			File inputFile = new File(outputDirName + File.separator + "xml", fileName.getName());
			File directory = inputFile.getParentFile();
			    
			directory.mkdirs();
			        
			if(inputFile.exists()){
				inputFile.delete();
			}
			try {
				inputFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return inputFile;
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}


	// ファイルコピー用
	public static void copyFile(File in, File out) {
		
		FileChannel sourceChannel = null;
		FileChannel destinationChannel = null;
		try {
			sourceChannel = new FileInputStream(in).getChannel();
			destinationChannel = new FileOutputStream(out).getChannel();
			sourceChannel.transferTo(0, sourceChannel.size(), destinationChannel);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (sourceChannel != null) { 
				try {
					sourceChannel.close();
				} catch (IOException e) { }
			}
			if (destinationChannel != null) {
				try {
					destinationChannel.close();
				} catch (IOException e) { }
			}
		}
	}
		
		
	// マウスオーバーで参照先の表示用
	private static void replaceRefId(XmlCursor cursor){
			
		HashMap<String, String> refMAP = new HashMap<String, String>();
			
		String id = null;
		cursor.push();
		QName qId = QName.valueOf("id");
		QName qTarget = QName.valueOf("target");
			
		//ID 及びテキストを取得して HashMap に格納
		while (!cursor.toNextToken().isNone()) {
				
			switch (cursor.currentTokenType().intValue()) {
			case TokenType.INT_START:
				if (!cursor.getName().toString().equals("question") && !cursor.getName().toString().equals("ansColumn") && cursor.getAttributeText(qId) != null){
					id = cursor.getAttributeText(qId);
					refMAP.put(id, cursor.getTextValue());
				}
				break;
			}
		}
		cursor.pop();
			
		//<ref> の target ID を参照先テキストに置き換え
		while (!cursor.toNextToken().isNone()) {
			switch (cursor.currentTokenType().intValue()){
			case TokenType.INT_START:
				if(cursor.getName().toString().equals("ref")){
					id = refMAP.get(cursor.getAttributeText(qTarget));
					if(id != null)
						cursor.setAttributeText(qTarget, id);
				}
				break;
			}
		}
	}
		
	// 終了タグの次のトークンまでカーソルを進める
	private static void toNextEndToken(XmlCursor cursor) {
		while (cursor.currentTokenType().intValue() != TokenType.INT_END) {
			cursor.toNextToken();
		}
		cursor.toNextToken();
	}
}
