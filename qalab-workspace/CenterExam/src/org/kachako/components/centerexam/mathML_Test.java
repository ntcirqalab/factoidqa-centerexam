package org.kachako.components.centerexam;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.kachako.components.centerexam.ExamWriterMultiple.ExamXmlWriter;

public class mathML_Test extends JCasAnnotator_ImplBase {
	
	public final static String PARAMS_OUTPUTDIR = "OutputDirectory";
	static String OUTPUTDIR;
	
	@Override
	public void initialize(UimaContext aContext)
			throws ResourceInitializationException {
		
		OUTPUTDIR = aContext.getConfigParameterValue(PARAMS_OUTPUTDIR).toString().trim();
		
		super.initialize(aContext);
		
		examXmlWriter = new ExamXmlWriter();
		
	}

	
	 private ExamXmlWriter examXmlWriter;
	
	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		// TODO Auto-generated method stub
		
		
		FSIterator<Annotation> fsIterator = jcas.getAnnotationIndex(SourceDocumentInformation.type).iterator();
		
		if(fsIterator.hasNext()){
			SourceDocumentInformation srcDocInfo = (SourceDocumentInformation) fsIterator.next();
			try {
				File directory = new File(OUTPUTDIR);
				String fileName= new File(new URI(srcDocInfo.getUri())).getName();
				File outputFile = new File(directory, fileName);
				
				
				examXmlWriter.writeCenterExamXML(jcas, outputFile);
				
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}
	}

}
