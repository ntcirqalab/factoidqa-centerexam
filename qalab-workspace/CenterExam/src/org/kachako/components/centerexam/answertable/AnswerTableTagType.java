package org.kachako.components.centerexam.answertable;

import org.apache.uima.cas.Type;
import org.kachako.types.centerexam.answertable.*;

public class AnswerTableTagType {
	
public String getBeginTagType(Type annotationType){
		
		if(annotationType.toString().equals(AnswerTable.class.getName())){
			return "answerTable";
		}
		else if(annotationType.toString().equals(Answer.class.getName())){
			return "answer";
		}
		else if(annotationType.toString().equals(AnswerColumn.class.getName())){
			return "answer_column";
		}
		else if(annotationType.toString().equals(AnswerColumnID.class.getName())){
			return "anscolumn_ID";
		}
		else if(annotationType.toString().equals(AnswerStyle.class.getName())){
			return "answer_style";
		}
		else if(annotationType.toString().equals(AnswerType.class.getName())){
			return "answer_type";
		}
		else if(annotationType.toString().equals(DataAT.class.getName())){
			return "data";
		}
		else if(annotationType.toString().equals(Knowledge.class.getName())){
			return "knowledge_type";
		}
		else if(annotationType.toString().equals(QuestionAT.class.getName())){
			return "question";
		}
		else if(annotationType.toString().equals(QuestionID.class.getName())){
			return "question_ID";
		}
		else if(annotationType.toString().equals(Score.class.getName())){
			return "score";
		}
		else if(annotationType.toString().equals(Section.class.getName())){
			return "section";
		}
		else if(annotationType.toString().equals(ProcessLog.class.getName())){
			return "process_log";
		}
		else if(annotationType.toString().equals(Option.class.getName())){
			return "option";
		}
		else if(annotationType.toString().equals(Confidence.class.getName())){
			return "confidence";
		}
		return null;
	}

}
