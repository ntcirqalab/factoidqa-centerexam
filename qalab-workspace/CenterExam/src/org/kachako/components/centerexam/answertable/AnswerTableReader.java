package org.kachako.components.centerexam.answertable;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.namespace.QName;

//import javax.xml.namespace.QName;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.collection.CollectionReader_ImplBase;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceConfigurationException;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Progress;
import org.apache.uima.util.ProgressImpl;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlCursor.TokenType;
import org.kachako.types.centerexam.answertable.*;

public class AnswerTableReader extends CollectionReader_ImplBase {
	
	public static final String PARAM_INPUTDIR = "InputDirectory"; 
	public static final String PARAM_SUBDIR = "BrowseSubDirectories";
	
	private ArrayList<File> mFiles;
	private Boolean mRecursive;
	private int mCorrentIndex = 0;

	
	public void initialize() throws ResourceInitializationException{
		
		File directory = new 
		File(((String) getConfigParameterValue(PARAM_INPUTDIR)).trim());
		mRecursive = (Boolean) getConfigParameterValue(PARAM_SUBDIR);
		if(null == mRecursive){
			mRecursive = Boolean.FALSE;
		}
		mCorrentIndex = 0;
		if(!directory.exists() || !directory.isDirectory()){
			throw new ResourceInitializationException(ResourceConfigurationException.DIRECTORY_NOT_FOUND,
					new Object[]{ PARAM_INPUTDIR, this.getMetaData().getName(), directory.getPath() });
		}
		mFiles = new ArrayList<File>();
		addFilesFromDir(directory);
		
		}
	
	private void addFilesFromDir(File dir){
		File[] files = dir.listFiles();
		for(int i=0; i<files.length; i++){
			if(!files[i].isDirectory()) {
				mFiles.add(files[i]);
			} else if(mRecursive){
				addFilesFromDir(files[i]);
			}
		}	
	}	

@Override
public void getNext(CAS aCAS) throws IOException, CollectionException {
	
	JCas jcas;
	try {
		jcas = aCAS.getJCas();
		readAnswerTable(jcas, mFiles.get(mCorrentIndex++), null, null); 
	} catch (CASException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

public static void readAnswerTable(JCas jcas, File file){
	try {
		readAnswerTable(jcas, file , null, null);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

public static void readAnswerTable(JCas jcas, File file, String rangeOfOptions, String numOfOptions) throws IOException{

	Stack<Annotation> jCasStack = new Stack<Annotation>();
	StringBuilder CenterExamDocument = new StringBuilder();
	int begin = 0;
	int end = 0;
	String ansColumn = null;
	
	ArrayList<Annotation> beginList = new ArrayList<Annotation>();
	ArrayList<Annotation> endList = new ArrayList<Annotation>();
	Annotation ann;
	int score = 0;
	
	// xml から cas へ書き出し
	try {
		
		noNamespace.AnswerTableDocument ansTable = (noNamespace.AnswerTableDocument) noNamespace.AnswerTableDocument.Factory.parse(file);
		XmlCursor cursor = ansTable.newCursor();
		
		
		while(!cursor.toNextToken().isNone()){
			
			switch(cursor.currentTokenType().intValue()){
			
			// Annotation の生成と開始位置のセット
			case TokenType.INT_START:
				begin = CenterExamDocument.length();
			
				// answerTable tag
				if(cursor.getName().toString().equals("answerTable")){
					AnswerTable answerTableFS = new AnswerTable(jcas);
					beginList.add(answerTableFS);
					
					answerTableFS.setBegin(begin);
					answerTableFS.setString(removeScriptTag(cursor.getTextValue()));
					answerTableFS.addToIndexes(jcas);
					
					answerTableFS.setFile(removeScriptTag(cursor.getAttributeText(QName.valueOf("filename"))));
					answerTableFS.setSelected(removeScriptTag(cursor.getAttributeText(QName.valueOf("selected"))));
					answerTableFS.setRangeOfOptions(removeScriptTag(rangeOfOptions));
					answerTableFS.setNumOfOptions(removeScriptTag(numOfOptions));
					
					jCasStack.push(answerTableFS);					
					
				}
				// answer tag
				else if(cursor.getName().toString().equals("answer")){
					Answer answerFS = new Answer(jcas);
					beginList.add(answerFS);
					
					answerFS.setBegin(begin);
					answerFS.setString(removeScriptTag(cursor.getTextValue()));
					answerFS.setAnsColumn(removeScriptTag(ansColumn));
					answerFS.addToIndexes(jcas);
					
					jCasStack.push(answerFS);
					
				}
				// answer_column tag
				else if(cursor.getName().toString().equals("answer_column")){
					AnswerColumn answerColumnFS = new AnswerColumn(jcas);
					beginList.add(answerColumnFS);
					
					answerColumnFS.setBegin(begin);
					answerColumnFS.setString(removeScriptTag(cursor.getTextValue()));
					answerColumnFS.addToIndexes(jcas);
					
					jCasStack.push(answerColumnFS);
					
					ansColumn = cursor.getTextValue();
					
				}
				// anscolumn_ID tag
				else if(cursor.getName().toString().equals("anscolumn_ID")){
					AnswerColumnID answerColumnIDFS = new AnswerColumnID(jcas);
					beginList.add(answerColumnIDFS);
					
					answerColumnIDFS.addToIndexes(jcas);
					answerColumnIDFS.setString(removeScriptTag(cursor.getTextValue()));
					answerColumnIDFS.setBegin(begin);
					answerColumnIDFS.setTF(false);
					
					jCasStack.push(answerColumnIDFS);

				}
				// answer_style tag
				else if(cursor.getName().toString().equals("answer_style")){
					AnswerStyle answerStyleFS = new AnswerStyle(jcas);
					beginList.add(answerStyleFS);
					
					answerStyleFS.setBegin(begin);
					answerStyleFS.setString(removeScriptTag(cursor.getTextValue()));
					answerStyleFS.addToIndexes(jcas);
					
					jCasStack.push(answerStyleFS);
					
				}
				// answer_type tag
				else if(cursor.getName().toString().equals("answer_type")){
					AnswerType answerTypeFS = new AnswerType(jcas);
					beginList.add(answerTypeFS);
					
					answerTypeFS.setBegin(begin);
					answerTypeFS.setString(removeScriptTag(cursor.getTextValue()));
					answerTypeFS.addToIndexes(jcas);
					
					jCasStack.push(answerTypeFS);
					
				}
				// data tag
				else if(cursor.getName().toString().equals("data")){
					DataAT dataFS = new DataAT(jcas);
					beginList.add(dataFS);
					
					dataFS.setBegin(begin);
					dataFS.setString(removeScriptTag(cursor.getTextValue()));
					dataFS.addToIndexes(jcas);
					
					jCasStack.push(dataFS);
					
				}
				// knowledge_type tag
				else if(cursor.getName().toString().equals("knowledge_type")){
					Knowledge knowledgeFS = new Knowledge(jcas);
					beginList.add(knowledgeFS);
					
					knowledgeFS.setBegin(begin);
					knowledgeFS.setString(removeScriptTag(cursor.getTextValue()));
					knowledgeFS.addToIndexes(jcas);
					
					jCasStack.push(knowledgeFS);
					
				}
				// question tag
				else if(cursor.getName().toString().equals("question")){
					QuestionAT questionFS = new QuestionAT(jcas);
					beginList.add(questionFS);
					
					questionFS.setBegin(begin);
					questionFS.setString(removeScriptTag(cursor.getTextValue()));
					questionFS.addToIndexes(jcas);
					
					jCasStack.push(questionFS);
					
				}
				// question_ID tag
				else if(cursor.getName().toString().equals("question_ID")){
					QuestionID questionIDFS = new QuestionID(jcas);
					beginList.add(questionIDFS);
					
					questionIDFS.setBegin(begin);
					questionIDFS.setString(removeScriptTag(cursor.getTextValue()));
					questionIDFS.addToIndexes(jcas);
					
					jCasStack.push(questionIDFS);
					
				}
				// score tag
				else if(cursor.getName().toString().equals("score")){
					Score scoreFS = new Score(jcas);
					beginList.add(scoreFS);
					
					scoreFS.setBegin(begin);
					scoreFS.setString(removeScriptTag(cursor.getTextValue()));
					scoreFS.addToIndexes();
					jCasStack.push(scoreFS);
					
//					score += calc(scoreFS.getString());
					
				}
				// section tag
				else if(cursor.getName().toString().equals("section")){
					Section sectionFS = new Section(jcas);
					beginList.add(sectionFS);
					
					sectionFS.setBegin(begin);
					sectionFS.setString(removeScriptTag(cursor.getTextValue()));
					sectionFS.addToIndexes(jcas);
					
					jCasStack.push(sectionFS);
					
				}
				// process_log tag
				else if(cursor.getName().toString().equals("process_log")){
					ProcessLog processLogFS = new ProcessLog(jcas);
					beginList.add(processLogFS);
					
					processLogFS.setBegin(begin);
					processLogFS.setString(removeScriptTag(cursor.getTextValue()));
					processLogFS.addToIndexes(jcas);
					
					jCasStack.push(processLogFS);
					
				}
				// option tag
				else if(cursor.getName().toString().equals("option")){
					Option optionFS = new Option(jcas);
					beginList.add(optionFS);
					
					optionFS.setBegin(begin);
					optionFS.setSection_ID(removeScriptTag(cursor.getAttributeText(QName.valueOf("section_ID"))));
					optionFS.setAnscolumns(removeScriptTag(cursor.getAttributeText(QName.valueOf("anscolumns"))));
					optionFS.addToIndexes();
					
					jCasStack.push(optionFS);
				}
				// confidence tag
				else if(cursor.getName().toString().equals("confidence")){
					Confidence confidenceFS = new Confidence(jcas);
					beginList.add(confidenceFS);
					
					confidenceFS.setBegin(begin);
					confidenceFS.setString(removeScriptTag(cursor.getTextValue()));
					confidenceFS.addToIndexes();
					
					jCasStack.push(confidenceFS);
				}
				break;
				
			// テキストのセット
			case TokenType.INT_TEXT:
				// XSS対策のため <script> か </script> があれば削除
				CenterExamDocument.append(removeScriptTag(cursor.getChars()));
				break;
				
			// 終了位置のセット	
			case TokenType.INT_END:
				end = CenterExamDocument.length();
				if(!jCasStack.empty()){
					ann = ((Annotation) jCasStack.pop());
					ann.setEnd(end);
					endList.add(ann);
				}
			default:
				break;
				
			}
						
		}
//		System.out.println(score);
		cursor.dispose();
		
		AnswerMachineType answerMachineType = new AnswerMachineType(jcas);
		answerMachineType.addToIndexes();
		
		AnswerTableAnnotationList annListFS = new AnswerTableAnnotationList(jcas);
		annListFS.addToIndexes();
		AnswerTableEndList endListFS = new AnswerTableEndList(jcas);
		endListFS.addToIndexes();
		
		FSArray fsBegin = new FSArray(jcas, beginList.size());
		FSArray fsEnd = new FSArray(jcas, endList.size());
		
//		System.out.println(beginList.size());
		
		for (int i = 0; i < beginList.size(); i++) {
			
			fsBegin.set(i, beginList.get(i));
			fsEnd.set(i, endList.get(i));
			
		}
		
		annListFS.setBeginList(fsBegin);
		endListFS.setEndList(fsEnd);
		
	} catch (XmlException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

@Override
public void close() throws IOException {
	// TODO Auto-generated method stub
	
}

@Override
public Progress[] getProgress() {
	// TODO Auto-generated method stub

	return new Progress[]{ new ProgressImpl(mCorrentIndex, mFiles.size(), Progress.ENTITIES) };
}

@Override
public boolean hasNext() throws IOException, CollectionException {
	// TODO Auto-generated method stub
	return mCorrentIndex < mFiles.size();
}

// XSS対策として script タグを削除
private static String removeScriptTag(String str){
	if(str == null)
		return "";
	String res = str.replaceAll("<script>", "");
	res = res.replaceAll("</script>", "");
	res = res.replaceAll("<SCRIPT>", "");
	res = res.replaceAll("</SCRIPT>", "");
	return res;
}

private static int calc(String str){
	
	int score = 0;
	String args[] = null;
	final String RANDOM_ORDER = "\\|";
	final String MULTIPLE_CORRECT ="\\|\\|";
	final String EXACT_MATCH_ORDER =",";
	
	Matcher exactMatcher = Pattern.compile(EXACT_MATCH_ORDER).matcher(str);
	Matcher multipleMatcher = Pattern.compile(MULTIPLE_CORRECT).matcher(str);
	Matcher randomMatcher = Pattern.compile(RANDOM_ORDER).matcher(str);
	
	if(exactMatcher.find())
		args = str.split(EXACT_MATCH_ORDER);
	else if(multipleMatcher.find())
		args = str.split(MULTIPLE_CORRECT);
	else if(randomMatcher.find())
		args = str.split(RANDOM_ORDER);
	if(args != null){
		for (int i = 0; i < args.length; i++) {
			score += Integer.valueOf(args[0]);
		}
	}
	else{
		score = Integer.valueOf(str);
	}
	return score;
}


}
