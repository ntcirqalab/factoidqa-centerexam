package org.kachako.components.centerexam.answertable;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.Type;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.kachako.types.centerexam.answertable.Answer;
import org.kachako.types.centerexam.answertable.AnswerColumn;
import org.kachako.types.centerexam.answertable.AnswerColumnID;
import org.kachako.types.centerexam.answertable.AnswerStyle;
import org.kachako.types.centerexam.answertable.AnswerTable;
import org.kachako.types.centerexam.answertable.AnswerTableAnnotationList;
import org.kachako.types.centerexam.answertable.AnswerType;
import org.kachako.types.centerexam.answertable.DataAT;
import org.kachako.types.centerexam.answertable.Knowledge;
import org.kachako.types.centerexam.answertable.QuestionAT;
import org.kachako.types.centerexam.answertable.QuestionID;
import org.kachako.types.centerexam.answertable.Score;
import org.kachako.types.centerexam.answertable.Section;
import org.kachako.types.centerexam.AnsColumn;
import org.kachako.types.centerexam.Question;
import org.kachako.types.centerexam.TagList;

import com.ibm.icu.text.Transliterator;

import au.com.bytecode.opencsv.CSVReader;


public class CSV2AnswerTableForMath extends JCasAnnotator_ImplBase {
	
	public static final String PARAM_CSV_DIR = "CSVDirectory";
	public static final String PARAM_OUTPUT_DIR = "OutputDirectory";
	
	static String CSV_DIR;
	static String OUTPUT_DIR;
	
	 ArrayList<String> casAnswerIdList;
	 ArrayList<String> csvAnswerIdList;
	 int idIndex;
	
	 HashMap<String, File> CSVFileMap;
	 HashMap<String, String> sectionMap;
	 HashMap<String, String> questionMap;
	 HashMap<String, String> answerMap;
	 HashMap<String, String> scoreMap;
	
	public static final String EXACT_AND_MULTIPLE = "(\\d+,)+\\d\\|\\|";
	public static final String RANDOM_ORDER = "\\|";
	public static final String MULTIPLE_CORRECT ="\\|\\|";
	public static final String EXACT_MATCH_ORDER =",";
	public static final String randomOrder = "|";
	public static final String multilpleCorrect = "||";
	public static final String exactMatchOrder =",";
	
	 Pattern exactAndMultiplePattern;
	 Pattern randomPattern;
	 Pattern multiplePattern;
	 Pattern exactPattern;

	
	
	@Override
	public void initialize(UimaContext aContext)
			throws ResourceInitializationException {
		
		CSV_DIR = aContext.getConfigParameterValue(PARAM_CSV_DIR).toString().trim();
		OUTPUT_DIR = aContext.getConfigParameterValue(PARAM_OUTPUT_DIR).toString().trim();
		
		CSVFileMap = new HashMap<String, File>();
		
		exactAndMultiplePattern = Pattern.compile(EXACT_AND_MULTIPLE);
		randomPattern = Pattern.compile(RANDOM_ORDER);
		multiplePattern = Pattern.compile(MULTIPLE_CORRECT);
		exactPattern = Pattern.compile(EXACT_MATCH_ORDER);
		
				
		File outputDir = new File(OUTPUT_DIR);
		if (!outputDir.exists()){
			outputDir.mkdirs();
		}
		
		File directory = new File(CSV_DIR);
		if(directory.isDirectory()){
			File csvFiles[] = directory.listFiles();
			for (int i = 0; i < csvFiles.length; i++) {
				File file = csvFiles[i];
				if(file.canRead() && file.getName().endsWith(".csv")){
					int index = file.getName().lastIndexOf(".");
					CSVFileMap.put(file.getName().substring(0, index), csvFiles[i]);
//					CSVFileMap.put(file.getName().substring(0, 7), csvFiles[i]);
				}
			}
		}
		// TODO Auto-generated method stub
		super.initialize(aContext);
	}
	
	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		// TODO Auto-generated method stub
		
		casAnswerIdList = new ArrayList<String>();
		csvAnswerIdList = new ArrayList<String>();
		idIndex = 0;
		sectionMap = new HashMap<String, String>();
		questionMap = new HashMap<String, String>();
		answerMap = new HashMap<String, String>();
		scoreMap = new HashMap<String, String>();
		
		ArrayList<Annotation> fsList = new ArrayList<Annotation>();
		ArrayList<Annotation> ansTableList = new ArrayList<Annotation>();
		
		String examName = null;
		String fileName = null;
		String examDocument = jcas.getDocumentText();
		String saveFileName = null;
			
		FSIterator<Annotation> fsIterator = jcas.getAnnotationIndex(SourceDocumentInformation.type).iterator();
		if (fsIterator.hasNext()){
			SourceDocumentInformation sourceDocInfo = (SourceDocumentInformation) fsIterator.next();
			try {
				File file = new File(new URI(sourceDocInfo.getUri()).getPath());
				int index = file.getName().lastIndexOf(".");
				examName = file.getName().substring(0, index);
				fileName = file.getName().substring(0, index);
//				examName = file.getName().substring(0,7);
				System.out.println(examName);
//				saveFileName = file.getName() + ".xml";
				saveFileName = file.getName();
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		TypeSystem typeSystem = jcas.getTypeSystem();
		Type type = typeSystem.getType(TagList.class.getName());
		FSIterator<FeatureStructure> fsIte= jcas.getFSIndexRepository().getAllIndexedFS(type);
		
		TagList tagList =  (TagList) fsIte.next();
		FSArray fsArray = tagList.getAnnotationList();
		
		for (int i = 0; i < fsArray.size(); i++) {
			fsList.add((Annotation) fsArray.get(i));
		}
		
		String answerType = null;
		String answerStyle = null;
		String answerColumn = null;
		String knowledgeType = null;
		String questionId = null;
		
		HashMap<String, String> answerTypeMap = new HashMap<String, String>();
		HashMap<String, String> answerStyleMap = new HashMap<String, String>();
		HashMap<String, String> answerColumnMap = new HashMap<String, String>();
		HashMap<String, String> knowledgeTypeMap = new HashMap<String, String>();
		HashMap<String, String> questionIDMap = new HashMap<String, String>();
		
		Transliterator tr = Transliterator.getInstance("Fullwidth-Halfwidth");
		
		
		for (int i = 0; i < fsList.size(); i++) {
			
			Annotation annotation = (Annotation) fsList.get(i);
			String annName = annotation.getType().getName();
			
			if( annName.equals(Question.class.getName()) ){
				answerType = ( (Question) annotation ).getAnswer_type();
				answerStyle = ( (Question) annotation ).getAnswer_style();
				knowledgeType = ( (Question) annotation ).getKnowledge_type();
				questionId = ( (Question) annotation ).getId();
				
				String anscolumn_ids = ( (Question) annotation ).getAnscolumn_ids();
				if(anscolumn_ids != null && !anscolumn_ids.equals("")){
					String ids[] = anscolumn_ids.split(",");
					for (int j = 0; j < ids.length; j++) {
						String id = ids[j];
						casAnswerIdList.add(id);
						answerTypeMap.put(id, answerType);
						answerStyleMap.put(id,answerStyle);
						knowledgeTypeMap.put(id, knowledgeType);
						questionIDMap.put(id, questionId);
					}
				}
			}
//			else if( annName.equals(AnsColumn.class.getName()) ){
//				
//				String id = ((AnsColumn) annotation).getId();
//				Transliterator tr = Transliterator.getInstance("Fullwidth-Halfwidth");
//				id = tr.transliterate(id);
//				casAnswerIdList.add(id);
//				answerTypeMap.put(id, answerType);
//				answerStyleMap.put(id,answerStyle);
//				answerColumnMap.put(id, examDocument.substring(annotation.getBegin(), annotation.getEnd()));
//				knowledgeTypeMap.put(id, knowledgeType);
//				questionIDMap.put(id, questionId);
//			}
		}

//System.out.println("check csv file");
//System.out.println(examName);
//System.out.println(CSVFileMap.get(examName));
System.out.println(answerTypeMap);
System.out.println(answerStyleMap);
System.out.println(knowledgeTypeMap);
System.out.println(questionIDMap);
		
		File csvFile = CSVFileMap.get(examName);
		if (csvFile != null){
		
			try {
				InputStreamReader in = new InputStreamReader(new FileInputStream(csvFile),"SJIS");
				CSVReader reader = new CSVReader(in);
				String nextLine[];
				reader.readNext();
				while( (nextLine = reader.readNext()) != null){
					if(nextLine[0].equals(""))
						continue;
//					String prefix = tr.transliterate(nextLine[0].substring(1,2));
					String prefix = "";
					String ansColumnID = exchangeId(nextLine[2], prefix);
					if( ansColumnID != null && !ansColumnID.equals("") ){		
						csvAnswerIdList.add(ansColumnID);
						sectionMap.put(ansColumnID, tr.transliterate(nextLine[0]));
						questionMap.put(ansColumnID, tr.transliterate(nextLine[1]));
						answerMap.put(ansColumnID, tr.transliterate(nextLine[3]));
						scoreMap.put(ansColumnID, tr.transliterate(nextLine[4]));
						answerColumnMap.put(ansColumnID, nextLine[2]);
					}
						
				}
				reader.close();
			
			} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

System.out.println(answerColumnMap);
System.out.println(csvAnswerIdList);

			StringBuffer sb = new StringBuffer();
			
			AnswerTable fsAnswerTable = new AnswerTable(jcas);
			fsAnswerTable.setFile(fileName);
			fsAnswerTable.setBegin(0);
			fsAnswerTable.addToIndexes();
			ansTableList.add(fsAnswerTable);
			
			for (int i = 0; i < csvAnswerIdList.size(); i++) {
			
				String id = csvAnswerIdList.get(i);
				String firstId = getFirstId(id);
				answerType = answerTypeMap.get(firstId);
				answerStyle = answerStyleMap.get(firstId);
				answerColumn = answerColumnMap.get(id);
				knowledgeType = knowledgeTypeMap.get(firstId);
				questionId = questionIDMap.get(firstId);
				
				if(answerType == null)
					answerType = " ";
				if(answerStyle == null)
					answerStyle = " ";
				if(answerColumn == null)
					answerColumn = " ";
				if(knowledgeType == null)
					knowledgeType = " ";
				if(questionId == null)
					questionId = " ";
				
				
				DataAT fsDataAT = new DataAT(jcas);
				fsDataAT.setBegin(sb.length());
				fsDataAT.addToIndexes();
				ansTableList.add(fsDataAT);
				
				Section fsSection = new Section(jcas);
				fsSection.setBegin(sb.length());
				fsSection.setString(sectionMap.get(id));
				sb.append(sectionMap.get(id));
				fsSection.setEnd(sb.length());
				fsSection.addToIndexes();
				ansTableList.add(fsSection);
				
				QuestionAT fsQuestionAT = new QuestionAT(jcas);
				fsQuestionAT.setBegin(sb.length());
				fsQuestionAT.setString(questionMap.get(id));
				sb.append(questionMap.get(id));
				fsQuestionAT.setEnd(sb.length());
				fsQuestionAT.addToIndexes();
				ansTableList.add(fsQuestionAT);
				
				AnswerColumn fsAnswerColumn = new AnswerColumn(jcas);
				fsAnswerColumn.setBegin(sb.length());
				fsAnswerColumn.setString(answerColumn);
				sb.append(answerColumn);
				fsAnswerColumn.setEnd(sb.length());
				fsAnswerColumn.addToIndexes();
				ansTableList.add(fsAnswerColumn);
				
				Answer fsAnswer = new Answer(jcas);
				fsAnswer.setBegin(sb.length());
				fsAnswer.setString(answerMap.get(id));
				sb.append(answerMap.get(id));
				fsAnswer.setEnd(sb.length());
				fsAnswer.addToIndexes();
				ansTableList.add(fsAnswer);
				
				Score fsScore = new Score(jcas);
				fsScore.setBegin(sb.length());
				fsScore.setString(scoreMap.get(id));
				sb.append(scoreMap.get(id));
				fsScore.setEnd(sb.length());
				fsScore.addToIndexes();
				ansTableList.add(fsScore);
				
				AnswerType fsAnswerType = new AnswerType(jcas);
				fsAnswerType.setBegin(sb.length());
				fsAnswerType.setString(answerType);
				sb.append(answerType);
				fsAnswerType.setEnd(sb.length());
				fsAnswerType.addToIndexes();
				ansTableList.add(fsAnswerType);
				
				AnswerStyle fsAnswerStyle = new AnswerStyle(jcas);
				fsAnswerStyle.setBegin(sb.length());
				fsAnswerStyle.setString(answerStyle);
				sb.append(answerStyle);
				fsAnswerStyle.setEnd(sb.length());
				fsAnswerStyle.addToIndexes();
				ansTableList.add(fsAnswerStyle);
				
				Knowledge fsKnowledge = new Knowledge(jcas);
				fsKnowledge.setBegin(sb.length());
				fsKnowledge.setString(knowledgeType);
				sb.append(knowledgeType);
				fsKnowledge.setEnd(sb.length());
				fsKnowledge.addToIndexes();
				ansTableList.add(fsKnowledge);
				
				QuestionID fsQuestionID = new QuestionID(jcas);
				fsQuestionID.setBegin(sb.length());
				fsQuestionID.setString(questionId);
				sb.append(questionId);
				fsQuestionID.setEnd(sb.length());
				fsQuestionID.addToIndexes();
				ansTableList.add(fsQuestionID);
				
				AnswerColumnID fsAnswerColumnID = new AnswerColumnID(jcas);
				fsAnswerColumnID.setBegin(sb.length());
				fsAnswerColumnID.setString(id);
				sb.append(id);
				fsAnswerColumnID.setEnd(sb.length());
				fsAnswerColumnID.addToIndexes();
				ansTableList.add(fsAnswerColumnID);
						
			}
			
			fsAnswerTable.setEnd(sb.length());
			
			AnswerTableAnnotationList answerTableAnnotationList = new AnswerTableAnnotationList(jcas);
			answerTableAnnotationList.addToIndexes();
			
			FSArray fsAnswerTaableAnnotationList = new FSArray(jcas, ansTableList.size());
			
			for (int i = 0; i < ansTableList.size(); i++) {
				fsAnswerTaableAnnotationList.set(i, ansTableList.get(i));
			}
			answerTableAnnotationList.setBeginList(fsAnswerTaableAnnotationList);
			
			if( saveFileName != null ){
				File saveFile = new File(OUTPUT_DIR.trim(), saveFileName);
				org.kachako.components.centerexam.answertable.AnswerTableWriter.writeAnswerTable(jcas, saveFile);
			}
		}
	
	}
	

	private String exchangeId(String id, String prefix) {

		if( exactAndMultiplePattern.matcher(id).find() ){
			System.out.println(multilpleCorrect +' '+ exactMatchOrder);
			String ids[] = id.split(multilpleCorrect);
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < ids.length; i++) {
				String tmp[] = ids[i].split(exactMatchOrder);
				for (int j = 0; j < tmp.length; j++) {
					sb.append(prefix + tmp[i]);
					if(i < tmp.length-1)
						sb.append(exactMatchOrder);
				}
				if(i< ids.length-1)
					sb.append(multilpleCorrect);
			}
			return sb.toString();
		}
		else if( exactPattern.matcher(id).find() ){
    		String ids[] = id.split(exactMatchOrder);
    		StringBuilder sb = new StringBuilder();
    		for (int i = 0; i < ids.length; i++) {
    			sb.append(prefix + ids[i]);
    			if(i < ids.length-1)
    				sb.append(exactMatchOrder);
    		}			
    		return sb.toString();
		}
    	else if( multiplePattern.matcher(id).find() ){
    		String ids[] = id.split(MULTIPLE_CORRECT);
    		StringBuilder sb = new StringBuilder();
    		for (int i = 0; i < ids.length; i++) {
    			sb.append(prefix + ids[i]);
    			if(i < ids.length-1)
    				sb.append(multilpleCorrect);
			}
    		return sb.toString();
    	}
    	else if( randomPattern.matcher(id).find() ){
    		String ids[] = id.split(RANDOM_ORDER);
    		StringBuilder sb = new StringBuilder();
    		for (int i = 0; i < ids.length; i++) {
    			sb.append(prefix + ids[i]);
    			if(i < ids.length-1)
    				sb.append(randomOrder);
    		}
    		return sb.toString();
    	}
    	else{
    		return prefix + id;
    	}
	}
	
//	private String exchangeId(String id){
//		String ids = exchangeId(id, "");
//		return ids;
//	}
	
	private String getFirstId(String id){
		
		if( exactPattern.matcher(id).find() ){
    		String ids[] = id.split(EXACT_MATCH_ORDER);
    		return ids[0];
		}
		else if( multiplePattern.matcher(id).find() ){
    		String ids[] = id.split(MULTIPLE_CORRECT);
    		return ids[0];
		}
		else if( randomPattern.matcher(id).find() ){
    		String ids[] = id.split(RANDOM_ORDER);
    		return ids[0];
		}
		else
			return id;
	}

}
