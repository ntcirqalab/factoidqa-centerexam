package org.kachako.components.centerexam.answertable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.xmlbeans.XmlException;
import org.kachako.types.centerexam.answertable.*;

public class AnswerTableGetter {
	
	public void getAnswerData(JCas jcas, HashMap<String, String> ansColumnIDArrayList, HashMap<String, String> answerArrayList, HashMap<String, String> scoreArrayList, HashMap<String, String> processLogArrayList) throws XmlException, IOException{
		
		int index = 0;
		String key = null;
		
		TypeSystem typeSystem = jcas.getTypeSystem();
		Type type = typeSystem.getType(AnswerTableAnnotationList.class.getName());
		FSIterator<FeatureStructure> fsIterator = jcas.getFSIndexRepository().getAllIndexedFS(type); 
		
		if(!fsIterator.hasNext()) return;
		
		AnswerTableAnnotationList annListFS =  (AnswerTableAnnotationList) fsIterator.next();
		FSArray fsArray = annListFS.getBeginList();
		ArrayList<Annotation> annList = new ArrayList<Annotation>();
		
		for (int i = 0; i < fsArray.size(); i++) {
			annList.add((Annotation) fsArray.get(i));
		}

		while(index < annList.size()){
		
			Annotation ann = annList.get(index);
if (ann == null) {
System.out.println("ann is null");	
index++;
continue;
}
			if(ann.getType().toString().equals(AnswerColumn.class.getName()))
				key = ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("String"));
			
			else if(ann.getType().toString().equals(Answer.class.getName()))
				answerArrayList.put(key, ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("String")));
			
			else if(ann.getType().toString().equals(AnswerColumnID.class.getName()))
				ansColumnIDArrayList.put(key, ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("String")));
			
			else if(ann.getType().toString().equals(Score.class.getName()))
				scoreArrayList.put(key, ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("String")));
			
			else if(ann.getType().toString().equals(ProcessLog.class.getName()))
				processLogArrayList.put(key, ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("String")));
			
			index++;
		}
		
	}
	
}
