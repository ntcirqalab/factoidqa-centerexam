package org.kachako.components.centerexam.answertable;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptionCharEscapeMap;
import org.apache.xmlbeans.XmlOptions;
import org.kachako.types.centerexam.answertable.AnswerColumnID;
import org.kachako.types.centerexam.answertable.AnswerTableAnnotationList;
import org.kachako.types.centerexam.answertable.AnswerTableEndList;
import org.kachako.types.centerexam.answertable.Option;


public class AnswerTableWriter extends JCasAnnotator_ImplBase {

	final public static String PARAM_OUTPUTDIR = "OutputDirectory"; 
	final public static String DOCTYPE = "answerTable";
	final public static String SYSTEM = "http://21robot.org/answerTable.dtd";
	String XML_OUTPUTDIR;
	String outPutXmlFileName = "";
	
	
	@Override
	public void initialize(UimaContext aContext) throws ResourceInitializationException {
		super.initialize(aContext);
		
		XML_OUTPUTDIR = aContext.getConfigParameterValue(PARAM_OUTPUTDIR).toString();
		
	}
	
	
	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		// TODO Auto-generated method stub
		
		 outPutXmlFileName = jcas.getAnnotationIndex().toString();	        
	     File file = new File(XML_OUTPUTDIR, outPutXmlFileName + ".xml");
	   
	     if(file.exists()){
	     	file.delete();
	     }
	     try {
	    	 file.createNewFile();
	     } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    	    
		writeAnswerTable(jcas, file);
         
	}

	// 書き出し（解答機コンポーネントの結果追加）
	public static void writeAnswerTable(FSArray Annotations, File file, String perfectScore, String score,
			String rate, String numOfCorrect, String numOfQuestion){
		
		if(Annotations == null || Annotations.size() < 1)
			return;
		
		XmlObject newXml = XmlObject.Factory.newInstance();
        XmlCursor cursor = newXml.newCursor();
        
        cursor.documentProperties().setDoctypeName(DOCTYPE);
		cursor.documentProperties().setDoctypeSystemId(SYSTEM);
		
        cursor.push();
        cursor.toNextToken();
        
        int index = 1;
        
     	String insertString = "";
		AnswerTableTagType tagType = new AnswerTableTagType();
		
		// 正答表タグ挿入 
		cursor.beginElement("answerTable");
		cursor.insertAttributeWithValue("accuracy_rate", rate);
		cursor.insertAttributeWithValue("correct_answers", numOfCorrect);
		cursor.insertAttributeWithValue("questions", numOfQuestion);
		cursor.insertAttributeWithValue("perfect_score", perfectScore);
		cursor.insertAttributeWithValue("total_score", score);
		
if (Annotations.get(0) == null) {
	System.out.println("Annotations.get(0) is null");	
	return;
}

		if(Annotations.get(0).getFeatureValueAsString(Annotations.get(0).getType().getFeatureByBaseName("file")) != null)
			cursor.insertAttributeWithValue("filename", Annotations.get(0).getFeatureValueAsString(Annotations.get(0).getType().getFeatureByBaseName("file")));
		if(Annotations.get(0).getFeatureValueAsString(Annotations.get(0).getType().getFeatureByBaseName("selected")) != null)
			cursor.insertAttributeWithValue("selected", Annotations.get(0).getFeatureValueAsString(Annotations.get(0).getType().getFeatureByBaseName("selected")));
		cursor.insertChars("\n");
		
		while(index < Annotations.size()){
			
			Annotation ann = (Annotation) Annotations.get(index);
			insertString = tagType.getBeginTagType(ann.getType());

			if(insertString == null){
				index++;
				continue;
			}
			if(insertString.equals("data")){
				cursor.beginElement("data");
				cursor.insertChars("\n");
			}
			else if(!insertString.equals("data") && !insertString.equals("answerTable")){
				if(insertString.equals("option")){
					Option option = (Option)Annotations.get(index);
					cursor.beginElement("option");
					cursor.insertAttributeWithValue("section_ID", option.getSection_ID());
					cursor.insertAttributeWithValue("anscolumns", option.getAnscolumns());
					cursor.toNextToken();
					cursor.insertChars("\n");
				}
				else{
					cursor.beginElement(insertString);
					cursor.insertChars(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("String")));
					cursor.toNextToken();
					cursor.insertChars("\n");
				}
			}
			
			if(insertString.equals("anscolumn_ID")){
			
				AnswerColumnID answerColumnID = (AnswerColumnID) Annotations.get(index);
				cursor.beginElement("TF");

				if(answerColumnID.getTF())
					cursor.insertChars("1");
				else
					cursor.insertChars("0");
				
				cursor.toNextToken();
				cursor.insertChars("\n");
			}
			
			if(insertString.equals("process_log")){
				cursor.toNextToken();
				cursor.insertChars("\n");
			}
			
			index++;
		}
		
		cursor.toNextToken();
				       
        cursor.pop();

        // xml ファイル保存
        try {
        	XmlOptions xmlOpt = new XmlOptions();
        	XmlOptionCharEscapeMap xmlOptionCharEscapeMap = new XmlOptionCharEscapeMap();
		    xmlOptionCharEscapeMap.addMapping('\"', XmlOptionCharEscapeMap.PREDEF_ENTITY);
		    xmlOptionCharEscapeMap.addMapping('\'', XmlOptionCharEscapeMap.PREDEF_ENTITY);
		    xmlOptionCharEscapeMap.addMapping('<', XmlOptionCharEscapeMap.PREDEF_ENTITY);
		    xmlOptionCharEscapeMap.addMapping('>', XmlOptionCharEscapeMap.PREDEF_ENTITY);
		    xmlOptionCharEscapeMap.addMapping('&', XmlOptionCharEscapeMap.PREDEF_ENTITY);
		       
		    xmlOpt.setSaveSubstituteCharacters(xmlOptionCharEscapeMap);
			cursor.save(file, xmlOpt);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        cursor.dispose();
		
	}

	// 書き出し(通常)
	public static void writeAnswerTable(JCas jcas, File file) {
		
		ArrayList<Annotation> fsAnnotationList = new ArrayList<Annotation>();
		ArrayList<Type> fsTypeList = new ArrayList<Type>();
		ArrayList<Annotation> fsTypeEndList = new ArrayList<Annotation>();
		ArrayList<Integer> fsBeginList = new ArrayList<Integer>();
		ArrayList<Integer> fsEndList = new ArrayList<Integer>();
		
		TypeSystem typeSystem = jcas.getTypeSystem();
	    Type type = typeSystem.getType(AnswerTableAnnotationList.class.getName());
	    
		FSIterator fsIterator = jcas.getFSIndexRepository().getAllIndexedFS(type);
		
		AnswerTableAnnotationList annList =  (AnswerTableAnnotationList) fsIterator.next();
		FSArray fsArrayAnn = annList.getBeginList();
		
		type = typeSystem.getType(AnswerTableEndList.class.getName());
		fsIterator = jcas.getFSIndexRepository().getAllIndexedFS(type);

//		AnswerTableEndList endList = (AnswerTableEndList) fsIterator.next();
//		FSArray fsArrayEnd = endList.getEndList();
		
		
		for (int i = 0; i < fsArrayAnn.size(); i++) {
			
			fsAnnotationList.add((Annotation) fsArrayAnn.get(i));
			fsTypeList.add(fsArrayAnn.get(i).getType());
//			fsTypeEndList.add((Annotation) fsArrayEnd.get(i));
			fsBeginList.add( ((Annotation) fsArrayAnn.get(i)).getBegin());
//			fsEndList.add( ((Annotation) fsArrayAnn.get(i)).getEnd());
						
		}
		
			Collections.sort(fsEndList);
		
        XmlObject newXml = XmlObject.Factory.newInstance();
        XmlCursor cursor = newXml.newCursor();
        
        cursor.documentProperties().setDoctypeName(DOCTYPE);
		cursor.documentProperties().setDoctypeSystemId(SYSTEM);
		
//      System.out.println(jcas.getDocumentText());
        
        cursor.push();
        cursor.toNextToken();
        
        int index = 1;
        boolean flag = false; 
        
     	String insertString = "";
		AnswerTableTagType tagType = new AnswerTableTagType();

		
		// 正答表タグ挿入	 
		cursor.beginElement(tagType.getBeginTagType(fsAnnotationList.get(0).getType()));
		cursor.insertAttributeWithValue("filename", fsAnnotationList.get(0).getFeatureValueAsString(fsAnnotationList.get(0).getType().getFeatureByBaseName("file")));
		cursor.insertChars("\n");
		
		while(index < fsAnnotationList.size()){
			
			if(flag){
				cursor.toNextToken();
				cursor.insertChars("\n");
				flag =false;
			}
			Annotation ann = fsAnnotationList.get(index);
			
			insertString = tagType.getBeginTagType(ann.getType());
			
			if(insertString.equals("data")){
				cursor.beginElement("data");
				cursor.insertChars("\n");
			}
			else if(!insertString.equals("data") && !insertString.equals("answerTable")){
				if(insertString.equals("option")){
					Option option = (Option)fsAnnotationList.get(index);
					cursor.beginElement("option");
					cursor.insertAttributeWithValue("section_ID", option.getSection_ID());
					cursor.insertAttributeWithValue("anscolumns", option.getAnscolumns());
					cursor.toNextToken();
					cursor.insertChars("\n");
				}
				else{
					cursor.beginElement(insertString);
					cursor.insertChars(ann.getFeatureValueAsString(ann.getType().getFeatureByBaseName("String")));
					cursor.toNextToken();
					cursor.insertChars("\n");
				}
			}
			
			if(insertString.equals("anscolumn_ID"))
				flag = true;
			
			if(insertString.equals("process_log")){
				flag = false;
				cursor.toNextToken();
			}
			
			index++;
		}
		
		cursor.toNextToken();
				       
        cursor.pop();

        // xml ファイル保存
        try {
        	XmlOptions xmlOpt = new XmlOptions();
        	XmlOptionCharEscapeMap xmlOptionCharEscapeMap = new XmlOptionCharEscapeMap();
		    xmlOptionCharEscapeMap.addMapping('\"', XmlOptionCharEscapeMap.PREDEF_ENTITY);
		    xmlOptionCharEscapeMap.addMapping('\'', XmlOptionCharEscapeMap.PREDEF_ENTITY);
		    xmlOptionCharEscapeMap.addMapping('<', XmlOptionCharEscapeMap.PREDEF_ENTITY);
		    xmlOptionCharEscapeMap.addMapping('>', XmlOptionCharEscapeMap.PREDEF_ENTITY);
		    xmlOptionCharEscapeMap.addMapping('&', XmlOptionCharEscapeMap.PREDEF_ENTITY);
		       
		    xmlOpt.setSaveSubstituteCharacters(xmlOptionCharEscapeMap);
			cursor.save(file, xmlOpt);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        cursor.dispose();
	}			
}
