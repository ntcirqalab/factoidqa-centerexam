package org.kachako.components.centerexam;

import java.util.ArrayList;
import java.util.HashMap;

public class SaveResult {
	
	private  ArrayList<String> componentList = new ArrayList<String>();
	private  HashMap<String, String> scoreMap = new HashMap<String, String>();
	private  HashMap<String, String> rateMap = new HashMap<String, String>();
	private  HashMap<String, String> correctMap = new HashMap<String, String>();
	private  HashMap<String, String> questionMap = new HashMap<String, String>();
	private  HashMap<String, String> perfectScoreMap = new HashMap<String, String>();
	
	
	
	public void setResult(String component, String score, String rate, String correct, String question, String perfectScore){
		componentList.add(component);
		scoreMap.put(component, score);
		rateMap.put(component, rate);
		correctMap.put(component, correct);
		questionMap.put(component, question);
		perfectScoreMap.put(component, perfectScore);
	}
	
	public String getResult(String component){
		if(!getComponent().contains(component))
			return null;
		String score = getScore(component);
		String rate = getRate(component);
		String correct = getNumOfCorrect(component);
		String question = getNumOfQuestion(component);
		String perfectScore = getPerfectScore(component);
		return component + ": " + "score=" + score + " rate=" + rate + " correct=" + correct
				+ " question" + question + " perfectScore=" + perfectScore;
	}
	
	public ArrayList<String> getComponent(){
		return componentList;
	}
	
	public String getScore(String component){
		return scoreMap.get(component);
	}
	
	public ArrayList<String> getScore(){
		ArrayList<String> scoreList = new ArrayList<String>();
		if( componentList.size() > 0 ){
			for (int i = 0; i < componentList.size(); i++) {
				scoreList.add(scoreMap.get(componentList.get(i)));
			}
			return scoreList;
		}
		else
			return null;
	}
	
	public String getRate(String component){
		return rateMap.get(component);
	}
	
	public ArrayList<String> getRate(){
		ArrayList<String> rateList = new ArrayList<String>();
		if( componentList.size() > 0 ){
			for (int i = 0; i < componentList.size(); i++) {
				rateList.add(rateMap.get(componentList.get(i)));
			}
			return rateList;
		}
		else
			return null;
	}
	
	public String getNumOfCorrect(String component){
		return correctMap.get(component);
	}
	
	public ArrayList<String> getNumOfCorrect(){
		ArrayList<String> rateList = new ArrayList<String>();
		if( componentList.size() > 0 ){
			for (int i = 0; i < componentList.size(); i++) {
				rateList.add(correctMap.get(componentList.get(i)));
			}
			return rateList;
		}
		else
			return null;
	}
	public String getNumOfQuestion(String component){
		return questionMap.get(component);
	}
	
	public ArrayList<String> getNumOfQuestion(){
		ArrayList<String> rateList = new ArrayList<String>();
		if( componentList.size() > 0 ){
			for (int i = 0; i < componentList.size(); i++) {
				rateList.add(questionMap.get(componentList.get(i)));
			}
			return rateList;
		}
		else
			return null;
	}
	
	public String getPerfectScore(String component){
		return perfectScoreMap.get(component);
	}
	
	public ArrayList<String> getPerfectScore(){
		ArrayList<String> perfectScoreList = new ArrayList<String>();
		if( componentList.size() > 0){
			for (int i = 0; i < componentList.size(); i++) {
				perfectScoreList.add(questionMap.get(componentList.get(i)));
			}
			return perfectScoreList;
		}
		else
			return null;
	}
	
	public void clear(){
		componentList.clear();
		scoreMap.clear();
		rateMap.clear();
		correctMap.clear();
		questionMap.clear();
	}
	
	public void clear(String component){
		if(!componentList.contains(component))
			return;
		componentList.remove(component);
		scoreMap.remove(component);
		rateMap.remove(component);
		correctMap.remove(component);
		questionMap.remove(component);
	}

}
