package org.kachako.components.centerexam;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Stack;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import javax.xml.namespace.QName;

import org.kachako.types.centerexam.*;
import org.kachako.types.centerexam.Math;
import org.kachako.types.centerexam.answertable.*;

import noNamespace.*;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.Type;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.collection.CollectionReader_ImplBase;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceConfigurationException;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Progress;
import org.apache.uima.util.ProgressImpl;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlCursor.TokenType;
import org.apache.xmlbeans.XmlException;


public class ExamReader extends CollectionReader_ImplBase{
	
	public static final String PARAM_INPUTDIR = "InputDirectory";
	public static final String PARAM_ANSTABLE = "AnswerTableDirectory";
	public static final String PARAM_INPFILENAME = "InputFileName";
	public static final String PARAM_REGEX = "Regex";
	
	private ArrayList<File> mFiles;
	private String ansTableDirPath;
	private String[] inputFiles; 
	private int mCorrentIndex;
	
	private HashMap<String, File> answerTableFileMap;
	private Pattern mathRegex = null;
	
	@Override
	public void initialize() throws ResourceInitializationException{
		
		File directory = new 
				File(((String) getConfigParameterValue(PARAM_INPUTDIR)).trim());
		ansTableDirPath = ((String) getConfigParameterValue(PARAM_ANSTABLE)).trim();
		inputFiles = ((String[]) getConfigParameterValue(PARAM_INPFILENAME));
		if(getConfigParameterValue(PARAM_REGEX) != null){ 
			mathRegex = Pattern.compile(getConfigParameterValue(PARAM_REGEX).toString().trim());
		}
		mCorrentIndex = 0;
		
		if(!directory.exists() || !directory.isDirectory()){
			throw new ResourceInitializationException(ResourceConfigurationException.DIRECTORY_NOT_FOUND,
					new Object[]{ PARAM_INPUTDIR, this.getMetaData().getName(), directory.getPath() });
		}
			
		mFiles = new ArrayList<File>();
		answerTableFileMap = new HashMap<String, File>();
		
		addFilesFromDir(directory, mFiles);
		
		
		if( ansTableDirPath != null && ansTableDirPath.length() > 0){
			File ansTableDirectory = new
					File(ansTableDirPath);
			File[] answerTablefiles = ansTableDirectory.listFiles(); 
			for (int i = 0; i < answerTablefiles.length; i++) {
				answerTableFileMap.put(answerTablefiles[i].getName(), answerTablefiles[i]);
			}
		}
	}
	
	private void addFilesFromDir(File dir, ArrayList<File> fileList){

		File[] files = dir.listFiles();
		ArrayList<String> inputFileList = new ArrayList<String>();
		if( inputFiles != null && inputFiles.length > 0){
			for (int i = 0; i < inputFiles.length; i++) {
				inputFileList.add(inputFiles[i]);
			}
		}
		for(int i=0; i<files.length; i++){
			if(!files[i].isDirectory() && files[i].canRead() && files[i].getPath().endsWith(".xml")) {
				if(inputFileList.isEmpty())
					fileList.add(files[i]);
				else if(inputFileList.indexOf(files[i].getName()) != -1)
					fileList.add(files[i]);
			}	
		}
	}	
	
	@Override
	public void getNext(CAS aCAS) throws IOException, CollectionException {
		// TODO Auto-generated method stub
		JCas jcas;
		try{
			jcas = aCAS.getJCas();
		}catch(CASException e) {
			throw new CollectionException(e);
		}
		File file = (File) mFiles.get(mCorrentIndex++);

		System.out.println(file.getName());

				
		Stack<Annotation> jCasStack = new Stack<Annotation>();
		StringBuilder CenterExamDocument = new StringBuilder();
		int begin = 0;
		int end = 0;
		
		String rangeOfOptions = null;
		String numOfOptions = null;
		
		HashMap<String, Annotation> refMap = new HashMap<String, Annotation>();
		ArrayList<Ref> refList = new ArrayList<Ref>();
		ArrayList<Annotation> annList = new ArrayList<Annotation>();
		ArrayList<Annotation> endList = new ArrayList<Annotation>();
		Annotation annotation;
		
		QName qSource = QName.valueOf("source");
		QName qSrcTxtURL = QName.valueOf("srcTxtURL");
		QName qSubject = QName.valueOf("subject");
		QName qYear = QName.valueOf("year");
		QName qId = QName.valueOf("id");
		QName qDigits = QName.valueOf("digits");
		QName qType = QName.valueOf("type");
		QName qComment = QName.valueOf("comment");
		QName qAnscol = QName.valueOf("anscol");
		QName qCorrection = QName.valueOf("correction");
		QName qSrc = QName.valueOf("src");
		QName qString = QName.valueOf("string");
		QName qMinimal = QName.valueOf("minimal");
		QName qAnswer_style = QName.valueOf("answer_style");
		QName qAnswer_type = QName.valueOf("answer_type");
		QName qKnowledge_type = QName.valueOf("knowledge_type");
		QName qTitle = QName.valueOf("title");
		QName qSectionId = QName.valueOf("sectionId");
		QName qTarget = QName.valueOf("target");
		QName qNumOfCols = QName.valueOf("numOfCols");
		QName qNumOfRows = QName.valueOf("numOfRows");
		
	    ExamDocument.class.getClassLoader();
		try {
			ExamDocument exam = (ExamDocument)ExamDocument.Factory.parse(file);
			XmlCursor cursor = exam.newCursor();
				
			while(!cursor.toNextToken().isNone()){
								
				switch(cursor.currentTokenType().intValue()){
					
				case TokenType.INT_START:
	
					begin = CenterExamDocument.length();
					String tagName = cursor.getName().toString();
					String tmp[] = tagName.split("}");
					tagName = tmp[tmp.length-1];
				
					// Annotation の作成と開始位置のセット
					// exam tag
					if(tagName.equals("exam")){
						Exam examFS = new Exam(jcas);
						annList.add(examFS);
						
						examFS.addToIndexes(jcas);
						examFS.setBegin(begin);						
						
						// 属性のセット
						examFS.setSource(cursor.getAttributeText(qSource));
						if(cursor.getAttributeText(qSrcTxtURL) != null) examFS.setSrcTxtURL(cursor.getAttributeText(qSrcTxtURL));
						examFS.setSubject(cursor.getAttributeText(qSubject));
						if(cursor.getAttributeText(qYear) != null) examFS.setYear(Integer.parseInt(cursor.getAttributeText(qYear)));

						java.util.List<Feature> list = examFS.getType().getFeatures(); 
						for (int i = 0; i < examFS.getType().getFeatures().size(); i++) {
							Feature feat = list.get(i);
							if(feat.getShortName().equals("id")){
								examFS.setFeatureValueFromString(feat, cursor.getAttributeText(QName.valueOf("id")));
							}
							if(feat.getShortName().equals("math")){
								if(mathRegex == null){
									examFS.setFeatureValueFromString(feat, "false");
								}
								else if(mathRegex.matcher(file.getName()).find()){
									examFS.setFeatureValueFromString(feat, "true");
								}
								else{
									examFS.setFeatureValueFromString(feat, "false");
								}
							}
							if(feat.getShortName().equals("range_of_options")){
								examFS.setFeatureValueFromString(feat, cursor.getAttributeText(QName.valueOf("range_of_options")));
								rangeOfOptions = cursor.getAttributeText(QName.valueOf("range_of_options"));
							}
							if(feat.getShortName().equals("num_of_options")){
								examFS.setFeatureValueFromString(feat, cursor.getAttributeText(QName.valueOf("num_of_options")));
								numOfOptions = cursor.getAttributeText(QName.valueOf("num_of_options"));
							}
						}
						jCasStack.push(examFS);
						
					}
					
					// ansColumn tag
					else if(tagName.equals("ansColumn")){
						AnsColumn ansColumnFS = new AnsColumn(jcas);
						annList.add(ansColumnFS);
						ansColumnFS.addToIndexes(jcas);
						ansColumnFS.setBegin(begin);
						
						// 属性のセット
						ansColumnFS.setId(cursor.getAttributeText(qId));
						
						refMap.put(cursor.getAttributeText(qId), ansColumnFS);
						
						jCasStack.push(ansColumnFS);
					}
					
					// blank tag
					else if(tagName.equals("blank")){
						Blank blankFS = new Blank(jcas);
						annList.add(blankFS);
						blankFS.addToIndexes(jcas);
						blankFS.setBegin(begin);
						
						// 属性のセット
						blankFS.setId(cursor.getAttributeText(qId));
						blankFS.setDigits(cursor.getAttributeText(qDigits));
						java.util.List<Feature> list = blankFS.getType().getFeatures();
						for (int i = 0; i < list.size(); i++) {
							Feature feat = list.get(i);
							if(feat.getShortName().equals("anscolumn_id")){
								blankFS.setFeatureValueFromString(feat, cursor.getAttributeText(QName.valueOf("anscolumn_id")));
							}
						}

						refMap.put(cursor.getAttributeText(qId), blankFS);
						
						jCasStack.push(blankFS);
						
					}
					
					// br tag
					else if(tagName.equals("br")){
						Br brFS = new Br(jcas);
						annList.add(brFS);
						brFS.addToIndexes(jcas);
						brFS.setBegin(begin);
						
						jCasStack.push(brFS);
						
					}
					
					// cell tag
					else if(tagName.equals("cell")){
						Cell cellFS = new Cell(jcas);
						annList.add(cellFS);
						cellFS.addToIndexes(jcas);
						cellFS.setBegin(begin);
						
						// 属性のセット
						cellFS.setTypes(cursor.getAttributeText(qType));
						
						jCasStack.push(cellFS);
						
					}
					
					// choice tag
					else if(tagName.equals("choice")){
						
						Choice choiceFS = new Choice(jcas);
						annList.add(choiceFS);
						choiceFS.addToIndexes(jcas);
						choiceFS.setBegin(begin);
						
						// 属性のセット
//						choiceFS.setRa(cursor.getAttributeText(qRa));
						Iterator<Feature> iterator = choiceFS.getType().getFeatures().iterator();
						while(iterator.hasNext()){
							Feature feat = iterator.next();
							if(feat.getShortName().equals("comment"))
								choiceFS.setFeatureValueFromString(feat, cursor.getAttributeText(qComment));
							else if(feat.getShortName().equals("ansnum"))
								choiceFS.setFeatureValueFromString(feat, cursor.getAttributeText(QName.valueOf("ansnum")));
						}
						jCasStack.push(choiceFS);
					}
					
					// choices tag
					else if(tagName.equals("choices")){
						Choices choicesFS = new Choices(jcas);
						annList.add(choicesFS);
						choicesFS.addToIndexes(jcas);
						choicesFS.setBegin(begin);
					
						// 属性のセット
						choicesFS.setComment(cursor.getAttributeText(qComment));
						choicesFS.setAnscol(cursor.getAttributeText(qAnscol));
						
						jCasStack.push(choicesFS);
					
					}
					
					// cNum tag
					else if(tagName.equals("cNum")){
						CNum cNumFS = new CNum(jcas);
						annList.add(cNumFS);
						cNumFS.addToIndexes(jcas);
						cNumFS.setBegin(begin);
						
						jCasStack.push(cNumFS);
						
					}
					
					// data tag
					else if(tagName.equals("data")){
						Data dataFS = new Data(jcas);
						annList.add(dataFS);
						dataFS.addToIndexes(jcas);
						dataFS.setBegin(begin);
						
						// 属性のセット
						dataFS.setId(cursor.getAttributeText(qId));
						dataFS.setTypes(cursor.getAttributeText(qType));
						
						refMap.put(cursor.getAttributeText(qId), dataFS);
						
						jCasStack.push(dataFS);
						
					}
					
					// formula tag
					else if(tagName.equals("formula")){
						Formula formulaFS = new Formula(jcas);
						annList.add(formulaFS);
						formulaFS.addToIndexes(jcas);
						formulaFS.setBegin(begin);
						
						// 属性のセット
						formulaFS.setComment(cursor.getAttributeText(qComment));
						
						jCasStack.push(formulaFS);
						
					}
					
					// garbled tag
					else if(tagName.equals("garbled")){
						Garbled garbledFS = new Garbled(jcas);
						annList.add(garbledFS);
						garbledFS.addToIndexes(jcas);
						garbledFS.setBegin(begin);
					
						// 属性のセット
						garbledFS.setCorrection(cursor.getAttributeText(qCorrection));
						
						jCasStack.push(garbledFS);
						
					}
					
					// img tag
					else if(tagName.equals("img")){
						Img imgFS = new Img(jcas);
						annList.add(imgFS);
						imgFS.addToIndexes(jcas);
						imgFS.setBegin(begin);
						
						// 属性のセット
						imgFS.setSrc(cursor.getAttributeText(qSrc));
						imgFS.setComment(cursor.getAttributeText(qComment));
						imgFS.setCorresponding_obj(cursor.getAttributeText(QName.valueOf("corresponding_obj")));
						
						jCasStack.push(imgFS);
						
					}
					
					// info tag
					else if(tagName.equals("info")){
						Info infoFS = new Info(jcas);
						annList.add(infoFS);
						infoFS.addToIndexes(jcas);
						infoFS.setBegin(begin);
						
						jCasStack.push(infoFS);
						
					}
					
					// introduction tag
					else if(tagName.equals("instruction")){
						Instruction instructionFS = new Instruction(jcas);
						annList.add(instructionFS);
						instructionFS.addToIndexes(jcas);
						instructionFS.setBegin(begin);
						
						jCasStack.push(instructionFS);
						
					}
					
					// label tag
					else if(tagName.equals("label")){
						Label labelFS = new Label(jcas);
						annList.add(labelFS);
						labelFS.addToIndexes(jcas);
						labelFS.setBegin(begin);
						
						jCasStack.push(labelFS);
						
					}
					
					// lText tag
					else if(tagName.equals("lText")){
						LText lTextFS = new LText(jcas);
						annList.add(lTextFS);
						lTextFS.addToIndexes(jcas);
						lTextFS.setBegin(begin);
						
						// 属性のセット
						lTextFS.setId(cursor.getAttributeText(qId));
						
						refMap.put(cursor.getAttributeText(qId), lTextFS);
						
						jCasStack.push(lTextFS);
						
					}
					
					// missing tag
					else if(tagName.equals("missing")){
						Missing missingFS = new Missing(jcas);
						annList.add(missingFS);
						missingFS.addToIndexes(jcas);
						missingFS.setBegin(begin);
						
						// 属性のセット
						missingFS.setAttributeString(cursor.getAttributeText(qString));
						
						jCasStack.push(missingFS);
						
					}
					
					// note tag
					else if(tagName.equals("note")){
						Note noteFS = new Note(jcas);
						annList.add(noteFS);
						noteFS.addToIndexes(jcas);
						noteFS.setBegin(begin);
						
						// 属性のセット
						noteFS.setId(cursor.getAttributeText(qId));
						
						refMap.put(cursor.getAttributeText(qId), noteFS);
						
						jCasStack.push(noteFS);
						
					}

					// question tag
					else if(tagName.equals("question")){
						Question questionFS = new Question(jcas);
						annList.add(questionFS);
						questionFS.addToIndexes(jcas);
						questionFS.setBegin(begin);
						
						// 属性のセット
						questionFS.setId(cursor.getAttributeText(qId));
						questionFS.setMinimal(cursor.getAttributeText(qMinimal));
						questionFS.setAnswer_style(cursor.getAttributeText(qAnswer_style));
						questionFS.setAnswer_type(cursor.getAttributeText(qAnswer_type));
						questionFS.setKnowledge_type(cursor.getAttributeText(qKnowledge_type));
						questionFS.setTitle(cursor.getAttributeText(qTitle));
						questionFS.setSectionId(cursor.getAttributeText(qSectionId));
						questionFS.setAnscol(cursor.getAttributeText(QName.valueOf("anscol")));
						
						java.util.List<Feature> list = questionFS.getType().getFeatures();
						for (int i = 0; i < list.size(); i++) {
							Feature feat = list.get(i);
							if(feat.getShortName().equals("anscolumn_ids")){
								questionFS.setFeatureValueFromString(feat, cursor.getAttributeText(QName.valueOf("anscolumn_ids")));
							}
						}
						
						refMap.put(cursor.getAttributeText(qId), questionFS);
						
						jCasStack.push(questionFS);
						
					}
					
					// quote tag
					else if(tagName.equals("quote")){
						Quote quoteFS = new Quote(jcas);
						annList.add(quoteFS);
						quoteFS.addToIndexes(jcas);
						quoteFS.setBegin(begin);
						
						jCasStack.push(quoteFS);
						
					}
					
					// ref tag
					else if(tagName.equals("ref")){
						Ref refFS = new Ref(jcas);
						refFS.addToIndexes(jcas);
						annList.add(refFS);
						refFS.setBegin(begin);
						refList.add(refFS);
						
						// 属性のセット
						refFS.setComment(cursor.getAttributeText(qComment));
						refFS.setTarget(cursor.getAttributeText(qTarget));
						
						jCasStack.push(refFS);
						
					}
					
					// row tag
					else if(tagName.equals("row")){
						Row rowFS = new Row(jcas);
						annList.add(rowFS);
						rowFS.addToIndexes(jcas);
						rowFS.setBegin(begin);
						
						jCasStack.push(rowFS);
						
					}
					
					// source tag
					else if(tagName.equals("source")){
						Source sourceFS = new Source(jcas);
						annList.add(sourceFS);
						sourceFS.addToIndexes(jcas);
						sourceFS.setBegin(begin);
						
						jCasStack.push(sourceFS);
						
					}
					
					// tbl tag
					else if(tagName.equals("tbl")){
						Tbl tblFS = new Tbl(jcas);
						annList.add(tblFS);
						tblFS.addToIndexes(jcas);
						tblFS.setBegin(begin);
						
						// 属性のセット
						if(cursor.getAttributeText(qNumOfCols) != null) tblFS.setNumOfCols(Integer.parseInt(cursor.getAttributeText(qNumOfCols)));
						if(cursor.getAttributeText(qNumOfRows) != null) tblFS.setNumOfRows(Integer.parseInt(cursor.getAttributeText(qNumOfRows)));
						
						jCasStack.push(tblFS);
						
					}
					
					// title tag
					else if(tagName.equals("title")){
						Title titleFS = new Title(jcas);
						annList.add(titleFS);
						titleFS.addToIndexes(jcas);
						titleFS.setBegin(begin);
						
						jCasStack.push(titleFS);
						
					}
					
					// uText tag
					else if(cursor.getName().toString().equals("uText")){
						UText uTextFS = new UText(jcas);
						annList.add(uTextFS);
						uTextFS.addToIndexes(jcas);
						uTextFS.setBegin(begin);
					
						// 属性のセット
						uTextFS.setId(cursor.getAttributeText(qId));
						
						refMap.put(cursor.getAttributeText(qId), uTextFS);
						
						jCasStack.push(uTextFS);
						
					}
					
					// caption tag
					else if(cursor.getName().toString().equals("caption")){
						Caption captionFS = new Caption(jcas);
						annList.add(captionFS);
						captionFS.addToIndexes(jcas);
						captionFS.setBegin(begin);
						
						jCasStack.push(captionFS);
					}
					
					// MathML用
					// math tag
					else if(tagName.equals("math")){
						Math mathFS = new Math(jcas);
						annList.add(mathFS);
						mathFS.addToIndexes(jcas);
						mathFS.setBegin(begin);
						
//						mathFS.setXmlns(cursor.getAttributeText(QName.valueOf("xmlns")));
						mathFS.setXmlns(cursor.getName().getNamespaceURI());
						mathFS.setDisplay(cursor.getAttributeText(QName.valueOf("display")));

						jCasStack.push(mathFS);
					}
					
					// semantics tag
					else if(tagName.equals("semantics")){
						
						Semantics semanticsFS = new Semantics(jcas);
						annList.add(semanticsFS);
						semanticsFS.addToIndexes(jcas);
						semanticsFS.setBegin(begin);
						
						jCasStack.push(semanticsFS);
					}
					
					//annotation-xml tag
					else if(tagName.equals("annotation-xml")){
						
						AnnotationXml annXmlFS = new AnnotationXml(jcas);
						annList.add(annXmlFS);
						annXmlFS.addToIndexes(jcas);
						annXmlFS.setBegin(begin);
						
						annXmlFS.setEncoding(cursor.getAttributeText(QName.valueOf("encoding")));
						
						jCasStack.push(annXmlFS);
					}
					
					// mrow tag
					else if(tagName.equals("mrow")){
						
						Mrow mrowFS = new Mrow(jcas);
						annList.add(mrowFS);
						mrowFS.addToIndexes(jcas);
						mrowFS.setBegin(begin);
						
						jCasStack.push(mrowFS);
					}
					
					// mi tag
					else if(tagName.equals("mi")){
						Mi miFS = new Mi(jcas);
					    annList.add(miFS);
					    miFS.addToIndexes(jcas);
					    miFS.setBegin(begin);
					    
					    miFS.setMathvariant(cursor.getAttributeText(QName.valueOf("mathvariant")));
					    miFS.setAnscolumn_id(cursor.getAttributeText(QName.valueOf("anscolumn_id")));
					    
					    jCasStack.push(miFS);
					}
					
					// ci tag
					else if(tagName.equals("ci")){
						
						Ci ciFS = new Ci(jcas);
						annList.add(ciFS);
						ciFS.addToIndexes(jcas);
						ciFS.setBegin(begin);
						
						ciFS.setCiType(cursor.getAttributeText(QName.valueOf("type")));
						
						jCasStack.push(ciFS);
					}
					
					// msub tag
					else if(tagName.equals("msub")){
						
						Msub msubFS = new Msub(jcas);
						annList.add(msubFS);
						msubFS.addToIndexes(jcas);
						msubFS.setBegin(begin);
						
						jCasStack.push(msubFS);
					}
					
					// apply tag
					else if(tagName.equals("apply")){
						
						Apply applyFS = new Apply(jcas);
						annList.add(applyFS);
						applyFS.addToIndexes(jcas);
						applyFS.setBegin(begin);
						
						jCasStack.push(applyFS);
					}
					
					// selector
					else if(tagName.equals("selector")){
						
						Selector selectorFS = new Selector(jcas);
						annList.add(selectorFS);
						selectorFS.addToIndexes(jcas);
						selectorFS.setBegin(begin);
						
						jCasStack.push(selectorFS);
					}
					
					// mo tag
					else if(tagName.equals("mo")){
						
						Mo moFS = new Mo(jcas);
						annList.add(moFS);
						moFS.addToIndexes(jcas);
						moFS.setBegin(begin);
						
						moFS.setStretchy(cursor.getAttributeText(QName.valueOf("stretchy")));

						jCasStack.push(moFS);
					}
					
					// mover tag
					else if(tagName.equals("mover")){
						
						Mover moverFS = new Mover(jcas);
						annList.add(moverFS);
						moverFS.addToIndexes(jcas);
						moverFS.setBegin(begin);
						
						jCasStack.push(moverFS);
					}
					
					// mn tag
					else if(tagName.equals("Mn")){
						
						Mn mnFS = new Mn(jcas);
						annList.add(mnFS);
						mnFS.addToIndexes(jcas);
						mnFS.setBegin(begin);
						
						jCasStack.push(mnFS);
					}
					
					// cn tag
					else if(tagName.equals("Cn")){
						
						Cn cnFS = new Cn(jcas);
						annList.add(cnFS);
						cnFS.addToIndexes(jcas);
						cnFS.setBegin(begin);
						
						jCasStack.push(cnFS);
					}
					
					// mfenced tag
					else if(tagName.equals("mfenced")){
						
						Mfenced mFencedFS = new Mfenced(jcas);
						annList.add(mFencedFS);
						mFencedFS.addToIndexes(jcas);
						mFencedFS.setBegin(begin);
						
						mFencedFS.setSeparators(cursor.getAttributeText(QName.valueOf("separators")));
						mFencedFS.setOpen(cursor.getAttributeText(QName.valueOf("open")));
						mFencedFS.setClose(cursor.getAttributeText(QName.valueOf("close")));
						
						jCasStack.push(mFencedFS);
					}
					
					// mn tag
					else if(tagName.equals("mn")){
						
						Mn mnFS = new Mn(jcas);
						annList.add(mnFS);
						mnFS.addToIndexes(jcas);
						mnFS.setBegin(begin);
						
						jCasStack.push(mnFS);
					}
					
					// cn tag
					else if(tagName.equals("cn")){
						
						Cn cnFS = new Cn(jcas);
						annList.add(cnFS);
						cnFS.addToIndexes(jcas);
						cnFS.setBegin(begin);
						
						jCasStack.push(cnFS);
					}
					
					// vector tag
					else if(tagName.equals("vector")){
						
						Vector vectorFS = new Vector(jcas);
						annList.add(vectorFS);
						vectorFS.addToIndexes(jcas);
						vectorFS.setBegin(begin);
						
						jCasStack.push(vectorFS);
					}
					
					// mtable tag
					else if(tagName.equals("mtable")){
						
						Mtable mTableFS = new Mtable(jcas);
						annList.add(mTableFS);
						mTableFS.addToIndexes(jcas);
						mTableFS.setBegin(begin);
						
						mTableFS.setColumnalign(cursor.getAttributeText(QName.valueOf("columnalign")));
						
						jCasStack.push(mTableFS);
					}
					
					// mtr tag
					else if(tagName.equals("mtr")){
						
						Mtr mTrFS = new Mtr(jcas);
						annList.add(mTrFS);
						mTrFS.addToIndexes(jcas);
						mTrFS.setBegin(begin);
						
						jCasStack.push(mTrFS);
					}
					
					// mtd tag
					else if(tagName.equals("mtd")){
						
						Mtd mTdFS = new Mtd(jcas); 
						annList.add(mTdFS);
						mTdFS.addToIndexes(jcas);
						mTdFS.setBegin(begin);
						
						jCasStack.push(mTdFS);
					}
					
					// mtext tag
					else if(tagName.equals("mtext")){
						
						Mtext mTextFS = new Mtext(jcas);
						annList.add(mTextFS);
						mTextFS.addToIndexes(jcas);
						mTextFS.setBegin(begin);
						
						jCasStack.push(mTextFS);
					}
					
					// piecewise tag
					else if(tagName.equals("piecewise")){
						
						Piecewise piecewiseFS = new Piecewise(jcas);
						annList.add(piecewiseFS);
						piecewiseFS.addToIndexes(jcas);
						piecewiseFS.setBegin(begin);
						
						jCasStack.push(piecewiseFS);
					}
					
					// piece tag
					else if(tagName.equals("piece")){
						
						Piece pieceFS = new Piece(jcas);
						annList.add(pieceFS);
						pieceFS.addToIndexes(jcas);
						pieceFS.setBegin(begin);
						
						jCasStack.push(pieceFS);
					}
					
					// otherwise tag
					else if(tagName.equals("otherwise")){
						
						Otherwise otherwiseFS = new Otherwise(jcas);
						annList.add(otherwiseFS);
						otherwiseFS.addToIndexes(jcas);
						otherwiseFS.setBegin(begin);
						
						jCasStack.push(otherwiseFS);
					}
					
					// plus tag
					else if(tagName.equals("plus")){
						
						Plus plusFS = new Plus(jcas);
						annList.add(plusFS);
						plusFS.addToIndexes(jcas);
						plusFS.setBegin(begin);
						
						jCasStack.push(plusFS);
					}
					
					// minus tag
					else if(tagName.equals("minus")){
						
						Minus minusFS = new Minus(jcas);
						annList.add(minusFS);
						minusFS.addToIndexes(jcas);
						minusFS.setBegin(begin);
						
						jCasStack.push(minusFS);
					}
					
					// times tag
					else if(tagName.equals("times")){
						
						Times timesFS = new Times(jcas);
						annList.add(timesFS);
						timesFS.addToIndexes(jcas);
						timesFS.setBegin(begin);
						
						jCasStack.push(timesFS);
					}
					
					// divide tag
					else if(tagName.equals("divide")){
						
						Divide divideFS = new Divide(jcas);
						annList.add(divideFS);
						divideFS.addToIndexes(jcas);
						divideFS.setBegin(begin);
						
						jCasStack.push(divideFS);
					}
					
					// power tag
					else if(tagName.equals("power")){
						
						Power powerFS = new Power(jcas);
						annList.add(powerFS);
						powerFS.addToIndexes(jcas);
						powerFS.setBegin(begin);
						
						jCasStack.push(powerFS);
					}
					
					// root tag
					else if(tagName.equals("root")){
						
						Root rootFS = new Root(jcas);
						annList.add(rootFS);
						rootFS.addToIndexes(jcas);
						rootFS.setBegin(begin);
						
						jCasStack.push(rootFS);
					}
					
					// abs tag
					else if(tagName.equals("abs")){
						
						Abs absFS = new Abs(jcas);
						annList.add(absFS);
						absFS.addToIndexes(jcas);
						absFS.setBegin(begin);
						
						jCasStack.push(absFS);
					}
					
					// conjugate tag
					else if(tagName.equals("conjugate")){
						
						Conjugate conjugateFS = new Conjugate(jcas);
						annList.add(conjugateFS);
						conjugateFS.addToIndexes(jcas);
						conjugateFS.setBegin(begin);
						
						jCasStack.push(conjugateFS);
					}
					
					// arg tag
					else if(tagName.equals("arg")){
						
						Arg argFS = new Arg(jcas);
						annList.add(argFS);
						argFS.addToIndexes(jcas);
						argFS.setBegin(begin);
						
						jCasStack.push(argFS);
					}
					
					// and tag
					else if(tagName.equals("and")){
						
						And andFS = new And(jcas);
						annList.add(andFS);
						andFS.addToIndexes(jcas);
						andFS.setBegin(begin);
						
						jCasStack.push(andFS);
					}
					
					// or tag
					else if(tagName.equals("or")){
						
						Or orFS = new Or(jcas);
						annList.add(orFS);
						orFS.addToIndexes(jcas);
						orFS.setBegin(begin);
						
						jCasStack.push(orFS);
					}
					
					// not tag
					else if(tagName.equals("not")){
						
						Not notFS = new Not(jcas);
						annList.add(notFS);
						notFS.addToIndexes(jcas);
						notFS.setBegin(begin);
						
						jCasStack.push(notFS);
					}
					
					// implies tag
					else if(tagName.equals("implies")){
						
						ImPlies imPliesFS = new ImPlies(jcas);
						annList.add(imPliesFS);
						imPliesFS.addToIndexes(jcas);
						imPliesFS.setBegin(begin);
						
						jCasStack.push(imPliesFS);
					}
					
					// forall tag
					else if(tagName.equals("forall")){
						
						Forall forAllFS = new Forall(jcas);
						annList.add(forAllFS);
						forAllFS.addToIndexes(jcas);
						forAllFS.setBegin(begin);
						
						jCasStack.push(forAllFS);
					}
					
					// eq tag
					else if(tagName.equals("eq")){
						
						Eq eqFS = new Eq(jcas); 
						annList.add(eqFS);
						eqFS.addToIndexes(jcas);
						eqFS.setBegin(begin);
						
						jCasStack.push(eqFS);
					}
					
					// neq tag
					else if(tagName.equals("neq")){
						
						Neq neqFS = new Neq(jcas);
						annList.add(neqFS);
						neqFS.addToIndexes(jcas);
						neqFS.setBegin(begin);
						
						jCasStack.push(neqFS);
					}
					
					// gt tag
					else if(tagName.equals("gt")){
						
						Gt gtFS = new Gt(jcas);
						annList.add(gtFS);
						gtFS.addToIndexes(jcas);
						gtFS.setBegin(begin);
						
						jCasStack.push(gtFS);
					}
					
					// lt tag
					else if(tagName.equals("lt")){
						
						Lt ltFS = new Lt(jcas);
						annList.add(ltFS);
						ltFS.addToIndexes(jcas);
						ltFS.setBegin(begin);
						
						jCasStack.push(ltFS);
					}
					
					// geq tag
					else if(tagName.equals("geq")){
						
						Geq geqFS = new Geq(jcas);
						annList.add(geqFS);
						geqFS.addToIndexes(jcas);
						geqFS.setBegin(begin);
						
						jCasStack.push(geqFS);
					}
					
					// leq tag
					else if(tagName.equals("leq")){
						
						Leq leqFS = new Leq(jcas);
						annList.add(leqFS);
						leqFS.addToIndexes(jcas);
						leqFS.setBegin(begin);
						
						jCasStack.push(leqFS);
					}
					
					// equivalent tag
					else if(tagName.equals("equivalent")){
						
						Equivalent equivalentFS = new Equivalent(jcas);
						annList.add(equivalentFS);
						equivalentFS.addToIndexes(jcas);
						equivalentFS.setBegin(begin);
						
						jCasStack.push(equivalentFS);
					}
					
					// factorof tag
					else if(tagName.equals("factorof")){
						
						Factorof factorofFS = new Factorof(jcas);
						annList.add(factorofFS);
						factorofFS.addToIndexes(jcas);
						factorofFS.setBegin(begin);
						
						jCasStack.push(factorofFS);
					}
					
					// diff tag
					else if(tagName.equals("diff")){
						
						Diff diffFS = new Diff(jcas);
						annList.add(diffFS);
						diffFS.addToIndexes(jcas);
						diffFS.setBegin(begin);
						
						jCasStack.push(diffFS);
					}
					
					// int tag
					else if(tagName.equals("int")){
						
						Int intFS = new Int(jcas);
						annList.add(intFS);
						intFS.addToIndexes(jcas);
						intFS.setBegin(begin);
						
						jCasStack.push(intFS);
					}
					
					// set tag
					else if(tagName.equals("set")){
						
						Set setFS = new Set(jcas);
						annList.add(setFS);
						setFS.addToIndexes(jcas);
						setFS.setBegin(begin);
						
						jCasStack.push(setFS);
					}
					
					// list tag
					else if(tagName.equals("list")){
						
						List listFS = new List(jcas);
						annList.add(listFS);
						listFS.addToIndexes(jcas);
						listFS.setBegin(begin);
						
						jCasStack.push(listFS);
					}
					
					// union tag
					else if(tagName.equals("union")){
						
						Union unionFS = new Union(jcas);
						annList.add(unionFS);
						unionFS.addToIndexes(jcas);
						unionFS.setBegin(begin);
						
						jCasStack.push(unionFS);
					}
					
					// intersect tag
					else if(tagName.equals("intersect")){
						
						Intersect intersectFS = new Intersect(jcas);
						annList.add(intersectFS);
						intersectFS.addToIndexes(jcas);
						intersectFS.setBegin(begin);
						
						jCasStack.push(intersectFS);
					}
					
					// prsubset tag
					else if(tagName.equals("prsubset")){
						
						Prsubset prsubsetFS = new Prsubset(jcas);
						annList.add(prsubsetFS);
						prsubsetFS.addToIndexes(jcas);
						prsubsetFS.setBegin(begin);
						
						jCasStack.push(prsubsetFS);
					}
					
					// sum tag
					else if(tagName.equals("sum")){
						
						Sum sumFS = new Sum(jcas);
						annList.add(sumFS);
						sumFS.addToIndexes(jcas);
						sumFS.setBegin(begin);
						
						jCasStack.push(sumFS);
					}

					// product tag
					else if(tagName.equals("product")){
						
						Product productFS = new Product(jcas);
						annList.add(productFS);
						productFS.addToIndexes(jcas);
						productFS.setBegin(begin);
						
						jCasStack.push(productFS);
					}
					
					// sin tag
					else if(tagName.equals("sin")){
						
						Sin sinFS = new Sin(jcas);
						annList.add(sinFS);
						sinFS.addToIndexes(jcas);
						sinFS.setBegin(begin);
						
						jCasStack.push(sinFS);
					}
					
					// cos tag
					else if(tagName.equals("cos")){
						
						Cos cosFS = new Cos(jcas);
						annList.add(cosFS);
						cosFS.addToIndexes(jcas);
						cosFS.setBegin(begin);
						
						jCasStack.push(cosFS);
					}
					
					// tan tag 
					else if(tagName.equals("tan")){
						
						Tan tanFS = new Tan(jcas);
						annList.add(tanFS);
						tanFS.addToIndexes(jcas);
						tanFS.setBegin(begin);
						
						jCasStack.push(tanFS);
					}
					
					// log tag
					else if(tagName.equals("log")){
						
						Log logFS = new Log(jcas);
						annList.add(logFS);
						logFS.addToIndexes(jcas);
						logFS.setBegin(begin);
						
						jCasStack.push(logFS);
					}
					
					// integers tag
					else if(tagName.equals("integers")){
						
						Integers integersFS = new Integers(jcas);
						annList.add(integersFS);
						integersFS.addToIndexes(jcas);
						integersFS.setBegin(begin);
						
						jCasStack.push(integersFS);
					}
					
					// naturalnumbers tag
					else if(tagName.equals("naturalnumbers")){
						
						Naturalnumbers naturalnumbersFS = new Naturalnumbers(jcas);
						annList.add(naturalnumbersFS);
						naturalnumbersFS.addToIndexes(jcas);
						naturalnumbersFS.setBegin(begin);
						
						jCasStack.push(naturalnumbersFS);
					}
					
					// imaginaryi tag
					else if(tagName.equals("imaginaryi")){
						
						Imaginaryi imaginaryiFS = new Imaginaryi(jcas);
						annList.add(imaginaryiFS);
						imaginaryiFS.addToIndexes(jcas);
						imaginaryiFS.setBegin(begin);
						
						jCasStack.push(imaginaryiFS);
					}
					
					// pi tag
					else if(tagName.equals("pi")){
						
						Pi piFS = new Pi(jcas);
						annList.add(piFS);
						piFS.addToIndexes(jcas);
						piFS.setBegin(begin);
						
						jCasStack.push(piFS);
					}

					// csymbol tag
					else if(tagName.equals("csymbol")){
						
						Csymbol csymbolFS = new Csymbol(jcas);
						annList.add(csymbolFS);
						csymbolFS.addToIndexes(jcas);
						csymbolFS.setBegin(begin);
						
						csymbolFS.setCd(cursor.getAttributeText(QName.valueOf("cd")));
						csymbolFS.setDefinitionURL(cursor.getAttributeText(QName.valueOf("definitionURL")));
						
						jCasStack.push(csymbolFS);
					}

					// mmultiscripts tag
					else if(tagName.equals("mmultiscripts")){
						
						Mmultiscripts mmultiscriptsFS = new Mmultiscripts(jcas);
						annList.add(mmultiscriptsFS);
						mmultiscriptsFS.addToIndexes(jcas);
						mmultiscriptsFS.setBegin(begin);
						
						jCasStack.push(mmultiscriptsFS);
					}

					// mprescripts tag
					else if(tagName.equals("mprescripts")){
						
						Mprescripts mprescriptsFS = new Mprescripts(jcas);
						annList.add(mprescriptsFS);
						mprescriptsFS.addToIndexes(jcas);
						mprescriptsFS.setBegin(begin);
						
						jCasStack.push(mprescriptsFS);
					}

					// none tag
					else if(tagName.equals("none")){
						
						None noneFS = new None(jcas);
						annList.add(noneFS);
						noneFS.addToIndexes(jcas);
						noneFS.setBegin(begin);
						
						jCasStack.push(noneFS);
					}

					// bvar tag
					else if(tagName.equals("bvar")){
						
						Bvar bvarFS = new Bvar(jcas);
						annList.add(bvarFS);
						bvarFS.addToIndexes(jcas);
						bvarFS.setBegin(begin);
						
						jCasStack.push(bvarFS);
					}

					// condition tag
					else if(tagName.equals("condition")){
						
						Condition conditionFS = new Condition(jcas);
						annList.add(conditionFS);
						conditionFS.addToIndexes(jcas);
						conditionFS.setBegin(begin);
						
						jCasStack.push(conditionFS);
					}

					// table tag
					else if(tagName.equals("table")){
						
						Table tableFS = new Table(jcas);
						annList.add(tableFS);
						tableFS.addToIndexes(jcas);
						tableFS.setBegin(begin);
						
						tableFS.setXmlns(cursor.getName().getNamespaceURI());
						
						jCasStack.push(tableFS);
					}
					
					// thead tag
					else if(tagName.equals("thead")){
						
						Thead theadFS = new Thead(jcas);
						
						annList.add(theadFS);
						theadFS.addToIndexes(jcas);
						theadFS.setBegin(begin);
						
						jCasStack.push(theadFS);
					}
					
					// tbody tag
					else if(tagName.equals("tbody")){
						
						Tbody tbodyFS = new Tbody(jcas);
						
						annList.add(tbodyFS);
						tbodyFS.addToIndexes(jcas);
						tbodyFS.setBegin(begin);
						
						jCasStack.add(tbodyFS);
					}
					
					// tr tag
					else if(tagName.equals("tr")){
						
						Tr trFS = new Tr(jcas);
						
						annList.add(trFS);
						trFS.addToIndexes(jcas);
						trFS.setBegin(begin);
						
						jCasStack.add(trFS);
					}
					
					// th tag
					else if(tagName.equals("th")){
						
						Th thFS = new Th(jcas);
						
						annList.add(thFS);
						thFS.addToIndexes(jcas);
						thFS.setBegin(begin);
						
						jCasStack.add(thFS);
					}
					
					// td tag
					else if(tagName.equals("td")){
						
						Td tdFS = new Td(jcas);
						
						annList.add(tdFS);
						tdFS.addToIndexes(jcas);
						tdFS.setBegin(begin);
						
						jCasStack.add(tdFS);
					}
					
					// mfrac tag
					else if(tagName.equals("mfrac")){
						
						Mfrac mfracFS = new Mfrac(jcas);
						annList.add(mfracFS);
						mfracFS.addToIndexes(jcas);
						mfracFS.setBegin(begin);
						
						jCasStack.push(mfracFS);
					}

					// munderorver tag
					else if(tagName.equals("munderover")){
						
						Munderorver munderorverFS = new Munderorver(jcas);
						annList.add(munderorverFS);
						munderorverFS.addToIndexes(jcas);
						munderorverFS.setBegin(begin);
						
						jCasStack.push(munderorverFS);
					}
					
					// msubsup tag
					else if(tagName.equals("msubsup")){
						
						Msubsup msubsupFS = new Msubsup(jcas);
						annList.add(msubsupFS);
						msubsupFS.addToIndexes(jcas);
						msubsupFS.setBegin(begin);
						
						jCasStack.push(msubsupFS);
					}

					// msqrt tag 
					else if(tagName.equals("msqrt")){
						
						Msqrt msqrtFS = new Msqrt(jcas);
						annList.add(msqrtFS);
						msqrtFS.addToIndexes(jcas);
						msqrtFS.setBegin(begin);
						
						jCasStack.push(msqrtFS);
					}

					// mroot tag
					else if(tagName.equals("mroot")){
						
						Mroot mrootFS = new Mroot(jcas);
						annList.add(mrootFS);
						mrootFS.addToIndexes(jcas);
						mrootFS.setBegin(begin);
						
						jCasStack.push(mrootFS);
					}

					// mstyle tag
					else if(tagName.equals("mstyle")){
						
						Mstyle mstyleFS = new Mstyle(jcas);
						annList.add(mstyleFS);
						mstyleFS.addToIndexes(jcas);
						mstyleFS.setBegin(begin);
						
						mstyleFS.setDisplaystyle(cursor.getAttributeText(QName.valueOf("displaystyle")));
						
						jCasStack.push(mstyleFS);
					}
					
					// msup tag
					else if(tagName.equals("msup")){
						
						Msup msupFS = new Msup(jcas);
						annList.add(msupFS);
						msupFS.addToIndexes(jcas);
						msupFS.setBegin(begin);
						
						jCasStack.push(msupFS);
					}
					
					// scalarproduct tag
					else if(tagName.equals("scalarproduct")){
						
						ScalarProduct scalarProductFS = new ScalarProduct(jcas);
						annList.add(scalarProductFS);
						scalarProductFS.addToIndexes(jcas);
						scalarProductFS.setBegin(begin);
						
						jCasStack.push(scalarProductFS);
						
					}
					
					// in tag
					else if(tagName.equals("in")){
						
						In inFS = new In(jcas);
						annList.add(inFS);
						inFS.addToIndexes(jcas);
						inFS.setBegin(begin);
						
						jCasStack.push(inFS);
					}
					
					// lowLimit tag
					else if(tagName.equals("lowlimit")){
						
						LowLimit lowLimitFS = new LowLimit(jcas);
						annList.add(lowLimitFS);
						lowLimitFS.addToIndexes(jcas);
						lowLimitFS.setBegin(begin);
						
						jCasStack.push(lowLimitFS);
					}
					
					// upLimit tag
					else if(tagName.equals("uplimit")){
						
						UpLimit upLimitFS = new UpLimit(jcas);
						annList.add(upLimitFS);
						upLimitFS.addToIndexes(jcas);
						upLimitFS.setBegin(begin);
						
						jCasStack.push(upLimitFS);
					}
					
					// logbase tag
					else if(tagName.equals("logbase")){
						
						LogBase logBaseFS = new LogBase(jcas);
						annList.add(logBaseFS);
						logBaseFS.addToIndexes(jcas);
						logBaseFS.setBegin(begin);
						
						jCasStack.push(logBaseFS);
					}
					
					// card tag
					else if(tagName.equals("card")){
						
						Card cardFS = new Card(jcas);
						annList.add(cardFS);
						cardFS.addToIndexes(jcas);
						cardFS.setBegin(begin);
						
						jCasStack.push(cardFS);
						
					}
					
					// degree tag
					else if(tagName.equals("degree")){
						
						Degree degreeFS = new Degree(jcas);
						annList.add(degreeFS);
						degreeFS.addToIndexes(jcas);
						degreeFS.setBegin(begin);
						
						jCasStack.push(degreeFS);
					}
					
					// mspace tag
					else if(tagName.equals("mspace")){
						
						Mspace mspaceFS = new Mspace(jcas);
						annList.add(mspaceFS);
						mspaceFS.addToIndexes(jcas);
						mspaceFS.setBegin(begin);
						
						mspaceFS.setWidth(cursor.getAttributeText(QName.valueOf("width")));
						
						jCasStack.push(mspaceFS);
						
					}
					
					// adata tag
					else if(tagName.equals("adata")){
						Adata adataFS = new Adata(jcas);
						annList.add(adataFS);
						adataFS.addToIndexes(jcas);
						adataFS.setBegin(begin);
						
						adataFS.setId(cursor.getAttributeText(QName.valueOf("id")));
						adataFS.setAdataType(cursor.getAttributeText(QName.valueOf("type")));
						adataFS.setField(cursor.getAttributeText(QName.valueOf("field")));
						
						jCasStack.push(adataFS);
					}
					
					// okuriL tag
					else if(tagName.equals("okuriL")){
						OkuriL okuriLFS = new OkuriL(jcas);
						annList.add(okuriLFS);
						okuriLFS.addToIndexes(jcas);
						okuriLFS.setBegin(begin);
						
						jCasStack.push(okuriLFS);
					}
					
					// okuri tag
					else if(tagName.equals("okuri")){
						Okuri okuriFS = new Okuri(jcas);
						annList.add(okuriFS);
						okuriFS.addToIndexes(jcas);
						okuriFS.setBegin(begin);
						
						jCasStack.push(okuriFS);
					}
					
					// sub tag
					else if(tagName.equals("sub")){
						Sub subFS = new Sub(jcas);
						annList.add(subFS);
						subFS.addToIndexes(jcas);
						subFS.setBegin(begin);
						
						jCasStack.push(subFS);
					}
					
					// sup tag
					else if(tagName.equals("sup")){
						Sup supFS = new Sup(jcas);
						annList.add(supFS);
						supFS.addToIndexes(jcas);
						supFS.setBegin(begin);
						
						jCasStack.push(supFS);
					}
					
					// kaeri tag
					else if(tagName.equals("kaeri")){
						Kaeri kaeriFS = new Kaeri(jcas);
						annList.add(kaeriFS);
						kaeriFS.addToIndexes(jcas);
						kaeriFS.setBegin(begin);
						
						jCasStack.push(kaeriFS);
					}
					
					//未定義タグ表示
					else{
						System.err.println("ExamReader: Undefined tag " + tagName);
					}

					break;
					
				// テキスト追加	
				case TokenType.INT_TEXT:
					CenterExamDocument.append(cursor.getChars());	
					break;
					
				// Annotation の終了位置をセット
				case TokenType.INT_END:
					end = CenterExamDocument.length();
					if(!jCasStack.empty()){
						annotation = ((Annotation) jCasStack.pop());
						annotation.setEnd(end);
						endList.add(annotation);
					}
					break;
			
				default:
					break;
				}
			}
			
			String Id;
			Annotation tmp;
			
			// ref tag にリンク先の CAS をセット
			for (int i = 0; i < refList.size(); i++) {
				
				Ref refFS = refList.get(i);
				Id = refFS.getFeatureValueAsString(refFS.getType().getFeatureByBaseName("target"));
				tmp = refMap.get(Id);
				
				if(tmp != null)
					refFS.setReference(tmp);

			}
			
			TagList tagListFS = new TagList(jcas);
			EndList endListFS = new EndList(jcas);
			tagListFS.addToIndexes();
			endListFS.addToIndexes();
			FSArray fsArray = new FSArray(jcas, annList.size());
			FSArray fsEnd = new FSArray(jcas, endList.size());
			
			// fsArray をセット
			for (int i = 0; i < annList.size(); i++) {
			
				fsArray.set(i, annList.get(i));
				fsEnd.set(i, endList.get(i));
				
			}
			tagListFS.setAnnotationList(fsArray);
			endListFS.setAnnotationList(fsEnd);
			
			cursor.dispose();
			
			// 正答表があれば正答データをセット
			if ( ansTableDirPath != null && ansTableDirPath.length() > 0){
				File aFile = answerTableFileMap.get(file.getName());
				if( aFile != null){
					org.kachako.components.centerexam.answertable.AnswerTableReader.readAnswerTable(jcas, aFile, numOfOptions, rangeOfOptions);
					setChoiceRa(jcas);
				}
			}
			
			jcas.setDocumentText(CenterExamDocument.toString());
			SourceDocumentInformation sourceDocumentInformation = new SourceDocumentInformation(jcas);
			sourceDocumentInformation.setUri(file.toURI().toString());
			sourceDocumentInformation.addToIndexes();
			
		} catch (XmlException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}

	private void setChoiceRa(JCas jcas) {
		String anscol_ids = null;
		int index = 0;
//		ArrayList<Annotation> fsList = new ArrayList<Annotation>();
		HashMap<String, String> answerMap = new HashMap<String, String>();
		Type type = jcas.getTypeSystem().getType(TagList.class.getName());
		FSIterator fsIterator = jcas.getFSIndexRepository().getAllIndexedFS(type);
		
		if(!fsIterator.hasNext()) return;
			
		FSArray fsArray = ((TagList)fsIterator.next()).getAnnotationList();
		answerMap = getAnswerMap(jcas);
			
//		System.out.println(answerMap);
			
		for (int i = 0; i < fsArray.size(); i++) {
			Annotation ann = (Annotation)fsArray.get(i);
			String annName = ann.getType().getName();
			if(annName.equals(Choices.class.getName())){
				anscol_ids = ((Choices)ann).getAnscol();
				index = 0;
			}
			else if(annName.equals(Choice.class.getName())){
				index++;
				Choice choice = (Choice)ann;
				choice.setRa("no");
				if(anscol_ids != null){
					for(String id : anscol_ids.split(" ")){
						if(answerMap.containsKey(id) && answerMap.get(id).equals(String.valueOf(index))){
							choice.setRa("yes");
							break;
						}
					}
				}
			}
		}
		
	}
	
	private HashMap<String, String> getAnswerMap(JCas jcas){
		HashMap<String, String> answerMap = new HashMap<String, String>();
		String answer = null;
		String anscol_id = null;
		Type type = jcas.getTypeSystem().getType(AnswerTableAnnotationList.class.getName());
		FSIterator<FeatureStructure> fsIterator = jcas.getFSIndexRepository().getAllIndexedFS(type);

		if(fsIterator.hasNext()){
			FSArray fsArray = ((AnswerTableAnnotationList) fsIterator.next()).getBeginList();
			for(int i = 0; i < fsArray.size(); i++){
				Annotation ann = (Annotation) fsArray.get(i);
				String annName = ann.getType().getName();
				if(annName.equals(DataAT.class.getName())){
					if(answer != null && anscol_id != null){
						String[] ids = split(anscol_id);
						String[] answers = split(answer);
						for (int j = 0; j < ids.length; j++) {
							if(j < answers.length)
								answerMap.put(ids[j], answers[j]);
							else{
								System.err.println("Failed split AsnwerTable data: id=" + anscol_id + "→" + Arrays.toString(ids) + "(" + ids.length + ")"
										+ " answer=" + answer + "→" + Arrays.toString(answers) + "(" + answers.length + ")");
								break;
							}
						}
						answer = null;
						anscol_id = null;
					}
				}
				else if(annName.equals(Answer.class.getName())){
					answer = ((Answer)ann).getString();
				}
				else if(annName.equals(AnswerColumnID.class.getName())){
					anscol_id = ((AnswerColumnID)ann).getString();
				}
			}
			if(answer != null && anscol_id != null){
				String[] ids = split(anscol_id);
				String[] answers = split(answer);
				for (int j = 0; j < ids.length; j++) {
					if(j < answers.length)
						answerMap.put(ids[j], answers[j]);
					else{
						System.err.println("Failed split AsnwerTable data: id=" + anscol_id + "→" + Arrays.toString(ids) + "(" + ids.length + ")"
								+ " answer=" + answer + "→" + Arrays.toString(answers) + "(" + answers.length + ")");
						break;
					}
				}
			}
		}
		return answerMap;
	}
	
	public static String[] split(String str){
		
		String[] args;
		final String RANDOM_ORDER = "\\|";
		final String MULTIPLE_CORRECT ="\\|\\|";
		final String EXACT_MATCH_ORDER =",";
		
		Matcher exactMatcher = Pattern.compile(EXACT_MATCH_ORDER).matcher(str);
		Matcher multipleMatcher = Pattern.compile(MULTIPLE_CORRECT).matcher(str);
		Matcher randomMatcher = Pattern.compile(RANDOM_ORDER).matcher(str);
		
		if(exactMatcher.find())
			args = str.split(EXACT_MATCH_ORDER);
		else if(multipleMatcher.find())
			args = str.split(MULTIPLE_CORRECT);
		else if(randomMatcher.find())
			args = str.split(RANDOM_ORDER);
		else{
			args = new String[1];
			args[0] = str;
		}
//		System.out.println(str + ": " + Arrays.toString(args) + "(" + args.length + ")     " + exactMatcher.find() + " " + multipleMatcher.find() + " " + randomMatcher.find());
		return args;
	}

	@Override
	public void close() throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Progress[] getProgress() {
		// TODO Auto-generated method stub
		return new Progress[]{ new ProgressImpl(mCorrentIndex, mFiles.size(), Progress.ENTITIES) };
	}

	@Override
	public boolean hasNext() throws IOException, CollectionException {
		// TODO Auto-generated method stub
		return mCorrentIndex < mFiles.size();
	}
	
	public int getNumberOfDocument(){
		return mFiles.size();
	}
	

}
