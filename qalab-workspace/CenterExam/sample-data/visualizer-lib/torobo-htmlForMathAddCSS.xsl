<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:m="http://www.w3.org/1998/Math/MathML">
<xsl:output doctype-system="torobo.dtd" method="html" version="5.0" encoding="UTF-8" />

<xsl:variable name="DEBUG" select="true()"/>

<xsl:template match="/">
    <div class="exam_body">
      <xsl:apply-templates />  
  	</div>
    <script type="text/javascript">
      function changeDisplay(el){
        var target_el = getNextSibling(el.firstChild);
        if (target_el.style.display != 'block'){
          target_el.style.display = 'block';
        } else {
          target_el.style.display = 'none';
        }
      }
      function getNextSibling(el){
        var next_el = el.nextSibling;
        while(next_el.className != 'exam_question_evidence'){
          next_el = next_el.nextSibling;
        }
        return next_el;
      }
    </script>
</xsl:template>

<xsl:template match="@* | *">
  <xsl:copy>
    <xsl:apply-templates select="@* | node()"/>
  </xsl:copy>
</xsl:template>

<xsl:template match="m:math[not(@display)]">
  <font color="red">
  <m:math> 
    <xsl:attribute name="style">margin-left:0.5em;</xsl:attribute>
    <xsl:if test="current()//m:mfrac">
      <xsl:attribute name="class">frac</xsl:attribute>
    </xsl:if>
    <xsl:apply-templates />
  </m:math>
  </font>
</xsl:template>

<xsl:template match="m:math[(@display)]">
  <div display="block">
  <font color="red">
  <m:math> 
    <xsl:if test="current()//m:mfrac">
      <xsl:attribute name="class">frac</xsl:attribute>
    </xsl:if>
    <xsl:apply-templates />
  </m:math>
  </font>
  </div>
</xsl:template>

<xsl:template match="m:semantics/m:mrow[.//m:mfrac or .//m:munderover or .//m:msubsup or .//m:msqrt or .//m:mroot]">
  <m:mstyle displaystyle="true">
    <xsl:apply-templates />
  </m:mstyle>
</xsl:template>

<xsl:template match="m:mrow//m:mrow[count(*) = 1 and count(@*) = 0]">
  <xsl:apply-templates />
</xsl:template>

<xsl:variable name="KANASET" select="'アイウエオカキクケコサシスセソタチツテトナニヌネノハヒフヘホ'" />

<xsl:template match="ref[contains($KANASET, substring(text(),1,1))
  and (string-length(text()) &gt; 1)]">
  <ref class="refkana2">
    <xsl:apply-templates select="@* | node()"/>
  </ref>
</xsl:template>

<xsl:template name="createMathblank">
  <xsl:param name="value"></xsl:param>
    <xsl:choose>
      <xsl:when test="string-length(text()) &gt; 1">
	<xsl:attribute name="class">mathblank kana2<xsl:value-of select="$value"/></xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
	<xsl:attribute name="class">mathblank<xsl:value-of select="$value"/></xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="m:mi[contains($KANASET, substring(text(),1,1))]">
  <xsl:if test="parent::m:msqrt">
    <m:mspace width=".3em"/>
  </xsl:if>
  <m:mi>
    <xsl:choose>
      <xsl:when test="(text() = preceding-sibling::*/descendant-or-self::m:mi/text() or text() = ancestor::*[ancestor::question]/preceding-sibling::*//m:mi/text() or  text() = ancestor::*[ancestor::question]/preceding-sibling::*/descendant-or-self::blank/text())">
	<xsl:call-template name="createMathblank">
	  <xsl:with-param name="value"> ref</xsl:with-param>
	</xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
	<xsl:call-template name="createMathblank"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:apply-templates select="@* | node()"/>
    <m:mspace height="1.1em" depth=".4em"/>
  </m:mi>
  <xsl:if test="parent::m:msqrt">
    <m:mspace width=".3em"/>
  </xsl:if>
</xsl:template>

<xsl:template match="m:mi[string-length(text()) = 1 and
   contains('ABCDEFGHIJKLMNOPQRSTUVWXYZ', text()) and (contains('O', text()) or preceding-sibling::m:mo/text() = '&#8736;') ]">
  <m:mi mathvariant="normal">
    <xsl:apply-templates select="@* | node()"/>
  </m:mi>
</xsl:template>

<xsl:template match="m:mi[text() = '&#xB0;']">
  <m:mi mathvariant="normal" class="MathML-Unit">
    <xsl:apply-templates select="@* | node()"/>
  </m:mi>
</xsl:template>

<xsl:template match="m:mrow[preceding-sibling::m:mo/text() = '&#x2062;']">
  <xsl:apply-templates />
</xsl:template>

<xsl:template match="m:mfenced[@separators='']">
 <m:mfenced>
  <xsl:for-each select="@*">
    <xsl:copy />
  </xsl:for-each>
  <m:mrow>
   <xsl:apply-templates />
  </m:mrow>
 </m:mfenced>
</xsl:template>

<xsl:template match="m:mfrac">
  <m:mfrac>
  <m:mrow><m:mspace width=".3em"/>
    <xsl:apply-templates select="*[1]" />
  <m:mspace width=".3em"/></m:mrow>
  <m:mrow><m:mspace width=".3em"/>
    <xsl:apply-templates select="*[2]" />
  <m:mspace width=".3em"/></m:mrow>
</m:mfrac>
</xsl:template>

<xsl:template match="m:mfenced[not(@open='') and not(@close='') and (count(*) = 1 or @separators='')]">
  <m:mrow><m:mo lspace=".2em" rspace=".2em">
     <xsl:if test="not(.//m:mfrac or .//m:msup or .//m:mi[contains($KANASET, substring(text(),1,1))])">
	   <xsl:attribute name="stretchy">false</xsl:attribute>
     </xsl:if>
     <xsl:if test="not(@open)">(</xsl:if>
     <xsl:value-of select="@open"/></m:mo>
     <xsl:apply-templates select="*"/>
     <m:mo lspace=".2em" rspace=".2em">
     <xsl:if test="not(.//m:mfrac or .//m:msup or .//m:mi[contains($KANASET, substring(text(),1,1))])">
	   <xsl:attribute name="stretchy">false</xsl:attribute>
     </xsl:if>
     <xsl:if test="not(@open)">)</xsl:if>
     <xsl:value-of select="@close"/></m:mo>
  </m:mrow>
</xsl:template>

<xsl:template match="m:mfenced[not(@open='') and not(@close='') and not(@separators) and count(*) &gt; 1 and not(../m:sup or ../m:sub)]">
  <xsl:if test="not(preceding-sibling::m:mo) and preceding-sibling::m:mi[string-length(text()) = 1 and contains('ABCDEFGHIJKLMNOPQRSTUVWXYZ', text())]">
    <m:mspace width=".5em"/>
  </xsl:if>
  <m:mrow>
    <m:mo>
      <xsl:if test="not(.//m:mfrac or .//m:sup or .//m:mi[contains($KANASET, substring(text(),1,1))])">
	<xsl:attribute name="stretchy">false</xsl:attribute>
      </xsl:if>
      <xsl:if test="not(@open)">(</xsl:if>
      <xsl:value-of select="@open"/></m:mo>
    <m:mrow><m:mspace width=".2em"/>
      <xsl:apply-templates select="*[position() = 1]"/>
    </m:mrow>
    <xsl:for-each select="*[position() != 1 and position() != last()]">
      <m:mo lspace=".2em">,</m:mo>
      <m:mrow><m:mspace width=".2em"/>
	<xsl:copy>
	  <xsl:apply-templates select="@* | node()"/>
	</xsl:copy>
      </m:mrow>
    </xsl:for-each>
    <m:mo lspace =".2em">,</m:mo>
    <m:mrow><m:mspace width=".2em"/>
      <xsl:apply-templates select="*[position() = last()]"/>
      <m:mspace width=".2em"/></m:mrow>
    <m:mo>
      <xsl:if test="not(.//m:mfrac or .//m:msup or .//m:mi[contains($KANASET, substring(text(),1,1))])">
	<xsl:attribute name="stretchy">false</xsl:attribute>
      </xsl:if>
      <xsl:if test="not(@open)">)</xsl:if>
      <xsl:value-of select="@close"/></m:mo>
  </m:mrow>
</xsl:template>


<xsl:template match="m:mfenced[@open='|' and @close='|' and @separators and not(.//m:mfrac) and preceding-sibling::m:mo/text() = '&#8290;' ]">
  <m:mo stretchy="false" rspace=".2em">|</m:mo>
  <xsl:apply-templates />
  <m:mo stretchy="false" lspace=".2em">|</m:mo>
</xsl:template>




<xsl:template match="m:mo[text()=':']">
  <m:mo lspace=".2em" rspace=".2em">:</m:mo>
</xsl:template>

<xsl:template match="m:semantics[.//m:csymbol[not(@cd)]//*]">
  <m:mrow><m:ci>[CSYMBOL]</m:ci> <xsl:apply-templates /></m:mrow>
</xsl:template>

<xsl:template match="m:semantics[.//m:leq/following-sibling::*//m:lt or .//m:lt/following-sibling::*//m:leq or .//m:geq/following-sibling::*//m:gt or .//m:gt/following-sibling::*//m:geq or
  .//m:leq/following-sibling::*//m:leq or .//m:lt/following-sibling::*//m:lt]">
  <m:mrow><m:ci>[ERROR CM]</m:ci> <xsl:apply-templates /></m:mrow>
</xsl:template>

<xsl:template match="m:semantics[./m:annotation-xml[.//m:mrow or .//m:mfenced or .//m:mi or .//m:mn or .//m:msub or .//m:msup or .//m:mover or .//m:mo or .//m:mtext or .//m:mfrac or .//m:msqrt or .//mroot or .//m:menclose or .//m:munder or .//m:munderover or .//m:mmultiscripts]]">
  <m:mrow><m:ci>[ERROR CM]</m:ci> <xsl:apply-templates /></m:mrow>
</xsl:template>


<xsl:template match="label[string-length(text()) = 1]
  [contains('①②③④', text())]">
  <label class="number">
   <xsl:apply-templates select="@*|node()"/>
  </label>
</xsl:template>
<xsl:template match="ref[not(@class)][string-length(text()) = 1]
  [contains('①②③④', text())]">
  <ref class="number">
   <xsl:apply-templates select="@*|node()"/>
  </ref>
</xsl:template>

<xsl:template match="m:mi/text()[self::node() = 'l']">&#x2113;</xsl:template>


<xsl:template match="question[
  @sectionId='7000M24' or @sectionId='7000M43' or 
  @sectionId='7000M44' or
  @sectionId='7000M64' or
  (parent::question/@sectionId='7000M82' and @id='Q5') or
  @sectionId='7000M83' or @sectionId='7000M84' or
  (parent::question/@sectionId='8000M22' and @id='Q6') or
  @sectionId='8000M43' or @sectionId='8000M61' or
  (parent::question/@sectionId='8000M62' and @id='Q6') or
  @sectionId='8000M64' or @sectionId='8000M81' or
  (parent::question/@sectionId='8000M82' and @id='Q5') or
  @sectionId='8000M83' or @sectionId='9000M43' or
  (parent::question/@sectionId='9000M61' and @id='Q3') or
  @sectionId='9000M64' or @sectionId='9000M83' or
  (parent::question/@sectionId='A000M21' and @id='Q3') or
  @sectionId='A000M24' or @sectionId='A000M43' or
  @sectionId='A000M44' or @sectionId='A000M64' or
  @sectionId='A000M83' or
  (parent::question/@sectionId='B000M22' and @id='Q6') or
  @sectionId='B000M24' or @sectionId='B000M43' or
  @sectionId='B000M64' or @sectionId='B000M83' or
  @sectionId='B000M84' or @sectionId='C000M43' or
  @sectionId='C000M61' or
  (parent::question/@sectionId='C000M62' and @id='Q6') or
  @sectionId='C000M64' or 
  @sectionId='C000M84' or
  (parent::question/@sectionId='D000M21' and @id='Q3') or
  (parent::question/@sectionId='D000M22' and @id='Q6') or
  @sectionId='D000M24' or @sectionId='D000M43' or
  @sectionId='D000M44' or
  (parent::question/@sectionId='D000M62' and @id='Q6') or
  @sectionId='D000M64' or @sectionId='D000M83' or
  @sectionId='D000M84' or
  (parent::question/@sectionId='E000M22' and @id='Q6') or
  @sectionId='E000M24' or @sectionId='E000M43' or
  @sectionId='E000M44' or
  (parent::question/@sectionId='E000M62' and @id='Q6') or
  @sectionId='E000M64' or @sectionId='E000M83' or
  @sectionId='E000M84' or
  (parent::question/@sectionId='F000M22' and @id='Q6') or
  @sectionId='F000M24' or @sectionId='F000M41' or
  @sectionId='F000M43' or @sectionId='F000M44' or
  (parent::question/@sectionId='F000M62' and @id='Q6') or
  @sectionId='F000M64' or @sectionId='F000M83' or
  @sectionId='G000M33' or @sectionId='G000M41' or
  @sectionId='G000M43' or @sectionId='G000M84' or
  @sectionId='H000M34' or @sectionId='H000M43' or
  @sectionId='H000M44' or @sectionId='H000M74' or
  @sectionId='H000M84' or
  @sectionId='J000M33' or @sectionId='J000M43' or
  @sectionId='J000M74' or @sectionId='J000M84' or
  @sectionId='K000M33' or @sectionId='K000M43' or 
  @sectionId='K000M74' or @sectionId='L000M33' or 
  @sectionId='L000M74' or @sectionId='M000M33' or 
  @sectionId='M000M74']
  //m:mi[string-length(text()) = 1 and contains('ABCDEFGHIJKLMNOPQRSTUVWXYZ', text())]">
  <m:mi mathvariant="normal">
    <xsl:apply-templates />
  </m:mi>
</xsl:template>

<xsl:template match="question[
  @sectionId='7000M21' or
  @sectionId='8000M42' or @sectionId='8000M82' or
  @sectionId='9000M24' or @sectionId='9000M42' or
  @sectionId='9000M82' or @sectionId='A000M82' or
  @sectionId='A000M84' or @sectionId='B000M42' or
  @sectionId='B000M61' or @sectionId='C000M42' or
  @sectionId='C000M82' or @sectionId='C000M83' or
  @sectionId='D000M42' or
  @sectionId='F000M42' or @sectionId='G000M72' or
  (parent::question/@sectionId='J000M71' and @id='Q3') or 
  @sectionId='J000M82' or
  @sectionId='K000M82' or
  @sectionId='K000M84'
  ]//m:mi[string-length(text()) = 1 and contains('ABDEFGHIJKLMNOPQRUVWXYZ', text())]">
  <m:mi mathvariant="normal">
    <xsl:apply-templates />
  </m:mi>
</xsl:template>

<xsl:template match="question[
  @sectionId='D000M82']//m:mi[string-length(text()) = 1 and contains('ABEFGHIJKLMNOPQRUVWXYZ', text())]">
  <m:mi mathvariant="normal">
    <xsl:apply-templates />
  </m:mi>
</xsl:template>

<xsl:template match="question[
  @sectionId='8000M24']//m:mi[string-length(text()) = 1 and contains('ABCDEFGHIJKLMNOPQRUVWXYZ', text())]">
  <m:mi mathvariant="normal">
    <xsl:apply-templates />
  </m:mi>
</xsl:template>

<xsl:template match="question[@sectionId='E000M42']//m:mi[string-length(text()) = 1 and contains('ABEFGHIJKLMNOPQRSTUVWXYZ', text())]">
  <m:mi mathvariant="normal">
    <xsl:apply-templates />
  </m:mi>
</xsl:template>

<xsl:template match="question[@sectionId='E000M82' or
  @sectionId='H000M42' or
  @sectionId='H000M72' or
  @sectionId='H000M82' or
  @sectionId='J000M72' or
  @sectionId='L000M72' or
  @sectionId='M000M72' or
  @sectionId='M000M73'  
]//m:mi[string-length(text()) = 1 and contains('PQ', text())]">
  <m:mi mathvariant="normal">
    <xsl:apply-templates />
  </m:mi>
</xsl:template>

<xsl:template match="question[@sectionId='C000M24']//m:mi[string-length(text()) = 1 and contains('ABCDEFGHIJKLMNOPQSTUVWXYZ', text())]">
  <m:mi mathvariant="normal">
    <xsl:apply-templates />
  </m:mi>
</xsl:template>

<xsl:template match="question[@sectionId='A000M41' or
  @sectionId='H000M33' or @sectionId='H000M81' or
  @sectionId='J000M31'
  ]//m:mi[string-length(text()) = 1 and contains('ABCDEFGHIJKLMNOPQRTUVWXYZ', text())]">
  <m:mi mathvariant="normal">
    <xsl:apply-templates />
  </m:mi>
</xsl:template>

<xsl:template match="question[@sectionId='J000M81']//m:mi[string-length(text()) = 1 and contains('AOP', text())]">
  <m:mi mathvariant="normal">
    <xsl:apply-templates />
  </m:mi>
</xsl:template>

<xsl:template match="question[@sectionId='K000M72']//m:mi[string-length(text()) = 1 and contains('ABHPQRS', text()) and not(following-sibling::m:mo)]">
  <m:mi mathvariant="normal">
    <xsl:apply-templates />
  </m:mi>
</xsl:template>

<xsl:template match ="exam">
	<xsl:apply-templates />
</xsl:template>

<xsl:template match="title">
  <h1>
	<b><font style="background-color:#bbffbb">
		<xsl:value-of select="." />
	</font></b>
  </h1>
</xsl:template>

<xsl:template match="question">
	<div>
	    <xsl:if test="@minimal = 'no'">
			<xsl:attribute name="class">
				<xsl:text>exam_section</xsl:text>
			</xsl:attribute>
		</xsl:if>
		<xsl:if test="@minimal = 'yes'">
			<xsl:attribute name="class">
				<xsl:text>exam_question</xsl:text>
			</xsl:attribute>
		</xsl:if>
	<xsl:apply-templates />
	<xsl:if test="@minimal = 'yes'">
	    <div name = "answer_type" class="exam_question_answer_knowledge_type">
			<font color="#ff0099">
		    	<xsl:value-of select="concat('Answer_Type：',@answer_type)" />
			<!-- </div> -->
			<!-- <div name = "knowledge_type" style="display:inline" class="exam_question_answer_knowledgeType"> -->
				<xsl:value-of select="concat('　　Knowledge_Type：',@knowledge_type)" />
			</font>
		</div>
	</xsl:if>
	<br />
	</div>
	<br />
</xsl:template>

<xsl:template match ="instruction">
	<div>
		<xsl:attribute name="class">
			<xsl:if test = "parent::question/@minimal = 'no'">
				<xsl:text>exam_instruction</xsl:text>
			</xsl:if>
			<xsl:if test = "parent::question/@minimal = 'yes'">
				<xsl:text>exam_question_sentence</xsl:text>
			</xsl:if>
		</xsl:attribute>
		<xsl:apply-templates />	
	</div>
</xsl:template>

<xsl:template match="choices">
  <div style="margin : 1em 0; margin-left : 2em; display : block; text-indent : 0; border:1px solid green">
	<xsl:apply-templates />
  </div>
</xsl:template>

<xsl:template match="choice">
  <div style="display : inline-block; margin-left : 0.5%;">
    <xsl:apply-templates />
  </div>
</xsl:template>

<xsl:template match="cNum">
  <div style="margin-right : 0.5em; display: inline;">
    <xsl:apply-templates />
  </div>
 </xsl:template>

<xsl:template match="ansColumn">
	<font class="exam_question_answer_column">
		<xsl:text> </xsl:text>
		<xsl:value-of select="." />
		<xsl:text> </xsl:text>
	</font>
</xsl:template>

<xsl:template match="uText">
	<u>
		<xsl:apply-templates />
	</u>
</xsl:template>

<xsl:template match = "blank">
	<font style="border-style:solid;border-color:black;border-width:1px;height:1">
		<xsl:text> </xsl:text>
		<xsl:value-of select="." />
		<xsl:text> </xsl:text>
	</font>
</xsl:template>

<xsl:template match ="label">
	<b>
		<xsl:if test = "parent::question/@minimal = 'no'">
			<xsl:attribute name="class">
				<xsl:text>exam_section_number</xsl:text>
			</xsl:attribute>
		</xsl:if>
		<xsl:if test = "parent::question/@minimal = 'yes'">
			<xsl:attribute name="class">
				<xsl:text>exam_question_number</xsl:text>
			</xsl:attribute>
		</xsl:if>
		<xsl:value-of select="." />
	</b>
</xsl:template>

<xsl:template match ="img">
	<div class="exam_sentence_image">
	<img>
		<xsl:attribute name="src">
			<xsl:value-of select="./@src" />
		</xsl:attribute>
	</img>
	</div>
</xsl:template>

<xsl:template match ="tbl">
	<div style="margin:1em 3% 1em 3%;">
	<table border="2px">
		<xsl:for-each select="row">
			<tr>
				<xsl:for-each select="cell">
					<td>
					<xsl:choose>
						<xsl:when test="name() = 'blank'">
								<font style="border-style:solid;border-color:black;border-width:2px;height:1">
								<xsl:text> </xsl:text>
								<xsl:value-of select="." />
								<xsl:text> </xsl:text>
								</font>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="." />
						</xsl:otherwise>
					</xsl:choose>
					</td>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</table>
	</div>
</xsl:template>

<xsl:template match ="data">
	<div>
		<xsl:attribute name="class">
			<xsl:if test = "parent::question/@minimal = 'no'">
				<xsl:text>exam_sentence</xsl:text>
			</xsl:if>
			<xsl:if test = "parent::question/@minimal = 'yes'">
				<xsl:text>exam_question_sentence</xsl:text>
			</xsl:if>
		</xsl:attribute>
	   <xsl:apply-templates />	
	</div>
</xsl:template>

<xsl:template match="ref">
	<font color="blue"><b>
	<xsl:element name="span">
		<xsl:attribute name="title">
			<xsl:value-of select="@target" />
		</xsl:attribute>
		<xsl:value-of select="." />
	</xsl:element>
	</b></font>
</xsl:template>


<xsl:template match ="result_table">
  <br />
  <table style="border:1px solid; border-collapse:collapse;">
	<xsl:apply-templates />
  </table>
  <br />
</xsl:template>

<xsl:template match ="result_tr">
    <tr style="border:1px solid; border-collapse:collapse;">
        <xsl:apply-templates>
          <xsl:with-param name="machine" select="@id" />
        </xsl:apply-templates>
    </tr>
</xsl:template>    
    
<xsl:template match ="result_th">
    <th style="border:1px solid;">
   		<xsl:value-of select="." />
    </th>
</xsl:template>

<xsl:template match ="result_td">
    <xsl:param name="machine" />
    <td style="border:1px solid;">
      <xsl:choose>
        <xsl:when test="$machine='correct'">
          <font style="padding:2px;">
          <xsl:value-of select="." />
          </font>
        </xsl:when>
        <xsl:when test="@ra='yes'">
          <font class="exam_question_answer_correct">
     		<xsl:value-of select="." />
          </font>
        </xsl:when>
        <xsl:otherwise>
          <font class="exam_question_answer_wrong">
            <xsl:value-of select="." />
          </font>
        </xsl:otherwise>
      </xsl:choose>
    </td>
</xsl:template>

<xsl:template match ="br">
	<br />
</xsl:template>

<xsl:template match="process_log">
  <div class="exam_question_random_answer" onclick="changeDisplay(this)">
  <xsl:attribute name="name">
    <xsl:value-of select="@id" />
  </xsl:attribute>
  <font color="blue" onmouseover="this.color='red'" onmouseout="this.color='blue'" class="ctext">
    <xsl:value-of select="concat('Process_Log(', @id, ')')" />
  </font>
  <div class="exam_question_evidence">
    <xsl:value-of select="." />
  </div>
  <br />
  </div>
</xsl:template>

</xsl:stylesheet>