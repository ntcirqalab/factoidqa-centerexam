function replaceAnswerType(){
	answerType = document.getElementsByName("answer_type")
	for(i=0; i<answerType.length; i++){
		str = answerType[i].innerHTML
		str = str.replace("sentence", "文")
		str = str.replace("term_person","人名")
		str = str.replace("term_location","地名")
		str = str.replace("term_time","年代")
		str = str.replace("term_other","その他用語")
		str = str.replace("referenceSymbol","参照記号")
		str = str.replace("image_graph","グラフ")
		str = str.replace("image_photo","写真")
		str = str.replace("image_map","地図")
		str = str.replace("image_table","表")
		str = str.replace("image_other","その他画像")
		str = str.replace("comb_sentences","文の組み合わせ")
		str = str.replace("comb_sentence_term","文と用語の組み合わせ")
		str = str.replace("comb_sentence_TF","文と正誤の組み合わせ")
		str = str.replace("comb_terms","用語の組み合わせ")
		str = str.replace("comb_images","記号と画像の組み合わせ")
		str = str.replace("comb_symbol_sentence","記号と文の組み合わせ")
		str = str.replace("comb_symbol_term","記号と用語の組み合わせ")
		str = str.replace("comb_symbol_TF","記号と正誤の組み合わせ")
		str = str.replace("comb_symbol_image","記号と画像の組み合わせ")
		str = str.replace("comb_symbols_chronological","記号と年代順の組み合わせ")
		str = str.replace("comb_symbols_other","記号の組み合わせ")
		str = str.replace("formula","数式")
		str = str.replace("orthograpty","言語表記")
		str = str.replace("other","その他")
		
		answerType[i].innerHTML = str
	}
}