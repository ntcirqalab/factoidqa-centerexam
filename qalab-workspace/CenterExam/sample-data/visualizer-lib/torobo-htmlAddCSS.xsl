<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
				xmlns:lxslt="http://xml.apache.org/xslt"
				xmlns:result="http://www.example.com/results"
				extension-element-prefixes="result"
				version="1.0">
				
<xsl:output method="html" encoding="UTF-8" version="5.0"/>

<!--テンプレート読込み-->
<xsl:template match="/">
	<!-- <link rel="stylesheet" type="text/css" href="./exam.css" /> -->
	<div></div>
  	<div class="exam_body">
  		<xsl:apply-templates />  
  	</div>
  	<script type="text/javascript">
  		function changeDisplay(el){
  			var target_el = getNextSibling(el.firstChild);
  			if (target_el.style.display != 'block'){
  				target_el.style.display = 'block';
  			} else {
  				target_el.style.display = 'none';
  			}
  		}
  		function getNextSibling(el){
  			var next_el = el.nextSibling;
  			while(next_el.className != 'exam_question_evidence'){
  				next_el = next_el.nextSibling;
  			}
  			return next_el;
  		}
  	</script>
</xsl:template>

<xsl:template name="insertComponent">
    <xsl:param name="start" />
    <xsl:param name="end" />
    <xsl:param name="component" />
    <b>
    
    	<input type="radio" name="component" onclick="changeResult();">
    	 	<xsl:attribute name="value">
    	 		<xsl:value-of select="@*[$component]" />
    	 	</xsl:attribute>
    	</input>
    	<xsl:value-of select="@*[$component]" />
    	<font color="red">
    		<xsl:value-of select="concat('　', '正解率：')"  />
    		<xsl:value-of select="format-number(@*[$component+2]*100, '###.00')" />%
    	</font>
    	<font color="blue">
    		<xsl:value-of select="concat('　得点：', @*[$component+1], '点')" />
    	</font>
    	<xsl:value-of select="concat('　', @*[$component+3], '／', @*[$component+4], '（正答数／問題数）')" />
    
    </b>
    <br />

	<xsl:if test="$start != $end">
    	<xsl:call-template name="insertComponent">
    		<xsl:with-param name="component" select="$start*6+1" />
            <xsl:with-param name="start" select="$start+1" />
            <xsl:with-param name="end" select="$end" />
    	</xsl:call-template>
    </xsl:if>
    
</xsl:template>


<xsl:template name="insertJavaScript">
	<xsl:param name="start" />
	<xsl:param name="end" />
	<xsl:param name="component" />
	<xsl:param name="index" />
	if(radio[<xsl:value-of select="$index" />].checked){
		component = document.getElementsByName("<xsl:value-of select="@*[$component]" />")
		for(i=0;i<xsl:text disable-output-escaping="yes">&lt;</xsl:text>component.length;i++){
		component[i].style.display = "";
		}
	<xsl:call-template name="loopJS">
		<xsl:with-param name="tmp" select="$component" />
		<xsl:with-param name="component" select="2" />
		<xsl:with-param name="start" select="1" />
		<xsl:with-param name="end" select="$end" />
	</xsl:call-template>
	}
	<xsl:if test="$start != $end">
		<xsl:call-template name="insertJavaScript">
			<xsl:with-param name="component" select="$start*6+1" />
            <xsl:with-param name="start" select="$start+1" />
            <xsl:with-param name="end" select="$end" />
            <xsl:with-param name="index" select="$index+1" />  
    	</xsl:call-template>
    </xsl:if>
	
</xsl:template>

<xsl:template name="loopJS">
	<xsl:param name="start" />
	<xsl:param name="end" />
	<xsl:param name="component" />
	<xsl:param name="tmp" />
	<xsl:if test="$tmp != $component">
		component = document.getElementsByName("<xsl:value-of select="@*[$component]" />")
		for(i=0;i<xsl:text disable-output-escaping="yes">&lt;</xsl:text>component.length;i++){
			component[i].style.display = "none";
		}
	</xsl:if>
	<xsl:if test="$start != $end">
		<xsl:call-template name="loopJS">
			<xsl:with-param name="tmp" select="$tmp" />
			<xsl:with-param name="component" select="$start*6+1" />
			<xsl:with-param name="start" select="$start+1" />
			<xsl:with-param name="end" select="$end" />
		</xsl:call-template>
	</xsl:if>
	
</xsl:template>


<xsl:template match="exam">         
	<br />
	<xsl:apply-templates />
</xsl:template>

<xsl:template match="title">
</xsl:template>

<xsl:template match="br">
<br />
</xsl:template>

<xsl:template name="result">

	<xsl:param name="start" />
	<xsl:param name="end" />
	<xsl:param name="component" />
	
	<div class="exam_question_answer_choices">
		<xsl:attribute name="name">
			<xsl:value-of select="@*[$start+1]" />
		</xsl:attribute>
		<B>
		<xsl:value-of select="@*[$start+1]" />
		</B>
		<br />
		<table border="2px" bordercoler="red" Cellspacing="0">
			<xsl:for-each select="choice">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="@*[$component] = 'yes'">
								<font class="exam_question_answer_correct">
									<xsl:apply-templates />
								</font>
								<font color="blue"><b>
									<xsl:text> （正答）</xsl:text>
								</b></font>
							</xsl:when>
							<xsl:when test="@*[$component] = 'systemYes'">
								<font class="exam_question_answer_correct">
									<xsl:apply-templates />
								</font>
								<font color="blue"><b>
									<xsl:text> （○：システム正答）</xsl:text>
								</b></font>
							</xsl:when>
							<xsl:when test="@*[$component] = 'systemNo'">
								<font class="exam_question_answer_wrong">
									<xsl:apply-templates />
								</font>
								<font color="red"><b>
									<xsl:text> （×：システム誤答）</xsl:text>
								</b></font>
							</xsl:when>
							<xsl:otherwise>
								<font clase="exam_question_answer_highlight">
									<xsl:apply-templates />
								</font>
							</xsl:otherwise>
					</xsl:choose>
				</td>
			</tr>
		</xsl:for-each>
	</table>
	</div>
	
	<xsl:if test="$start != $end">
    	<xsl:call-template name="result">
    		<xsl:with-param name="component" select="$start*2+1" />
            <xsl:with-param name="start" select="$start+1" />
            <xsl:with-param name="end" select="$end" />
    	</xsl:call-template>
    </xsl:if>
    
</xsl:template>


<xsl:template match="choices">

	<xsl:call-template name="result">
    	<xsl:with-param name="component" select="1" />
        <xsl:with-param name="start" select="1" />
        <xsl:with-param name="end" select="@components" />
    </xsl:call-template>
	
</xsl:template>

<xsl:template match="ansColumn">
	<font class="exam_question_answer_column">
		<xsl:text>　</xsl:text>
		<xsl:value-of select="." />
		<xsl:text>　</xsl:text>
	</font>
</xsl:template>

<xsl:template match="uText">
	<u>
		<xsl:apply-templates />
	</u>
</xsl:template>

<xsl:template match = "blank">
	<font style="border-style:solid;border-color:black;border-width:1px;height:1">
		<xsl:text>　</xsl:text>
		<xsl:value-of select="." />
		<xsl:text>　</xsl:text>
	</font>
</xsl:template>

<xsl:template match ="label">
	<b>
		<xsl:if test = "parent::question/@minimal = 'no'">
			<xsl:attribute name="class">
				<xsl:text>exam_section_number</xsl:text>
			</xsl:attribute>
		</xsl:if>
		<xsl:if test = "parent::question/@minimal = 'yes'">
			<xsl:attribute name="class">
				<xsl:text>exam_question_number</xsl:text>
			</xsl:attribute>
		</xsl:if>
		<xsl:value-of select="." />
	</b>
</xsl:template>

<xsl:template match ="img">
	<div class="exam_sentence_image">
	<img>
		<xsl:attribute name="src">
			<xsl:value-of select="./@src" />
		</xsl:attribute>
	</img>
	</div>
</xsl:template>

<xsl:template match ="tbl">
	<div style="margin:1em 3% 1em 3%;">
	<table border="2px">
		<xsl:for-each select="row">
			<tr>
				<xsl:for-each select="cell">
					<td>
					<xsl:choose>
						<xsl:when test="name() = 'blank'">
								<font style="border-style:solid;border-color:black;border-width:2px;height:1">
								<xsl:text>　</xsl:text>
								<xsl:value-of select="." />
								<xsl:text>　</xsl:text>
								</font>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="." />
						</xsl:otherwise>
					</xsl:choose>
					</td>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</table>
	</div>
</xsl:template>
	
<xsl:template match="question">
	<xsl:if test="@minimal = 'no'">
		<hr />
	</xsl:if>
	<div>
	    <xsl:if test="@minimal = 'no'">
			<xsl:attribute name="class">
				<xsl:text>exam_section</xsl:text>
			</xsl:attribute>
		</xsl:if>
		<xsl:if test="@minimal = 'yes'">
			<xsl:attribute name="class">
				<xsl:text>exam_question</xsl:text>
			</xsl:attribute>
		</xsl:if>
	<xsl:apply-templates />
	<xsl:if test="@minimal = 'yes'">
	    <div name = "answer_type" class="exam_question_answer_knowledge_type">
			<font color="#ff0099">
		    	<xsl:value-of select="concat('Answer_Type：',@answer_type)" />
			<!-- </div> -->
			<!-- <div name = "knowledge_type" style="display:inline" class="exam_question_answer_knowledgeType"> -->
				<xsl:value-of select="concat('　　Knowledge_Type：',@knowledge_type)" />
			</font>
		</div>
	</xsl:if>
	<br />
	</div>
	<br />
</xsl:template>

<xsl:template match ="instruction">
	<div>
		<xsl:attribute name="class">
			<xsl:if test = "parent::question/@minimal = 'no'">
				<xsl:text>exam_instruction</xsl:text>
			</xsl:if>
			<xsl:if test = "parent::question/@minimal = 'yes'">
				<xsl:text>exam_question_sentence</xsl:text>
			</xsl:if>
		</xsl:attribute>
		<xsl:apply-templates />	
	</div>
</xsl:template>

<xsl:template match ="data">
	<div>
		<xsl:attribute name="class">
			<xsl:if test = "parent::question/@minimal = 'no'">
				<xsl:text>exam_sentence</xsl:text>
			</xsl:if>
			<xsl:if test = "parent::question/@minimal = 'yes'">
				<xsl:text>exam_question_sentence</xsl:text>
			</xsl:if>
		</xsl:attribute>
	<xsl:apply-templates />	
	</div>
</xsl:template>

<xsl:template match="lText">
	<ul>
		<li>
		<xsl:value-of select="." />
		</li>
	</ul>
</xsl:template>

<xsl:template match="ref">
	<font color="blue"><b>
	<xsl:element name="span">
		<xsl:attribute name="title">
			<xsl:value-of select="@target" />
		</xsl:attribute>
		<xsl:value-of select="." />
	</xsl:element>
	</b></font>
</xsl:template>

<xsl:template match="process_log">
	<div class="exam_question_random_answer" onclick="changeDisplay(this)">
	<xsl:attribute name="name">
		<xsl:value-of select="@id" />
	</xsl:attribute>
	<font color="blue" onmouseover="this.color='red'" onmouseout="this.color='blue'" class="ctext">
		<xsl:value-of select="concat('Process_Log(', @id, ')')" />
	</font>
	<div class="exam_question_evidence">
		<xsl:value-of select="." />
	</div>
	<br />
	</div>
</xsl:template>

</xsl:stylesheet>