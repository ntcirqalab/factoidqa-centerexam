package org.kachako.components.collection_reader;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import javax.xml.bind.JAXBException;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.collection.CollectionReader_ImplBase;
import org.apache.uima.examples.SourceDocumentInformation;

import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.ResourceSpecifier;
import org.apache.uima.util.Progress;

import org.kachako.types.qa.Question;

/**
 * @author Yoshinobu Kano
 *
 */
public class InteractiveQuestionCollectionReader extends CollectionReader_ImplBase {
	protected static final String TARGET_LANGUAGE_QUESTION_VIEW_NAME = "TargetLanguageQuestionView"; //TODO decide view name
	protected static final String PARAM_INTERACTIVE_QUESTION = "InteractiveQuestion";
	private String interactiveQuestion;

	@Override
	public boolean initialize(ResourceSpecifier aSpecifier,
			Map<String, Object> aAdditionalParams)
			throws ResourceInitializationException {
		
		super.initialize(aSpecifier, aAdditionalParams);

		interactiveQuestion = (String) getConfigParameterValue(PARAM_INTERACTIVE_QUESTION);
		
		return true;
	}


	private boolean notyetRetrieved = true;
	
	@Override
	public void getNext(CAS topicCAS) throws IOException, CollectionException {
		try {

			//get JCas
			JCas topicJCas = topicCAS.getJCas();

			//create Question and Answer views
			JCas questionViewJCas = topicJCas.createView(TARGET_LANGUAGE_QUESTION_VIEW_NAME); //TODO decide sofa name

			//add Question FSs and create document of Question views
			Question questionFS = new Question(questionViewJCas);
			questionFS.addToIndexes(questionViewJCas);
			

			//set span and add document text
//			int beginningOfQuestionIndex = questionSofaDocument.length();
	//		questionSofaDocument.append(questionElement.getvalue()).append(LINE_SEPARATOR);
		//	int endOfQuestionIndex = questionSofaDocument.length() - 1;
			//questionFS.setBegin(beginningOfQuestionIndex);
			//questionFS.setEnd(endOfQuestionIndex);
			questionFS.setBegin(0);
			questionFS.setEnd(interactiveQuestion.length());
			
			//set document of Question views
			questionViewJCas.setDocumentText(interactiveQuestion);

			//set URI of Question views
			SourceDocumentInformation targetLanguageQuestionSofaSourceDocumentInformationFS = new SourceDocumentInformation(questionViewJCas);
			targetLanguageQuestionSofaSourceDocumentInformationFS.addToIndexes(questionViewJCas);
			targetLanguageQuestionSofaSourceDocumentInformationFS.setUri("interactive");

			notyetRetrieved = false;
			
		} catch (CASException e) {
			e.printStackTrace();
			throw new CollectionException(e);
		}
	}


	@Override
	public boolean hasNext() throws IOException, CollectionException {
		return notyetRetrieved;
	}


	@Override
	public void close() throws IOException {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Progress[] getProgress() {
		// TODO Auto-generated method stub
		return null;
	}
}
