package aclia_writer;

import javax.xml.bind.JAXBException;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;

public class Ir4qaWriterPackedAnalysisEngine extends
		Ir4qaWriterAnalysisEngineAbstract {

	private static final String GENERATED_CLASS_PACKAGE_NAME = "generated.ir4qa";
	private final String PARAM_OUTPUT_XML_FILE_NAME = "OutputXmlFileName";
	private generated.ir4qa.TOPICSET topicSetElement = null;
	private String outputXmlFileName = null;

	@Override
	public void initialize(UimaContext aContext)
			throws ResourceInitializationException {
		super.initialize(aContext);

		topicSetElement = new generated.ir4qa.TOPICSET();
		outputXmlFileName = (String) aContext.getConfigParameterValue(PARAM_OUTPUT_XML_FILE_NAME);
	}

	@Override
	public void process(JCas topicJCas) throws AnalysisEngineProcessException {
		//create METADATA
		try {
			if(topicSetElement.getMETADATA() == null){
				topicSetElement.setMETADATA(createMetadata(topicJCas));
			}
		} catch (CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}

		//create TOPIC and add to list of TOPIC
		try {
			topicSetElement.getTOPIC().add(createTopic(topicJCas));
		} catch (CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}
	}

	@Override
	public void collectionProcessComplete()
			throws AnalysisEngineProcessException {
		super.collectionProcessComplete();

		try {
			//marshall
			marshall(topicSetElement, outputXmlFileName, GENERATED_CLASS_PACKAGE_NAME);
		} catch (JAXBException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}

	}
}
