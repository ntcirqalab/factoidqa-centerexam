package aclia_writer;

import java.util.Iterator;
import java.util.List;

import org.apache.uima.cas.CASException;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.TOP;

import org.kachako.types.qa.Metadata;
import org.kachako.types.qa.Question;

public abstract class QuestionWriterAnalysisEngineAbstract extends
		AcliaWriterAbstract {

	protected static final String TARGET_LANGUAGE_QUESTION_VIEW_NAME = "TargetLanguageQuestionView"; //TODO decide view name
	protected static final String ENGLISH_QUESTION_VIEW_NAME = "EnglishQuestionView"; //TODO decide view name
	protected String language = null;

	protected generated.gold_standard.TOPICSET createTopicSet(JCas topicJCas) throws CASException {
		generated.gold_standard.TOPICSET topicSetElement = new generated.gold_standard.TOPICSET();
		topicSetElement.setMETADATA(createMetadata(topicJCas));
		topicSetElement.getTOPIC().add(createTopic(topicJCas));
		return topicSetElement;
	}

	protected generated.gold_standard.METADATA createMetadata(JCas topicJCas) throws CASException {
		generated.gold_standard.METADATA metadataElement = new generated.gold_standard.METADATA();
		Iterator<TOP> metadataFSIterator = topicJCas.getView(ENGLISH_QUESTION_VIEW_NAME).getJFSIndexRepository().getAllIndexedFS(Metadata.type);
		if(metadataFSIterator.hasNext()){
			Metadata metadataFS = (Metadata) metadataFSIterator.next();
			metadataElement.setDESCRIPTION(metadataFS.getDescription());
			metadataElement.setCORPUS(metadataFS.getCorpus());
			metadataElement.setLANGUAGE(createLanguage(metadataFS));
			metadataElement.setVERSION(metadataFS.getVersion());
		}
		return metadataElement;
	}

	protected generated.gold_standard.LANGUAGE createLanguage(Metadata metadataFS) {
		generated.gold_standard.LANGUAGE languageElement = new generated.gold_standard.LANGUAGE();
		languageElement.setSOURCE(metadataFS.getSourceLanguage());
		languageElement.setTARGET(metadataFS.getTargetLanguage());
		language = metadataFS.getTargetLanguage();
		return languageElement;
	}

	protected generated.gold_standard.TOPIC createTopic(JCas topicJCas) throws CASException {
		generated.gold_standard.TOPIC topicElement = new generated.gold_standard.TOPIC();
		//set ID
		{
			Iterator<TOP> sourceDocumentInformationFSIterator = topicJCas.getView(ENGLISH_QUESTION_VIEW_NAME).getJFSIndexRepository().getAllIndexedFS(SourceDocumentInformation.type);
			if(sourceDocumentInformationFSIterator.hasNext()){
				topicElement.setID(((SourceDocumentInformation) sourceDocumentInformationFSIterator.next()).getUri());
			}
		}
		//set QUESTION
		{
			Iterator<TOP> targetLangugageQuestionFSIterator = topicJCas.getView(TARGET_LANGUAGE_QUESTION_VIEW_NAME).getJFSIndexRepository().getAllIndexedFS(Question.type);
			Iterator<TOP> englishQuestionFSIterator = topicJCas.getView(ENGLISH_QUESTION_VIEW_NAME).getJFSIndexRepository().getAllIndexedFS(Question.type);
			List<generated.gold_standard.QUESTION> questionElementList = topicElement.getQUESTION();
			if(targetLangugageQuestionFSIterator.hasNext()){
				questionElementList.add(createQuestion((Question) targetLangugageQuestionFSIterator.next(), language));				
			}
			if(englishQuestionFSIterator.hasNext()){
				questionElementList.add(createQuestion((Question) englishQuestionFSIterator.next(), "EN"));
			}
		}
		//set NARRATIVE
		{
			Iterator<TOP> targetLangugageQuestionFSIterator = topicJCas.getView(TARGET_LANGUAGE_QUESTION_VIEW_NAME).getJFSIndexRepository().getAllIndexedFS(Question.type);
			Iterator<TOP> englishQuestionFSIterator = topicJCas.getView(ENGLISH_QUESTION_VIEW_NAME).getJFSIndexRepository().getAllIndexedFS(Question.type);
			List<generated.gold_standard.NARRATIVE> narrativeElementList = topicElement.getNARRATIVE();
			if(targetLangugageQuestionFSIterator.hasNext()){
				narrativeElementList.add(createNarrative((Question) targetLangugageQuestionFSIterator.next(), language));
			}
			if(englishQuestionFSIterator.hasNext()){
				narrativeElementList.add(createNarrative((Question) englishQuestionFSIterator.next(), "EN"));
			}
		}
		return topicElement;
	}

	protected generated.gold_standard.NARRATIVE createNarrative(Question questionFS, String langugage) {
		generated.gold_standard.NARRATIVE narrativeElement = new generated.gold_standard.NARRATIVE();
		narrativeElement.setLANG(langugage);
		narrativeElement.setvalue(((Question)questionFS).getCoveredText());
		return narrativeElement;
	}

	protected generated.gold_standard.QUESTION createQuestion(Question questionFS, String language) {
		generated.gold_standard.QUESTION questionElement = new generated.gold_standard.QUESTION();
		questionElement.setLANG(language);
		questionElement.setvalue(((Question)questionFS).getCoveredText());
		return questionElement;
	}
}
