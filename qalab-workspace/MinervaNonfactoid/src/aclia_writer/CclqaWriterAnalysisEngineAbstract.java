package aclia_writer;

import java.util.Iterator;
import java.util.List;

import org.apache.uima.cas.CASException;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.TOP;

import org.kachako.types.qa.AnswerCandidate;
import org.kachako.types.qa.Metadata;

/**
 * @author Takayuki SUZUKI
 *
 */
public abstract class CclqaWriterAnalysisEngineAbstract extends AcliaWriterAbstract {

	protected static final String CCLQA_VIEW_NAME = "CclqaView";

	protected generated.cclqa.TOPICSET createTopicSet(JCas topicJCas) throws CASException {
		generated.cclqa.TOPICSET topicSetElement = new generated.cclqa.TOPICSET();
		topicSetElement.setMETADATA(createMetadata(topicJCas));
		topicSetElement.getTOPIC().add(createTopic(topicJCas));
		return topicSetElement;
	}

	protected generated.cclqa.METADATA createMetadata(JCas topicJCas) throws CASException {
		generated.cclqa.METADATA metadataElement = new generated.cclqa.METADATA();
		Iterator<TOP> metadataFSIterator = topicJCas.getView(CCLQA_VIEW_NAME).getJFSIndexRepository().getAllIndexedFS(Metadata.type);
		if(metadataFSIterator.hasNext()){
			Metadata metadataFS = (Metadata) metadataFSIterator.next();
			metadataElement.setDESCRIPTION(metadataFS.getDescription());
			metadataElement.setRUNID(metadataFS.getRunId());
		}
		return metadataElement;
	}

	protected generated.cclqa.TOPIC createTopic(JCas topicJCas) throws CASException {
		generated.cclqa.TOPIC topicElement = new generated.cclqa.TOPIC();
		//set ID
		{
			Iterator<TOP> sourceDocumentInformationFSIterator = topicJCas.getView(CCLQA_VIEW_NAME).getJFSIndexRepository().getAllIndexedFS(SourceDocumentInformation.type);
			if(sourceDocumentInformationFSIterator.hasNext()){
				topicElement.setID(((SourceDocumentInformation) sourceDocumentInformationFSIterator.next()).getUri());
			}
		}
		//set CCLQA_RESULT
		topicElement.setCCLQARESULT(createCclqaResult(topicJCas));
		return topicElement;
	}

	protected generated.cclqa.CCLQARESULT createCclqaResult(JCas topicJCas) throws CASException {
		generated.cclqa.CCLQARESULT cclqaResultElement = new generated.cclqa.CCLQARESULT();
		Iterator<TOP> answerCandidateFSIterator = topicJCas.getView(CCLQA_VIEW_NAME).getJFSIndexRepository().getAllIndexedFS(AnswerCandidate.type);
		List<generated.cclqa.ANSWERCANDIDATE> answerCandidateElementList = cclqaResultElement.getANSWERCANDIDATE();
		while(answerCandidateFSIterator.hasNext()){
			answerCandidateElementList.add(createAnswerCandidate((AnswerCandidate) answerCandidateFSIterator.next()));
		}
		return cclqaResultElement;
	}

	protected generated.cclqa.ANSWERCANDIDATE createAnswerCandidate(AnswerCandidate answerCandidateFS) {
		generated.cclqa.ANSWERCANDIDATE answerCandidateElement = new generated.cclqa.ANSWERCANDIDATE();
		answerCandidateElement.setRANK(String.valueOf(answerCandidateFS.getRank()));
		answerCandidateElement.setSCORE(String.valueOf(answerCandidateFS.getScore()));
		answerCandidateElement.setvalue(answerCandidateFS.getCoveredText());
		return answerCandidateElement;
	}
}
