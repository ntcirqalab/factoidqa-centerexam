package aclia_writer;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;

/**
 * @author Takayuki SUZUKI
 *
 */
public abstract class AcliaWriterAbstract extends JCasAnnotator_ImplBase {
	protected void marshall(Object topicSetElement, String xmlFileName, String packageName) throws JAXBException {
		//open XML file
		File outputFile = new File(xmlFileName);

		//create JAXBContext
		JAXBContext context = JAXBContext.newInstance(packageName);

		//get Marshaller
		Marshaller marshaller = context.createMarshaller();

		//marshalling
		marshaller.marshal(topicSetElement, outputFile);
	}
}
