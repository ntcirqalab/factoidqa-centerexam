package aclia_writer;

import javax.xml.bind.JAXBException;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;

public class QuestionAnalysisWriterSeparatedAnalysisEngine extends
		QuestionAnalysisWriterAnalysisEngineAbstract {

	private int topicCounter = 0;
	private static final String GENERATED_CLASS_PACKAGE_NAME = "generated.question_analysis";
	private static final String PARAM_OUTPUT_XML_FILE_NAME_PREFIX = "OutputXmlFileNamePrefix";
	private String outputXmlFileNamePrefix = null;

	@Override
	public void initialize(UimaContext aContext)
			throws ResourceInitializationException {
		outputXmlFileNamePrefix = (String) aContext.getConfigParameterValue(PARAM_OUTPUT_XML_FILE_NAME_PREFIX);

		super.initialize(aContext);
	}

	@Override
	public void process(JCas topicJCas) throws AnalysisEngineProcessException {
		//set output XML file name
		String outputXmlFileName = outputXmlFileNamePrefix.concat(String.valueOf(topicCounter)).concat(".xml"); topicCounter++;

		try {
			//create TOPIC_SET
			generated.question_analysis.TOPICSET topicSetElement = createTopicSet(topicJCas);

			//marshall
			marshall(topicSetElement, outputXmlFileName, GENERATED_CLASS_PACKAGE_NAME);

		} catch (CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		} catch (JAXBException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}
	}
}
