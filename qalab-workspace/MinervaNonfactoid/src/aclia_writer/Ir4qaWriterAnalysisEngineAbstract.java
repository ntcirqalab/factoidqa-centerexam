package aclia_writer;

import generated.ir4qa.DOCUMENT;
import generated.ir4qa.IR4QARESULT;

import java.util.Iterator;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.TOP;

import org.kachako.types.qa.Document;
import org.kachako.types.qa.Metadata;

public abstract class Ir4qaWriterAnalysisEngineAbstract extends
		AcliaWriterAbstract {

	protected static final String IR4QA_VIEW_NAME = "Ir4qaView";

	protected generated.ir4qa.TOPICSET createTopicSet(JCas topicJCas) throws CASException {
		generated.ir4qa.TOPICSET topicSetElement = new generated.ir4qa.TOPICSET();
		topicSetElement.setMETADATA(createMetadata(topicJCas));
		topicSetElement.getTOPIC().add(createTopic(topicJCas));
		return topicSetElement;
	}

	protected generated.ir4qa.METADATA createMetadata(JCas topicJCas) throws CASException {
		generated.ir4qa.METADATA metadataElement = new generated.ir4qa.METADATA();
		Iterator<TOP> metadataFSIterator = topicJCas.getView(IR4QA_VIEW_NAME).getJFSIndexRepository().getAllIndexedFS(Metadata.type);
		if(metadataFSIterator.hasNext()){
			Metadata metadataFS = (Metadata) metadataFSIterator.next();
			metadataElement.setDESCRIPTION(metadataFS.getDescription());
			metadataElement.setRUNID(metadataFS.getRunId());
		}
		return metadataElement;
	}

	protected generated.ir4qa.TOPIC createTopic(JCas topicJCas) throws CASException {
		generated.ir4qa.TOPIC topicElement = new generated.ir4qa.TOPIC();
		//set ID
		{
			Iterator<TOP> sourceDocumentInformationFSIterator = topicJCas.getView(IR4QA_VIEW_NAME).getJFSIndexRepository().getAllIndexedFS(SourceDocumentInformation.type);
			if(sourceDocumentInformationFSIterator.hasNext()){
				topicElement.setID(((SourceDocumentInformation) sourceDocumentInformationFSIterator.next()).getUri());
			}
		}
		//set CCLQA_RESULT
		topicElement.setIR4QARESULT(createIr4qaResult(topicJCas));
		return topicElement;
	}

	protected generated.ir4qa.IR4QARESULT createIr4qaResult(JCas topicJCas) {
		generated.ir4qa.IR4QARESULT ir4qaResultElement = new generated.ir4qa.IR4QARESULT();

		for (Iterator<TOP> documentFSIterator = topicJCas.getJFSIndexRepository().getAllIndexedFS(Document.type); documentFSIterator.hasNext();) {
			Document documentFS = (Document) documentFSIterator.next();
			generated.ir4qa.DOCUMENT documentElement = new generated.ir4qa.DOCUMENT();
			documentElement.setDOCID(documentFS.getDocId());
			documentElement.setRANK(Integer.toString(documentFS.getRank()));
			documentElement.setSCORE(Double.toString(documentFS.getScore()));
			ir4qaResultElement.getDOCUMENT().add(documentElement);
		}

		return ir4qaResultElement;
	}

}
