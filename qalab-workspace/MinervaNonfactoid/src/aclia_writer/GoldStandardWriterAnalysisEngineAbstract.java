package aclia_writer;

import java.util.Iterator;
import java.util.List;

import org.apache.uima.cas.CASException;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.TOP;
import org.kachako.types.qa.Answer;
import org.kachako.types.qa.AnswerType;
import org.kachako.types.qa.Metadata;
import org.kachako.types.qa.Nugget;
import org.kachako.types.qa.Question;

/**
 * @author Takayuki SUZUKI
 *
 */
public abstract class GoldStandardWriterAnalysisEngineAbstract extends AcliaWriterAbstract {

	protected static final String TARGET_LANGUAGE_QUESTION_VIEW_NAME = "TargetLanguageQuestionView";
	protected static final String ENGLISH_QUESTION_VIEW_NAME = "EnglishQuestionView";
	protected static final String ANSWER_VIEW_NAME = "AnswerView";
	protected String language = null;

	protected generated.gold_standard.TOPICSET createTopicSet(JCas topicJCas) throws CASException {
		generated.gold_standard.TOPICSET topicSetElement = new generated.gold_standard.TOPICSET();
		topicSetElement.setMETADATA(createMetadata(topicJCas));
		topicSetElement.getTOPIC().add(createTopic(topicJCas));
		return topicSetElement;
	}

	protected generated.gold_standard.METADATA createMetadata(JCas topicJCas) throws CASException {
		generated.gold_standard.METADATA metadataElement = new generated.gold_standard.METADATA();
		Iterator<TOP> metadataFSIterator = topicJCas.getView(TARGET_LANGUAGE_QUESTION_VIEW_NAME).getJFSIndexRepository().getAllIndexedFS(Metadata.type);
		if(metadataFSIterator.hasNext()){
			Metadata metadataFS = (Metadata) metadataFSIterator.next();
			metadataElement.setCORPUS(metadataFS.getCorpus());
			metadataElement.setDESCRIPTION(metadataFS.getDescription());
			metadataElement.setLANGUAGE(createLanguage(metadataFS));
			metadataElement.setVERSION(metadataFS.getVersion());
		}
		return metadataElement;
	}

	protected generated.gold_standard.LANGUAGE createLanguage(Metadata metadataFS) {
		generated.gold_standard.LANGUAGE languageElement = new generated.gold_standard.LANGUAGE();
		languageElement.setSOURCE(metadataFS.getSourceLanguage());
		languageElement.setTARGET(metadataFS.getTargetLanguage());
		language = metadataFS.getTargetLanguage();
		return languageElement;
	}

	protected generated.gold_standard.TOPIC createTopic(JCas topicJCas) throws CASException {
		generated.gold_standard.TOPIC topicElement = new generated.gold_standard.TOPIC();
		//set ID
		{
			Iterator<TOP> sourceDocumentInformationFSIterator = topicJCas.getView(TARGET_LANGUAGE_QUESTION_VIEW_NAME).getJFSIndexRepository().getAllIndexedFS(SourceDocumentInformation.type);
			if(sourceDocumentInformationFSIterator.hasNext()){
				topicElement.setID(((SourceDocumentInformation) sourceDocumentInformationFSIterator.next()).getUri());
			}
		}
		//set TITLE
		{
			Iterator<TOP> questionFSIterator = topicJCas.getView(TARGET_LANGUAGE_QUESTION_VIEW_NAME).getJFSIndexRepository().getAllIndexedFS(Question.type);
			if(questionFSIterator.hasNext()){
				topicElement.setTITLE(((Question) questionFSIterator.next()).getTitle());
			}
		}
		//set QUESTION
		{
			Iterator<TOP> targetLangugageQuestionFSIterator = topicJCas.getView(TARGET_LANGUAGE_QUESTION_VIEW_NAME).getJFSIndexRepository().getAllIndexedFS(Question.type);
			Iterator<TOP> englishQuestionFSIterator = topicJCas.getView(ENGLISH_QUESTION_VIEW_NAME).getJFSIndexRepository().getAllIndexedFS(Question.type);
			List<generated.gold_standard.QUESTION> questionElementList = topicElement.getQUESTION();
			if(targetLangugageQuestionFSIterator.hasNext()){
				questionElementList.add(createQuestion((Question) targetLangugageQuestionFSIterator.next(), language));				
			}
			if(englishQuestionFSIterator.hasNext()){
				questionElementList.add(createQuestion((Question) englishQuestionFSIterator.next(), "EN"));
			}
		}
		//set ANSWERTYPE
		{
			Iterator<TOP> answerTypeFSIterator = topicJCas.getView(ANSWER_VIEW_NAME).getJFSIndexRepository().getAllIndexedFS(AnswerType.type);
			List<generated.gold_standard.ANSWERTYPE> answerTypeElementList = topicElement.getANSWERTYPE();
			while(answerTypeFSIterator.hasNext()){
				answerTypeElementList.add(createAnswerType((AnswerType) answerTypeFSIterator.next()));
			}
		}
		//set NARRATIVE
		{
			Iterator<TOP> targetLangugageQuestionFSIterator = topicJCas.getView(TARGET_LANGUAGE_QUESTION_VIEW_NAME).getJFSIndexRepository().getAllIndexedFS(Question.type);
			Iterator<TOP> englishQuestionFSIterator = topicJCas.getView(ENGLISH_QUESTION_VIEW_NAME).getJFSIndexRepository().getAllIndexedFS(Question.type);
			List<generated.gold_standard.NARRATIVE> narrativeElementList = topicElement.getNARRATIVE();
			if(targetLangugageQuestionFSIterator.hasNext()){
				narrativeElementList.add(createNarrative((Question) targetLangugageQuestionFSIterator.next(), language));
			}
			if(englishQuestionFSIterator.hasNext()){
				narrativeElementList.add(createNarrative((Question) englishQuestionFSIterator.next(), "EN"));
			}
		}
		//set ANSWER
		{
			topicElement.setANSWER(createAnswer(topicJCas.getView(ANSWER_VIEW_NAME)));
		}
		return topicElement;
	}

	protected generated.gold_standard.ANSWER createAnswer(JCas answerViewJCas) {
		generated.gold_standard.ANSWER answerElement = new generated.gold_standard.ANSWER();
		//set TEXT
		{
			Iterator<TOP> answerFSIterator = answerViewJCas.getJFSIndexRepository().getAllIndexedFS(Answer.type);
			List<generated.gold_standard.TEXT> textElementList = answerElement.getTEXT();
			while(answerFSIterator.hasNext()){
				textElementList.add(createText((Answer) answerFSIterator.next()));
			}
		}
		//set NUGGET
		{
			Iterator<TOP> nuggetFSIterator = answerViewJCas.getJFSIndexRepository().getAllIndexedFS(Nugget.type);
			List<generated.gold_standard.NUGGET> nuggetElementList = answerElement.getNUGGET();
			while(nuggetFSIterator.hasNext()){
				nuggetElementList.add(createNugget((Nugget) nuggetFSIterator.next()));
			}
		}
		return answerElement;
	}

	protected generated.gold_standard.NUGGET createNugget(Nugget nuggetFS) {
		generated.gold_standard.NUGGET nuggetElement = new generated.gold_standard.NUGGET();
		nuggetElement.setID(((Nugget)nuggetFS).getId());
		nuggetElement.setNONVITAL(String.valueOf(((Nugget)nuggetFS).getNonVital()));
		nuggetElement.setVITAL(String.valueOf(((Nugget)nuggetFS).getVital()));
		nuggetElement.setSCORE(String.format("%.4f", ((Nugget)nuggetFS).getScore()));
		nuggetElement.setvalue(((Nugget)nuggetFS).getNuggetText());
		return nuggetElement;
	}

	protected generated.gold_standard.TEXT createText(Answer answerFS) {
		generated.gold_standard.TEXT textElement = new generated.gold_standard.TEXT();
		textElement.setDOCNO(((Answer)answerFS).getDocNo());
		textElement.setID(((Answer)answerFS).getId());
		textElement.setvalue(((Answer)answerFS).getCoveredText());
		return textElement;
	}

	protected generated.gold_standard.ANSWERTYPE createAnswerType(AnswerType answerTypeFS) {
		generated.gold_standard.ANSWERTYPE answerTypeElement = new generated.gold_standard.ANSWERTYPE();
		answerTypeElement.setvalue(((AnswerType)answerTypeFS).getAnswerType());
		return answerTypeElement;
	}

	protected generated.gold_standard.NARRATIVE createNarrative(Question questionFS, String langugage) {
		generated.gold_standard.NARRATIVE narrativeElement = new generated.gold_standard.NARRATIVE();
		narrativeElement.setLANG(langugage);
		narrativeElement.setvalue(((Question)questionFS).getCoveredText());
		return narrativeElement;
	}

	protected generated.gold_standard.QUESTION createQuestion(Question questionFS, String language) {
		generated.gold_standard.QUESTION questionElement = new generated.gold_standard.QUESTION();
		questionElement.setLANG(language);
		questionElement.setvalue(((Question)questionFS).getCoveredText());
		return questionElement;
	}
}
