package aclia_writer;

import generated.question_analysis.ANSWERTYPE;
import generated.question_analysis.KEYTERM;
import generated.question_analysis.KEYTERMS;
import generated.question_analysis.QUESTIONANALYSIS;

import java.util.Iterator;

import org.apache.uima.cas.CASException;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.TOP;

import org.kachako.types.qa.AnswerType;
import org.kachako.types.qa.KeyTerm;
import org.kachako.types.qa.Metadata;

public abstract class QuestionAnalysisWriterAnalysisEngineAbstract extends
		AcliaWriterAbstract {

	protected static final String QUESTION_ANALYSIS_VIEW_NAME = "QuestionAnalysisView";
	protected String language = null;

	protected generated.question_analysis.TOPICSET createTopicSet(JCas topicJCas) throws CASException {
		generated.question_analysis.TOPICSET topicSetElement = new generated.question_analysis.TOPICSET();
		topicSetElement.setMETADATA(createMetadata(topicJCas));
		topicSetElement.getTOPIC().add(createTopic(topicJCas));
		return topicSetElement;
	}

	protected generated.question_analysis.METADATA createMetadata(JCas topicJCas) throws CASException {
		generated.question_analysis.METADATA metadataElement = new generated.question_analysis.METADATA();
		Iterator<TOP> metadataFSIterator = topicJCas.getView(QUESTION_ANALYSIS_VIEW_NAME).getJFSIndexRepository().getAllIndexedFS(Metadata.type);
		if(metadataFSIterator.hasNext()){
			Metadata metadataFS = (Metadata) metadataFSIterator.next();
			metadataElement.setDESCRIPTION(metadataFS.getDescription());
			metadataElement.setRUNID(metadataFS.getRunId());
		}
		return metadataElement;
	}

	protected generated.question_analysis.TOPIC createTopic(JCas topicJCas) throws CASException {
		generated.question_analysis.TOPIC topicElement = new generated.question_analysis.TOPIC();
		//set ID
		{
			Iterator<TOP> sourceDocumentInformationFSIterator = topicJCas.getView(QUESTION_ANALYSIS_VIEW_NAME).getJFSIndexRepository().getAllIndexedFS(SourceDocumentInformation.type);
			if(sourceDocumentInformationFSIterator.hasNext()){
				topicElement.setID(((SourceDocumentInformation) sourceDocumentInformationFSIterator.next()).getUri());
			}
		}
		//set QUESTION_ANALYSIS
		topicElement.setQUESTIONANALYSIS(createQuestionAnalysis(topicJCas));
		return topicElement;
	}

	protected generated.question_analysis.QUESTIONANALYSIS createQuestionAnalysis(JCas topicJCas) throws CASException {
		generated.question_analysis.QUESTIONANALYSIS questionAnalysisElement = new generated.question_analysis.QUESTIONANALYSIS();
		//set ANSWERTYPE
		questionAnalysisElement.setANSWERTYPE(createAnswerType(topicJCas));
		//set KEYTERMS
		questionAnalysisElement.setKEYTERMS(createKeyTerms(topicJCas));
		return questionAnalysisElement;
	}

	protected generated.question_analysis.ANSWERTYPE createAnswerType(JCas topicJCas) throws CASException {
		generated.question_analysis.ANSWERTYPE answerTypeElement = new generated.question_analysis.ANSWERTYPE();
		Iterator<TOP> answerTypeFSIterator = topicJCas.getView(QUESTION_ANALYSIS_VIEW_NAME).getJFSIndexRepository().getAllIndexedFS(AnswerType.type);
		if(answerTypeFSIterator.hasNext()){
			AnswerType answerTypeFS = (AnswerType) answerTypeFSIterator.next();
			answerTypeElement.setSCORE(Double.toString(answerTypeFS.getScore()));
			answerTypeElement.setvalue(answerTypeFS.getAnswerType());
		}
		return answerTypeElement;
	}

	private generated.question_analysis.KEYTERMS createKeyTerms(JCas topicJCas) {
		generated.question_analysis.KEYTERMS keyTermsElement = new generated.question_analysis.KEYTERMS();

		for (Iterator<TOP> keytermFSIterator = topicJCas.getJFSIndexRepository().getAllIndexedFS(KeyTerm.type); keytermFSIterator.hasNext();) {
			KeyTerm keyTermFS = (KeyTerm) keytermFSIterator.next();
			generated.question_analysis.KEYTERM keyTermElement = new generated.question_analysis.KEYTERM();
			keyTermElement.setvalue(keyTermFS.getCoveredText());
			keyTermElement.setSCORE(Double.toString(keyTermFS.getScore()));
			keyTermsElement.setLANGUAGE(keyTermFS.getLanguage());
			keyTermsElement.getKEYTERM().add(keyTermElement);
		}

		return keyTermsElement;
	}


}
