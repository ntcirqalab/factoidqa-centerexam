

import java.util.Iterator;

import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;

public class TestAnnotator extends JCasAnnotator_ImplBase {

	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		try {
			aJCas.createView("AView");
			aJCas.createView("BView");
			for (Iterator<JCas> jCasIterator = aJCas.getViewIterator(); jCasIterator.hasNext();) {
				JCas jCas = jCasIterator.next();
				System.out.println(jCas.getViewName());
			}
			System.out.println("completed");
			JCas aView = aJCas.getView("AView");
			aView.setDocumentText("AView1");
			aView.reset();
			aView.setDocumentText("AView2");
			System.out.println(aView.getDocumentText());
		} catch (CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}
	}

}
