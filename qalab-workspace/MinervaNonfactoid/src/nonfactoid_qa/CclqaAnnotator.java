package nonfactoid_qa;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.regex.Matcher;

import javax.swing.text.html.HTMLDocument.HTMLReader.HiddenAction;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.TOP;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.kachako.share.regex.RegexCache;
import org.kachako.types.qa.AnswerFeature;
import org.kachako.types.qa.Result;
import org.u_compare.shared.japanese.syntactic.morpheme.Morpheme;

/**
 * パッセージを生成します。
 * 事前条件：形態素解析、タグ除去
 * @author Takayuki SUZUKI
 *
 */
public class CclqaAnnotator extends JCasAnnotator_ImplBase {

	RegexCache regexCache = new RegexCache();

	private final class ScoreInformation {
		private double all, style, esk, topic;

		ScoreInformation(double all, double style, double esk, double topic) {
			this.all = all;
			this.style = style;
			this.esk = esk;
			this.topic = topic;
		}
	}

	private final class Range {
		private int start;
		private int end;

		Range(int start, int end) {
			this.start = start;
			this.end = end;
		}
	}
	
	private final class PassageInformation {
		private String url;
		private Range range;
		private double score;
		private String exp;

		PassageInformation(String url, Range range, double score, String exp) {
			this.url = url;
			this.range = range;
			this.score = score;
			this.exp = exp;
		}
	}

	@Override
	public void process(JCas baseJCas) throws AnalysisEngineProcessException {
		TreeMap<String, List<String>> urlToText = new TreeMap<>(); // ウィンドウを切り出す処理でキーの昇順にイテレートする必要があるためTreeMapとした
		Map<String, ListedMorpheme> urlToListedMorphemeMap = new HashMap<>();
		Map<String, Double> ansFeature = new HashMap<>();
		try {
			for (Iterator<JCas> ir4qaViewSnippetViewIterator = baseJCas.getViewIterator(Constant.IR4QA_SNIPPET_VIEW_NAME_PREFIX);
					ir4qaViewSnippetViewIterator.hasNext();){
				JCas ir4qaSnippetView = ir4qaViewSnippetViewIterator.next();
				String cacheUrl = ((Result) ir4qaSnippetView.getJFSIndexRepository().getAllIndexedFS(Result.type).next()).getUrl();
				// プレーンテキストを取得する
				List<String> textLine = Arrays.asList(ir4qaSnippetView.getDocumentText().split(Constant.LINE_SEPARATOR));
				// キャッシュのURLをページのURLに戻す
				// キャッシュを使っていない場合はそのまま
				String url = cacheUrl;
				if (!urlToText.containsKey(url)) {
					urlToText.put(url, new ArrayList<String>());
				}
				urlToText.get(url).add("");	// dummyを入れる
				for (String l : textLine) {
					if(regexCache.find("\\S", l) && !regexCache.find("ページ検索結果にもどる", l)) {
						urlToText.get(url).add(l);						
					}
				}
				urlToText.get(url).add(""); // dummyを入れる
				// 形態素解析の結果を取得する
				urlToListedMorphemeMap.put(url, new ListedMorpheme(ir4qaSnippetView));
			}
			
			// 特徴表現とそのχ2乗値の取得
			JCas answerFeatureView = baseJCas.getView(Constant.ANSWER_FEATURE_VIEW_NAME);
			for (Iterator<TOP> answerFeatureFSIterator
					= answerFeatureView.getJFSIndexRepository().getAllIndexedFS(AnswerFeature.type);
					answerFeatureFSIterator.hasNext();) {
				AnswerFeature answerFeatureFS = (AnswerFeature) answerFeatureFSIterator.next();
				ansFeature.put(answerFeatureFS.getWord(), answerFeatureFS.getChiSquare());
			}
		} catch (CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}

		// ここからパッセージ抽出
		class WindowInformation {
			String url;
			int start;
			int end;

			WindowInformation(String url, int start, int end) {
				this.url = url;
				this.start = start;
				this.end = end;
			}
		}
		
		// キーワードを取得
		LinkedHashSet<String> kwN = new LinkedHashSet<>();
		LinkedHashSet<String> kyouki = new LinkedHashSet<>();
		LinkedHashSet<String> fukugo = new LinkedHashSet<>();
		try {
			for (Iterator<JCas> viewIterator = baseJCas.getViewIterator(); viewIterator.hasNext();) {
				JCas view = viewIterator.next();
				kwN.addAll(Utilities.getKeywordSet(view, Constant.KEY_TERM_ATTRIBUTE_KW_NOUN));
				kyouki.addAll(Utilities.getKeywordSet(view, Constant.KEY_TERM_ATTRIBUTE_KYOUKI));
				fukugo.addAll(Utilities.getKeywordSet(view, Constant.KEY_TERM_ATTRIBUTE_FUKUGO));
			}
		} catch (CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}
		List<WindowInformation> window = new ArrayList<>();

		// プレーンテキストを読み込んでウィンドウを切り出す
		// キーワードが1つ以上含まれる文とその前後数文
		// 時間がかかるのでこれ以外の文は捨てる
		for (Map.Entry<String, List<String>> entry : urlToText.entrySet()) {
			String url = entry.getKey();
			List<String> text = entry.getValue();
			int start = 0;
			boolean found = false;
			int count = 0;

			for (int i = 0; i < text.size(); i++) {
				boolean lf = false;

				for (String kw : kwN) {
					if(regexCache.find(kw, text.get(i))) {
						count = 0;
						lf = true;
						if (!found) {
							found = true;
							start = Math.max(i - Constant.WINDOWSIZE, 0);
						}
					}
				}
				if (!lf && found) {
					count++;

					if (count == Constant.WINDOWSIZE || i == text.size() - 1) {
						int end = i;
						window.add(new WindowInformation(url, start, end));
						start = 0;
						found = false;
						count = 0;
					}
				}
			}
		}
		
		// 形態素解析の結果
		// my mecabResult = ();

		// 文のスコアを計算
		LinkedHashMap<String, List<ScoreInformation>> score = new LinkedHashMap<>();

		for (Map.Entry<String, List<String>> entry : urlToText.entrySet()) {
			score.put(entry.getKey(), new ArrayList<ScoreInformation>());
			for (int i = 0; i <= entry.getValue().size(); i++) {
				// 初期値．計算した結果0になるのとは区別
				score.get(entry.getKey()).add(new ScoreInformation(-1., 0., 0., 0.));
			}
		}

		// ESK計算用
		// 元のプログラムでは2次元配列だが、実質1次元配列なので1次元とする。
		List<String> sentQ = new ArrayList<>(kwN);

		// 切り出したウィンドウごとに
		for (WindowInformation w : window) {
			String url = w.url;
			int start = w.start;
			int end = w.end;

			for (int i = start; i <= end; i++) {
				List<Morpheme> result = urlToListedMorphemeMap.get(url).get(i);
				// スコアリング
				Scoring scoring;
				try {
					scoring = new Scoring(urlToText.get(url).get(i), kyouki, result, sentQ, ansFeature, kwN, fukugo);
				} catch (IOException e) {
					e.printStackTrace();
					throw new AnalysisEngineProcessException(e);
				}

				score.get(url).get(i).style = scoring.s;
				score.get(url).get(i).esk = scoring.e;
				score.get(url).get(i).topic = scoring.t;

				// スコアの統合
				switch (Constant.MARGE) {
				case PLUS:
					score.get(url).get(i).all
						= Constant.A * scoring.t + Constant.B * scoring.e + Constant.C * scoring.s;
					break;
				case MULTIPLY:
					score.get(url).get(i).all
						= Math.pow(scoring.t, Constant.A) * Math.pow(scoring.e, Constant.B) * Math.pow(scoring.s, Constant.C);
					break;
				}

				// 長さで正規化
				if (!result.isEmpty()) {
					score.get(url).get(i).all /= Math.log(result.size()) + 1;
				}
			}
		}

		// パッセージの作成
		List<PassageInformation> passage = new ArrayList<>();
		for (Map.Entry<String, List<ScoreInformation>> entry : score.entrySet()) {
			passage.addAll(makePassage(entry.getKey(), entry.getValue(), urlToText.get(entry.getKey())));
		}
		
		// ここで既にpassageには作成されたパッセージが格納されているはず。
		
		//Utilities.createViews(baseJCas, Constant., passage, setUpViewFunction, 0);
		return;
	}

	private List<PassageInformation> makePassage(String url, List<ScoreInformation> scoreRef,
			List<String> textRef) {
		ArrayList<Double> score = new ArrayList<>();
		List<PassageInformation> passages = new ArrayList<>();

		// 一時変数にコピー
		for (ScoreInformation scoreInfo : scoreRef) {
			score.add(scoreInfo.all);
		}

		class HillInformation {
			int sent;
			double value;

			HillInformation(int sent, double value) {
				this.sent = sent;
				this.value = value;
			}
		}
		TreeSet<HillInformation> hill = new TreeSet<>(new Comparator<HillInformation>() {
			@Override
			public int compare(HillInformation o1, HillInformation o2) {
				return Double.compare(o2.value, o1.value);
			};
		});
		// 極大値の検出
		// 片側に＝が入ってもよし 2008/01/31
		for(int i = 1; i < score.size(); i++) {
			if (score.get(i) > 0 && 
					(score.get(i) >= score.get(i - 1) && score.get(i) > score.get(i + 1) || 
					score.get(i) > score.get(i - 1) && score.get(i) >= score.get(i + 1))) {
				hill.add(new HillInformation(i, score.get(i)));
			}
		}

		// パッセージ作成
		// 極大値を大きい方から見ていって
		for (HillInformation hillInformation : hill) {
			int sent = hillInformation.sent; // 極大点の座標
			double hillValue = hillInformation.value; // 極大値

			// 前を見る(極大値から始める)
			int j = sent;
			// パッセージの開始位置
			int s0 = 0;
			while(true) {
				// 極大点との距離
				// int x = sent - j;
				// 距離による減衰を考慮して，閾値を下回るまで
				if (score.get(j) > hillValue * Constant.TH_PAS) {
					s0 = j;
				} else {
					break;
				}
				j--;
			}

			// 後ろを見る(極大値の直後から)
			j = sent + 1;
			// パッセージの終了位置
			int s1 = sent;
			while(true) {
				// 極大点との距離
				// int x = j - sent;
				// 距離による減衰を考慮して，閾値を下回るまで
				if (score.get(j) > hillValue * Constant.TH_PAS) {
					s1 = j;
				} else {
					break;
				}
				j++;
			}

			// int length = s1 - s0 + 1;

			// スコアは極大値のみか
			double scoreValue = hillValue;

			StringBuilder text = new StringBuilder();
			for(int k = s0; k <= s1; k++) {
				text.append(textRef.get(k));
			}
			passages.add(new PassageInformation(url, new Range(s0, s1), scoreValue, text.toString()));
		}
		return passages;
	}

	/**
	 * 文書の形態素解析の結果(Morpheme FS)を行ごとに分けて扱うためのクラスです。
	 * @author Takayuki SUZUKI
	 *
	 */
	private final class ListedMorpheme {
		private List<List<Morpheme>> listedMorpheme;
		
		ListedMorpheme(JCas ir4qaSnippetView) {
			listedMorpheme = new ArrayList<>();
			for (Iterator<Annotation> morphemeFSIterator
					= ir4qaSnippetView.getAnnotationIndex(Morpheme.type).iterator();
					morphemeFSIterator.hasNext();) {
				Morpheme morphemeFS = (Morpheme) morphemeFSIterator.next();
				if (isNewLine(morphemeFS)) {
					listedMorpheme.add(new ArrayList<Morpheme>());
				}
				listedMorpheme.get(listedMorpheme.size() - 1).add(morphemeFS);
			}
		}

		private int previousEndIndex = 0;

		private boolean isNewLine(Morpheme morphemeFS) {
			boolean returnValue;
			if (previousEndIndex + 1 < morphemeFS.getBegin() || morphemeFS.getBegin() == 0) {
				returnValue = true;
			} else {
				returnValue = false;
			}
			previousEndIndex = morphemeFS.getEnd();
			return returnValue;
		}
		
		/**
		 * @param i
		 * @return i行目のMorpheme FSのList
		 */
		List<Morpheme> get(int i) {
			return listedMorpheme.get(i);
		}
		
	}

	private final class Scoring {
		private double s = 0., e = 0., t = 0.;

		Scoring(String line, LinkedHashSet<String> kyouki, List<Morpheme> mecabResult, List<String> sentQ, Map<String, Double> ansFeatrue, LinkedHashSet<String> kwN, LinkedHashSet<String> fukugo) throws IOException {
			if (Constant.C != 0.) {
				s = writingStyle(line, ansFeatrue, mecabResult);
			}
			if (Constant.B != 0.) {
				e = esk(mecabResult, sentQ, kwN);
			}
			if (Constant.A != 0.) {
				t = topic(line, kyouki, mecabResult, fukugo, kwN);
			}
		}

		private double writingStyle(String line,
				Map<String, Double> ansFeatrue, List<Morpheme> mecabResult) throws IOException {
			double score = 0.;
			Map<String, Integer> freq = new HashMap<>();
			String prev = "^";	// 文頭
			Set<String> kishutsu = new HashSet<>();

			// mecabの形態素解析結果からバイグラムの取得を行なう
			for (Morpheme r : mecabResult) {
				String morph = r.getSurfaceForm();
				String pos = r.getPos();
				String yomi = r.getReadings(0);

				String feat = Utilities.getFeature(morph, pos, yomi);

				// バイグラム作成
				String bigram = prev.concat(feat);
				prev = feat;

				if (!kishutsu.contains(bigram)) {
					freq.put(bigram, 1);
				}
				
				kishutsu.add(bigram);
			}

			String feat = "\\$";
			String bigram = prev.concat(feat);
			if (!kishutsu.contains(bigram)) {
				freq.put(bigram, freq.containsKey(bigram) ? freq.get(bigram) + 1 : 0);
			}
			
			// スコアリング
			for (String bg : freq.keySet()) {
				double p = ansFeatrue.containsKey(bg) ? ansFeatrue.get(bg) : 0.;
				p = Math.sqrt(p);
				score += p;
			}

			return score;
		}

		private double esk(List<Morpheme> mecabResult,
				List<String> sentQ, LinkedHashSet<String> kwN) {
			double score = 0.;
			// sentQと同様の理由により1次元リストとした。
			Utilities.GetKeyword getKeyword = new Utilities.GetKeyword(mecabResult.iterator());
			List<String> sentA = new ArrayList<>(getKeyword.kwNoun);

			if (!sentA.isEmpty()) {
				/**
				 * 2次元配列へのリファレンス2つと、共起関係を見る語数、減衰パラメータを引数とし、2つのESKによる計算結果を返す
				 * 1形態素の組み合わせの計算と同時に、後に続く計算の下処理を行なう
				 * sentencePはsentenceと同じ形をした配列で、要素が配列(a,b,c,d,e)となっている
				 * a...その語が最初に現れた位置
				 * b...その語が最初に現れた意味カテゴリの位置
				 * c...その語が1つ前に現れた位置 初めての場合は-1 さらに2文に共通して現れる場合は-2
				 * d...もう一方の文でその語が最初に現れる位置
				 * e...もう一方の文でその語が最初に現れる意味カテゴリの位置
				 * wordPlcの要素は、その語が後でどこに出現するかを示した配列になっている。初めて出てくる語以外では定義されない
				 */
				List<String> sentence1 = sentA;
				List<String> sentence2 = sentQ;
				double lambda = 0.8; // 減衰パラメータ
				// 2つの文の内積（類似度）
				class Product {
					double sentence1and2, sentence1and1, sentence2and2;
				}
				Product product = new Product();
				int value1, value2; // 2つの文の値
				Set<String> check = new HashSet<>(); // 同じ語を2度計算しないようにチェックするSet
				class SentenceP {
					int a, b, c, d, e;
					SentenceP(int a, int b, int c) {
						this.a = a;
						this.b = b;
						this.c = c;
					}
					SentenceP(SentenceP that) {
						this.a = that.a;
						this.b = that.b;
						this.c = that.c;
						this.d = that.d; 
						this.e = that.e;
					}
				}
				Map<Integer, SentenceP> sentenceP1 = new HashMap<>();
				Map<Integer, SentenceP> sentenceP2 = new HashMap<>(); // 文を計算処理できるようにしたもの
				Map<Integer, Map<Integer, Integer>> wordPlc1 = new HashMap<>();
				Map<Integer, Map<Integer, Integer>> wordPlc2 = new HashMap<>(); // 重なる語の場所が入る配列
				
				for (int s1 = 0; s1 < sentence2.size(); s1++) {
					if (check.contains(sentence2.get(s1))) {
						continue;
					}
					check.add(sentence2.get(s1));
					sentenceP2.put(s1, new SentenceP(s1, 0, -1));
					value2 = 1;
					if (!wordPlc2.containsKey(s1)) {
						wordPlc2.put(s1, new HashMap<Integer, Integer>());
					}
					wordPlc2.get(s1).put(0, s1);
					for (int mw1 = s1 + 1; mw1 < sentence2.size(); mw1++) {
						if (sentence2.get(s1).equals(sentence2.get(mw1))) {
							sentenceP2.put(mw1, new SentenceP(s1, 0, wordPlc2.get(s1).get(value2 - 1)));
							wordPlc2.get(s1).put(value2, mw1);
							value2++;
						}
					}
					product.sentence2and2 += value2 * value2;
				}
				check.clear();
				for (int s1 = 0; s1 < sentence1.size(); s1++) {
					if (check.contains(sentence1.get(s1))) {
						continue;
					}
					check.add(sentence1.get(s1));
					value1 = 1; value2 = 0;
					sentenceP1.put(s1, new SentenceP(s1, 0, -1));

					for (int mw1 = 0; mw1 < sentence2.size(); mw1++) {
						if (sentence1.get(s1).equals(sentence2.get(mw1))) {
							sentenceP1.get(s1).c = -2;
							sentenceP1.get(s1).d = mw1;
							sentenceP1.get(s1).e = 0;
							value2 = wordPlc2.get(mw1).size();
							break;
						}
					}
					wordPlc1.get(s1).put(0, s1);
					for (int mw1 = s1 + 1; mw1 < sentence1.size(); mw1++) {
						if (sentence1.get(s1).equals(sentence1.get(mw1))) {
							sentenceP1.put(mw1, new SentenceP(sentenceP1.get(s1)));
							sentenceP1.get(mw1).c = wordPlc1.get(s1).get(value1 - 1);
							wordPlc1.get(s1).put(value1, mw1);
							value1++;
						}
					}
					product.sentence1and2 += value1 * value2;
					product.sentence1and1 += value1 * value1;
				}

				for(int s1 = 0; s1 < sentence1.size() -1; s1++) {
					if (sentenceP1.get(s1).c >= 0){
						continue;
					}
					for(int s2 = s1 + 1; s2 < sentence1.size(); s2++) {
						if(sentenceP1.get(s2).c > s1){
							continue;
						}
						value1 = 0;
						for (int mw1 : wordPlc1.get(s1).values()) {
							for (int mw2 : wordPlc1.get(sentenceP1.get(s2).a).values()){
								if (mw1 < mw2) {
									value1 += Math.pow(lambda, mw2 - mw1 - 1);
								}
							}
						}
						product.sentence1and1 += value1 * value1;
						if (sentenceP1.get(s1).c == -2 && sentenceP1.get(sentenceP1.get(s2).a).c == -2) {
							value2 = 0;
							for (int mw1 : wordPlc2.get(sentenceP1.get(s1).d).values()) {
								for (int mw2 : wordPlc2.get(sentenceP1.get(s2).d).values()) {
									if(mw1 < mw2){
										value2 += Math.pow(lambda, mw2 - mw1 - 1);
									}
								}
							}
							product.sentence1and2 += value1 * value2;
						}
					}
				}
				for (int s1 = 0; s1 < sentence2.size() - 1; s1++) {
					if(sentenceP2.get(s1).c >= 0){
						continue;
					}
					for (int s2 = s1 + 1; s2 < sentence2.size(); s2++) {
						if(sentenceP2.get(s2).c > s1){
							continue;
						}
						value2=0;
						for (int mw1 : wordPlc2.get(s1).values()){
							for (int mw2 : wordPlc2.get(sentenceP2.get(s2).a).values()){
								if (mw1 < mw2) {
									value2 += Math.pow(lambda, mw2 - mw1 - 1);
								}
							}
						}
						product.sentence2and2 += value2 * value2;
					}
				}
				score = product.sentence1and2 / Math.sqrt(product.sentence1and1 * product.sentence2and2);
			}
			return score;
		}

		private double topic(String line, LinkedHashSet<String> kyouki,
				List<Morpheme> mecabResult, LinkedHashSet<String> fukugo,
				LinkedHashSet<String> kwN) {
			double score = 0;
			Map<String, Integer> freq = new HashMap<>();

			for (Morpheme r : mecabResult) {
				String morph = r.getSurfaceForm();
				freq.put(morph, freq.containsKey(morph) ? freq.get(morph) + 1 : 1);
			}

			// 関連語およびキーワードが存在したらスコア加算
			for (String morph : freq.keySet()) {
				score += kyouki.contains(morph) ? 1. : 0.;
			}
			// キーワード複合語が存在したらさらにスコア加算
			if (Constant.D != 0.) {
				for (String f : fukugo) {
					if (kwN.contains(f)) {
						continue;
					}
					score += line.contains(f) ? Constant.D : 0.;
				}
			}

			return score;
		}
	}
}
