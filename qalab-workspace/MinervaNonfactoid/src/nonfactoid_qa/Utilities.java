package nonfactoid_qa;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Formatter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.axis.utils.ByteArrayOutputStream;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.TOP;
import org.apache.uima.jcas.tcas.Annotation;
import org.kachako.share.regex.RegexCache;

import org.kachako.types.qa.KeyTerm;
import org.u_compare.comparable.gui.NewColumnGroup;
import org.u_compare.shared.japanese.syntactic.morpheme.Morpheme;

import com.ibm.icu.text.CharsetDetector;
import com.ibm.icu.text.CharsetMatch;

// 一箇所でしか使われていないメソッドはそこに移動する
/**
 * 複数のAnnotatorで使用するメソッドを集めたユーティリティクラスです。
 * @author Takayuki SUZUKI
 *
p */
public class Utilities {
	
	public static LinkedHashSet<String> getKeywordSet(JCas view,
			String keyTermAttribute) {
		LinkedHashSet<String> keywordSet = new LinkedHashSet<>();

		for (Iterator<Annotation> keyTermFSIterator = view.getAnnotationIndex(KeyTerm.type).iterator(); keyTermFSIterator.hasNext();) {
			KeyTerm keyTermFS = (KeyTerm) keyTermFSIterator.next();

			if (keyTermFS.getAttribute().equals(keyTermAttribute)) {
				keywordSet.add(keyTermFS.getCoveredText());
			}
		}

		return keywordSet;
	}

	private static final int NUMBER_OF_RETRIES = 10;
	private static final long RETRY_INTERVAL_MILLISECONDS = 10000;

	/**
	 * URLにアクセスして得られたHTML文書を返します。
	 * @param urlToHtml アクセス先のURLがキーとして格納されたマップ。得られたHTML文書は対応する値として格納される。
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	public static void myGet(Map<String, String> urlToHtml) throws IOException, InterruptedException {
		LinkedHashSet<String> urlSet = new LinkedHashSet<>(urlToHtml.keySet());
		urlToHtml.clear();

		for (String urlString : urlSet) {
			URL url = new URL(urlString);

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			try {
				for (int i = 0; i < NUMBER_OF_RETRIES; i++) {
					connection.connect();
					if (connection.getResponseCode() == HttpURLConnection.HTTP_UNAVAILABLE) {
						System.err.println("Service unavailable. Retrying after 10 seconds.");
						Thread.sleep(RETRY_INTERVAL_MILLISECONDS);
					}
					else {
						break;
					}
				}
			} catch (UnknownHostException e) {
				continue;
			}

			try {
				urlToHtml.put(urlString, getString(connection.getInputStream()));
			} catch (FileNotFoundException e) {
			}
		}
		return;
	}

	private static String getString(InputStream inputStream) throws IOException {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		for (int length = 0; (length = inputStream.read(buffer)) >= 0;) {
			byteArrayOutputStream.write(buffer, 0, length);
		}

		CharsetDetector detector = new CharsetDetector();
		return detector.getString(byteArrayOutputStream.toByteArray(), null);
	}
	
	public static interface CreateViewsFunction<T> {
		void apply(JCas view, T content, int index);
	}
	public static <T> int createViews(JCas baseJCas, String viewNamePrefix, Collection<T> contents, CreateViewsFunction<T> setUpViewFunction, int start) throws CASException {
		int count = start;
		for (T content: contents) {
			JCas view = baseJCas.createView(String.format("%s.%d", viewNamePrefix, count));
			setUpViewFunction.apply(view, content, count);
			count++;
		}
		return count + 1;
	}
	private static final RegexCache regexCache = new RegexCache();
	private static Set<String> surface = null;
	
	// 品詞情報から素性選択
	public static String getFeature(String morph, String pos, String yomi) throws IOException {
		if (surface == null) {
			initializeSurface();
		}
		String feat;
		// 表層をそのまま素性とする場合
		String surfaceElement = String.format("%s(%s)", yomi, pos);
		if(surface.contains(surfaceElement) || regexCache.find("/^(助詞|助動詞|名詞,非自立)/", pos)) {
			feat = surfaceElement;
	    }
	    // 品詞に置き換える場合
	    else {
	    	feat = String.format("<%s>", pos);
	    }

	    return feat;
	}

	private static void initializeSurface() throws IOException {
		surface = new HashSet<>();
		try (BufferedReader file = new BufferedReader(new FileReader(Constant.SURFACE_FILE_NAME))) {
			for (String inputLine = null; (inputLine = file.readLine()) != null;) {
				if (regexCache.find("^\\#", inputLine)) {
					continue;
				}
				else if (regexCache.find("^(.*?\\(.*\\))", inputLine)) {
					surface.add(regexCache.lastMatched(1));
				}
			}
		}
	}

	static class GetKeyword {
		LinkedHashSet<String> kwNoun;
		LinkedHashSet<String> kwOther;
		
		<T extends Annotation> GetKeyword(Iterator<T> morphemeFSIterator) {
			kwNoun = new LinkedHashSet<>(); kwOther = new LinkedHashSet<>();
			
			while (morphemeFSIterator.hasNext()) {
				Morpheme morphemeFS = (Morpheme) morphemeFSIterator.next();
				if (isNounKeyword(morphemeFS)) {
					if (!kwNoun.contains(morphemeFS.getSurfaceForm())) {
						kwNoun.add(morphemeFS.getSurfaceForm());
					}
				} else if (isVerbKeyword(morphemeFS)) {
					if (!kwOther.contains(morphemeFS.getSurfaceForm())) {
						kwOther.add(morphemeFS.getSurfaceForm());
					}
				}
			}
		}

		private boolean isNounKeyword(Morpheme morphemeFS) {
			String detailedPosInMecabForm = convertDetailedPosToMecabForm(morphemeFS
					.getDetailedPos());
			if (regexCache
					.find(
							"^(名詞,(普通名詞|サ変接続|形容動詞語幹|ナイ形容詞語幹|副詞可能|一般|数|固有名詞|接尾,(サ変接続|一般|形容動詞語幹|助数詞|人名|地域))|記号,アルファベット)",
							detailedPosInMecabForm)
					&& !regexCache.find("^(何|何者)$",
							morphemeFS.getConjugateForm())) {
				return true;
			} else {
				return false;
			}
		}

		private boolean isVerbKeyword(Morpheme morphemeFS) {
			String detailedPosInMecabForm = convertDetailedPosToMecabForm(morphemeFS
					.getDetailedPos());
			if (regexCache.find("^(動詞,自立|形容詞,自立)", detailedPosInMecabForm)
					&& !regexCache.find("^(ある|いる|する|なる|やる)$",
							morphemeFS.getConjugateForm())) {
				return true;
			} else {
				return false;
			}
		}

	}

	public static String convertDetailedPosToMecabForm(String chasenDetailedPos) {
		return chasenDetailedPos.replace('-', ',');
	}

	public static String join(List<String> strings, String separator) {
		StringBuilder joinedStrnig = new StringBuilder();
		for (String string : strings) {
			joinedStrnig.append(string);
			joinedStrnig.append(separator);
		}
		return joinedStrnig.substring(0,
				joinedStrnig.length() - separator.length());
	}
	
	private Utilities() {}
}
