package nonfactoid_qa;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.kachako.share.regex.RegexCache;
import org.kachako.types.qa.AnswerFeature;
import org.u_compare.shared.japanese.syntactic.morpheme.Morpheme;

/**
 * 回答の特徴表現を抽出します。
 * @author Takayuki SUZUKI
 *
 */
public class AnswerFeatureAnnotator2 extends JCasAnnotator_ImplBase {
	private static final RegexCache regexCache = new RegexCache(); 
	
	// 取得する類似度の閾値 || 件数
	private static final double TH_SIM = 0.6;
	private static final int QNUM = 500;

	// 出力件数
	private static final int N_OUTPUT = 200;

	// インデックスファイルのあるディレクトリ
	private static final String DIR_INDEX = "corpus/index3";
	// 回答文中のバイグラムのDFファイルがあるディレクトリ
	private static final String DIR_DF = "corpus/bigram-df3";
	// 回答文集合全体でのバイグラムのDFファイル
	private static final String FILE_DF_ALL = DIR_DF + "/bigram-df-all.txt";

	// 疑問詞リスト
	// 値は前・後何形態素を見るか
	// 疑問詞リスト
	private static final Map<String, String> WH = new HashMap<>();

	// 表層のまま使う名詞 QFの位置に来る
	private static final Set<String> surph = new HashSet<>();

	public AnswerFeatureAnnotator2() {
		super();

		//WH.put("ナニ(名詞,代名詞,一般)", "nani");
		WH.put("ドコ(名詞,代名詞,一般)", "doko");
		WH.put("ダレ(名詞,代名詞,一般)", "dare");
		//WH.put("ナン(名詞,代名詞,一般)", "nan");
		WH.put("ドチラ(名詞,代名詞,一般)", "dochira");
		WH.put("ドレ(名詞,代名詞,一般)", "dore");
		//WH.put("ドッチ(名詞,代名詞,一般)", "docchi");
		WH.put("イツ(名詞,代名詞,一般)", "itsu");
		WH.put("ドナタ(名詞,代名詞,一般)", "donata");
		//WH.put("イクツ(名詞,代名詞,一般)", "ikutsu");
		//WH.put("ドッカ(名詞,代名詞,一般)", "dokka");
		WH.put("イズレ(名詞,代名詞,一般)", "izure");
		//WH.put("ナアニ(名詞,代名詞,一般)", "naani");
		WH.put("ドノ(連体詞)", "dono");
		WH.put("ドンナ(連体詞)", "donna");
		WH.put("ドウイウ(連体詞)", "douiu");
		WH.put("イカナル(連体詞)", "ikanaru");
		WH.put("ドウ(副詞,助詞類接続)", "dou");
		WH.put("ナゼ(副詞,助詞類接続)", "naze");
		WH.put("ドウシテ(副詞,一般)", "doushite");
		WH.put("ナンデ(副詞,一般)", "nande");
		WH.put("イクラ(副詞,一般)", "ikura");
		WH.put("ナゼ(副詞,一般)", "naze");
		WH.put("イツノマニ(副詞,一般)", "itsunomani");
		//WH.put("ッテナ(助詞,格助詞,連語)", "ttena");
		//WH.put("ナニモノ(名詞,一般)", "nanimono");

		surph.add("イミ(名詞,サ変接続)");
		surph.add("ホウホウ(名詞,一般)");
		surph.add("チガイ(名詞,ナイ形容詞語幹)");
		surph.add("ヒト(名詞,一般)");
		surph.add("カタ(名詞,接尾,特殊)");
		surph.add("ナマエ(名詞,一般)");
		surph.add("ツクリカタ(名詞,一般)");
		surph.add("カンジ(名詞,一般)");
		surph.add("リユウ(名詞,一般)");
		surph.add("コトバ(名詞,一般)");
		surph.add("バショ(名詞,一般)");
		surph.add("ユライ(名詞,サ変接続)");
		surph.add("シカタ(名詞,ナイ形容詞語幹)");
		surph.add("ゴゲン(名詞,一般)");
		surph.add("カゼ(名詞,一般)");
		surph.add("ナイヨウ(名詞,一般)");
		surph.add("オススメ(名詞,サ変接続)");
		surph.add("ジョウホウ(名詞,一般)");
		surph.add("ゲンイン(名詞,一般)");
		surph.add("コツ(名詞,一般)");
		surph.add("メリット(名詞,一般)");
		surph.add("デメリット(名詞,一般)");
		surph.add("カンケイ(名詞,サ変接続)");
		surph.add("キジュン(名詞,一般)");
		surph.add("ジョウタイ(名詞,一般)");
		surph.add("ヨミカタ(名詞,一般)");
		surph.add("ツカイカタ(名詞,一般)");
		surph.add("テイギ(名詞,サ変接続)");
		surph.add("ヤリカタ(名詞,一般)");
		surph.add("トクチョウ(名詞,一般)");
		surph.add("エイキョウ(名詞,サ変接続)");
		surph.add("ジョウケン(名詞,一般)");
		surph.add("ケイイ(名詞,一般)");
		surph.add("コト(名詞,一般)");
		surph.add("シュダン(名詞,一般)");
		surph.add("モクテキ(名詞,一般)");

	}

	private final class SearchInterrogatives {
		private int idxWh; // 疑問詞の位置
		private int idx;
		private String whpos;
		private List<String> morphList;

		SearchInterrogatives(JCas answerFeatureViewJCas) throws IOException {
			idxWh = -1; // 疑問詞の位置
			idx = 3;
			whpos = "";
			morphList = new ArrayList<>(Arrays.asList("none", "none", "none")); // ダミー入り

			for (Iterator<Annotation> morphemeFSIterator = answerFeatureViewJCas.getAnnotationIndex(Morpheme.type).iterator(); morphemeFSIterator.hasNext();) {
				Morpheme morphemeFS = (Morpheme) morphemeFSIterator.next();
				
				String morph = morphemeFS.getSurfaceForm();
				String pos = Utilities.convertDetailedPosToMecabForm(morphemeFS.getDetailedPos());
				String yomi = morphemeFS.getReadings(0);

				// 読点は飛ばす
				// TODO 他の形態素解析器の出力にも対応させる
				if(pos.equals("記号,読点,*,*")) {
					continue;
				}

				String feat = Utilities.getFeature(morph, pos, yomi);
				String whKey = String.format("%s(%s)", yomi, pos);
				if (WH.containsKey(whKey)) {
					idxWh = idx;
					whpos = whKey;
				}
				morphList.add(feat);
				idx++;
			}
		}
	}

	private List<Double> calculateWeigt(List<String> f0) {
		List<Double> weight = new ArrayList<>();
		final double r = 0.8; // 減衰定数

		// 類似度計算の際の重みを計算しておく
		for(int i = 0; i < Math.min(f0.size(), 7); i++) {
			if (f0.get(i).equals("none")) {
				weight.add(0.);
			} else if (surph.contains(f0.get(i))) {
				weight.add(1.);
			} else {
				int gen = Math.abs(i - 3);
				double w1 = (regexCache.find("<.*>", f0.get(i)) ? 0.25 : 0.5);
				weight.add(Math.pow(r, gen) * w1);
			}
		}
		return weight;
	}

	private final class Index {
		private double sim;
		private String qId;
		
		Index(double sim, String qId) {
			this.sim = sim;
			this.qId = qId;
		}
	}
		
	private final class ReadIndexFile {
		private Map<String, Index> index;
		private String wh;
		
		ReadIndexFile(String whpos, List<Double> weight, String f0) throws IOException {
			// インデックスファイルを開く
			index = new HashMap<>();
			wh = WH.get(whpos);
			try (BufferedReader in = new BufferedReader(new FileReader(String.format("%s/%s.txt", DIR_INDEX, wh)))) {
				for (String inputLine = null; (inputLine = in.readLine()) != null;) {
					if (regexCache.find("^\\# total: (\\d+)", inputLine)) {
						continue;
					} else {
						String[] splitLine = inputLine.split(" ");
						String f = splitLine[0];
						String qIds = splitLine[1];

						double sim = sim(f0, f, weight);
						index.put(f, new Index(sim, qIds));
					}
				}
			}
		}
	}
	
	// 7-gram の類似度を計算
	// 疑問詞からの距離に応じて減衰
	// 品詞よりも表層のままの要素を重視
	// 表層のままの名詞を重視
	private double sim(String f1, String f2, List<Double> weight) {
		double sim = 0.;
		String[] f1Array = f1.split("_");
		String[] f2Array = f2.split("_");
		
		for (int i = 0; i < Math.min(Math.min(f1Array.length, f2Array.length), 7); i++) {
			if (f1Array[i].equals("none")) {
				continue;
			}
			// 一致
			else if (f1Array[i].equals(f2Array[i])) {
				sim += weight.get(i);
			}
			// (入力側)表層 ←→ (コーパス側)品詞
			// 品詞が一致すれば少し加点
			else if (surph.contains(f1Array[i]) && regexCache.find("<名詞,(一般|サ変接続),.*>", f2Array[i])) {
				sim += weight.get(i) * 0.5;
			}
			// (入力側)品詞 ←→ (コーパス側)表層
			// または，互いに表層だが一致しない
			// ペナルティ
			else if (surph.contains(f2Array[i])) {
				sim -= 1.;
			}
		}

		return sim;
	}

	private Set<String> getType(Map<String, Index> index) {
		// 類似度が高い質問から一定件数(一定タイプ数)を取得
		// 類似度が一定の割合になるまで
		List<String> qId = new ArrayList<>();
		Set<String> type = new HashSet<>();
		TreeSet<Map.Entry<String, Index>> sortedIndexEntrySet = new TreeSet<>(new Comparator<Map.Entry<String, Index>>() {
			@Override
			public int compare(Map.Entry<String, Index> o1, Map.Entry<String, Index> o2) {
				return Double.compare(o2.getValue().sim, o1.getValue().sim);
			}
		});
		sortedIndexEntrySet.addAll(index.entrySet());
		double simMax = sortedIndexEntrySet.first().getValue().sim;

		for (Map.Entry<String, Index> entry : sortedIndexEntrySet) {
			String f = entry.getKey();
			double sim = entry.getValue().sim;
			if (sim < simMax * TH_SIM || qId.size() >= QNUM) {
				break;
			} else {
				qId.addAll(Arrays.asList(entry.getValue().qId.split(",")));
				type.add(f);
			}
		}

		return type;
	}

	private final class GetDf {
		private Map<String, Integer> df;
		private int dfType;
		
		GetDf(String wh, Set<String> typeSet) throws IOException {
			df = new HashMap<>();
			dfType = 0;
			
			// DFを取得(類似タイプ間で足し合わせ)
			try (BufferedReader in = new BufferedReader(new FileReader(DIR_DF + "/bigram-df-" + wh + ".txt"))) {
				boolean flag = false;

				for (String inputLine = null; (inputLine = in.readLine()) != null;) {
					if (regexCache.find("^\\#", inputLine)) {
						continue;
					} else if(regexCache.find("<TYPE ngram=\"(.*?)\" num=\"(\\d+)\">", inputLine)) {
						if (typeSet.contains(regexCache.lastMatched(1))) {
							flag = true;
							dfType += Integer.parseInt(regexCache.lastMatched(2));
						} else {
							flag = false;
						}
					} else if (flag && regexCache.find("^(.*?) (\\d+)$", inputLine)) {
						int newValue = Integer.parseInt(regexCache.lastMatched(2)) +
								(df.containsKey(regexCache.lastMatched(1)) ? df.get(regexCache.lastMatched(1)) : 0);
						df.put(regexCache.lastMatched(1), newValue);
					}
				}
			}
		}
	}

	private Map<String, Integer> getAllDf() throws IOException {
		// DF all 取得
		Map<String, Integer> dfAll = new HashMap<>();
		try (BufferedReader in = new BufferedReader(new FileReader(FILE_DF_ALL))) {
			for (String inputLine = null; (inputLine = in.readLine()) != null;) {
				if (regexCache.find("^(.*?) (\\d+)$", inputLine)) {
					dfAll.put(regexCache.lastMatched(1), Integer.parseInt(regexCache.lastMatched(2)));
				}
			}
		}
		return dfAll;
	}

	private Map<String, Double> calculateChi(Map<String, Integer> df, Map<String, Integer> dfAll, int dfType) {

		// カイ2乗値計算
		Map<String, Double> chi = new HashMap<>(); 

		for (Map.Entry<String, Integer> entry : df.entrySet()) {
			String w = entry.getKey();
			int A = entry.getValue();

			// 頻度が低い語はノイズとして除く
			if (A < 2) {
				continue;
			}

			int AB = dfAll.containsKey(w) ? dfAll.get(w) : 0;
			int B = AB - A;
			int AC = dfType;
			int C = AC - A;
			int N = Constant.DF_TOTAL;
			int BD = N - AC;
			int D = BD - B;
			int CD = C + D;

			double chiValue = (double) N * Math.pow((A * D - B * C), 2) / (AB * CD * AC * BD);
			chi.put(w, chiValue);
		}
		
		return chi;
	}

	private void storeChi(JCas answerFeatureViewJCas, Map<String, Double> chi) {
		TreeSet<Map.Entry<String, Double>> sortedChiEntrySet = new TreeSet<>(new Comparator<Map.Entry<String, Double>>() {
			@Override
			public int compare(Entry<String, Double> o1,
					Entry<String, Double> o2) {
				return Double.compare(o2.getValue(), o1.getValue());
			}
		});
		sortedChiEntrySet.addAll(chi.entrySet());

		double max = sortedChiEntrySet.first().getValue();
		int cnt = 0;
		for (Map.Entry<String, Double> entry : sortedChiEntrySet) {
			if (cnt >= N_OUTPUT) {
				break;
			}
			else {
				double c = entry.getValue() / max;
				AnswerFeature answerFeatureFS = new AnswerFeature(answerFeatureViewJCas);
				answerFeatureFS.addToIndexes();
				answerFeatureFS.setWord(entry.getKey());
				answerFeatureFS.setChiSquare(c);
			}
			cnt++;
		}
	}

	@Override
	public void process(JCas baseJCas) throws AnalysisEngineProcessException {

		JCas answerFeatureViewJCas;
		try {
			answerFeatureViewJCas = baseJCas.getView(Constant.ANSWER_FEATURE_VIEW_NAME);
		} catch (CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}

		SearchInterrogatives searchInterrogatives;
		try {
			searchInterrogatives = new SearchInterrogatives(answerFeatureViewJCas);
		} catch (IOException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}
		int idxWh = searchInterrogatives.idxWh;
		String whpos = searchInterrogatives.whpos;
		List<String> morphList = searchInterrogatives.morphList;
		
		// 疑問詞があったら
		// 特徴表現＝タイプ＝7gramを取得
		if(idxWh > 2) {
			morphList.addAll(Arrays.asList("none", "none", "none")); // ダミー挿入

			int mae = 3;
			int at = 3;
			int start = idxWh - mae;
			int end = idxWh + at;
			List<String> f0 = morphList.subList(start, end);
			String joinedF0 = Utilities.join(f0, "_");
			
			List<Double> weight = calculateWeigt(f0);

			ReadIndexFile readIndexFile;
			try {
				 readIndexFile = new ReadIndexFile(whpos, weight, joinedF0);
			} catch (IOException e) {
				e.printStackTrace();
				throw new AnalysisEngineProcessException(e);
			}
			Map<String, Index> index = readIndexFile.index;
			String wh = readIndexFile.wh;
			
			Set<String> typeSet = getType(index);
			
			GetDf getDf;
			try {
				getDf = new GetDf(wh, typeSet);
			} catch (IOException e) {
				e.printStackTrace();
				throw new AnalysisEngineProcessException(e);
			}
			
			Map<String, Integer> dfAll;
			try {
				dfAll = getAllDf();
			} catch (IOException e) {
				e.printStackTrace();
				throw new AnalysisEngineProcessException(e);
			}
			
			Map<String, Double> chi = calculateChi(getDf.df, dfAll, getDf.dfType);
			storeChi(answerFeatureViewJCas, chi);
		}
		// 疑問詞が無かったら
		else {
		}
		return;
    }
}
