package nonfactoid_qa;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;

import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.IntegerArray;
import org.apache.uima.jcas.cas.TOP;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.kachako.types.qa.Query;
import org.kachako.types.qa.Result;
import org.kachako.types.qa.ViewId;


/**
 * 渡されたクエリをYahoo! Web検索を使って検索し、その結果を返します。
 * 
 * 入力クエリは、クエリごとにWeb Seach Query Viewを作成し、そのSofaにクエリを格納します。
 * クエリは"WORD1 WORD2 WORD3..."という形式で格納します。各単語間は半角スペース1個です。
 * 
 * 各クエリに対する検索結果は、1件ずつWeb Search Result Viewが作成され、それにResult FSとして格納されます。
 *
 * @author Takayuki SUZUKI
 */
public class YahooSearchAnnotator extends JCasAnnotator_ImplBase {

	@Override
	public void process(JCas baseJCas) throws AnalysisEngineProcessException {
		try {		
			// 検索APIのURLとその検索結果を格納するマップと、クエリを逆引きするマップ
			// 各Web Search Query Viewの各クエリから検索APIのURLを生成
			final Map<ViewId, ArrayList<ViewId>> resultViewIdFSsMap = new HashMap<>();

			SetUpUrlMaps setUpUrlMaps = new SetUpUrlMaps(baseJCas);
			Map<String, String> urlToHtml = setUpUrlMaps.urlToHtml;
			final Map<String, ViewId> urlToQueryViewIdFS = setUpUrlMaps.urlToQueryViewIdFS;

			// 検索
			Utilities.myGet(urlToHtml);

			// Web Search Snippet Viewを作成し検索結果を格納する
			for (Map.Entry<String, String> entry : urlToHtml.entrySet()) {
				final String url = entry.getKey();
				String html = entry.getValue();
				Document yahooXml = new SAXBuilder().build(new StringReader(html));

				Utilities.createViews(
						baseJCas,
						Constant.WEB_SEARCH_RESULT_VIEW_NAME_PREFIX,
						(List<Element>) yahooXml.getRootElement().getChildren("Result"),
						new Utilities.CreateViewsFunction<Element>() {
							@Override
							public void apply(JCas view, Element resultElement, int index) {
								view.setDocumentText(((Element) resultElement.getChildren("Summary").get(0)).getText());
								addResultFS(view, resultElement);
								addSourceDocumentInformationFS(view, url);
								addViewId(view, resultViewIdFSsMap, urlToQueryViewIdFS.get(url), index);
							};
						},
						0);
			}

			// 各Query ViewId素性構造に子となるResult ViewIdを追加
			for (Iterator<JCas> queryViewJCasIterator = baseJCas.getViewIterator(Constant.WEB_SEARCH_QUERY_VIEW_NAME_PREFIX); queryViewJCasIterator.hasNext();) {
				JCas queryViewJCas = queryViewJCasIterator.next();
				ViewId queryViewIdFS = (ViewId) queryViewJCas.getJFSIndexRepository().getAllIndexedFS(ViewId.type).next();
				
				ArrayList<ViewId> resultViewIdFSs = resultViewIdFSsMap.get(queryViewIdFS);
				FSArray resultViewIdFSArray = new FSArray(queryViewJCas, resultViewIdFSs.size());
				for (int i = 0; i < resultViewIdFSs.size(); i++) {
					resultViewIdFSArray.set(i, resultViewIdFSs.get(i));
				}
				
				queryViewIdFS.setChildren(resultViewIdFSArray);
			}
			
		} catch (CASException | IOException | InterruptedException | JDOMException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}

		return;
	}

	private final class SetUpUrlMaps {
		Map<String, String> urlToHtml;
		Map<String, ViewId> urlToQueryViewIdFS;
		
		SetUpUrlMaps(JCas baseJCas) throws CASException, UnsupportedEncodingException{
			urlToHtml = new HashMap<>(); urlToQueryViewIdFS = new HashMap<>();
			
			for (Iterator<JCas> queryViewIterator = baseJCas.getViewIterator(Constant.WEB_SEARCH_QUERY_VIEW_NAME_PREFIX);queryViewIterator.hasNext();) {
				JCas queryViewJCas = queryViewIterator.next();

				String query = queryViewJCas.getDocumentText();
				String url = queryToUrl(query, "web", Constant.SNUM * 3, Constant.RMDOMAIN);
				urlToHtml.put(url, ""); // TODO 検索目的によってSNUMが異なるのに対処する
				
				ViewId queryViewIdFS = new ViewId(queryViewJCas);
				queryViewIdFS.addToIndexes();
				urlToQueryViewIdFS.put(url, queryViewIdFS);
			}
		}

		private String queryToUrl(String query, String type, int sNum, String[] rmDomain) throws UnsupportedEncodingException {
			String[] q = query.split("\\ ");
			for (int i = 0; i < q.length; i++) {
				if (!q[i].equals("OR")) {
					q[i] = "\"".concat(q[i]).concat("\"");
				}
			}

			query = Utilities.join(Arrays.asList(q), " ");

			for (String rm : rmDomain) {
				query.concat(" -inurl:").concat(rm);
			}

			query = URLEncoder.encode(query, "UTF-8");

			String queryUrl = "";
			if (type.equals("web")) {
				queryUrl = String.format("http://search.yahooapis.jp/WebSearchService/V2/webSearch?appid=%s&query=%s&results=%d",
								Constant.YAHOO_API_ID, query, sNum);
			}

			return queryUrl;
		}
	}

	private void addSourceDocumentInformationFS(JCas snippetViewJCas, String url) {
		SourceDocumentInformation snippetInformationFS = new SourceDocumentInformation(snippetViewJCas);
		snippetInformationFS.addToIndexes();
		snippetInformationFS.setUri(url);
		return;
	}

	private void addResultFS(JCas snippetViewJCas, Element resultElement) {
		Result resultFS = new Result(snippetViewJCas);
		resultFS.addToIndexes();
		resultFS.setUrl(((Element) resultElement.getChildren("Url").get(0)).getText());
		resultFS.setTitle(((Element) resultElement.getChildren("Title").get(0)).getText());
		return;
	}

	private void addViewId(JCas snippetViewJCas, Map<ViewId, ArrayList<ViewId>> resultViewIdFSsMap, ViewId queryViewIdFS, int snippetViewId) {
		// Web Seach Resut ViewにViewId素性構造を格納
		FSArray queryViewIdFSs = new FSArray(snippetViewJCas, 1);
		queryViewIdFSs.set(0, queryViewIdFS);

		ViewId resultViewIdFS = new ViewId(snippetViewJCas);
		resultViewIdFS.addToIndexes();
		resultViewIdFS.setParents(queryViewIdFSs);
		
		// Query ViewIdからResult ViewIdを引けるようにする
		ArrayList<ViewId> resultViewIdFSs = resultViewIdFSsMap.get(queryViewIdFS);
		if (resultViewIdFSs == null) {
			resultViewIdFSs = new ArrayList<>();
		}
		resultViewIdFSs.add(resultViewIdFS);
		resultViewIdFSsMap.put(queryViewIdFS, resultViewIdFSs);

		return;
	}


}
