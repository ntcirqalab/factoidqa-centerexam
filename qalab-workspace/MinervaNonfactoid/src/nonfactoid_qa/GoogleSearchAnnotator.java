package nonfactoid_qa;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.kachako.types.qa.ViewId;

import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.customsearch.Customsearch;
import com.google.api.services.customsearch.model.Search;

public class GoogleSearchAnnotator extends JCasAnnotator_ImplBase {
	private static final String GOOGLE_API_KEY = "AIzaSyC-uH9cRx6B1e2VPeM6fX8ofmQ-re_6wqY"; //TODO 鈴木のAPI keyなので後でkachako用のAPI keyに変更する
	private static final String CUSTOMSEARCH_ENGINE_ID = "008194797360003695282:uxon90j2ohq";

	@Override
	public void process(JCas baseJCas) throws AnalysisEngineProcessException {
		Customsearch customsearch =	new Customsearch(new NetHttpTransport(), new GsonFactory(),	null);

		
		try {
			int viewCount = 0;
			for (Iterator<JCas> webSearchQueryViewIterator = baseJCas.getViewIterator(Constant.WEB_SEARCH_QUERY_VIEW_NAME_PREFIX);
					webSearchQueryViewIterator.hasNext();) {
				JCas webSearchQueryView = webSearchQueryViewIterator.next();
				final ArrayList<ViewId> resultViewIdFSs = new ArrayList<>();

				final ViewId queryViewIdFS = new ViewId(webSearchQueryView);
				queryViewIdFS.addToIndexes();

				// 検索
				Customsearch.Cse.List cseList = customsearch.cse().list(webSearchQueryView.getDocumentText());
				cseList.setKey(GOOGLE_API_KEY);
				cseList.setCx(CUSTOMSEARCH_ENGINE_ID);
				Search searchResult = cseList.execute();
				List<com.google.api.services.customsearch.model.Result> resultItems = searchResult.getItems();
				
				// 結果をWeb Search Result Viewに格納
				viewCount = Utilities.createViews(
						baseJCas,
						Constant.WEB_SEARCH_RESULT_VIEW_NAME_PREFIX,
						resultItems,
						new Utilities.CreateViewsFunction<com.google.api.services.customsearch.model.Result>() {
							@Override
							public void apply(JCas view, com.google.api.services.customsearch.model.Result resultItem, int index) {
								view.setDocumentText(resultItem.getSnippet());
								addResultFS(view, resultItem);
								addSourceDocumentInformationFS(view, resultItem.getFormattedUrl());
								addViewIdFS(view, resultViewIdFSs, queryViewIdFS);
							}
						},
						viewCount);
				
				// Web Search Query Viewから対応する検索結果のWeb Search Result Viewを引けるように
				FSArray resultViewIdFSArray = new FSArray(webSearchQueryView, resultViewIdFSs.size());
				for (int i = 0; i < resultViewIdFSs.size(); i++) {
					resultViewIdFSArray.set(i, resultViewIdFSs.get(i));
				}
				queryViewIdFS.setChildren(resultViewIdFSArray);
			}
		} catch (CASException | IOException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}
	}

	private void addResultFS(JCas view,	com.google.api.services.customsearch.model.Result resultItem) {
		org.kachako.types.qa.Result resultFS = new org.kachako.types.qa.Result(view);
		resultFS.addToIndexes();
		resultFS.setUrl("http://" + resultItem.getFormattedUrl());
		resultFS.setTitle(resultItem.getTitle());
		return;
	}

	private void addSourceDocumentInformationFS(JCas view, String url) {
		SourceDocumentInformation sourceDocumentInformationFS = new SourceDocumentInformation(view);
		sourceDocumentInformationFS.addToIndexes();
		sourceDocumentInformationFS.setUri(url);
		return;
	}

	private void addViewIdFS(JCas resultViewJCas, ArrayList<ViewId> resultViewIdFSs, ViewId queryViewIdFS) {
		// Web Seach Resut ViewにViewId素性構造を格納
		FSArray queryViewIdFSs = new FSArray(resultViewJCas, 1);
		queryViewIdFSs.set(0, queryViewIdFS);

		ViewId resultViewIdFS = new ViewId(resultViewJCas);
		resultViewIdFS.addToIndexes();
		resultViewIdFS.setParents(queryViewIdFSs);
		
		// 元のQuery ViewIdから対応する検索結果のResult ViewIdをたどれるように
		resultViewIdFSs.add(resultViewIdFS);

		return;
	}

}
