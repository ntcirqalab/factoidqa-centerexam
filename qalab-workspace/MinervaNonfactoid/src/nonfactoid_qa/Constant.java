package nonfactoid_qa;

public class Constant {
//	PRE_EXPERIMENT and EXPERIMENT modes are not supported.
//	public enum LaunchMode {
//		NORMAL, PRE_EXPERIMENT, EXPERIMENT
//	}
//	public static final LaunchMode MODE = LaunchMode.EXPERIMENT;
//	These exeternal files are not used as PRE_EXPERIMENT and EXPERIMENT modes are not supported.
//	public static final String DIRT = "sample/topic2";
//	public static final String DIRD = "sample/doc2";
	public static double A = 1.;
	public static double B = 0.;
	public static double C = 1.;
	public static double D = 0.;
	public enum MargeMethod {
		PLUS, MULTIPLY
	}
	public static final MargeMethod MARGE = MargeMethod.MULTIPLY;
	public static final int SNUM = 30;
	//	public static final boolean ORIGINAL_DOC = true; このパラメタはサポートされません。
	public static final int TIMEOUT = 10;
	public static final String[] RMDOMAIN = { "qac", "www.amazon.co.jp",
			"www.rakuten.co.jp", "store.yahoo.co.jp", "shop" };
	public static final int N = 5;
	public static final double TH_SIM_PAS = 0.7;
	public static final double W_CNUM = 0.25;
	public static final double TH_PAS_NUM = 100;
	// public static final boolean P3BUN = false; このパラメタはサポートされません。
	public static final double TH_PAS = 0.5;
	public static final int L = 200;
	public static final int WINDOWSIZE = 3;
	public static final int DF_TOTAL =  902259;
	public enum PrintMode {
		MINIMUM, NORMAL, FULL
	}
	public static PrintMode PRINT_MODE = PrintMode.MINIMUM;
	// public static boolean MARK = false; このパラメタはサポートされません。
	public static final String YAHOO_API_ID = "FebtFUCxg64qHGjFqOWj7tyPfKHoQYAuXDrdSQmHIbkntzEd5ZSDE.smf3sIT0E-"; // TODO fix
	
	public static final String OUT_T_CHARSET = "UTF-8";
	public static final String IN_T_CHARSET = "UTF-8";

	public static final String KEY_TERM_ATTRIBUTE_KW_NOUN = "kwNoun";
	public static final String KEY_TERM_ATTRIBUTE_KW_OTHER = "kwOther";
	public static final String KEY_TERM_ATTRIBUTE_FUKUGO = "fukugo";
	public static final String KEY_TERM_ATTRIBUTE_FUKUGO2 = "fukugo2";
	public static final String KEY_TERM_ATTRIBUTE_KYOUKI = "kyouki";

	public static final String ERROR_MESSAGE_FILE_NOT_FOUND = "Specified file is not found.";
	public static final String ERROR_MESSAGE_FILE_FORMAT_INVALID = "Input file format is not valid.";
	public static final String ERROR_MESSAGE_CONNECTION_FAILED = "Connection failed.";

	public static final String ORIGINAL_QUESTION_VIEW_NAME = "OriginalQuestionView";
	public static final String QUESTION_ANALYSIS_VIEW_NAME = "QuestionAnalysisView";
	public static final String ANSWER_FEATURE_VIEW_NAME = "AnswerFeatureView";

	public static final String WEB_SEARCH_QUERY_VIEW_NAME_PREFIX = "WebSearchQueryView";
	public static final String WEB_SEARCH_RESULT_VIEW_NAME_PREFIX = "WebSearchResultView";

	public static final String KYOUKI_WEB_SEARCH_QUERY_VIEW_NAME_PREFIX = "KyoukiWebSearchQueryView";
	public static final String KYOUKI_WEB_SEARCH_RESULT_VIEW_NAME_PREFIX = "KyoukiWebSearchResultView";
	public static final String KYOUKI_SNIPPET_VIEW_NAME_PREFIX = "KyoukiSnippetView";

	public static final String IR4QA_WEB_SEARCH_QUERY_VIEW_NAME_PREFIX = "Ir4qaWebSearchQueryView";
	public static final String IR4QA_WEB_SEARCH_RESULT_VIEW_NAME_PREFIX = "Ir4qaWebSearchResultView";
	public static final String IR4QA_SNIPPET_VIEW_NAME_PREFIX = "Ir4qaSnippetView";

	public static final String PASSAGE_VIEW_NAME_PREFIX = "PassageView";
	public static final String SURFACE_FILE_NAME = "surface2.txt";
	
	public static final String LINE_SEPARATOR = "\n";
}