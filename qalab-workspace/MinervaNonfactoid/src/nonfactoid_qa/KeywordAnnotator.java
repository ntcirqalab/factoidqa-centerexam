package nonfactoid_qa;

import java.util.Iterator;
import java.util.LinkedHashSet;

import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.kachako.share.regex.RegexCache;
import org.kachako.types.qa.KeyTerm;
import org.u_compare.shared.japanese.syntactic.morpheme.Morpheme;

/**
 * Question Analysis Viewに格納されている質問文からキーワードを検出し、KeyTerm Annotationを作成します。
 * 
 * 事前条件：Question Analysis Viewに格納されている質問文に対して、形態素解析が完了している。
 * 事後条件：Question Analysis Viewに、抽出されたキーワードがKeyTerm素性構造として格納されている。
 * 
 * @author Takayuki SUZUKI
 *
 */
public class KeywordAnnotator extends JCasAnnotator_ImplBase {

	private final RegexCache regexCache = new RegexCache();

	@Override
	public void process(JCas baseJCas)
			throws AnalysisEngineProcessException {
		JCas questionAnalysisViewJCas;
		try {
			questionAnalysisViewJCas = baseJCas
					.getView(Constant.QUESTION_ANALYSIS_VIEW_NAME);
		} catch (CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}

		GetQInfo getQInfo = new GetQInfo(questionAnalysisViewJCas);

		addAllKeyTermFS(questionAnalysisViewJCas, getQInfo.kwNoun,
				Constant.KEY_TERM_ATTRIBUTE_KW_NOUN);
		addAllKeyTermFS(questionAnalysisViewJCas,
				getQInfo.kwOther,
				Constant.KEY_TERM_ATTRIBUTE_KW_OTHER);
		addAllKeyTermFS(questionAnalysisViewJCas, getQInfo.fukugo,
				Constant.KEY_TERM_ATTRIBUTE_FUKUGO);
		addAllKeyTermFS(questionAnalysisViewJCas,
				getQInfo.fukugo2,
				Constant.KEY_TERM_ATTRIBUTE_FUKUGO2);
		return;
	}

	private final class GetQInfo {
		private LinkedHashSet<String> kwNoun, kwOther, fukugo, fukugo2;

		GetQInfo(JCas questionAnalysisViewJCas) {
			Utilities.GetKeyword keywordSets = new Utilities.GetKeyword(questionAnalysisViewJCas.getAnnotationIndex(Morpheme.type).iterator());
			kwNoun = keywordSets.kwNoun;
			kwOther = keywordSets.kwOther;
			fukugo = kwChunk(questionAnalysisViewJCas, false);
			fukugo2 = kwChunk(questionAnalysisViewJCas, true);
		}
	}

	private LinkedHashSet<String> kwChunk(JCas questionAnalysisViewJCas,
			boolean getNo) {
		LinkedHashSet<String> fukugoSet = new LinkedHashSet<>();
		String fukugo = "";
		boolean noun = false, kakko = false, no = false;

		for (Iterator<Annotation> morphemeFSIterator = questionAnalysisViewJCas
				.getAnnotationIndex(Morpheme.type).iterator(); morphemeFSIterator.hasNext();) {
			Morpheme morphemeFS = (Morpheme) morphemeFSIterator.next();
			String detailedPosInMecabForm = Utilities.convertDetailedPosToMecabForm(morphemeFS
					.getDetailedPos());
			if (regexCache.find("記号,括弧開", detailedPosInMecabForm)) {
				if (!fukugo.isEmpty()) {
					if (no) {
						fukugo = fukugo.replaceFirst("の$", "");
						no = false;
					}
					if (!fukugoSet.contains(fukugo)) {
						fukugoSet.add(fukugo);
					}
					fukugo = "";
				}
				kakko = true;
			} else if (regexCache.find("記号,括弧閉", detailedPosInMecabForm)) {
				kakko = false;
			} else if (kakko) {
				fukugo = fukugo.concat(morphemeFS.getSurfaceForm());
			} else if (regexCache
					.find(
							"^(接頭詞,(数接続|名詞接続)|名詞,(サ変接続|形容動詞語幹|ナイ形容詞語幹|副詞可能|一般|数|固有名詞|接尾,(サ変接続|一般|形容動詞語幹|助数詞|人名|地域))|記号,アルファベット)",
							detailedPosInMecabForm)
					&& !regexCache.find("^(何|何者)$",
							morphemeFS.getConjugateForm())) {
				if (!noun) {
					if (!fukugo.isEmpty()) {
						if (no) {
							fukugo = fukugo.replaceFirst("の$", "");
							no = false;
						}
						if (!fukugoSet.contains(fukugo)) {
							fukugoSet.add(fukugo);
						}
						fukugo = "";
					}
					noun = true;
				}
				fukugo = fukugo.concat(morphemeFS.getSurfaceForm());
				no = false;
			} else if (getNo && noun
					&& regexCache.find("助詞,連体化", detailedPosInMecabForm)) {
				fukugo = fukugo.concat(morphemeFS.getSurfaceForm());
				no = true;
			} else {
				noun = false;
			}
		}
		if (!fukugo.isEmpty()) {
			if (no) {
				fukugo = fukugo.replaceFirst("の$", "");
			}
			if (!fukugoSet.contains(fukugo)) {
				fukugoSet.add(fukugo);
			}
		}

		return fukugoSet;
	}

	private void addAllKeyTermFS(JCas questionAnalysisViewJCas,
			LinkedHashSet<String> keywordSet, String keyTermAttributeName) {
		for (Iterator<Annotation> morphemeFSIterator = questionAnalysisViewJCas.getAnnotationIndex(Morpheme.type).iterator(); morphemeFSIterator.hasNext();) {
			Morpheme morphemeFS = (Morpheme) morphemeFSIterator.next();
			if (keywordSet.contains(morphemeFS.getSurfaceForm())) {
				KeyTerm keyTermFS = new KeyTerm(questionAnalysisViewJCas);
				keyTermFS.addToIndexes(questionAnalysisViewJCas);
				keyTermFS.setBegin(morphemeFS.getBegin());
				keyTermFS.setEnd(morphemeFS.getEnd());
				keyTermFS.setScore(0);
				keyTermFS.setAttribute(keyTermAttributeName);
			}
		}
		return;
	}
}
