package nonfactoid_qa;

import java.util.Iterator;
import java.util.Map;

import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.kachako.types.qa.ViewId;

import com.ibm.icu.text.Transliterator;

/**
 * このAnnotatorはWeb検索の結果を形態素解析器に掛けるための準備をします。
 * 各Kyouki Web Search Result ViewごとにKyouki Snippet Viewを作成し、result素性構造に格納されているSummaryをコピーして格納します。
 * @author Takayuki SUZUKI
 *
 */
public class KyoukiAnnotator2 extends JCasAnnotator_ImplBase {

	@Override
	public void process(JCas baseJCas) throws AnalysisEngineProcessException {
		try {
			for (Iterator<JCas> kyoukiWebSearchResultViewIterator = baseJCas.getViewIterator(Constant.KYOUKI_WEB_SEARCH_RESULT_VIEW_NAME_PREFIX); kyoukiWebSearchResultViewIterator.hasNext();) {
				JCas kyoukiWebSearchResultViewJCas = kyoukiWebSearchResultViewIterator.next();

				String kyoukiSnippetViewName = kyoukiWebSearchResultViewJCas.getViewName().replaceFirst(Constant.KYOUKI_WEB_SEARCH_RESULT_VIEW_NAME_PREFIX, Constant.KYOUKI_SNIPPET_VIEW_NAME_PREFIX);
				JCas kyoukiSnippetViewJCas = baseJCas.createView(kyoukiSnippetViewName);

				String content = Transliterator.getInstance("Halfwidth-Fullwidth").transliterate(kyoukiWebSearchResultViewJCas.getDocumentText());
				kyoukiSnippetViewJCas.setDocumentText(content);
				
				ViewId parentKyoukiWebSearchResultViewId = new ViewId(kyoukiWebSearchResultViewJCas);
				ViewId childKyoukiSnippetViewId = new ViewId(kyoukiSnippetViewJCas);
				FSArray children = new FSArray(kyoukiWebSearchResultViewJCas, 1);
				children.set(0, childKyoukiSnippetViewId);
				FSArray parents = new FSArray(kyoukiSnippetViewJCas, 1);
				parents.set(0, parentKyoukiWebSearchResultViewId);
				parentKyoukiWebSearchResultViewId.setChildren(children);
				parentKyoukiWebSearchResultViewId.addToIndexes();
				childKyoukiSnippetViewId.setParents(parents);
				childKyoukiSnippetViewId.addToIndexes();
			}
		} catch (CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}
	}

}
