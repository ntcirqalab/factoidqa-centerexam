package nonfactoid_qa.test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import nonfactoid_qa.Constant;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.collection.CollectionReader_ImplBase;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Progress;

public class WebSearchQueryCollectionReader extends CollectionReader_ImplBase {
	private BufferedReader in;
	private String QUERY_FILE_NAME = "../MinervaNonfactoid/src/nonfactoid_qa/test/query.txt";
	private int queryCount;
	
	@Override
	public void getNext(CAS baseCas) throws IOException, CollectionException {
		try {
			JCas baseJCas = baseCas.getJCas();
			while (in.ready()) {
				JCas webSearchQueryViewJCas = baseJCas.createView(String.format("%s.%d", Constant.WEB_SEARCH_QUERY_VIEW_NAME_PREFIX, queryCount));
				queryCount++;
				webSearchQueryViewJCas.setDocumentText(in.readLine());
			}
		} catch (CASException e) {
			e.printStackTrace();
			throw new CollectionException(e);
		}
		return;
	}

	@Override
	public void close() throws IOException {
		in.close();
		return;
	}

	@Override
	public Progress[] getProgress() {
		return null;
	}

	@Override
	public boolean hasNext() throws IOException, CollectionException {
		return in.ready();
	}

	@Override
	public void initialize() throws ResourceInitializationException {
		super.initialize();
		
		queryCount = 0;

		try {
			in = new BufferedReader(new FileReader(QUERY_FILE_NAME));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new ResourceInitializationException(e);
		}
	}
}
