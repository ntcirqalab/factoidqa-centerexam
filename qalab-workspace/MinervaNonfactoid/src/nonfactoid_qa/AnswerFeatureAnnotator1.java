package nonfactoid_qa;

import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.kachako.share.regex.RegexCache;

/**
 * 回答の特徴表現を抽出する準備をします。
 * @author Takayuki SUZUKI
 *
 */
public class AnswerFeatureAnnotator1 extends JCasAnnotator_ImplBase {

	private static final RegexCache regexCache = new RegexCache();

	@Override
	public void process(JCas baseJCas) throws AnalysisEngineProcessException {
		JCas originalQuestionViewJCas;
		try {
			originalQuestionViewJCas = baseJCas.getView(Constant.ORIGINAL_QUESTION_VIEW_NAME);
		} catch (CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}
		
		String question = extractQuestion(originalQuestionViewJCas.getDocumentText());

		JCas answerFeatureViewJCas;
		try {
			answerFeatureViewJCas = baseJCas.createView(Constant.ANSWER_FEATURE_VIEW_NAME);
		} catch (CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}
		answerFeatureViewJCas.setDocumentText(question);
		
		return;
	}

	private String extractQuestion(String originalQuestion) {
		String question = originalQuestion;
		// 入力から質問文を取り出す
		if (regexCache.find("^(\\S+)\\s*:\\s*\"?(.*?)\"?\\s*$", originalQuestion)) {
			question = regexCache.lastMatched(2);
		}

		question = question.replaceAll("を(教|おし)えて.*$", "は何ですか");
		// 文末を統一
		question = question.replaceAll("[？。]$", "");
		question = question.concat("。");

		return question;
	}
}
