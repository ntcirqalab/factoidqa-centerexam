package nonfactoid_qa;

import java.util.ArrayList;
import java.util.LinkedHashSet;

import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.kachako.types.qa.Query;

/**
 * このAnnotatorは、共起語を取得するための準備をします。
 * 各検索クエリごとにKyouki Web Search Query Viewを作成し、クエリを格納します。
 * @author Takayuki SUZUKI
 * 
 * 事前条件：Question Analysis Viewに格納されている質問文に対してキーワード抽出が完了している。
 * 事後条件：検索クエリごとにKyouki Web Search Query Viewが作成されている。
 */
public class KyoukiAnnotator1 extends JCasAnnotator_ImplBase {

	@Override
	public void process(JCas baseJCas) throws AnalysisEngineProcessException {
		try {
			JCas questionAnalysisViewJCas = baseJCas
					.getView(Constant.QUESTION_ANALYSIS_VIEW_NAME);
			LinkedHashSet<String> kwOther = Utilities.getKeywordSet(
					questionAnalysisViewJCas,
					Constant.KEY_TERM_ATTRIBUTE_KW_OTHER);
			LinkedHashSet<String> fukugo = Utilities.getKeywordSet(
					questionAnalysisViewJCas,
					Constant.KEY_TERM_ATTRIBUTE_FUKUGO);

			ArrayList<String> keyword = createKeyword(fukugo, kwOther);

			createKyoukiWebSearchQueryViews(keyword, baseJCas);

		} catch (CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}
		return;
	}

	private void createKyoukiWebSearchQueryViews(ArrayList<String> keyword, JCas baseJCas) throws CASException {
		int count = 0;
		for (int i = 0; i < keyword.size(); i++) {
			for (int j = i + 1; j < keyword.size(); j++) {
				for (int k = j + 1; k < keyword.size(); k++) {
					String word = String.format("%s %s %s", keyword.get(i),
							keyword.get(j), keyword.get(k));
						JCas queryViewJCas = baseJCas.createView(String.format("%s.%d", Constant.KYOUKI_WEB_SEARCH_QUERY_VIEW_NAME_PREFIX, count));
						queryViewJCas.setDocumentText(word);

						count++;
				}
			}
		}
		return;
	}
	
	private ArrayList<String> createKeyword(LinkedHashSet<String> fukugo, LinkedHashSet<String> kwOther){
		ArrayList<String> kwTemp1 = new ArrayList<>();
		kwTemp1.addAll(fukugo);
		kwTemp1.addAll(kwOther);

		ArrayList<String> keyword = kwTemp1;
		while (keyword.size() <= 2) {
			keyword.add(" ");
		}
		
		return keyword;
	}
}
