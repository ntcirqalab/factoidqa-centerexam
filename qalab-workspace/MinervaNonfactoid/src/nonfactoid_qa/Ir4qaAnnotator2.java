package nonfactoid_qa;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.TOP;
import org.kachako.share.regex.RegexCache;
import org.kachako.types.qa.Result;

import com.ibm.icu.text.Transliterator;

/**
 * パッセージの元になる文章を取得します。
 * 取得した文書は、それぞれIr4qa Snippet Viewに格納されます。
 * @author Takayuki SUZUKI
 *
 */
public class Ir4qaAnnotator2 extends JCasAnnotator_ImplBase {

	RegexCache regexCache = new RegexCache();

	private class SnippetInfo {
		List<String> url = new ArrayList<>();
		List<String> title = new ArrayList<>();
	}

	@Override
	public void process(JCas baseJCas) throws AnalysisEngineProcessException {
		LinkedHashMap<String, String> urlToHtml = new LinkedHashMap<>();
		LinkedHashMap<String, SnippetInfo> snippetInfo = new LinkedHashMap<>(); // 検索結果のSummaryをキーとしてスニペットの情報を引くマップ

		// 検索結果をsnippetInfoに格納
		try {
			for (Iterator<JCas> snippetViewJCasIterator = baseJCas.getViewIterator(Constant.IR4QA_WEB_SEARCH_RESULT_VIEW_NAME_PREFIX); snippetViewJCasIterator.hasNext();) {
				JCas snippetViewJCas = snippetViewJCasIterator.next();
				
				for (Iterator<TOP> resultFSIterator = snippetViewJCas.getJFSIndexRepository().getAllIndexedFS(Result.type);
						resultFSIterator.hasNext() && snippetInfo.size() <= Constant.SNUM;) {
					Result resultFS = (Result) resultFSIterator.next();
					// キーとなるSummaryを取得
					String text = snippetViewJCas.getDocumentText();
	
					SnippetInfo resultSnippetInfo = snippetInfo.get(text); 
					if (resultSnippetInfo == null) {
						resultSnippetInfo = new SnippetInfo();
					}
					
					resultSnippetInfo.title.add(resultFS.getTitle());
					resultSnippetInfo.url.add(resultFS.getUrl());
					
					snippetInfo.put(text, resultSnippetInfo);
				}
			}
		} catch (CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}

		// 文書取得用のハッシュ
		for (Map.Entry<String, SnippetInfo> entry : snippetInfo.entrySet()) {
			// 同じ表層の snippet に対応する URL は複数あるので，とりあえず1つ目
			String url = entry.getValue().url.get(0);

			// とりあえず URL だけ集める
			urlToHtml.put(url, ""); 
		}

		// 検索でヒットしたページの本文を取得
		try {
			Utilities.myGet(urlToHtml);
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}
		
		// それぞれページの本文とURLをIr4qa Snippet Viewに格納
		try {
			Utilities.createViews(
					baseJCas,
					Constant.IR4QA_SNIPPET_VIEW_NAME_PREFIX,
					urlToHtml.entrySet(),
					new Utilities.CreateViewsFunction<Map.Entry<String, String>>() {
						@Override
						public void apply(JCas view, Map.Entry<String, String> entry, int index) {
							String text = new HtmlToText(entry.getValue()).temp;
							if (text.length() > 2500) {
								text = text.substring(0, 2500);
							}
							view.setDocumentText(text);
							Result resultFS = new Result(view);
							resultFS.addToIndexes();
							resultFS.setUrl(entry.getKey());
							return;
						};
					},
					0);
		} catch (CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}
		return;
	}

	private final class HtmlToText {
		private String temp;
		private String title;

		HtmlToText(String html) {
			List<String> htmlLine = Arrays.asList(html.split("\n"));

			String brtag = "(blockquote|form|noframes|noscript|div|center|fieldset|address|h[1-6]|p|pre|dir|dl|menu|ol|table|ul|li|dl|dt|dd|caption|col|colgroup|thead|tr|th|td|tfoot|tbody|hr|isindex|applet|button|del|iframe|ins|map|object|script)";

			boolean comment = false;
			boolean body = false;
			StringBuilder text = new StringBuilder();

			for (String line : htmlLine) {
				// タイトルの取得
				if (regexCache.find("<title>(.*?)<\\/title>", line)) {
					title = regexCache.lastMatched(1);
				}

				// <BODY>内のみを残す
				if (regexCache.find("<body.*>", line)) {
					body = true;
					continue;
				} else if (regexCache.find("<\\/body>", line)) {
					body = false;
					comment = false;	// コメントの終了タグが無かった場合など
					break;
				}
				if (!body) {
					continue;
				}

				// ヤフーキャッシュのエラー
				if (regexCache.find("<!--エラー-->", line)) {
					body = false;
				}

				// まず，1行のコメントを除いて
				line = line.replaceAll("<!--.*?-->", "");

				// 次に複数行に渡るコメントを除く
				// 基本的にスクリプトもこれに含まれる
				if(regexCache.find("<!--", line)) {
					comment = true;
				} else if(comment && regexCache.find("-->", line)) {
					comment = false;
					continue;
				}
				if (comment) {
					continue;
				}

				// 行内のスクリプト
				line = line.replaceAll("<script.*?>.*?<\\/script>", "\n\n");

				// 「^@」ではない．(まる1)などの表示できない機種依存文字？
				// これを除かないと以降も文字化けするので注意．
				line = line.replaceAll(" |\\©", "");
				line = line.replaceAll("\r", "");

				// ブロック要素の前後には改行が入る
				line = line.replaceAll(String.format("<\\/?%s.*?>", brtag), "\n\n");
				// その後に<br>を改行に置き換える
				line = line.replaceAll("<br>", "\n");
				// 最後に残りのタグを除く
				line = line.replaceAll("<.*?>", "");

				// 複数行にまたがるタグに未対応

				line = line.replaceAll("　", " "); // 全角スペース->半角スペース
				line = line.replaceAll("[\t ]+", " "); // スペース(タブ)の連続
				line = line.replaceAll("^\\s*\\n", ""); // 空白だけの行

				line = Transliterator.getInstance("Halfwidth-Fullwidth").transliterate(line); // 全角に統一
				line = line.replaceAll("[─━‥−…-]", "—"); // K-1対策

				// 小数点を置換．アルファベット列の場合も．
				line = line.replaceAll("([０１-９ａ-ｚＡ-Ｚ])．(?=[０１-９ａ-ｚＡ-Ｚ])", "$1・");
				// ピリオドを句点に置換
				line = line.replaceAll("．", "。");
				line = line.replaceAll("[。！？]+", "。");
				line = line.replaceAll("。(?!(）|」|\n))n", "$1\n");
				line = line.replaceAll("([、，])\n", "$1");

				text.append(line);
			}

			List<String> textLine = Arrays.asList(text.toString().split("\n"));

			temp = Utilities.join(divide(finalcut(blank(jap(textLine)))), "\n");
			return;
		}

		// 無駄な空行を除く．
		private List<String> divide(List<String> textLine) {
			StringBuilder text = new StringBuilder();
			int l = Constant.L;

			for (String line : textLine) {
				line = chomp(line);

				StringBuilder remainingLine = new StringBuilder(line);
				for (String dividedLine = null; remainingLine.length() > 0; ) {
					dividedLine = remainingLine.substring(0, Math.min(l, remainingLine.length()));
					remainingLine.delete(0, l);
					text.append(dividedLine).append('\n');
				}
			}

			return new ArrayList<>(Arrays.asList(text.toString().split("\n")));
		}

		// それでも残ってしまうタグを除くフィルター
		// その他除くもの
		// ブログなどのレス ＞＞
		// 項目の羅列 ｜X｜X｜X｜ ■Y■Y■Y
		private List<String> finalcut(List<String> textLine) {
			StringBuilder text = new StringBuilder();

			String tag = "Ａ|ＡＢＢＲ|ＡＣＲＯＮＹＭ|ＡＰＰＬＥＴ|Ｂ|ＢＡＳＥＦＯＮＴ|ＢＤＯ|ＢＩＧ|ＢＲ|ＢＵＴＴＯＮ|ＣＩＴＥ|ＣＯＤＥ|ＤＥＬ|ＤＦＮ|ＥＭ|ＦＯＮＴ|Ｉ|ＩＦＲＡＭＥ|ＩＭＧ|ＩＮＰＵＴ|ＩＮＳ|ＫＢＤ|ＬＡＢＥＬ|ＭＡＰ|ＯＢＪＥＣＴ|Ｑ|ＲＢ|ＲＢＣ|ＲＰ|ＲＴ|ＲＴＣ|ＲＵＢＹ|Ｓ|ＳＡＭＰ|ＳＥＬＥＣＴ|ＳＭＡＬＬ|ＳＰＡＮ|ＳＴＲＩＫＥ|ＳＴＲＯＮＧ|ＳＵＢ|ＳＵＰ|ＴＥＸＴＡＲＥＡ|ＴＴ|Ｕ|ＶＡＲ|ＷＢＲ|ａ|ａｂｂｒ|ａｃｒｏｎｙｍ|ａｐｐｌｅｔ|ｂ|ｂａｓｅｆｏｎｔ|ｂｄｏ|ｂｉｇ|ｂｒ|ｂｕｔｔｏｎ|ｃｉｔｅ|ｃｏｄｅ|ｄｅｌ|ｄｆｎ|ｅｍ|ｆｏｎｔ|ｉ|ｉｆｒａｍｅ|ｉｍｇ|ｉｎｐｕｔ|ｉｎｓ|ｋｂｄ|ｌａｂｅｌ|ｍａｐ|ｏｂｊｅｃｔ|ｑ|ｒｂ|ｒｂｃ|ｒｐ|ｒｔ|ｒｔｃ|ｒｕｂｙ|ｓ|ｓａｍｐ|ｓｅｌｅｃｔ|ｓｍａｌｌ|ｓｐａｎ|ｓｔｒｉｋｅ|ｓｔｒｏｎｇ|ｓｕｂ|ｓｕｐ|ｔｅｘｔａｒｅａ|ｔｔ|ｕ|ｖａｒ|ｗｂｒ";

			for (String line : textLine) {
				line = chomp(line);

				if (regexCache.find("(■[^■]+){3,}", line) || regexCache.find("(｜[^｜]+){3,}", line) || regexCache.find("^＞＞", line)) {
					continue;
				}

				line = line.replaceAll(String.format("＜／?(%s)( .*?)?＞", tag), "");
				text.append(String.format("%s\n", line));
			}

			return Arrays.asList(text.toString().split("\n"));
		}

		// 無駄な空行を除く．
		private List<String> blank(List<String> textLine) {
			StringBuilder text = new StringBuilder();
			boolean prevBlank = false;

			for (String line : textLine) {
				line = chomp(line);

				if (regexCache.find("^\\s*$", line) && !prevBlank) {
					text.append("\n");
					prevBlank = true;
				} else {
					prevBlank = false;
					text.append(String.format("%s\n", line));
				}
			}

			return Arrays.asList(text.toString().split("\n"));
		}

		// 文内の文字種の比率をみて，非日本語文を除く．
		// 短すぎる文も除く
		// これで残ったタグやスクリプト，URLなども除けるはず．
		private List<String> jap(List<String> textLine) {
			List<String> textTemp = new ArrayList<>();

			String nonJap = "０１２３４５６７８９"
				+ "ａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ"
				+ "ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ"
				+ "！”＃＄％＆’＊＋，．／：；＜＝＞？＠「￥」＾＿‘｛｜｝〜−";
			// （）は説明文でも使われるので除いた．

			for (String line : textLine) {
				line = chomp(line);
				if (regexCache.find("\\S", line)) {
					int lenAll = line.length();
					if (lenAll < 10) {
						continue;
					}

					int lenNj = 0;
					for (char c: line.toCharArray()) {
						if (nonJap.contains(String.valueOf(c))) {
							lenNj++;
						}
					}
					double rate = (double) lenNj / (double) lenAll;
					if (rate > 0.4) {
						continue;
					}
				}
				textTemp.add(String.format("%s\n", line));
			}
			return textTemp;
		}
		
		private String chomp(String s) {
			if (s.endsWith("\n")) {
				return s.substring(0, s.length() - 1);
			}
			else {
				return s;
			}
		}
	}

}
