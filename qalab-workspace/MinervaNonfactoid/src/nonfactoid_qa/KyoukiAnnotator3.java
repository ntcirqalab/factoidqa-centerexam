package nonfactoid_qa;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.TOP;
import org.apache.uima.jcas.tcas.Annotation;
import org.kachako.share.regex.RegexCache;
import org.kachako.types.qa.KeyTerm;
import org.kachako.types.qa.Query;
import org.kachako.types.qa.ViewId;
import org.u_compare.shared.japanese.syntactic.morpheme.Morpheme;

/**
 * このAnnotatorは、共起語を抽出しKeytermに加えます。
 * 抽出された共起語は、各Kyouki Snippet ViewにKeyTerm FSとして格納されます。
 * @author Takayuki SUZUKI
 *
 */
public class KyoukiAnnotator3 extends JCasAnnotator_ImplBase {

	private static final String hinsi = "名詞,(サ変接続|形容動詞語幹|ナイ形容詞語幹|一般|数|固有名詞)|動詞,自立|形容詞,自立";

	private final RegexCache regexCache = new RegexCache();

	@Override
	public void process(JCas baseJCas) throws AnalysisEngineProcessException {

		try {
			JCas questionAnalysisViewJCas = baseJCas.getView(Constant.QUESTION_ANALYSIS_VIEW_NAME);

			LinkedHashSet<KeyTerm> kwNoun = getKeyTermSet(questionAnalysisViewJCas, Constant.KEY_TERM_ATTRIBUTE_KW_NOUN);
			LinkedHashSet<KeyTerm> kwOther = getKeyTermSet(questionAnalysisViewJCas, Constant.KEY_TERM_ATTRIBUTE_KW_OTHER);
			LinkedHashSet<KeyTerm> keyTermFSSet = new LinkedHashSet<>();
			keyTermFSSet.addAll(kwNoun); keyTermFSSet.addAll(kwOther);

			SetUpKyouki setUpKyouki = new SetUpKyouki(baseJCas, keyTermFSSet);

			for (Map.Entry<String, Double> entry : setUpKyouki.kyouki.entrySet()){
				String kyoukiWord = entry.getKey();
				double score = entry.getValue();
				KeyTerm keyTermFS = setUpKyouki.keyTermFSMap.get(kyoukiWord);
				keyTermFS.setAttribute(Constant.KEY_TERM_ATTRIBUTE_KYOUKI);
				keyTermFS.setScore(score);
				keyTermFS.addToIndexes();
			}

			for (KeyTerm keyTermFS : keyTermFSSet) {
				keyTermFS.setScore(setUpKyouki.kyoukiMax);
			}
			
		} catch (CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}

		return;
	}

	private final class SetUpKyouki {
		private double kyoukiMax;
		private Map<String, Double> kyouki;
		private Map<String, KeyTerm> keyTermFSMap;

		SetUpKyouki(JCas baseJCas, LinkedHashSet<KeyTerm> keyTermFSSet) throws CASException {
			keyTermFSMap = new HashMap<>();

			Map<String, Double> relatedTemp = new HashMap<>();

			for (Iterator<JCas> webSearchQueryViewJCasIterator = baseJCas.getViewIterator(Constant.KYOUKI_WEB_SEARCH_QUERY_VIEW_NAME_PREFIX); webSearchQueryViewJCasIterator.hasNext(); ) {
				JCas webSearchQueryViewJCas = webSearchQueryViewJCasIterator.next();

				Map<String, Integer> freq = new HashMap<>();

				StringBuilder stpwrds = new StringBuilder("する|ある|いる|おる|なる|やる|いう|もつ|おく|とる|つく|よる|あう|でる|ため|よう|かつ|の|ところ|こと|できる|ない|思う|日|月|人|私|年|同|等|他|何|ヶ|・+|[あ-んが-ぽ]|[０-９]|一|事|為|(ニュース)?サイト|ページ|ホームページ|日記|ブログ|リンク|検索|アーカイブ|ａｍｐ|ｇｔ|ｌｔ|ｑｕｏｔ");
				for (KeyTerm keyTermFS : keyTermFSSet) {
					stpwrds.append("|").append(keyTermFS.getCoveredText());
				}
				String stpwrdsRegex = stpwrds.insert(0, "^(").append(")").toString();

				Iterator<TOP> viewIdFSIterator = webSearchQueryViewJCas.getJFSIndexRepository().getAllIndexedFS(ViewId.type);
				ViewId viewIdFS = (ViewId) viewIdFSIterator.next();

				for (int i = 0; i < viewIdFS.getChildren().size(); i++) {
					JCas snippetViewJCas = null;
					ViewId childKyoukiWebSearchResultViewId = (ViewId) viewIdFS.getChildren(i);
					for (Iterator<TOP> kyoukiWebSearchResultViewIdIterator = childKyoukiWebSearchResultViewId.getCAS().getJCas().getJFSIndexRepository().getAllIndexedFS(ViewId.type);
							kyoukiWebSearchResultViewIdIterator.hasNext();) {
						ViewId kyoukiWebSearchResultViewId = (ViewId) kyoukiWebSearchResultViewIdIterator.next();
						if (kyoukiWebSearchResultViewId != childKyoukiWebSearchResultViewId) {
							snippetViewJCas = kyoukiWebSearchResultViewId.getChildren(0).getCAS().getJCas();
						}
					}

					Set<String> kishutsu = new HashSet<>();

					for (Iterator<Annotation> result = snippetViewJCas.getAnnotationIndex(Morpheme.type).iterator(); result.hasNext();) {
						Morpheme morphemeFS = (Morpheme) result.next();

						String detailedPosInMecabForm = Utilities.convertDetailedPosToMecabForm(morphemeFS.getDetailedPos());
						if (regexCache.find(hinsi, detailedPosInMecabForm)) {
							String surfaceForm = morphemeFS.getSurfaceForm();
							if (kishutsu.contains(surfaceForm) || regexCache.find(stpwrdsRegex, surfaceForm) || regexCache.find(stpwrdsRegex, morphemeFS.getBaseForm())) {
								continue;
							}

							kishutsu.add(surfaceForm);
							freq.put(surfaceForm, (freq.containsKey(surfaceForm) ? freq.get(surfaceForm) : 0) + 1);
							KeyTerm keyTermFS = new KeyTerm(snippetViewJCas, morphemeFS.getBegin(), morphemeFS.getEnd()); keyTermFS.setLanguage("JA");
							keyTermFSMap.put(surfaceForm, keyTermFS);
						}
					}
				}

				/*if (viewIdFS.getChildren().size() >= 10 ) {*/
					TreeSet<Map.Entry<String, Integer>> sortedFreqEntrySet = new TreeSet<>(new Comparator<Map.Entry<String, Integer>>() {
						@Override
						public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
							return Integer.compare(o2.getValue(), o1.getValue());
						}
					});
					sortedFreqEntrySet.addAll(freq.entrySet());
					for (Map.Entry<String, Integer> freqEntry : sortedFreqEntrySet) {
						double f = (double) freqEntry.getValue() / (double) viewIdFS.getChildren().size();
						if (!relatedTemp.containsKey(freqEntry.getKey()) || relatedTemp.get(freqEntry.getKey()) < f) {
							relatedTemp.put(freqEntry.getKey(), f);
						}
					}
				/*}*/
			}

			double max = Collections.max(relatedTemp.values());
			Map<String, Double> related = new HashMap<>(relatedTemp);
			for (Double relatedValue : related.values()) {
				if (relatedValue < max / 10) {
					related.values().remove(relatedValue);
				}
			}

			kyouki = related;
			kyoukiMax = max;
		}
	}

	private LinkedHashSet<KeyTerm> getKeyTermSet(JCas view, String keyTermAttribute) {
		LinkedHashSet<KeyTerm> keyTermSet = new LinkedHashSet<>();

		for (Iterator<Annotation> keyTermFSIterator = view.getAnnotationIndex(KeyTerm.type).iterator(); keyTermFSIterator.hasNext();) {
			KeyTerm keyTermFS = (KeyTerm) keyTermFSIterator.next();

			if (keyTermFS.getAttribute().equals(keyTermAttribute)) {
				keyTermSet.add(keyTermFS);
			}
		}

		return keyTermSet;
	}
}
