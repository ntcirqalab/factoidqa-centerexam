package nonfactoid_qa;

import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.kachako.share.regex.RegexCache;
import org.kachako.types.qa.InputMultiSofaID;

import com.ibm.icu.text.Transliterator;

/**
 * 質問の前処理を行います。
 * 
 * 事前条件：無し。
 * 事後条件：Question Analysis Viewに前処理済みの質問文が格納される。
 * 
 * @author Takayuki SUZUKI
 *
 */
public class PreprocessAnnotator extends JCasAnnotator_ImplBase {

	private final RegexCache regexCache = new RegexCache();
	
	@Override
	public void process(JCas baseJCas) throws AnalysisEngineProcessException {
		JCas originalQuestionViewJCas;
		try {
			originalQuestionViewJCas = baseJCas.getView(Constant.ORIGINAL_QUESTION_VIEW_NAME);
		} catch (CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}

		// 入力から質問文を取り出す
		String qLine = originalQuestionViewJCas.getDocumentText();
		if(regexCache.find("\\d+\\s(.+)", qLine)) {
			qLine = regexCache.lastMatched();
		}
		qLine = qLine.replaceAll("[？?\\s]", "");
		// クエリが半角英数字を含む場合は全角文字に変換する
		qLine = Transliterator.getInstance("Halfwidth-Fullwidth").transliterate(qLine);
		qLine = qLine.replaceAll("[─━‥−…-]", "—"); // K-1対策

		JCas questionAnalysisViewJCas;
		try {
			questionAnalysisViewJCas = baseJCas.createView(Constant.QUESTION_ANALYSIS_VIEW_NAME);
		} catch (CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}
		questionAnalysisViewJCas.setDocumentText(qLine);

		return;
	}
}
