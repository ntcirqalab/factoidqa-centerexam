package nonfactoid_qa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;

/**
 * パッセージの元になる文章を取得するための前準備を行います。
 * Ir4qa Web Search Query Viewが作成され、文章の検索クエリが格納されます。
 * @author Takayuki SUZUKI
 *
 */
public class Ir4qaAnnotator1 extends JCasAnnotator_ImplBase {

	@Override
	public void process(JCas baseJCas) throws AnalysisEngineProcessException {
		//キーワード取得
		LinkedHashSet<String> kwN = new LinkedHashSet<>();
		LinkedHashSet<String> kwO = new LinkedHashSet<>();
		LinkedHashSet<String> fukugo = new LinkedHashSet<>();
		LinkedHashSet<String> fukugo2 = new LinkedHashSet<>();
		try {
			for (Iterator<JCas> viewIterator = baseJCas.getViewIterator(); viewIterator.hasNext(); ) {
				JCas view = viewIterator.next();
				kwN.addAll(Utilities.getKeywordSet(view, Constant.KEY_TERM_ATTRIBUTE_KW_NOUN));
				kwO.addAll(Utilities.getKeywordSet(view, Constant.KEY_TERM_ATTRIBUTE_KW_OTHER));
				fukugo.addAll(Utilities.getKeywordSet(view, Constant.KEY_TERM_ATTRIBUTE_FUKUGO));
				fukugo2.addAll(Utilities.getKeywordSet(view, Constant.KEY_TERM_ATTRIBUTE_FUKUGO2));
			}
		} catch(CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}

		// クエリ生成
		LinkedHashSet<String> searchQuery = queryFormulation(kwN, kwO, fukugo, fukugo2);
		
		// クエリに対応するviewを作成
		try {
			Utilities.createViews(
					baseJCas,
					Constant.IR4QA_WEB_SEARCH_QUERY_VIEW_NAME_PREFIX,
					searchQuery,
					new Utilities.CreateViewsFunction<String>() {
						@Override
						public void apply(JCas view, String content, int index) {
							view.setDocumentText(content);
						}
					},
					0);
		} catch (CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}
		
		return;
	}

	private LinkedHashSet<String> queryFormulation(LinkedHashSet<String> kwN,
			LinkedHashSet<String> kwO, LinkedHashSet<String> fukugo,
			LinkedHashSet<String> fukugo2) {
		LinkedHashSet<String> searchQuery = new LinkedHashSet<>();

		// 引用符などは後でつける
		for (LinkedHashSet<String> k : Arrays.asList(kwN, fukugo, fukugo2)) {
			// 用言なし
			List<String> kw1 = new ArrayList<>(k);
			Collections.sort(kw1);
			// 用言あり
			List<String> kw2 = new ArrayList<>();
			kw2.addAll(kw1); kw2.addAll(kwO);
			Collections.sort(kw2);

			String sq1 = Utilities.join(kw1, " ");
			searchQuery.add(sq1);
			String sq2 = Utilities.join(kw2, " ");
			searchQuery.add(sq2);
		}
		return searchQuery;		
	}

}
