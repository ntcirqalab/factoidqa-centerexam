

/* First created by JCasGen Mon Jun 25 14:26:38 JST 2012 */
package org.kachako.types.qa;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.TOP;


/** 
 * Updated by JCasGen Fri Oct 26 14:29:37 JST 2012
 * XML source: /home/kano/workspace/MinervaNonfactoid/src/AcliaTypeSystemDescriptor.xml
 * @generated */
public class AnswerType extends TOP {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(AnswerType.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected AnswerType() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public AnswerType(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public AnswerType(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: answerType

  /** getter for answerType - gets 
   * @generated */
  public String getAnswerType() {
    if (AnswerType_Type.featOkTst && ((AnswerType_Type)jcasType).casFeat_answerType == null)
      jcasType.jcas.throwFeatMissing("answerType", "org.kachako.types.qa.AnswerType");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnswerType_Type)jcasType).casFeatCode_answerType);}
    
  /** setter for answerType - sets  
   * @generated */
  public void setAnswerType(String v) {
    if (AnswerType_Type.featOkTst && ((AnswerType_Type)jcasType).casFeat_answerType == null)
      jcasType.jcas.throwFeatMissing("answerType", "org.kachako.types.qa.AnswerType");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnswerType_Type)jcasType).casFeatCode_answerType, v);}    
   
    
  //*--------------*
  //* Feature: score

  /** getter for score - gets 
   * @generated */
  public double getScore() {
    if (AnswerType_Type.featOkTst && ((AnswerType_Type)jcasType).casFeat_score == null)
      jcasType.jcas.throwFeatMissing("score", "org.kachako.types.qa.AnswerType");
    return jcasType.ll_cas.ll_getDoubleValue(addr, ((AnswerType_Type)jcasType).casFeatCode_score);}
    
  /** setter for score - sets  
   * @generated */
  public void setScore(double v) {
    if (AnswerType_Type.featOkTst && ((AnswerType_Type)jcasType).casFeat_score == null)
      jcasType.jcas.throwFeatMissing("score", "org.kachako.types.qa.AnswerType");
    jcasType.ll_cas.ll_setDoubleValue(addr, ((AnswerType_Type)jcasType).casFeatCode_score, v);}    
  }

    