

/* First created by JCasGen Mon Jun 25 14:26:38 JST 2012 */
package org.kachako.types.qa;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.AnnotationBase;
import org.apache.uima.jcas.cas.TOP;


/** 
 * Updated by JCasGen Fri Oct 26 14:29:38 JST 2012
 * XML source: /home/kano/workspace/MinervaNonfactoid/src/AcliaTypeSystemDescriptor.xml
 * @generated */
public class ViewId extends AnnotationBase {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(ViewId.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected ViewId() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public ViewId(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public ViewId(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: parents

  /** getter for parents - gets 
   * @generated */
  public FSArray getParents() {
    if (ViewId_Type.featOkTst && ((ViewId_Type)jcasType).casFeat_parents == null)
      jcasType.jcas.throwFeatMissing("parents", "org.kachako.types.qa.ViewId");
    return (FSArray)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ViewId_Type)jcasType).casFeatCode_parents)));}
    
  /** setter for parents - sets  
   * @generated */
  public void setParents(FSArray v) {
    if (ViewId_Type.featOkTst && ((ViewId_Type)jcasType).casFeat_parents == null)
      jcasType.jcas.throwFeatMissing("parents", "org.kachako.types.qa.ViewId");
    jcasType.ll_cas.ll_setRefValue(addr, ((ViewId_Type)jcasType).casFeatCode_parents, jcasType.ll_cas.ll_getFSRef(v));}    
    
  /** indexed getter for parents - gets an indexed value - 
   * @generated */
  public TOP getParents(int i) {
    if (ViewId_Type.featOkTst && ((ViewId_Type)jcasType).casFeat_parents == null)
      jcasType.jcas.throwFeatMissing("parents", "org.kachako.types.qa.ViewId");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((ViewId_Type)jcasType).casFeatCode_parents), i);
    return (TOP)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((ViewId_Type)jcasType).casFeatCode_parents), i)));}

  /** indexed setter for parents - sets an indexed value - 
   * @generated */
  public void setParents(int i, TOP v) { 
    if (ViewId_Type.featOkTst && ((ViewId_Type)jcasType).casFeat_parents == null)
      jcasType.jcas.throwFeatMissing("parents", "org.kachako.types.qa.ViewId");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((ViewId_Type)jcasType).casFeatCode_parents), i);
    jcasType.ll_cas.ll_setRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((ViewId_Type)jcasType).casFeatCode_parents), i, jcasType.ll_cas.ll_getFSRef(v));}
   
    
  //*--------------*
  //* Feature: children

  /** getter for children - gets 
   * @generated */
  public FSArray getChildren() {
    if (ViewId_Type.featOkTst && ((ViewId_Type)jcasType).casFeat_children == null)
      jcasType.jcas.throwFeatMissing("children", "org.kachako.types.qa.ViewId");
    return (FSArray)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ViewId_Type)jcasType).casFeatCode_children)));}
    
  /** setter for children - sets  
   * @generated */
  public void setChildren(FSArray v) {
    if (ViewId_Type.featOkTst && ((ViewId_Type)jcasType).casFeat_children == null)
      jcasType.jcas.throwFeatMissing("children", "org.kachako.types.qa.ViewId");
    jcasType.ll_cas.ll_setRefValue(addr, ((ViewId_Type)jcasType).casFeatCode_children, jcasType.ll_cas.ll_getFSRef(v));}    
    
  /** indexed getter for children - gets an indexed value - 
   * @generated */
  public TOP getChildren(int i) {
    if (ViewId_Type.featOkTst && ((ViewId_Type)jcasType).casFeat_children == null)
      jcasType.jcas.throwFeatMissing("children", "org.kachako.types.qa.ViewId");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((ViewId_Type)jcasType).casFeatCode_children), i);
    return (TOP)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((ViewId_Type)jcasType).casFeatCode_children), i)));}

  /** indexed setter for children - sets an indexed value - 
   * @generated */
  public void setChildren(int i, TOP v) { 
    if (ViewId_Type.featOkTst && ((ViewId_Type)jcasType).casFeat_children == null)
      jcasType.jcas.throwFeatMissing("children", "org.kachako.types.qa.ViewId");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((ViewId_Type)jcasType).casFeatCode_children), i);
    jcasType.ll_cas.ll_setRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((ViewId_Type)jcasType).casFeatCode_children), i, jcasType.ll_cas.ll_getFSRef(v));}
  }

    