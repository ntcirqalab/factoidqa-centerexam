
/* First created by JCasGen Mon Jun 25 14:26:38 JST 2012 */
package org.kachako.types.qa;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Fri Oct 26 14:29:38 JST 2012
 * @generated */
public class InputMultiSofaID_Type extends Annotation_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (InputMultiSofaID_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = InputMultiSofaID_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new InputMultiSofaID(addr, InputMultiSofaID_Type.this);
  			   InputMultiSofaID_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new InputMultiSofaID(addr, InputMultiSofaID_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = InputMultiSofaID.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.qa.InputMultiSofaID");
 
  /** @generated */
  final Feature casFeat_sofaID;
  /** @generated */
  final int     casFeatCode_sofaID;
  /** @generated */ 
  public String getSofaID(int addr) {
        if (featOkTst && casFeat_sofaID == null)
      jcas.throwFeatMissing("sofaID", "org.kachako.types.qa.InputMultiSofaID");
    return ll_cas.ll_getStringValue(addr, casFeatCode_sofaID);
  }
  /** @generated */    
  public void setSofaID(int addr, String v) {
        if (featOkTst && casFeat_sofaID == null)
      jcas.throwFeatMissing("sofaID", "org.kachako.types.qa.InputMultiSofaID");
    ll_cas.ll_setStringValue(addr, casFeatCode_sofaID, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public InputMultiSofaID_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_sofaID = jcas.getRequiredFeatureDE(casType, "sofaID", "uima.cas.String", featOkTst);
    casFeatCode_sofaID  = (null == casFeat_sofaID) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_sofaID).getCode();

  }
}



    