

/* First created by JCasGen Mon Jun 25 14:26:38 JST 2012 */
package org.kachako.types.qa;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.TOP;


import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Fri Oct 26 14:29:38 JST 2012
 * XML source: /home/kano/workspace/MinervaNonfactoid/src/AcliaTypeSystemDescriptor.xml
 * @generated */
public class Nugget extends TOP {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Nugget.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Nugget() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Nugget(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Nugget(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: nuggetText

  /** getter for nuggetText - gets 
   * @generated */
  public String getNuggetText() {
    if (Nugget_Type.featOkTst && ((Nugget_Type)jcasType).casFeat_nuggetText == null)
      jcasType.jcas.throwFeatMissing("nuggetText", "org.kachako.types.qa.Nugget");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Nugget_Type)jcasType).casFeatCode_nuggetText);}
    
  /** setter for nuggetText - sets  
   * @generated */
  public void setNuggetText(String v) {
    if (Nugget_Type.featOkTst && ((Nugget_Type)jcasType).casFeat_nuggetText == null)
      jcasType.jcas.throwFeatMissing("nuggetText", "org.kachako.types.qa.Nugget");
    jcasType.ll_cas.ll_setStringValue(addr, ((Nugget_Type)jcasType).casFeatCode_nuggetText, v);}    
   
    
  //*--------------*
  //* Feature: id

  /** getter for id - gets 
   * @generated */
  public String getId() {
    if (Nugget_Type.featOkTst && ((Nugget_Type)jcasType).casFeat_id == null)
      jcasType.jcas.throwFeatMissing("id", "org.kachako.types.qa.Nugget");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Nugget_Type)jcasType).casFeatCode_id);}
    
  /** setter for id - sets  
   * @generated */
  public void setId(String v) {
    if (Nugget_Type.featOkTst && ((Nugget_Type)jcasType).casFeat_id == null)
      jcasType.jcas.throwFeatMissing("id", "org.kachako.types.qa.Nugget");
    jcasType.ll_cas.ll_setStringValue(addr, ((Nugget_Type)jcasType).casFeatCode_id, v);}    
   
    
  //*--------------*
  //* Feature: vital

  /** getter for vital - gets 
   * @generated */
  public int getVital() {
    if (Nugget_Type.featOkTst && ((Nugget_Type)jcasType).casFeat_vital == null)
      jcasType.jcas.throwFeatMissing("vital", "org.kachako.types.qa.Nugget");
    return jcasType.ll_cas.ll_getIntValue(addr, ((Nugget_Type)jcasType).casFeatCode_vital);}
    
  /** setter for vital - sets  
   * @generated */
  public void setVital(int v) {
    if (Nugget_Type.featOkTst && ((Nugget_Type)jcasType).casFeat_vital == null)
      jcasType.jcas.throwFeatMissing("vital", "org.kachako.types.qa.Nugget");
    jcasType.ll_cas.ll_setIntValue(addr, ((Nugget_Type)jcasType).casFeatCode_vital, v);}    
   
    
  //*--------------*
  //* Feature: nonVital

  /** getter for nonVital - gets 
   * @generated */
  public int getNonVital() {
    if (Nugget_Type.featOkTst && ((Nugget_Type)jcasType).casFeat_nonVital == null)
      jcasType.jcas.throwFeatMissing("nonVital", "org.kachako.types.qa.Nugget");
    return jcasType.ll_cas.ll_getIntValue(addr, ((Nugget_Type)jcasType).casFeatCode_nonVital);}
    
  /** setter for nonVital - sets  
   * @generated */
  public void setNonVital(int v) {
    if (Nugget_Type.featOkTst && ((Nugget_Type)jcasType).casFeat_nonVital == null)
      jcasType.jcas.throwFeatMissing("nonVital", "org.kachako.types.qa.Nugget");
    jcasType.ll_cas.ll_setIntValue(addr, ((Nugget_Type)jcasType).casFeatCode_nonVital, v);}    
   
    
  //*--------------*
  //* Feature: score

  /** getter for score - gets 
   * @generated */
  public double getScore() {
    if (Nugget_Type.featOkTst && ((Nugget_Type)jcasType).casFeat_score == null)
      jcasType.jcas.throwFeatMissing("score", "org.kachako.types.qa.Nugget");
    return jcasType.ll_cas.ll_getDoubleValue(addr, ((Nugget_Type)jcasType).casFeatCode_score);}
    
  /** setter for score - sets  
   * @generated */
  public void setScore(double v) {
    if (Nugget_Type.featOkTst && ((Nugget_Type)jcasType).casFeat_score == null)
      jcasType.jcas.throwFeatMissing("score", "org.kachako.types.qa.Nugget");
    jcasType.ll_cas.ll_setDoubleValue(addr, ((Nugget_Type)jcasType).casFeatCode_score, v);}    
  }

    