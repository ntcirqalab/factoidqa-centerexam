

/* First created by JCasGen Mon Jun 25 14:26:38 JST 2012 */
package org.kachako.types.qa;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.TOP;


/** 
 * Updated by JCasGen Fri Oct 26 14:29:38 JST 2012
 * XML source: /home/kano/workspace/MinervaNonfactoid/src/AcliaTypeSystemDescriptor.xml
 * @generated */
public class Metadata extends TOP {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Metadata.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Metadata() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Metadata(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Metadata(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: description

  /** getter for description - gets 
   * @generated */
  public String getDescription() {
    if (Metadata_Type.featOkTst && ((Metadata_Type)jcasType).casFeat_description == null)
      jcasType.jcas.throwFeatMissing("description", "org.kachako.types.qa.Metadata");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Metadata_Type)jcasType).casFeatCode_description);}
    
  /** setter for description - sets  
   * @generated */
  public void setDescription(String v) {
    if (Metadata_Type.featOkTst && ((Metadata_Type)jcasType).casFeat_description == null)
      jcasType.jcas.throwFeatMissing("description", "org.kachako.types.qa.Metadata");
    jcasType.ll_cas.ll_setStringValue(addr, ((Metadata_Type)jcasType).casFeatCode_description, v);}    
   
    
  //*--------------*
  //* Feature: version

  /** getter for version - gets 
   * @generated */
  public String getVersion() {
    if (Metadata_Type.featOkTst && ((Metadata_Type)jcasType).casFeat_version == null)
      jcasType.jcas.throwFeatMissing("version", "org.kachako.types.qa.Metadata");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Metadata_Type)jcasType).casFeatCode_version);}
    
  /** setter for version - sets  
   * @generated */
  public void setVersion(String v) {
    if (Metadata_Type.featOkTst && ((Metadata_Type)jcasType).casFeat_version == null)
      jcasType.jcas.throwFeatMissing("version", "org.kachako.types.qa.Metadata");
    jcasType.ll_cas.ll_setStringValue(addr, ((Metadata_Type)jcasType).casFeatCode_version, v);}    
   
    
  //*--------------*
  //* Feature: sourceLanguage

  /** getter for sourceLanguage - gets 
   * @generated */
  public String getSourceLanguage() {
    if (Metadata_Type.featOkTst && ((Metadata_Type)jcasType).casFeat_sourceLanguage == null)
      jcasType.jcas.throwFeatMissing("sourceLanguage", "org.kachako.types.qa.Metadata");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Metadata_Type)jcasType).casFeatCode_sourceLanguage);}
    
  /** setter for sourceLanguage - sets  
   * @generated */
  public void setSourceLanguage(String v) {
    if (Metadata_Type.featOkTst && ((Metadata_Type)jcasType).casFeat_sourceLanguage == null)
      jcasType.jcas.throwFeatMissing("sourceLanguage", "org.kachako.types.qa.Metadata");
    jcasType.ll_cas.ll_setStringValue(addr, ((Metadata_Type)jcasType).casFeatCode_sourceLanguage, v);}    
   
    
  //*--------------*
  //* Feature: targetLanguage

  /** getter for targetLanguage - gets 
   * @generated */
  public String getTargetLanguage() {
    if (Metadata_Type.featOkTst && ((Metadata_Type)jcasType).casFeat_targetLanguage == null)
      jcasType.jcas.throwFeatMissing("targetLanguage", "org.kachako.types.qa.Metadata");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Metadata_Type)jcasType).casFeatCode_targetLanguage);}
    
  /** setter for targetLanguage - sets  
   * @generated */
  public void setTargetLanguage(String v) {
    if (Metadata_Type.featOkTst && ((Metadata_Type)jcasType).casFeat_targetLanguage == null)
      jcasType.jcas.throwFeatMissing("targetLanguage", "org.kachako.types.qa.Metadata");
    jcasType.ll_cas.ll_setStringValue(addr, ((Metadata_Type)jcasType).casFeatCode_targetLanguage, v);}    
   
    
  //*--------------*
  //* Feature: corpus

  /** getter for corpus - gets 
   * @generated */
  public String getCorpus() {
    if (Metadata_Type.featOkTst && ((Metadata_Type)jcasType).casFeat_corpus == null)
      jcasType.jcas.throwFeatMissing("corpus", "org.kachako.types.qa.Metadata");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Metadata_Type)jcasType).casFeatCode_corpus);}
    
  /** setter for corpus - sets  
   * @generated */
  public void setCorpus(String v) {
    if (Metadata_Type.featOkTst && ((Metadata_Type)jcasType).casFeat_corpus == null)
      jcasType.jcas.throwFeatMissing("corpus", "org.kachako.types.qa.Metadata");
    jcasType.ll_cas.ll_setStringValue(addr, ((Metadata_Type)jcasType).casFeatCode_corpus, v);}    
   
    
  //*--------------*
  //* Feature: runId

  /** getter for runId - gets 
   * @generated */
  public String getRunId() {
    if (Metadata_Type.featOkTst && ((Metadata_Type)jcasType).casFeat_runId == null)
      jcasType.jcas.throwFeatMissing("runId", "org.kachako.types.qa.Metadata");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Metadata_Type)jcasType).casFeatCode_runId);}
    
  /** setter for runId - sets  
   * @generated */
  public void setRunId(String v) {
    if (Metadata_Type.featOkTst && ((Metadata_Type)jcasType).casFeat_runId == null)
      jcasType.jcas.throwFeatMissing("runId", "org.kachako.types.qa.Metadata");
    jcasType.ll_cas.ll_setStringValue(addr, ((Metadata_Type)jcasType).casFeatCode_runId, v);}    
  }

    