

/* First created by JCasGen Mon Mar 26 18:24:42 JST 2012 */
package org.kachako.types.qa;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.jcas.cas.StringArray;


import org.apache.uima.jcas.cas.IntegerArray;
import org.apache.uima.jcas.cas.StringList;
import org.apache.uima.jcas.cas.TOP;
import org.apache.uima.jcas.cas.IntegerList;


/** 
 * Updated by JCasGen Wed Jun 20 02:27:05 JST 2012
 * XML source: C:/Users/Takayuki/workspace/MinervaNonfactoid/src/AcliaTypeSystemDescriptor.xml
 * @generated */
public class Query extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Query.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Query() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Query(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Query(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Query(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: viewIdReferences

  /** getter for viewIdReferences - gets 
   * @generated */
  public IntegerArray getViewIdReferences() {
    if (Query_Type.featOkTst && ((Query_Type)jcasType).casFeat_viewIdReferences == null)
      jcasType.jcas.throwFeatMissing("viewIdReferences", "org.kachako.types.qa.Query");
    return (IntegerArray)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((Query_Type)jcasType).casFeatCode_viewIdReferences)));}
    
  /** setter for viewIdReferences - sets  
   * @generated */
  public void setViewIdReferences(IntegerArray v) {
    if (Query_Type.featOkTst && ((Query_Type)jcasType).casFeat_viewIdReferences == null)
      jcasType.jcas.throwFeatMissing("viewIdReferences", "org.kachako.types.qa.Query");
    jcasType.ll_cas.ll_setRefValue(addr, ((Query_Type)jcasType).casFeatCode_viewIdReferences, jcasType.ll_cas.ll_getFSRef(v));}    
    
  /** indexed getter for viewIdReferences - gets an indexed value - 
   * @generated */
  public int getViewIdReferences(int i) {
    if (Query_Type.featOkTst && ((Query_Type)jcasType).casFeat_viewIdReferences == null)
      jcasType.jcas.throwFeatMissing("viewIdReferences", "org.kachako.types.qa.Query");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((Query_Type)jcasType).casFeatCode_viewIdReferences), i);
    return jcasType.ll_cas.ll_getIntArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((Query_Type)jcasType).casFeatCode_viewIdReferences), i);}

  /** indexed setter for viewIdReferences - sets an indexed value - 
   * @generated */
  public void setViewIdReferences(int i, int v) { 
    if (Query_Type.featOkTst && ((Query_Type)jcasType).casFeat_viewIdReferences == null)
      jcasType.jcas.throwFeatMissing("viewIdReferences", "org.kachako.types.qa.Query");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((Query_Type)jcasType).casFeatCode_viewIdReferences), i);
    jcasType.ll_cas.ll_setIntArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((Query_Type)jcasType).casFeatCode_viewIdReferences), i, v);}
  }

    