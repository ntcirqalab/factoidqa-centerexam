

/* First created by JCasGen Mon Jun 25 14:26:38 JST 2012 */
package org.kachako.types.qa;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Fri Oct 26 14:29:37 JST 2012
 * XML source: /home/kano/workspace/MinervaNonfactoid/src/AcliaTypeSystemDescriptor.xml
 * @generated */
public class AnswerCandidate extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(AnswerCandidate.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected AnswerCandidate() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public AnswerCandidate(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public AnswerCandidate(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public AnswerCandidate(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: rank

  /** getter for rank - gets 
   * @generated */
  public int getRank() {
    if (AnswerCandidate_Type.featOkTst && ((AnswerCandidate_Type)jcasType).casFeat_rank == null)
      jcasType.jcas.throwFeatMissing("rank", "org.kachako.types.qa.AnswerCandidate");
    return jcasType.ll_cas.ll_getIntValue(addr, ((AnswerCandidate_Type)jcasType).casFeatCode_rank);}
    
  /** setter for rank - sets  
   * @generated */
  public void setRank(int v) {
    if (AnswerCandidate_Type.featOkTst && ((AnswerCandidate_Type)jcasType).casFeat_rank == null)
      jcasType.jcas.throwFeatMissing("rank", "org.kachako.types.qa.AnswerCandidate");
    jcasType.ll_cas.ll_setIntValue(addr, ((AnswerCandidate_Type)jcasType).casFeatCode_rank, v);}    
   
    
  //*--------------*
  //* Feature: score

  /** getter for score - gets 
   * @generated */
  public double getScore() {
    if (AnswerCandidate_Type.featOkTst && ((AnswerCandidate_Type)jcasType).casFeat_score == null)
      jcasType.jcas.throwFeatMissing("score", "org.kachako.types.qa.AnswerCandidate");
    return jcasType.ll_cas.ll_getDoubleValue(addr, ((AnswerCandidate_Type)jcasType).casFeatCode_score);}
    
  /** setter for score - sets  
   * @generated */
  public void setScore(double v) {
    if (AnswerCandidate_Type.featOkTst && ((AnswerCandidate_Type)jcasType).casFeat_score == null)
      jcasType.jcas.throwFeatMissing("score", "org.kachako.types.qa.AnswerCandidate");
    jcasType.ll_cas.ll_setDoubleValue(addr, ((AnswerCandidate_Type)jcasType).casFeatCode_score, v);}    
   
    
  //*--------------*
  //* Feature: docId

  /** getter for docId - gets 
   * @generated */
  public String getDocId() {
    if (AnswerCandidate_Type.featOkTst && ((AnswerCandidate_Type)jcasType).casFeat_docId == null)
      jcasType.jcas.throwFeatMissing("docId", "org.kachako.types.qa.AnswerCandidate");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnswerCandidate_Type)jcasType).casFeatCode_docId);}
    
  /** setter for docId - sets  
   * @generated */
  public void setDocId(String v) {
    if (AnswerCandidate_Type.featOkTst && ((AnswerCandidate_Type)jcasType).casFeat_docId == null)
      jcasType.jcas.throwFeatMissing("docId", "org.kachako.types.qa.AnswerCandidate");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnswerCandidate_Type)jcasType).casFeatCode_docId, v);}    
  }

    