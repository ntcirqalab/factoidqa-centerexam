
/* First created by JCasGen Mon Mar 26 18:24:42 JST 2012 */
package org.kachako.types.qa;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

import org.apache.uima.jcas.cas.TOP_Type;

/** 
 * Updated by JCasGen Wed Jun 20 02:27:05 JST 2012
 * @generated */
public class Query_Type extends Annotation_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Query_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Query_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Query(addr, Query_Type.this);
  			   Query_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Query(addr, Query_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Query.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.qa.Query");
 
  /** @generated */
  final Feature casFeat_viewIdReferences;
  /** @generated */
  final int     casFeatCode_viewIdReferences;
  /** @generated */ 
  public int getViewIdReferences(int addr) {
        if (featOkTst && casFeat_viewIdReferences == null)
      jcas.throwFeatMissing("viewIdReferences", "org.kachako.types.qa.Query");
    return ll_cas.ll_getRefValue(addr, casFeatCode_viewIdReferences);
  }
  /** @generated */    
  public void setViewIdReferences(int addr, int v) {
        if (featOkTst && casFeat_viewIdReferences == null)
      jcas.throwFeatMissing("viewIdReferences", "org.kachako.types.qa.Query");
    ll_cas.ll_setRefValue(addr, casFeatCode_viewIdReferences, v);}
    
   /** @generated */
  public int getViewIdReferences(int addr, int i) {
        if (featOkTst && casFeat_viewIdReferences == null)
      jcas.throwFeatMissing("viewIdReferences", "org.kachako.types.qa.Query");
    if (lowLevelTypeChecks)
      return ll_cas.ll_getIntArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_viewIdReferences), i, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_viewIdReferences), i);
  return ll_cas.ll_getIntArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_viewIdReferences), i);
  }
   
  /** @generated */ 
  public void setViewIdReferences(int addr, int i, int v) {
        if (featOkTst && casFeat_viewIdReferences == null)
      jcas.throwFeatMissing("viewIdReferences", "org.kachako.types.qa.Query");
    if (lowLevelTypeChecks)
      ll_cas.ll_setIntArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_viewIdReferences), i, v, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_viewIdReferences), i);
    ll_cas.ll_setIntArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_viewIdReferences), i, v);
  }
 



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Query_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_viewIdReferences = jcas.getRequiredFeatureDE(casType, "viewIdReferences", "uima.cas.IntegerArray", featOkTst);
    casFeatCode_viewIdReferences  = (null == casFeat_viewIdReferences) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_viewIdReferences).getCode();

  }
}



    