

/* First created by JCasGen Wed Sep 26 17:50:54 JST 2012 */
package org.kachako.types.qa;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.TOP;


/** 
 * Updated by JCasGen Fri Oct 26 14:29:37 JST 2012
 * XML source: /home/kano/workspace/MinervaNonfactoid/src/AcliaTypeSystemDescriptor.xml
 * @generated */
public class AnswerFeature extends TOP {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(AnswerFeature.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected AnswerFeature() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public AnswerFeature(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public AnswerFeature(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: word

  /** getter for word - gets 
   * @generated */
  public String getWord() {
    if (AnswerFeature_Type.featOkTst && ((AnswerFeature_Type)jcasType).casFeat_word == null)
      jcasType.jcas.throwFeatMissing("word", "org.kachako.types.qa.AnswerFeature");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnswerFeature_Type)jcasType).casFeatCode_word);}
    
  /** setter for word - sets  
   * @generated */
  public void setWord(String v) {
    if (AnswerFeature_Type.featOkTst && ((AnswerFeature_Type)jcasType).casFeat_word == null)
      jcasType.jcas.throwFeatMissing("word", "org.kachako.types.qa.AnswerFeature");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnswerFeature_Type)jcasType).casFeatCode_word, v);}    
   
    
  //*--------------*
  //* Feature: chiSquare

  /** getter for chiSquare - gets 
   * @generated */
  public double getChiSquare() {
    if (AnswerFeature_Type.featOkTst && ((AnswerFeature_Type)jcasType).casFeat_chiSquare == null)
      jcasType.jcas.throwFeatMissing("chiSquare", "org.kachako.types.qa.AnswerFeature");
    return jcasType.ll_cas.ll_getDoubleValue(addr, ((AnswerFeature_Type)jcasType).casFeatCode_chiSquare);}
    
  /** setter for chiSquare - sets  
   * @generated */
  public void setChiSquare(double v) {
    if (AnswerFeature_Type.featOkTst && ((AnswerFeature_Type)jcasType).casFeat_chiSquare == null)
      jcasType.jcas.throwFeatMissing("chiSquare", "org.kachako.types.qa.AnswerFeature");
    jcasType.ll_cas.ll_setDoubleValue(addr, ((AnswerFeature_Type)jcasType).casFeatCode_chiSquare, v);}    
  }

    