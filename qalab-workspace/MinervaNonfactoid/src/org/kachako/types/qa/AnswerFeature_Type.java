
/* First created by JCasGen Wed Sep 26 17:50:54 JST 2012 */
package org.kachako.types.qa;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.cas.TOP_Type;

/** 
 * Updated by JCasGen Fri Oct 26 14:29:37 JST 2012
 * @generated */
public class AnswerFeature_Type extends TOP_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (AnswerFeature_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = AnswerFeature_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new AnswerFeature(addr, AnswerFeature_Type.this);
  			   AnswerFeature_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new AnswerFeature(addr, AnswerFeature_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = AnswerFeature.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.qa.AnswerFeature");
 
  /** @generated */
  final Feature casFeat_word;
  /** @generated */
  final int     casFeatCode_word;
  /** @generated */ 
  public String getWord(int addr) {
        if (featOkTst && casFeat_word == null)
      jcas.throwFeatMissing("word", "org.kachako.types.qa.AnswerFeature");
    return ll_cas.ll_getStringValue(addr, casFeatCode_word);
  }
  /** @generated */    
  public void setWord(int addr, String v) {
        if (featOkTst && casFeat_word == null)
      jcas.throwFeatMissing("word", "org.kachako.types.qa.AnswerFeature");
    ll_cas.ll_setStringValue(addr, casFeatCode_word, v);}
    
  
 
  /** @generated */
  final Feature casFeat_chiSquare;
  /** @generated */
  final int     casFeatCode_chiSquare;
  /** @generated */ 
  public double getChiSquare(int addr) {
        if (featOkTst && casFeat_chiSquare == null)
      jcas.throwFeatMissing("chiSquare", "org.kachako.types.qa.AnswerFeature");
    return ll_cas.ll_getDoubleValue(addr, casFeatCode_chiSquare);
  }
  /** @generated */    
  public void setChiSquare(int addr, double v) {
        if (featOkTst && casFeat_chiSquare == null)
      jcas.throwFeatMissing("chiSquare", "org.kachako.types.qa.AnswerFeature");
    ll_cas.ll_setDoubleValue(addr, casFeatCode_chiSquare, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public AnswerFeature_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_word = jcas.getRequiredFeatureDE(casType, "word", "uima.cas.String", featOkTst);
    casFeatCode_word  = (null == casFeat_word) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_word).getCode();

 
    casFeat_chiSquare = jcas.getRequiredFeatureDE(casType, "chiSquare", "uima.cas.Double", featOkTst);
    casFeatCode_chiSquare  = (null == casFeat_chiSquare) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_chiSquare).getCode();

  }
}



    