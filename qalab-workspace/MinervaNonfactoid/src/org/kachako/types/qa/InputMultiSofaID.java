

/* First created by JCasGen Mon Jun 25 14:26:38 JST 2012 */
package org.kachako.types.qa;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Fri Oct 26 14:29:38 JST 2012
 * XML source: /home/kano/workspace/MinervaNonfactoid/src/AcliaTypeSystemDescriptor.xml
 * @generated */
public class InputMultiSofaID extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(InputMultiSofaID.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected InputMultiSofaID() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public InputMultiSofaID(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public InputMultiSofaID(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public InputMultiSofaID(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: sofaID

  /** getter for sofaID - gets 
   * @generated */
  public String getSofaID() {
    if (InputMultiSofaID_Type.featOkTst && ((InputMultiSofaID_Type)jcasType).casFeat_sofaID == null)
      jcasType.jcas.throwFeatMissing("sofaID", "org.kachako.types.qa.InputMultiSofaID");
    return jcasType.ll_cas.ll_getStringValue(addr, ((InputMultiSofaID_Type)jcasType).casFeatCode_sofaID);}
    
  /** setter for sofaID - sets  
   * @generated */
  public void setSofaID(String v) {
    if (InputMultiSofaID_Type.featOkTst && ((InputMultiSofaID_Type)jcasType).casFeat_sofaID == null)
      jcasType.jcas.throwFeatMissing("sofaID", "org.kachako.types.qa.InputMultiSofaID");
    jcasType.ll_cas.ll_setStringValue(addr, ((InputMultiSofaID_Type)jcasType).casFeatCode_sofaID, v);}    
  }

    