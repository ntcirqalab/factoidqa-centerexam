
/* First created by JCasGen Mon Jun 25 14:26:38 JST 2012 */
package org.kachako.types.qa;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Fri Oct 26 14:29:38 JST 2012
 * @generated */
public class Nugget_Type extends TOP_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Nugget_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Nugget_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Nugget(addr, Nugget_Type.this);
  			   Nugget_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Nugget(addr, Nugget_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Nugget.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.qa.Nugget");
 
  /** @generated */
  final Feature casFeat_nuggetText;
  /** @generated */
  final int     casFeatCode_nuggetText;
  /** @generated */ 
  public String getNuggetText(int addr) {
        if (featOkTst && casFeat_nuggetText == null)
      jcas.throwFeatMissing("nuggetText", "org.kachako.types.qa.Nugget");
    return ll_cas.ll_getStringValue(addr, casFeatCode_nuggetText);
  }
  /** @generated */    
  public void setNuggetText(int addr, String v) {
        if (featOkTst && casFeat_nuggetText == null)
      jcas.throwFeatMissing("nuggetText", "org.kachako.types.qa.Nugget");
    ll_cas.ll_setStringValue(addr, casFeatCode_nuggetText, v);}
    
  
 
  /** @generated */
  final Feature casFeat_id;
  /** @generated */
  final int     casFeatCode_id;
  /** @generated */ 
  public String getId(int addr) {
        if (featOkTst && casFeat_id == null)
      jcas.throwFeatMissing("id", "org.kachako.types.qa.Nugget");
    return ll_cas.ll_getStringValue(addr, casFeatCode_id);
  }
  /** @generated */    
  public void setId(int addr, String v) {
        if (featOkTst && casFeat_id == null)
      jcas.throwFeatMissing("id", "org.kachako.types.qa.Nugget");
    ll_cas.ll_setStringValue(addr, casFeatCode_id, v);}
    
  
 
  /** @generated */
  final Feature casFeat_vital;
  /** @generated */
  final int     casFeatCode_vital;
  /** @generated */ 
  public int getVital(int addr) {
        if (featOkTst && casFeat_vital == null)
      jcas.throwFeatMissing("vital", "org.kachako.types.qa.Nugget");
    return ll_cas.ll_getIntValue(addr, casFeatCode_vital);
  }
  /** @generated */    
  public void setVital(int addr, int v) {
        if (featOkTst && casFeat_vital == null)
      jcas.throwFeatMissing("vital", "org.kachako.types.qa.Nugget");
    ll_cas.ll_setIntValue(addr, casFeatCode_vital, v);}
    
  
 
  /** @generated */
  final Feature casFeat_nonVital;
  /** @generated */
  final int     casFeatCode_nonVital;
  /** @generated */ 
  public int getNonVital(int addr) {
        if (featOkTst && casFeat_nonVital == null)
      jcas.throwFeatMissing("nonVital", "org.kachako.types.qa.Nugget");
    return ll_cas.ll_getIntValue(addr, casFeatCode_nonVital);
  }
  /** @generated */    
  public void setNonVital(int addr, int v) {
        if (featOkTst && casFeat_nonVital == null)
      jcas.throwFeatMissing("nonVital", "org.kachako.types.qa.Nugget");
    ll_cas.ll_setIntValue(addr, casFeatCode_nonVital, v);}
    
  
 
  /** @generated */
  final Feature casFeat_score;
  /** @generated */
  final int     casFeatCode_score;
  /** @generated */ 
  public double getScore(int addr) {
        if (featOkTst && casFeat_score == null)
      jcas.throwFeatMissing("score", "org.kachako.types.qa.Nugget");
    return ll_cas.ll_getDoubleValue(addr, casFeatCode_score);
  }
  /** @generated */    
  public void setScore(int addr, double v) {
        if (featOkTst && casFeat_score == null)
      jcas.throwFeatMissing("score", "org.kachako.types.qa.Nugget");
    ll_cas.ll_setDoubleValue(addr, casFeatCode_score, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Nugget_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_nuggetText = jcas.getRequiredFeatureDE(casType, "nuggetText", "uima.cas.String", featOkTst);
    casFeatCode_nuggetText  = (null == casFeat_nuggetText) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_nuggetText).getCode();

 
    casFeat_id = jcas.getRequiredFeatureDE(casType, "id", "uima.cas.String", featOkTst);
    casFeatCode_id  = (null == casFeat_id) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_id).getCode();

 
    casFeat_vital = jcas.getRequiredFeatureDE(casType, "vital", "uima.cas.Integer", featOkTst);
    casFeatCode_vital  = (null == casFeat_vital) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_vital).getCode();

 
    casFeat_nonVital = jcas.getRequiredFeatureDE(casType, "nonVital", "uima.cas.Integer", featOkTst);
    casFeatCode_nonVital  = (null == casFeat_nonVital) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_nonVital).getCode();

 
    casFeat_score = jcas.getRequiredFeatureDE(casType, "score", "uima.cas.Double", featOkTst);
    casFeatCode_score  = (null == casFeat_score) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_score).getCode();

  }
}



    