

/* First created by JCasGen Mon Jun 25 14:26:38 JST 2012 */
package org.kachako.types.qa;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Fri Oct 26 14:29:38 JST 2012
 * XML source: /home/kano/workspace/MinervaNonfactoid/src/AcliaTypeSystemDescriptor.xml
 * @generated */
public class KeyTerm extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(KeyTerm.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected KeyTerm() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public KeyTerm(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public KeyTerm(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public KeyTerm(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: score

  /** getter for score - gets 
   * @generated */
  public double getScore() {
    if (KeyTerm_Type.featOkTst && ((KeyTerm_Type)jcasType).casFeat_score == null)
      jcasType.jcas.throwFeatMissing("score", "org.kachako.types.qa.KeyTerm");
    return jcasType.ll_cas.ll_getDoubleValue(addr, ((KeyTerm_Type)jcasType).casFeatCode_score);}
    
  /** setter for score - sets  
   * @generated */
  public void setScore(double v) {
    if (KeyTerm_Type.featOkTst && ((KeyTerm_Type)jcasType).casFeat_score == null)
      jcasType.jcas.throwFeatMissing("score", "org.kachako.types.qa.KeyTerm");
    jcasType.ll_cas.ll_setDoubleValue(addr, ((KeyTerm_Type)jcasType).casFeatCode_score, v);}    
   
    
  //*--------------*
  //* Feature: attribute

  /** getter for attribute - gets 
   * @generated */
  public String getAttribute() {
    if (KeyTerm_Type.featOkTst && ((KeyTerm_Type)jcasType).casFeat_attribute == null)
      jcasType.jcas.throwFeatMissing("attribute", "org.kachako.types.qa.KeyTerm");
    return jcasType.ll_cas.ll_getStringValue(addr, ((KeyTerm_Type)jcasType).casFeatCode_attribute);}
    
  /** setter for attribute - sets  
   * @generated */
  public void setAttribute(String v) {
    if (KeyTerm_Type.featOkTst && ((KeyTerm_Type)jcasType).casFeat_attribute == null)
      jcasType.jcas.throwFeatMissing("attribute", "org.kachako.types.qa.KeyTerm");
    jcasType.ll_cas.ll_setStringValue(addr, ((KeyTerm_Type)jcasType).casFeatCode_attribute, v);}    
   
    
  //*--------------*
  //* Feature: language

  /** getter for language - gets 
   * @generated */
  public String getLanguage() {
    if (KeyTerm_Type.featOkTst && ((KeyTerm_Type)jcasType).casFeat_language == null)
      jcasType.jcas.throwFeatMissing("language", "org.kachako.types.qa.KeyTerm");
    return jcasType.ll_cas.ll_getStringValue(addr, ((KeyTerm_Type)jcasType).casFeatCode_language);}
    
  /** setter for language - sets  
   * @generated */
  public void setLanguage(String v) {
    if (KeyTerm_Type.featOkTst && ((KeyTerm_Type)jcasType).casFeat_language == null)
      jcasType.jcas.throwFeatMissing("language", "org.kachako.types.qa.KeyTerm");
    jcasType.ll_cas.ll_setStringValue(addr, ((KeyTerm_Type)jcasType).casFeatCode_language, v);}    
  }

    