
/* First created by JCasGen Mon Jun 25 14:26:38 JST 2012 */
package org.kachako.types.qa;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.cas.TOP_Type;

/** 
 * Updated by JCasGen Fri Oct 26 14:29:38 JST 2012
 * @generated */
public class Metadata_Type extends TOP_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Metadata_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Metadata_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Metadata(addr, Metadata_Type.this);
  			   Metadata_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Metadata(addr, Metadata_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Metadata.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("org.kachako.types.qa.Metadata");
 
  /** @generated */
  final Feature casFeat_description;
  /** @generated */
  final int     casFeatCode_description;
  /** @generated */ 
  public String getDescription(int addr) {
        if (featOkTst && casFeat_description == null)
      jcas.throwFeatMissing("description", "org.kachako.types.qa.Metadata");
    return ll_cas.ll_getStringValue(addr, casFeatCode_description);
  }
  /** @generated */    
  public void setDescription(int addr, String v) {
        if (featOkTst && casFeat_description == null)
      jcas.throwFeatMissing("description", "org.kachako.types.qa.Metadata");
    ll_cas.ll_setStringValue(addr, casFeatCode_description, v);}
    
  
 
  /** @generated */
  final Feature casFeat_version;
  /** @generated */
  final int     casFeatCode_version;
  /** @generated */ 
  public String getVersion(int addr) {
        if (featOkTst && casFeat_version == null)
      jcas.throwFeatMissing("version", "org.kachako.types.qa.Metadata");
    return ll_cas.ll_getStringValue(addr, casFeatCode_version);
  }
  /** @generated */    
  public void setVersion(int addr, String v) {
        if (featOkTst && casFeat_version == null)
      jcas.throwFeatMissing("version", "org.kachako.types.qa.Metadata");
    ll_cas.ll_setStringValue(addr, casFeatCode_version, v);}
    
  
 
  /** @generated */
  final Feature casFeat_sourceLanguage;
  /** @generated */
  final int     casFeatCode_sourceLanguage;
  /** @generated */ 
  public String getSourceLanguage(int addr) {
        if (featOkTst && casFeat_sourceLanguage == null)
      jcas.throwFeatMissing("sourceLanguage", "org.kachako.types.qa.Metadata");
    return ll_cas.ll_getStringValue(addr, casFeatCode_sourceLanguage);
  }
  /** @generated */    
  public void setSourceLanguage(int addr, String v) {
        if (featOkTst && casFeat_sourceLanguage == null)
      jcas.throwFeatMissing("sourceLanguage", "org.kachako.types.qa.Metadata");
    ll_cas.ll_setStringValue(addr, casFeatCode_sourceLanguage, v);}
    
  
 
  /** @generated */
  final Feature casFeat_targetLanguage;
  /** @generated */
  final int     casFeatCode_targetLanguage;
  /** @generated */ 
  public String getTargetLanguage(int addr) {
        if (featOkTst && casFeat_targetLanguage == null)
      jcas.throwFeatMissing("targetLanguage", "org.kachako.types.qa.Metadata");
    return ll_cas.ll_getStringValue(addr, casFeatCode_targetLanguage);
  }
  /** @generated */    
  public void setTargetLanguage(int addr, String v) {
        if (featOkTst && casFeat_targetLanguage == null)
      jcas.throwFeatMissing("targetLanguage", "org.kachako.types.qa.Metadata");
    ll_cas.ll_setStringValue(addr, casFeatCode_targetLanguage, v);}
    
  
 
  /** @generated */
  final Feature casFeat_corpus;
  /** @generated */
  final int     casFeatCode_corpus;
  /** @generated */ 
  public String getCorpus(int addr) {
        if (featOkTst && casFeat_corpus == null)
      jcas.throwFeatMissing("corpus", "org.kachako.types.qa.Metadata");
    return ll_cas.ll_getStringValue(addr, casFeatCode_corpus);
  }
  /** @generated */    
  public void setCorpus(int addr, String v) {
        if (featOkTst && casFeat_corpus == null)
      jcas.throwFeatMissing("corpus", "org.kachako.types.qa.Metadata");
    ll_cas.ll_setStringValue(addr, casFeatCode_corpus, v);}
    
  
 
  /** @generated */
  final Feature casFeat_runId;
  /** @generated */
  final int     casFeatCode_runId;
  /** @generated */ 
  public String getRunId(int addr) {
        if (featOkTst && casFeat_runId == null)
      jcas.throwFeatMissing("runId", "org.kachako.types.qa.Metadata");
    return ll_cas.ll_getStringValue(addr, casFeatCode_runId);
  }
  /** @generated */    
  public void setRunId(int addr, String v) {
        if (featOkTst && casFeat_runId == null)
      jcas.throwFeatMissing("runId", "org.kachako.types.qa.Metadata");
    ll_cas.ll_setStringValue(addr, casFeatCode_runId, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Metadata_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_description = jcas.getRequiredFeatureDE(casType, "description", "uima.cas.String", featOkTst);
    casFeatCode_description  = (null == casFeat_description) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_description).getCode();

 
    casFeat_version = jcas.getRequiredFeatureDE(casType, "version", "uima.cas.String", featOkTst);
    casFeatCode_version  = (null == casFeat_version) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_version).getCode();

 
    casFeat_sourceLanguage = jcas.getRequiredFeatureDE(casType, "sourceLanguage", "uima.cas.String", featOkTst);
    casFeatCode_sourceLanguage  = (null == casFeat_sourceLanguage) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_sourceLanguage).getCode();

 
    casFeat_targetLanguage = jcas.getRequiredFeatureDE(casType, "targetLanguage", "uima.cas.String", featOkTst);
    casFeatCode_targetLanguage  = (null == casFeat_targetLanguage) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_targetLanguage).getCode();

 
    casFeat_corpus = jcas.getRequiredFeatureDE(casType, "corpus", "uima.cas.String", featOkTst);
    casFeatCode_corpus  = (null == casFeat_corpus) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_corpus).getCode();

 
    casFeat_runId = jcas.getRequiredFeatureDE(casType, "runId", "uima.cas.String", featOkTst);
    casFeatCode_runId  = (null == casFeat_runId) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_runId).getCode();

  }
}



    