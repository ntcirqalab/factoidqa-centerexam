package org.kachako.share.util;

/**
   タプルを表すためのクラス。
 */

public class Tuple<F,S> {

    public final F fst;
    public final S snd;

    public Tuple(F _fst, S _snd){
        this.fst = _fst;
        this.snd = _snd;
    } 

}