package org.kachako.share.regex;

import java.util.*;
import java.util.regex.*;

public class RegexCache {
	
	private static class Key {
		public final String exp;
		public final int flg;
		public Key(String exp, int flg){
			this.exp = exp;
			this.flg = flg;
		}
		
		public boolean equals(Object o){
			if(o instanceof Key){
				Key k = (Key)o;
				return exp.equals(k.exp) && flg == k.flg;
			}
			return false;
		}
		
		public int hashCode(){
			final int M = 37;
			int ans = 17;
			
			ans = M*ans + exp.hashCode();
			ans = M*ans + flg;
			
			return ans;
		}
	}

    private Map<Key, Pattern> mem = new HashMap<>();

    private Matcher m = null;

    public Pattern compile(String regex){
        return compile(regex, 0);
    }

    public Pattern compile(String regex, int flags){
        Key key = new Key(regex, flags);
        Pattern p = mem.get(key);
        if(p != null)
            return p;

        p = Pattern.compile(regex, flags);
        mem.put(key, p);
        return p;
    }

    public boolean matches(String regex, CharSequence input){
        m = compile(regex).matcher(input);
        return m.matches();
    }

    public boolean find(String regex, CharSequence input){
        m = compile(regex).matcher(input);
        return m.find();
    }

    public String lastMatched(){
        return m.group();
    }

    public String lastMatched(int i){
        return m.group(i);
    }

    public PartialApp appQuery(String query){
        return new PartialApp(this, query);
    }

    public static class PartialApp {

        private final RegexCache r;
        private final String s;

        public PartialApp(RegexCache r, String s){
            this.r = r;
            this.s = s;
        }

        public boolean matches(String regex){
            return r.matches(regex, s);
        }

        public boolean find(String regex){
            return r.find(regex, s);
        }

    }

}
