package org.u_compare;

import java.io.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;

/**
 * @author Takayuki
 *
 */
public class AcliaCRTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new File("C:/Users/Takayuki/Documents/My Dropbox/realglobe/UCompare/aclia2_dataset/aclia2_dataset/gold_standard/gold standard/ACLIA2-JA-T.xml"));
			Element root = doc.getDocumentElement();
			printChildren((Node)root);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return;
	}
	
	private static void printChildren(Node childNode){
		System.out.println(childNode.getNodeName());
		System.out.println(childNode.getNodeValue());
		NodeList children = childNode.getChildNodes();
		for(int i = 0; i < children.getLength(); i++){
			printChildren(children.item(i));
		}
		return;
	}

}
