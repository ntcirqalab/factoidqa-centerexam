package aclia_reader;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

import javax.xml.bind.JAXBException;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;

import org.kachako.types.qa.AnswerType;
import org.kachako.types.qa.KeyTerm;
import org.kachako.types.qa.Metadata;

/**
 * @author Takayuki SUZUKI
 *
 */
public class QuestionAnalysisCollectionReader extends AcliaCollectionReaderAbstract {

	private Queue<generated.question_analysis.TOPIC> topicElementQueue = new LinkedList<generated.question_analysis.TOPIC>();
	private generated.question_analysis.METADATA metadataElement = null;
	private static final String PARAM_INPUT_XML_FILE_NAME = "InputXmlFileName";
	private static final String GENERATED_CLASS_PACKAGE_NAME = "generated.question_analysis";
	private static final String QUESTION_ANALYSIS_VIEW_NAME = "QuestionAnalysisView"; // TODO decide view name

	@Override
	public void initialize() throws ResourceInitializationException {
		super.initialize();

		try {
			//get TOPIC_SET
			generated.question_analysis.TOPICSET topicSetElement = (generated.question_analysis.TOPICSET) unmarshall(PARAM_INPUT_XML_FILE_NAME, GENERATED_CLASS_PACKAGE_NAME);

			//get TOPICs and add queue
			topicElementQueue.addAll(topicSetElement.getTOPIC());

			//get METADATA
			metadataElement = topicSetElement.getMETADATA();

		} catch (JAXBException e) {
			e.printStackTrace();
			throw new ResourceInitializationException(e);
		}
	}
	
	@Override
	public void getNext(CAS topicCAS) throws IOException, CollectionException {
		try{
			//get current TOPIC
			generated.question_analysis.TOPIC currentTopicElement = topicElementQueue.poll();

			//get JCas
			JCas topicJCas = topicCAS.getJCas();

			//create Question Analysis view
			JCas questionAnalysisViewJCas = topicJCas.createView(QUESTION_ANALYSIS_VIEW_NAME); //TODO decide sofa name

			//add Metadata
			addMetadata(topicJCas);

			//create and set Documents of Question Analysis sofa, and add KeyTerm types
			setUpQuestionAnalysisView(currentTopicElement, questionAnalysisViewJCas);

		} catch(CASException e){
			e.printStackTrace();
			throw new CollectionException(e);
		}
	}

	private void addMetadata(JCas topicJCas){
		Metadata metadataFS = new Metadata(topicJCas);
		metadataFS.addToIndexes();
		metadataFS.setDescription(metadataElement.getDESCRIPTION());
		metadataFS.setRunId(metadataElement.getRUNID());
	}

	private void setUpQuestionAnalysisView(generated.question_analysis.TOPIC currentTopicElement, JCas questionAnalysisViewJCas) {
		StringBuilder questionAnalysisSofaDocument = new StringBuilder();

		//add AnswerType
		addAnswerType(currentTopicElement, questionAnalysisViewJCas);

		//add KeyTerms
		for(generated.question_analysis.KEYTERM keyTermElement : currentTopicElement.getQUESTIONANALYSIS().getKEYTERMS().getKEYTERM()) {
			addKeyTerm(keyTermElement, questionAnalysisViewJCas, questionAnalysisSofaDocument, currentTopicElement.getQUESTIONANALYSIS().getKEYTERMS().getLANGUAGE());
		}

		//set document of Question Analysis sofa
		questionAnalysisViewJCas.setDocumentText(questionAnalysisSofaDocument.toString());

		//set language of Question Analysis sofa
		//TODO decide language of sofa
//		questionAnalysisViewJCas.setDocumentLanguage(currentTopic.getQUESTIONANALYSIS().getKEYTERMS().getLANGUAGE());

		//set URI of Question Analysis sofa
		SourceDocumentInformation questionAnalysisSofaSourceDocumentInformationFS = new SourceDocumentInformation(questionAnalysisViewJCas);
		questionAnalysisSofaSourceDocumentInformationFS.addToIndexes();
		questionAnalysisSofaSourceDocumentInformationFS.setUri(currentTopicElement.getID());
	}

	private void addAnswerType(generated.question_analysis.TOPIC currentTopicElement, JCas questionAnalysisViewJCas) {
		AnswerType answerTypeFS = new AnswerType(questionAnalysisViewJCas);
		answerTypeFS.addToIndexes();
		answerTypeFS.setAnswerType(currentTopicElement.getQUESTIONANALYSIS().getANSWERTYPE().getvalue());
		answerTypeFS.setScore(Double.parseDouble(currentTopicElement.getQUESTIONANALYSIS().getANSWERTYPE().getSCORE()));
	}

	private void addKeyTerm(generated.question_analysis.KEYTERM currentKeyTermElement, JCas questionAnalysisViewJCas, StringBuilder questionAnalysisSofaDocument, String language) {
		KeyTerm keyTermFS = new KeyTerm(questionAnalysisViewJCas);
		keyTermFS.addToIndexes();
		int beginningOfKeyTermIndex = questionAnalysisSofaDocument.length();
		questionAnalysisSofaDocument.append(currentKeyTermElement.getvalue()).append(LINE_SEPARATOR);
		int endOfKeyTermIndex = questionAnalysisSofaDocument.length() - 1;
		keyTermFS.setBegin(beginningOfKeyTermIndex);
		keyTermFS.setEnd(endOfKeyTermIndex);
		keyTermFS.setScore(Double.parseDouble(currentKeyTermElement.getSCORE()));
		keyTermFS.setLanguage(language);
	}

	@Override
	public boolean hasNext() throws IOException, CollectionException {
		return !topicElementQueue.isEmpty();
	}
}
