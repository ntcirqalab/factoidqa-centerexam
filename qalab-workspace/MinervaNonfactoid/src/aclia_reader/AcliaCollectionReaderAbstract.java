package aclia_reader;

import java.io.File;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.uima.collection.CollectionReader_ImplBase;
import org.apache.uima.util.Progress;

/**
 * 
 * @author Takayuki SUZUKI
 *
 */
public abstract class AcliaCollectionReaderAbstract extends
		CollectionReader_ImplBase {
	protected final String LINE_SEPARATOR = "\n";

	protected Object unmarshall(String xmlFileName, String packageName) throws JAXBException{
		//open XML file
		File targetXmlDocument = new File(xmlFileName);

		//parse the documents and unmarshall

		//create JAXBContext
		JAXBContext context = JAXBContext.newInstance(packageName);

		//get Unmarshaller
		Unmarshaller unmarshaller = context.createUnmarshaller();

		//unmarshalling
		return unmarshaller.unmarshal(targetXmlDocument);
	}

	@Override
	public void close() throws IOException {
	}

	@Override
	public Progress[] getProgress() {
		return null;
	}
}
