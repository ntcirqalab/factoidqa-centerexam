package aclia_reader;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

import javax.xml.bind.JAXBException;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.kachako.types.qa.AnswerCandidate;
import org.kachako.types.qa.Metadata;

/**
 * @author Takayuki SUZUKI
 *
 */
public class CclqaCollectionReader extends AcliaCollectionReaderAbstract {

	private Queue<generated.cclqa.TOPIC> topicElementQueue = new LinkedList<generated.cclqa.TOPIC>();
	private generated.cclqa.METADATA metadataElement = null;
	private static final String GENERATED_CLASS_PACKAGE_NAME = "generated.cclqa";
	private static final String CCLQA_VIEW_NAME = "CclqaView"; //TODO decide CCLQA view name
	private static final String PARAM_INPUT_XML_FILE_NAME = "InputXmlFileName";

	@Override
	public void initialize() throws ResourceInitializationException {
		super.initialize();

		//get input file name
		String inputXmlFileName = (String) getConfigParameterValue(PARAM_INPUT_XML_FILE_NAME);

		try {
			//get TOPIC_SET
			generated.cclqa.TOPICSET topicSetElement = (generated.cclqa.TOPICSET) unmarshall(inputXmlFileName, GENERATED_CLASS_PACKAGE_NAME);

			//get TOPICs and add queue
			topicElementQueue.addAll(topicSetElement.getTOPIC());

			//get METADATA
			metadataElement = topicSetElement.getMETADATA();
		} catch (JAXBException e) {
			e.printStackTrace();
			throw new ResourceInitializationException(e);
		}
	}

	@Override
	public void getNext(CAS topicCAS) throws IOException, CollectionException {
		try{
			//get current TOPIC
			generated.cclqa.TOPIC currentTopicElement = topicElementQueue.poll();

			//get JCas
			JCas topicJCas = topicCAS.getJCas();

			//create CCLQA view
			JCas cclqaViewJCas = topicJCas.createView(CCLQA_VIEW_NAME);

			//add Metadata
			addMetadata(topicJCas.getView(CCLQA_VIEW_NAME));

			//create and set Documents of CCLQA view, and add AnswerCandidate FSs
			setUpCclqaView(currentTopicElement, cclqaViewJCas);

		} catch(CASException e){
			e.printStackTrace();
			throw new CollectionException(e);
		}
	}

	private void addMetadata(JCas targetJCas) {
		Metadata metadataFS = new Metadata(targetJCas);
		metadataFS.addToIndexes();
		metadataFS.setDescription(metadataElement.getDESCRIPTION());
		metadataFS.setRunId(metadataElement.getRUNID());		
	}

	private void setUpCclqaView(generated.cclqa.TOPIC currentTopicElement, JCas cclqaViewJCas) {
		StringBuilder cclqaSofaDocument = new StringBuilder();

		//add AnswerCandidates
		for(generated.cclqa.ANSWERCANDIDATE answerCandidateElement : currentTopicElement.getCCLQARESULT().getANSWERCANDIDATE()) {
			addAnswerCandidate(answerCandidateElement, cclqaViewJCas, cclqaSofaDocument);
		}

		//set document of CCLQA sofa
		cclqaViewJCas.setDocumentText(cclqaSofaDocument.toString());

		//set URI of CCLQA sofa
		SourceDocumentInformation cclqaSofaSourceDocumentInformationFS = new SourceDocumentInformation(cclqaViewJCas);
		cclqaSofaSourceDocumentInformationFS.addToIndexes();
		cclqaSofaSourceDocumentInformationFS.setUri(currentTopicElement.getID());
	}

	private void addAnswerCandidate(generated.cclqa.ANSWERCANDIDATE currentAnswerCandidateElement, JCas cclqaViewJCas, StringBuilder cclqaSofaDocument) {
		AnswerCandidate answerCandidateFS = new AnswerCandidate(cclqaViewJCas);
		answerCandidateFS.addToIndexes();
		int beginningOfAnswerCandidateIndex = cclqaSofaDocument.length();
		cclqaSofaDocument.append(currentAnswerCandidateElement.getvalue()).append(LINE_SEPARATOR);
		int endOfAnswerCandidateIndex = cclqaSofaDocument.length() - 1;
		answerCandidateFS.setBegin(beginningOfAnswerCandidateIndex);
		answerCandidateFS.setEnd(endOfAnswerCandidateIndex);
		answerCandidateFS.setRank(Integer.parseInt(currentAnswerCandidateElement.getRANK()));
		answerCandidateFS.setScore(Double.parseDouble(currentAnswerCandidateElement.getSCORE()));
	}

	@Override
	public boolean hasNext() throws IOException, CollectionException {
		return !topicElementQueue.isEmpty();
	}
}
