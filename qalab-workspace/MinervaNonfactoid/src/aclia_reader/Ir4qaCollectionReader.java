package aclia_reader;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

import javax.xml.bind.JAXBException;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;

import org.kachako.types.qa.Document;
import org.kachako.types.qa.Metadata;

/**
 * @author Takayuki SUZUKI
 *
 */
public class Ir4qaCollectionReader extends AcliaCollectionReaderAbstract {

	private static final String PARAM_INPUT_XML_FILE_NAME = "InputXmlFileName";
	private static final String GENERATED_CLASS_PACKAGE_NAME = "generated.ir4qa";
	private static final String IR4QA_VIEW_NAME = "Ir4qaView";
	private Queue<generated.ir4qa.TOPIC> topicElementQueue = new LinkedList<generated.ir4qa.TOPIC>();
	private generated.ir4qa.METADATA metadataElement = null;

	// 2012-03-26 実装済
	@Override
	public void initialize() throws ResourceInitializationException {
		super.initialize();

		//get input file name
		String inputXmlFileName = (String) getConfigParameterValue(PARAM_INPUT_XML_FILE_NAME);

		try {
			//get TOPIC_SET
			generated.ir4qa.TOPICSET topicSetElement = (generated.ir4qa.TOPICSET) unmarshall(inputXmlFileName, GENERATED_CLASS_PACKAGE_NAME);

			//get TOPICs and add queue
			topicElementQueue.addAll(topicSetElement.getTOPIC());

			//get METADATA
			metadataElement = topicSetElement.getMETADATA();
		} catch (JAXBException e) {
			e.printStackTrace();
			throw new ResourceInitializationException(e);
		}
	}

	// 2012-03-26 実装済
	@Override
	public void getNext(CAS topicCAS) throws IOException, CollectionException {
		try{
			//get current TOPIC
			generated.ir4qa.TOPIC currentTopicElement = topicElementQueue.poll();

			//get JCas
			JCas topicJCas = topicCAS.getJCas();

			//create IR4QA view
			JCas cclqaViewJCas = topicJCas.createView(IR4QA_VIEW_NAME);

			//add Metadata
			addMetadata(topicJCas.getView(IR4QA_VIEW_NAME));

			//add Document FSs
			setUpIr4qaView(currentTopicElement, cclqaViewJCas);

		} catch(CASException e){
			e.printStackTrace();
			throw new CollectionException(e);
		}
	}

	// 2012-03-26 実装済
	private void addMetadata(JCas targetJCas) {
		Metadata metadataFS = new Metadata(targetJCas);
		metadataFS.addToIndexes();
		metadataFS.setDescription(metadataElement.getDESCRIPTION());
		metadataFS.setRunId(metadataElement.getRUNID());		
	}

	// 2012-03-26 実装済
	private void setUpIr4qaView(generated.ir4qa.TOPIC currentTopicElement,
			JCas ir4qaViewJCas) {
		//add Document FSs
		for(generated.ir4qa.DOCUMENT documentElement : currentTopicElement.getIR4QARESULT().getDOCUMENT()){
			addDocument(documentElement, ir4qaViewJCas);
		}

		//set URI of Ir4qa Sofa
		SourceDocumentInformation ir4qaSofaSourceDocumentInformationFS = new SourceDocumentInformation(ir4qaViewJCas);
		ir4qaSofaSourceDocumentInformationFS.addToIndexes();
		ir4qaSofaSourceDocumentInformationFS.setUri(currentTopicElement.getID());
	}

	// 2012-03-26 実装済
	private void addDocument(generated.ir4qa.DOCUMENT documentElement, JCas ir4qaViewJCas) {
		Document documentFS = new Document(ir4qaViewJCas);
		documentFS.addToIndexes();
		documentFS.setRank(Integer.parseInt(documentElement.getRANK()));
		documentFS.setDocId(documentElement.getDOCID());
		documentFS.setScore(Float.parseFloat(documentElement.getSCORE()));
	}

	// 2012-03-26 実装済
	@Override
	public boolean hasNext() throws IOException, CollectionException {
		return !topicElementQueue.isEmpty();
	}

}
