package org.kachako.share.regex;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;


public class RegexCache
{
	private static class Key
	{
		public final String exp;
		public final int flg;
		public Key(String exp, int flg){
			this.exp = exp;
			this.flg = flg;
		}

		public boolean equals(Object obj){
			if(obj instanceof Key){
				Key key = (Key)obj;
				return exp.equals(key.exp) && flg == key.flg;
			}
			return false;
		}

		public int hashCode(){
			final int M = 37;
			int ans = 17;

			ans = M*ans + exp.hashCode();
			ans = M*ans + flg;

			return ans;
		}
	}

	private Map<Key, Pattern> mem = new HashMap<>();

	private Matcher matcher = null;

	public Pattern compile(String regex){
		return compile(regex, 0);
	}

	public Pattern compile(String regex, int flags){
		Key key = new Key(regex, flags);
		Pattern p = mem.get(key);
		if(p != null)
			return p;

		try {
			p = Pattern.compile(regex, flags);
		}
		catch(PatternSyntaxException e){
			e.printStackTrace();
			System.err.println(regex);
			throw e;
		}
		mem.put(key, p);
		return p;
	}

	public boolean matches(String regex, CharSequence input){
		matcher = compile(regex).matcher(input);
		return matcher.matches();
	}

	public boolean find(String regex, CharSequence input){
		if (input == null)
			return false;
		matcher = compile(regex).matcher(input);
		return matcher.find();
	}

	public String lastMatched(){
		return matcher.group();
	}

	public String lastMatched(int i){  
		return matcher.group(i);
	}

	public PartialApp appQuery(String query){
		return new PartialApp(this, query);
	}

	public static class PartialApp {

		private final RegexCache regCache;
		private final String str;

		public PartialApp(RegexCache regCache, String str){
			this.regCache	= regCache;
			this.str		= str;
		}

		public boolean matches(String regex){
			return regCache.matches(regex, str);
		}

		public boolean find(String regex){
			return regCache.find(regex, str);
		}

	}

}
