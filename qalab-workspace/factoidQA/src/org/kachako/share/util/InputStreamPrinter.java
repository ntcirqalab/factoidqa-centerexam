package org.kachako.share.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
   InputStreamを標準出力に流すユーティリティクラス
 */

public class InputStreamPrinter extends Thread {

	private int timeout = 1000;

	private InputStream is;
	private String header;

	public InputStreamPrinter(InputStream is){
		this(is, "");
	}

	public InputStreamPrinter(InputStream is, String header){
		super();
		this.is = is;
		this.header = header;
	}

	@Override
	public void run()
	{
		try{
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			try {
				long t = System.currentTimeMillis();
				while(true){
					if(!br.ready()){
						if(System.currentTimeMillis() - t >= timeout)
							break;
						Thread.sleep(10);
						continue;
					}
					String line = br.readLine();
					if(line == null) break;
System.err.print(header + " : ");
System.err.println(line);
					t = System.currentTimeMillis();
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
			finally {
				br.close();
			}
		}
		catch(IOException e){
			e.printStackTrace();
		}

	}

}