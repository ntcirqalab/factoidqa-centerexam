package org.kachako.ui.model.ssh;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashMap;


/**
 * 
 * 
 * @author 
 *
 */
public class ResourceLoader
{
	private static final ResourceLoader instance = new ResourceLoader();
	private LinkedHashMap<Path, File> memo = new LinkedHashMap<Path, File>();

	/**
	 * ResourceLoaderを構築します.
	 */
	private ResourceLoader() {
	}


	public static ResourceLoader getInstance() {
		return instance;
	}

	/**
	 * リソースファイルを取得します.
	 * 
	 * @param path
	 * @return
	 */
	public File getResourceFile(Path path)
	{
		if (memo.containsKey(path)) {
			return memo.get(path);
		}

		ClassLoader classLoader = ResourceLoader.class.getClassLoader();

		String[] nameParts = path.getFileName().toString().split("\\.", 2);
		String prefix = nameParts[0];
		String suffix = nameParts.length >= 2 ? "." + nameParts[1] : "";
		Path ret;
		try {
			ret = Files.createTempFile(prefix, suffix);
			ResourceFinalizer.getInstance().deleteOnExit(ret);
		}
		catch (IOException ioe) {
System.out.println("IOException error:"+ioe);
			return null;
		}

		OutputStream outputStream	= null;
		InputStream inputStream 	= null;
		try {
			outputStream = Files.newOutputStream(ret);
			// クラスパスのルートのパスを検索
			inputStream = classLoader.getResourceAsStream(path.toString());

			if(outputStream == null) {
				return null;
			}
			if(inputStream == null) {
				outputStream.close();
				return null;
			}

			byte[] buffer = new byte[1024];
			for (int length = inputStream.read(buffer); length >= 0; length = inputStream.read(buffer)) {
				outputStream.write(buffer, 0, length);
				outputStream.flush();
			}
		}
		catch(IOException ioe) {
System.out.println("IOException error:"+ioe);
			return null;
		}
		catch(Exception e) {
System.out.println("Exception error:"+e);
			return null;
		}
		finally {
			try {
				if(outputStream != null) {
					outputStream.close();
				}
				if(inputStream != null) {
					inputStream.close();
				}
			} catch (Exception e) {
				return null;
			}
		}

		File retFile = ret.toFile();
		memo.put(path, retFile);
		return retFile;
	}

}
