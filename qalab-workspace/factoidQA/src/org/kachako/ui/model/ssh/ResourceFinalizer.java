package org.kachako.ui.model.ssh;

import java.util.*;
import java.io.*;
import java.nio.file.*;

public class ResourceFinalizer {

	private ResourceFinalizer() {
		Runnable hook = new Runnable() {
			@Override public void run() {
				for (Path path : deletePaths) {
					try {
						deleteRecursively(path);
					}
					catch (Exception ex) {
					}
				}
			}
		};
		Runtime.getRuntime().addShutdownHook(new Thread(hook));
	}

	static private final ResourceFinalizer instance = new ResourceFinalizer();
	static public ResourceFinalizer getInstance() {
		return instance;
	}

	final private ArrayList<Path> deletePaths = new ArrayList<Path>();

	public void deleteOnExit(Path filePath) {
		deletePaths.add(filePath);
	}


	void deleteRecursively(Path path) throws IOException {
		if (Files.isDirectory(path)) {
			for (Path child : Files.newDirectoryStream(path)) {
				deleteRecursively(child);
			}
		}
		Files.deleteIfExists(path);
	}

}
