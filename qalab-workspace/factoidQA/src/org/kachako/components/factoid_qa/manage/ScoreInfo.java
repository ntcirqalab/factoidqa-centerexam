package org.kachako.components.factoid_qa.manage;

import java.util.TreeMap;

/**
 * スコア情報を管理する.
 *
 * @author JSA saitou
 *
 */
public class ScoreInfo
{
	/*
	 * ステージ１終了時点のスコア
	 */
	private TreeMap<Integer,Double> stage1	= new TreeMap<Integer,Double>();

	/*
	 * ステージ２終了時点のスコア
	 */
	private TreeMap<Integer,Double> stage2	=  new TreeMap<Integer,Double>();

	/*
	 * ステージ３終了時点のスコア
	 */
	private TreeMap<Integer,Double> stage3	= new TreeMap<Integer,Double>();

	/*
	 * ステージ４終了時点のスコア（最終スコア）
	 */
	private TreeMap<Integer,Double> stage4	= new TreeMap<Integer,Double>();

	/*
	 * ステージ４終了時点のスコア（最終スコア）のコピー(最終までいったものだけ)
	 */
	private TreeMap<Integer,Double> stageFinal	= new TreeMap<Integer,Double>();

	/*
	 * 2gram初期スコア
	 */
	private TreeMap<Integer,Double> c2gram	= new TreeMap<Integer,Double>();

	/*
	 * 2gram実スコア
	 */
	private TreeMap<Integer,Double> r2gram	= new TreeMap<Integer,Double>();

	/*
	 * キーワード初期スコア
	 */
	private TreeMap<Integer,Double> cKeyword	= new TreeMap<Integer,Double>();

	/*
	 * キーワード実スコア
	 */
	private TreeMap<Integer,Double> rKeyword	= new TreeMap<Integer,Double>();

	/*
	 * キーワード実スコアのトータル
	 */
	private TreeMap<Integer,Double> rKeywordTotal	= new TreeMap<Integer,Double>();

	/*
	 * 構文初期スコア
	 */
	private double cConst	= 0;

	/*
	 * 構文推定スコア
	 */
	private TreeMap<Integer,Double> pConst	= new TreeMap<Integer,Double>();

	/*
	 * 構文実スコア
	 */
	private TreeMap<Integer,Double> rConst	= new TreeMap<Integer,Double>();

	/*
	 * NE,num初期スコア
	 */
	private TreeMap<Integer,Double> cNe	= new TreeMap<Integer,Double>();

	/*
	 * NE,num推定スコア
	 */
	private TreeMap<Integer,Double> pNe	= new TreeMap<Integer,Double>();

	/*
	 * NE,num実スコア
	 */
	private TreeMap<Integer,Double> rNe	= new TreeMap<Integer,Double>();

	/*
	 * 文スコア（文内解候補のスコアの最大値）
	 */
	private double line	= 0;

	private TreeMap<Integer,Double> decided	= new TreeMap<Integer,Double>();

	public ScoreInfo() {
	}

	/**
	 * ステージ１終了時点のスコアを取得する.
	 *
	 * @return ステージ１終了時点のスコア
	 */
	public TreeMap<Integer,Double> getStage1()
	{
		return stage1;
	}

	/**
	 * ステージ１終了時点のスコアを設定する.
	 *
	 * @param score スコア
	 */
	public void setStage1(TreeMap<Integer,Double> score)
	{
		this.stage1	= score;
	}

	/**
	 * ステージ２終了時点のスコアを取得する.
	 *
	 * @return ステージ２終了時点のスコア
	 */
	public TreeMap<Integer,Double> getStage2()
	{
		return stage2;
	}

	/**
	 * ステージ２終了時点のスコアを設定する.
	 *
	 * @param score スコア
	 */
	public void setStage2(TreeMap<Integer,Double> score)
	{
		this.stage2	= score;
	}

	/**
	 * ステージ３終了時点のスコアを取得する.
	 *
	 * @return ステージ３終了時点のスコア
	 */
	public TreeMap<Integer,Double> getStage3()
	{
		return stage3;
	}

	/**
	 * ステージ３終了時点のスコアを設定する.
	 *
	 * @param score スコア
	 */
	public void setStage3(TreeMap<Integer,Double> score)
	{
		this.stage3	= score;
	}

	/**
	 * ステージ４終了時点のスコアを取得する.
	 *
	 * @return ステージ４終了時点のスコア
	 */
	public TreeMap<Integer,Double> getStage4()
	{
		return stage4;
	}

	/**
	 * ステージ４終了時点のスコアを設定する.
	 *
	 * @param score スコア
	 */
	public void setStage4(TreeMap<Integer,Double> score)
	{
		this.stage4	= score;
	}

	/**
	 * ステージ４終了時点のスコア（最終スコア）のコピーを取得する.
	 *
	 * @return ステージ４終了時点のスコア（最終スコア）のコピー
	 */
	public TreeMap<Integer,Double> getStageFinal()
	{
		return stageFinal;
	}

	/**
	 * ステージ４終了時点のスコア（最終スコア）のコピーを設定する.
	 *
	 * @param score スコア
	 */
	public void setStageFinal(TreeMap<Integer,Double> score)
	{
		this.stageFinal	= score;
	}

	/**
	 * 2gram初期スコアを取得する.
	 *
	 * @return 2gram初期スコア
	 */
	public TreeMap<Integer,Double> getC2gram()
	{
		return c2gram;
	}

	/**
	 * 2gram初期スコアを設定する.
	 *
	 * @param score スコア
	 */
	public void setC2gram(TreeMap<Integer,Double> score)
	{
		this.c2gram	= score;
	}

	/**
	 * 2gram実スコアを取得する.
	 *
	 * @return 2gram実スコア
	 */
	public TreeMap<Integer,Double> getR2gram()
	{
		return r2gram;
	}

	/**
	 * 2gram実スコアを設定する.
	 *
	 * @param score スコア
	 */
	public void setR2gram(TreeMap<Integer,Double> score)
	{
		this.r2gram	= score;
	}

	/**
	 * キーワード初期スコアを取得する.
	 *
	 * @return キーワード初期スコア
	 */
	public TreeMap<Integer,Double> getCkeyword()
	{
		return cKeyword;
	}

	/**
	 * キーワード初期スコアを設定する.
	 *
	 * @param score スコア
	 */
	public void setCkeyword(TreeMap<Integer,Double> score)
	{
		this.cKeyword	= score;
	}

	/**
	 * キーワード実スコアを取得する.
	 *
	 * @return キーワード実スコア
	 */
	public TreeMap<Integer,Double> getRkeyword()
	{
		return rKeyword;
	}

	/**
	 * キーワード実スコアを設定する.
	 *
	 * @param score スコア
	 */
	public void setRkeyword(TreeMap<Integer,Double> score)
	{
		this.rKeyword	= score;
	}

	/**
	 * 全体のキーワード実スコアを取得する.
	 *
	 * @return キーワード実スコア
	 */
	public TreeMap<Integer,Double> getRkeywordTotal()
	{
		return rKeywordTotal;
	}

	/**
	 * キーワード実スコアを設定する.
	 *
	 * @param score スコア
	 */
	public void setRkeywordTotal(TreeMap<Integer,Double> score)
	{
		this.rKeywordTotal	= score;
	}

	/**
	 * 構文初期スコアを取得する.
	 *
	 * @return 構文初期スコア
	 */
	public double getCconst()
	{
		return cConst;
	}

	/**
	 * 構文初期スコアを設定する.
	 *
	 * @param score スコア
	 */
	public void setCconst(double score)
	{
		this.cConst	= score;
	}

	/**
	 * 構文推定スコアを取得する.
	 *
	 * @return 構文推定スコア
	 */
	public TreeMap<Integer,Double> getPconst()
	{
		return pConst;
	}

	/**
	 * 構文推定スコアを設定する.
	 *
	 * @param score スコア
	 */
	public void setPconst(TreeMap<Integer,Double> score)
	{
		this.pConst	= score;
	}

	/**
	 * 構文実スコアを取得する.
	 *
	 * @return 構文推定スコア
	 */
	public TreeMap<Integer,Double> getRconst()
	{
		return rConst;
	}

	/**
	 * 構文実スコアを設定する.
	 *
	 * @param score スコア
	 */
	public void setRconst(TreeMap<Integer,Double> score)
	{
		this.rConst	= score;
	}

	/**
	 * NE,num初期スコアを取得する.
	 *
	 * @return NE,num初期スコア
	 */
	public TreeMap<Integer,Double> getCne()
	{
		return cNe;
	}

	/**
	 * NE,num初期スコアを設定する.
	 *
	 * @param score スコア
	 */
	public void setCne(TreeMap<Integer,Double> score)
	{
		this.cNe	= score;
	}

	/**
	 * NE,num推定スコアを取得する.
	 *
	 * @return NE,num推定スコア
	 */
	public TreeMap<Integer,Double> getPne()
	{
		return pNe;
	}

	/**
	 * NE,num推定スコアを設定する.
	 *
	 * @param score スコア
	 */
	public void setPne(TreeMap<Integer,Double> score)
	{
		this.pNe	= score;
	}

	/**
	 * NE,num実スコアを取得する.
	 *
	 * @return NE,num実スコア
	 */
	public TreeMap<Integer,Double> getRne()
	{
		return rNe;
	}

	/**
	 * NE,num実スコアを設定する.
	 *
	 * @param score スコア
	 */
	public void setRne(TreeMap<Integer,Double> score)
	{
		this.rNe	= score;
	}

	/**
	 * 文スコア（文内解候補のスコアの最大値）を取得する.
	 *
	 * @return 文スコア（文内解候補のスコアの最大値）
	 */
	public double getLine()
	{
		return line;
	}

	/**
	 * 文スコア（文内解候補のスコアの最大値）を設定する.
	 *
	 * @param score スコア
	 */
	public void setLine(double score)
	{
		this.line	= score;
	}

	public TreeMap<Integer,Double> getDecided()
	{
		return decided;
	}

	public void setDecided(TreeMap<Integer,Double> decided)
	{
		this.decided	= decided;
	}

}
