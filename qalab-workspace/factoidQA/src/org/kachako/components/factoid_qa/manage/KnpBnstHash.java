package org.kachako.components.factoid_qa.manage;

import java.util.HashMap;

public class KnpBnstHash
{
	private HashMap<Integer, KnpBnstHash> bnstMap	= new HashMap<Integer, KnpBnstHash>();

	public KnpBnstHash() {
	}

	public KnpBnstHash(int bnstNum, KnpBnstHash hash) {
		bnstMap.put(bnstNum, hash);
	}

	public KnpBnstHash getKnpBnstHash(int bnstNum) {
		if(bnstMap.containsKey(bnstNum)) {
			return bnstMap.get(bnstNum);			
		}
		return null;
	}

	public void setKnpBnstHash(int bnstNum, KnpBnstHash hash) {
		bnstMap.put(bnstNum, hash);
	}

	public HashMap<Integer, KnpBnstHash> getBnstMap() {
		return bnstMap;
	}
}
