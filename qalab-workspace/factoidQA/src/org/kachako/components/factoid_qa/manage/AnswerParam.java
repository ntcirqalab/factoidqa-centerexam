package org.kachako.components.factoid_qa.manage;

/**
 * 解生成のためのパラメータを管理する.
 * 
 * @author JSA Saitou
 *
 */
public class AnswerParam
{
	// 構文距離スコア（最大値）
	private double relation;
	// 疑問詞、解候補の格一致（＊キーワード重要度の総和、最大値）
	private double wkaku;
	// キーワードの格一致（最大値）
	private double kaku;
	// カラ格、マデ格以外（乗算）
	private double karamade;
	// 括弧内（＊キーワード重要度の総和、最大値）
	private double kakko;
	private double btok;
	// 数量表現に与える最低点の比率
	private double num;
	// 単純な枝刈りまたはビームサーチのときの閾値
	private double border;

	private String sPart;
	private String sWord;

	private double cBigram;
	private double rBigram;

	// NE,num初期スコア
	private double cNe;
	// NE,num推定スコア
	private double preNe;
	// NE,num実スコア
	private double realNe;

	// 文字スコアのパラメータ
	private double charaScoreKeyword;
	private double charaScoreConst;

	// 推定スコアのパラメータ
	private double preScoreConst;

	// 実スコアのパラメータ
	private double realScoreKeyword;
	private double realScoreConst;

	// 最大スコアのパラメータ
	private double alp0;	// キーワード
	private double alp1;	// 係り受け
	private double alp2;	// 質問型

	private int neMode;		// NE/num/other
	private String neType;	// 質問型

	// 探索制御モード
	private int mode;

	public AnswerParam() {

	}

	public double getRelation()
	{
		return relation;
	}

	public void setRelation(double relation)
	{
		this.relation	=  relation;
	}

	public double getWkaku()
	{
		return wkaku;
	}

	public void setWkaku(double wkaku)
	{
		this.wkaku	=  wkaku;
	}

	public double getKaku()
	{
		return kaku;
	}

	public void setKaku(double kaku)
	{
		this.kaku	=  kaku;
	}

	public double getKaramade()
	{
		return karamade;
	}

	public void setKaramade(double karamade)
	{
		this.karamade	=  karamade;
	}

	public double getKakko()
	{
		return kakko;
	}

	public void setKakko(double kakko)
	{
		this.kakko	=  kakko;
	}

	public double getBtok()
	{
		return btok;
	}

	public void setBtok(double btok)
	{
		this.btok	=  btok;
	}

	public double getNum()
	{
		return num;
	}

	public void setNum(double num)
	{
		this.num	=  num;
	}

	public double getBorder()
	{
		return border;
	}

	public void setBorder(double border)
	{
		this.border	=  border;
	}

	public String getSPart()
	{
		return sPart;
	}

	public void setSPart(String sPart)
	{
		this.sPart	=  sPart;
	}

	public String getSWord()
	{
		return sWord;
	}

	public void setSWord(String sWord)
	{
		this.sWord	=  sWord;
	}

	public double getCBigram()
	{
		return cBigram;
	}

	public void setCBigram(double cBigram)
	{
		this.cBigram	=  cBigram;
	}

	public double getRealBigram()
	{
		return rBigram;
	}

	public void setRealBigram(double rBigram)
	{
		this.rBigram	=  rBigram;
	}

	public double getCNe()
	{
		return cNe;
	}

	public void setCNe(double cNe)
	{
		this.cNe	=  cNe;
	}

	public double getPreNe()
	{
		return preNe;
	}

	public void setPreNe(double preNe)
	{
		this.preNe	=  preNe;
	}

	public double getRealNe()
	{
		return realNe;
	}

	public void setRealNe(double realNe)
	{
		this.realNe	=  realNe;
	}

	public double getCharaScoreKeyword()
	{
		return charaScoreKeyword;
	}

	public void setCharaScoreKeyword(double charaScoreKeyword)
	{
		this.charaScoreKeyword	=  charaScoreKeyword;
	}

	public double getCharaScoreConst()
	{
		return charaScoreConst;
	}

	public void setCharaScoreConst(double charaScoreConst)
	{
		this.charaScoreConst	=  charaScoreConst;
	}

	public double getPreScoreConst()
	{
		return preScoreConst;
	}

	public void setPreScoreConst(double preScoreConst)
	{
		this.preScoreConst	=  preScoreConst;
	}

	public double getRealScoreKeyword()
	{
		return realScoreKeyword;
	}

	public void setRealScoreKeyword(double realScoreKeyword)
	{
		this.realScoreKeyword	=  realScoreKeyword;
	}

	public double getRealScoreConst()
	{
		return realScoreConst;
	}

	public void setRealScoreConst(double realScoreConst)
	{
		this.realScoreConst	=  realScoreConst;
	}

	public double getAlp0()
	{
		return alp0;
	}

	public void setAlp0(double alp0)
	{
		this.alp0	=  alp0;
	}

	public double getAlp1()
	{
		return alp1;
	}

	public void setAlp1(double alp1)
	{
		this.alp1	=  alp1;
	}

	public double getAlp2()
	{
		return alp2;
	}

	public void setAlp2(double alp2)
	{
		this.alp2	=  alp2;
	}

	public int getNeMode()
	{
		return neMode;
	}

	public void setNeMode(int neMode)
	{
		this.neMode	=  neMode;
	}

	public String getNeType()
	{
		return neType;
	}

	public void setNeType(String neType)
	{
		this.neType	=  neType;
	}

	public int getMode()
	{
		return mode;
	}

	public void setMode(int mode)
	{
		this.mode	=  mode;
	}

}
