package org.kachako.components.factoid_qa.manage;

import java.util.List;
import java.util.Map;

import org.kachako.types.japanese.syntactic.KnpSegment;
import org.kachako.types.japanese.syntactic.KyotoDependency;
import org.kachako.types.japanese.syntactic.morpheme.Morpheme;


/**
 * 検索パッセージの一文の情報を管理する.
 *
 * @author JSA saitou
 *
 */
public class LineInfo
{
	/*
	 * パッセージ中の文
	 */
	private String line	= null;

	/*
	 * パッセージ中の文（複数文を考慮）
	 */
	private String lineL0	= null;

	/*
	 * 質問文キーワードの出現数
	 */
	private List<Integer> keycount	= null;

	/*
	 * 質問文キーワードの出現数（複数文を考慮）
	 */
	private List<Integer> keycountL0	= null;

	/*
	 * 質問文キーワードの出現数（パッセージ先頭までの和）
	 */
	private List<Integer> keycountTotal	= null;

	/*
	 * 処理レベルの経過
	 */
	private int level	= 0;

	/*
	 * 形態素解析結果
	 */
	private List<Morpheme> morphemeList	= null;

	/*
	 * パッセージ内全文の形態素解析結果
	 */
	private List<Morpheme> morphemeTotalList	= null;

	/*
	 * 形態素の表層表現
	 */
	private List<String> morphemeHyosoList	= null;

	/*
	 * パッセージ内全形態素の表層表現
	 */
	private List<String> morphemeHyosoTotalList	= null;

	/*
	 * 形態素解析結果から得られるキーワードのリスト
	 */
	private List<KeyWord> keyWordList	= null;

	/*
	 * 形態素解析結果から得られるキーワードのパッセージのトータルリスト
	 */
	private List<KeyWord> keyWordTotalList	= null;

	/*
	 * 構文解析結果：文節のリスト
	 */
	private List<KnpSegment> knpSegmentList	= null;

	/*
	 * 構文解析結果：パッセージ内全文節のリスト
	 */
	private List<KnpSegment> knpSegmentTotalList	= null;

	/*
	 * 構文解析結果の形態素情報
	 */
	private Map<KnpSegment,List<Morpheme>> knpInfoMap	= null;

	/*
	 * 構文解析結果の形態素情報（パッセージ内全体）
	 */
	private Map<KnpSegment,List<Morpheme>> knpInfoTotalMap	= null;

	/*
	 * 係り受け関係
	 */
	private List<KyotoDependency> kyotoDependencyList	= null;

	/*
	 * 係り受け関係（パッセージ内全体）
	 */
	private List<KyotoDependency> kyotoDependencyTotalList	= null;

	/**
	 * KNPの構文木
	 */
//	private HashMap<Integer,KnpBnstTree> knpHash	= null;
	private KnpBnstHash knpHash	= null;

	/*
	 * 文節区切りの推定
	 */
	private List<String> pBnst	= null;

	/*
	 * 行番号
	 */
	private int lineNo	= 0;

	/*
	 * パッセージ先頭の行番号
	 */
	private int startLineNo	= 0;

	/*
	 * 文書番号(URL)
	 */
	private String source	= null;

	/*
	 * 対応する数量表現の出現数
	 */
	private List<Integer> neTotal	= null;

	/*
	 * 複数文照合をするか否かのフラグ
	 */
	private boolean multiconst	= false;

	/*
	 * 形態素情報にNEタイプを付加したリスト
	 */
	private List<String> neDataList	= null;


	public LineInfo() {
	}

	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line	= line;
	}

	public String getLineL0() {
		return lineL0;
	}

	public void setLineL0(String lineL0) {
		this.lineL0	= lineL0;
	}

	public List<Integer> getKeycount() {
		return keycount;
	}

	public void setKeycount(List<Integer> keycount) {
		this.keycount	= keycount;
	}

	public List<Integer> getKeycountL0() {
		return keycountL0;
	}

	public void setKeycountL0(List<Integer> keycountL0) {
		this.keycountL0	= keycountL0;
	}

	public List<Integer> getKeycountTotal() {
		return keycountTotal;
	}

	public void setKeycountTotal(List<Integer> keycountTotal) {
		this.keycountTotal	= keycountTotal;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level	= level;
	}

	public List<Morpheme> getMorphemeList() {
		return morphemeList;
	}

	public void setMorphemeList(List<Morpheme> morphemeList) {
		this.morphemeList	= morphemeList;
	}

	public List<Morpheme> getMorphemeTotalList() {
		return morphemeTotalList;
	}

	public void setMorphemeTotalList(List<Morpheme> morphemeTotalList) {
		this.morphemeTotalList	= morphemeTotalList;
	}

	public List<String> getMorphemeHyosoList() {
		return morphemeHyosoList;
	}

	public void setMorphemeHyosoList(List<String> morphemeHyosoList) {
		this.morphemeHyosoList	= morphemeHyosoList;
	}

	public List<String> getMorphemeHyosoTotalList() {
		return morphemeHyosoTotalList;
	}

	public void setMorphemeHyosoTotalList(List<String> morphemeHyosoTotalList) {
		this.morphemeHyosoTotalList	= morphemeHyosoTotalList;
	}

	public List<KeyWord> getKeyWordList() {
		return keyWordList;
	}

	public void setKeyWordList(List<KeyWord> keyWordList) {
		this.keyWordList	= keyWordList;
	}

	public List<KeyWord> getKeyWordTotalList() {
		return keyWordTotalList;
	}

	public void setKeyWordTotalList(List<KeyWord> keyWordTotalList) {
		this.keyWordTotalList	= keyWordTotalList;
	}

	public List<KnpSegment> getKnpSegmentList() {
		return knpSegmentList;
	}

	public void setKnpSegmentList(List<KnpSegment> knpSegmentList) {
		this.knpSegmentList	= knpSegmentList;
	}

	public List<KnpSegment> getKnpSegmentTotalList() {
		return knpSegmentTotalList;
	}

	public void setKnpSegmentTotalList(List<KnpSegment> knpSegmentTotalList) {
		this.knpSegmentTotalList	= knpSegmentTotalList;
	}

	public Map<KnpSegment,List<Morpheme>> getKnpInfoMap() {
		return knpInfoMap;
	}

	public void setKnpInfoMap(Map<KnpSegment,List<Morpheme>> knpInfoMap) {
		this.knpInfoMap	= knpInfoMap;
	}

	public Map<KnpSegment,List<Morpheme>> getKnpInfoTotalMap() {
		return knpInfoTotalMap;
	}

	public void setKnpInfoTotalMap(Map<KnpSegment,List<Morpheme>> knpInfoTotalMap) {
		this.knpInfoTotalMap	= knpInfoTotalMap;
	}

	public List<KyotoDependency> getKyotoDependencyList() {
		return kyotoDependencyList;
	}

	public void setKyotoDependencyList(List<KyotoDependency> kyotoDependencyList) {
		this.kyotoDependencyList	= kyotoDependencyList;
	}

	public List<KyotoDependency> getKyotoDependencyTotalList() {
		return kyotoDependencyTotalList;
	}

	public void setKyotoDependencyTotalList(List<KyotoDependency> kyotoDependencyTotalList) {
		this.kyotoDependencyTotalList	= kyotoDependencyTotalList;
	}

	public KnpBnstHash getKnpHash() {
		return knpHash;
	}

	public void setKnpHash(KnpBnstHash knpHash) {
		this.knpHash	= knpHash;
	}

	public List<String> getPbnst() {
		 return pBnst;
	}

	public void setPbnst(List<String> pBnst) {
		 this.pBnst	= pBnst;
	}

	public int getLineNo() {
		return lineNo;
	}

	public void setLineNo(int lineNo) {
		this.lineNo	= lineNo;
	}

	public int getStartLineNo() {
		return startLineNo;
	}

	public void setStartLineNo(int startLineNo) {
		this.startLineNo	= startLineNo;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source	= source;
	}

	public List<Integer> getNeTotal() {
		return neTotal;
	}

	public void setNeTotal(List<Integer> neTotal) {
		this.neTotal	= neTotal;
	}

	public boolean getMulticonst() {
		return multiconst;
	}

	public void setMulticonst(boolean multiconst) {
		this.multiconst	= multiconst;
	}

	public List<String> getNeDataList() {
		return neDataList;
	}

	public void setNeDataList(List<String> neDataList) {
		this.neDataList	= neDataList;
	}

}
