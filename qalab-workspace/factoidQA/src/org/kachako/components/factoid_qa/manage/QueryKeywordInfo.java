package org.kachako.components.factoid_qa.manage;

public class QueryKeywordInfo
{
	/*
	 * キーワード
	 */
	private String word	= null;

	/*
	 * キーワードの形態素番号
	 */
	private int location	= 0;

	/*
	 * キーワードの種類
	 */
	private String part	= null;

	private int relDown	= 0;
	private int relUp	= 0;
	private int rel2Down	= 0;
	private int rel2Up	= 0;
	private String kaku	= null;
	private String wKaku	= null;
	private double weight	= 0;
	private String yougen	= null;

	/*
	 * KNPの文節番号
	 */
	private int bnstNo	= 0;

	/*
	 * KNPの文節内形態素番号
	 */
	private int bnstMorphemeNo	= 0;


	public QueryKeywordInfo() {
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word	= word;
	}

	public int getLocation() {
		return location;
	}

	public void setLocation(int location) {
		this.location	= location;
	}

	public String getPart() {
		return part;
	}

	public void setPart(String part) {
		this.part	= part;
	}

	public int getRelDown() {
		return relDown;
	}

	public void setRelDown(int relDown) {
		this.relDown	= relDown;
	}

	public int getRelUp() {
		return relUp;
	}

	public void setRelUp(int relUp) {
		this.relUp	= relUp;
	}

	public int getRel2Down() {
		return rel2Down;
	}

	public void setRel2Down(int rel2Down) {
		this.rel2Down	= rel2Down;
	}

	public int getRel2Up() {
		return rel2Up;
	}

	public void setRel2Up(int rel2Up) {
		this.rel2Up	= rel2Up;
	}

	public String getKaku() {
		return kaku;
	}

	public void setKaku(String kaku) {
		this.kaku	= kaku;
	}

	public String getWkaku() {
		return wKaku;
	}

	public void setWkaku(String wKaku) {
		this.wKaku	= wKaku;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight	= weight;
	}

	public int getBnstNo() {
		return bnstNo;
	}

	public void setBnstNo(int bnstNo) {
		this.bnstNo	= bnstNo;
	}

	public int getBnstMorphemeNo() {
		return bnstMorphemeNo;
	}

	public void setBnstMorphemeNo(int bnstMorphemeNo) {
		this.bnstMorphemeNo	= bnstMorphemeNo;
	}

	public String getYougen() {
		return yougen;
	}

	public void setYougen(String yougen) {
		this.yougen	= yougen;
	}

}
