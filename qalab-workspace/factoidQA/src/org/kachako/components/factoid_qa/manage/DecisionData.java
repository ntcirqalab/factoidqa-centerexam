package org.kachako.components.factoid_qa.manage;

/**
 * 決定リストのデータを管理する.
 * 
 * @author JSA saitou
 *
 */
public class DecisionData
{
	private String brach	= null;

	private String evidence	= null;

	private int right		= 0;

	private int miss		= 0;

	private double llh		= 0;

	public DecisionData(String brach,String evidence,int right,int miss,double llh)
	{
		this.brach	= brach;
		this.evidence	= evidence;
		this.right		= right;
		this.miss		= miss;
		this.llh		= llh;
	}

	public DecisionData() {
	}

	public String getBranch() {
		return brach;
	}

	public void setBranch(String brach) {
		this.brach	= brach;
	}

	public String getEvidence() {
		return evidence;
	}

	public void setEvidence(String evidence) {
		this.evidence	= evidence;
	}

	public int getRight() {
		return right;
	}

	public void setRight(int right) {
		this.right	= right;
	}

	public int getMiss() {
		return miss;
	}

	public void setMiss(int miss) {
		this.miss	= miss;
	}

	public double getLlh() {
		return llh;
	}

	public void setLlh(double llh) {
		this.llh	= llh;
	}

}
