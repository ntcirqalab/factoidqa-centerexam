package org.kachako.components.factoid_qa.manage;

import java.util.List;

/**
 * クエリ情報を管理する.
 * 
 * @author JSA saitou
 *
 */
public class QueryInfo
{
	private List<QueryKeywordInfo> keywordInfos	= null;
	private String type		= null;
	private String pattern1	= null;
	private String pattern2	= null;
	private String[] gramList	= null;
	private String objectWord	= null;
	private String objectSplt	= null;
	private int objectLength	= 0;
	private String attributeWord	= null;
	private String attributeSplt	= null;
	private int attributeLength	= 0;
	private KnpBnstHash knpHash	= null;

	public QueryInfo() {
	}

	public List<QueryKeywordInfo> getKeyWordInfos() {
		return keywordInfos;
	}

	public void setKeyWordInfos(List<QueryKeywordInfo> keywordInfos) {
		this.keywordInfos	= keywordInfos;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type	= type;
	}

	public String getPattern1() {
		return pattern1;
	}

	public void setPattern1(String pattern1) {
		this.pattern1	= pattern1;
	}

	public String getPattern2() {
		return pattern2;
	}

	public void setPattern2(String pattern2) {
		this.pattern2	= pattern2;
	}

	public String[] get2gramList() {
		return gramList;
	}

	public void set2gramList(String[] gramList) {
		this.gramList	= gramList;
	}

	public String getObjectWord() {
		return objectWord;
	}

	public void setObjectWord(String word) {
		this.objectWord	= word;
	}

	public String getObjectSplt() {
		return objectSplt;
	}

	public void setObjectSplt(String splt) {
		this.objectSplt	= splt;
	}

	public int getObjectLength() {
		return objectLength;
	}

	public void setObjectLength(int len) {
		this.objectLength	= len;
	}

	public String getAttributeWord() {
		return attributeWord;
	}

	public void setAttributeWord(String word) {
		this.attributeWord	= word;
	}

	public String getAttributeSplt() {
		return attributeSplt;
	}

	public void setAttributeSplt(String splt) {
		this.attributeSplt	= splt;
	}

	public int getAttributeLength() {
		return attributeLength;
	}

	public void setAttributeLength(int len) {
		this.attributeLength	= len;
	}

	public KnpBnstHash getKnpHash() {
		return knpHash;
	}

	public void setKnpHash(KnpBnstHash knpHash) {
		this.knpHash	= knpHash;
	}
}
