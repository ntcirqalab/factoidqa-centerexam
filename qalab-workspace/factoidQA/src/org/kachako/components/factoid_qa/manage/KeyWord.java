package org.kachako.components.factoid_qa.manage;

/**
 * キーワード情報を管理する.
 * 
 * @author JSA saitou
 *
 */
public class KeyWord
{
	/*
	 * キーワード
	 */
	private String word	= null;

	/*
	 * キーワードの形態素番号
	 */
	private int location	= 0;

	/*
	 * キーワードの次の形態素番号
	 */
	private int locationE	= 0;

	/*
	 * キーワードの要素
	 */
	private String part	= null;

	/*
	 * 
	 */
	private String org	= null;

	public KeyWord(String word, int location, int locationE, String part, String org)
	{
		this.word = word;
		this.location = location;
		this.locationE = locationE;
		this.part = part;
		this.org = org;
	}

	public KeyWord(String word, int begin, int end, String termType)
	{
		this(word, begin, end, termType, null);
	}

	public String getWord() {
		return word;
	}

	public int getLocation() {
		return location;
	}

	public int getLocationE() {
		return locationE;
	}

	public String getPart() {
		return part;
	}

	public String getOrg() {
		return org;
	}

}
