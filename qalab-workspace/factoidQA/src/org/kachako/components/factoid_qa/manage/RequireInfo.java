package org.kachako.components.factoid_qa.manage;

/**
 *  質問文（クエリ）から抽出すべき情報を管理する.
 * 
 * @author JSA Saitou
 *
 */
public class RequireInfo
{
	/*
	 * 情報の種類
	 */
	private String type		= null;

	/*
	 * 固有表現情報
	 */
	private String qword	= null;

	/*
	 * パターン1
	 */
	private String pattern1	= null;

	/*
	 * パターン2
	 */
	private String pattern2	= null;


	public RequireInfo(String type, String qword, String pattern1, String pattern2)
	{
		this.type = type;
		this.qword = qword;
		this.pattern1 = pattern1;
		this.pattern2 = pattern2;
	}

	public RequireInfo(String type, String qword, String pattern1) {
		this(type, qword, pattern1, null);
	}

	public RequireInfo(String type, String qword) {
		this(type, qword, null, null);
	}

	public RequireInfo() {
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type	= type;
	}

	public String getQword() {
		return qword;
	}

	public void setQword(String qword) {
		this.qword	= qword;
	}

	public String getPattern1() {
		return pattern1;
	}

	public void setPattern1(String pattern1) {
		this.pattern1	= pattern1;
	}

	public String getPattern2() {
		return pattern2;
	}

	public void setPattern2(String pattern2) {
		this.pattern2	= pattern2;
	}
}
