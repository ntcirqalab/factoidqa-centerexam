package org.kachako.components.factoid_qa.module;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.apache.uima.jcas.cas.StringArray;
import org.kachako.components.factoid_qa.manage.KnpBnstHash;
import org.kachako.components.factoid_qa.manage.QueryInfo;
import org.kachako.components.factoid_qa.manage.QueryKeywordInfo;
import org.kachako.share.regex.RegexCache;
import org.kachako.types.japanese.syntactic.KnpSegment;
import org.kachako.types.japanese.syntactic.KyotoDependency;
import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

public class CwqGetRelation04
{
	/* 各種精度向上手法を使うかのスイッチ */
	// 係受け構造照合で疑問文の係受構造変換規則を使わない
	private static final boolean NO_TRANS_RULES = false;

	private static final RegexCache regexCache = new RegexCache();

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	/**
	 * クエリーを何かする??
	 * 
	 * @param queryInfo
	 * @param kyotoDependencyList
	 * @param knpSegmentList
	 * @param knpInfoMap
	 * @param sentence
	 * @return
	 */
	public static QueryInfo getRelationQuery(QueryInfo queryInfo,
												List<KyotoDependency> kyotoDependencyList,
												List<KnpSegment> knpSegmentList,
												Map<KnpSegment,List<Morpheme>> knpInfoMap,
												String sentence)
	{
		QueryInfo tempQueryInfo	= queryInfo;
		List<QueryKeywordInfo> keywordInfos	= tempQueryInfo.getKeyWordInfos();
//		HashMap<Integer,KnpBnstTree> knpHash	= tempQueryInfo.getKnpHash();
		KnpBnstHash knpHash	= tempQueryInfo.getKnpHash();

		int bnstNum	= -1;
		int bustMorphemeNum	= -1;
		int i	= 0;
		LOOP:
		for(KnpSegment knpSegment : knpInfoMap.keySet()) {
			List<Morpheme>	morphemeList	= knpInfoMap.get(knpSegment);
			//String text	= knpSegment.getCoveredText();
			for(int j = 0; j < morphemeList.size(); j++) {
				String str	= morphemeList.get(j).getSurfaceForm();
				if(regexCache.find("^(何|いつ|誰|だれ|な[んに]|いくら?|ど[れのうこ]|どんな)",str)) {
					// 解候補が指定されていないときは疑問詞を対象とする
					bnstNum	= i;
					bustMorphemeNum	= j;
					break LOOP;
				}
			}
			i++;
		}
		if(bnstNum < 0 && bustMorphemeNum < 0) {
			// 疑問詞がない場合は、文末に疑問詞があると仮定する。
			if(knpSegmentList.size() > 0) {
				bnstNum	= knpSegmentList.size()-1;
				bustMorphemeNum	= knpInfoMap.get(knpSegmentList.get(knpSegmentList.size()-1)).size();				
			}
			else {
				bnstNum	= 0;
				bustMorphemeNum	= 0;
			}
		}

		List<QueryKeywordInfo> tempKeywordInfos	= getRelation(keywordInfos,kyotoDependencyList,knpSegmentList,
																knpHash,bnstNum,bustMorphemeNum);

		String q1 = "((だった|であ(る|った)|という)?の?(で(す|しょう|あろう|した)|だ(ろう|った))?)?か";
		String q2 = "((といいま(す|した)|といった|にあ(る|ります))の?(で(す|しょう|あろう|した)|だ(ろう|った))?)?か";

		if(regexCache.find("(名前?|国|都市|会社|人.?)は(何処?|なん|いつ|どこ|誰|だれ)"+q1,sentence)) {
			// Aの名前は何ですか -> XはAです。
			for(int k = 0; k < tempKeywordInfos.size(); k++) {
				QueryKeywordInfo keywordInfo	= tempKeywordInfos.get(k);
				int relUp	= keywordInfo.getRelUp();
				int relDown	= keywordInfo.getRelDown();
				if(!NO_TRANS_RULES && relUp >= 0 && relDown < 0) {
					if(relUp > 2) {
						keywordInfo.setRel2Up(relUp-2);
						keywordInfo.setRel2Down(1);
					}
					else {
						keywordInfo.setRel2Up(0);
						keywordInfo.setRel2Down(1);
					}
				}
				else {
					keywordInfo.setRel2Up(relUp);
					keywordInfo.setRel2Down(relDown);
				}
				tempKeywordInfos.set(k, keywordInfo);
			}
		}
		else if(regexCache.find("は(何処?|なん|いつ|どこ|誰|だれ)"+q1,sentence)) {
			// Aは何ですか -> XはAです。
			for(int k = 0; k < tempKeywordInfos.size(); k++) {
				QueryKeywordInfo keywordInfo	= tempKeywordInfos.get(k);
				int relUp	= keywordInfo.getRelUp();
				int relDown	= keywordInfo.getRelDown();
				if(!NO_TRANS_RULES && relUp >= 0 && relDown < 0) {
					if(relUp >  1 ) {
						keywordInfo.setRel2Up(relUp-1);
						keywordInfo.setRel2Down(1);
					}
					else {
						keywordInfo.setRel2Up(0);
						keywordInfo.setRel2Down(1);
					}
				}
				else {
					keywordInfo.setRel2Up(relUp);
					keywordInfo.setRel2Down(relDown);
				}
				tempKeywordInfos.set(k, keywordInfo);
			}
		}
		else if(regexCache.find("(何処?|なん|いつ|どこ?|誰|だれ)の(.+?)"+q1,sentence) 
				&& regexCache.find("(こと|もの|作品|会社|企業|都市|[国県市町村人])",regexCache.lastMatched(2))) {
			// AはだれのBですか -> XのBはAです。
			for(int k = 0; k < tempKeywordInfos.size(); k++) {
				QueryKeywordInfo keywordInfo	= tempKeywordInfos.get(k);
				int relUp	= keywordInfo.getRelUp();
				int relDown	= keywordInfo.getRelDown();
				if(!NO_TRANS_RULES && relUp >= 0 && relDown == 1) {
					if(relUp >  1 ) {
						keywordInfo.setRel2Up(relUp-1);
						keywordInfo.setRel2Down(2);
					}
					else {
						keywordInfo.setRel2Up(0);
						keywordInfo.setRel2Down(2);
					}
				}
				else {
					keywordInfo.setRel2Up(relUp);
					keywordInfo.setRel2Down(relDown);
				}
				tempKeywordInfos.set(k, keywordInfo);
			}
		}
		else if(regexCache.find("(名前|国|都市|会社|人.?)は(何処?|なん|いつ|どこ|誰|だれ)"+q2,sentence)) { 
			// Aの名前は何といいますか -> AはXです。
			for(int k = 0; k < tempKeywordInfos.size(); k++) {
				QueryKeywordInfo keywordInfo	= tempKeywordInfos.get(k);
				int relUp	= keywordInfo.getRelUp();
				int relDown	= keywordInfo.getRelDown();
				if(!NO_TRANS_RULES && relUp >= 0 && relDown == 1) {
					if(relUp >  1 ) {
						keywordInfo.setRel2Up(relUp-1);
						keywordInfo.setRel2Down(2);
					}
					else {
						keywordInfo.setRel2Up(1);
						keywordInfo.setRel2Down(0);
					}
				}
				else {
					keywordInfo.setRel2Up(relUp);
					keywordInfo.setRel2Down(relDown);
				}
				tempKeywordInfos.set(k, keywordInfo);
			}
		}
		else if(regexCache.find("は(何処?|なん|いつ|どこ|誰|だれ)"+q2,sentence)) { 
			// Aは何といいますか。 -> AはXです。
			for(int k = 0; k < tempKeywordInfos.size(); k++) {
				QueryKeywordInfo keywordInfo	= tempKeywordInfos.get(k);
				int relUp	= keywordInfo.getRelUp();
				int relDown	= keywordInfo.getRelDown();
				if(!NO_TRANS_RULES && relUp >= 0 && relDown == 1) {
					keywordInfo.setRel2Up(relUp);
					keywordInfo.setRel2Down(0);
				}
				else {
					keywordInfo.setRel2Up(relUp);
					keywordInfo.setRel2Down(relDown);
				}
				tempKeywordInfos.set(k, keywordInfo);
			}
		}
		tempQueryInfo.setKeyWordInfos(tempKeywordInfos);

		return tempQueryInfo;
	}

	/**
	 * 各キーワードに関して、疑問詞文節との関係を得る.
	 * 
	 * @param keywordInfos キーワード情報のリスト
	 * @param kyotoDependencyList 
	 * @param knpSegmentList
	 * @param knpHash
	 * @param bnstNum 解候補の文節序数
	 * @param bnstMorphemeNum 解候補の文節内形態素序数
	 * @return
	 */
	public static List<QueryKeywordInfo> getRelation(List<QueryKeywordInfo> keywordInfos,
											List<KyotoDependency> kyotoDependencyList,
											List<KnpSegment> knpSegmentList,
											KnpBnstHash knpHash,
											int bnstNum, int bnstMorphemeNum)
	{
		// 解候補の文節を探索
		List<QueryKeywordInfo> tempKeywordInfo	= searchInner(bnstNum, bnstMorphemeNum,keywordInfos);
		int i = 0;
		for(QueryKeywordInfo keywordInfo : tempKeywordInfo) {
			int relUp	= keywordInfo.getRelUp();
			int relDown	= keywordInfo.getRelDown();
			if(!(relUp != 0 || relDown != 0)) {
				Integer[]	len	= searchLength(knpHash, bnstNum, keywordInfo.getBnstNo());
				keywordInfo.setRelUp(len[0]);
				keywordInfo.setRelDown(len[1]);
			}
			tempKeywordInfo.set(i, keywordInfo);
			i++;
		}

		KnpSegment knpSegment	= knpSegmentList.get(bnstNum);
		KnpSegment	sourceSegment	= null;
		KnpSegment	targetSegment	= null;
		for(KyotoDependency dependency : kyotoDependencyList) {
			sourceSegment	= (KnpSegment)dependency.getSource();
			targetSegment	= (KnpSegment)dependency.getTarget();

			if(sourceSegment.equals(knpSegment)) {
				break;
			}
		}
		if(sourceSegment != null) {
			StringArray sourceBunsetsuStrArray	= sourceSegment.getKnpBunsetsuFeatures();
			for(int index=0; index < sourceBunsetsuStrArray.size(); index++) {
				String feature	= sourceBunsetsuStrArray.get(index);
				// 疑問詞（解候補）の格を調べる
				if(regexCache.find("係:(.+)", feature)) {
					String wkaku	= regexCache.lastMatched(1);
					if(tempKeywordInfo != null && tempKeywordInfo.size() > 0) {
						QueryKeywordInfo keywordInfo	= tempKeywordInfo.get(0);
						if(keywordInfo == null) {
							keywordInfo	= new QueryKeywordInfo();
							keywordInfo.setWkaku(wkaku);
							tempKeywordInfo.add(keywordInfo);
						}
						else {
							keywordInfo.setWkaku(wkaku);
							tempKeywordInfo.set(0, keywordInfo);
						}
					}
					else {
						if(tempKeywordInfo == null) {
							tempKeywordInfo	= new ArrayList<QueryKeywordInfo>();
						}
						QueryKeywordInfo keywordInfo	= new QueryKeywordInfo();
						keywordInfo.setWkaku(wkaku);
						tempKeywordInfo.add(keywordInfo);
					}
				}
			}
			if(targetSegment != null) {
				StringArray targetBunsetsuStrArray	= targetSegment.getKnpBunsetsuFeatures();
				for(int index=0; index < targetBunsetsuStrArray.size(); index++) {
					String feature	= targetBunsetsuStrArray.get(index);
					// 疑問詞（解候補）の格を調べる
					if(regexCache.find("用言", feature)) {
						// 各キーワードの格を調べる
						for(int h=0; h < tempKeywordInfo.size(); h++) {
							QueryKeywordInfo keywordInfo	= tempKeywordInfo.get(h);
							int qbnstNo	= keywordInfo.getBnstNo();
							if(knpSegmentList.contains(qbnstNo)) {
								KnpSegment knpSegment2	= knpSegmentList.get(qbnstNo);
								for(KyotoDependency dependency : kyotoDependencyList) {
									KnpSegment sourceSegment2	= (KnpSegment)dependency.getSource();

									if(sourceSegment2.equals(knpSegment2)) {
										StringArray sourceBunsetsuStrArray2	= sourceSegment2.getKnpBunsetsuFeatures();
										for(int j=0; j < sourceBunsetsuStrArray2.size(); j++) {
											String feature2	= sourceBunsetsuStrArray2.get(j);
											// 疑問詞（解候補）の格を調べる
											if(regexCache.find("係:(.+)", feature2)) {
												keywordInfo.setKaku(regexCache.lastMatched(1));
											}
											else {
												keywordInfo.setKaku("");
											}
											if(regexCache.find("(用言.*)", feature2)) {
												keywordInfo.setYougen(regexCache.lastMatched(1));
											}
											else {
												keywordInfo.setYougen("");
											}
										}
									}
								}
							}
							tempKeywordInfo.set(h, keywordInfo);
						}
					}
				}				
			}
		}

		return tempKeywordInfo;
	}


	//----------------------------------------------------------------
	// private methods
	//----------------------------------------------------------------
	/**
	 * 解候補（または疑問詞）とキーワードとの構文距離を求める.
	 * 
	 * @param knpHashMap
	 * @param qi
	 * @param ki
	 * @return
	 */
	private static Integer[] searchLength(KnpBnstHash knpHash, int qi,int ki)
	{
		Map<Integer,List<Integer>> qTree	= new TreeMap<Integer,List<Integer>>();
		Map<Integer,List<Integer>> kTree	= new TreeMap<Integer,List<Integer>>();
		int i	= 0;
		int j	= 0;

		// 文節qiから文末までの構文枝を配列で返す
		while (true) {
			int i_tmp = i + 1;
			List<Integer> tmp = Knp2hash01.searchTree(knpHash, qi, i_tmp);
			if(tmp != null && tmp.get(0) != null) {
				qTree.put(i++, tmp);
			}
			else {
				break;
			}
		}

		// 文節$k_iから文末までの構文枝を配列で返す
		while (true) {
			int j_tmp = j + 1;
			List<Integer> tmp = Knp2hash01.searchTree(knpHash, ki, j_tmp);
			if(tmp != null && tmp.get(0) != null) {
				kTree.put(j++, tmp);
			}
			else {
				break;
			}
		}

		int relUp	= 0;
		int relDown	= 0;
		int length	= 0;
		int minLength	= -1;

		for(Entry<Integer,List<Integer>> qEntry : qTree.entrySet()) {
			for(Entry<Integer,List<Integer>> kEntry : kTree.entrySet()) {
				List<Integer>	qTemp	= qEntry.getValue();
				List<Integer>	kTemp	= kEntry.getValue();
				List<Integer>	qDef	= new ArrayList<>();
				List<Integer>	kDef	= new ArrayList<>();

				ROUTE:
				for(int q = 0; q < qTemp.size(); q++) {
					for(int k = 0; k < kTemp.size(); k++) {
						boolean flag	= false;
						for(int h = 0; qTemp.get(h) != null || kTemp.get(h) != null; i++) {
							if(!(qTemp.get(h) != null && kTemp.get(h) != null )
									|| qTemp.get(h) != kTemp.get(h)) {
								flag	= true;
								break;
							}
						}
						if(!flag) {
							break ROUTE;
						}
						kDef.add(kTemp.remove(0));
					}
					qDef.add(qTemp.remove(0));
				}
				length = qDef.size() + kDef.size();
				if(minLength == -1 || length < minLength ) {
					minLength = length;
					relUp    = 0;
					relDown  = 0;
					if(qDef.size() != 0) {
						relDown  = qDef.size();
					}
					if(kDef.size() != 0) {
						relUp  = kDef.size();
					}
				}
			}
		}

		Integer[]	rel	= {relUp,relDown};
		return rel;
	}

	/**
	 * 解候補の文節を探索し、キーワードとの関係を得る.
	 * 
	 * @param bnstNum 今いる文節番号
	 * @param bnstMorphemeNum 今いる文節内形態素番号
	 * @param qKeywordInfo
	 * @return
	 */
	private static List<QueryKeywordInfo> searchInner(int bnstNum, int bnstMorphemeNum,
											List<QueryKeywordInfo> qKeywordInfo)
	{
		List<QueryKeywordInfo> tempKeywordInfo	= qKeywordInfo;

		// 解候補と同じ文節内のキーワードを探す
		int i	= 0;
		for(QueryKeywordInfo keywordInfo : tempKeywordInfo) {
			int qBnstNo		= keywordInfo.getBnstNo();
			int qBnstMorphemeNo	= keywordInfo.getBnstMorphemeNo();
			if(bnstNum == qBnstNo) {
				if(bnstMorphemeNum >= qBnstMorphemeNo) {
					keywordInfo.setRelUp(1);
					keywordInfo.setRelDown(0);
				}
				else {
					keywordInfo.setRelUp(0);
					keywordInfo.setRelDown(1);
				}
			}
			tempKeywordInfo.set(i, keywordInfo);
			++i;
		}
		return tempKeywordInfo;
	}
}
