package org.kachako.components.factoid_qa.module;

import java.util.ArrayList;
import java.util.List;

import org.kachako.share.regex.RegexCache;
import org.kachako.types.japanese.syntactic.KnpSegment;

/**
 * KNPの出力データから疑問詞を含む文節の番号を返す
 * 
 * @author saitou
 *
 */
public class PknumDcInterrogate
{
	private static final RegexCache regexCache = new RegexCache();

	/**
	 * コンストラクタ
	 */
	public PknumDcInterrogate()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	// KNPの出力データから疑問詞を含む文節の番号を返す
	// sub dc_interrogate(@) {
	/**
	 * KNPの出力データから疑問詞を含む文節の番号を返す.
	 * 
	 * @param knpInfoMap KnpSegmentに対する形態素情報
	 * @return 文節番号の配列
	 */
	public static Integer[] dcInterrogate(List<KnpSegment> knpSegmentList)
	{
		List<Integer>	intList	= new ArrayList<Integer>();

		int	num	= 0;
		for(KnpSegment knpSegment : knpSegmentList){
			String	text	= knpSegment.getCoveredText();
			if(regexCache.find("^(何|なに|誰|だれ|何処|どこ|何時|いつ|いくら|幾ら)",text)) {
				intList.add(num);
			}
			else if(regexCache.find("(どれ|どの)", text)) {
				intList.add(num);
			}
			else if(regexCache.find("(いく|幾)つ", text)) {
				intList.add(num);
			}
			++num;
		}

		return (Integer[])intList.toArray(new Integer[0]);
	}
}
