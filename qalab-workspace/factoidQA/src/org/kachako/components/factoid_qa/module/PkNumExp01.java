package org.kachako.components.factoid_qa.module;

import java.util.List;

import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

public class PkNumExp01
{
	/**
	 * コンストラクタ
	 */
	public PkNumExp01()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	// sub pk_num_exp(@){
	public static String pkNumExp(List<Morpheme> morphemeList)
	{
		boolean	flag	= false;
		String	output	= "";
		String	suffix	= "";
		String	buffer	= "";

		for(int i = morphemeList.size() - 1;  i > 0; i--) {
			Morpheme	morpheme	= morphemeList.get(i);
			String surface	= morpheme.getSurfaceForm();
			String pos		= morpheme.getPos();
			String detailedPos	= morpheme.getDetailedPos();

			if(detailedPos.equals("数詞")) {
				if(!flag) {
					flag	= true;
					output	= surface + suffix;
					suffix = "";
				}
				else {
					output = surface + buffer + output;
					buffer = "";
				}
			}
			else if(pos.equals("接尾辞")) {
				if(!flag) {
					suffix = surface + suffix;
				}
				else {
					buffer = surface + buffer;
				}
			}
			else if(flag) {
				if(pos.equals("接頭辞")) {
					if(buffer.equals("")) {
						output = surface + output;
					}
					else {
						buffer = surface + buffer;
					}
				}
				else {
					// 数詞 + 助数辞でない名詞 + 接尾辞(「あたり」とか)
					// のような場合に「数詞 + 接尾辞」で出力してしまうのを防ぐ
					buffer = surface + buffer;
				}
			}
			else {
				suffix = "";
			}
		}
		return output;
	}

}
