package org.kachako.components.factoid_qa.module;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.kachako.components.factoid_qa.manage.DecisionData;
import org.kachako.share.regex.RegexCache;

public class SelAtr03
{
	private static final RegexCache regexCache = new RegexCache();

	/**
	 * コンストラクタ
	 */
	public SelAtr03()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	//sub sel_atr($$$$){
	public static HashMap<String,String> selAtr(Map<String,Integer> kadokawaAtrHm,
																HashMap<String, List<DecisionData>> atrDecisionDataMap,
																String branch,List<String> objCandidateList,
																HashMap<String,String> cpcBySubMap,
																HashMap<String,String> cphByIdMap,
																HashMap<String,String> cphByNameMap)
	{
		String evidence	= "";
		String atrNum	= "";
		String atrExp	= "";
		String atrInf	= "";
		double threshold	= -1.7; // 対数尤度の閾値

		List<String>	atrCandidateList	= new ArrayList<String>();
		boolean testFlag = false;	// 実験用ルーチン (true:有効 false:無効)
		for(int i = 0 ; i < objCandidateList.size(); i++ ) {
			if(judgeAtr(kadokawaAtrHm, objCandidateList.get(i),cpcBySubMap,cphByIdMap,cphByNameMap) == 1) {
				// 実験用 決定リストを使わずに，数値との距離が最も近い名詞を返す

				if(regexCache.find("(.+?)\\((\\d+)\\)=(.+)",objCandidateList.get(i)) && testFlag) {
					atrNum	= regexCache.lastMatched(2);
					atrExp	= regexCache.lastMatched(1);
					atrInf	= regexCache.lastMatched(3);
					int j = 0;
					while(objCandidateList.get(j) != null) {
						if(regexCache.find("^.*?\\("+atrNum+"\\)=.+",objCandidateList.get(j))) {
							objCandidateList.remove(j);
							continue;
						}
						j++;
					}
					HashMap<String,String> hm	= new HashMap<String,String>();
					hm.put("atrNum",regexCache.lastMatched(2));
					hm.put("atrExp",regexCache.lastMatched(1));
					hm.put("atrInf",regexCache.lastMatched(3));

					return hm;
				}

				atrCandidateList.add(objCandidateList.get(i));
			}
		}

		HashMap<String,String> hm	= new HashMap<String,String>();
		hm.put("atrNum","");
		hm.put("atrExp","");
		hm.put("atrInf","");

//System.out.println("branch="+branch);
		List<DecisionData> atrDecisionDataList	= atrDecisionDataMap.get(branch);
		if(atrDecisionDataList == null) {
			return null;
		}
		for(int i = 0; i < atrDecisionDataList.size(); ++i) {
			DecisionData	decisionData	= atrDecisionDataList.get(i);
			evidence	= decisionData.getEvidence();
			double llh	= decisionData.getLlh();
			int right	= decisionData.getRight();
			//int miss	= decisionData.getMiss();

			// 尤度が閾値よりも低くなったとき && 正解数が0のとき
			if(llh < threshold && right == 0) {
				return hm;
			}

			for(int j = 0 ; j < atrCandidateList.size() ; ++j) {
				if(regexCache.find("(.+?)\\((\\d+)\\)=(.+)",atrCandidateList.get(j))) {
					if(evidence.equals(regexCache.lastMatched(3))) {
						atrNum	= regexCache.lastMatched(2);
						atrExp	= regexCache.lastMatched(1);
						atrInf	= regexCache.lastMatched(3);

						// ものの候補から属性を削除
						int count	= 0;
						while(objCandidateList.get(count) != null) {
							if(regexCache.find("^.*?\\("+atrNum+"\\=.+", objCandidateList.get(count))) {
								objCandidateList.remove(count);
								continue;
							}
							count++;
						}

						hm.put("atrNum",atrNum);
						hm.put("atrExp",atrExp);
						hm.put("atrInf",atrInf);

						return hm;
					}
				}
			}
		}

		return null;
	}


	//----------------------------------------------------------------
	// private methods
	//----------------------------------------------------------------

	//sub judge_atr($$){
	private static int judgeAtr(Map<String,Integer> kadokawaAtrHm,String word,
										HashMap<String,String> cpcBySubMap,HashMap<String,String> cphByIdMap,
										HashMap<String,String> cphByNameMap)
	{
		if(cpcBySubMap == null || cphByIdMap == null || cphByNameMap == null) {
			return 0;
		}

		String ids	= "";
		String exp	= "";
		String[] expBit	= null;

		if(regexCache.find("^(.+?)\\(",word)) {
			String	tmp	= regexCache.lastMatched(1);
			expBit	= tmp.split("-");

			StringBuilder builder = new StringBuilder();
			for(String str : expBit) {
				builder.append(str);
			}

			exp	= builder.toString();

			List<String>	tmpList	= new ArrayList<String>();
			for(int i = expBit.length-1; i > -1; --i) {
				tmpList.add(expBit[i]);
			}
			expBit	= (String[])tmpList.toArray(new String[0]);

			if(kadokawaAtrHm.containsKey(exp) || kadokawaAtrHm.containsKey(expBit[0])) {
				return 1;
			}
		}

		// EDR辞書検索
		if(cphByNameMap.get(exp) != null) {
			ids	= cphByNameMap.get(exp);
			HashMap<String,Integer> ancMap	= EdrCP.edrcpAllAncestor(ids, cpcBySubMap);
			for(String key :  ancMap.keySet()) {
				if(cphByIdMap.get(key) == null) {
					continue;
				}
				if(regexCache.find("計る対象でとらえた量", cphByIdMap.get(key))) {
					return 1;
				}
			}
		}
		else if(cphByNameMap.get(expBit) != null) {
			ids	= cphByNameMap.get(expBit);
			HashMap<String,Integer> ancMap	= EdrCP.edrcpAllAncestor(ids, cpcBySubMap);
			for(String key :  ancMap.keySet()) {
				if(cphByIdMap.get(key) == null) {
					continue;
				}
				if(regexCache.find("計る対象でとらえた量", cphByIdMap.get(key))) {
					return 1;
				}
			}
		}

		return 0;
	}

}
