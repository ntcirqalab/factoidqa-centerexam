package org.kachako.components.factoid_qa.module;

import java.util.ArrayList;
import java.util.List;

public class CwqGet2gram01
{

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	/**
	 * 2グラムで文字列を取り出す.
	 * 
	 * @param query 文字列
	 *
	 * @return 文字列の配列
	 */
	public static String[] get2gram(String query)
	{
		if(query == null) {
			return null;
		}

		List<String> gramList	= new ArrayList<String>();
		while(query.length() >= 4) {
			gramList.add(query.substring(0, 4));
			query = query.substring(2);
		}

		return (String[])gramList.toArray(new String[0]);
	}
}
