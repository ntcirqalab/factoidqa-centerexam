package org.kachako.components.factoid_qa.module;

import java.util.List;

public class SpotParen
{

	/* 場所 */
	private static String[] patParenList = {
							// pattern 0: 括弧の中(「」)
							"(?:"
								+"(?:(?:^|\n)「\\s.*)"
									+"("
										+"(?:(?:^|\n)\\S+\\s.*)+?"
									+")"
								+"(?:(?:^|\n)」\\s.*)"
							+")",
							// pattern 1: 括弧の中(『』)
							"(?:"
								+"(?:(?:^|\n)『\\s.*)"
									+"("
										+"(?:(?:^|\n)\\S+\\s.*)+?"
									+")"
								+"(?:(?:^|\n)』\\s.*)"
							+")",
							// pattern 2: 括弧の中(（）)
							"(?:"
								+"(?:(?:^|\n)（\\s.*)"
									+"("
										+"(?:(?:^|\n)\\S+\\s.*)+?"
									+")"
								+"(?:(?:^|\n)）\\s.*)"
							+")",
							// pattern 3: 括弧の中(“”)
							"(?:"
								+"(?:(?:^|\n)“\\s.*)"
									+"("
										+"(?:(?:^|\n)\\S+\\s.*)+?"
									+")"
								+"(?:(?:^|\n)”\\s.*)"
							+")"
						};

	// 大域変数
//	private static final int FLAG_INIT	= 1;

	/**
	 * コンストラクタ
	 */
	public SpotParen()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	//	##################################################################
	//	# spotter_paren()
	//	#   形態素情報列において，指定した形態素を含む，指定した型の場所表現
	//	#   が あれば取り出す．なければ ""
	//	##################################################################
	//	sub spotter_paren($$$) {
	public String spotterParen(List<String> lines, int morphemeNo, String netType)
	{
		String ans	= "";

		if(netType.equals("PAREN")) {
			String[] linesIds = SpotAnswer.spotterAddIndex(lines);

			// パタンを順番に適用して，一番最初に見つかったものを利用
			for(int i = 0 ; i < patParenList.length; i++) {
//System.out.println(patParenList[i]);
				String pattern	= patParenList[i];
				ans	= SpotAnswer.spotterMkPatMatchFuncMorphForAns(pattern, linesIds, morphemeNo);
				if(ans != null && ans.length() > 0) {
					break;
				}
			}
		}
		else {
			ans = "";
		}

		return ans;
	}

}
