package org.kachako.components.factoid_qa.module;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.kachako.share.regex.RegexCache;
import org.kachako.types.qa.KeyTerm;

/**
 * パッセージ生成クラス
 * 
 * @author JSA saitou
 *
 */
public class MakePassage
{
//	private  RegexCache regexCache = new RegexCache();

	/**
	 * コンストラクタ
	 */
	public MakePassage()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	/**
	 * 文章を一文ずつに分ける.
	 * 
	 * @param str 文書文字列
	 * @return
	 */
	public static String[] getTextLines(String text, RegexCache regexCache)
	{
		String str	= text;
		str = str.replaceAll("<(\\s*)\\/[^>]+>","。");
		str = str.replaceAll("<[^>]+>","");	//<hogehoge>, </hogehoge>除去

		// 半角の英数字記号文字を全角文字へ
		str	= convertOneByteToTwoByte(str);

		str = str.replaceAll("[\r\n]+","\n");
		str	= str.replaceAll("^．．．","");
		str	= str.replaceAll("．{3,}","。");
		if(!regexCache.find("。$", str)) {
			str	= str + "。";
		}
		str	= str.replaceAll("[　| ]","");
		str = str.replaceAll("^\n","");
		str = str.replaceAll("[\n]+","\n");
		str	= str.replaceAll("[。！？．，：｜＊−・]+?、","。");
		str	= str.replaceAll("、+","、");
		str	= str.replaceAll("[、！？．，：｜＊−・]+?。","。");
		str	= str.replaceAll("([、，：]+?)\n","$1");
		str	= str.replaceAll("([！？])","$1。");
		str	= str.replaceAll("。+","。");

		str = str.replaceAll("\\x00", "");

		str = str.replaceAll("[●○◆◇■□△▲☆★]","。");
		str = str.replaceAll("[▽▼]","、");

		str = str.replaceAll("(&nbsp|&gt|&quot);","");	// ノーブレイクスペースの除去
		str = str.replaceAll("([一二三四五六七八九〇零十百千万億兆]+)[\\,|、|，]([一二三四五六七八九〇零十百千万億兆]{3,})","$1$2");

		str = str.replaceAll("/＝(写真|同)((＜([上下左右]|中央?)＞|([上下左右]|中央?))*([、・](ロイター|ＡＰ|同))?＝?|（[１２３４５６７８９０]+）|([^。]{1,12}?写す(?=。)))","");
		str = str.replaceAll("[─━‥−…-]{3,}","");
		str = str.replaceAll("[─━‥−…-]{2,}","、");

		str = str.replaceAll("【(.*?)】","　$1　");
		str = str.replaceAll("［(.*?)］","。$1　");

		str = str.replaceAll("＜(.*?)＞","　$1　");
		str = str.replaceAll("〈(.*?)〉","　$1　");

		str = str.replaceAll("<\\/[^>]+>\\n","。");

		str = str.replaceAll("^[＞>]","");
		str = str.replaceAll("([０１２３４５６７８９]+)[、|，]([０１２３４５６７８９]{3,})","$1$2");

		str = str.replaceAll("　+","　");
		str = str.replaceAll("！([^？！])","！\n"+"$1");
		str = str.replaceAll("？+([^）」])","？\n"+"$1");

		str = str.replaceAll("[　、]*、[　、]*","、");

		str = str.replaceAll("[。\n]+","。");
		str = str.replaceAll("^[。、　]+","");
		str = str.replaceAll("[。、　]*。[。、　]*","。");
		str = str.replaceAll("。(?![）」])","。\n");
		str = str.replaceAll("\n。","");

		//str = str.toUpperCase();
		String[] array = str.split("\n");
		List<String> list	= new ArrayList<String>();
		for(String s : array) {
//System.out.println("["+s+"]");
			s = s.replaceAll("^[\n]+","");
			if(regexCache.find("^。$", s)) {
				continue;
			}
			if(!regexCache.find("。$", s)) {
				s	= s + "。";
			}
			list.add(s);
		}
		return list.toArray(new String[0]);
	}

	/**
	 * 一文にキーワードが存在するカウント数を取得する.
	 * 
	 * @param document 文
	 * @param keyTermList キーワードリスト
	 * @return 文に対してのキーワードカウント数のリスト.
	 */
	public static List<Integer> getKeywordCount(String document, List<KeyTerm> keyTermList)
	{
		List<Integer> keyCountList	= new ArrayList<Integer>();
		for(KeyTerm keyTerm : keyTermList) {
			String word = keyTerm.getCoveredText();
word = word.replaceAll("\\?","");			
			Pattern p = Pattern.compile(word);
			Matcher m = p.matcher(document);
			m.reset();
			int count	= 0;
			while(m.find()) {
				count++;
			}
			keyCountList.add(count);
		}
		return keyCountList;
	}

	/**
	 * 文に数値情報が存在するカウント数を取得する.
	 *
	 * @param document 文
	 * @param pattern1 パターン1
	 * @param pattern2 パターン2
	 * @return 文に対しての数値情報カウント数のリスト.
	 */
	public static List<Integer> getNeCount(String document, String pattern1, String pattern2)
	{
		int count1	= 0;
		int count2	= 0;
		if(pattern1 != null && pattern2 != null) {
			Pattern p = Pattern.compile(pattern1+"|"+pattern2);
			Matcher m = p.matcher(document);
			m.reset();
			while(m.find()) {
				if(m.group().matches(pattern1)) {
					count1++;
				}
				if(m.group().matches(pattern2)) {
					count2++;
				}
			}
		}
		else if(pattern1 != null) {
			Pattern p = Pattern.compile(pattern1);
			Matcher m = p.matcher(document);
			m.reset();
			while(m.find()) {
				count1++;
			}				
		}
		else if(pattern2 != null) {
			Pattern p = Pattern.compile(pattern2);
			Matcher m = p.matcher(document);
			m.reset();
			while(m.find()) {
				count2++;
			}				
		}
		List<Integer>	countList	= new ArrayList<Integer>();
		countList.add(count1);
		countList.add(count2);

		return countList;
	}

	/**
	 * 文書のスコアを取得する.
	 * 
	 * @param sentList
	 * @param url
	 * @param requireType
	 * @param keyTermList
	 * @param keyCountMap
	 * @param neCountMap
	 * @return
	 */
	public static double getPassageScore(String[] sentList, String url, String requireType,
														List<KeyTerm> keyTermList,
														String pattern1, String pattern2)
	{
		List<Integer> existList = new ArrayList<>();	// 各キーワードの出現数（パッセージ）
		int existLine =  0;    // 出現キーワード種数（最大の１行）   (重み付き)
		int existPassage =  0;    // 出現キーワード種数（パッセージ）   (重み付き)
		int pscore1       = 0;     // パッセージのスコア
		int ne = 0;

		for(int i = 0; i < sentList.length; i++) {
			List<Integer> keyCountList	= getKeywordCount(sentList[i], keyTermList);
			List<Integer> neCountList	= getNeCount(sentList[i], pattern1, pattern2);

			int existLineTemp	= 0;
			if(!requireType.equals("none") && neCountList.get(0) != 0) {
				existLineTemp++;
				ne = 2;
			}
			else if(!requireType.equals("none") && neCountList.get(1) != 0) {
				existLineTemp += 0.5;
				if(ne == 0) {
					ne	= 1;
				}
			}
			// 各キーワードの出現数（１行）
			List<Integer> existTemp	= keyCountList;
			if(existList.size() > 0) {
				for(int j = 0 ; j < existList.size() ; j++ ) {
					int value	= existList.get(j)+existTemp.get(j);
					existList.set(j, value);
				}
			}
			else {
				existList = existTemp;
			}

			for(int j = 0 ; j < existTemp.size() ; j++ ) {
				if(existTemp.get(j) != 0) {
					existLineTemp	+= keyTermList.get(j).getScore();
				}
			}
			if(existLine < existLineTemp) {
				existLine	= existLineTemp;
			}
		}

		// 出現キーワード総数*品詞スコア、および出現キーワードの種類の数を数える
		for(int i = 0 ; i < existList.size(); i++) {
			//double temp = existList.get(i) * keyTermList.get(i).getScore();
			if(existList.get(i) > 0) {
				existPassage += keyTermList.get(i).getScore();
			}
		}
		// パッセージのスコアを計算する
		pscore1 = existPassage + existLine + ne + getYearByKeyword(keyTermList,url);

		return pscore1;
	}


	//----------------------------------------------------------------
	// private methods
	//----------------------------------------------------------------

	private static int getYearByKeyword(List<KeyTerm> keyTermList,String url)
	{
		String year	= "";
		for(int i = 0; i < keyTermList.size(); i++) {
			KeyTerm keyTerm	= keyTermList.get(i);
			String word	= keyTerm.getCoveredText();
			if((word.matches("^(１９)?９８"))
				|| (i != 0 && keyTermList.get(i-1).getCoveredText() != null
					&& keyTermList.get(i-1).getCoveredText().matches("^平成$")
					&& word.matches("^(１０|十)")))
			{
				year	= "９８年";
			}
			if((word.matches("^(１９)?９９"))
					|| (i != 0 && keyTermList.get(i-1).getCoveredText() != null
						&& keyTermList.get(i-1).getCoveredText().matches("^平成$")
						&& word.matches("^(１１|十一)")))
			{
				year	= "９９年";
			}
		}

		 if((year.equals("９８年") && url.matches("^(98|JA-98|JY-1998)"))
				 || (year.equals("９９年") && url.matches("^(99|JA-99|JY-1999)"))) {
			 return 1;
		 }
		return 0;
	}

	private  static String convertOneByteToTwoByte(String str)
	{
		int difference	= 'Ａ' - 'A';
		char[] cc = str.toCharArray();
		StringBuilder sb = new StringBuilder();
		for (char c : cc) {
			char newChar = c;
			if ((('A' <= c) && (c <= 'Z')) || (('a' <= c) && (c <= 'z'))
					|| (('0' <= c) && (c <= '9')) || isSign(c)) {
				// 変換対象のcharだった場合に全角文字と半角文字の差分を足す
				newChar = (char) (c + difference);
			}

			sb.append(newChar);
		}
		return sb.toString();
	}

	private static boolean isSign(char pc)
	{
		char[] signs	= { '!' , '"', '#' , '$' ,'%' , '&' ,'\'' , '(' , ')' , '*' , '+' , ',' , '.' , '/' , ':' , ';' , '<' , '=' , '>' , '?' , '@' , '[' , '\\' , ']' , '^' , '_' , '`' , '{' , '|' , '}' , '~' ,'-' };
		for(char c : signs) {
			if(c == pc) {
				return true;
			}
		}
		return false;
	}
}
