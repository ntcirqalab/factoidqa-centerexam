package org.kachako.components.factoid_qa.module;

import java.util.HashMap;
import java.util.List;

import org.kachako.components.factoid_qa.manage.RequireInfo;
import org.kachako.share.regex.RegexCache;
import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

/**
 * 質問文（クエリ）から抽出すべき情報の種類を特定する.
 *
 * @author JSA saitou
 *
 */
public class GetRequireInfo
{
	/**
	 * コンストラクタ
	 */
	public GetRequireInfo() {
	}


	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	/**
	 * 質問文（クエリ）から抽出すべき情報を取得する.
	 * @param query
	 * @param morphemeList
	 * @param josujiMap
	 * @return
	 */
	public static RequireInfo get(String query, List<Morpheme> morphemeList, HashMap<String,Integer> josujiMap)
	{
		if(query == null) {
			return null;
		}

		RegexCache regexCache	= new RegexCache();

		query = query.replaceAll("「.*?」|（.*?）|［.*?］", "");
		query = query.replaceAll("[、。．！？　\n\r\t ]$", "");

		RegexCache.PartialApp partialApp = regexCache.appQuery(query);

		String	type	= null;
		String	qword	= null;
		String	pattern1	= null;
		String	pattern2	= null;

		if(partialApp.find("何秒何")) {
			type	= "time.second-closely";
			qword	= regexCache.lastMatched();
			pattern1	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+秒[０-９〇一二三四五六七八九十零]+";
			pattern2	= "([０-９〇一二三四五六七八九十零百千万億兆・．—]+[時分秒].{0,1})+[０-９〇一二三四五六七八九十零]?";
		}
		else if(partialApp.find("何[十百千万億兆]*(％|パーセント)")) {
			type	= "rate";
			qword	= regexCache.lastMatched();
			pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(［?％］?|パーセント)";
			pattern2 = "([^．・]|^)[０-９〇一二三四五六七八九十零]*[．・][０-９〇一二三四五六七八九十零]+(?![．・])|[０-９〇一二三四五六七八九十零百千万億兆・．—]+(［?％］?|パーセント|(分の|対)[０-９〇一二三四五六七八九十零百千万億兆・．—]+)|[０-９〇一二三四五六七八九十零]+割([０-９〇一二三四五六七八九十零]分)?([０-９〇一二三四五六七八九十零]厘)?|([０-９〇一二三四五六七八九十零]分)?[０-９〇一二三四五六七八九十零]厘";
		}
		else if(partialApp.find("何割(何分)?(何厘)?")) {
			type	= "rate";
			qword	= regexCache.lastMatched();
			pattern1 = "[０-９〇一二三四五六七八九十零]+割([０-９〇一二三四五六七八九十零]分)?([０-９〇一二三四五六七八九十零]厘)?|[^．・][０-９〇一二三四五六七八九十零]*[．・][０-９〇一二三四五六七八九十零]+[^．・]";
			pattern2 = "([^．・]|^)[０-９〇一二三四五六七八九十零]*[．・][０-９〇一二三四五六七八九十零]+(?![．・])|[０-９〇一二三四五六七八九十零百千万億兆・．—]+(［?％］?|パーセント|(分の|対)[０-９〇一二三四五六七八九十零百千万億兆・．—]+)|[０-９〇一二三四五六七八九十零]+割([０-９〇一二三四五六七八九十零]分)?([０-９〇一二三四五六七八九十零]厘)?|([０-９〇一二三四五六七八九十零]分)?[０-９〇一二三四五六七八九十零]厘";
		}
		else if(partialApp.find("割合は|率は") && partialApp.find("どれ(だけ|ほど|[くぐ]らい)|どの(程度|ていど|[くぐ]らい)|(いく|幾)[らつ]")) {
			type	= "rate";
			qword	= regexCache.lastMatched();
			pattern1 = "([^．・]|^)[０-９〇一二三四五六七八九十零]*[．・][０-９〇一二三四五六七八九十零]+(?![．・])|[０-９〇一二三四五六七八九十零百千万億兆・．—]+(［?％］?|パーセント|(分の|対)[０-９〇一二三四五六七八九十零百千万億兆・．—]+)|[０-９〇一二三四五六七八九十零]+割([０-９〇一二三四五六七八九十零]分)?([０-９〇一二三四五六七八九十零]厘)?|([０-９〇一二三四五六七八九十零]分)?[０-９〇一二三四五六七八九十零]厘";
		}
		else if(partialApp.find("([^速秒分時]|^)(何[十百千万億兆]*(ｍ|Ｍ|メートル))")) {
			type	= "length";
			qword	= regexCache.lastMatched(2);
			pattern1	= "([^速秒分時]|^)[０-９〇一二三四五六七八九十零百千万億兆・．—]+(ｍ|Ｍ|メートル)";
			pattern2	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(［?([ｋＫｃＣｍＭμｎｐ]?[ｍＭ]］?(?!／[ｓＳｍＭｈＨ])|ｙ|ｆ|ｉｎｃｈ)|(キロ|センチ|ミリ|マイクロ|ナノ|ピコ)?メートル|マイル|ヤード|フィート|インチ|キロ|センチ|ミリ|オングストローム|光年|天文単位)";
		}
		else if(partialApp.find("([^速秒分時]|^)(何[十百千万億兆]*(ｋ|Ｋ|キロ)(ｍ|Ｍ|メートル))")
				&& partialApp.find("([長高深]さ?|距離|幅)は")
				&& partialApp.find("(何キロ)")) {
			type	= "length";
			qword	= regexCache.lastMatched(1);
			pattern1	= "([^速秒分時]|^)[０-９〇一二三四五六七八九十零百千万億兆・．—]+(ｋ|Ｋ|キロ)(ｍ|Ｍ|メートル)?";
			pattern2	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(［?([ｋＫｃＣｍＭμｎｐ]?[ｍＭ]］?(?!／[ｓＳｍＭｈＨ])|ｙ|ｆ|ｉｎｃｈ)|(キロ|センチ|ミリ|マイクロ|ナノ|ピコ)?メートル|マイル|ヤード|フィート|インチ|キロ|センチ|ミリ|オングストローム|光年|天文単位)";
		}
		else if(partialApp.find("([^速秒分時]|^)(何[十百千万億兆]*(ｃ|Ｃ|センチ)(ｍ|Ｍ|メートル))")
				&& partialApp.find("([長高深]さ?|距離|幅)は")
				&& partialApp.find("(何センチ)")) {
			type	= "length";
			qword	= regexCache.lastMatched(1);
			pattern1	= "([^速秒分時]|^)[０-９〇一二三四五六七八九十零百千万億兆・．—]+(ｃ|Ｃ|センチ)(ｍ|Ｍ|メートル)?";
			pattern2	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(［?([ｋＫｃＣｍＭμｎｐ]?[ｍＭ]］?(?!／[ｓＳｍＭｈＨ])|ｙ|ｆ|ｉｎｃｈ)|(キロ|センチ|ミリ|マイクロ|ナノ|ピコ)?メートル|マイル|ヤード|フィート|インチ|キロ|センチ|ミリ|オングストローム|光年|天文単位)";
		}
		else if(partialApp.find("([^速秒分時]|^)(何[十百千万億兆]*(ｍ|Ｍ|ミリ)(ｍ|Ｍ|メートル))")
				&& partialApp.find("([長高深]さ?|距離|幅)は")
				&& partialApp.find("(何ミリ)")) {
			type	= "length";
			qword	= regexCache.lastMatched(1);
			pattern1	= "([^速秒分時]|^)[０-９〇一二三四五六七八九十零百千万億兆・．—]+(ｍ|Ｍ|ミリ)(ｍ|Ｍ|メートル)?";
			pattern2	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(［?([ｋＫｃＣｍＭμｎｐ]?[ｍＭ]］?(?!／[ｓＳｍＭｈＨ])|ｙ|ｆ|ｉｎｃｈ)|(キロ|センチ|ミリ|マイクロ|ナノ|ピコ)?メートル|マイル|ヤード|フィート|インチ|キロ|センチ|ミリ|オングストローム|光年|天文単位)";
		}
		else if(partialApp.find("[^速秒分時](何[十百千万億兆]*(オングストローム|光年|天文単位))")) {
			type	= "length";
			qword	= regexCache.lastMatched(1);
			pattern1	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+"+regexCache.lastMatched(2);
			pattern2	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(［?([ｋＫｃＣｍＭμｎｐ]?[ｍＭ]］?(?!／[ｓＳｍＭｈＨ])|ｙ|ｆ|ｉｎｃｈ)|(キロ|センチ|ミリ|マイクロ|ナノ|ピコ)?メートル|マイル|ヤード|フィート|インチ|キロ|センチ|ミリ|オングストローム|光年|天文単位)";
		}
		else if(partialApp.find("[^速秒分時](何([μｎｐ]?[ｍＭ]|[ｙｆ]|ｉｎｃｈ|(マイクロ|ナノ|ピコ)?メートル|マイル|ヤード|フィート|インチ))")
				&& partialApp.find("([長高深]さ?|距離|幅)は")
				&& partialApp.find("(どれ(だけ|ほど|[くぐ]らい)|どの(程度|ていど|[くぐ]らい)|(いく|幾)[らつ])")) {
			type	= "length";
			qword	= regexCache.lastMatched(1);
			pattern1	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(［?([μｎｐ][ｍＭ]］?(?!／[ｓＳｍＭｈＨ])|ｙ|ｆ|ｉｎｃｈ)|(キロ|センチ|ミリ|マイクロ|ナノ|ピコ)?メートル|キロ|センチ|ミリ|マイル|ヤード|フィート|インチ|オングストローム|光年|天文単位)";
		}
		// 速度
		else if(partialApp.find("(速度?|[秒分時])[はで]?(何[十百千万億兆]*(ｍ|Ｍ|メートル))")) {
			type	= "speed";
			qword	= regexCache.lastMatched(2);
			pattern1	= "(.[速秒分時])?[０-９〇一二三四五六七八九十零百千万億兆・．—]+(ｍ|Ｍ|メートル)";
			pattern2	= "((.[速秒分時])?[０-９〇一二三四五六七八九十零百千万億兆・．—]+(ｍ|Ｍ|ｙ|ｆ|ｉｎｃｈ|(キロ|センチ|ミリ|マイクロ|ナノ|ピコ)?メートル|マイル|ヤード|フィート|インチ|キロ|センチ|ミリ|オングストローム|光年|天文単位)|[０-９〇一二三四五六七八九十零百千万億兆・．—]+［?[ｋＫｃＣｍＭμｎｐ]?[ｍＭ]／[ｓＳｍＭｈＨ]］?)";
		}
		else if(partialApp.find("(速度?|[秒分時])[はで]?(何[十百千万億兆]*(ｋ|Ｋ|キロ)(ｍ|Ｍ|メートル))")) {
			type	= "speed";
			qword	= regexCache.lastMatched(2);
			pattern1	= "(.[速秒分時])?[０-９〇一二三四五六七八九十零百千万億兆・．—]+(ｋ|Ｋ|キロ)(ｍ|Ｍ|メートル)?";
			pattern2	= "((.[速秒分時])?[０-９〇一二三四五六七八九十零百千万億兆・．—]+(ｍ|Ｍ|ｙ|ｆ|ｉｎｃｈ|(キロ|センチ|ミリ|マイクロ|ナノ|ピコ)?メートル|マイル|ヤード|フィート|インチ|キロ|センチ|ミリ|オングストローム|光年|天文単位)|[０-９〇一二三四五六七八九十零百千万億兆・．—]+［?[ｋＫｃＣｍＭμｎｐ]?[ｍＭ]／[ｓＳｍＭｈＨ]］?)";
		}
		else if(partialApp.find("速度?は")
				&& partialApp.find("どれ(だけ|ほど|[くぐ]らい)|どの(程度|ていど|[くぐ]らい)|(いく|幾)[らつ]")) {
			type	= "speed";
			qword = regexCache.lastMatched();
			pattern1	= "(.[速秒分時])?[０-９〇一二三四五六七八九十零百千万億兆・．—]+((ｋ|Ｋ|キロ)?(ｍ|Ｍ|メートル)|(ｋ|Ｋ|キロ))";
			pattern2	= "((.[速秒分時])?[０-９〇一二三四五六七八九十零百千万億兆・．—]+(ｍ|Ｍ|ｙ|ｆ|ｉｎｃｈ|(キロ|センチ|ミリ|マイクロ|ナノ|ピコ)?メートル|マイル|ヤード|フィート|インチ|キロ|センチ|ミリ|オングストローム|光年|天文単位)|[０-９〇一二三四五六七八九十零百千万億兆・．—]+［?[ｋＫｃＣｍＭμｎｐ]?[ｍＭ]／[ｓＳｍＭｈＨ]］?)";
		}
		// 面積
		else if(partialApp.find("何[十百千万億兆]*(平方(メートル|ｍ|Ｍ)|平米)")) {
			type	= "area";
			qword	= regexCache.lastMatched();
			pattern1	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(平方(メートル|ｍ|Ｍ)|平米)";
			pattern2	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(［?([ｋＫｃＣｍＭμｎｐ]?[ｍＭ]２|ｈ?ａ)］?|平方(キロ|センチ|ミリ|マイクロ|ナノ|ピコ)?(メートル|[ｍＭ])|(エーカ|アール|ヘクタール|坪|平米))";
		}
		else if(partialApp.find("何[十百千万億兆]*(ｈａ|ヘクタール)")) {
			type	= "area";
			qword    = regexCache.lastMatched();
			pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(ｈａ|ヘクタール)";
			pattern2 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(［?([ｋＫｃＣｍＭμｎｐ]?[ｍＭ]２|ｈ?ａ)］?|平方(キロ|センチ|ミリ|マイクロ|ナノ|ピコ)?(メートル|[ｍＭ])|(エーカ|アール|ヘクタール|坪|平米))";
		}
		else if(partialApp.find("何[十百千万億兆]*(坪|エーカ)")) {
			type	= "area";
			qword	= regexCache.lastMatched();
			pattern1	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+"+regexCache.lastMatched(1);
			pattern2	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(［?([ｋＫｃＣｍＭμｎｐ]?[ｍＭ]２|ｈ?ａ)］?|平方(キロ|センチ|ミリ|マイクロ|ナノ|ピコ)?(メートル|[ｍＭ])|(エーカ|アール|ヘクタール|坪|平米))";
		}
		else if(partialApp.find("何[十百千万億兆]*(平方(キロ|センチ|ミリ|マイクロ|ナノ|ピコ)(メートル|ｍ|Ｍ)|エーカ|アール|平米)")
				|| (partialApp.find("広さ?は|面積は") && partialApp.find("どれ(だけ|ほど|[くぐ]らい)|どの(程度|ていど|[くぐ]らい)|(いく|幾)[らつ]"))) {
			type	= "area";
			qword	= regexCache.lastMatched();
			pattern2	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(［?([ｋＫｃＣｍＭμｎｐ]?[ｍＭ]２|ｈ?ａ)］?|平方(キロ|センチ|ミリ|マイクロ|ナノ|ピコ)?(メートル|[ｍＭ])|(エーカ|アール|ヘクタール|坪|平米))";
		}
		else if(partialApp.find("何[歳才]")
				|| (partialApp.find("(年齢|歳)は") && partialApp.find("どれ(だけ|ほど|[くぐ]らい)|どの(程度|ていど|[くぐ]らい)|(いく|幾)[らつ]|何年"))) {
				type	= "year";
				qword	= regexCache.lastMatched();
				pattern1	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+[歳才]";
				pattern2 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+";
		}
		else if(partialApp.find("何[十百千万億兆]*年(の期?)?間")) {
			type	= "period.year";
			qword	= regexCache.lastMatched();
			pattern1	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+年(も?の期?)?間";
			pattern2	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+年";
		}
		else if(partialApp.find("何[十百千万億兆]*[かヵカヶ]?月(の期?)?間")) {
			type	= "period.month";
			qword	= regexCache.lastMatched();
			pattern1	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+[かヵカヶ]?月間";
			pattern2	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+[かヵカヶ]?月";
		}
		else if(partialApp.find("何[十百千万億兆]*日(の期?)?間")) {
			type	= "period.day";
			qword	= regexCache.lastMatched();
			pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+日(も?の期?)?間";
			pattern2 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+日";
		}
		else if(partialApp.find("期間は") && partialApp.find("どれ(だけ|ほど|[くぐ]らい)|どの(程度|ていど|[くぐ]らい)|(いく|幾)[らつ]")) {
			type	= "period";
			qword	= regexCache.lastMatched();
			pattern1	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(年|[かヵカヶ]?月|日)(も?の期?)?間";
			pattern2	= "([０-９〇一二三四五六七八九十零百千万億兆・．—]+(年|[かヵカヶ]?月|日).*)+";
		}
		else if(partialApp.find("(何[十百千万億兆]*年)(ぶり|目|おき)")) {
			type	= "interval.year";
			qword	= regexCache.lastMatched();
			pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+年"+regexCache.lastMatched(2);
			pattern2 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+年";
		}
		else if(partialApp.find("(何[十百千万億兆]*[かヵカヶ]?月)(ぶり|目|おき)")) {
			type	= "interval.month";
			qword	= regexCache.lastMatched();
			pattern1	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+[かヵカヶ]?月"+regexCache.lastMatched(2);
			pattern2 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+[かヵカヶ]?月";
		}
		else if(partialApp.find("(何[十百千万億兆]*日)(ぶり|目|おき)")) {
			type	= "interval.day";
			qword	= regexCache.lastMatched();
			pattern1	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+日"+regexCache.lastMatched(2);
			pattern2	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+日";
		}
		else if(partialApp.find("何年.*何月.*何日")) {
			type	= "date.year-month-day";
			qword	= regexCache.lastMatched();
			pattern1	= "([０-９〇一二三四五六七八九十零百千万億兆・．—]+|元)(（.*?）)?年.{0,1}[０-９〇一二三四五六七八九十零]+月.{0,1}[０-９〇一二三四五六七八九十零]+日";
			pattern2	= "(([０-９〇一二三四五六七八九十零]+|元)(（.*?）)?年(.{0,1}[０-９〇一二三四五六七八九十零]+月(.{0,1}[０-９〇一二三四五六七八九十零]+日)?)?)|[０-９〇一二三四五六七八九十零]+月(.{0,1}[０-９〇一二三四五六七八九十零]+日)?|[０-９〇一二三四五六七八九十零]+日";
		}
		else if(partialApp.find("何年.*何月")) {
			type	= "date.year-month";
			qword	= regexCache.lastMatched();
			pattern1 = "([０-９〇一二三四五六七八九十零百千万億兆・．—]+|元)(（.*?）)?年.{0,1}[０-９〇一二三四五六七八九十零]+月";
			pattern2 = "(([０-９〇一二三四五六七八九十零]+|元)(（.*?）)?年(.{0,1}[０-９〇一二三四五六七八九十零]+月(.{0,1}[０-９〇一二三四五六七八九十零]+日)?)?)|[０-９〇一二三四五六七八九十零]+月(.{0,1}[０-９〇一二三四五六七八九十零]+日)?|[０-９〇一二三四五六七八九十零]+日";
		}
		else if(partialApp.find("何年")) {
			type	= "date.year";
			qword	= regexCache.lastMatched();
			pattern1 = "([０-９〇一二三四五六七八九十零百千万億兆・．—]+|元)(（.*?）)?年(?!生)|(明治|大正|昭和|平成|[０-９〇一二三四五六七八九十零]+)[０-９〇一二三四五六七八九十零]年生";
			pattern2 = "(([０-９〇一二三四五六七八九十零]+|元)(（.*?）)?年(.{0,1}[０-９〇一二三四五六七八九十零]+月(.{0,1}[０-９〇一二三四五六七八九十零]+日)?)?)|[０-９〇一二三四五六七八九十零]+月(.{0,1}[０-９〇一二三四五六七八九十零]+日)?|[０-９〇一二三四五六七八九十零]+日";
		}
		else if(partialApp.find("何月.*何日")) {
			type	= "date.month-day";
			qword	= regexCache.lastMatched();
			pattern1	= "[０-９〇一二三四五六七八九十零]+月.{0,1}[０-９〇一二三四五六七八九十零]+日";
			pattern2	= "(([０-９〇一二三四五六七八九十零]+|元)(（.*?）)?年(.{0,1}[０-９〇一二三四五六七八九十零]+月(.{0,1}[０-９〇一二三四五六七八九十零]+日)?)?)|[０-９〇一二三四五六七八九十零]+月(.{0,1}[０-９〇一二三四五六七八九十零]+日)?|[０-９〇一二三四五六七八九十零]+日";
		}
		else if(partialApp.find("何月")) {
			type	= "date.month";
			qword	= regexCache.lastMatched();
			pattern1	= "[０-９〇一二三四五六七八九十零]+月";
			pattern2	= "(([０-９〇一二三四五六七八九十零]+|元)(（.*?）)?年(.{0,1}[０-９〇一二三四五六七八九十零]+月(.{0,1}snum+日)?)?)|[０-９〇一二三四五六七八九十零]+月(.{0,1}[０-９〇一二三四五六七八九十零]+日)?|[０-９〇一二三四五六七八九十零]+日";
		}
		else if(partialApp.find("何日")) {
			type	= "date.day";
			qword	= regexCache.lastMatched();
			pattern1 = "[０-９〇一二三四五六七八九十零]+日";
			pattern2 = "(([０-９〇一二三四五六七八九十零]+|元)(（.*?）)?年(.{0,1}[０-９〇一二三四五六七八九十零]+月(.{0,1}[０-９〇一二三四五六七八九十零]+日)?)?)|[０-９〇一二三四五六七八九十零]+月(.{0,1}[０-９〇一二三四五六七八九十零]+日)?|[０-９〇一二三四五六七八九十零]+日";
		}
		else if(partialApp.find("いつ")) {
			type	= "date";
			qword	= regexCache.lastMatched();
			pattern1	= "(([０-９〇一二三四五六七八九十零]+|元)(（.*?）)?年(.{0,1}[０-９〇一二三四五六七八九十零]+月(.{0,1}[０-９〇一二三四五六七八九十零]+日)?)?)|[０-９〇一二三四五六七八九十零]+月(.{0,1}[０-９〇一二三四五六七八九十零]+日)?|[０-９〇一二三四五六七八九十零]+日";
		}
		// 時間
		else if(partialApp.find("何時.*何分.*何秒")) {
			type	= "time.hour-minute-second";
			qword	= regexCache.lastMatched();
			pattern1	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+時.{0,1}[０-９〇一二三四五六七八九十零]+分.{0,1}[０-９〇一二三四五六七八九十零]+秒";
			pattern2	= "([０-９〇一二三四五六七八九十零百千万億兆・．—]+[時分秒].{0,1})+";
		}
		else if(partialApp.find("何時.*何分")) {
			type	= "time.hour-minute";
			qword	= regexCache.lastMatched();
			pattern1	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+時.{0,1}[０-９〇一二三四五六七八九十零]+分";
			pattern2	= "([０-９〇一二三四五六七八九十零百千万億兆・．—]+[時分秒].{0,1})+";
		}
		else if(partialApp.find("何時")) {
			type	= "time.hour";
			qword	= regexCache.lastMatched();
			pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+時";
			pattern2 = "([０-９〇一二三四五六七八九十零百千万億兆・．—]+[時分秒].{0,1})+";
		}
		else if ( partialApp.find("何分.*何秒")) {
			type     = "time.minute-second";
			qword	= regexCache.lastMatched();
			pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+分.{0,1}[０-９〇一二三四五六七八九十零]+秒";
			pattern2 = "([０-９〇一二三四五六七八九十零百千万億兆・．—]+[時分秒].{0,1})+";
		}
		else if ( partialApp.find("何分")) {
			type     = "time.minute";
			qword	= regexCache.lastMatched();
			pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+分";
			pattern2 = "([０-９〇一二三四五六七八九十零百千万億兆・．—]+[時分秒].{0,1})+";
		}
		else if ( partialApp.find("何秒")) {
			type     = "time.second";
			qword	= regexCache.lastMatched();
			pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+秒";
			pattern2 = "([０-９〇一二三四五六七八九十零百千万億兆・．—]+[時分秒].{0,1})+";
		}
		else if(partialApp.find("どれ(だけ|ほど|[くぐ]らい)|どの(程度|ていど|[くぐ]らい)|(いく|幾)[らつ]")
				&& partialApp.find("時間[は]?")) {
			type	= "time";
			qword	= regexCache.lastMatched();
			pattern1	= "([０-９〇一二三四五六七八九十零百千万億兆・．—]+[時分秒].{0,1})+";
		}
		else if(partialApp.find("何[十百千万億兆]*(([ｋＫｍμｎｐ]|キロ|ミリ|マイクロ|ナノ|ピコ)?(ｇ|グラム)|ｔ|トン)")) {
			String	unit	= regexCache.lastMatched(2);
			type	= "weight";
			qword	= regexCache.lastMatched();
			if(regexCache.find("([ｋＫ]|キロ)(ｇ|グラム)", unit)) {
				pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+([ｋＫ]|キロ)(ｇ|グラム)?";
			}
			else if(regexCache.find("(ｇ|グラム)", unit)) {
				pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(ｇ|グラム)";
			}
			else if(regexCache.find("(ｍ|ミリ)(ｇ|グラム)", unit)) {
				pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(ｍ|ミリ)(ｇ|グラム)";
			}
			else if(regexCache.find("ｔ|トン", unit)) {
				pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(ｔ|トン)";
			}
			pattern2	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(［?[ｋＫｍＭμｎｐ]?[Ｇｇ]］?|(キロ|ミリ|マイクロ|ナノ|ピコ)?グラム|(ポンド|キロ|トン|［?[ｔＮ](?![ａ-ｎＡ-Ｎ])］?))";
		}
		else if(partialApp.find("(重さ?|[重質]量)は")
				&& partialApp.find("どれ(だけ|ほど|[くぐ]らい)|どの(程度|ていど|[くぐ]らい)|(いく|幾)[らつ]|何(キロ|ポンド)")) {
			String	unit = regexCache.lastMatched(1);
			type	= "weight";
			qword	= regexCache.lastMatched();
			if(unit.equals("キロ")) {
				pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(キロ(グラム)?|［?[ｋＫ][Ｇｇ]］?";
			}
			else if(unit.equals("ポンド")) {
				pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+ポンド";
			}
			pattern2 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(［?[ｋＫｍＭμｎｐ]?[Ｇｇ]］?|(キロ|ミリ|マイクロ|ナノ|ピコ)?グラム|(ポンド|キロ|トン|［?[ｔＮ](?![ａ-ｎＡ-Ｎ])］?))";
		}
		else if(partialApp.find("何[十百千万億兆]*円")) {
			type	= "money.yen";
			qword	= regexCache.lastMatched();
			pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+円|¥[０-９〇一二三四五六七八九十零百千万億兆・．—]+";
			pattern2 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(円|銭|両|ドル|ユーロ|ポンド|フラン|ペセタ|リラ|マルク|ウ[ォオ]ン|バーツ|ペソ)|[¥＄][０-９〇一二三四五六七八九十零百千万億兆・．—]+";
		}
		else if(partialApp.find("何[十百千万億兆]*ドル")) {
			type	= "money.dollar";
			qword	= regexCache.lastMatched();
			pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+ドル|＄[０-９〇一二三四五六七八九十零百千万億兆・．—]+";
		}
		else if(partialApp.find("何[十百千万億兆]*(銭|両|ユーロ|ポンド|フラン|ペセタ|リラ|マルク|ウォン|バーツ|ペソ)")
				|| (partialApp.find("([金額益収給]|予算|年棒|価格?|値段?)は") && partialApp.find("何[十百千万億兆]*(ポンド)"))) {
			type	= "money";
			qword	= regexCache.lastMatched();
			pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+"+regexCache.lastMatched(1);
			pattern2 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(円|銭|両|ドル|ユーロ|ポンド|フラン|ペセタ|リラ|マルク|ウォン|バーツ|ペソ)|[¥＄][０-９〇一二三四五六七八九十零百千万億兆・．—]+";
		}
		else if(partialApp.find("([金額益収給]|収入|支出|予算|年棒|価格?|値段?)は")
				&& partialApp.find("どれ(だけ|ほど|[くぐ]らい)|どの(程度|ていど|[くぐ]らい)|(いく|幾)[らつ]")) {
			type	= "money";
			qword	= regexCache.lastMatched(1);
			pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(円|ドル)|[¥＄][０-９〇一二三四五六七八九十零百千万億兆・．—]+";
			pattern2 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(円|銭|両|ドル|ユーロ|ポンド|フラン|ペセタ|リラ|マルク|ウォン|バーツ|ペソ)|[¥＄][０-９〇一二三四五六七八九十零百千万億兆・．—]+";
		}
		else if(partialApp.find("何[十百千万億兆]*[回度]")
				|| (partialApp.find("[回度]数は") && partialApp.find("どれ(だけ|ほど|[くぐ]らい)|どの(程度|ていど|[くぐ]らい)|(いく|幾)[らつ]"))) {
			type	= "vol";
			qword	= regexCache.lastMatched();
			pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+[回度]";
			pattern2 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+";
		}
		else if(partialApp.find("何[十百千万億兆]*[個こつ]")) {
			type	= "vol";
			qword	= regexCache.lastMatched();
			pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+[個こつ]";
			pattern2 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+";
		}
		else if(partialApp.find("何(十百)?[かヵカヶ箇]?([国所条])")) {
			type	= "vol";
			qword	= regexCache.lastMatched();
			pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+[かヵカヶ箇]?"+regexCache.lastMatched(2);
			pattern2 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+";
		}
		else if(partialApp.find("何[十百千万億兆]*([階本冊箱枚台頭匹羽株杯棟件軒輪隻機発人校社勝敗章節]|得?点|ダース|ケース|ゴール|ポイント|セーブ(ポイント)?)"))
		{
			type	= "vol";
			qword	= regexCache.lastMatched();
			pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+"+regexCache.lastMatched(1);
			pattern2 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+";
		}
		else if(partialApp.find("第何(.)")) {
			type	= "vol";
			qword	= regexCache.lastMatched();
			pattern1 = "第[０-９〇一二三四五六七八九十零百千万億兆・．—]+"+regexCache.lastMatched(1);
			pattern2 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+";
		}
		else if(partialApp.find("(人口|[人客者]数)は")
				&& partialApp.find("どれ(だけ|ほど|[くぐ]らい)|どの(程度|ていど|[くぐ]らい)|(いく|幾)[らつ]")) {
			type	= "vol";
			qword	= regexCache.lastMatched();
			pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+人";
			pattern2 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+";
		}
		else if(partialApp.find("([階本冊箱枚台頭匹羽株杯棟件軒輪隻機発人校社勝敗章節]|得?点|ダース|ケース|ゴール|ポイント|セーブ(ポイント)?)数は")) {
			pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+"+regexCache.lastMatched(1);
			if(partialApp.find("どれ(だけ|ほど|[くぐ]らい)|どの(程度|ていど|[くぐ]らい)|(いく|幾)[らつ]")) {
				qword	= regexCache.lastMatched();
				type	= "vol";
				pattern2 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+";
			} else { pattern1 = ""; }
		}
		else if(partialApp.find("(誰|だれ).*((といいま(す|した)|(だった|であ(る|った)|とい(う|った))?の?(で(す|しょう|あろう|した)|だ(ろう|った))?)?か$|[がとのに]|$)")) {
			type	= "person";
			qword	= regexCache.lastMatched(1);
		}
		else if(partialApp.find("(名字|姓|[本芸者氏]名|([人士婦者員長王官僚将手]|人物|コーチ|監督|審判|タレント|知事|首相|総裁|大臣|大統領|大名|将軍)(の名前|の?名))は")
				&& partialApp.find("(何|なに).*((といいま(す|した)|(だった|であ(る|った)|とい(う|った))?の?(で(す|しょう|あろう|した)|だ(ろう|った))?)?か$|$)")
				|| (partialApp.find("((何|なん).*という(人|[男女]性?))"))) {
			type	= "person";
			qword	= regexCache.lastMatched(1);
		}
		else if(partialApp.find("(何|どの)曜日")) {
			type	= "time.week";
			qword	= regexCache.lastMatched();
			pattern1 = "[日月火水木金土]曜日?|(サン|マン|フライ|サタ)デ[ーイィ]";
		}
		else if(partialApp.find("(((どこ?|何処)の|(何|なん)という)国)")
				|| (partialApp.find("国(の名前|名)?は") && partialApp.find("(どこ|何処|何|なに|なん).*(といいま(す|した)|(だった|であ(る|った)|とい(う|った))?の?(で(す|しょう|あろう|した)|だ(ろう|った))?)?か$"))) {
			type	= "place.country";
			qword	= regexCache.lastMatched(1);
		}
		else if(partialApp.find("((何|(どこ?|何処)の)県)")
				|| ( partialApp.find("県(の名前|名)?は") && partialApp.find("(どこ|何処|何|なに|なん).*(といいま(す|した)|(だった|であ(る|った)|とい(う|った))?の?(で(す|しょう|あろう|した)|だ(ろう|った))?)?か$"))) {
			type	= "place.prefecture";
			qword	= regexCache.lastMatched(1);
			pattern1 = "北海道|青森|岩手|秋田|宮城|山形|福島|新潟|茨城|栃木|群馬|長野|富山|千葉|埼玉|東京|山梨|神奈川|静岡|愛知|岐阜|石川|三重|滋賀|福井|和歌山|奈良|京都|大阪|兵庫|岡山|鳥取|広島|島根|山口|徳島|香川|愛媛|高知|福岡|大分|佐賀|長崎|宮崎|熊本|鹿児島|沖縄";
		}
		else if(partialApp.find("(何|(どこ?|何処)の|(何|なん)という)([市町村]|地[域方]?|場所)")) {
			type	= "place.region";
			qword	= regexCache.lastMatched();
			pattern1 = regexCache.lastMatched(4);
		}
		else if(partialApp.find("([市町村]|地[域方]?|場所)(の名前|名)?は.*(どこ|何処?|なに|なん).*(といいま(す|した)|(だった|であ(る|った)|とい(う|った))?の?(で(す|しょう|あろう|した)|だ(ろう|った))?)?か$")) {
			type	= "place.region";
			qword	= regexCache.lastMatched(3);
			pattern1	= regexCache.lastMatched(1);
		}
		else if(partialApp.find("(どこ|何処)にあ(り|る(といいま(す|した)|(だった|であ(る|った)|とい(う|った))?の?(で(す|しょう|あろう|した)|だ(ろう|った))?)?か$)")) {
			type	= "place.region";
			qword	= regexCache.lastMatched(1);
		}
		else if(partialApp.find("((何|(どこ?|何処)の|(何|なん)という)(((?!大会)(?!(箇|か|ヶ|ケ)所)[^あ-ん]{0,3}[省庁所署党社会])|派閥?|集団|グループ|企業|組織|団体|チーム|大学|([小中]学|高)校))")
				|| (partialApp.find("([省庁署党社]|(?<!大)会|(?<!箇|か|ヶ|ケ)所|派閥?|集団|グループ|企業|組織|団体|チーム|大学|([小中]学|高)校)(の名前|名)?は")
						&& partialApp.find("(どこ|何処|何|なに|なん).*(といいま(す|した)|(だった|であ(る|った)|とい(う|った))?の?(で(す|しょう|あろう|した)|だ(ろう|った))?)?か$")) ){
			type	= "organization";
			qword	= regexCache.lastMatched(1);
		}
		else if(partialApp.find(".の(何処|どこ).*((といいま(す|した)|(だった|であ(る|った)|とい(う|った))?の?(で(す|しょう|あろう|した)|だ(ろう|った))?)?か$|が|と|に|で|の|へ|を|から|まで|より)")) {
			type	= "part|place|organization";
			qword	= regexCache.lastMatched(1);
		}
		else if(partialApp.find("([^の]|^)(何処|どこ).*((といいま(す|した)|(だった|であ(る|った)|とい(う|った))?の?(で(す|しょう|あろう|した)|だ(ろう|った))?)?か$|が|と|に|で|の|へ|を|から|まで|より)")) {
			type	= "place|organization";
			qword	= regexCache.lastMatched(2);
		}
		else if(partialApp.find("(何|なに|なん|どれ)(といいま(す|した)|(だった|であ(る|った)|とい(う|った))?の?(で(す|しょう|あろう|した)|だ(ろう|った))?)?か$")
				|| partialApp.find("(何|なに|どれ)([がのとに]|$)")
				|| partialApp.find("(何|なに|なん|どれ)で")
				|| partialApp.find("(なぜ|何故|どうして)")) {
			type	= "none";
			qword	= regexCache.lastMatched(1);
		}

		if(type == null) {
			for(int i = 0; i < morphemeList.size(); i++)
			{
				Morpheme morpheme	= morphemeList.get(i);
				Morpheme forwardMorpheme	= i > 0 ? morphemeList.get(i-1) : null;
				Morpheme nextMorpheme	= i < morphemeList.size()-1 ? morphemeList.get(i+1) : null;

				String surface	= morpheme.getSurfaceForm();
				if(forwardMorpheme != null) {
					String forwordText	= forwardMorpheme.getSurfaceForm();
					if(regexCache.matches("(数|量)", surface) && regexCache.matches("([^番]\\S*)",forwordText)) {
						String	temp	= regexCache.lastMatched(1);
						if(josujiMap.containsKey(temp) && partialApp.find("(どれ(だけ|ほど|[くぐ]らい)|どの(程度|ていど|[くぐ]らい)|(いく|幾)[らつ])|(いく|幾)つ")) {
							type	= "vol";
							qword	= regexCache.lastMatched();
							pattern1	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+"+temp;
							break;
						}
					}
				}
				if(regexCache.matches("(何|幾|いく)", surface) && nextMorpheme != null) {
					String temp = regexCache.lastMatched(1);
					if(regexCache.matches("([^番]\\S*)", nextMorpheme.getSurfaceForm())) {
						if(josujiMap.containsKey(regexCache.lastMatched(1))) {
							type	= "vol";
							qword	= temp + regexCache.lastMatched(1);
							pattern1	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+"+regexCache.lastMatched(1);
							break;
						}
					}
				}
			}
			if(type == null) {
				if(partialApp.find("(何番)") || partialApp.find("番号は.+(何|(いく|幾)つ|どれ(だけ|ほど|[くぐ]らい)|どの(程度|ていど|[くぐ]らい)|(いく|幾)[らつ])"))
				{
					type	= "num.num";
					qword	= regexCache.lastMatched(1);
					pattern1	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(番(?![年月日時分秒])|(?!([年月日時分秒]|[０-９〇一二三四五六七八九十零百千万億兆・．—])))";

				}
				else if(partialApp.find("(いくら|幾ら)([のとにで]|(といいま(す|した)|(だった|であ(る|った)|とい(う|った))?の?(で(す|しょう|あろう|した)|だ(ろう|った))?)?か\\$)")) {
					type	= "money";
					qword	= regexCache.lastMatched(1);
					pattern1	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(円|ドル)|[¥＄][０-９〇一二三四五六七八九十零百千万億兆・．—]+";
					pattern2	= "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(円|銭|両|ドル|ユーロ|ポンド|フラン|ペセタ|リラ|マルク|ウォン|バーツ|ペソ)|[¥＄][０-９〇一二三四五六七八九十零百千万億兆・．—]+";

				}
				else if(partialApp.find("(いく|幾)つ") || partialApp.find("どれ(だけ|ほど|[くぐ]らい)|どの(程度|ていど|[くぐ]らい)|(いく|幾)[らつ]")) {
					// 数値全般
					type	= "num";
					qword	= regexCache.lastMatched();
					pattern1 = "[０-９〇一二三四五六七八九十零百千万億兆・．—]+(?!([年月日時分秒]|[０-９〇一二三四五六七八九十零百千万億兆・．—]))";

					// 特定の数値情報を抽出しない
				}
				else {
					type	= "none";
					if(partialApp.find("((何|なに|なん|どんな|どれ(だけ|ほど|[くぐ]らい)|どの(程度|ていど|[くぐ]らい)|(いく|幾)[らつ]).*?)((といいま(す|した)|(だった|であ(る|った)|とい(う|った))?の?(で(す|しょう|あろう|した)|だ(ろう|った))?)?か\\$|[のがとに])")) {
						qword	= regexCache.lastMatched(1);
					}
				}
			}

		}
		return new RequireInfo(type,qword,pattern1,pattern2);
	}
}
