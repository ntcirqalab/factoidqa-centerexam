package org.kachako.components.factoid_qa.module;

import java.util.List;

import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

public class PkNumSuffix01
{

	/**
	 * コンストラクタ
	 */
	public PkNumSuffix01()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	//	# 文節データ(1文節分)を入力として、
	//	# 最後の数詞の直後に現れる名詞性名詞助数辞を返す。
	//	sub pk_num_suffix(@){
	public static String pkNumSuffix(List<Morpheme> morphemeList)
	{
		String	suffix	= "";

		for(int i = morphemeList.size()-1; i > 0; --i) {
			Morpheme	morpheme	= morphemeList.get(i);
			String surface	= morpheme.getSurfaceForm();
			String detailedPos	= morpheme.getDetailedPos();

			if(detailedPos.equals("数詞")) {
				return suffix;
			}

			if(detailedPos.equals("名詞性名詞助数辞")) {
				suffix	= surface + suffix;
			}
			else {
				suffix	= "";
			}
		}
		return suffix;
	}

}
