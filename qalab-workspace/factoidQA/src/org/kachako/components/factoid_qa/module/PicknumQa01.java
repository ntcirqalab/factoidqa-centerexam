package org.kachako.components.factoid_qa.module;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.uima.jcas.cas.StringArray;
import org.kachako.components.factoid_qa.manage.DecisionData;
import org.kachako.share.regex.RegexCache;
import org.kachako.types.japanese.syntactic.KnpSegment;
import org.kachako.types.japanese.syntactic.KyotoDependency;
import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

/**
 * 数量表現抽出.
 * 
 * @author saitou
 *
 */
public class PicknumQa01
{
	private static final RegexCache regexCache = new RegexCache();
 
	/**
	 * コンストラクタ
	 */
	public PicknumQa01()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	//sub picknum($$@)
	/**
	 * 
	 * @param decisionList
	 * @param atrList
	 * @param opt
	 * @param threshold
	 * @param kyotoDependencyList
	 * @param knpSegmentList
	 * @param knpInfoMap
	 * @param cpcBySubMap
	 * @param cphByIdMap
	 * @param cphByNameMap
	 * @return
	 */
	public static final String[] picknum(String[] decisionList, String[] atrList,
											String opt, String threshold,
											List<KyotoDependency> kyotoDependencyList,
											List<KnpSegment> knpSegmentList,
											Map<KnpSegment,List<Morpheme>> knpInfoMap,
											HashMap<String,String> cpcBySubMap,
											HashMap<String,String> cphByIdMap,
											HashMap<String,String> cphByNameMap)
	{
		// 決定リスト読み込み
		HashMap<String, List<DecisionData>>	atrDecisionDataMap	= ReadDL02.constDecisionList("ATR", decisionList);
		HashMap<String, List<DecisionData>>	objDecisionDataMap	= ReadDL02.constDecisionList("OBJ", decisionList);

		Map<String,Integer>	kadokawaAtrHm	= readKadokawa(atrList);

		List<String>	outList	= new ArrayList<String>();

		// object候補の、数値文節からの距離
		Map<String,Integer> objCandDistHm	= new HashMap<String,Integer>();

		Integer[]	bunsetsuNumList	= null;
		if(opt.indexOf("i") >= 0) {
			// 疑問詞を含む文節の番号取得
			bunsetsuNumList	= PknumDcInterrogate.dcInterrogate(knpSegmentList);
		}
		else {
			// 数詞を含む文節の番号取得
			bunsetsuNumList	= PknumDcNumber.dcNumber(knpInfoMap);
		}

		for(int num : bunsetsuNumList) {
			// 数値を含む文節から数値の形態素の始点と終点を返す
			Integer[]	numKeitaiso	= PicknumKeitaiso01.numKeitaiso(num, knpSegmentList, knpInfoMap);
			int	keitaisoBegin	= numKeitaiso[0];
			int	keitaisoEnd		= numKeitaiso[1];

			KnpSegment	knpSegment	= knpSegmentList.get(num);
			// 形態素情報一覧
			List<Morpheme>	morphemeList	= knpInfoMap.get(knpSegment);
			// 文節への付加情報
			StringArray	bunsetsuFeatures	= knpSegment.getKnpBunsetsuFeatures();

			String	numExp	= null;
			if(opt.indexOf("i") >= 0) {
				numExp	= PkIntExp01.getIntExp(morphemeList);
			}
			else {
				// 数値表層表現
				numExp	= PkNumExp01.pkNumExp(morphemeList);
			}

			// 文節の格助詞を返す
			String	numInf	= PknumDcParticle01.dcParticle(bunsetsuFeatures);
			// 最後の数詞の直後に現れる名詞性名詞助数辞を返す
			String	numSuffix	= PkNumSuffix01.pkNumSuffix(morphemeList);

			// 述語文節
			HashMap<String,Object>	predHm	= GetPredInf01.getPredInf(kyotoDependencyList,knpSegment);
			KnpSegment	predKnpSegment	= (KnpSegment)predHm.get("predKnpSegment");
			String		predInf			= (String)predHm.get("predInf");

			// object候補収集
			List<String>	objCandidateList	= new ArrayList<String>();
			String	objInc = GetObj.getObjInc(morphemeList);
			if(!objInc.equals("")) {
				objInc	= objInc+"("+ num +")="+numInf+"=inc\n";
				numInf	= "内";
				objCandidateList.add(objInc);
			}
			String[]	objBack	= GetObj.getObjBack("back", num, knpSegmentList, knpInfoMap);
			if(objBack != null && objBack.length > 0) {
				for(String obj : objBack) {
					objCandidateList.add(obj);
				}
			}
			String	objFwd	= GetObj.getObjFwd("near", num, kyotoDependencyList, knpSegmentList, knpInfoMap);
			if(objFwd.length() > 0) {
				objCandidateList.add(objFwd);
			}
			if(predKnpSegment != null && !knpSegment.equals(predKnpSegment)) {
				int predNum	= 0;
				for(KnpSegment segment : knpSegmentList) {
					if(segment.equals(predKnpSegment)) {
						break;
					}
				}
				String[]	objBack2	= GetObj.getObjBack("parallel", predNum, knpSegmentList, knpInfoMap);
				if(objBack2 != null && objBack2.length > 0) {
					for(String obj : objBack2) {
						objCandidateList.add(obj);
					}
				}
				String	objFwd2	= GetObj.getObjFwd("far", predNum, kyotoDependencyList, knpSegmentList, knpInfoMap);
				if(objFwd2.length() > 0) {
					objCandidateList.add(objFwd2);
				}
			}
			String	objTopic	= GetObj.getObjTopic(knpSegmentList, knpInfoMap);
			if(objTopic.length() > 0) {
				if(regexCache.find(".+?\\((\\d+)\\)=提題=topic",objTopic)) {
					if(regexCache.lastMatched(1).equals(String.valueOf(num))) {
						objCandidateList.add(objTopic);
					}
				}
			}

			String	branch	= "";
			if(opt.indexOf("s") >= 0) {
				branch	= numInf + " - " + numSuffix + " - " + predInf;
			}
			else {
				branch	= numInf + " - " + predInf;
			}

			int	count	= 0;
			for(String objCand : objCandidateList) {
				if(regexCache.matches("(.+?)\\((\\d+)\\)=(.+?)\n",objCand)) {
					String	objExp	= regexCache.lastMatched(1);
					String	objNum	= regexCache.lastMatched(2);
					String	objInf	= regexCache.lastMatched(3);
					int	intObjNum	= Integer.parseInt(objNum);
					objCandidateList.set(count, objExp+"("+objNum+")="+objInf+"\n");

					if(regexCache.find("=inc",objInf) || num != intObjNum) {
						objCandDistHm.put(objCand, Math.abs(intObjNum - num));
					}
				}
				++count;
			}

			String atrNum	= null;
			String atrExp	= null;
			String atrInf	= null;
			String objExp	= null;
			String objNum	= null;
			String objInf	= null;
			// object候補を数値文節に近い順に並べかえ
			objCandidateList	= sortObjectList(objCandDistHm);
			if(objCandidateList != null) {
				HashMap<String,String> atrMap	= SelAtr03.selAtr(kadokawaAtrHm, atrDecisionDataMap, branch, objCandidateList,
																							cpcBySubMap, cphByIdMap, cphByNameMap);
				atrNum	= (atrMap != null && atrMap.containsKey("atrNum") ? atrMap.get("atrNum") : null);
				atrExp	= (atrMap != null && atrMap.containsKey("atrExp") ? atrMap.get("atrExp") : null);
				atrInf	= (atrMap != null && atrMap.containsKey("atrInf") ? atrMap.get("atrInf") : null);
				if(atrExp != null && !atrExp.equals("")) {
					List<String> aroundAtrList	= new ArrayList<String>();
					if(regexCache.matches("fwd", atrInf)) {
						String[] aroundAtr	= GetObj.getObjBack("back", Integer.parseInt(atrNum), knpSegmentList, knpInfoMap);
						for(String str : aroundAtr) {
							aroundAtrList.add(str);
						}
					}
					if(regexCache.matches("back", atrInf)) {
						String objFwdStr	= GetObj.getObjFwd("near", Integer.parseInt(atrNum), kyotoDependencyList, knpSegmentList, knpInfoMap);
						aroundAtrList.add(objFwdStr);
					}
					if(aroundAtrList != null) {
						for(int i = 0; i < aroundAtrList.size() ; i++) {
							String	str	= aroundAtrList.get(i);
							Pattern p = Pattern.compile("(.+)\n");
							Matcher m = p.matcher(str);
							aroundAtrList.set(i, m.replaceFirst("$1-atr("+atrInf+")\n"));
						}
						objCandidateList.addAll(aroundAtrList);
					}
				}

				for(int i = 0; i < objCandidateList.size(); ++i) {
					String objCand	= objCandidateList.get(i);
					if(regexCache.matches("(.+?)\\((\\d+)\\)=(.+?)\n",objCand)) {
						objExp	= regexCache.lastMatched(1);
						objNum	= regexCache.lastMatched(2);
						objInf	= regexCache.lastMatched(3);
						int	intObjNum	= Integer.parseInt(objNum);
						objCandidateList.set(i, objExp+"("+objNum+")="+objInf+"\n");
						objCandDistHm.put(objCand, Math.abs(intObjNum - num));
					}
				}

				// object 候補を数値文節に近い順に並べかえ
				objCandidateList	= sortObjectList(objCandDistHm);
				HashMap<String,String> objMap	= SelObj02.selObj(threshold, objDecisionDataMap, branch, objCandidateList);
				objNum	= (objMap != null && objMap.containsKey("objNum") ? objMap.get("objNum") : null);
				objExp	= (objMap != null && objMap.containsKey("objExp") ? objMap.get("objExp") : null);
				objInf	= (objMap != null && objMap.containsKey("objInf") ? objMap.get("objInf") : null);
			}

			if((objExp != null && !objExp.equals("")) && (atrExp != null && !atrExp.equals(""))) {
				Pattern p = Pattern.compile("[-]");
				Matcher m = p.matcher(objExp);
				objExp	= m.replaceAll("");
				m = p.matcher(atrExp);
				atrExp	= m.replaceAll("");
				outList.add(numExp+"("+keitaisoBegin+"-"+keitaisoEnd+"),"+objExp+","+atrExp+"\n");
			}
			else if(objExp != null && !objExp.equals("")) {
				Pattern p = Pattern.compile("[-]");
				Matcher m = p.matcher(objExp);
				objExp	= m.replaceAll("");
				outList.add(numExp+"("+keitaisoBegin+"-"+keitaisoEnd+"),"+objExp+"\n");
			}
			else if(atrExp != null && !atrExp.equals("")) {
				Pattern p = Pattern.compile("[-]");
				Matcher m = p.matcher(atrExp);
				atrExp	= m.replaceAll("");
				outList.add(numExp+"("+keitaisoBegin+"-"+keitaisoEnd+"),,"+atrExp+"\n");
			}
			else {
				outList.add(numExp+"("+keitaisoBegin+"-"+keitaisoEnd+"),\n");
			}
			if (objCandidateList != null)
				objCandidateList.clear();
		}
		return (String[])outList.toArray(new String[0]);
	}

	//----------------------------------------------------------------
	// private methods
	//----------------------------------------------------------------

	//sub r_kadokawa($){
	private static final Map<String,Integer> readKadokawa(String[] atrList)
	{
		Map<String,Integer>	atrHm	= new LinkedHashMap<String,Integer>();
		for(String str : atrList) {
//			if(regexCache.matches("^([^\t]+)\t([^\t]+)$", str)) {
			if(regexCache.find("^(.+?)\t", str)) {
				atrHm.put(regexCache.lastMatched(1),1);
			}
		}
		return atrHm;
	}

	private static List<String> sortObjectList(Map<String,Integer> objHm)
	{
		if(objHm == null || objHm.size() <= 0) {
			return null;
		}

		List<Map.Entry<String,Integer>> entries = new ArrayList<Map.Entry<String,Integer>>(objHm.entrySet());
		Collections.sort(entries, new Comparator<Map.Entry<String,Integer>>() {
			public int compare(Map.Entry<String,Integer> o1, Map.Entry<String,Integer> o2){
				Map.Entry<String,Integer> e1 = o1;
				Map.Entry<String,Integer> e2 = o2;
				return ((Integer)e1.getValue()).compareTo((Integer)e2.getValue());
			}
		});

		List<String>	tmpList	= new ArrayList<String>();
		for(Map.Entry<String, Integer> entry : entries) {
			tmpList.add(entry.getKey());
		}
		return tmpList;
	}
}
