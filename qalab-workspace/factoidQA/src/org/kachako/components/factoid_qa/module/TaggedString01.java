package org.kachako.components.factoid_qa.module;

import java.util.ArrayList;
import java.util.List;

public class TaggedString01
{
	/**
	 * コンストラクタ
	 */
	public TaggedString01()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	//sub char_types($) {
	public static List<String> charTypes(String str)
	{
		List<String> list	= new ArrayList<>();
		String[] array	= str.split("");
		for(String value : array) {
			if(value == null || value.length() <= 0) {
				continue;
			}
			list.add(charType1(value));
		}

		return list;
	}


	//----------------------------------------------------------------
	// private methods
	//----------------------------------------------------------------

	//sub char_type_1($) {
	private static String charType1(String str)
	{
		String charType = "OTHER";

		if(str.matches("^.$")) {
			if(str.matches("[　]")) { // 空白，全角空白
				charType = "ZSPACE";
			}
			else if(str.matches("[０-９一二三四五六七八九十〇百千万億兆京]")) {
				charType = "ZDIGIT";
			}
			else if(str.matches("[ａ-ｚ]")) {
				charType = "ZLLET";
			}
			else if(str.matches("[Ａ-Ｚ]")) {
				charType = "ZULET";
			}
			else if(str.matches("[ぁ-ん]")) {
				charType = "HIRAG";
			}
			else if(str.matches("[ァ-ヶ]")) {
				charType = "KATAK";
			}
		}
		else {
			System.err.println("char_types_1: Unexpected error in the string ("+str+")");
		}

		return charType;
	}
}
