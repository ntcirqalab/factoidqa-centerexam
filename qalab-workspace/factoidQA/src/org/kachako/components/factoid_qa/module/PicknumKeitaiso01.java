package org.kachako.components.factoid_qa.module;

import java.util.List;
import java.util.Map;

import org.kachako.types.japanese.syntactic.KnpSegment;
import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

public class PicknumKeitaiso01
{

	/**
	 * コンストラクタ
	 */
	public PicknumKeitaiso01()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	/**
	 * 数値を含む文節から数値の形態素の始点と終点を返す.
	 * 入力は(数値を含む文節の番号,構文解析データ)
	 * 始点 = 数値文節の頭
	 * 終点 = 数値文節中の(助詞|判定詞)の一つ前の形態素、
	 * どちらも存在しない場合は最後の形態素
	 * 
	 * @param bunsetsuNum 文節番号
	 * @param knpSegmentList KnpSegmentリスト
	 * @param knpInfoMap KnpSegmentに対する形態素情報Map
	 * @return Integer型配列. 0=数値文節の始点、1=数値文節中の一つ前の形態素、 どちらも存在しない場合は最後の形態素の位置（終点）
	 */
	public static Integer[] numKeitaiso(int bunsetsuNum, 
							List<KnpSegment> knpSegmentList, Map<KnpSegment,List<Morpheme>> knpInfoMap)
	{
		int	keitaisoBegin	= -1;
		int	keitaisoEnd		 = 0;

		for(int i = 0; i < bunsetsuNum; i++) {
			KnpSegment	knpSegment	= knpSegmentList.get(i);
			List<Morpheme>	morphemeList	= knpInfoMap.get(knpSegment);
			keitaisoBegin	+= morphemeList.size() - 1;
		}

		boolean	flag	= true;
		KnpSegment	knpSegment	= knpSegmentList.get(bunsetsuNum);
		List<Morpheme>	morphemeList	= knpInfoMap.get(knpSegment);
		for(int i = 0; i < morphemeList.size(); i++) {
			Morpheme	morpheme	= morphemeList.get(i);
			String pos		= morpheme.getPos();
			String surface	= morpheme.getSurfaceForm();
			if(pos.equals("助詞") || pos.equals("判定詞") || surface.matches("^[、|。]")) {
				flag = false;
			}
			else if(flag) {
				keitaisoEnd++;
			}
		}

		keitaisoEnd += keitaisoBegin;
		keitaisoBegin++;
		Integer[]	numlist	= {keitaisoBegin, keitaisoEnd};
		return numlist;
	}

}
