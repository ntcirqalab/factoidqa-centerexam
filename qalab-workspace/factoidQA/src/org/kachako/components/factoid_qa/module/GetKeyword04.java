package org.kachako.components.factoid_qa.module;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.kachako.components.factoid_qa.manage.KeyWord;
import org.kachako.share.regex.RegexCache;
import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

/**
 * 形態素解析の結果からキーワードを生成します.
 * 
 * @author JSA saitou
 *
 */
public class GetKeyword04
{

	/**
	 * コンストラクタ
	 */
	public GetKeyword04() {
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	/**
	 * キーワードのリストを取得します.
	 * 
	 * @param morphemeList 形態素情報のリスト
	 * @return
	 */
	public static List<KeyWord> getKeyword(List<Morpheme> morphemeList)
	{
		Collections.sort(morphemeList, new Comparator<Morpheme>(){
			public int compare(Morpheme x, Morpheme y){
				return x.getBegin() - y.getBegin();
			}
		});

		RegexCache regCache	= new RegexCache();

		List<KeyWord> keywordList = new ArrayList<>();
		for(int i = 0; i < morphemeList.size(); i++)
		{
			Morpheme morpheme	= morphemeList.get(i);
			Morpheme nextMorpheme	= i < morphemeList.size()-1 ? morphemeList.get(i+1) : null;

			String surface	= morpheme.getSurfaceForm(); //表層
			String base		= morpheme.getBaseForm(); //原型
			String pos		= morpheme.getPos(); //品詞
			String dpos		= morpheme.getDetailedPos(); //品詞細分類

//System.out.println("Morpheme surface="+surface+" base="+base+" pos="+pos+" detailedPos="+dpos+" begin="+morpheme.getBegin());
			//int begin	= morpheme.getBegin();
			//int end		= morpheme.getEnd();

			if(pos != null && pos.equals("名詞")){
				if(dpos != null && dpos.equals("数詞")){
					if(!regCache.find("^(何|なん|幾|いく)$", surface)){
						if(nextMorpheme != null && nextMorpheme.getPos().equals("接尾辞")){
							addKeyword(keywordList, surface+nextMorpheme.getSurfaceForm(), i, i+1, "numeral");
							i++;
						}
						else {
							addKeyword(keywordList, surface, i, i, "numeral");
						}
					}
					continue;
				}
				else if(dpos != null && dpos.equals("形式名詞") || 
						regCache.find("^(な[にん]|誰|だれ|何処?|どこ|[幾いく]ら?|名前|なまえ|いつ(頃|ごろ)?|数|人)$", surface)){
					continue;
				}

				if(dpos != null && dpos.equals("副詞的名詞")){
					if(!regCache.find("^([くぐ]らい)$", surface)){
						addKeyword(keywordList, surface, i, i, "adjective");
					}
				}
				else if(dpos != null && regCache.find("^(人|地|組織)名$", dpos)){
					addKeyword(keywordList, surface, i, i, "proper");
				}
				else {
					addKeyword(keywordList, surface, i, i, "noun");
				}

			}
			else if(pos != null && pos.equals("未定義語")){
				addKeyword(keywordList, surface.toUpperCase(), i, i, "unknown", surface);
			}
			else if(pos != null && pos.equals("接頭辞")){

				if(!regCache.find("^[お御]$", surface)){
					addKeyword(keywordList, surface, i, i, "adjective");
				}
			}
			else if(pos != null && pos.equals("接尾辞")){
				if((dpos != null && !regCache.find("^(動|形容)詞性", dpos)) && !regCache.find("^つ$", surface)){
					addKeyword(keywordList, surface, i, i, "adjective");
				}
			}
			else if(pos != null && pos.equals("動詞")){
				if((base != null && !regCache.find("^([あ-わ][うくすつぬふむゆる]|できる)$", base)) && !regCache.find("^(いつか|あるか)$", surface)){
					String kw	= base;
					kw	= kw.replaceAll(".$", "");
					kw	= kw.replaceAll("起[きこ]", "起");
					kw	= kw.replaceAll("ず$", "じ");
					//String kw = base.substring(0, base.length()-2).replaceAll("起[きこ]", "起").replaceAll("ず$", "じ");
					addKeyword(keywordList, kw, i, i, "verb", base);
				}
			}
			else if(pos != null && pos.equals("形容詞")){
				String kw = base.replaceAll("(い|だ)$", "");
				addKeyword(keywordList, kw, i, i, "adjective", base);
			}
			else if(pos != null && pos.equals("連体詞")){
				if(!regCache.find("^(何|なん)(の|という)$", surface)){
					addKeyword(keywordList, surface, i, i, "adjective");
				}
			}
			else if(pos != null && pos.equals("副詞")){
				if(regCache.find("^(いつ|だけ|いくら|(何|なん)[とで]|如何して|いかに)$", surface)){
					addKeyword(keywordList,surface, i, i, "adjective");
				}
			}
			else if(pos != null && pos.equals("特殊")){
				if(dpos.equals("記号") && !regCache.find("^[！？・]$", surface)){
					addKeyword(keywordList, surface.toUpperCase(), i, i, "adjective", surface); //TODO org
				}
			}
			else if(pos != null && pos.equals("感動詞")){
				if(!regCache.find("^(何|なに)$", surface)){
					addKeyword(keywordList, surface, i, i, "noun");
				}
			}
		}

		return keywordList;
	}


	//----------------------------------------------------------------
	// private methods
	//----------------------------------------------------------------

	private static void addKeyword(List<KeyWord> keywords, String word, int begin, int end, String termType, String org) {
		KeyWord kw = new KeyWord(word, begin, end, termType, org);
		keywords.add(kw);
	}

	private static void addKeyword(List<KeyWord> keywords, String word, int begin, int end, String termType){
		KeyWord kw = new KeyWord(word, begin, end, termType);
		keywords.add(kw);
	}
}
