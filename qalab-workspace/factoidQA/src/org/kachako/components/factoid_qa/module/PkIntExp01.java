package org.kachako.components.factoid_qa.module;

import java.util.List;

import org.kachako.share.regex.RegexCache;
import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

public class PkIntExp01
{
	private static final RegexCache regexCache = new RegexCache();

	/**
	 * コンストラクタ
	 */
	public PkIntExp01()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	// sub get_int_exp(@){
	public static String getIntExp(List<Morpheme> morphemeList)
	{
		boolean	flag	= false;
		String	output	= "";

		for(int i = morphemeList.size() - 1;  i > 0; i--) {
			Morpheme	morpheme	= morphemeList.get(i);
			String surface	= morpheme.getSurfaceForm();
			String pos		= morpheme.getPos();
			String detailedPos	= morpheme.getDetailedPos();
			if(flag 
				|| (!pos.equals("助詞") && !regexCache.matches("[、|。]",surface) && !pos.equals("判定詞"))
				|| detailedPos.equals("副助詞")) {
				if(!output.equals("")) {
					output	= surface + output;
				}
				else {
					output = surface;
				}
				flag = true;
			}
		}
		return output;
	}
}
