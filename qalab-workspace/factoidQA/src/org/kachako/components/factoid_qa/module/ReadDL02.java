package org.kachako.components.factoid_qa.module;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.kachako.components.factoid_qa.manage.DecisionData;
import org.kachako.share.regex.RegexCache;

public class ReadDL02
{
	/**
	 * コンストラクタ
	 */
	public ReadDL02()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	// sub const_decision_list($$){
	public static HashMap<String, List<DecisionData>> constDecisionList(String par, String[] dList)
	{
		String	branch	= "";
		boolean	flag	= false;
		int		zeroRight	= 0;	// 正解が0であった事例の数
		int		zeroMiss	= 0;	// 不正解が0であった事例の数
//		int		numberOfEvidence	= 0;
		int		numberOfCase		= 0;
		double	ldConstRight	= 0.138198029;	// 線形ディスカウティング法の比例定数
		double	ldConstMiss		= 0.030969061;	// ここで入れてる値は適当

		RegexCache regexCache = new RegexCache();
		List<DecisionData>	decisionDataList	= new ArrayList<DecisionData>();
		HashMap<String, List<DecisionData>>	branchMap	= new HashMap<String, List<DecisionData>>();

		for(String line : dList) {
			if(regexCache.matches("^zero_atr_right:zero_atr_miss = (\\d+):(\\d+)", line) && par.equals("ATR")) {
				zeroRight	= Integer.parseInt(regexCache.lastMatched(1));
				zeroMiss	= Integer.parseInt(regexCache.lastMatched(2));

				// ディスカウンティングの比例定数計算
				ldConstRight = (zeroRight / ( zeroRight + numberOfCase ) +
								zeroRight / ( zeroRight + 2 * numberOfCase ) ) / 2;

				ldConstMiss =  ( zeroMiss / ( zeroMiss + numberOfCase ) +
								zeroMiss / ( zeroMiss + 2 * numberOfCase ) ) / 2;
			}
			else if(regexCache.matches("^zero_obj_right:zero_obj_miss = (\\d+):(\\d+)", line) && par.equals("OBJ")) {
				zeroRight	= Integer.parseInt(regexCache.lastMatched(1));
				zeroMiss	= Integer.parseInt(regexCache.lastMatched(2));

				// ディスカウンティングの比例定数計算
				ldConstRight = ( zeroRight / ( zeroRight + numberOfCase ) +
								zeroRight / ( zeroRight + 2 * numberOfCase ) ) / 2;

				ldConstMiss = ( zeroMiss / ( zeroMiss + numberOfCase ) +
								zeroMiss / ( zeroMiss + 2 * numberOfCase ) ) / 2;
			}
			else if(regexCache.matches("^number_of_atr_evidence = (\\d+)",line) && par.equals("ATR")) {
//				numberOfEvidence = Integer.parseInt(regexCache.lastMatched(1));
			}
			else if(regexCache.matches("^number_of_obj_evidence = (\\d+)",line) && par.equals("OBJ")) {
//				numberOfEvidence = Integer.parseInt(regexCache.lastMatched(1));
			}
			else if(regexCache.matches("^number_of_atr_case = (\\d+)",line) && par.equals("ATR")) {
				numberOfCase = Integer.parseInt(regexCache.lastMatched(1));
			}
			else if(regexCache.matches("^number_of_obj_case = (\\d+)",line) && par.equals("OBJ")) {
				numberOfCase = Integer.parseInt(regexCache.lastMatched(1));
			}

			if(regexCache.matches("<"+par+">",line)) {
				flag = true;
			}
			else if(regexCache.matches("<\\/"+par+">",line)) {
				flag = false;
			}
			else if(regexCache.matches("^branch = (.+?)",line)) {
				if(!branch.equals("") && decisionDataList.size() > 0) {
					decisionDataList	= sortDecisionData(decisionDataList);
					branchMap.put(branch, decisionDataList);
				}
				branch = regexCache.lastMatched(1);
				decisionDataList	= new ArrayList<DecisionData>(); 
			}
			else if(regexCache.matches("^(\\d+)\\t(\\d+)\\t(.+)",line) && flag) {
				int	right	= Integer.parseInt(regexCache.lastMatched(1));
				int	miss	= Integer.parseInt(regexCache.lastMatched(2));
				String	evidence	= regexCache.lastMatched(3);
				DecisionData	decisionData	= new DecisionData();
				decisionData.setBranch(branch);
				decisionData.setRight(right);
				decisionData.setMiss(miss);
				decisionData.setEvidence(evidence);

				double	pRight	= 0;
				if(right == 0) {
					pRight	= ldConstRight / zeroRight;
				}
				else {
					pRight	= right * ( 1 - ldConstRight ) / numberOfCase;
				}

				double	pMiss	= 0;
				if( miss == 0 ) {
					pMiss	= ldConstMiss / zeroMiss;
				}
				else {
					pMiss	= miss * ( 1 - ldConstMiss ) / numberOfCase;
				}

				double	llh	= Math.log(pRight / pMiss);
				//if(Double.isInfinite(llh)) {
				//	llh	= 0.0;
				//}
				decisionData.setLlh(llh);

				decisionDataList.add(decisionData);
			}
		}
		return branchMap;
	}

	private static List<DecisionData> sortDecisionData(List<DecisionData> decisionDataList)
	{
		// ソート
		Collections.sort(decisionDataList, new Comparator<DecisionData>(){
			public int compare(DecisionData x, DecisionData y){
				return ((Double)y.getLlh()).compareTo((Double)x.getLlh());
			}
		});
		return decisionDataList;
	}
}
