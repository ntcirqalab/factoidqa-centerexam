package org.kachako.components.factoid_qa.module;

import java.util.List;
import java.util.Map;

import org.kachako.types.japanese.syntactic.KnpSegment;
import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

public class TrLoc01
{
	/**
	 * コンストラクタ
	 */
	public TrLoc01()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	/**
	 *  JUMANの形態素番号をKNPの文節番号、文節内形態素番号に変換する.
	 *  
	 * @param loc JUMAN形態素番号
	 * @param knpInfoMap
	 * @return intの配列. [0]:KNPの文節番号、[1]:文節内形態素番号
	 */
	public static int[] juman2knp(int loc, Map<KnpSegment,List<Morpheme>> knpInfoMap)
	{
		//int k	= 0;
		int num	= 0;
		int[] numList	= new int[2];
		numList[0]	= 0;
		numList[1]	= 0;
		int count	= 0;
		for(KnpSegment key : knpInfoMap.keySet()) {
			List<Morpheme>	morphemeList	= knpInfoMap.get(key);
			count	+= morphemeList.size();
			if(loc < count) {
				for(int j = 0; j < morphemeList.size(); j++) {
					if(count-morphemeList.size()+j == loc) {
						numList[0]	= num;
						numList[1]	= j;
						return numList;
					}				
				}
			}
			num++;
		}
		return numList;
	}

	/**
	 * KNPの文節番号、文節内形態素番号をJUMANの形態素番号に変換する.
	 * 
	 * @param locX KNPの文節番号
	 * @param locY 文節内形態素番号
	 * @param knpInfoMap
	 * @return JUMANの形態素番号
	 */
	public static int knp2juman(int locX, int locY,Map<KnpSegment,List<Morpheme>> knpInfoMap)
	{
		int k	= 0;
		int num	= 0;
		for(KnpSegment key : knpInfoMap.keySet()){
			List<Morpheme>	morphemeList	= knpInfoMap.get(key);
			if(num != locX) {
				k += morphemeList.size()-1;
				num++;
				continue;
			}
			for(int j = 0; j < morphemeList.size(); j++) {
				if(j == locY) {
					return k;
				}
				k++;
			}
			num++;
		}
		return 0;
	}

}
