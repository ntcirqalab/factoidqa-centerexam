package org.kachako.components.factoid_qa.module;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.uima.jcas.cas.StringArray;
import org.kachako.components.factoid_qa.manage.KnpBnstHash;
import org.kachako.share.regex.RegexCache;
import org.kachako.types.japanese.syntactic.KnpSegment;
import org.kachako.types.japanese.syntactic.KyotoDependency;

/**
 * KNPの構文構造をハッシュに変換するクラス
 * 
 * @author saitou
 *
 */
public class Knp2hash01
{
	private static final RegexCache regexCache = new RegexCache();

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	/**
	 * 第num文節 に係り受けする構文木をハッシュで表す.
	 * 
	 * @param num 始点の文節番号
	 * @param kyotoDependencyList 
	 * @param knpSegmentList
	 * @param flag true:type P（並列）の文節を得る / false:type Pの文節を得ない
	 * @return
	 */
	public static KnpBnstHash knp2Hash(int num,
										List<KyotoDependency> kyotoDependencyList,
										List<KnpSegment> knpSegmentList,
										boolean flag)
	{
		if(knpSegmentList == null) {
			return null;
		}
		if(!knpSegmentList.contains(num)) {
			return null;
		}
		KnpBnstHash	knpHash	= new KnpBnstHash();
		KnpSegment	knpSegment	= knpSegmentList.get(num);
//		KyotoDependency dependency	= kyotoDependencyList.get(num);
//		KnpSegment	sourceSegment	= (KnpSegment)dependency.getSource();
//		KnpSegment	targetSegment	= (KnpSegment)dependency.getTarget();

		TREE:
		for(int i = num-1; i >= 0; i--) {
			KyotoDependency dependency1	= kyotoDependencyList.get(i);
			KnpSegment	targetSegment1	= (KnpSegment)dependency1.getTarget();
			if(targetSegment1.equals(knpSegment)) {
				String	strLabel	= dependency1.getLabel();
				if(regexCache.find("[ADI]",strLabel)) {
					if(!flag) {
						for(int j = i-1; j >= 0; j--) {
							KyotoDependency dependency2	= kyotoDependencyList.get(j);
							KnpSegment	targetSegment2	= (KnpSegment)dependency2.getSource();
							if(targetSegment2.equals(knpSegment)) {
								String	strLabel2	= dependency2.getLabel();
								if(strLabel2.equals("P")) {
									continue TREE;
								}
							}
						}
					}
					KnpBnstHash tmpHash = knp2Hash(i,kyotoDependencyList,knpSegmentList,true);
					KnpBnstHash margeHash	= margeHash(knpHash.getKnpBnstHash(num),tmpHash);
					knpHash.setKnpBnstHash(num, margeHash);
				}
				else if(flag && strLabel.equals("P")) {
					int y = 0;
					for(y = 0; y < knpSegmentList.size(); y++) {
						if(knpSegmentList.get(y).equals(targetSegment1)) {
							break;
						}
					}
					KnpSegment	target	= targetSegment1;
					for(int index = y; index < kyotoDependencyList.size(); index++) {
						KyotoDependency dependency2 = kyotoDependencyList.get(y);
						KnpSegment	sourceSegment2	= (KnpSegment)dependency2.getSource();
						KnpSegment	targetSegment2	= (KnpSegment)dependency2.getTarget();
						if(target.equals(sourceSegment2)) {
							if(dependency2.getLabel().equals("P")) {
								target	= targetSegment2;
							}
						}
					}
					if(target.equals(knpSegment)) {
						KnpBnstHash tmpHash1 = knp2Hash(num,kyotoDependencyList,knpSegmentList,false);
						KnpBnstHash tmpHash2 = knp2Hash(i,kyotoDependencyList,knpSegmentList,true);
						KnpBnstHash margeHash	= margeHash((tmpHash1 != null ? tmpHash1.getKnpBnstHash(num) : null),(tmpHash2 != null ? tmpHash2.getKnpBnstHash(i) : null));
						knpHash.setKnpBnstHash(i, margeHash);
					}
				}
			}
		}

		return knpHash;
	}


	/**
	 * 指定された文節から文末までの構文構造を返す
	 * 
	 * @param knpHash
	 * @param i 指定文節の番号
	 * @param num num番目の選択肢を返す
	 * @return
	 */
	public static List<Integer> searchTree(KnpBnstHash knpHash, int i, int num)
	{
		if(knpHash == null) {
			return null;
		}

		HashMap<Integer, KnpBnstHash> bnstMap	= knpHash.getBnstMap();
		if(bnstMap == null || bnstMap.size() <= 0) {
			return null;
		}

		for(Entry<Integer,KnpBnstHash> entry : bnstMap.entrySet()){
			if(entry.getKey() == i) {
				if(--num == 0) {
					List<Integer> ret	= new ArrayList<>();
					ret.add(i);
					return ret;
				}
			}
			else {
				List<Integer>	route	= searchTree(entry.getValue(), i, num);
				if(route != null && route.size() > 0) {
					return route;
				}
			}
		}
		return null;
	}

	/**
	 * ハッシュ1の最後の文節をハッシュ2の最初の提題の文節に係り受けさせる.
	 * 提題の文節がないときは最初の文節に係り受けさせる.
	 * 
	 * @param knpHash1
	 * @param knpHash2
	 * @param knpHashNum1
	 * @param kyotoDependencyList
	 * @param knpSegmentList
	 * @param teidaiBnstNum
	 * @return
	 */
	public static KnpBnstHash margeHash4(KnpBnstHash knpHash1,
													KnpBnstHash knpHash2, int knpHashNum1,
													List<KyotoDependency> kyotoDependencyList,
													List<KnpSegment> knpSegmentList,
													int teidaiBnstNum)
	{
		// knpMapNum1 : knpTreeMap1の文節数

		if(teidaiBnstNum < 0) {
			TREE:
			for(int i = 0; i < knpSegmentList.size(); i++) {
				KnpSegment	knpSegmet	= knpSegmentList.get(i);
				for(KyotoDependency dependency : kyotoDependencyList) {
					KnpSegment	sourceSegment	= (KnpSegment)dependency.getSource();
					if(sourceSegment.equals(knpSegmet)) {
						StringArray	sourchBunsetsuStrArray	= sourceSegment.getKnpBunsetsuFeatures();
						for(int index=0; index < sourchBunsetsuStrArray.size(); index++) {
							if(sourchBunsetsuStrArray.get(index).equals("提題")) {
								teidaiBnstNum	= i;
								break TREE;
							}
						}
					}
				}
			}
			if(teidaiBnstNum < 0) {
				return margeHash1(knpHash1,knpHash2,knpHashNum1);
			}
		}
		KnpBnstHash newHash	= new KnpBnstHash();
		HashMap<Integer, KnpBnstHash> bnstMap2	= knpHash2.getBnstMap();
		if(bnstMap2 != null && bnstMap2.size() > 0) {
			for(Entry<Integer,KnpBnstHash> entry : bnstMap2.entrySet()) {
				Integer temp = entry.getKey();
				if(temp == teidaiBnstNum) {
					KnpBnstHash tempHash	= addKnpNum(entry.getValue(),knpHashNum1);
					KnpBnstHash margeHash = margeHash(knpHash1,tempHash);
					newHash.setKnpBnstHash(temp+knpHashNum1, margeHash);
				}
				else {
					KnpBnstHash margeHash	= margeHash4(knpHash1,entry.getValue(),knpHashNum1,kyotoDependencyList,knpSegmentList,teidaiBnstNum);
					newHash.setKnpBnstHash(temp+knpHashNum1, margeHash);
				}
			}
		}
		return newHash;
	}


	//----------------------------------------------------------------
	// private methods
	//----------------------------------------------------------------

	//# ハッシュのマージ
	//####################################################
	//sub marge_hash(\%\%) {
	private static KnpBnstHash margeHash(KnpBnstHash knpHash1, KnpBnstHash knpHash2)
	{
		KnpBnstHash margeHash	= new KnpBnstHash();
		if(knpHash1 != null) {
			HashMap<Integer, KnpBnstHash> bnstMap	= knpHash1.getBnstMap();
			for(Entry<Integer,KnpBnstHash> entry : bnstMap.entrySet()) {
				margeHash.setKnpBnstHash(entry.getKey(), entry.getValue());
			}
		}

		if(knpHash2 != null) {
			HashMap<Integer, KnpBnstHash> bnstMap	= knpHash2.getBnstMap();
			for(Entry<Integer,KnpBnstHash> entry : bnstMap.entrySet()) {
				margeHash.setKnpBnstHash(entry.getKey(), entry.getValue());
			}
		}

		return margeHash;
	}

	//# 文節番号（ハッシュツリーのキー）に定数を加算する
	//####################################################
	//sub add_knpnum(\%$) {
	private static KnpBnstHash addKnpNum(KnpBnstHash knpHash, int n)
	{
		if(knpHash == null) {
			return null;
		}
		HashMap<Integer, KnpBnstHash> bnstMap	= knpHash.getBnstMap();
		if(bnstMap == null || bnstMap.size() <= 0) {
			return null;
		}
		KnpBnstHash newHash	= new KnpBnstHash();
		for(Entry<Integer,KnpBnstHash> entry : bnstMap.entrySet()) {
			Integer tempKey = entry.getKey();
			KnpBnstHash tempHash	= addKnpNum(entry.getValue(),n);
			newHash.setKnpBnstHash(tempKey+n, tempHash);
		}
		return newHash;
	}

	//# ハッシュ1の最後の文節をハッシュ2の最初の文節に係り受けさせる
	//####################################################
	//sub marge_hash1 (\%\%$) {
	private static KnpBnstHash margeHash1(KnpBnstHash knpHash1, KnpBnstHash knpHash2, int knpNum1)
	{
		// knpNum1 : knpHash1の文節数
		KnpBnstHash newHash	= knpHash1;
		HashMap<Integer, KnpBnstHash> bnstMap	= knpHash2.getBnstMap();
		for(Entry<Integer,KnpBnstHash> entry : bnstMap.entrySet()) {
			newHash.setKnpBnstHash(knpNum1+entry.getKey(), entry.getValue());
		}
		return newHash;
	}
}
