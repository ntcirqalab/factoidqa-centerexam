package org.kachako.components.factoid_qa.module;

import java.util.HashMap;
import java.util.List;

import org.apache.uima.jcas.cas.StringArray;
import org.kachako.share.regex.RegexCache;
import org.kachako.types.japanese.syntactic.KnpSegment;
import org.kachako.types.japanese.syntactic.KyotoDependency;
//import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

public class GetPredInf01
{
	private static final RegexCache regexCache = new RegexCache();

	/**
	 * コンストラクタ
	 */
	public GetPredInf01()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	//# KNP2次元配列から述語の文節番号、表層表現、品詞を返す
	//sub get_pred_inf($@){
	/**
	 * KNP結果から述語のKnpSegment、品詞を返す
	 * 
	 * @param bunsetsuNum 文節番号
	 * @param knpSegmentList KnpSegmentリスト
	 * @param knpInfoMap KnpSegmentに対する形態素情報Map
	 * 
	 * @return 述語のKnpSegmentと品詞を格納したHashMap. key=\"predKnpSegment\":KnpSegment, key=\"predInf\":品詞
	 */
	public static HashMap<String,Object> getPredInf(List<KyotoDependency> kyotoDependencyList,
													KnpSegment knpSegment)
	{
		if(kyotoDependencyList == null || kyotoDependencyList.size() <= 0) {
			return null;
		}

		KnpSegment	predKnpSegment	= null;
		//String	predExp	= null;
		String	predInf	= null;

		HashMap<String,Object>	hm	= new HashMap<String,Object>();

		// 文節への付加情報
		StringArray	bunsetsuFeatures	= knpSegment.getKnpBunsetsuFeatures();

		for(int i=0; i < bunsetsuFeatures.size(); i++) {
			String	str	= bunsetsuFeatures.get(i);
			if(regexCache.find("用言:",str)) {
				if(regexCache.find("用言:強:(動|形|判)",str)) {
					predInf	= regexCache.lastMatched(1);
					if(predInf.equals("動")) {
						if(regexCache.find("<〜れる>",str)) {
							predInf	= "受";
						}
						else {
							predInf	= "能";
						}
					}
				}
				else {
					predInf	= "他";
				}
				predKnpSegment	= knpSegment;
				//predExp	= getPredExp(morphemeList);
				hm.put("predKnpSegment", predKnpSegment);
				hm.put("predInf", predInf);

				return hm;
			}
		}

		for(int j = 0; j < kyotoDependencyList.size(); ++j) {
			KyotoDependency dependency	= kyotoDependencyList.get(j);
			KnpSegment	sourceSegment	= (KnpSegment)dependency.getSource();
			KnpSegment	targetSegment	= (KnpSegment)dependency.getTarget();
			if(sourceSegment.equals(knpSegment)) {
				return getPredInf(kyotoDependencyList,targetSegment);
			}
		}

		hm.put("predKnpSegment", null);
		hm.put("predInf", null);

		return hm;
	}


	//----------------------------------------------------------------
	// private methods
	//----------------------------------------------------------------

	//# 述語文節を後ろからスキャンし、最後の助詞とそれ以降の句読点を削除
	//sub get_pred_exp(@){
//	private static String getPredExp(List<Morpheme> morphemeList)
//	{
//		boolean	flag	= false;
//		String	predExp	= "";
//
//		for(int i = morphemeList.size()-1; i > 0; --i) {
//			Morpheme	morpheme	= morphemeList.get(i);
//			String surface	= morpheme.getSurfaceForm();
//			String pos		= morpheme.getPos();
//
//			if(pos.equals("助詞") && !flag) {
//				flag	= true;
//			}
//			else if(regexCache.matches("^[、|。]", surface) || flag) {
//				predExp = surface + predExp;
//			}
//		}
//		return predExp;
//	}

}
