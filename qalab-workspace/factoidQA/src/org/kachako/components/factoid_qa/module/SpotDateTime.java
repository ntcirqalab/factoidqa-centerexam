package org.kachako.components.factoid_qa.module;

import java.util.List;

public class SpotDateTime
{

	/* 否定パタン (最終解に対するパタン) */

	// 「ぶり」「ごと」が後続するときは排除
//	private static final String PAT_NEG_DATE = "(?:(?:ぶり|ごと)$)";

	/* 肯定パタン (形態素解析列に対するパタン) */

	//  [亜-瑤] : 漢字1文字に対応する?
	//  [^あ-んア-ン０-９] から変更
	private static final String PAT_DATE_YEAR_INTERNAL	=
		"(?:"
			+"(?:"	// 元号+数字+「年」     (例:  「平成１５年」)
				+"(?:"	// 元号 オプショナル (漢字2文字としているがもっとうまいのはない?)
					+"(?:[亜-瑤]{2}\\s.*\n)"	// 二文字 一単語
					+"|"
					+"(?:[亜-瑤]\\s.*\n)"	// 一文字 二単語
					+"(?:[亜-瑤]\\s.*\n)"
					+")?"
				+"[０-９元〇一二三四五六七八九十百千万億兆]+\\s.*\n"	// 数字．「元年」もある．
				+"年[頃代春]?\\s.*"	// 「年」     # 「年代」「年頃」「年春」 は一単語
			+")|"
			+"(?:"	// 年表現
				+"(?:一?昨|去|今|再?来)年\\s.*"	// 「去年」などの表現
			+")|"
			+"(?:"	// 元号+数字+「（」+数字+「）」+「年」   (例:  「平成１５（２００３）年」)
				+"(?:"	// 元号 オプショナル (漢字2文字としているがもっとうまいのはない?)
					+"(?:[亜-瑤]{2}\\s.*\n)"	// 二文字 一単語
					+"|(?:[亜-瑤]\\s.*\n)"	// 一文字 二単語
					+"(?:[亜-瑤]\\s.*\n)"
					+")?"
				+"[０-９元〇一二三四五六七八九十百千万億兆]+\\s.*\n"	// 数字
				+"（\\s.*\n"	// 「（」
					+"[０-９〇一二三四五六七八九十百千万億兆]+\\s.*\n"	// 数字
				+"）\\s.*\n"	// 「）」
				+"年[頃代春]?\\s.*"	// 「年」     # 「年代」「年頃」「年春」 は一単語
			+")|"
			+"(?:"	// 元号+数字+「年（」+数字+「）」 (例:  「平成１５年（２００３）」)
				+"(?:"	// 元号 オプショナル (漢字2文字としているがもっとうまいのはない?)
					+"(?:[亜-瑤]{2}\\s.*\n)"	// 二文字 一単語
					+"|"
					+"(?:[亜-瑤]\\s.*\n)"	// 一文字 二単語
					+"(?:[亜-瑤]\\s.*\n)"
				+")?"
				+"[０-９元〇一二三四五六七八九十百千万億兆]+\\s.*\n"	// 数字
				+"年\\s.*\n"	// 「年」
				+"（\\s.*\n"	// 「（」
					+"[０-９〇一二三四五六七八九十百千万億兆]+\\s.*\n"	// 数字
				+"）\\s.*"	// 「）」
			+")|"
			+"(?:"	// 数字+「（」+元号+数字+「）」+「年」   (例:  「２００３（平成１５）年」)
				+"[０-９〇一二三四五六七八九十百千万億兆]+\\s.*\n"	// 数字
					+"（\\s.*\n"	// 「（」
						+"(?:"	// 元号 オプショナル (漢字2文字としているがもっとうまいのはない?)
							+"(?:[亜-瑤]{2}\\s.*\n)"	// 二文字 一単語
							+"|"
							+"(?:[亜-瑤]\\s.*\n)"	// 一文字 二単語
							+"(?:[亜-瑤]\\s.*\n)"
						+")?"
						+"[０-９元〇一二三四五六七八九十百千万億兆]+\\s.*\n"	// 数字
					+"）\\s.*\n"	// 「）」
				+"年[頃代春]?\\s.*"	// 「年」     # 「年代」「年頃」「年春」 は一単語
			+")|"
			+"(?:"	// 数字+「年（」+元号+数字+「）」   (例:  「２００３年（平成１５）」)
				+"[０-９〇一二三四五六七八九十百千万億兆]+\\s.*\n"	// 数字
				+"年\\s.*\n"	// 「年」     # 「年代」「年頃」 は一単語
				+"（\\s.*\n"	// 「（」
					+"(?:"	// 元号 オプショナル (漢字2文字としているがもっとうまいのはない?)
						+"(?:[亜-瑤]{2}\\s.*\n)"	// 二文字 一単語
						+"|"
						+"(?:[亜-瑤]\\s.*\n)"	// 一文字 二単語
						+"(?:[亜-瑤]\\s.*\n)"
					+")?"
				+"[０-９元〇一二三四五六七八九十百千万億兆]+\\s.*\n"	// 数字
				+"）\\s.*"	// 「）」
			+")"
		+")"
		+"(?:\n(?:前半|後半|初め|始め?|初頭|半ば|暮れ|始まり|終り|末|頃|[こご]ろ|[初晩]?[春夏秋冬])\\s.*)?"	// 「初め」など オプショナル
		+"(?: \n(?:ぶり|ごと)\\s.*)?";	// 「ぶり」は入れたくないがまずは入れておく

	private static final String PAT_DATE_MONTH_INTERNAL	= 
		"(?:"
			+"(?:"	// 数字+「月」+「上旬」など
				+"(?:"
					+"(?:"
						+"[０-９零〇一二三四五六七八九十百千万億兆]+\\s.*\n"	// 数字
						+"月頃?\\s.*"	// 「月」  「月頃」は一単語になる．
					+")|"
					+"(?:"
						+"五月\\s.*"	// 「さつき」になってしまう．
					+")"
				+")"
				+"(?:\n(?:[上中下]旬|[頭末]|半ば|頃|[こご]ろ)\\s.*)?"	//「上旬」など  オプショナル
			+")|"
			+"(?:"	// 来月など
				+"(?:先々?|今|再?来)月\\s.*"	// 「来月」など
			+")"
		+")";

	private static final String PAT_DATE_DAY_INTERNAL	=
		"(?:"
			+"(?:"	// 数字+「日」
				+"[０-９零〇一二三四五六七八九十百千万億兆]+\\s.*\n"	// 数字
				+"日[頃夜付]?\\s.*"	// 「日」, 「日頃」「日夜」「日付」はついてしまう
			+")|"
			+"(?:"	// 「翌日」など
				+"(?:(?:一?昨|今|明後?|翌)日|あさって)\\s.*"	// 「翌日」など
			+")"
		+")"
		+"(?: \n(?:ぶり|ごと)\\s.*)?";	// 「ぶり」は入れたくないがまずは引っかける

//	private static final String PAT_DATE_YEAR	= "(" + PAT_DATE_YEAR_INTERNAL + ")";
//	private static final String PAT_DATE_MONTH	= "(" + PAT_DATE_MONTH_INTERNAL + ")";
//	private static final String PAT_DATE_DAY	= "(" + PAT_DATE_DAY_INTERNAL + ")";

	private static final String PAT_DATE_YEAR_MONTH_DAY = 
									String.format("((?:^|\n)%s\n%s\n%s)",
												PAT_DATE_YEAR_INTERNAL,
												PAT_DATE_MONTH_INTERNAL,
												PAT_DATE_DAY_INTERNAL
											);

	private static final String PAT_DATE_YEAR_MONTH = 
									String.format("((?:^|\n)%s\n%s)", 
												PAT_DATE_YEAR_INTERNAL,
												PAT_DATE_MONTH_INTERNAL
											);

	private static final String PAT_DATE_MONTH_DAY =
									String.format("((?:^|\n)%s\n%s)", 
												PAT_DATE_MONTH_INTERNAL,
												PAT_DATE_DAY_INTERNAL
											);

	private static final String PAT_DATE_YEAR =
									String.format("((?:^|\n)%s)", 
												PAT_DATE_YEAR_INTERNAL
											);

	private static final String PAT_DATE_MONTH =
									String.format("((?:^|\n)%s)", 
												PAT_DATE_MONTH_INTERNAL
											);

	private static final String PAT_DATE_DAY =
									String.format("((?:^|\n)%s)", 
												PAT_DATE_DAY_INTERNAL
											);

	private static final String PAT_DATE_INTERNAL =
									String.format("(?:(?:(?:^|\n)%s\n%s\n%s)"
												+"|(?:(?:^|\n)%s\n%s)"
												+"|(?:(?:^|\n)%s\n%s)"
												+"|(?:(?:^|\n)%s)"
												+"|(?:(?:^|\n)%s)"
												+"|(?:(?:^|\n)%s))",
												PAT_DATE_YEAR_INTERNAL, PAT_DATE_MONTH_INTERNAL, PAT_DATE_DAY_INTERNAL,
												PAT_DATE_YEAR_INTERNAL, PAT_DATE_MONTH_INTERNAL,
												PAT_DATE_MONTH_INTERNAL,PAT_DATE_DAY_INTERNAL,
												PAT_DATE_YEAR_INTERNAL,PAT_DATE_MONTH_INTERNAL,PAT_DATE_DAY_INTERNAL
											);

	/* 時間用パタン */

	private static final String PAT_TIME_HOUR_INTERNAL = 
		"(?:"
			+"(?:"
				+"(?:午[前後]\\s.*\n)?"	// 「午前」など  オプショナル
				+"[０-９零〇一二三四五六七八九十百千万億兆]+\\s.*\n"	// 数字
				+"(?:"
					+"(?:"
						+"時間?\\s.*"	// 「時間」
						+"(?:\n(?:[こご]ろ|頃|前|まえ|[過す]ぎ)\\s.*)?"	// 「前」など    オプショナル
					+")|"
					+"(?:"
						+"時間後\\s.*"	// 「時間後」は「じかんおくれ」という一単語
					+")"
				+")"
			+")"
		+")";

	private static final String PAT_TIME_HOUR_FOR_DATE_INTERNAL =
		"(?:"
			+"(?:"
			+"(?:午[前後]\\s.*\n)?"	// 「午前」など  オプショナル
				+"(?:"
					+"(?:"
						+"[０-９零〇一二三四五六七八九十百千万億兆]+\\s.*\n"	//  数字
						+"時\\s.*"	// 「時」
					+")"
					+"|"
					+"(?:"
						+"[零一二三四五六七八九十百千万億兆]+時\\s.*"	// 「零時」のように一語になっているものもある
					+")"
				+")"
				+"(?:\n(?:[こご]ろ|頃|前|まえ|[過す]ぎ)\\s.*)?"	// 「前」など    オプショナル
			+")"
			+"|"
			+"(?:"
				+"(?:早?朝|正午|午[前後]|昼(?:[さ下]がり|過ぎ)|夕方?|深?夜|晩|未明|頃|[こご]ろ)\\s.*"	// 「早朝」など漠然とした時間表現
			+")"
		+")";

	private static final String PAT_TIME_MINUTE_INTERNAL =
		"(?:"
			+"(?:"
				+"(?:"
					+"[０-９零〇一二三四五六七八九十百千万億兆]+\\s.*\n"	// 数字
					+"分\\s.*"	// 「分」
				+")"
				+"|"
				+"(?:"
					+"[半]\\s.*"	// 「八時半」の「半」
				+")"
			+")"
			+"(?:\n(?:[こご]ろ|頃|前|まえ|[過す]ぎ)\\s.*)?"	// 「前」など    オプショナル
		+")";

	private static final String PAT_TIME_SECOND_INTERNAL =
		"(?:"
			+"[０-９零〇一二三四五六七八九十百千万億兆]+\\s.*\n"	// 数字
			+"秒\\s.*"	// 「秒」
			+"(?:\n(?:[こご]ろ|頃|前|まえ|[過す]ぎ)\\s.*)?"	// 「前」など    オプショナル
		+")";

	private static final String PAT_TIME_SUBSECOND_INTERNAL =
		"(?:"
			+"[０-９零〇一二三四五六七八九十百千万億兆]+\\s.*"	// 数字
				+"(?:\n(?:[こご]ろ|頃|前|まえ|[過す]ぎ)\\s.*)?"	// 「前」など    オプショナル
		+")";

//	private static final String PAT_TIME_HOUR			= "(" + PAT_TIME_HOUR_INTERNAL + ")";
//	private static final String PAT_TIME_HOUR_FOR_DATE	= "(" + PAT_TIME_HOUR_FOR_DATE_INTERNAL + ")";
//	private static final String PAT_TIME_MINUTE			= "(" + PAT_TIME_MINUTE_INTERNAL + ")";
//	private static final String PAT_TIME_SECOND			= "(" + PAT_TIME_SECOND_INTERNAL + ")";
//	private static final String PAT_TIME_SUBSECOND		= "(" + PAT_TIME_SUBSECOND_INTERNAL + ")";

	private static final String PAT_TIME_INTERNAL =
									String.format("(?:"
												+"(?:(?:^|\n)%s\n%s\n%s\n%s)"
												+"|(?:(?:^|\n)%s\n%s\n%s)"
												+"|(?:(?:^|\n)%s\n%s\n%s)"
												+"|(?:(?:^|\n)%s\n%s)"
												+"|(?:(?:^|\n)%s\n%s)"
												+"|(?:(?:^|\n)%s\n%s)"
												+"|(?:(?:^|\n)%s)"
												+"|(?:(?:^|\n)%s)"
												+"|(?:(?:^|\n)%s)"
												+")",
												PAT_TIME_HOUR_INTERNAL,PAT_TIME_MINUTE_INTERNAL,PAT_TIME_SECOND_INTERNAL,PAT_TIME_SUBSECOND_INTERNAL,
												PAT_TIME_HOUR_INTERNAL,PAT_TIME_MINUTE_INTERNAL,PAT_TIME_SECOND_INTERNAL,
												PAT_TIME_MINUTE_INTERNAL,PAT_TIME_SECOND_INTERNAL,PAT_TIME_SUBSECOND_INTERNAL,
												PAT_TIME_HOUR_INTERNAL,PAT_TIME_MINUTE_INTERNAL,
												PAT_TIME_MINUTE_INTERNAL,PAT_TIME_SECOND_INTERNAL,
												PAT_TIME_SECOND_INTERNAL,PAT_TIME_SUBSECOND_INTERNAL,
												PAT_TIME_HOUR_INTERNAL,
												PAT_TIME_MINUTE_INTERNAL,
												PAT_TIME_SECOND_INTERNAL
											);

	private static final String PAT_TIME	= "(" + PAT_TIME_INTERNAL + ")";

	private static final String PAT_TIME_FOR_DATE_INTERNAL =
									String.format("(?:"
												+"(?:(?:^|\n)%s\n%s\n%s\n%s)"
												+"|(?:(?:^|\n)%s\n%s\n%s)"
												+"|(?:(?:^|\n)%s\n%s\n%s)"
												+"|(?:(?:^|\n)%s\n%s)"
												+"|(?:(?:^|\n)%s\n%s)"
												+"|(?:(?:^|\n)%s\n%s)"
												+"|(?:(?:^|\n)%s)"
												+"|(?:(?:^|\n)%s)"
												+"|(?:(?:^|\n)%s)"
												+")",
												PAT_TIME_HOUR_FOR_DATE_INTERNAL,PAT_TIME_MINUTE_INTERNAL,PAT_TIME_SECOND_INTERNAL,PAT_TIME_SUBSECOND_INTERNAL,
												PAT_TIME_HOUR_FOR_DATE_INTERNAL,PAT_TIME_MINUTE_INTERNAL,PAT_TIME_SECOND_INTERNAL,
												PAT_TIME_MINUTE_INTERNAL,PAT_TIME_SECOND_INTERNAL,PAT_TIME_SUBSECOND_INTERNAL,
												PAT_TIME_HOUR_FOR_DATE_INTERNAL,PAT_TIME_MINUTE_INTERNAL,
												PAT_TIME_MINUTE_INTERNAL,PAT_TIME_SECOND_INTERNAL,
												PAT_TIME_SECOND_INTERNAL,PAT_TIME_SUBSECOND_INTERNAL,
												PAT_TIME_HOUR_FOR_DATE_INTERNAL,
												PAT_TIME_MINUTE_INTERNAL,
												PAT_TIME_SECOND_INTERNAL
											);

//	private static final String PAT_TIME_FOR_DATE	= "(" + PAT_TIME_FOR_DATE_INTERNAL + ")";

	/* 日付時間の複合パタン */

	private static final String PAT_DATE_TIME =
									String.format("("
												+"(?:(?:^|\n)%s%s)"
												+"|(?:(?:^|\n)%s)"
												+"|(?:(?:^|\n)%s)"
												+")",
												PAT_DATE_INTERNAL,PAT_TIME_FOR_DATE_INTERNAL,
												PAT_DATE_INTERNAL,
												PAT_TIME_FOR_DATE_INTERNAL
											);

	private static final String PAT_TIME_HOUR_MINUTE_SECOND =
									String.format("((?:^|\n)%s\n%s\n%s)",
												PAT_TIME_HOUR_INTERNAL,
												PAT_TIME_MINUTE_INTERNAL,
												PAT_TIME_SECOND_INTERNAL
											);

	private static final String PAT_TIME_HOUR_MINUTE =
									String.format("((?:^|\n)%s\n%s)",
												PAT_TIME_HOUR_INTERNAL,
												PAT_TIME_MINUTE_INTERNAL
											);

	private static final String PAT_TIME_MINUTE_SECOND =
									String.format("((?:^|\n)%s\n%s)",
												PAT_TIME_MINUTE_INTERNAL,
												PAT_TIME_SECOND_INTERNAL
											);

	private static final String PAT_TIME_HOUR =
									String.format("((?:^|\n)%s)",
												PAT_TIME_HOUR_INTERNAL
											);

	private static final String PAT_TIME_MINUTE =
									String.format("((?:^|\n)%s)",
												PAT_TIME_MINUTE_INTERNAL
											);

	private static final String PAT_TIME_SECOND =
									String.format("((?:^|\n)%s)",
												PAT_TIME_SECOND_INTERNAL
											);

	private static final String PAT_TIME_SECOND_CLOSELY =
									String.format("((?:^|\n)%s\n%s)",
												PAT_TIME_SECOND_INTERNAL,
												PAT_TIME_SUBSECOND_INTERNAL
											);

	/* 大域変数 */
//	private static final int FLAG_INIT	= 1;

	/**
	 * コンストラクタ
	 */
	public SpotDateTime()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	//	##################################################################
	//	# spotter_date()
	//	#   形態素情報列において，指定した形態素を含む，指定した型の日付表現
	//	#   が あれば取り出す．なければ ""
	//	##################################################################
	//	sub spotter_date($$$) {
	public String spotterDate(List<String> neDataList, int morphemeNo, String netType)
	{
		String ans	= "";

		String[] linesIds = SpotAnswer.spotterAddIndex(neDataList);

		if(netType.equals("date.year-month-day")) {
			ans	= SpotAnswer.spotterMkPatMatchFuncMorphForAns(PAT_DATE_YEAR_MONTH_DAY, linesIds, morphemeNo);
		}
		else if(netType.equals("date.year-month")) {
			ans	= SpotAnswer.spotterMkPatMatchFuncMorphForAns(PAT_DATE_YEAR_MONTH, linesIds, morphemeNo);
		}
		else if(netType.equals("date.month-day")) {
			ans	= SpotAnswer.spotterMkPatMatchFuncMorphForAns(PAT_DATE_MONTH_DAY, linesIds, morphemeNo);
		}
		else if (netType.equals("date.year")) {
			ans	= SpotAnswer.spotterMkPatMatchFuncMorphForAns(PAT_DATE_YEAR, linesIds, morphemeNo);
		}
		else if (netType.equals("date.month")) {
			ans	= SpotAnswer.spotterMkPatMatchFuncMorphForAns(PAT_DATE_MONTH, linesIds, morphemeNo);
		}
		else if (netType.equals("date.day")) {
			ans	= SpotAnswer.spotterMkPatMatchFuncMorphForAns(PAT_DATE_DAY, linesIds, morphemeNo);
		}
		else {
			ans	= SpotAnswer.spotterMkPatMatchFuncMorphForAns(PAT_DATE_TIME, linesIds, morphemeNo);
		}
		return ans;
	}

	//	##################################################################
	//	# spotter_time()
	//	#   形態素情報列において，指定した形態素を含む，指定した型の時刻表現
	//	#   が あれば取り出す．なければ ""
	//	##################################################################
	//	sub spotter_time($$$) {
	public String spotterTime(List<String> neDataList, int morphemeNo, String netType)
	{
		String ans	= "";

		String[] linesIds = SpotAnswer.spotterAddIndex(neDataList);

		if(netType.equals("time.hour-minute-second")) {
			ans	= SpotAnswer.spotterMkPatMatchFuncMorphForAns(PAT_TIME_HOUR_MINUTE_SECOND, linesIds, morphemeNo);
		}
		else if(netType.equals("time.hour-minute")) {
			ans	= SpotAnswer.spotterMkPatMatchFuncMorphForAns(PAT_TIME_HOUR_MINUTE, linesIds, morphemeNo);
		}
		else if(netType.equals("time.minute-second")) {
			ans	= SpotAnswer.spotterMkPatMatchFuncMorphForAns(PAT_TIME_MINUTE_SECOND, linesIds, morphemeNo);
		}
		else if (netType.equals("time.hour")) {
			ans	= SpotAnswer.spotterMkPatMatchFuncMorphForAns(PAT_TIME_HOUR, linesIds, morphemeNo);
		}
		else if (netType.equals("time.minute")) {
			ans	= SpotAnswer.spotterMkPatMatchFuncMorphForAns(PAT_TIME_MINUTE, linesIds, morphemeNo);
		}
		else if (netType.equals("time.second")) {
			ans	= SpotAnswer.spotterMkPatMatchFuncMorphForAns(PAT_TIME_SECOND, linesIds, morphemeNo);
		}
		else if (netType.equals("time.second-closely")) {
			ans	= SpotAnswer.spotterMkPatMatchFuncMorphForAns(PAT_TIME_SECOND_CLOSELY, linesIds, morphemeNo);
		}
		else {
			ans	= SpotAnswer.spotterMkPatMatchFuncMorphForAns(PAT_TIME, linesIds, morphemeNo);
		}
		return ans;
	}

}
