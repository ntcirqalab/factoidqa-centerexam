package org.kachako.components.factoid_qa.module;

import java.util.List;

public class SpotOrg
{
	/* 場所 */
	// ORGANIZATION がふられていない形態素について，場所とみなす形態素
	private static final String PAT_ORG_MORPH = "(?:^|\n)(?:[亜-瑤Ａ-Ｚａ-ｚァ-ヶー・]+\\s(?:\\S+\\s){2}(?:名詞|未定義語|接頭辞|接尾辞\\s\\S+\\s名詞性|副詞|特殊\\s\\S+\\s記号).*(?:<>|<PERSON>|<ORGANIZATION>|<LOCATION>|<ARTIFACT>).*)";
	private static final String PAT_ORG_MORPH_HEAD = "(?:^|\n)(?:[亜-瑤Ａ-Ｚａ-ｚァ-ヶ][亜-瑤Ａ-Ｚａ-ｚァ-ヶー・]*\\s(?:\\S+\\s){2}(?:名詞|未定義語|接頭辞|接尾辞\\s\\S+\\s名詞性|副詞|特殊\\s\\S+\\s記号).*(?:<>|<PERSON>|<ORGANIZATION>|<LOCATION>|<ARTIFACT>).*)";

	private static String PAT_PERSON_SUFFIX_IN	= "";

	private static String[] patOrgList = {
						// pattern 0: ORGANIZATIONタグの連続
						"("
							+"(?:"	// ORGANIZATIONタグの連続
							+"(?:(?:^|\n).*<ORGANIZATION>.*)+"
							+"(?:"
							+"(?:" + PAT_ORG_MORPH + ")*"
							+"(?:" + PAT_PERSON_SUFFIX_IN + ")"	// Spot_ORG_Suffix.pmからのデータ。textファイルにするか？
							+")?"
							+")"
						+")",
						// pattern 1: 英語等の表記の場合 一つは ORGANIZATIONを含むようにする．
						"("
							+"(?:"
								+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*<ORGANIZATION>.*)"
								+"(?:"
									+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ・][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<ORGANIZATION>).*)*"
									+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<ORGANIZATION>).*)"
								+")?"
							+")"
							+"|"
							+"(?:"
								+"(?:"
									+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<ORGANIZATION>).*)"
									+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ・][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<ORGANIZATION>).*)*"
								+")?"
								+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*<ORGANIZATION>.*)"
							+")"
							+"|"
							+"(?:"
								+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<ORGANIZATION>).*)"
								+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ・][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<ORGANIZATION>).*)*"
								+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*<ORGANIZATION>.*)"
								+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ・][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<ORGANIZATION>).*)*"
								+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<ORGANIZATION>).*)"
							+")"
						+")",
						// pattern 2: 括弧の中(「」)
						"(?:"
							+"(?:(?:^|\n)「\\s.*)"
								+"("
									+"(?:(?:^|\n)\\S+\\s.*)+?"
								+")"
							+"(?:(?:^|\n)」\\s.*)"
						+")",
						// pattern 3: 括弧の中(『』)
						"(?:"
							+"(?:(?:^|\n)『\\s.*)"
								+"("
									+"(?:(?:^|\n)\\S+\\s.*)+?"
								+")"
							+"(?:(?:^|\n)』\\s.*)"
						+")",
						// pattern 4: 括弧の中(（）)
						"(?:"
							+"(?:(?:^|\n)（\\s.*)"
								+"("
									+"(?:(?:^|\n)\\S+\\s.*)+?"
								+")"
							+"(?:(?:^|\n)）\\s.*)"
						+")",
						// pattern 5: 括弧の中(“”)
						"(?:"
							+"(?:(?:^|\n)“\\s.*)"
								+"("
									+"(?:(?:^|\n)\\S+\\s.*)+?"
								+")"
							+"(?:(?:^|\n)”\\s.*)"
						+")",
						// pattern 6:  ORGANIZATIONがなくても英語名をつなぐ
						"("
							+"(?:"
								+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<ORGANIZATION>).*)"
								+"(?:"
									+"(?:\n[Ａ-Ｚァ-ヶ・][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<ORGANIZATION>).*)*"
									+"(?:\n[Ａ-Ｚァ-ヶ][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<ORGANIZATION>).*)"
								+")?"
							+")"
						+")",
						// pattern 7: 最後の表現をつかって前に延ばす
						"("
							+"(?:"
								+"(?:"
									+"(?:" + PAT_ORG_MORPH_HEAD + ")"
									+"(?:" + PAT_ORG_MORPH + ")*"
									+"(?:" + PAT_PERSON_SUFFIX_IN + ")"	// Spot_ORG_Suffix.pmからのデータ。textファイルにするか？
								+")?"
							+")"
						+")"
						//pattern 8: 最終手段 ほとんど何でもあり
						// なし
				};

	// 大域変数
//	private static final int FLAG_INIT	= 1;

	/**
	 * コンストラクタ
	 */
	public SpotOrg(String suffixIn)
	{
		PAT_PERSON_SUFFIX_IN	= suffixIn;
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	//	##################################################################
	//	# spotter_organization()
	//	#   形態素情報列において，指定した形態素を含む，指定した型の場所表現
	//	#   が あれば取り出す．なければ ""
	//	##################################################################
	//	sub spotter_organization($$$) {
	public String spotterOrganization(List<String> neDataList, int morphemeNo, String netType)
	{
		String ans	= "";

		if(netType.equals("ORGANIZATION")) {
			String[] linesIds = SpotAnswer.spotterAddIndex(neDataList);

			// パタンを順番に適用して，一番最初に見つかったものを利用
			for(int i = 0 ; i < patOrgList.length; i++) {
//System.out.println(patPersonList[i]);
				String pattern	= patOrgList[i];
				ans	= SpotAnswer.spotterMkPatMatchFuncMorphForAns(pattern, linesIds, morphemeNo);
				if(ans != null && ans.length() > 0) {
					break;
				}
			}
		}
		else {
			ans = "";
		}

		return ans;
	}

}
