package org.kachako.components.factoid_qa.module;

import java.util.List;
import java.util.Map;

import org.kachako.components.factoid_qa.manage.AnswerParam;
import org.kachako.components.factoid_qa.manage.LineInfo;
import org.kachako.components.factoid_qa.manage.QueryInfo;
import org.kachako.share.regex.RegexCache;
import org.kachako.types.japanese.syntactic.KnpSegment;
import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

public class CwqMakeAns04
{
	// 表現の長さの上限(括弧内，および，文節により抽出された場合)
	private static final int MAX_EXP_LEN = 20;

	private static final RegexCache regexCache = new RegexCache();

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	/**
	 * 解形態素から解答形態素列を作る。
	 * 
	 * @param param パラメータクラス
	 * @param lid 文ID
	 * @param morphemeNo 形態素番号
	 * @param queryInfo クエリ情報
	 * @param lineInfoMap 文の情報のMap
	 * @param dateflag 日付解の補完を行うかのフラグ
	 * @param personSuffixIn 人物パターン形式
	 * @param personSuffixOut 人物パターン形式
	 *
	 * @return 解答とする文字列
	 */
	public static String mkAns(AnswerParam param, String lid, int morphemeNo, QueryInfo queryInfo,
									Map<String, LineInfo> lineInfoMap, boolean dateflag,
									String personSuffixIn,String personSuffixOut)
	{
		Map<String, LineInfo> tempLineInfoMap	= lineInfoMap;
		LineInfo lineInfo	= tempLineInfoMap.get(lid);
		// 形態素情報に固有表現付加したリスト
		List<String> neDataList	= lineInfo.getNeDataList();
		List<String> morphemeHyosoList	= lineInfo.getMorphemeHyosoList();
		List<KnpSegment> knpSegmentList	= lineInfo.getKnpSegmentList();
		Map<KnpSegment,List<Morpheme>> knpInfoMap	= lineInfo.getKnpInfoMap();
		String pattern1	= queryInfo.getPattern1();
		if(pattern1 == null) {
			pattern1	= "";
		}

		{
			String ans		= null;
			String ansNew	= null;
			int noFailSafe	= 0;
			String neType	= param.getNeType();

			// 日付，時間の場合は，それらのパタンのみを適用し，
			// パタンにかからなかったときのフェイルセイフは適用しない
			if(regexCache.find("date|time", neType)) {
				// 日付
				if(regexCache.find("date", neType)) {
					SpotDateTime spotDateTime	= new SpotDateTime();
					ansNew = spotDateTime.spotterDate(neDataList, morphemeNo, "date");
					if(ans == null || ansNew.length() > ans.length()) {
						ans = ansNew;
					}
				}

				// 時間
				if(regexCache.find("time", neType)) {
					SpotDateTime spotDateTime	= new SpotDateTime();
					ansNew = spotDateTime.spotterTime(neDataList, morphemeNo, "time");
					if(ans == null || ansNew.length() > ans.length()) {
						ans = ansNew;
					}
				}
				// 空文字列や未定義でも
				// パタンにかからなかったときのフェイルセイフは適用しない
				if(ans != null) {
					return ans;
				}
				else {
					return "";
				}
			}

			//###########################################################
			// 人，場所，組織
			// 解が見つからない場合にはフェイルセイフを適用こともある
			//###########################################################

			// 人
			if(regexCache.find("PERSON", neType)) {
				SpotPerson spotPerson	= new SpotPerson(personSuffixOut);
				ansNew	= spotPerson.spotterPerson(neDataList, morphemeNo, "PERSON");
				if(ans == null || ansNew.length() > ans.length()) {
					ans = ansNew;
				}

				noFailSafe = 1; // 人の場合はフェイルセイフを使わない方がよさそう
			}

			// 場所
			if(regexCache.find("LOCATION", neType)) {
				SpotLoc spotLoc	= new SpotLoc();
				ansNew = spotLoc.spotterLocation(neDataList, morphemeNo, "LOCATION");
				if(ans == null || ansNew.length() > ans.length()) {
					ans = ansNew;
				}

				noFailSafe = 1; // 場所の場合はフェイルセイフを使わない方がよさそう
			}

			// 団体/組織
			if(regexCache.find("ORGANIZATION", neType)) {
				SpotOrg spotOrg	= new SpotOrg(personSuffixIn);
				ansNew = spotOrg.spotterOrganization(neDataList, morphemeNo, "ORGANIZATION");
				if(ans == null || ansNew.length() > ans.length()) {
					ans = ansNew;
				}

				noFailSafe = 1; // 団体/組織の場合はフェイルセイフを使わない方がよさそう
			}

			if(ans != null && !ans.equals("")) {
				// ここまでで解が見つかっている場合はそれを返す．
				return ans;
			}
			if(noFailSafe == 1) {
				// フェイルセーフをつかわないのなら，ここで強制終了
				if(ans == null || ans.equals("")) {
					return "";
				}
				else {
					return ans;
				}
			}
		}

		if(
			(
				(param.getNeMode() == 2 && !param.getNeType().equals("num"))
					|| (param.getNeMode() == 0 && param.getNeType() != null)
			)
			&& morphemeHyosoList.get(morphemeNo).matches("^[０-９〇一二三四五六七八九十零百千万億兆・．—]+$")
		)
		{
			// 数量表現のとき、パタンに合致する部分を解とする

			String ans = "";
			int x	= morphemeNo - 8;
			if(x < 0) {
				x = 0;
			}
			for(int i = x ; i <= morphemeNo ; i++) {
				String tmp = "";
				for(int j = i ; j < morphemeHyosoList.size() && j <= i + 8 ; j++) {
					if(morphemeHyosoList.get(j) == null) {
						continue;
					}
					tmp += morphemeHyosoList.get(j);
//					if(j >= morphemeNo 
//							&& tmp.matches("^(約|おお?よそ|だいたい)?((プラス|マイナス)?[０-９〇一二三四五六七八九十零百千万億兆・．—]+(〜|から))?(プラス|マイナス)?("+pattern1+")(くらい|程度|ほど|以[上下内]|未満)?$")
//							)
					if(j >= morphemeNo 
							&& regexCache.find("^(約|おお?よそ|だいたい)?((プラス|マイナス)?[０-９〇一二三四五六七八九十零百千万億兆・．—]+(〜|から))?(プラス|マイナス)?("+pattern1+")(くらい|程度|ほど|以[上下内]|未満)?$", tmp)
						)
					{
						ans = tmp;
					}
				}
				if(ans != null && !ans.equals("")) {
					return ans;
				}
			}
		}

		// 解候補が括弧内にあるか?
		if(param.getNeType() == null || param.getNeType().equals("") || param.getNeType().equals("0")) {
			SpotParen spotParen	= new SpotParen();
			String ans = spotParen.spotterParen(morphemeHyosoList, morphemeNo, "PAREN");

			// もしも見つかったなら，表現の長さで制限をかけ，その長さ以下であれば返す．
			// 制限よりも長ければ候補としない( "" を返し，フェイルセーフにいかない)．
			if(ans != null && !ans.equals("")) {
				if(ans.split("").length <= MAX_EXP_LEN) {
					return ans;
				}
				else {
					return "";
				}
			}
		}

		// フェイルセイフ
		// 同一文節内で形態素を接続
		String ans = morphemeHyosoList.get(morphemeNo);
		int[] kNumList	= TrLoc01.juman2knp(morphemeNo, knpInfoMap);
		int kx	= kNumList[0];
		int ky	= kNumList[1];
		int kyTmp = ky;

		KnpSegment knpSegment	= knpSegmentList.get(kx);
		while( --kyTmp >= 0 ) {
			List<Morpheme> tempMorphemeList	= knpInfoMap.get(knpSegment);
			Morpheme morpheme	= tempMorphemeList.get(kyTmp);
			String surfaceForm	= morpheme.getSurfaceForm();
			String pos			= morpheme.getPos();
			String detailePos	= morpheme.getDetailedPos();
			if((pos.matches("名詞|未定義語|接頭辞|接尾辞") && regexCache.find("名詞性", detailePos))
					|| (pos.matches("副詞|特殊") && detailePos.matches("記号"))) {
				ans = surfaceForm + ans;
			}
			else {
				break;
			}
		}

		kyTmp = ky;
		while(true) {
			++kyTmp;
			List<Morpheme> morphemeList	= knpInfoMap.get(knpSegment);
			if(morphemeList == null) {
				break;
			}

			if(morphemeList.contains(kyTmp)) {
				Morpheme morpheme	= morphemeList.get(kyTmp);
				String surfaceForm	= morpheme.getSurfaceForm();
				String pos			= morpheme.getPos();
				String detailePos	= morpheme.getDetailedPos();

				if(morpheme != null && ((pos.matches("名詞|未定義語|接頭辞|接尾辞") && regexCache.find("名詞性", detailePos))
						|| (pos.matches("副詞|特殊") && detailePos.matches("記号")))) {
					ans += surfaceForm;
				}
				else {
					break;
				}
			}
			else {
				break;
			}
		}

		// 先頭と最後にある記号を削る
		String sym	= "・";
		ans	= ans.replaceFirst("^"+sym, "");
		ans	= ans.replaceFirst(sym+"$", "");

		// もしも見つかったなら，表現の長さで制限をかけ，それよりも短ければ返す．
		if((ans != null && !ans.equals("")) && (ans.split("").length <= MAX_EXP_LEN)) {
			return testAns(ans, param.getNeType());
		}
		return "";
	}

	//----------------------------------------------------------------
	// private methods
	//----------------------------------------------------------------

	/**
	 * 数値の解は除外してみるテスト
	 * 
	 * @param ans
	 * @param neType
	 * @return
	 */
	private static String testAns(String ans, String neType)
	{
		if(neType.matches("^(PERSON|ORGANIZATION|LOCATION)$")) {
			if(ans.matches("^[０-９〇一二三四五六七八九十零百千万億兆・．—]+$")) {
				ans = "";
			}
		}
		return ans;
	}
}
