package org.kachako.components.factoid_qa.module;

import java.util.List;

/**
 * 人名用パタン
 * @author JSA Saitou
 *
 */
public class SpotPerson
{
	/* 肯定パタン (形態素解析列に対するパタン) */
	private static final String PAT_PERSON_PREFIX_CHAR		= "故姥甥嫁兄姑妻姉師弟夫父母妹婿娘姪嫂舅者";	// 「孫」さんがいる．「者」も追加
	private static final String PAT_NON_PERSON_CHAR			= PAT_PERSON_PREFIX_CHAR + "！”＃＄％＆’（）＊＋，．／：；＜＝＞？＠¥＾＿‘｛｜｝〜−";
	private static final String PAT_NON_PERSON_CHAR_HEAD	= PAT_NON_PERSON_CHAR + "・";	// 先頭には「・」はこない
	private static final String PAT_PERSON_KANJI_EXP		= "[亜-瑤ぁ-んァ-ヶー]+";  // [亜-瑤] は漢字1文字
	private static final String PAT_PERSON_ALPHA_KANA_EXP	= "[Ａ-Ｚａ-ｚァ-ヶー・]+";
	private static final String PAT_PERSON_ALPHA_KANA_EXP_HEAD	= "[Ａ-Ｚａ-ｚァ-ヶー]+";
	private static final String PAT_PERSON_POS = "(?:名詞|未定義語|接頭辞|接尾辞\\s\\S+\\s名詞性|副詞|特殊\\s\\S+\\s記号)";

	private static final String PAT_PERSON_KANJI_MORPH =
								"(?:"
									+ PAT_PERSON_KANJI_EXP + "\\s(?:\\S+\\s){2}" + PAT_PERSON_POS + ".*(?:<>|<PERSON>).*"
								+")";

	private static final String PAT_PERSON_ALPHA_KANA_MORPH =
								"(?:"
			  						+ PAT_PERSON_ALPHA_KANA_EXP + "\\s(?:\\S+\\s){2}" + PAT_PERSON_POS + ".*(?:<>|<PERSON>|<ORGANIZATION>|<ARTIFACT>).*"
			  					+")";

	private static final String PAT_PERSON_ALPHA_KANA_MORPH_HEAD =
								"(?:"
			  						+ PAT_PERSON_ALPHA_KANA_EXP_HEAD + "\\s(?:\\S+\\s){2}" + PAT_PERSON_POS + ".*(?:<>|<PERSON>|<ORGANIZATION>|<ARTIFACT>).*"
			  					+")";

	private static final String PAT_PERSON_PREFIX_MORPH =
								"(?:"
									+"(?:"
										+"\\S+人"
										+"|"
										+"[長二三四五六七八九十][男女]"
										+"|"
										+"[" + PAT_PERSON_PREFIX_CHAR + "]"
									+")\\s.*"
								+")";

	private static String PAT_PERSON_SUFFIX_OUT	= "";

	// パタンは配列の先頭のものから順次適用され，ヒットしたらそれを利用
	private static String[] patPersonList = {
				// Pattern 0 : PERSON タグが入っている場合でカナ，アルファベットの場合  PERSONに矛盾しないように結ぶ
				"("
					+"(?!(?:^|\n)[" + PAT_NON_PERSON_CHAR_HEAD + "]\\s.*)"	// 先頭形態素チェック
					+"(?!(?:^|\n)" + PAT_PERSON_PREFIX_MORPH + ")"
					+"(?:"	// PERSONタグの連続
						+"(?:(?:^|\n)" + PAT_PERSON_ALPHA_KANA_MORPH + ")*"
						+"(?:(?:^|\n)" + PAT_PERSON_ALPHA_KANA_EXP + "\\s.*<PERSON>.*)+"
						+"(?:"
							+"(?:(?:|\n)" + PAT_PERSON_ALPHA_KANA_MORPH + ")*?"
							+"(?="
								+"(?:(?:|\n)[・]\\s.*)?"
								+"(?:(?:|\n)[先前元新旧正副]\\s.*)?"
								+"(?:" + PAT_PERSON_SUFFIX_OUT + ")"
							+")"
						+")?"
					+")"
				+")",
				// Pattern 1 : PERSON タグが入っている場合で日本語の場合  PERSONに矛盾しないように結ぶ
				"("
					+"(?!(?:^|\n)[" + PAT_NON_PERSON_CHAR_HEAD + "]\\s.*)"	// 先頭形態素チェック
					+"(?!(?:^|\n)" + PAT_PERSON_PREFIX_MORPH + ")"
					+"(?:"	// PERSONタグの連続
						+"(?:(?:^|\n)[^" + PAT_NON_PERSON_CHAR + "]\\s(?:\\S+\\s){2}" + PAT_PERSON_SUFFIX_OUT + ".*)*"
						// タグがないところは1文字の漢字などを連接
						+"(?:(?:^|\n)" + PAT_PERSON_KANJI_EXP + "\\s.*<PERSON>.*)+"
						+"(?:"
							+"(?:(?:|\n)" + PAT_PERSON_KANJI_MORPH + ")*?"
							+"(?="
								+"(?:(?:|\n)[・]\\s.*)?"
								+"(?:(?:|\n)[先前元新旧正副]\\s.*)?"
								+"(?:" + PAT_PERSON_SUFFIX_OUT + ")"
							+")"
						+")?"
					+")"
				+")",
				// Pattern 2 : 英語等の表記の場合 一つは PERSONを含むようにする．
				"("
					+"(?:"
						+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*<PERSON>.*)"
						+"(?:"
							+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ・][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<PERSON>|<ORGANIZATION>|<ARTIFACT>).*)*"
							+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<PERSON>|<ORGANIZATION>|<ARTIFACT>).*)"
						+")?"
					+")"
					+"|"
					+"(?:"
						+"(?:"
							+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<PERSON>|<ORGANIZATION>|<ARTIFACT>).*)"
							+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ・][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<PERSON>|<ORGANIZATION>|<ARTIFACT>).*)*"
						+")?"
						+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*<PERSON>.*)"
					+")"
					+"|"
					+"(?:"
						+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<PERSON>|<ORGANIZATION>|<ARTIFACT>).*)"
						+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ・][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<PERSON>|<ORGANIZATION>|<ARTIFACT>).*)*"
						+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*<PERSON>.*)"
						+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ・][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<PERSON>|<ORGANIZATION>|<ARTIFACT>).*)*"
						+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<PERSON>|<ORGANIZATION>|<ARTIFACT>).*)"
					+")"
				+")",
				// Pattern 3 : 名前に後続する表現を種に人名を見つける(カナ，アルファベットの場合)  PERSONに矛盾しないように結ぶ
				"(?:"
					+"(?!(?:^|\n)[" + PAT_NON_PERSON_CHAR_HEAD + "]\\s.*)"	// 先頭形態素チェック
					+"(?!(?:^|\n)" + PAT_PERSON_PREFIX_MORPH + ")"	// 先頭形態素チェック
					+"("
						+"(?:(?:^|\n)" + PAT_PERSON_ALPHA_KANA_MORPH_HEAD + ")"
						+"(?:(?:^|\n)" + PAT_PERSON_ALPHA_KANA_MORPH + ")*?"
					+")"
					+"(?:"
						+"(?:(?:|\n)[・]\\s.*)?"
						+"(?:(?:|\n)[先前元新旧正副]\\s.*)?"
						+"(?:(?:|\n)" + PAT_PERSON_SUFFIX_OUT + ")"
					+")"
				+")",
				// Pattern 4 : 名前に後続する表現を種に人名を見つける(日本語の場合)  PERSONに矛盾しないように結ぶ 
				"(?:"
					+"(?!(?:^|\n)[" + PAT_NON_PERSON_CHAR_HEAD + "]\\s.*)"	// 先頭形態素チェック
					+"(?!(?:^|\n)" + PAT_PERSON_PREFIX_MORPH + ")"	// 先頭形態素チェック
					+"("
						+"(?:(?:^|\n)" + PAT_PERSON_KANJI_MORPH + ")+"
					+")"
					+"(?:"
						+"(?:(?:|\n)[・]\\s.*)?"
						+"(?:(?:|\n)[先前元新旧正副]\\s.*)?"
						+"(?:(?:|\n)" + PAT_PERSON_SUFFIX_OUT + ")"
					+")"
				+")",
				// Pattern 5 : 名前に前接する表現を種に人名を見つける(英語の場合)
				"(?:"
					+"(?:(?:^|\n)" + PAT_PERSON_PREFIX_MORPH + ")"
					+"(?:(?:^|\n)[・]\\s.*)?"
					+"("
						+"(?:(?:^|\n)" + PAT_PERSON_ALPHA_KANA_MORPH_HEAD + ")"
						+"(?:"
							+"(?:(?:^|\n)" + PAT_PERSON_ALPHA_KANA_MORPH + ")*"
							+"(?:(?:^|\n)" + PAT_PERSON_ALPHA_KANA_MORPH_HEAD + ")"
						+")?"
					+")"
				+")",
				// Pattern 6 : 名前に前接する表現を種に人名を見つける(日本語の場合)
				"(?:"
					+"(?:(?:^|\n)" + PAT_PERSON_PREFIX_MORPH + ")"
					+"(?:(?:^|\n)[・]\\s.*)?"
					+"("
						+"(?:(?:^|\n)" + PAT_PERSON_KANJI_MORPH + ")+"
					+")"
				+")",
				// Pattern 7 :  PERSONがなくても英語名をつなぐ
				"("
					+"(?:"
						+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<PERSON>|<ORGANIZATION>|<ARTIFACT>).*)"
						+"(?:"
							+"(?:\n[Ａ-Ｚァ-ヶ・][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<PERSON>|<ORGANIZATION>|<ARTIFACT>).*)*"
							+"(?:\n[Ａ-Ｚァ-ヶ][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<PERSON>|<ORGANIZATION>|<ARTIFACT>).*)"
						+")?"
					+")"
				+")",
				// Pattern 8 : 括弧の中(「」)
				"(?:"
					+"(?:(?:^|\n)「\\s.*)"
					+"("
						+"(?:(?:^|\n)\\S+\\s.*)+?"
					+")"
					+"(?:(?:^|\n)」\\s.*)"
				+")",
				// Pattern 9 : 括弧の中(『』)
				"(?:"
					+"(?:(?:^|\n)『\\s.*)"
					+"("
						+"(?:(?:^|\n)\\S+\\s.*)+?"
					+")"
					+"(?:(?:^|\n)』\\s.*)"
				+")",
				// Pattern 10 : 括弧の中(（）)
				"(?:"
					+"(?:(?:^|\n)（\\s.*)"
					+"("
						+"(?:(?:^|\n)\\S+\\s.*)+?"
					+")"
					+"(?:(?:^|\n)）\\s.*)"
				+")",
				// Pattern 11 : 括弧の中(“”)
				"(?:"
					+"(?:(?:^|\n)“\\s.*)"
					+"("
						+"(?:(?:^|\n)\\S+\\s.*)+?"
					+")"
					+"(?:(?:^|\n)”\\s.*)"
				+")"
	};

	// 大域変数
//	private static final int FLAG_INIT	= 1;

	/**
	 * コンストラクタ
	 */
	public SpotPerson(String suffixOut)
	{
		PAT_PERSON_SUFFIX_OUT	= suffixOut;
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	//	##################################################################
	//	# spotter_person()
	//	#   形態素情報列において，指定した形態素を含む，指定した型の日付表現
	//	#   が あれば取り出す．なければ ""
	//	##################################################################
	//	sub spotter_person($$$) {
	public String spotterPerson(List<String> neDataList, int morphemeNo, String netType)
	{
		String ans	= "";

		if(netType.equals("PERSON")) {
			String[] linesIds = SpotAnswer.spotterAddIndex(neDataList);

			// パタンを順番に適用して，一番最初に見つかったものを利用
			for(int i = 0 ; i < patPersonList.length; i++) {
//System.out.println(patPersonList[i]);
				String pattern	= patPersonList[i];
				ans	= SpotAnswer.spotterMkPatMatchFuncMorphForAns(pattern, linesIds, morphemeNo);
				if(ans != null && ans.length() > 0) {
					break;
				}
			}
		}
		else {
			ans = "";
		}

		return ans;
	}

}
