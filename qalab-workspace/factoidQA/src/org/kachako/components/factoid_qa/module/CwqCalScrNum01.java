package org.kachako.components.factoid_qa.module;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.kachako.components.factoid_qa.manage.AnswerParam;
import org.kachako.components.factoid_qa.manage.LineInfo;
import org.kachako.components.factoid_qa.manage.QueryInfo;
import org.kachako.components.factoid_qa.manage.ScoreInfo;
import org.kachako.share.regex.RegexCache;
import org.kachako.types.japanese.syntactic.KnpSegment;
import org.kachako.types.japanese.syntactic.KyotoDependency;
import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

public class CwqCalScrNum01
{
	private static final RegexCache regexCache = new RegexCache();

	/**
	 * コンストラクタ
	 */
	public CwqCalScrNum01()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	/**
	 * 数値情報同定によるスコア導出
	 * 
	 * @param param パラメータクラス
	 * @param lineInfo 文の情報
	 * @param scoreInfo スコア情報
	 * @param queryInfo クエリ情報
	 * @param dList 属性の判定 決定リスト
	 * @param aList 属性の判定 決定リスト
	 * @param cpcBySubMap EDR概念体系辞書
	 * @param cphByIdMap EDR概念見出し辞書
	 * @param cphByNameMap EDR概念見出し辞書
	 *
	 * @return スコア情報
	 */
	public static ScoreInfo calscrRnum(AnswerParam param,LineInfo lineInfo,
											ScoreInfo scoreInfo, QueryInfo queryInfo,
											String[] dList, String[] aList,
											HashMap<String,String> cpcBySubMap,
											HashMap<String,String> cphByIdMap,
											HashMap<String,String> cphByNameMap)
	{
		ScoreInfo	tempScoreInfo	= scoreInfo;
		// 質問文の「もの」または「属性」がとれていない場合
		if(queryInfo.getObjectWord() == null || queryInfo.getAttributeWord() == null) {
			tempScoreInfo.setRne(scoreInfo.getPne());
			return tempScoreInfo;
		}

		// 初期値を設定
		// 質問文タイプに合致する解候補には最低点(param.getNum())をあたえる。
		boolean flag = false;
		TreeMap<Integer,Double> pNeMap	= tempScoreInfo.getPne();
		TreeMap<Integer,Double> rNeMap	= tempScoreInfo.getRne();

		for(Entry<Integer,Double> entry : pNeMap.entrySet()) {
			int i = entry.getKey();
			if(pNeMap.get(i) > 0.0) {
				rNeMap.put(i, param.getNum());
				if(!flag) {
					flag	= true;
				}
			}
			else {
				rNeMap.put(i, 0.0);
			}
		}
		tempScoreInfo.setRne(rNeMap);

		if(!flag) {
			// 行中に数値がない場合
			return tempScoreInfo;
		}

		List<KyotoDependency> kyotoDependencyList	= lineInfo.getKyotoDependencyList();
		List<KnpSegment> knpSegmentList	= lineInfo.getKnpSegmentList();
		Map<KnpSegment, List<Morpheme>> knpInfoMap	= lineInfo.getKnpInfoMap();

		// 数量表現抽出（新）
		String[] numLines	= PicknumQa01.picknum(dList, aList, "", "-999", kyotoDependencyList, knpSegmentList, knpInfoMap,
																						cpcBySubMap, cphByIdMap, cphByNameMap);
		for(String num : numLines) {
			String[] numArray	= num.split(",");
			if(numArray.length <= 1) {
				//  「もの」「属性」がとれていないとき
				continue;
			}
			// 数量表現、始点、終点
			int nStart	= 0;
			int nEnd	= 0;
			if(regexCache.find("\\((\\d+)\\-(\\d+)\\)$", num)) {
				nStart	= Integer.parseInt(regexCache.lastMatched(1));
				nEnd	= Integer.parseInt(regexCache.lastMatched(2));
			}

			flag	= false;
			for(int i = nStart; i <= nEnd; i++) {
				if(pNeMap.get(i) > 0.0) {
					flag	= true;
					break;
				}
			}
			if(!flag) {
				// 抽出した数量表現が質問文タイプと一致しないとき
				continue;
			}


			// スコア
			double scrObj	= 0;
			double scrAtt	= 0;
			//  一致文字数
			int nObj	= 0;
			int nAtt	= 0;
			// 数量表現
			String lObj	= null;
			String lAtt	= null;

			//  ものを照合
			if(numArray[1].length() > 0 && regexCache.find("\\S+$",numArray[1])) {
				lObj	= regexCache.lastMatched();
				if(queryInfo.getObjectWord() != null) {
					Pattern p = Pattern.compile(queryInfo.getObjectSplt());
					Matcher m = p.matcher(lObj);
					m.reset();
					while(m.find()) {
						 nObj++;
					}

					// 最大１となるようにスコアリング
					scrObj =  ( ( nObj / ( lObj.length() / 2 ) ) +
						  ( nObj / queryInfo.getObjectLength() ) ) / 2;
				}
			}

			// 属性を照合
			if(numArray[2].length() > 0 && regexCache.find("\\S+$",numArray[2])) {
				lAtt = regexCache.lastMatched();
				if(queryInfo.getAttributeWord() != null) {
					Pattern p = Pattern.compile(queryInfo.getAttributeSplt());
					Matcher m = p.matcher(lAtt);
					m.reset();
					while(m.find()) {
						 nAtt++;
					}

					// 最大１となるようにスコアリング
					scrAtt = ( ( nAtt / ( lAtt.length() / 2 ) ) +
						  ( nAtt / queryInfo.getAttributeLength() ) ) / 2;
				}
			}

			double scr	= 0;
			// param.getNum()=0.5 なら gain=0.5
			double gain	= ( 1 - param.getNum() );
			// スコア計算
			if(queryInfo.getObjectWord() != null && queryInfo.getAttributeWord() != null) {
				scr = ( ( scrObj + scrAtt ) / 2 ) * gain;
			}
			else if(queryInfo.getObjectWord() != null) {
				scr = scrObj * gain;
			}
			else {
				scr = scrAtt * gain;
			}

			// 念のため
			if(scr > gain) {
				scr = gain;
			}
			for(int i = nStart ; i <= nEnd ; i++ ) {
				// $$scr_rnum[$i]が最大１となる
				if(rNeMap.get(i) > 0) {
					rNeMap.put(i, rNeMap.get(i)+scr);
				}
			}
			tempScoreInfo.setRne(rNeMap);
		}
		return tempScoreInfo;
	}
}
