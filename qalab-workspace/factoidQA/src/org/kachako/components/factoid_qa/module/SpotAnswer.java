package org.kachako.components.factoid_qa.module;

import java.util.ArrayList;
import java.util.List;

import org.kachako.share.regex.RegexCache;

public class SpotAnswer
{

	private static final RegexCache regexCache	= new RegexCache();

	/**
	 * コンストラクタ
	 */
	public SpotAnswer()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	//	##################################################################
	//	# 形態素情報にインデクスを付加する
	//	##################################################################
	//	sub spotter_add_index($) {
	public static String[] spotterAddIndex(List<String> neDataList)
	{
		int morphemeNo	= 0;
		List<String> list	= new ArrayList<>();
		if(neDataList != null && neDataList.size() > 0) {
			for(String line : neDataList) {
				line	= line.substring(0, line.length());
				list.add(line + "<morph id=\"" + ( morphemeNo++ ) + "\">");
			}
		}
		return list.toArray(new String[0]);
	}

	//	##################################################################
	//	# パタンマッチ用の関数(形態素対応)を生成
	//	#   「複数行の形態素情報(リストのリファレンス)と注目している形態素番号を与えると，
	//	#     指定した形態素を含みそのパタンにマッチする行の表層表現を返す」
	//	#    という関数を生成し，そのポインタを返す
	//	##################################################################
	//
	//	sub spotter_mk_pat_match_func_morph_for_ans($) {
	public static String spotterMkPatMatchFuncMorphForAns(String pattern,String[] lines, int morphemeNo)
	{
		if(lines == null || lines.length <= 0) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		for(String line : lines) {
			sb.append(line).append("\n");
		}
		String strTarget	= sb.toString();
		String strExtrcted	= "";
		String ans			= "";
		String strIdM		= "<morph id=\"" + morphemeNo +"\">";

		for(int i = 0; i <= morphemeNo; i++) {
			if(regexCache.find(pattern, strTarget)) {
				strExtrcted	= regexCache.lastMatched(1);
				if(strExtrcted.indexOf(strIdM) >= 0) {
					break;
				}
				else {
					strExtrcted = "";
				}
			}
			strTarget	= strTarget.substring(strTarget.indexOf("\n")+1);
		}
		if(!strExtrcted.equals("")) {
			String[] extrctedList	= strExtrcted.split("\n");
			for(String str : extrctedList) {
				if(regexCache.find("^(\\S+)\\s", str)) {
					ans	= ans + regexCache.lastMatched(1);
				}
			}
		}
		return ans;
	}

}
