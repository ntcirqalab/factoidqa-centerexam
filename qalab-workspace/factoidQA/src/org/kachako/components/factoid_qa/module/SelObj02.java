package org.kachako.components.factoid_qa.module;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.kachako.components.factoid_qa.manage.DecisionData;
import org.kachako.share.regex.RegexCache;

public class SelObj02
{
	private static final RegexCache regexCache = new RegexCache();

	/**
	 * コンストラクタ
	 */
	public SelObj02()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	// sub sel_obj($$$@){
	public static HashMap<String,String> selObj(String threshold,
											HashMap<String, List<DecisionData>>	objDecisionDataMap,
											String branch,List<String> objCandidateList)
	{
		String	mode		= "";

		Pattern pattern = Pattern.compile("[i]");
		Matcher matcher = pattern.matcher(threshold);
		if(matcher.find()) {
			threshold	= matcher.replaceAll("");
			mode	= "ignore";
		}

		HashMap<String,String> hm	= new HashMap<String,String>();
		hm.put("objNum","");
		hm.put("objExp","");
		hm.put("objInf","");

//System.out.println("["+branch+"]");
		List<DecisionData> objDecisionDataList	= objDecisionDataMap.get(branch);
		if(objDecisionDataList == null) {
			return null;
		}

		for(int i = 0; i < objDecisionDataList.size(); ++i) {
			DecisionData	decisionData	= objDecisionDataList.get(i);
			String evidence	= decisionData.getEvidence();
			double llh	= decisionData.getLlh();
			if(evidence.equals("non_obj") && !mode.equals("ignore")
					|| llh < Double.parseDouble(threshold)) {
				return hm;
			}
			for(int j = 0 ; j < objCandidateList.size() ; ++j) {
				if(regexCache.find("(.+?)\\((\\d+)\\)=(.+)",objCandidateList.get(j))) {
					String tempInf	= regexCache.lastMatched(3);
					if(evidence.equals(tempInf)) {
						hm.put("objNum",regexCache.lastMatched(2));
						hm.put("objExp",regexCache.lastMatched(1));
						hm.put("objInf",regexCache.lastMatched(3));

						return hm;
					}
				}
			}
		}
		return null;
	}
}
