package org.kachako.components.factoid_qa.module;

import org.apache.uima.jcas.cas.StringArray;
import org.kachako.share.regex.RegexCache;

public class PknumDcParticle01
{
	private static final RegexCache regexCache = new RegexCache();

	/**
	 * コンストラクタ
	 */
	public PknumDcParticle01()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	//#KNPの出力データにおいて指定された文節の格助詞を返す
	// sub dc_particle(@) {
	/**
	 * KNPの出力データにおいて指定された文節の格助詞を返す.
	 * 
	 * @param bunsetsuFeatures 文節の付加情報
	 * @return 文節の格助詞
	 */
	public static String dcParticle(StringArray bunsetsuFeatures)
	{
		if(bunsetsuFeatures == null || bunsetsuFeatures.size() <= 0) {
			return "";
		}

		for(int i=0; i < bunsetsuFeatures.size(); i++) {
			String	str	= bunsetsuFeatures.get(i);
			if(regexCache.find("係:(.+)", str)) {
				return regexCache.lastMatched(1);
			}
		}
		return "na";
	}
}
