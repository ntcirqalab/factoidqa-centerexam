package org.kachako.components.factoid_qa.module;

import java.util.ArrayList;
import java.util.List;

import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

public class JumanInfo01
{
	/**
	 * コンストラクタ
	 */
	public JumanInfo01()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	/**
	 * 
	 * @param morpheme 形態素情報
	 * @return
	 */
	public static String morpheInfoExp(Morpheme morpheme)
	{
		if(morpheme == null) {
			return null;
		}
		return morpheme.getSurfaceForm();
	}

	//sub get_StartEnd_char_from_morph_info(@) {
	public static List<String> getStartEndCharFromMorpheInfo(List<Morpheme> morphemeList)
	{
		List<String> tagStartEnd = new ArrayList<>();

		for(Morpheme morpheme : morphemeList) {
			String exp = morpheInfoExp(morpheme);
			String pos = morpheInfoPosAll(morpheme);

			String[] charExp	= exp.split("");
			int lenExp	= charExp.length - 1;

			if(lenExp == 1) {
				tagStartEnd.add("S-"+pos);
			}
			else {
				tagStartEnd.add("B-"+pos);
				lenExp -= 2;
				for(int i = 0; i < lenExp; i++) {
					tagStartEnd.add("I-"+pos);
				}
				tagStartEnd.add("E-"+pos);
			}
		}

		return tagStartEnd;
	}


	//----------------------------------------------------------------
	// private methods
	//----------------------------------------------------------------

	//sub morphinfo_POS_all($) {
	private static String morpheInfoPosAll(Morpheme morpheme)
	{
		if(morpheme == null) {
			return null;
		}

		if(morpheme.getDetailedPos().equals("*")) {
			return morpheme.getPos();
		}
		return morpheme.getPos() + "-" +morpheme.getDetailedPos();
	}
}
