package org.kachako.components.factoid_qa.module;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.kachako.components.factoid_qa.manage.AnswerParam;
import org.kachako.components.factoid_qa.manage.KeyWord;
import org.kachako.components.factoid_qa.manage.KnpBnstHash;
import org.kachako.components.factoid_qa.manage.LineInfo;
import org.kachako.components.factoid_qa.manage.QueryInfo;
import org.kachako.components.factoid_qa.manage.QueryKeywordInfo;
import org.kachako.components.factoid_qa.manage.ScoreInfo;
import org.kachako.share.regex.RegexCache;
import org.kachako.types.japanese.syntactic.KnpSegment;
import org.kachako.types.japanese.syntactic.KyotoDependency;
import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

/**
 * 命題照合スコア計算をするクラスです.
 *
 * @author JSA saitou
 *
 */
public class CwqCalScr05
{
	private static final RegexCache regexCache = new RegexCache();

	/**
	 * コンストラクタ
	 */
	public CwqCalScr05()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	/**
	 * ボーダーラインに満たないものをリストから消す（原始的探索制御用）.
	 * 
	 * @param param パラメータ
	 * @param scoreInfoMap 文スコア情報
	 * @param lidList 文IDのリスト
	 * @param ansNum 検索結果数
	 * @param level レベル
	 * @return
	 */
	public static List<String> cutBorder(AnswerParam param,Map<String, ScoreInfo> scoreInfoMap,
												List<String> lidList, int ansNum, int level)
	{
		Map<String, ScoreInfo> tempScoreInfoMap	= scoreInfoMap;
		List<String> tempLidList	= lidList;
		List<String> cutLidList		= new ArrayList<String>();
		double border	= 0;
		double firstLine	= 0;
		boolean flag	= false;
		for(String lid : lidList) {
			ScoreInfo	scoreInfo	= tempScoreInfoMap.get(lid);
			double line	= scoreInfo.getLine();
			if(!flag) {
				border	= line * param.getBorder();
				firstLine	= line;
				flag	= true;
			}
			else {
				if(line < border) {
					cutLidList.add(lid);
					tempLidList.remove(lid);
				}
			}
		}

		if(param.getMode() == -2 && level != 0) {
			// 単純な枝刈りのとき、解候補の最低数を確保する
			if(tempLidList.size() >= ansNum && firstLine > 0 ) {
				return tempLidList;
			}

			int ansCand = 0;
			for(String cutLid : cutLidList) {
				for(String tempLid : tempLidList) {
					ScoreInfo	scoreInfo	= tempScoreInfoMap.get(tempLid);
					TreeMap<Integer,Double> rKeyMap	= scoreInfo.getRkeyword();
					for(Entry<Integer,Double> entry :  rKeyMap.entrySet()) {
						double score	= entry.getValue();
						if(score > 0) {
							ansCand++;
						}
					}
					if(ansCand > ansNum) {
						return tempLidList;
					}
				}
				tempLidList.add(cutLid);
			}
		}
		return tempLidList;
	}

	/**
	 * 文節の区切りと格を推定する.
	 * 
	 * @param morphemeList 形態素情報リスト
	 * @return
	 */
	public static List<String> cutBnst(List<Morpheme> morphemeList)
	{
		List<String> pBustList	= new ArrayList<>();

		Morpheme currentMorpheme	= null;
		for(int i = 0; i < morphemeList.size(); i++) {
			currentMorpheme	= morphemeList.get(i);
			Morpheme nextMorpheme	= null;
			if(i + 1 < morphemeList.size()) {
				nextMorpheme	= morphemeList.get(i+1);
			}

			String currentHyoso	= currentMorpheme.getSurfaceForm(); // 0
			String currentPos	= currentMorpheme.getPos(); // 3
			String currentDetailedPos	= currentMorpheme.getDetailedPos(); // 5
			String currentConjugateType	= currentMorpheme.getConjugateType(); //7
			String currentConjugateForm	= currentMorpheme.getConjugateForm(); // 9

			String nextHyoso	= nextMorpheme != null ? nextMorpheme.getSurfaceForm() : ""; // 0
			String nextPos		= nextMorpheme != null ?  nextMorpheme.getPos() : ""; // 3
			String nextDetailedPos		= nextMorpheme != null ?  nextMorpheme.getDetailedPos() : ""; // 5
			String nextConjugateType	= nextMorpheme != null ?  nextMorpheme.getConjugateType() : ""; //7
			String nextConjugateForm	= nextMorpheme != null ?  nextMorpheme.getConjugateForm() : ""; //9

			// 文節が切れない条件
			if(
				(
					(
						(currentPos.equals("形容詞") && regexCache.find("連用",currentConjugateForm))
						|| currentPos.equals("未定義語")
						|| regexCache.find("^(サ変名詞|名詞性)",currentDetailedPos)
					)
					&& nextConjugateType.equals("サ変動詞")
				)
				|| (regexCache.find("^(名詞|未定義語)",currentPos) && regexCache.find("^名詞",nextDetailedPos) )
				|| currentDetailedPos.matches("^括弧始$")
				|| regexCache.find("^[、。]", nextHyoso)
				|| nextPos.matches("^助動?詞$")
				|| ( nextHyoso.equals("の") && nextDetailedPos.equals("形式名詞") )
			  )
			{
				continue;
			}
			else if( // 文節が切れる条件
					regexCache.find("^[、。　]", currentHyoso)
							|| regexCache.find("^(連体|接続|指示|感動|副)詞", currentPos)
							|| (   currentPos.matches("^(形容|助?動|判定)詞")
								&& currentConjugateType.matches("連体") )
							|| (   currentPos.matches("^名詞")
								&& currentDetailedPos.matches("^(副詞的|時相)名詞") )
							|| nextHyoso.equals("　")
							|| nextPos.matches("^(連体|感動)詞$")
							|| nextDetailedPos.equals("括弧始")
							|| ( currentPos.equals("助詞") && !nextPos.matches("^(判定詞|接尾辞)") )
							|| (   currentHyoso.matches("^(名詞|未定義語)")
								&& nextPos.matches("^((形容|動)詞|接頭辞)") )
							|| ( currentConjugateType.equals("サ変動詞") && nextPos.matches("^動詞$") )
							|| (
								(
									currentPos.matches("^(形容|判定|助?動)詞")
									|| currentDetailedPos.matches("^(動詞性|形容詞性述語)接尾辞")
								)
								&& nextPos.matches("^((形容|動|判定|名)詞|未定義語|接頭辞)")
							)
							|| (
									currentPos.matches("^接尾辞")
								&& (  nextPos.matches("^((形容|動)詞|接頭辞)")
									|| nextDetailedPos.equals("サ変名詞"))
							)
							|| (   currentPos.equals("特殊")
								&& !currentDetailedPos.equals("括弧終")
								&& nextPos.matches("^((形容|助?動|判定)詞|接頭辞)") )
							|| ( !currentDetailedPos.equals("名詞接頭辞") && nextDetailedPos.equals("数詞") )
						  ) {
				Morpheme lastMorpheme	= null;
				if(i >= 1 && currentHyoso.equals("、")) {
					currentMorpheme	= morphemeList.get(i-1);
					if(i >= 2) {
						lastMorpheme = morphemeList.get(i-2);
					}
				}
				else if(i >= 1) {
					lastMorpheme = morphemeList.get(i-1);
				}

				currentHyoso	= currentMorpheme.getSurfaceForm(); // 0
				currentPos		= currentMorpheme.getPos(); // 3
				currentDetailedPos	= currentMorpheme.getDetailedPos(); // 5
				currentConjugateType	= currentMorpheme.getConjugateType(); //7
				currentConjugateForm	= currentMorpheme.getConjugateForm(); // 9

				String lastHyoso	= (lastMorpheme != null ? lastMorpheme.getSurfaceForm(): ""); // 0
				String lastPos	= (lastMorpheme != null ? lastMorpheme.getPos() : ""); // 3
				String lastDetailedPos	= (lastMorpheme != null ? lastMorpheme.getDetailedPos() : ""); // 5

				String bnstStr	= "* ";
				// 格の推定
				if(currentPos.equals("助詞")) {
					if(lastHyoso.matches("^(へ|など)$") && currentHyoso.equals("の")) {
						bnstStr = bnstStr +"連体|ノ格";
					}
					else if(lastHyoso.equals("もの") && currentHyoso.equals("の")) {
						bnstStr = bnstStr +"ノ格|連用";
					}
					else if(lastHyoso.equals("と") && currentHyoso.equals("も")) {
						bnstStr = bnstStr +"ト格";
					}
					else if(lastHyoso.equals("で") && currentHyoso.equals("は")) {
						bnstStr = bnstStr +"デ格";
					}
					else if(currentHyoso.matches("^([はもか]|だけ|こそ)$")) {
						bnstStr = bnstStr +"未格";
					}
					else if(currentHyoso.equals("でも")) {
						bnstStr = bnstStr +"デ格";
					}
					else if(currentHyoso.equals("で")) {
						bnstStr = bnstStr +"デ格|連用";
					}
					else if(currentHyoso.equals("から")) {
						if(lastPos.matches("(助?動|形容|判定)詞")
								|| regexCache.find("(助?動|形容)詞性", lastDetailedPos)) {
							bnstStr = bnstStr +"連用";
						}
						else {
							bnstStr = bnstStr +"カラ格";
						}
					}
					else if(currentHyoso.equals("まで")) {
						if(lastPos.matches("(助?動|形容|判定)詞")
								|| regexCache.find("(助?動|形容)詞性", lastDetailedPos)) {
							bnstStr = bnstStr +"連用";
						}
						else {
							bnstStr = bnstStr +"マデ格";
						}
					}
					else if(currentHyoso.matches("^けれ?ども?$")) {
						bnstStr = bnstStr +"連用";
					}
					else if(currentHyoso.equals("と")) {
						bnstStr = bnstStr +"ト格|連用";
					}
					else if(currentDetailedPos.equals("格助詞")) {
						if(currentHyoso.equals("が")) {
							bnstStr = bnstStr +"ガ格";
						}
						else if(currentHyoso.equals("を")) {
							bnstStr = bnstStr +"ヲ格";
						}
						else if(currentHyoso.equals("に")) {
							bnstStr = bnstStr +"ニ格|連用";
						}
						else if(currentHyoso.equals("の")) {
							bnstStr = bnstStr +"ノ格";
						}
					}
					else if(currentDetailedPos.equals("接続助詞")) {
						if(currentHyoso.equals("や")) {
							bnstStr = bnstStr +"連体";
						}
						else if(currentHyoso.equals("が")) {
							bnstStr = bnstStr +"連用";
						}
						else if(currentHyoso.equals("の")) {
							if(nextConjugateForm.matches("テ形$")) {
								bnstStr = bnstStr +"連体";
							}
							else {
								bnstStr = bnstStr +"ノ格";
							}
						}
					}
				}
				else if(currentPos.matches("^(名詞|未定義語)")
						|| regexCache.find("^名詞性", currentDetailedPos)) {
					if(nextDetailedPos.equals("括弧始")) {
						bnstStr = bnstStr +"同格連体";
					}
					else if(currentDetailedPos.matches("^(時相|副詞的)名詞$")) {
						bnstStr = bnstStr +"連用";
					}
					else if(nextPos.matches("^(名詞|未定義語)$")
							|| regexCache.find("^名詞性", currentDetailedPos)) {
						bnstStr = bnstStr +"無格|隣接|連体";
					}
					else if(currentDetailedPos.matches("^サ変名詞$")) {
						bnstStr = bnstStr +"連用";
					}
					else {
						bnstStr = bnstStr +"連体|連用|隣接";
					}
				}
				else if(currentPos.matches("^(感動|接続|副)詞$")
						|| currentDetailedPos.matches("^副詞形態指示詞$")
						|| currentConjugateForm.matches("連用|条件形")) {
					bnstStr = bnstStr +"連用";
				}
				else if(currentPos.equals("連体詞")
						|| currentDetailedPos.matches("^連体詞形態指示詞$")) {
					bnstStr = bnstStr +"連体";
				}
				else if(currentDetailedPos.matches("^括弧終$")) {
					bnstStr = bnstStr +"連用|連体";
				}
				else if(currentPos.equals("接尾辞")
						|| currentConjugateForm.matches("(タ|基本)形$")
						|| nextDetailedPos.matches("^括弧始$")) {
					bnstStr = bnstStr +"連格";
				}
				else if(currentHyoso.matches("^[。　]$")) {
					bnstStr = bnstStr +"n/a";
				}
				else if(nextPos.matches("^(名詞|未定義語)")
						|| regexCache.find("^名詞性", nextDetailedPos)
						|| currentConjugateForm.matches("連体")) {
					bnstStr = bnstStr +"連格|連体";
				}
				else {
					bnstStr = bnstStr +"無格|連格|連体|連用|隣接";
				}

				bnstStr = bnstStr +"\n";
				pBustList.add(bnstStr);
			}
		}
		return pBustList;
	}

	/**
	 * 2gramのスコアを求める.
	 * 
	 * @param param パラメータ情報クラス
	 * @param gramList  質問文バイグラムのリスト
	 * @param lidList 文IDのリスト
	 * @param lineInfoMap 文の情報のMap
	 * @param scoreInfoMap スコア情報のMap
	 *
	 * @return スコア情報のMap
	 */
	public static LinkedHashMap<String, ScoreInfo> calScrC2gram(AnswerParam param, String[] gramList,
												List<String> lidList,
												LinkedHashMap<String, LineInfo> lineInfoMap,
												LinkedHashMap<String, ScoreInfo> scoreInfoMap)
	{
		 LinkedHashMap<String, ScoreInfo> tempScoreInfoMap	= scoreInfoMap;

		for(String lid : lidList) {
			LineInfo	lineInfo	= lineInfoMap.get(lid);
			ScoreInfo	scoreInfo	= tempScoreInfoMap.get(lid);
			String lineL0	= lineInfo.getLineL0();
			int index	= 0;
			int baseCount	= 0;
			for(String gram : gramList) {
				if(regexCache.find(gram, lineL0)) {
					baseCount++;
				}
			}

			String[] l2gramArray	= CwqGet2gram01.get2gram(lineL0);
//			List<Integer> l2gramHitList	= new ArrayList<>();
			HashMap<Integer,Double> l2gramHitMap	= new HashMap<Integer,Double>();
			int i	= 0;
			BIGRAM:
			for(String l2gram : l2gramArray) {
				l2gramHitMap.put(i, 0.0);
				for(String gram : gramList) {
					if(l2gram.equals(gram)) {
						l2gramHitMap.put(i, 1.0);
						i++;
						continue BIGRAM;
					}
				}
				i++;
			}

			// 最初から一文字ごとにスコア付け
			double score = 0;
			TreeMap<Integer,Double> tempScore2gramMap	= new TreeMap<Integer,Double>();
			for(index = 0; index < l2gramArray.length; ++index) {
				score	= baseCount;
				if(index > 0) {
					score	-= l2gramHitMap.get(index-1);
				}
				score	-= l2gramHitMap.get(index);
				tempScore2gramMap.put(index, score);
			}
			// 最後の１文字のスコア
			if(l2gramHitMap.size() > 1) {
				score	= baseCount - l2gramHitMap.get(l2gramHitMap.size()-1);
				tempScore2gramMap.put(l2gramHitMap.size(), score);
			}

			scoreInfo.setC2gram(tempScore2gramMap);
			tempScoreInfoMap.put(lid, scoreInfo);
		}
		return tempScoreInfoMap;
	}

	/**
	 * キーワード含有スコアの推定を求める.
	 *
	 * @param param パラメータクラス
	 * @param queryInfo クエリ情報
	 * @param lidList 文IDリスト
	 * @param lineInfoMap 文の情報のMap
	 * @param scoreInfoMap スコア情報のMap
	 *
	 * @return スコア情報のMap
	 */
	 public static LinkedHashMap<String, ScoreInfo> calScrCKey(AnswerParam param, QueryInfo queryInfo,
												List<String> lidList,
												LinkedHashMap<String, LineInfo> lineInfoMap,
												LinkedHashMap<String, ScoreInfo> scoreInfoMap)
	{
		 LinkedHashMap<String, ScoreInfo> tempScoreInfoMap	= scoreInfoMap;

		double baseCount	= 0;
		double baseCount2	= 0;
		int base		= 0;
		int first		= 0;

		List<QueryKeywordInfo> keywordInfos	= queryInfo.getKeyWordInfos();
		List<String> keyLists	= new ArrayList<>();
		for(QueryKeywordInfo keywordInfo : keywordInfos) {
			String word	= keywordInfo.getWord();
			keyLists.add(word);
		}

		// キーワードの長さでソート(降順)
		Collections.sort(keyLists, new Comparator<String>(){
			public int compare(String x, String y){
				return y.length() - x.length();
			}
		});

		StringBuilder sb = new StringBuilder();
		for(String word : keyLists) {
			 sb.append(word).append("|");
		}
		String	keys	=  sb.toString();
		if (keys.length() > 0)
			keys	= keys.substring(0,keys.length()-1);

		for(String lid : lidList) {
			LineInfo	lineInfo	= lineInfoMap.get(lid);
			ScoreInfo	scoreInfo	= tempScoreInfoMap.get(lid);
			List<Integer> keyCountL0	= lineInfo.getKeycountL0();
			for(int i = 0; i < keyCountL0.size(); i++) {
				if(keyCountL0.get(i) > 0) {
					if(keywordInfos != null && keywordInfos.size() > 0 && keywordInfos.size() > i) {
						if(keywordInfos.contains(i)) {
							double	weight	= keywordInfos.get(i).getWeight(); 
							baseCount	+= weight;
						}
					}
				}
			}
			if(param.getNeType().equals("0")) {
				// 「」（）
				baseCount	+= param.getKakko();
			}

			String tmpLineLo	= lineInfo.getLineL0();
//System.out.println("lid="+lid+" " +tmpLineLo);
keys = keys.replaceAll("\\?","\\\\?");
			Pattern p = Pattern.compile(keys);
			Matcher m = p.matcher(tmpLineLo);
			m.reset();
			while(m.find()) {
				String secondStr	= m.group();
				first	= (tmpLineLo.substring(0, m.start()).length());
				int second	= first + (secondStr.length());
				String thirdStr	= tmpLineLo.substring(m.end());
				String kakuTmp	= "";
				TreeMap<Integer,Double> tempScoreCkey	= scoreInfo.getCkeyword();
				for(int j = base; j < first; j++) {
					tempScoreCkey.put(j,baseCount);
				}
				for(int j = first; j < second; j++ ) {
					if(baseCount-1 < 0) {
						tempScoreCkey.put(j,0.0);
					}
					else {
						tempScoreCkey.put(j,baseCount - 1);
					}
				}
				scoreInfo.setCkeyword(tempScoreCkey);
				tempScoreInfoMap.put(lid,scoreInfo);

				List<QueryKeywordInfo>	QueryKeywordInfo	= queryInfo.getKeyWordInfos();
				for(QueryKeywordInfo keywordInfo : QueryKeywordInfo) {
					String word	= keywordInfo.getWord();
					String kaku	= keywordInfo.getKaku();
					double weight	= keywordInfo.getWeight();
					if(kaku == null || kaku.length() <= 0) {
						continue;
					}
					if(!word.equals(secondStr)) {
						continue;
					}
					kakuTmp	= kaku2hyoso(kaku);
					if(regexCache.find("^"+kakuTmp, thirdStr)) {
						baseCount2	+= param.getKaku() * weight;
					}
				}
				base = m.start()+1;
			}
//			tmpLineLo	= tmpLineLo.substring(0, tmpLineLo.length()-1);
			int second	= tmpLineLo.length();
			TreeMap<Integer,Double> tempScoreCkey	= scoreInfo.getCkeyword();
			if(tempScoreCkey == null) {
				tempScoreCkey	= new TreeMap<Integer,Double>();
			}
			for(int j = base; j < second; j++ ) {
				tempScoreCkey.put(j,baseCount);
			}
			scoreInfo.setCkeyword(tempScoreCkey);
			tempScoreInfoMap.put(lid,scoreInfo);
			if(baseCount2 != 0) {
				for(int j = 0; j < tempScoreCkey.size(); j++ ) {
					double score	= tempScoreCkey.get(j);
					tempScoreCkey.put(j, score+baseCount2);
				}
				scoreInfo.setCkeyword(tempScoreCkey);
				tempScoreInfoMap.put(lid,scoreInfo);
			}
		}
		return tempScoreInfoMap;
	}

	/**
	 * 構文構造の一致（最大スコア）
	 *
	 * @param param パラメータクラス
	 * @param queryInfo クエリ情報
	 * @param lidList 文IDのリスト
	 * @param lineInfoMap 文の情報のMap
	 * @param scoreInfoMap スコア情報のMap
	 * @return スコア情報のMap
	 */
	public static LinkedHashMap<String, ScoreInfo> calScrCConst(AnswerParam param, QueryInfo queryInfo,
													List<String> lidList,
													LinkedHashMap<String, LineInfo> lineInfoMap,
													LinkedHashMap<String, ScoreInfo> scoreInfoMap,
													boolean depmatchSumFlag, boolean depmatchDiffFlag)
	{
		LinkedHashMap<String, ScoreInfo> tempScoreInfoMap	= scoreInfoMap;
		List<QueryKeywordInfo> keywordInfos	= queryInfo.getKeyWordInfos();

		for(String lid : lidList) {
			LineInfo	lineInfo	= lineInfoMap.get(lid);
			ScoreInfo	scoreInfo	= tempScoreInfoMap.get(lid);
			scoreInfo.setCconst(0);
			List<Integer> keyCountL0	= lineInfo.getKeycountL0();
			for(int j = 0; j < keyCountL0.size(); j++) {
				if(keyCountL0.get(j) == 0) {
					continue;
				}
				int tmp	= 0;
				// 構文の距離の一致
				double relDown = keywordInfos.get(j).getRel2Down();
				double relUp = keywordInfos.get(j).getRelUp();
				double rel2Down = keywordInfos.get(j).getRel2Down();
				double rel2Up = keywordInfos.get(j).getRel2Up();
				double s = matchConst(relUp,relDown,relUp,relDown,depmatchSumFlag,depmatchDiffFlag);
				if(rel2Up != 0 || rel2Down != 0) {
					s	= compNum(s,matchConst(rel2Up,rel2Down,rel2Up,rel2Down,depmatchSumFlag,depmatchDiffFlag));
				}
				tmp	+= s * param.getRelation();
				// キーワードの格の一致
				String kaku	= keywordInfos.get(j).getKaku();
				if(kaku != null && kaku.length() > 0) {
					if(kaku.matches("^(カラ|マデ)格")) {
						tmp	+= param.getKaku();
					}
					else {
						tmp	+= param.getKaku() * param.getKaramade();
					}
				}
				scoreInfo.setCconst(scoreInfo.getCconst() + (tmp * keywordInfos.get(j).getWeight()));
			}

			// 疑問詞（と解候補）の格の一致
			String wKaku	= keywordInfos.get(0).getWkaku();
			if(wKaku != null && wKaku.length() > 0) {
				if(wKaku.matches("^(カラ|マデ)格")) {
					scoreInfo.setCconst(scoreInfo.getCconst() + param.getWkaku());
				}
				else {
					scoreInfo.setCconst(scoreInfo.getCconst() + (param.getWkaku() * param.getKaramade()));
				}
			}
			else if(regexCache.find("^(date|time)", param.getNeType())) {
				scoreInfo.setCconst(scoreInfo.getCconst() + (param.getWkaku() * 0.8));
			}
			tempScoreInfoMap.put(lid, scoreInfo);
		}
		return tempScoreInfoMap;
	}

	/**
	 * 質問文タイプに対応する数量表現が含まれるか.
	 * 
	 * @param param パラメータクラス
	 * @param queryInfo クエリ情報
	 * @param lidList 文IDのリスト
	 * @param lineInfoMap 文の情報のMap
	 * @param scoreInfoMap スコア情報のMap
	 * @return スコア情報のMap
	 */
	public static LinkedHashMap<String, ScoreInfo> calScrCNum(AnswerParam param, QueryInfo queryInfo,
												List<String> lidList,
												LinkedHashMap<String, LineInfo> lineInfoMap,
												LinkedHashMap<String, ScoreInfo> scoreInfoMap)
	{
		LinkedHashMap<String, ScoreInfo> tempScoreInfoMap	= scoreInfoMap;

		if(param.getNeMode() == 1) {
			for(String lid : lidList) {
				LineInfo	lineInfo	= lineInfoMap.get(lid);
				String line	= lineInfo.getLineL0();
				int len	= line.length();
				ScoreInfo	scoreInfo	= tempScoreInfoMap.get(lid);
				TreeMap<Integer,Double> tempScoreCne	= scoreInfo.getCne();
				for(int j = 0 ; j < len; j++) {
					tempScoreCne.put(j,1.0);	// すべて１（MAXスコア）
				}
				scoreInfo.setCne(tempScoreCne);
				tempScoreInfoMap.put(lid,scoreInfo);
			}
		}
		else if(param.getNeType() == null || param.getNeType().equals("") || param.getNeType().equals("0")) {
			for(String lid : lidList) {
				LineInfo	lineInfo	= lineInfoMap.get(lid);
				String line	= lineInfo.getLine();
				int len		= line.length();
				ScoreInfo	scoreInfo	= tempScoreInfoMap.get(lid);
				TreeMap<Integer,Double> tempScoreCne	= scoreInfo.getCne();
				for(int j = 0 ; j < len; j++) {
					tempScoreCne.put(j,0.0);
				}
				scoreInfo.setCne(tempScoreCne);
				tempScoreInfoMap.put(lid,scoreInfo);
			}
		}
		else {	// 数量表現の場合
			List<QueryKeywordInfo>	QueryKeywordInfo	= queryInfo.getKeyWordInfos();
			StringBuffer buf = new StringBuffer();
			for(QueryKeywordInfo keywordInfo : QueryKeywordInfo) {
				String word	= keywordInfo.getWord();
				buf.append(word).append("|");
			}
			String	keys	= buf.toString();
			if (keys.length() > 0) {
				keys	= keys.substring(0,keys.length()-1);
				keys = keys.replaceAll("\\?","\\\\?");
			}
			for(String lid : lidList) {
				LineInfo	lineInfo	= lineInfoMap.get(lid);
				String line	= lineInfo.getLine();
				int len		= line.length();
				ScoreInfo	scoreInfo	= tempScoreInfoMap.get(lid);
				TreeMap<Integer,Double> tempScoreCne	= new TreeMap<Integer,Double>();
				// スコアを初期化
				for(int j = 0 ; j < len; j++) {
					tempScoreCne.put(j,0.0);
				}
				scoreInfo.setCne(tempScoreCne);
				tempScoreInfoMap.put(lid,scoreInfo);
				if(queryInfo.getPattern2() != null) {
					Pattern p = Pattern.compile(queryInfo.getPattern2());
					Matcher m = p.matcher(line);
					m.reset();
					while(m.find()) {
						String num	= m.group();
						int offset	= (line.substring(0, m.start()).length());
						if(num.matches(keys)) {
							continue;
						}
						Pattern p2 = Pattern.compile("[0-9〇一二三四五六七八九十零百千万億兆]+");
						Matcher m2 = p2.matcher(num);
						m2.reset();
						while(m2.find()) {
							int first  = offset + (num.substring(0, m2.start()).length());
							int second = first + m2.group().length();
							for(int j = first ; j < second ; j++ ) {
								// 数量表現の数字部分にスコア付与
								tempScoreCne.put(j,0.5);
							}
							scoreInfo.setCne(tempScoreCne);
							tempScoreInfoMap.put(lid,scoreInfo);
						}
					}
				}
				if(queryInfo.getPattern1() != null) {
					Pattern p = Pattern.compile(queryInfo.getPattern1());
					Matcher m = p.matcher(line);
					m.reset();
					while(m.find()) {
						String num	= m.group();
						int offset	= (line.substring(0, m.start()).length());
						if(num.matches(keys)) {
							continue;
						}
						Pattern p2 = Pattern.compile("[０-９〇一二三四五六七八九十零百千万億兆]+");
						Matcher m2 = p2.matcher(num);
						m2.reset();
						while(m2.find()) {
							int first  = offset + (num.substring(0, m2.start()).length());
							int second = first + m2.group().length();
							for(int j = first ; j < second ; j++ ) {
								// 数量表現の数字部分にスコア付与
								tempScoreCne.put(j,1.0);
							}
							scoreInfo.setCne(tempScoreCne);
							tempScoreInfoMap.put(lid,scoreInfo);
						}
					}
				}
			}
		}
		return tempScoreInfoMap;
	}

	/**
	 * 2gramの実スコア（初期スコアの分配）.
	 * 
	 * @param param パラメータクラス
	 * @param lid 文ID
	 * @param lineInfoMap 文の情報のMap
	 * @param scoreInfoMap スコア情報のMap
	 * @return スコア情報のMap
	 */
	public static LinkedHashMap<String, ScoreInfo> calScrR2gram(AnswerParam param,
														String lid,
														LinkedHashMap<String, LineInfo> lineInfoMap,
														LinkedHashMap<String, ScoreInfo> scoreInfoMap)
	{
		LinkedHashMap<String, ScoreInfo> tempScoreInfoMap	= scoreInfoMap;

		LineInfo lineInfo	= lineInfoMap.get(lid);
		ScoreInfo scoreInfo	= tempScoreInfoMap.get(lid);

		List<Morpheme> morphemeList	= lineInfo.getMorphemeList();
		TreeMap<Integer,Double> c2gramMap	= scoreInfo.getC2gram();
		TreeMap<Integer,Double> r2gramMap	= scoreInfo.getR2gram();
		TreeMap<Integer,Double> tempC2gramMap	= c2gramMap;
		
		int i	= 0;
		int pos = 0;
		for(Morpheme morpheme : morphemeList) {
			String hyoso	= morpheme.getSurfaceForm();
			int temp	= hyoso.length();
			for(int k = 0; k < temp; k++) {
				double score1	= r2gramMap != null && r2gramMap.get(i) != null ? r2gramMap.get(i) : 0.0;
				double score2 = tempC2gramMap != null && tempC2gramMap.get(pos) != null ? tempC2gramMap.get(pos) : 0.0;
				r2gramMap.put(i, score1 + score2);
//				tempC2gramMap.remove(0);
				++pos;
			}
			double score	= r2gramMap != null && r2gramMap.get(i) != null ? r2gramMap.get(i) : 0.0;
			if(score > 0 && temp > 0) {
				r2gramMap.put(i, score / (double)temp);
			}
			else {
				r2gramMap.put(i, 0.0);				
			}
			i++;
		}
		scoreInfo.setR2gram(r2gramMap);
		tempScoreInfoMap.put(lid, scoreInfo);

		return tempScoreInfoMap;
	}

	/**
	 * NE,numの推定スコア（初期スコアの分配）
	 * 
	 * @param param パラメータクラス
	 * @param lid 文ID
	 * @param lineInfoMap 文の情報のMap
	 * @param scoreInfoMap スコア情報のMap
	 * @return スコア情報のMap
	 */
	public static LinkedHashMap<String, ScoreInfo> calScrPNum(AnswerParam param, String lid,
																	LinkedHashMap<String, LineInfo> lineInfoMap,
																	LinkedHashMap<String, ScoreInfo> scoreInfoMap)
	{
		LinkedHashMap<String, ScoreInfo> tempScoreInfoMap	= scoreInfoMap;

		LineInfo lineInfo	= lineInfoMap.get(lid);
		ScoreInfo scoreInfo	= tempScoreInfoMap.get(lid);

		List<Morpheme> morphemeList	= lineInfo.getMorphemeList();
		TreeMap<Integer,Double> pNeMap	= scoreInfo.getPne();
		TreeMap<Integer,Double> cNeMap	= scoreInfo.getCne();
		TreeMap<Integer,Double> tempCNeMap	= cNeMap;

		// 形態素スコア ＝ 文字スコアの和を形態素長で割ったもの
		int i	= 0;
		int pos = 0;
		for(Morpheme morpheme : morphemeList) {
			pNeMap.put(i, 0.0);
			String hyoso	= morpheme.getSurfaceForm();
			int temp	= hyoso.length(); // 形態素の語長
			for(int k = 0; k < temp; k++) {
				if(pNeMap.get(i) == null || pNeMap.get(i) == 0) {
					double score = tempCNeMap != null && tempCNeMap.get(pos) != null ? tempCNeMap.get(pos) : 0.0;
					pNeMap.put(i, score);
				}
				//tempCNeMap.remove(0);
				++pos;
			}
			i++;
		}
		scoreInfo.setPne(pNeMap);
		tempScoreInfoMap.put(lid, scoreInfo);

		return tempScoreInfoMap;
	}

	/**
	 * キーワードの実スコア算出.
	 * 
	 * @param param パラメータクラス
	 * @param queryInfo クエリ情報
	 * @param lid 文ID
	 * @param lineInfoMap 文の情報のMap
	 * @param scoreInfoMap スコア情報のMap
	 * @param offset 形態素のインデックス
	 * 
	 * @return スコア情報のMap
	 */
	public static LinkedHashMap<String, ScoreInfo> calScrRKey(AnswerParam param, QueryInfo queryInfo,
													String lid,
													LinkedHashMap<String, LineInfo> lineInfoMap,
													LinkedHashMap<String, ScoreInfo> scoreInfoMap,
													int offset)
	{
		LinkedHashMap<String, ScoreInfo> tempScoreInfoMap	= scoreInfoMap;

		List<Double> hitList	= new ArrayList<>();

		LineInfo lineInfo	= lineInfoMap.get(lid);
		ScoreInfo tempScoreInfo	= tempScoreInfoMap.get(lid);

		List<QueryKeywordInfo> qKeywordInfo	= queryInfo.getKeyWordInfos();

		List<Morpheme> morphemeList	= null;
		List<String> hyosoList	= null;
		List<KeyWord> keyWordList	=  null;
		TreeMap<Integer,Double> scoreRKeyMap	= null;

		// 複数文照合
		if(lineInfo.getMulticonst()) {
			morphemeList	= lineInfo.getMorphemeTotalList();
			hyosoList	= lineInfo.getMorphemeHyosoTotalList();
			keyWordList	=  lineInfo.getKeyWordTotalList();
			scoreRKeyMap	= tempScoreInfo.getRkeywordTotal();
		}
		else {
			morphemeList	= lineInfo.getMorphemeList();
			hyosoList	= lineInfo.getMorphemeHyosoList();
			keyWordList	=  lineInfo.getKeyWordList();
			scoreRKeyMap	= tempScoreInfo.getRkeyword();			
		}

		if(hyosoList != null) {
			for(int i = offset; i < hyosoList.size(); i++) {
				if(hyosoList.get(i) == null) {
					break;
				}
				// 初期化
				Morpheme morpheme	= morphemeList.get(i);
				String surfaceForm	= morpheme.getSurfaceForm();
				String pos	= morpheme.getPos();
				String detailedPos	= morpheme.getDetailedPos();
				//String str	= pos + " * " + detailedPos;
				StringBuilder buf = new StringBuilder();
				buf.append(surfaceForm).append(" ");
				buf.append("*").append(" ");
				buf.append("*").append(" ");
				buf.append(pos).append(" ");
				buf.append("*").append(" ");
				buf.append(detailedPos).append(" ");
				String str	= buf.toString();
				String sPart	= param.getSPart();
				String sWord	= param.getSWord();
				if(regexCache.find("^((\\S+\\s){3}("+sPart+")|("+sWord+"))", str)) {
					scoreRKeyMap.put(i, -1.0);					
				}
				else {
					scoreRKeyMap.put(i, 0.0);
				}
			}
		}
		// キーワードの一致
		for(int j = 0; j < qKeywordInfo.size(); j++) {
			String	qWord	= qKeywordInfo.get(j).getWord();
			String	qPart	= qKeywordInfo.get(j).getPart();
			String	qKaku	= qKeywordInfo.get(j).getKaku();
			double	qWeight	= qKeywordInfo.get(j).getWeight();

			// 初期化
			hitList.add(0.0);
			if(keyWordList != null) {
				for(int i = 0; i < keyWordList.size(); i++) {
					String kword	= keyWordList.get(i).getWord();
					String kpart	= keyWordList.get(i).getPart();
					int klocation	= keyWordList.get(i).getLocation();
					int klocationE	= keyWordList.get(i).getLocationE();
					if(kword.equals(qWord) && kpart.equals(qPart)) {
						// 質問文キーワードと検索文キーワードが一致したら
						for(int k = klocation; k <= klocationE; k++) {
							scoreRKeyMap.put(k, -2.0);// キーワード部分のスコアを下げる
						}
						if(hitList.get(j) == 0) {
							hitList.set(j, (double)qWeight);
						}
						// キーワード ＋ 助詞 の一致      < 2
	
						if(qKaku != null && hitList.get(j) == qWeight
								&& (hyosoList != null && hyosoList.get(klocationE+1) != null)
								&& (hyosoList.get(klocationE+1).equals(kaku2hyoso(qKaku)))) {
							double hit	= hitList.get(j);
	
							hitList.set(j, hit + param.getKaku() * qWeight);
						}
					}
				}
			}
		}

		double sum	= 0;
		double gain	= 0;
		String kakko	= "";
		boolean keyflag	= false;

		for(double hit : hitList) {
			if(hit != 0) {
				sum	= hit;
			}
		}

		if(hyosoList != null) {
			for(int i = offset; i < hyosoList.size(); i++) {
				double temp	= 0;
				if(param.getNeType().equals("0")) {
					//	括弧の中に加点する
					String hyoso = morphemeList.get(i).getSurfaceForm();
					if(kakko.equals("") && regexCache.find("^[「（『“]",hyoso)) {
						boolean flag = false;
						if(keyflag) {
							// キーワード直後
							if(hyoso.equals("「")) {
								gain  = 1.0;
								kakko = "」";
							}
							else if(hyoso.equals("（")) {
								gain  = 0.9;
								kakko = "）";
							}
							else if(hyoso.equals("『")) {
								gain  = 1;
								kakko = "』";
							}
							else if(hyoso.equals("“")) {
								gain  = 0.3;
								kakko = "”";
							}
						}
						else {
							// その他
							if(hyoso.equals("「")) {
								gain  = 0.5;
								kakko = "」";
							}
							else if(hyoso.equals("（")) {
								gain  = 0;
								kakko = "）";
							}
							else if(hyoso.equals("『")) {
								gain  = 0.5;
								kakko = "』";
							}
							else if(hyoso.equals("“")) {
								gain  = 0.1;
								kakko = "”";
							}
						}
	
						// 「」内の形態素が９以上のときは加点しない（引用、話し言葉）
						for(int j = i + 1 ; j <= i + 10 ; j++) {
							if(j >= morphemeList.size()) {
								break;
							}
							String surfaceForm2 = morphemeList.get(j).getSurfaceForm();
							if(regexCache.find("^"+kakko, surfaceForm2)) {
								flag = true;
								break;
							}
						}
						if(!flag) {
							gain  = 0;
							kakko = "";
						}
					}
					else if(!kakko.equals("")) {	// 括弧内
						if(regexCache.find("^"+kakko, hyoso)) {
							kakko = "";
							gain  = 0.0;
						}
						else if(scoreRKeyMap.get(i) >= 0 ) {
							// 「」（）『』“”の中の解候補に加点
							temp	= param.getKakko() * gain;
						}
					}
	
					if(scoreRKeyMap.get(i) == -2 ) {
						keyflag = true;	// キーワードのとき
					}
					else {
						keyflag = false;
					}
				}
	
				if(scoreRKeyMap.get(i) >= 0 ) {
					scoreRKeyMap.put(i, sum + temp);
				}
			}
		}

		if(lineInfo.getMulticonst()) {
			tempScoreInfo.setRkeywordTotal(scoreRKeyMap);				
		}
		else {
			tempScoreInfo.setRkeyword(scoreRKeyMap);				
		}
		tempScoreInfoMap.put(lid,tempScoreInfo);

		return tempScoreInfoMap;
	}

	//	#構文の推定スコアを導出
	//	#&calscr_pconst(\%PARA,\@{$$query{keyword}},\@{$$line{key_total}{$lid}},
	//	#	       \@{ $$line{k_data_total}{$lid} },\@{ $$line{k_data}{$lid} },
	//	#              \@{$$score{r_keyword}{$lid}},\@{ $$score{p_const}{$lid}},
	//	#              \@p_bnst_total);
	//	#第一引数 パラメータのハッシュ
	//	#第二引数 キーワード配列のアドレス
	//	#第三引数 検索結果文のキーワード配列のアドレス
	//	#第四引数 形態素解析結果配列のアドレス（複数文結合）
	//	#第五引数 形態素解析結果配列のアドレス（単文）
	//	#第六引数 キーワード実スコア配列のアドレス
	//	#第七引数 スコアを代入する配列のアドレス
	//	#第八引数 文節区切り（推定）された形態素解析結果配列
	//	#戻り値 なし
	//	###############################################
	//	sub calscr_pconst(\%\@\@\@\@\@\@\@\@){
	/**
	 * 構文の推定スコアを導出.
	 * 
	 * @param param パラメータクラス
	 * @param queryInfo クエリ情報
	 * @param lid 文ID
	 * @param lineInfoMap 文の情報のMap
	 * @param scoreInfoMap スコア情報のMap
	 * @param pBustList 文節区切り（推定）された形態素解析結果リスト
	 * @param depmatchSumFlag 係り受け構造照合の「和」のスコアを使うかのフラグ
	 * @param depmatchDiffFlag 係り受け構造照合の「差」のスコアを使うかのフラグ
	 *
	 * @return スコア情報のMap
	 */
	public static LinkedHashMap<String, ScoreInfo> calScrPConst(AnswerParam param, QueryInfo queryInfo,
														String lid,
														LinkedHashMap<String, LineInfo> lineInfoMap,
														LinkedHashMap<String, ScoreInfo> scoreInfoMap,
														List<String> pBustList, boolean depmatchSumFlag, boolean depmatchDiffFlag)
	{
		LinkedHashMap<String, ScoreInfo> tempScoreInfoMap	= scoreInfoMap;
		ScoreInfo tempScoreInfo	= tempScoreInfoMap.get(lid);

		List<QueryKeywordInfo> keywordInfos	= queryInfo.getKeyWordInfos();

		LineInfo lineInfo	= lineInfoMap.get(lid);

		List<KeyWord> keyWordList		= null;
		List<Morpheme> morphemeList	= null;
		List<Morpheme> morphemeTotalList	= null;

		// 複数文照合
		if(lineInfo.getMulticonst()) {
			keyWordList		= lineInfo.getKeyWordTotalList();
			morphemeList			= lineInfo.getMorphemeList();
			morphemeTotalList	= lineInfo.getMorphemeTotalList();
		}
		else {
			keyWordList		= lineInfo.getKeyWordList();
			morphemeList			= lineInfo.getMorphemeList();
			morphemeTotalList	= morphemeList;			
		}

		// 検索文中に出現する質問文キーワードを検出
		List<KeyWord> coKeyList = new ArrayList<>();
		HashMap<String,Integer> flagMap	= new HashMap<String,Integer>();
		for(QueryKeywordInfo keywordInfo : keywordInfos) {
			String qword	= keywordInfo.getWord();
			String qpart	= keywordInfo.getPart();
			String temp	= qword+","+qpart;

			if(flagMap.containsKey(temp) && flagMap.get(temp) == 1) {
				continue;
			}
			flagMap.put(temp, 1);

			if(keyWordList != null) {
				for(int i = 0; i < keyWordList.size(); i++) {
					String kword	= keyWordList.get(i).getWord();
					String kpart	= keyWordList.get(i).getPart();
					if(qword.equals(kword) && qpart.equals(kpart)) {
						coKeyList.add(keyWordList.get(i));
					}
				}
			}
		}

		// 前の文の形態素数
		int offset = (morphemeTotalList != null ? morphemeTotalList.size()  - morphemeList.size() : 0);

		TreeMap<Integer,Double> scorePConstMap	= tempScoreInfo.getPconst();
		TreeMap<Integer,Double> scoreRKeyMap		= tempScoreInfo.getRkeyword();

		for(int i = 0; i < morphemeList.size(); i++) {
			int j = i + offset;
			scorePConstMap.put(i,0.0);
			if(scoreRKeyMap != null && scoreRKeyMap.get(i) != null && scoreRKeyMap.get(i) <= 0) {
				continue;
			}

			int bj = numBnst(pBustList, j);
			double score = calcPrel(param, j, bj, pBustList, keywordInfos, coKeyList, "rel", depmatchSumFlag, depmatchDiffFlag);
			if(keywordInfos.get(0).getRel2Up() != 0 || keywordInfos.get(0).getRel2Down() != 0) {
				score = compNum(score,calcPrel(param, j, bj, pBustList, keywordInfos, coKeyList, "rel2", depmatchSumFlag, depmatchDiffFlag));
			}
			double pConstScore	= scorePConstMap.get(i);
			pConstScore = pConstScore + score;
			pConstScore	= pConstScore + compKaku(keywordInfos.get(0).getWkaku(),(pBustList.size() > 0 ? pBustList.get(bj):null),param,"解候補") * param.getWkaku();
			scorePConstMap.put(i,score);
		}
		tempScoreInfo.setPconst(scorePConstMap);
		tempScoreInfoMap.put(lid,tempScoreInfo);

		return tempScoreInfoMap;
	}

	/**
	 * 質問タイプによるスコア推定.
	 *
	 * @param param パラメータクラス
	 * @param queryInfo クエリ情報
	 * @param lid 文ID
	 * @param lineInfoMap 文の情報のMap
	 * @param scoreInfoMap スコア情報のMap
	 *
	 * @return スコア情報のMap
	 */
	public static LinkedHashMap<String, ScoreInfo> calScrPNe(AnswerParam param, String lid,
																					LinkedHashMap<String, LineInfo> lineInfoMap,
																					LinkedHashMap<String, ScoreInfo> scoreInfoMap)
	{
		LinkedHashMap<String, ScoreInfo> tempScoreInfoMap	= scoreInfoMap;
		ScoreInfo tempScoreInfo	= tempScoreInfoMap.get(lid);

		LineInfo lineInfo	= lineInfoMap.get(lid);
		List<Morpheme> morphemeList	= lineInfo.getMorphemeList();

		TreeMap<Integer,Double> pNeMap	= tempScoreInfo.getPne();
		if(param.getNeType().equals("PERSON")) {
			for(int i = 0 ; i < morphemeList.size(); i++) {
				String pos	= morphemeList.get(i).getPos();
				String detailedPos	= morphemeList.get(i).getDetailedPos();
				if(!(pos.equals("未定義語") || detailedPos.matches("^(人名|記号|カタカナ)$"))) {
					pNeMap.put(i, 0.0);
				}
			}
			tempScoreInfo.setPne(pNeMap);
		}
		else if(param.getNeType().equals("LOCATION")) {
			for(int i = 0 ; i < morphemeList.size(); i++) {
				String pos	= morphemeList.get(i).getPos();
				String detailedPos	= morphemeList.get(i).getDetailedPos();
				if(!(pos.equals("未定義語") || detailedPos.matches("^(地名|記号|カタカナ)$"))) {
					pNeMap.put(i, 0.0);
				}
			}
			tempScoreInfo.setPne(pNeMap);
		}
		else if(param.getNeType().equals("ORGANIZATION")) {
			for(int i = 0 ; i < morphemeList.size(); i++) {
				String pos	= morphemeList.get(i).getPos();
				String detailedPos	= morphemeList.get(i).getDetailedPos();
				if(!(pos.equals("未定義語") || detailedPos.matches("^(組織名|記号|カタカナ)$"))) {
					pNeMap.put(i, 0.0);
				}
			}
			tempScoreInfo.setPne(pNeMap);
		}
		else if(param.getNeType().equals("LOCATION|ORGANIZATION")) {
			for(int i = 0 ; i < morphemeList.size(); i++) {
				String pos	= morphemeList.get(i).getPos();
				String detailedPos	= morphemeList.get(i).getDetailedPos();
				if(!(pos.equals("未定義語") || detailedPos.matches("^((組織|地)名|記号|カタカナ)$"))) {
					pNeMap.put(i, 0.0);
				}
			}
			tempScoreInfo.setPne(pNeMap);
		}

		if(param.getNeMode() == 1) {
			int monoword = 0;
			for(int i = 0 ; i < morphemeList.size(); i++) {
				String hyoso	= morphemeList.get(i).getSurfaceForm();
				String pos		= morphemeList.get(i).getPos();
				String detailedPos	= morphemeList.get(i).getDetailedPos();
				if(regexCache.find("^[^ぁ-んァ-ヶ]", hyoso) && regexCache.find("副詞|接頭辞|未定義語|名詞", pos) && regexCache.find("[^数]", detailedPos)) {
					if(monoword != 0) {
						monoword	= 2;
						if(i > 0 && pNeMap.get(i-1) != null && pNeMap.get(i-1) < 1) {
							pNeMap.put(i-1, 1.0);
						}
					}
					else {
						monoword = 1;
					}
				}
				else {
					if(monoword == 2) {
						if(i > 0 && pNeMap.get(i-1) != null && pNeMap.get(i-1) < 1) {
							pNeMap.put(i-1, 1.0);
						}
					}
					monoword = 0;
				}
			}
			tempScoreInfo.setPne(pNeMap);
		}

		tempScoreInfoMap.put(lid,tempScoreInfo);

		return tempScoreInfoMap;
	}

	/**
	 * 解候補に対して、係り受けのマッチングによりスコア付け.
	 * 
	 * @param param パラメータクラス
	 * @param queryInfo クエリ情報
	 * @param lid 文ID
	 * @param lineInfoMap 文の情報のMap
	 * @param scoreInfoMap スコア情報のMap
	 * @param knpHashMap KNP結果の係り受け情報のHash
	 * @param bnstNum 複数文照合のとき、直前までの文の文節数（単文のときは0）
	 * @param distOnlyFlag 距離のスコアのみにするかのフラグ
	 * @param depmatchSumFlag 係り受け構造照合の「和」のスコアを使うかのフラグ
	 * @param depmatchDiffFlag 係り受け構造照合の「差」のスコアを使うかのフラグ
	 * 
	 * @return スコア情報のMap
	 */
	public static LinkedHashMap<String, ScoreInfo> calScrRConst(AnswerParam param, QueryInfo queryInfo,
														String lid,
														LinkedHashMap<String, LineInfo> lineInfoMap,
														LinkedHashMap<String, ScoreInfo> scoreInfoMap,
														KnpBnstHash knpHash/*HashMap<Integer,KnpBnstTree> knpHashMap*/,
														int bnstNum, boolean distOnlyFlag,boolean depmatchSumFlag, boolean depmatchDiffFlag)
	{
		LinkedHashMap<String, ScoreInfo> tempScoreInfoMap	= scoreInfoMap;

		List<QueryKeywordInfo> keywordInfos	= queryInfo.getKeyWordInfos();

		LineInfo lineInfo	= lineInfoMap.get(lid);
		ScoreInfo tempScoreInfo	= tempScoreInfoMap.get(lid);
		TreeMap<Integer,Double> scoreRConstMap	= tempScoreInfo.getRconst();
		TreeMap<Integer,Double> scoreRKeyMap		= tempScoreInfo.getRkeyword();

		List<KeyWord> keyWordList		= null;
		Map<KnpSegment,List<Morpheme>> knpInfoMap	= null;
		List<KyotoDependency> kyotoDependencyList	= null;
		List<KnpSegment> knpSegmentList	= null;

		// 複数文照合
		if(lineInfo.getMulticonst()) {
			keyWordList		= lineInfo.getKeyWordTotalList();
			knpInfoMap	= lineInfo.getKnpInfoTotalMap();
			kyotoDependencyList	= lineInfo.getKyotoDependencyTotalList();
			knpSegmentList	= lineInfo.getKnpSegmentTotalList();
		}
		else {
			keyWordList		= lineInfo.getKeyWordList();
			knpInfoMap	= lineInfo.getKnpInfoMap();
			kyotoDependencyList	= lineInfo.getKyotoDependencyList();
			knpSegmentList	= lineInfo.getKnpSegmentList();			
		}

		// 検索文中に出現する質問文キーワードを検出
		List<QueryKeywordInfo> coKeyInfos = new ArrayList<>();
		HashMap<String,Integer> flagMap	= new HashMap<String,Integer>();
		for(QueryKeywordInfo keywordInfo : keywordInfos) {
			String qword	= keywordInfo.getWord();
			String qpart	= keywordInfo.getPart();
			String temp	= qword+","+qpart;

			if(flagMap.containsKey(temp) && flagMap.get(temp) == 1) {
				continue;
			}
			flagMap.put(temp, 1);
			if(keyWordList != null) {
				for(int i = 0; i < keyWordList.size(); i++) {
					String kword	= keyWordList.get(i).getWord();
					String kpart	= keyWordList.get(i).getPart();
					if(qword.equals(kword) && qpart.equals(kpart)) {
						coKeyInfos.add(keywordInfo);
					}
				}
			}
		}

		// 各キーワードの文節、形態素位置を記録
		for(int i = 0 ; i < coKeyInfos.size(); i++) {
			QueryKeywordInfo keywordInfo	= coKeyInfos.get(i);
			int[] numList	= TrLoc01.juman2knp(keywordInfo.getLocation(), knpInfoMap);
			keywordInfo.setBnstNo(numList[0]);
			keywordInfo.setBnstMorphemeNo(numList[1]);
			coKeyInfos.set(i, keywordInfo);
		}

		int k = -1;	// 形態素オフセット
		int num	= 0;
		for(KnpSegment key : knpInfoMap.keySet()) {
			if(num < bnstNum) {
				++num;
				continue;
			}

			List<Morpheme>	morphemeList	= knpInfoMap.get(key);
			for(int j = 0; j < morphemeList.size(); j++) {
				++k;
				scoreRConstMap.put(k, 0.0);	// 初期値
				if(scoreRKeyMap != null && !scoreRKeyMap.containsKey(k) || scoreRKeyMap.get(k) <= 0) {
					scoreRConstMap.put(k, scoreRKeyMap.get(k));
					continue;
				}

				// 構文関係の初期化
				if(coKeyInfos != null && coKeyInfos.size() > 0) {
					coKeyInfos.get(0).setWkaku(null);
					for(int h = 0 ; h < coKeyInfos.size(); h++) {
						coKeyInfos.get(h).setRelUp(0);
						coKeyInfos.get(h).setRelDown(0);
						coKeyInfos.get(h).setKaku(null);
					}
				}
//				coKeyInfos = CwqGetRelation04.getRelation(coKeyInfos, kyotoDependencyList, knpSegmentList, knpHashMap,num, j);
				coKeyInfos = CwqGetRelation04.getRelation(coKeyInfos, kyotoDependencyList, knpSegmentList, knpHash,num, j);
		
				if(coKeyInfos.get(0).getWkaku() != null) {
					double score	= compKaku(keywordInfos.get(0).getWkaku(),coKeyInfos.get(0).getWkaku(),param,"解候補");
					score *= param.getWkaku();
					scoreRConstMap.put(k, score);
				}

				int kIdx	= 0;    // = &knp2juman($i,$j,$b_data);
				if(distOnlyFlag) {
					kIdx = TrLoc01.knp2juman(num, j, knpInfoMap);
				}
				double score = calcRrel(param, keywordInfos, coKeyInfos, "rel", kIdx, distOnlyFlag, depmatchSumFlag, depmatchDiffFlag);
				if(keywordInfos.get(0).getRel2Up() >= 0 || keywordInfos.get(0).getRel2Down() >= 0) {
					score = compNum(score,calcRrel(param, keywordInfos, coKeyInfos, "rel2", kIdx, distOnlyFlag, depmatchSumFlag, depmatchDiffFlag));
				}
				scoreRConstMap.put(k, scoreRConstMap.get(k) + score);
			}
			++num;
		}
		tempScoreInfo.setRconst(scoreRConstMap);
		tempScoreInfoMap.put(lid, tempScoreInfo);

		return tempScoreInfoMap;
	}

	/**
	 * 初期スコアの導出(行スコアの導出)
	 * 
	 * @param param パラメータクラス
	 * @param queryInfo クエリ情報
	 * @param lidList 文IDのリスト
	 * @param scoreInfoMap スコア情報のMap
	 *
	 * @return スコア情報のMap
	 */
	public static LinkedHashMap<String, ScoreInfo> getScore0(AnswerParam param, QueryInfo queryInfo,
												List<String> lidList,
												LinkedHashMap<String, ScoreInfo> scoreInfoMap)
	{
		LinkedHashMap<String, ScoreInfo> tempScoreInfoMap	= scoreInfoMap;

		double max	= 0;
		double tmp	= 0;

		for(String lid : lidList) {
			ScoreInfo scoreInfo	= tempScoreInfoMap.get(lid);
			max	= 0;
			TreeMap<Integer,Double> cNeMap	= scoreInfo.getCne();
			TreeMap<Integer,Double> c2gramMap	= scoreInfo.getC2gram();
			TreeMap<Integer,Double> cKeyMap	= scoreInfo.getCkeyword();
			if(param.getMode() > 1) {
				for(Entry<Integer,Double> entry : cNeMap.entrySet()) {
					int i	= entry.getKey();
					double cConst	 = param.getCharaScoreConst();
					tmp	= (param.getCBigram() * (c2gramMap.get(i) != null ? c2gramMap.get(i) : 0))
								+ (param.getCharaScoreKeyword() * (cKeyMap.get(i) != null ? cKeyMap.get(i) : 0))
								+ cConst + (param.getCNe() * cNeMap.get(i));
					if(max < tmp) {
						max	= tmp;
					}
				}
			}
			else if(param.getMode() == 1) { // MAXのみのとき
				for(Entry<Integer,Double> entry : cNeMap.entrySet()) {
					int i	= entry.getKey();
					if(param.getNeMode() != 0 && (param.getNeType() != null && !param.getNeType().equals("") && !param.getNeType().equals("0"))) {
						tmp	= (param.getCBigram() * (c2gramMap.get(i) != null ? c2gramMap.get(i) : 0))
									+ param.getAlp0() + param.getAlp1()
									+ (param.getCNe() * cNeMap.get(i));
					}
					else {
						tmp	= (param.getCBigram() * c2gramMap.get(i))
								+ param.getAlp0() + param.getAlp1() + param.getAlp2();
					}
					if(max < tmp) {
						max	= tmp;
					}
				}
			}
			else { // 原始的探索制御
				for(Entry<Integer,Double> entry : cNeMap.entrySet()) {
					int i	= entry.getKey();
					if(param.getNeMode() != 0 && (param.getNeType() != null && !param.getNeType().equals("") && !param.getNeType().equals("0"))) {
						tmp	= (param.getCBigram() * (c2gramMap.get(i) != null ? c2gramMap.get(i) : 0))
								+ (param.getCNe() * cNeMap.get(i));
					}
					else {
						tmp	= (param.getCBigram() * c2gramMap.get(i));
					}
					if(max < tmp) {
						max	= tmp;
					}
				}
			}
			scoreInfo.setLine(max);
			tempScoreInfoMap.put(lid, scoreInfo);
		}

		return tempScoreInfoMap;
	}

	/**
	 * スコア１を算出する.
	 * 
	 * @param param パラメータクラス
	 * @param lid 文ID
	 * @param scoreInfoMap スコア情報のMap
	 *
	 * @return スコア情報のMap
	 */
	public static LinkedHashMap<String, ScoreInfo> getScore1(AnswerParam param, String lid,LinkedHashMap<String,ScoreInfo> scoreInfoMap)
	{
		LinkedHashMap<String, ScoreInfo> tempScoreInfoMap	= scoreInfoMap;
		ScoreInfo scoreInfo	= tempScoreInfoMap.get(lid);

		double max = 0;
		TreeMap<Integer,Double> r2gram		= scoreInfo.getR2gram();
		TreeMap<Integer,Double> rKeyWord	= scoreInfo.getRkeyword();
		TreeMap<Integer,Double> pNeMap	= scoreInfo.getPne();
		TreeMap<Integer,Double> score1		= scoreInfo.getStage1();
		TreeMap<Integer,Double> decided	= scoreInfo.getDecided();
		double cConst	= scoreInfo.getCconst();

		for(Entry<Integer,Double> entry : pNeMap.entrySet()) {
			int i = entry.getKey();
			if(rKeyWord != null && rKeyWord.size() > 0 && rKeyWord.get(i) != null && rKeyWord.get(i) < 0) {
				// k_score01が-1の場合:助詞かキーワードの場合
				score1.put(i, rKeyWord.get(i));
				decided.put(i, rKeyWord.get(i));
			}
			else {
				double rKeywordScore	= 0.0;
				if(rKeyWord != null &&  rKeyWord.size() > 0 && rKeyWord.get(i) != null) {
					rKeywordScore	= rKeyWord.get(i);
				}
				decided.put(i, (r2gram.get(i) * param.getRealBigram()) + (rKeywordScore * param.getRealScoreKeyword()));
				if(param.getMode() > 1) {
					score1.put(i, decided.get(i) + (cConst * param.getCharaScoreConst()) + (pNeMap.get(i) * param.getPreNe()));
				}
				else if(param.getMode() == 1 ) {	// MAXのみのとき
					if(param.getNeMode() == 0 && (param.getNeType() != null && !param.getNeType().equals("") && !param.getNeType().equals("0"))) {
						score1.put(i, decided.get(i) + param.getAlp1() + (pNeMap.get(i) * param.getPreNe()));
					}
					else {
						score1.put(i, decided.get(i) + param.getAlp1() + param.getAlp2());
					}
				}
				else {
					if(param.getNeMode() == 0 && (param.getNeType() != null && !param.getNeType().equals("") && !param.getNeType().equals("0"))) {
						score1.put(i, decided.get(i) + (pNeMap.get(i) * param.getPreNe()));
					}
					else {
						score1.put(i, decided.get(i));
					}
				}
				if(score1.get(i) > max) {
					max = score1.get(i);
				}
			}
		}
		scoreInfo.setStage1(score1);
		scoreInfo.setDecided(decided);
		scoreInfo.setLine(max);
		tempScoreInfoMap.put(lid, scoreInfo);

		return tempScoreInfoMap;
	}

	//	#スコア２の算出
	//	#&get_score2(\%PARA,\@{$$score{p_const}{$lid}},\@{ $$score{p_ne}{$lid} },
	//	#	    \@{ $$score{2}{$lid} },\@{ $$score{decided}{$lid} });
	//	#第一引数 パラメータのハッシュ
	//	#第二引数 構文の推定スコア
	//	#第三引数 NE,numの推定スコア
	//	#第四引数 スコア２の配列
	//	#第五引数 決定済みスコアの配列
	//	#戻り値   文スコア
	//	######################################################
	//	sub get_score2(\%\@$\@\@\@){
	/**
	 * スコア２を算出する.
	 * 
	 * @param param パラメータクラス
	 * @param lid 文ID
	 * @param scoreInfoMap スコア情報のMap
	 *
	 * @return スコア情報のMap
	 */
	public static LinkedHashMap<String,ScoreInfo> getScore2(AnswerParam param, String lid, LinkedHashMap<String,ScoreInfo> scoreInfoMap)
	{
		LinkedHashMap<String, ScoreInfo> tempScoreInfoMap	= scoreInfoMap;
		ScoreInfo scoreInfo	= tempScoreInfoMap.get(lid);

		double max = 0;
		TreeMap<Integer,Double> pConstMap	= scoreInfo.getPconst();
		TreeMap<Integer,Double> pNeMap	= scoreInfo.getPne();
		TreeMap<Integer,Double> score2		= scoreInfo.getStage2();
		TreeMap<Integer,Double> decided	= scoreInfo.getDecided();

		for(Entry<Integer,Double> entry : decided.entrySet()) {
			int i	= entry.getKey();
			if(decided.get(i) < 0) {
				score2.put(i, decided.get(i));
				continue;
			}
			else {
				score2.put(i, decided.get(i) + (pConstMap.get(i) * param.getPreScoreConst()) + (pNeMap.get(i) * param.getPreNe()));
			}
			if(score2.get(i) > max) {
				max = score2.get(i);
			}
		}
		scoreInfo.setStage2(score2);
		scoreInfo.setLine(max);
		tempScoreInfoMap.put(lid, scoreInfo);

		return tempScoreInfoMap;
	}

	/**
	 * スコア３を算出する.
	 * 
	 * @param param パラメータクラス
	 * @param lid 文ID
	 * @param scoreInfoMap スコア情報のMap
	 *
	 * @return スコア情報のMap
	 */
	public static LinkedHashMap<String,ScoreInfo> getScore3(AnswerParam param, String lid, LinkedHashMap<String,ScoreInfo> scoreInfoMap)
	{
		LinkedHashMap<String, ScoreInfo> tempScoreInfoMap	= scoreInfoMap;
		ScoreInfo scoreInfo	= tempScoreInfoMap.get(lid);

		double max = 0;
		TreeMap<Integer,Double> rConstMap	= scoreInfo.getRconst();
		TreeMap<Integer,Double> pNeMap	= scoreInfo.getPne();
		TreeMap<Integer,Double> score3		= scoreInfo.getStage3();
		TreeMap<Integer,Double> decided	= scoreInfo.getDecided();

		for(Entry<Integer,Double> entry : decided.entrySet()) {
			int i	= entry.getKey();
			if(decided.get(i) < 0) {
				score3.put(i, decided.get(i));
				continue;
			}
			else {
				double score	= decided.get(i);
				score	+= (rConstMap != null && rConstMap.get(i) != null ?  rConstMap.get(i) : 0 ) * param.getRealScoreConst();
				decided.put(i, score);
				if(param.getMode() > 1) {
					score3.put(i, decided.get(i) + pNeMap.get(i) * param.getPreNe());
				}
				else if(param.getMode() == 1 ) {
					if((param.getMode() == 0 ) && (param.getNeType() != null && !param.getNeType().equals("") && !param.getNeType().equals("0"))) {
						score3.put(i, decided.get(i) + (pNeMap.get(i) * param.getPreNe()));
					}
					else {
						score3.put(i, decided.get(i) + param.getAlp2());
					}
				}
				else {
					if(param.getMode() == 0 && param.getNeType() != null) {
						score3.put(i, decided.get(i) + (pNeMap.get(i) * param.getPreNe()));
					}
					else {
						score3.put(i, decided.get(i));
					}
				}
				if(max < score3.get(i)) {
					max = score3.get(i);
				}
			}
		}
		scoreInfo.setStage3(score3);
		scoreInfo.setDecided(decided);
		scoreInfo.setLine(max);
		tempScoreInfoMap.put(lid, scoreInfo);

		return tempScoreInfoMap;
	}

	/**
	 * スコア4を算出する.
	 * 
	 * @param param パラメータクラス
	 * @param lid 文ID
	 * @param scoreInfoMap スコア情報のMap
	 *
	 * @return スコア情報のMap
	 */
	public static LinkedHashMap<String,ScoreInfo> getScore4(AnswerParam param, String lid, LinkedHashMap<String,ScoreInfo> scoreInfoMap)
	{
		LinkedHashMap<String, ScoreInfo> tempScoreInfoMap	= scoreInfoMap;
		ScoreInfo scoreInfo	= tempScoreInfoMap.get(lid);

		double max = 0;
		TreeMap<Integer,Double> rNeMap	= scoreInfo.getRne();
		TreeMap<Integer,Double> score4		= scoreInfo.getStage4();
		TreeMap<Integer,Double> decided	= scoreInfo.getDecided();

		for(Entry<Integer,Double> entry : decided.entrySet()) {
			int i	= entry.getKey();
			if(decided.get(i) < 0) {
				score4.put(i, decided.get(i));
				continue;
			}
			else {
				double score	= decided.get(i);
				score	+= (rNeMap != null && rNeMap.get(i) != null ? rNeMap.get(i) : 0)  * param.getRealNe();
				decided.put(i, score);
				score4.put(i, score);
				if(max < score4.get(i)) {
					max = score4.get(i);
				}
			}
		}
		scoreInfo.setStage4(score4);
		scoreInfo.setDecided(decided);
		scoreInfo.setLine(max);
		tempScoreInfoMap.put(lid, scoreInfo);

		return tempScoreInfoMap;
	}

	/**
	 * 探索制御なしのとき、実スコア導出
	 * 
	 * @param param パラメータクラス
	 * @param lid 文ID
	 * @param scoreInfoMap スコア情報のMap
	 *
	 * @return スコア情報のMap
	 */
	public static LinkedHashMap<String,ScoreInfo> getAllScore(AnswerParam param, String lid, LinkedHashMap<String,ScoreInfo> scoreInfoMap)
	{
		LinkedHashMap<String, ScoreInfo> tempScoreInfoMap	= scoreInfoMap;
		ScoreInfo scoreInfo	= tempScoreInfoMap.get(lid);

		double max = 0;
		TreeMap<Integer,Double> rKeyMap	= scoreInfo.getRkeyword();
		TreeMap<Integer,Double> rNeMap	= scoreInfo.getRne();
		TreeMap<Integer,Double> rConstMap	= scoreInfo.getRconst();
		TreeMap<Integer,Double> r2gram		= scoreInfo.getR2gram();
		TreeMap<Integer,Double> score4		= scoreInfo.getStage4();

		for(Entry<Integer,Double> entry : rKeyMap.entrySet()) {
			int i	= entry.getKey();
			if(rKeyMap.get(i) < 0) {
				score4.put(i, rKeyMap.get(i));
			}
			else {
				if(rNeMap.get(i) == 0) {
					rNeMap.put(i,0.0);
				}
				if(rConstMap.get(i) == 0) {
					rConstMap.put(i,0.0);
				}
				score4.put(i, (r2gram.get(i) * param.getRealBigram())
							+ (rKeyMap.get(i) * param.getRealScoreKeyword())
							+ (rConstMap.get(i) * param.getRealScoreConst())
							+ (rNeMap.get(i) * param.getRealNe()));
			}
			if(max < score4.get(i)) {
				max = score4.get(i);
			}

		}
		scoreInfo.setStage4(score4);
		scoreInfo.setRne(rNeMap);
		scoreInfo.setRconst(rConstMap);
		scoreInfo.setLine(max);
		tempScoreInfoMap.put(lid, scoreInfo);

		return tempScoreInfoMap;
	}

	/**
	 * 構文の距離を照合し、スコアを付与.
	 * 
	 * @param qx 質問文キーワードの係り受け情報
	 * @param qy 質問文キーワードの係り受け情報
	 * @param cx 検索文キーワードの係り受け情報
	 * @param cy 検索文キーワードの係り受け情報
	 * @param depmatchSumFlag 係り受け構造照合の「和」のスコアを使うかのフラグ
	 * @param depmatchDiffFlag 係り受け構造照合の「差」のスコアを使うかのフラグ
	 *
	 * @return 照合スコア
	 */
	public static double matchConst(double qx, double qy, double cx, double cy, boolean depmatchSumFlag, boolean depmatchDiffFlag)
	{
		double temp = 0.0;

		// 照合度に応じてスコアを与える
		// 上式 疑問詞、解候補との距離の和に基づくスコア（最大0.5）
		//      距離の和が小さければ高いスコア、大きければ低いスコアを与える
		// 下式 疑問詞、解候補との距離の差に基づくスコア（最大0.5）
		//      距離の差が小さければ高いスコア、大きければ低いスコアを与える

		if((qx != 0 || qy != 0) && (cx != 0 || cy != 0)) {
			if(depmatchSumFlag) {
				temp += 1/(1+(Math.sqrt(Math.pow(qx,2) + Math.pow(qy,2))+Math.sqrt(Math.pow(cx,2) + Math.pow(cy,2)))/2);
			}
			if(depmatchDiffFlag) {
				temp += 1/(2+Math.sqrt(Math.pow((qx - cx),2) + Math.pow((qy - cy),2)));
			}
			if(!depmatchSumFlag || !depmatchDiffFlag) {
				temp *= 2;
			}
		}

		return temp;
	}

	/**
	 * NE同定によるスコア導出.
	 * 
	 * @param param パラメータクラス
	 * @param lid 文ID
	 * @param lineInfoMap 文の情報のMap
	 * @param scoreInfoMap スコア情報のMap
	 *
	 * @return スコア情報のMap
	 */
	public static LinkedHashMap<String, ScoreInfo> calScrRNe(AnswerParam param, String lid,
																				LinkedHashMap<String, LineInfo> lineInfoMap,
																				LinkedHashMap<String, ScoreInfo> scoreInfoMap)
	{
		LinkedHashMap<String, ScoreInfo> tempScoreInfoMap	= scoreInfoMap;
		ScoreInfo scoreInfo	= tempScoreInfoMap.get(lid);

		LineInfo lineInfo	= lineInfoMap.get(lid);
		List<String> neDataList	= lineInfo.getNeDataList();
		double monoword	= 0;
		double monoscr	= 0.7;

		if(neDataList != null && neDataList.size() > 0) {
			for(int i = 0; i < neDataList.size(); i++) {
				String neData	= neDataList.get(i);
				TreeMap<Integer,Double> rNeMap	= scoreInfo.getRne();
				// 一文字の漢字の連接
				if(regexCache.find("^[^ぁ-んァ-ヶ]\\s", neData) && regexCache.find("^(\\S+\\s){3}(副詞|接頭辞|未定義語|名詞\\s\\S+\\s[^数])", neData)) {
					if(monoword != 0) {
						monoword = 2;
						if(i > 0 && rNeMap.get(i-1) < monoscr) {
							rNeMap.put(i-1, monoscr);
						}
					}
					else {
						monoword = 1;
					}
				}
				else {
					if(monoword == 2 ) {
						if(i > 0 && rNeMap.get(i-1) < monoscr) {
							rNeMap.put(i-1, monoscr);
						}
					}
					monoword = 0;
				}

				if(regexCache.find("<"+param.getNeType()+">$",neData)) {
					rNeMap.put(i, 1.0);
				}
				else if ((param.getNeType().matches("PERSON") && regexCache.find("^(\\S+\\s){5}人名", neData))
						|| (param.getNeType().matches("LOCATION") && regexCache.find("^(\\S+\\s){5}地名",neData))
						|| (param.getNeType().matches("ORGANIZATION") && regexCache.find("^(\\S+\\s){5}組織名",neData))
						|| (param.getNeType().matches("LOCATION|ORGANIZATION") 
								&& (regexCache.find("^(\\S+\\s){5}アルファベット",neData) || regexCache.find("^\\S\\S+\\s(\\S+\\s){4}記号",neData)))) {
					rNeMap.put(i, 0.7);
				}
				else if(regexCache.find("^\\S{2,}\\s(\\S+\\s){4}カタカナ",neData)) {
					rNeMap.put(i, 0.5);
				}
				else {
					rNeMap.put(i, 0.0);
				}
				scoreInfo.setRne(rNeMap);
			}
		}
		tempScoreInfoMap.put(lid, scoreInfo);
		return tempScoreInfoMap;
	}


	//----------------------------------------------------------------
	// private methods
	//----------------------------------------------------------------

	/**
	 * 格の照合
	 *
	 * @param qkaku
	 * @param cKaku
	 * @param param
	 * @param flag
	 * @return
	 */
	private static double compKaku(String qkaku, String cKaku, AnswerParam param, String flag)
	{
		if(cKaku != null && cKaku.length() > 0) {
			cKaku	= cKaku.substring(0, cKaku.length()-1);
			if(regexCache.matches("^\\*\\s(.+)$",cKaku)) {
				cKaku = regexCache.lastMatched(1);
			}
			if(cKaku.equals("n/a")) {
				return 0;
			}
		}
		else {
			return 0;
		}

		// 疑問詞の格
		if(qkaku != null && qkaku.length() > 0) {
			if(regexCache.find("((^|\\|)"+qkaku+"|^\\*)", cKaku)) {
				if(regexCache.find("^(カラ|マデ)格", qkaku)) {
					return 1;
				}
				else {
					return param.getKaramade();
				}
			}
			else if(regexCache.find("^[未ガ]格", qkaku) && regexCache.find("(^|\\|)[未ガ]格", cKaku)) {
				return 0.4;
			}
			else if(regexCache.find("^[ノガ]格", qkaku) && regexCache.find("(^|\\|)[ノガ]格", cKaku)) {
				return 0.3;
			}
		}
		else if(flag.equals("解候補") && regexCache.find("^(date|time)", param.getNeType())) {
			if(regexCache.find("(^|\\|)ニ格", cKaku)) {
				return 0.8;
			}
			else if(regexCache.find("(^|\\|)(無格|連用|隣接)", cKaku)) {
				return 0.5;
			}
		}

		return 0;
	}

	/**
	 * 形態素番号→文節番号
	 * 
	 * @param bnstList
	 * @param n
	 * @return
	 */
	private static int numBnst(List<String> bnstList, int n)
	{
		int k = 0;
		for(int i=0; i < bnstList.size(); i++) {
			//String bnst	= bnstList.get(i);
			if(n == k++) {
				return i;
			}
		}
		return 0;
	}

	/**
	 * 大きい方の数字を返す.
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	private static double compNum(double a, double b)
	{
		if(a > b) {
			return a;
		}
		return b;
	}

	/**
	 * 解候補とキーワードの位置関係と間の文節数から近似スコアを導く
	 * 
	 * @param param
	 * @param index
	 * @param bnstNum
	 * @param bunstList
	 * @param keywordInfos
	 * @param coKeyList
	 * @param rel
	 * @param depmatchSumFlag
	 * @param depmatchDiffFlag
	 * @return
	 */
	private static double calcPrel(AnswerParam param, int index, int bnstNum,
											List<String> bunstList, List<QueryKeywordInfo> keywordInfos,
											List<KeyWord> coKeyList, String rel,
											boolean depmatchSumFlag, boolean depmatchDiffFlag)
	{
		HashMap<String,Double> sKey	= new HashMap<String,Double>();

		for(int j = 0 ; j < coKeyList.size(); j++) {
			String kword	= coKeyList.get(j).getWord();
			String kpart	= coKeyList.get(j).getPart();
			int location	= coKeyList.get(j).getLocation();
			int bj	= numBnst(bunstList,location);
			int bNum	= Math.abs(bnstNum - bj);	// キーワードと解候補の間の文節数
			int beNum	= bunstList.size() - bj;	// キーワードと文末の間の文節数

			for(int k = 0 ; k < keywordInfos.size(); k++) {
				String qword	= keywordInfos.get(k).getWord();
				String qpart	= keywordInfos.get(k).getPart();
				if(!(qword.equals(kword) && qpart.equals(kpart))) {
					continue;
				}
				double temp = 0;

				int x	= 0;
				int y	= 0;

				int qup		= (rel.equals("rel2") ? keywordInfos.get(k).getRel2Up() : keywordInfos.get(k).getRelUp());
				int qdown	= (rel.equals("rel2") ? keywordInfos.get(k).getRel2Down() : keywordInfos.get(k).getRelDown());

				if(index > location) {
					// 解候補の前にキーワード
					if(qup != 0) {
						if(qdown != 0 || qup <= bNum ) {
							// q(x,y)
							x = qup;
							y = qdown;
						}
						else {	// q(x,0) , x>文節数
							temp = compNum(matchConst(qup,qdown,bNum,0,depmatchSumFlag,depmatchDiffFlag),
											matchConst(qup,qdown,1,0,depmatchSumFlag,depmatchDiffFlag));
						}
					}
					else if(qdown == 1 ) {	// q(0,1)
						x = 1;
						y = 0;
					}
					else if(qdown == 2 ) {	// q(0,2)
						x = 1;
						y = 1;
					}
					else {	// q(0,x) , x>2
						if(qdown <= bNum ) {
							x = 1;
							y = qdown;
						}
						else {
							temp = compNum(matchConst(qup,qdown,1,bNum,depmatchSumFlag,depmatchDiffFlag),
											matchConst(qup,qdown,1,0,depmatchSumFlag,depmatchDiffFlag));
						}
					}
				}
				else {	// 解候補の後にキーワード
					if(qdown != 0) {	// q(x,y)
						if(qup != 0 || qdown <= bNum) { // q(x,y)
							x = qup;
							y = qdown;
						}
						else {	// q(x,0) , x>文節数
							temp = compNum(matchConst(qup,qdown,0,bNum,depmatchSumFlag,depmatchDiffFlag),
										matchConst(qup,qdown,0,1,depmatchSumFlag,depmatchDiffFlag));
						}
						x = qup;
						y = qdown;
					}
					else if(qup == 1 ) {	// q(1,0)
						x = 0;
						y = 1;
					}
					else if(qup == 2) {		// q(2,0)
						x = 1;
						y = 1;
					}
					else {	// q(x,0) , x>2
						if(qup <= beNum ) {
							x = qup;
							y = 1;
						}
						else {
							temp = compNum(matchConst(qup,qdown,beNum,1,depmatchSumFlag,depmatchDiffFlag),
										matchConst(qup,qdown,0,1,depmatchSumFlag,depmatchDiffFlag));
						}
					}
				}
				if(temp == 0) {
					temp = matchConst(qup,qdown,x,y,depmatchSumFlag,depmatchDiffFlag) * param.getRelation();
				}
				// キーワードの格
				String kaku	= keywordInfos.get(k).getKaku();
				if(kaku != null && kaku.length() > 0) {
					temp += compKaku(kaku,bunstList.get(bj),param,"キーワード") * param.getKaku();
				}
				temp *=  keywordInfos.get(k).getWeight();

				if(!sKey.containsKey(qword) || temp > sKey.get(qword)) {
					sKey.put(qword, temp);
				}
			}
		}

		double score = 0;
		for(double s : sKey.values()) {
			if(s > 0) {
				score += s;
			}
		}
		return score;
	}

	/**
	 * キーワードごとに係り受けと格の照合を行なう.
	 * 
	 * @param param
	 * @param keywordInfos
	 * @param coKeyInfos
	 * @param rel
	 * @param kIndex
	 * @param distOnlyFlag
	 * @param depmatchSumFlag
	 * @param depmatchDiffFlag
	 * @return
	 */
	private static double calcRrel(AnswerParam param, 
									List<QueryKeywordInfo> keywordInfos,
									List<QueryKeywordInfo> coKeyInfos, String rel, int kIndex,
									boolean distOnlyFlag, boolean depmatchSumFlag, boolean depmatchDiffFlag)
	{
		HashMap<String,Double> sKey	= new HashMap<String,Double>();

		for(int i = 0 ; i < keywordInfos.size(); i++) {
			int qup		= (rel.equals("rel2") ? keywordInfos.get(i).getRel2Up() : keywordInfos.get(i).getRelUp());
			int qdown	= (rel.equals("rel2") ? keywordInfos.get(i).getRel2Down() : keywordInfos.get(i).getRelDown());
			String qword	= keywordInfos.get(i).getWord();
			String qpart	= keywordInfos.get(i).getPart();
			String qkaku	= keywordInfos.get(i).getKaku();
			double weight	= keywordInfos.get(i).getWeight();
			if(!(qup == 0 || qdown == 0)) {
				continue;
			}
			for(int j = 0 ; j < coKeyInfos.size(); j++) {
				String kword	= coKeyInfos.get(j).getWord();
				String kpart	= coKeyInfos.get(j).getPart();
				String kkaku	= coKeyInfos.get(j).getKaku();
				if(!qword.equals(kword) || !qpart.equals(kpart)) {
					continue;
				}
				double temp	= 0.0;
				if(qkaku != null && kkaku != null) {
					temp += (compKaku(qkaku,kkaku,param,"キーワード")) * param.getKakko();
				}
				//  同一キーワードと、疑問詞または解候補との距離の一致
				if(!distOnlyFlag) {
					// 距離のみを用いるのでなければ，係受の近さを計算
					temp += (matchConst(qup,qdown,coKeyInfos.get(j).getRelUp(),coKeyInfos.get(j).getRelDown(),depmatchSumFlag,depmatchDiffFlag)) * param.getRelation();
				}
				else {
					// キーワードの近さのみ利用するとき
					temp += ( 1 / ( Math.log(Math.abs(coKeyInfos.get(j).getLocation() - kIndex) ) + 1 ) ) * param.getRelation();
				}
				temp *= weight;

				// １つの質問文キーワードが検索文中に複数含まれる場合は、
				// その中でスコアのもっとも高いもののスコアをとる。
				if(!sKey.containsKey(kword) || temp > sKey.get(kword)) {
					sKey.put(kword, temp);
				}
			}
		}

		double score = 0;
		for(double s : sKey.values()) {
			if(s > 0) {
				score += s;
			}
		}
		return score;
	}

	/**
	 * 「ノ格」等の表現を対応する助詞そのものに変換
	 * 
	 * @param kakuStr 格名の入った文字列
	 * @return 助詞
	 */
	private static String kaku2hyoso(String kakuStr)
	{
		String str = kakuStr.replaceAll("格", "");
		if(str.equals("未")) {
			str	= "は";
		}
		Pattern p = Pattern.compile("[ァ-ン]");
		Matcher m = p.matcher(str);
		str	= m.replaceAll("[ぁ-ん]");
		return str;
	}

}
