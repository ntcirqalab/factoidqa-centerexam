package org.kachako.components.factoid_qa.module;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.kachako.types.japanese.syntactic.KnpSegment;
import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

public class PknumDcNumber
{
	/**
	 * コンストラクタ
	 */
	public PknumDcNumber()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	// # KNPの出力データから数詞を含む文節の番号を返す
	//sub dc_number(@) {
	/**
	 * KNPの出力データから数詞を含む文節の番号を返す.
	 * 
	 * @param knpInfoMap
	 * @return 文節番号の配列
	 */
	public static Integer[] dcNumber(Map<KnpSegment,List<Morpheme>> knpInfoMap)
	{
		List<Integer>	intList	= new ArrayList<Integer>();

		int	num	= 0;
		for(KnpSegment key : knpInfoMap.keySet()){
			List<Morpheme>	morphemeList	= knpInfoMap.get(key);
			for(int i=0; i < morphemeList.size(); i++) {
				Morpheme	morpheme	= morphemeList.get(i);
				String	detailedPos	= morpheme.getDetailedPos();
				if(detailedPos.equals("数詞")) {
					intList.add(num);
					break;
				}
			}
			++num;
		}

		return (Integer[])intList.toArray(new Integer[0]);
	}
}
