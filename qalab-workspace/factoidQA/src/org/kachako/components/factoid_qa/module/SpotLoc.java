package org.kachako.components.factoid_qa.module;

import java.util.List;

public class SpotLoc
{
	private static final String[] patLocList	= {
							// pattern 0: LOCATIONタグの連続
							"("
								+"(?:"	// LOCATIONタグの連続
									+"(?:(?:^|\n).*<LOCATION>.*)"
									+"(?:"
										+"(?:(?:^|\n)[・]\\s.*(<LOCATION>|<>).*)?"
										+"(?:(?:^|\n).*<LOCATION>.*)"
									+")*"
								+")"
							+")",
							// pattern 1: 括弧の中(「」)
							"(?:"
								+"(?:(?:^|\n)「\\s.*)"
								+"("
									+"(?:(?:^|\n)\\S+\\s.*)+?"
								+")"
								+"(?:(?:^|\n)」\\s.*)"
							+")",
							// pattern 2: 括弧の中(『』)
							"(?:"
								+"(?:(?:^|\n)『\\s.*)"
								+"("
									+"(?:(?:^|\n)\\S+\\s.*)+?"
								+")"
								+"(?:(?:^|\n)』\\s.*)"
							+")",
							// pattern 3: 括弧の中(（）)
							"(?:"
								+"(?:(?:^|\n)（\\s.*)"
								+"("
									+"(?:(?:^|\n)\\S+\\s.*)+?"
								+")"
								+"(?:(?:^|\n)）\\s.*)"
							+")",
							// pattern 4: 括弧の中(“”)
							"(?:"
								+"(?:(?:^|\n)“\\s.*)"
								+"("
									+"(?:(?:^|\n)\\S+\\s.*)+?"
								+")"
								+"(?:(?:^|\n)”\\s.*)"
							+")",
							// pattern 5:  LOCATIONがなくても英語名をつなぐ
							"("
								+"(?:"
									+"(?:(?:^|\n)[Ａ-Ｚァ-ヶ][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<LOCATION>).*)"
									+"(?:"
										+"(?:\n[Ａ-Ｚァ-ヶ・][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<LOCATION>).*)*"
										+"(?:\n[Ａ-Ｚァ-ヶ][Ａ-Ｚａ-ｚァ-ヶー]*\\s.*(?:<>|<LOCATION>).*)"
									+")?"
								+")"
							+")"
							// pattern 6: 最終手段 ほとんど何でもあり  LOCATIONに矛盾しないように結ぶ
							// なし
					};

	// 大域変数
//	private static final int FLAG_INIT	= 1;

	/**
	 * コンストラクタ
	 */
	public SpotLoc()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	//	##################################################################
	//	# spotter_location()
	//	#   形態素情報列において，指定した形態素を含む，指定した型の場所表現
	//	#   が あれば取り出す．なければ ""
	//	##################################################################
	//	sub spotter_location($$$) {
	public String spotterLocation(List<String> neDataList, int morphemeNo, String netType)
	{
		String ans	= "";

		if(netType.equals("LOCATION")) {
			String[] linesIds = SpotAnswer.spotterAddIndex(neDataList);

			// パタンを順番に適用して，一番最初に見つかったものを利用
			for(int i = 0 ; i < patLocList.length; i++) {
//System.out.println(patPersonList[i]);
				String pattern	= patLocList[i];
				ans	= SpotAnswer.spotterMkPatMatchFuncMorphForAns(pattern, linesIds, morphemeNo);
				if(ans != null && ans.length() > 0) {
					break;
				}
			}
		}
		else {
			ans = "";
		}

		return ans;
	}

}
