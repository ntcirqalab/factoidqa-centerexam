package org.kachako.components.factoid_qa.module;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.uima.jcas.cas.StringArray;
import org.kachako.share.regex.RegexCache;
import org.kachako.types.japanese.syntactic.KnpSegment;
import org.kachako.types.japanese.syntactic.KyotoDependency;
import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

public class GetObj
{
	private static final RegexCache regexCache = new RegexCache();

	/**
	 * コンストラクタ
	 */
	public GetObj()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	// ******** GetObj_back01.pm ****************
	/**
	 * 
	 * @param loc
	 * @param bunsetsuNum
	 * @param knpSegmentList
	 * @param knpInfoMap
	 * @return
	 */
	public static String[] getObjBack(String loc, int bunsetsuNum, List<KnpSegment> knpSegmentList,
															Map<KnpSegment,List<Morpheme>> knpInfoMap)
	{
		if(knpSegmentList == null || knpSegmentList.size() <= 0) {
			return null;
		}

		List<String>	objList	= new ArrayList<String>();
		for(int i = 0; i < bunsetsuNum; i++) {
			KnpSegment	knpSegment	= knpSegmentList.get(i);
			// 文節への付加情報
			StringArray	bunsetsuFeatures	= knpSegment.getKnpBunsetsuFeatures();
			boolean	taigenFlag	= false;
			boolean	suuryouFlag	= false;
			String	kakariStr	= null;
			for(int j = 0; j < bunsetsuFeatures.size(); j++) {
				String	str	= bunsetsuFeatures.get(j);
				if(str.equals("体言")) {
					taigenFlag	= true;
				}
				else if(str.equals("数量")) {
					suuryouFlag	= true;
				}
				else if(regexCache.find("(係:.+)", str)) {
					kakariStr	= regexCache.lastMatched(1);
				}
			}
			if(taigenFlag && (!loc.equals("parallel") || suuryouFlag)) {
				if(kakariStr != null) {
					// 形態素情報一覧
					List<Morpheme>	morphemeList	= knpInfoMap.get(knpSegment);
					String obj	= getObjExp(morphemeList);
					obj = obj +"("+i+")="+kakariStr+"="+loc+"\n";
					objList.add(obj);
				}
			}
		}

		return (String[])objList.toArray(new String[0]);
	}

	// ******** GetObj_fwd01.pm ****************
	// sub get_obj_fwd($$@){
	public static String getObjFwd(String act,int bunsetsuNum,
										List<KyotoDependency> kyotoDependencyList,
										List<KnpSegment> knpSegmentList,
										Map<KnpSegment,List<Morpheme>> knpInfoMap)
	{
		if(kyotoDependencyList == null || kyotoDependencyList.size() <= 0) {
			return "";
		}

		String	output	= "";
		for(int i = 0; i < kyotoDependencyList.size(); ++i) {
			KyotoDependency dependency	= kyotoDependencyList.get(i);
			KnpSegment	sourceSegment	= (KnpSegment)dependency.getSource();
			KnpSegment	targetSegment	= (KnpSegment)dependency.getTarget();

			if(sourceSegment.equals(knpSegmentList.get(bunsetsuNum))) {
				boolean	suuryouFlag	= false;
				boolean	taigenFlag	= false;
				String	kakariStr	= "";
				int		fwdNum		= 0;
				if(targetSegment == null) {
					continue;
				}
				StringArray	targetBunsetsuArray	= targetSegment.getKnpBunsetsuFeatures();
				for(int j = 0; j < targetBunsetsuArray.size(); ++j) {
					String	feature	= targetBunsetsuArray.get(j);
					if(feature.equals("数量")) {
						suuryouFlag	= true;
					}
					else if(act.equals("near") && feature.equals("体言")) {
						taigenFlag	= true;
					}
					else if(regexCache.find("(係:.+|文末)", feature)) {
						kakariStr	= regexCache.lastMatched(1);
						for(KnpSegment segment : knpSegmentList) {
							if(segment.equals(targetSegment)) {
								break;
							}
							++fwdNum;
						}
					}
				}

				if(suuryouFlag) {
					if(taigenFlag) {
						output	= getObjExp(knpInfoMap.get(targetSegment));
						if(!kakariStr.equals("")) {
							output += "("+fwdNum+")="+kakariStr+"=fwd\n";
						}
					}

					StringArray	sourceBunsetsuArray	= sourceSegment.getKnpBunsetsuFeatures();
					for(int j = 0; j < sourceBunsetsuArray.size(); ++j) {
						String	feature	= sourceBunsetsuArray.get(j);

						if(act.equals("far") && feature.equals("係:連格")) {
							output	= getObjExp(knpInfoMap.get(targetSegment));
							if(!kakariStr.equals("")) {
								output	+= "("+fwdNum+")="+kakariStr+"=after_pred\n";
							}
						}
					}

				}
			}
		}
		return output;
	}

	// ******** GetObj_inc01.pm ****************
	// sub get_obj_inc(@){
	public static String getObjInc(List<Morpheme> morphemeList)
	{
		if(morphemeList == null || morphemeList.size() <= 0) {
			return null;
		}

		String	objInc	= "";
		String	buffer	= "";

		for(int i = morphemeList.size()-1; i >= 0; --i) {
			Morpheme	morpheme	= morphemeList.get(i);
			String	surface	= morpheme.getSurfaceForm();
			String	pos		= morpheme.getPos();
			String	detailedPos	= morpheme.getDetailedPos();

			if(pos.equals("接尾辞")) {
				buffer	= surface + buffer;
			}
			else if(detailedPos.equals("数詞")) {
				return objInc;
			}
			else if(regexCache.matches("[、|。]",surface) && regexCache.find("助詞|判定詞|特殊",pos)) {
				objInc	= surface + buffer + objInc;
				buffer  = "";
			}
		}
		return objInc;
	}

	// ******** GetObjExp01.pm ****************
	// sub get_obj_exp(@){
	public static String getObjExp(List<Morpheme> morphemeList)
	{
		if(morphemeList == null || morphemeList.size() <= 0) {
			return null;
		}

		boolean	flag	= false;
		String	output	= "";

		for(int i = morphemeList.size()-1; i >= 0; --i) {
			Morpheme	morpheme	= morphemeList.get(i);
			String	surface	= morpheme.getSurfaceForm();
			String	pos		= morpheme.getPos();

			if(flag || (!pos.equals("助詞") && !regexCache.matches("[、|。]",surface) && !pos.equals("判定詞"))) {
				if(!output.equals("")) {
					output	= surface + "-" + output;
				}
				else {
					output	= surface;
				}

				flag = true;
			}
		}
		return output;
	}

	// ******** GetObj_topic01.pm ****************
	// sub get_obj_topic(@){
	public static String getObjTopic(List<KnpSegment> knpSegmentList,Map<KnpSegment,List<Morpheme>> knpInfoMap)
	{
		String	topic	= "";

		for(int i = 0; i < knpSegmentList.size(); ++i) {
			KnpSegment	knpSegment	= knpSegmentList.get(i);
			StringArray	bunsetsuFeatures	= knpSegment.getKnpBunsetsuFeatures();
			for(int j = 0; j < bunsetsuFeatures.size(); ++j) {
				String	feature	= bunsetsuFeatures.get(j);
				if(feature.equals("提題")) {
					topic	= getObjExp(knpInfoMap.get(knpSegment));
					topic	+= "("+i+")=提題=topic\n";
					return topic;
				}
			}
		}
		return "";
	}
}
