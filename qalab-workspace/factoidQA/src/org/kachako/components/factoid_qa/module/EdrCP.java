package org.kachako.components.factoid_qa.module;

import java.util.HashMap;

/**
 * EDR辞書検
 * 
 * @author saitou
 *
 */
public class EdrCP
{
	/**
	 * コンストラクタ
	 */
	public EdrCP()
	{
	}

	//----------------------------------------------------------------
	// public methods
	//----------------------------------------------------------------

	//sub EDRCP_all_ancestor($) {
	public static HashMap<String,Integer> edrcpAllAncestor(String ids, HashMap<String,String> cpcBySubMap)
	{
		HashMap<String,Integer> cpMap	= new HashMap<String,Integer>();
		for(String id : ids.split("|")) {
			edrcpAllAncestorSub(cpMap,id, 0,cpcBySubMap);
		}

	    return cpMap;
	}


	//----------------------------------------------------------------
	// private methods
	//----------------------------------------------------------------
	//sub EDRCP_all_ancestor_sub($$) {
	public static void edrcpAllAncestorSub(HashMap<String,Integer> cpMap, String id, int index,HashMap<String,String> cpcBySubMap)
	{
	    if(!cpMap.containsKey(id) || cpMap.get(id) > index) {
	    		cpMap.put(id, index);
	    }

	    if(cpcBySubMap != null && cpcBySubMap.get(id) != null) {
	    		String sups	= cpcBySubMap.get(id);
	    		for(String idSup :  sups.split("|")) {
	    			edrcpAllAncestorSub(cpMap,idSup, index+1,cpcBySubMap);
	    	}
	    }
	}
}
