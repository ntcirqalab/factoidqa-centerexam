package org.kachako.components.factoid_qa.search.result;

import java.io.File;


public class TestReadWebSearch
{
	/**
	 * テスト メイン.
	 *
	 * @param args
	 */
	public static void main(String[] args)
	{
		try {
			// ResultSet情報をXMLファイルから作成
			YahooXMLResultSet	xmlResultSet	= YahooXMLResultSet.create(new File("../factoidQA_JSA/tmp/webSearch.xml"));

System.out.println("XMLResultSet Read OK");

			// （ResultSet情報から）Result情報を取得
			YahooXMLResult[]	xmlResults	= xmlResultSet.getResult();
System.out.println("XMLResult n="+xmlResults.length);

			// Result情報の出力
			for(YahooXMLResult xmlResult : xmlResults) {
				System.out.println("Result title="+xmlResult.getTitle());
				System.out.println("Result summary="+xmlResult.getSummary());
				System.out.println("Result url="+xmlResult.getUrl());
				System.out.println("");
			}

		} catch(XMLAccessException e) {
			e.printStackTrace();
		}

		return;
	}

}
