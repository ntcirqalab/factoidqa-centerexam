package org.kachako.components.factoid_qa.search.result;

import java.util.ArrayList;
import java.util.List;


/**
 * (XMLの)Result情報を提供します.
 *
 * @author M.Okuwaki
 * @version $Revision: 1.0$ $Date: 0000-00-00 00:00:00 +0900$
 */
public class YahooXMLResult
{
	/**
	 * Resultのエレメント名
	 */
	private static final String	RESULT_ELEMENT_NAME	= "Result";

	/**
	 * タイトルのパラメータ名.
	 */
	private static final String	TITLE_PARAM_NAME	= "Title";

	/**
	 * サマリのパラメータ名.
	 */
	private static final String	SUMMARY_PARAM_NAME	= "Summary";

	/**
	 * URLのパラメータ名.
	 */
	private static final String	URL_PARAM_NAME		= "Url";

	/**
	 * Resultパラメータ.
	 */
	private ParameterReader	resultParam;


	/**
	 * Result情報を構築します.
	 *
	 * @param resultParam Resultパラメータ
	 */
	protected YahooXMLResult(ParameterReader resultParam) {
		this.resultParam	= resultParam;
		return;
	}

	/**
	 * ResultのXMLファイルからResultSet情報を作成します.
	 *
	 * @param file ResultSetのXMLファイル
	 * @return ResultSet情報
	 * @throws IllegalArgumentException 不正な引数で呼び出した場合
	 */
	public static YahooXMLResult[] create(YahooXMLResultSet xmlResultSet)
	{
		// パラメータチェック
		if(xmlResultSet == null) {
			throw new IllegalArgumentException("Illegal parameter. xmlResultSet is null");
		}

		 ParameterReader[]	resultParams	= xmlResultSet.getChilds(RESULT_ELEMENT_NAME);

		 List<YahooXMLResult>	list	= new ArrayList<YahooXMLResult>();
		 for(ParameterReader resultParam : resultParams) {
			 list.add(new YahooXMLResult(resultParam));
		 }

		 return list.toArray(new YahooXMLResult[list.size()]);
	}

	/**
	 * タイトルを取得します.
	 *
	 * @return タイトル。取得できない場合にはnull
	 */
	public String getTitle()
	{
		return resultParam.getValue(TITLE_PARAM_NAME);
	}

	/**
	 * サマリを取得します.
	 *
	 * @return サマリ。取得できない場合にはnull
	 */
	public String getSummary()
	{
		return resultParam.getValue(SUMMARY_PARAM_NAME);
	}

	/**
	 * URLを取得します.
	 *
	 * @return URL。取得できない場合にはnull
	 */
	public String getUrl()
	{
		return resultParam.getValue(URL_PARAM_NAME);
	}
}
