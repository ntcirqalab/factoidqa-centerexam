package org.kachako.components.factoid_qa.search;

/**
 * Web検索結果の情報を管理します.
 * 
 * @author JSA saitou
 *
 */
public class WebSearchResult
{
	private String title	= null;
	private String summary	= null;
	private String url		= null;

	public WebSearchResult(String title, String summary, String url)
	{
		this.title		= title;
		this.summary	= summary;
		this.url		= url;
	}

	/**
	 * タイトルを設定します.
	 * 
	 * @param title タイトル
	 */
	public void setTitle(String title)
	{
		this.title	= title;
	}

	/**
	 * タイトルを取得します.
	 *
	 * @return タイトル。取得できない場合にはnull
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * サマリを設定します.
	 * 
	 * @param summary サマリ
	 */
	public void setSummary(String summary)
	{
		this.summary	= summary;
	}

	/**
	 * サマリを取得します.
	 *
	 * @return サマリ。取得できない場合にはnull
	 */
	public String getSummary()
	{
		return summary;
	}

	/**
	 * URLを設定します.
	 * 
	 * @param url URL
	 */
	public void setUrl(String url)
	{
		this.url	= url;
	}

	/**
	 * URLを取得します.
	 *
	 * @return URL。取得できない場合にはnull
	 */
	public String getUrl()
	{
		return url;
	}

}
