package org.kachako.components.factoid_qa.search.result;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;


/**
 * XMLをアクセスするための基底クラス.
 *
 * @author M.Okuwaki
 * @version $Revision: 1.0$ $Date: 0000-00-00 00:00:00 +0900$
 */
public abstract class XMLAccess extends XMLParameter
{
	/**
	 * XMLアクセスを構築します.
	 *
	 * @param paramElement 対象とするパラメータエレメント
	 */
	protected XMLAccess(Element paramElement) {
		super(paramElement);

		return;
	}

	/**
	 * XMLアクセスを構築します.
	 *
	 * @param xmlParameter XML表記のパラメータ
	 */
	protected XMLAccess(XMLParameter xmlParameter) {
		super(xmlParameter.paramElement);

		return;
	}

	/**
	 * XMLファイルからXMLドキュメントを作成します.
	 *
	 * @param file XMLファイル
	 * @return XMLドキュメント
	 * @throws IllegalArgumentException 不正な引数で呼び出した場合
	 * @throws XMLAccessException 作成に失敗した場合
	 */
	protected static Document createDocuemt(File file) throws XMLAccessException
	{
		// パラメータチェック
		if(file == null) {
			throw new IllegalArgumentException("Illegal parameter. file is null");
		}

		// DOM作成のファクトリー作成
		DocumentBuilderFactory	factory	= null;
		try {
			factory	= DocumentBuilderFactory.newInstance();
		} catch(FactoryConfigurationError fce) {
			throw new XMLAccessException("failed. new DocumentBuilderFactory Instance. file="+file,fce);
		}

		// DOMパーサに妥当性チェックをなしに設定
		factory.setValidating(false);

		// DOMのビルダー作成
		DocumentBuilder	builder	= null;
		try {
			builder	= factory.newDocumentBuilder();
		} catch(ParserConfigurationException pce) {
			throw new XMLAccessException("failed. newDocumentBuilder. file="+file,pce);
		}

		// ドキュメント作成（XMLパース）
		Document	doc	= null;
		try {
			doc	= builder.parse(file);
		} catch(SAXException se) {
			throw new XMLAccessException("failed. parse file="+file,se);
		} catch(IOException ioe) {
			throw new XMLAccessException("failed. parse file="+file,ioe);
		} catch(IllegalArgumentException iae) {
			throw new XMLAccessException("failed. parse file="+file,iae);
		}

		return doc;
	}

	/**
	 * XML形式入力ストリームからXMLドキュメントを作成します.
	 *
	 * @param is XMLの入力ストリーム
	 * @return XMLドキュメント
	 * @throws IllegalArgumentException 不正な引数で呼び出した場合
	 * @throws XMLAccessException 作成に失敗した場合
	 */
	protected static Document createDocuemt(InputStream is) throws XMLAccessException
	{
		// パラメータチェック
		if(is == null) {
			throw new IllegalArgumentException("Illegal parameter. is is null");
		}

		// DOM作成のファクトリー作成
		DocumentBuilderFactory	factory	= null;
		try {
			factory	= DocumentBuilderFactory.newInstance();
		} catch(FactoryConfigurationError fce) {
			throw new XMLAccessException("failed. new DocumentBuilderFactory Instance.",fce);
		}

		// DOMパーサに妥当性チェックをなしに設定
		factory.setValidating(false);

		// DOMのビルダー作成
		DocumentBuilder	builder	= null;
		try {
			builder	= factory.newDocumentBuilder();
		} catch(ParserConfigurationException pce) {
			throw new XMLAccessException("failed. newDocumentBuilder.",pce);
		}

		// ドキュメント作成（XMLパース）
		Document	doc	= null;
		try {
			doc	= builder.parse(is);
		} catch(SAXException se) {
			throw new XMLAccessException("failed.",se);
		} catch(IOException ioe) {
			throw new XMLAccessException("failed.",ioe);
		} catch(IllegalArgumentException iae) {
			throw new XMLAccessException("failed.",iae);
		}

		return doc;
	}
}
