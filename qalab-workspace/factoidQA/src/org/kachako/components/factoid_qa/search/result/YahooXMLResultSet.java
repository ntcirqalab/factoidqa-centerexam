package org.kachako.components.factoid_qa.search.result;

import java.io.File;
import java.io.InputStream;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * (XMLの)ResultSet情報を提供します.
 *
 * @author M.Okuwaki
 * @version $Revision: 1.0$ $Date: 0000-00-00 00:00:00 +0900$
 */
public class YahooXMLResultSet extends XMLAccess
{
	/**
	 * ルート(ResultSet)のエレメント名
	 */
	private static final String	ROOT_ELEMENT_NAME	= "ResultSet";


	//-----------------------------------------------------
	// private constructor
	//-----------------------------------------------------

	/**
	 * ResultSet情報を構築します.
	 *
	 * @param rootElement 設定の対象となるルートエレメント
	 */
	protected YahooXMLResultSet(Element rootElement) {
		super(rootElement);

		return;
	}

	/**
	 * ResultSetのXMLファイルからResultSet情報を作成します.
	 *
	 * @param file ResultSetのXMLファイル
	 * @return ResultSet情報
	 * @throws IllegalArgumentException 不正な引数で呼び出した場合
	 * @throws XMLAccessException XMLのアクセスが失敗した場合
	 */
	public static YahooXMLResultSet create(File file) throws XMLAccessException
	{
		// パラメータチェック
		if(file == null) {
			throw new IllegalArgumentException("Illegal parameter. file is null");
		}

		// ドキュメント作成
		Document	doc	= null;
		try {
			doc	= createDocuemt(file);
		} catch(Exception e) {
			throw new XMLAccessException("failed. create Document. file="+file,e);
		}

		// ルート要素の取得
		Element	rootElement	= doc.getDocumentElement();
		if(rootElement == null) {
			throw new XMLAccessException("failed. document roor element not found. file="+file);
		}

		// ルート要素の名前チェック
		String	rootName	= rootElement.getNodeName();
		if(!ROOT_ELEMENT_NAME.equalsIgnoreCase(rootName)) {
			throw new XMLAccessException("failed. root element mismatch. file="+file+",name="+ROOT_ELEMENT_NAME);
		}

		return new YahooXMLResultSet(rootElement);
	}

	/**
	 * ResultSetのXML形式入力ストリームからResultSet情報を作成します.
	 *
	 * @param file ResultSetのXML形式入力ストリーム
	 * @return ResultSet情報
	 * @throws IllegalArgumentException 不正な引数で呼び出した場合
	 * @throws XMLAccessException XMLのアクセスが失敗した場合
	 */
	public static YahooXMLResultSet create(InputStream is) throws XMLAccessException
	{
		// パラメータチェック
		if(is == null) {
			throw new IllegalArgumentException("Illegal parameter. InputStream is null");
		}

		// ドキュメント作成
		Document	doc	= null;
		try {
			doc	= createDocuemt(is);
		} catch(Exception e) {
			throw new XMLAccessException("failed. create Document.",e);
		}

		// ルート要素の取得
		Element	rootElement	= doc.getDocumentElement();
		if(rootElement == null) {
			throw new XMLAccessException("failed. document roor element not found.");
		}

		// ルート要素の名前チェック
		String	rootName	= rootElement.getNodeName();
		if(!ROOT_ELEMENT_NAME.equalsIgnoreCase(rootName)) {
			throw new XMLAccessException("failed. root element mismatch. name="+ROOT_ELEMENT_NAME);
		}

		return new YahooXMLResultSet(rootElement);
	}

	/**
	 * Result情報を取得します.
	 *
	 * @return Result情報
	 */
	public YahooXMLResult[] getResult() {
		return YahooXMLResult.create(this);
	}
}
