package org.kachako.components.factoid_qa.search;

public class SearchFailedException extends Exception {

	private static final long serialVersionUID = -1137657492421952679L;
	private Exception e;

	public Exception getCause() {
		return e;
	}

	public SearchFailedException(Exception e) {
		this.e = e;
	}
}
