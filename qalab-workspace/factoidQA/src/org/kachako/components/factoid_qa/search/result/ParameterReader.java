package org.kachako.components.factoid_qa.search.result;


/**
 * パラメータを読み取るインターフェースを提供します.
 *
 * @author M.Okuwaki
 * @version $Revision: 1.0$ $Date: 0000-00-00 00:00:00 +0900$
 */
public interface ParameterReader
{
	/**
	 * 指定される名前の子パラメータにアクセスするParameterReaderを取得します.
	 *
	 * @param name 子供のパラメータ名
	 * @return 子パラメータのParameterReader
	 */
	public ParameterReader getChild(String name);

	/**
	 * 指定される名前の子パラメータにアクセスするParameterReaderを取得します.
	 *
	 * @param name 子供のパラメータ名
	 * @return 子パラメータのParameterReaderの配列
	 */
	public ParameterReader[] getChilds(String name);

	/**
	 * 指定される名前の子パラメータの有無を確認します.
	 *
	 * @param name 子供のパラメータ名
	 * @return 子パラメータがある場合にはtrue、ない場合にはfalse
	 */
	public boolean isChild(String name);

	/**
	 * パラメータ名で指定される値パラメータの有無を確認します.
	 *
	 * @param name パラメータ名
	 * @return 値パラメータがある場合にはtrue、ない場合にはfalse
	 */
	public boolean isValue(String name);

	/**
	 * パラメータ名で指定される値を文字列で取得します.
	 *
	 * @param name パラメータ名
	 * @return パラメータ値。取得できない場合にはnull
	 */
	public String getValue(String name);

	/**
	 * パラメータ名で指定される値を文字列で取得します.
	 *
	 * 値が見つからない場合、デフォルト値で指定される値を戻す。
	 *
	 * @param name パラメータ名
	 * @param defaultValue デフォルト値
	 * @return パラメータ値。取得できない場合にはデフォルト値
	 */
	public String getValue(String name,String defaultValue);

	/**
	 * パラメータ名で指定されるすべての値を文字列で取得します.
	 *
	 * @param name パラメータ名
	 * @return パラメータ値。取得できない場合には空の配列
	 */
	public String[] getValues(String name);
}
