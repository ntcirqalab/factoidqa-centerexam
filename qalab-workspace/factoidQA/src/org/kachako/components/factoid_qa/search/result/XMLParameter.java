package org.kachako.components.factoid_qa.search.result;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;


/**
 * XML表記のパラメータの読み取りを提供します.
 *
 * 属性記述と子要素の記述を（区別なく）パラメータとして提供します。
 *
 * @author M.Okuwaki
 * @version $Revision: 1.0$ $Date: 0000-00-00 00:00:00 +0900$
 */
public class XMLParameter implements ParameterReader
{
	/**
	 * パラメータの要素.
	 */
	protected Element	paramElement	= null;


	/**
	 * パラメータエレメントから、XMLParameterを構築します.
	 *
	 * @param paramElement パラメータエレメント
	 */
	public XMLParameter(Element paramElement) {
		this.paramElement	= paramElement;
		return;
	}

	/**
	 * パラメータから、XMLParameterを構築します.
	 *
	 * @param xmlParameter パラメータ
	 */
	public XMLParameter(XMLParameter xmlParameter) {
		this.paramElement	= xmlParameter.paramElement;
		return;
	}


	/**
	 * 指定される名前の子パラメータにアクセスするParameterReaderを取得します.
	 *
	 * @param name 子供のパラメータ名
	 * @return 子パラメータのParameterReader
	 */
	@Override
	public ParameterReader getChild(String name)
	{
		ParameterReader[]	confParamReader	= getChilds(name);

		return confParamReader.length > 0 ? confParamReader[0] : null;
	}

	/**
	 * 指定される名前の子パラメータにアクセスするParameterReaderを取得します.
	 *
	 * @param name 子供のパラメータ名
	 * @return 子パラメータのParameterReaderの配列
	 */
	@Override
	public ParameterReader[] getChilds(String name)
	{
		if(name == null) {
			return new ParameterReader[0];
		}

		List<ParameterReader>	listConfigParam	= new ArrayList<ParameterReader>();

		// 指定の名前の子elementを取得
		Element[]	chiilds	= getChildElement(paramElement,name);
		for(Element chiild : chiilds) {
			// 子供の子elementを取得
			Element[]	chiildChilds	= getChildElement(chiild,null);
			// 子供の子element数をチェック
			if(chiildChilds.length <= 0) {
				// (さらに)属性を取得
				NamedNodeMap	nameMap	= chiild.getAttributes();

				// 子供の子element、属性がなければ、パラメータの記述なので
				// 親となるParameterReaderには、ならない.
				if(nameMap.getLength() <= 0) {
					continue;
				}
			}

			listConfigParam.add(new XMLParameter(chiild));
		}

		return listConfigParam.toArray(new ParameterReader[listConfigParam.size()]);
	}

	/**
	 * 指定される名前の子パラメータの有無を確認します.
	 *
	 * @param name 子供のパラメータ名
	 * @return 子パラメータがある場合にはtrue、ない場合にはfalse
	 */
	@Override
	public boolean isChild(String name) {
		return getChilds(name).length > 0;
	}

	/**
	 * パラメータ名で指定される値パラメータの有無を確認します.
	 *
	 * @param name パラメータ名
	 * @return 値パラメータがある場合にはtrue、ない場合にはfalse
	 */
	@Override
	public boolean isValue(String name)
	{
		// パラメータチェック
		if(name == null) {
			return false;
		}

		// 属性
		if(paramElement.hasAttribute(name)) {
			return true;
		}

		// 指定の名前の子elementを取得
		Element[]	chiilds	= getChildElement(paramElement,name);

		return chiilds.length > 0;
	}

	/**
	 * パラメータ名で指定される値を文字列で取得します.
	 *
	 * @param name パラメータ名
	 * @return パラメータ値。取得できない場合にはnull
	 */
	@Override
	public String getValue(String name) {
		return getValue(name,null);
	}

	/**
	 * パラメータ名で指定される値を文字列で取得します.
	 *
	 * 値が見つからない場合、デフォルト値で指定される値を戻す。
	 *
	 * @param name パラメータ名
	 * @param defaultValue デフォルト値
	 * @return パラメータ値。取得できない場合にはデフォルト値
	 */
	@Override
	public String getValue(String name,String defaultValue)
	{
		String[]	values	= getValues(name);

		return values.length > 0 ? values[0] : defaultValue;
	}

	/**
	 * パラメータ名で指定されるすべての値を文字列で取得します.
	 *
	 * @param name パラメータ名
	 * @return パラメータ値。取得できない場合には空の配列
	 */
	@Override
	public String[] getValues(String name)
	{
		// パラメータチェック
		if(name == null) {
			return new String[0];
		}

		List<String>	listValue	= new ArrayList<String>();
		if(paramElement != null) {
			// 属性
			if(paramElement.hasAttribute(name)) {
				listValue.add(paramElement.getAttribute(name));
			}

			// 指定の名前の子elementを取得
			Element[]	chiilds	= getChildElement(paramElement,name);
			for(Element chiild : chiilds) {
				Node	node	= chiild.getFirstChild();
				if(node != null && node.getNodeType() == Node.TEXT_NODE) {
					listValue.add(node.getNodeValue());
				}
			}
		}

		return listValue.toArray(new String[listValue.size()]);
	}

	/**
	 * 指定される名前の子エレメントを取得します.
	 *
	 * 名前にnullを指定した場合、すべての子エレメントを取得します。
	 *
	 * @param parent 親エレメント
	 * @param name name 名前
	 * @return 取得した要素の配列。見つからない場合には空の配列
	 */
	protected static Element[] getChildElement(Element parent,String name)
	{
		// パラメータチェック
		if(parent == null) {
			return new Element[0];	// error
		}

		// 子要素を検索
		List<Element>	listElement	= new ArrayList<Element>();
		for(Node childNode=parent.getFirstChild(); childNode != null; childNode=childNode.getNextSibling()) {
			// 要素ノードをチェック
			if(childNode.getNodeType() != Node.ELEMENT_NODE) {
				continue;	// 次のノードへ
			}

			// 名前のチェック
			if(name != null) {
				String	nodeName	= childNode.getNodeName();
				if(name.equalsIgnoreCase(nodeName)) {
					// 指定の名前の子要素
					listElement.add(Element.class.cast(childNode));
				}
			}
			else {
				// (名前未指定の場合の)子要素
				listElement.add(Element.class.cast(childNode));
			}
		}

		return listElement.toArray(new Element[listElement.size()]);
	}
}
