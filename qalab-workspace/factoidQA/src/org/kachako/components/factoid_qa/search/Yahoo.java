
package org.kachako.components.factoid_qa.search;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import org.kachako.components.factoid_qa.search.result.XMLAccessException;
import org.kachako.components.factoid_qa.search.result.YahooXMLResult;
import org.kachako.components.factoid_qa.search.result.YahooXMLResultSet;


/**
 * SearchEngineの実装. Yahoo! Japan.
 * 
 * @author fujima 
 * 
 */
public class Yahoo implements SearchEngine
{
	/* HTTPリトライカウンタ */
	private static final int	NUMBER_OF_RETRIES = 10;
	/* HTTPリトライ間隔 ミリ秒*/
	private static final long	RETRY_INTERVAL_MILLISECONDS = 10000;

	/**
	 * 検索を実行します.
	 * 
	 * @param queries 検索クエリのリスト
	 * @param resultNum 検索結果の数
	 * @param appId アプリケーションID
	 * 
	 * @return 検索URL接続の入力ストリーム
	 * @throws IOException
	 * @throws InterruptedException
	 */
	@Override
	public InputStream search(List<String> queries, int resultNum, String appId) throws SearchFailedException
	{
		// 検索URL生成
		String query	= null;
		try {
			query = URLEncoder.encode(StringUtils.join(queries, ' '), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new SearchFailedException(e);
		}
		String urlString	= "http://search.yahooapis.jp/WebSearchService/V2/webSearch?appid=" + appId
																+ "&query=" + query + "&results=" + resultNum;

System.out.println(urlString);

		URL url = null;
		try {
			url = new URL(urlString);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw new SearchFailedException(e);
		}

		try {
			HttpURLConnection connection = null;
			// リトライ
			for(int count = 0; count < NUMBER_OF_RETRIES; count++)
			{
				connection = (HttpURLConnection)url.openConnection();
				if(connection.getResponseCode() == HttpURLConnection.HTTP_UNAVAILABLE) {
					System.err.println("Service unavailable. Retrying after 10 seconds.");
					Thread.sleep(RETRY_INTERVAL_MILLISECONDS);
				}
				else {
					break;
				}
			}
			return connection.getInputStream();

		} catch(IOException e) {
			e.printStackTrace();
System.err.println("Connection failed");
			throw new SearchFailedException(e);
		} catch(InterruptedException e) {
			e.printStackTrace();
			throw new SearchFailedException(e);
		}
	}

	/**
	 * 検索結果を解析し、情報を返します.
	 * 
	 * @param is 検索結果の入力ストリーム
	 * @return 検索結果情報クラス
	 * @throws SearchFailedException
	 */
	public WebSearchResult[] getWebSearchResult(InputStream is) throws SearchFailedException
	{
		List<WebSearchResult> list	= new ArrayList<>();

		try {
			// ResultSet情報をXMLファイルから作成
			YahooXMLResultSet	xmlResultSet	= YahooXMLResultSet.create(is);
System.out.println("XMLResultSet Read OK");

			// （ResultSet情報から）Result情報を取得
			YahooXMLResult[]	xmlResults	= xmlResultSet.getResult();
System.out.println("XMLResult n="+xmlResults.length);

			// Result情報の出力
			for(YahooXMLResult xmlResult : xmlResults) {
				String title	= xmlResult.getTitle();
				String summary	= xmlResult.getSummary();
				String url		= xmlResult.getUrl();
				System.out.println("Result title="+title);
				System.out.println("Result summary="+summary);
				System.out.println("Result url="+url);
				System.out.println("");
				list.add(new WebSearchResult(title,summary,url));
			}

		} catch(XMLAccessException e) {
			e.printStackTrace();
			throw new SearchFailedException(e);
		}

		return list.toArray(new WebSearchResult[list.size()]);
	}
}
