package org.kachako.components.factoid_qa.search;

import java.io.InputStream;
import java.util.List;

public interface SearchEngine
{
	public InputStream search(List<String> queries, int n, String appId)
			throws SearchFailedException;

	public WebSearchResult[] getWebSearchResult(InputStream is)
			throws SearchFailedException;

}
