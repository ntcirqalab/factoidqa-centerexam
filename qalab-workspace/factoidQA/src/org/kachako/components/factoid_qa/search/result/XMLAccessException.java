package org.kachako.components.factoid_qa.search.result;


/**
 * XMLアクセスで例外が発生した場合にスローされます.
 *
 * @author M.Okuwaki
 * @version $Revision: 1.0$ $Date: 0000-00-00 00:00:00 +0900$
 */
public class XMLAccessException extends Exception
{
	/**
	 * シリアル・バージョンID.
	 */
	private static final long	serialVersionUID	= 1L;


	/**
	 * 指定された詳細メッセージを持つConfigExceptionを構築します.
	 *
	 * @param message 詳細メッセージ
	 */
	public XMLAccessException(String message) {
		super(message);
	}

	/**
	 * 指定された詳細メッセージと、原因の例外を持つ、ConfigExceptionを生成します.
	 *
	 * @param message 詳細メッセージ
	 * @param cause 原因の例外
	 */
	public XMLAccessException(String message,Throwable cause) {
		super(message,cause);
	}
}
