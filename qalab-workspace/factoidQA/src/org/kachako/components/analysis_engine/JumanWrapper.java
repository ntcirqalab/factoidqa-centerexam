package org.kachako.components.analysis_engine;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.kachako.components.utils.AbstractStdioWrapper;

import org.apache.uima.UimaContext;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.StringArray;
import org.apache.uima.resource.ResourceInitializationException;
import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

/**
 * 形態素解析システムJumanに質問文を渡しCasに結果を格納します.
 * 
 * @author JSA Saitou
 *
 */
public class JumanWrapper extends AbstractStdioWrapper
{
	// 正規表現のパターン
	private static final String NOT_MORPHEME_PATTERN = "^([@]|EOS).*$";

	// 区切り文字
	private static final String ENTRY_SEPARATOR = "\\s";

	// 実行ファイルパス
	private final String PARAM_JUMAN_EXEC_FILE	= "JumanExecFile";

	// 文字エンコード
	private final String PARAM_ENCODE		= "Encode";

	private  String jumanExecFile	= null;
	private  String encode	= null;

	private int beginOfMorpheme	= 0;

	public void initialize(UimaContext aContext) throws ResourceInitializationException
	{
		super.initialize(aContext);

System.out.println("--------JumanWrapper initialize---------");

		// JUMAN実行ファイルパス取得
		jumanExecFile = (String)aContext.getConfigParameterValue(PARAM_JUMAN_EXEC_FILE);

		// 文字エンコード取得
		encode = (String)aContext.getConfigParameterValue(PARAM_ENCODE);
	}

	@Override
	public String[] getCommand(String os)
	{
		List<String> commands	= new ArrayList<String>();
		commands.add(jumanExecFile);
		commands.add("-e");

		return commands.toArray(new String[0]);
	}

	@Override
	public String getStdinEncodingName() {
		return encode;
	}

	@Override
	public String getStdoutEncodingName() {
		return encode;
	}

	@Override
	public boolean redirectErrorStream() {
		// TODO 自動生成されたメソッド・スタブ
		return false;
	}

	@Override
	public void recoverProcess() {
		// TODO 自動生成されたメソッド・スタブ
	}

	@Override
	public boolean readLineAtInitialization(String line) {
		// TODO 自動生成されたメソッド・スタブ
		return true;
	}

	@Override
	public boolean readLineAndUpdateCas(String line, JCas jCas)
	{
//System.out.println(line);
		// Morpheme生成
		setMorpheme(line,jCas);

		if(line.equals("EOS")) {
			beginOfMorpheme	= 0;
			return true;
		}
		return false;
	}
	@Override
	public String convertCasToTextLines(JCas jCas)
	{
		String	text	= jCas.getDocumentText();

		String lineSep	= System.lineSeparator();
		Pattern pattern	= Pattern.compile(lineSep+"\\z");
		Matcher matcher	= pattern.matcher(text);
		if(!matcher.find()) {
			text	= text + lineSep;
		}
		return text;
	}

	private void setMorpheme(String line, JCas jCas)
	{
		if(line.equals("EOS")) {
			return;
		}

		if(!Pattern.matches(NOT_MORPHEME_PATTERN, line)) {
			String[] str = line.split(ENTRY_SEPARATOR);

			String[] allReadings	= { str[1] };
			StringArray strArray = new StringArray(jCas, allReadings.length);
			strArray.copyFromArray(allReadings, 0, 0, allReadings.length);
	
			Morpheme morpheme = new Morpheme(jCas);
			morpheme.setSurfaceForm(str[0]);
			morpheme.setReadings(strArray);
			morpheme.setBaseForm(str[2]);
			morpheme.setPos(str[3]);
			morpheme.setDetailedPos(str[5]);
			morpheme.setBegin(beginOfMorpheme);
			morpheme.setEnd(beginOfMorpheme + str[0].length());
			morpheme.setConjugateType(str[7]);
			morpheme.setConjugateForm(str[9]);
			morpheme.addToIndexes();
			beginOfMorpheme += str[0].length();
		}
		return;
	}
}
