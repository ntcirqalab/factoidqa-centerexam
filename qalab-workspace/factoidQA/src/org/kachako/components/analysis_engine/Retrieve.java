
package org.kachako.components.analysis_engine;

//import java.io.BufferedReader;
//import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
//import java.io.FileReader;
//import java.io.IOException;
import java.io.InputStream;
//import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
//import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.TOP;
//import org.apache.uima.jcas.cas.TOP;
import org.apache.uima.resource.ResourceInitializationException;
//import org.kachako.factoid_qa.manage.RequireInfo;
//import org.kachako.factoid_qa.module.GetRequireInfo;
import org.kachako.components.factoid_qa.search.SearchEngine;
import org.kachako.components.factoid_qa.search.SearchFailedException;
import org.kachako.components.factoid_qa.search.WebSearchResult;
import org.kachako.components.factoid_qa.search.Yahoo;

import org.kachako.types.qa.Document;
import org.kachako.types.qa.KeyTerm;
import org.kachako.types.qa.Question;
//import org.kachako.ui.model.ssh.ResourceLoader;
import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

/**
 * 文書を検索し、結果を保存します.
 *
 * @author JSA Saitou
 *
 */
public class Retrieve extends JCasAnnotator_ImplBase
{
	/* RS実行結果 VIEW名 PREFIX */
	private static final String RS_RESULT_VIEW_NAME_PREFIX	= "RS";

//	private final ResourceLoader	resourceLoader	= ResourceLoader.getInstance();

	// 質問文が格納されているView(Sofa)名
	private final String PARAM_QUESTION_VIEW_NAME	= "QuestionViewName";
	// Yahoo検索API使用するかのフラグ
	private final String PARAM_USE_API_FLAG	= "UseYahooAPI";
	// Yahoo アプリケーションID
	private final String PARAM_APP_ID	= "YahooAppId";
	// 検索結果数
	private final String PARAM_RESULT_NUM	= "ResultNum";

	// 質問文が格納されているView(Sofa)名
	private  String questionViewName		= null;
	private  boolean useApiFlag	= true;
	private  String yahooAppId		= null;
	private  int resultNum		= 0;

	// 助数辞
//	private String[] josujiList		= null;

	private SearchEngine searchEngine;

	@Override
	public void initialize(UimaContext aContext) throws ResourceInitializationException
	{
		super.initialize(aContext);

System.out.println("--------Retrieve initialize---------");

		//  質問文が格納されているView(Sofa)名取得
		questionViewName = (String)aContext.getConfigParameterValue(PARAM_QUESTION_VIEW_NAME);
		// Yahoo検索API使用するかのフラグ取得
		useApiFlag	= (Boolean)aContext.getConfigParameterValue(PARAM_USE_API_FLAG);
		// Yahoo アプリケーションIDの取得
		yahooAppId	= (String)aContext.getConfigParameterValue(PARAM_APP_ID);
		// 検索結果数の取得
		resultNum	= (Integer)aContext.getConfigParameterValue(PARAM_RESULT_NUM);

		searchEngine = new Yahoo();

		// 助数辞の読み込み
//		setJosujiList("org/kachako/factoid_qa/suffix.txt");
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException
	{
System.out.println("--------Retrieve start---------");

		try {
			JCas questionViewCas	=  jCas.getView(questionViewName);

			// 質問文の取得
			Iterator<TOP> questionFSIterator = questionViewCas.getJFSIndexRepository().getAllIndexedFS(Question.type);
			String	questionText	= null;
			if(questionFSIterator.hasNext()) {
				Question	questionFS	= (Question)questionFSIterator.next();
				questionText	= questionFS.getCoveredText();
System.out.println("questionText=["+questionText+"]");
			}

			// TOPIC IDの取得
			String	topicId	= null;
			Iterator<TOP> sourceDocInfoFSIterator = questionViewCas.getJFSIndexRepository().getAllIndexedFS(SourceDocumentInformation.type);
			if(sourceDocInfoFSIterator.hasNext()) {
				// TOPIC_ID 取得
				topicId = ((SourceDocumentInformation) sourceDocInfoFSIterator.next()).getUri();
System.out.println("topic id="+topicId);
			}

			// 形態素解析結果取得
			Iterator<TOP> morphemeFSIterator = questionViewCas.getJFSIndexRepository().getAllIndexedFS(Morpheme.type);

			List<Morpheme> morphemeList = new ArrayList<>();
			while(morphemeFSIterator.hasNext()) {
				morphemeList.add((Morpheme)morphemeFSIterator.next());
			}

			// ソート
			Collections.sort(morphemeList, new Comparator<Morpheme>(){
				public int compare(Morpheme x, Morpheme y){
					return x.getBegin() - y.getBegin();
				}
			});

			// キーワード取得
			Iterator<TOP> keytermFSIterator = questionViewCas.getJFSIndexRepository().getAllIndexedFS(KeyTerm.type);

			List<KeyTerm> keyTermList = new ArrayList<>();
			while(keytermFSIterator.hasNext()) {
				keyTermList.add((KeyTerm)keytermFSIterator.next());
			}

			// クエリの生成
			List<String> queries = makeSubqueries(keyTermList, morphemeList);

			WebSearchResult[] searchResults	= null;
			if(useApiFlag) {
				try {
					InputStream	is	= searchEngine.search(queries, resultNum, yahooAppId);
					// 検索結果をパースする。
					searchResults	= searchEngine.getWebSearchResult(is);
				}
				catch (SearchFailedException e) {
					e.printStackTrace();
					throw new AnalysisEngineProcessException(e);
				}
			}
			else {
				// 真面目に検索していると、すぐにAPIリミットに達してしまう。
				// テスト用
				try {
					InputStream		is	= new FileInputStream("../factoidQA/tmp/webSearch.xml");
//					InputStream		is	= new FileInputStream("../factoidQA/tmp/webSearch_5.xml");
					// 検索結果をパースする。
					searchResults	= searchEngine.getWebSearchResult(is);
				}
				catch (FileNotFoundException | SearchFailedException e) {
					e.printStackTrace();
					throw new AnalysisEngineProcessException(e);
				}
			}

			//HashMap<String,Integer> josujiMap	= getJosuji();
			// 質問文（クエリ）から抽出すべき情報を取得する。
			//RequireInfo	requireInfo	= GetRequireInfo.get(questionText, morphemeList, josujiMap);
			//String requireType	= requireInfo.getType();
			//String pattern1	= requireInfo.getPattern1();
			//String pattern2	= requireInfo.getPattern2();

			int	count	= 1;
			// Title、Summary、URLを取り出す
			if(searchResults != null && searchResults.length > 0) {
				//HashMap<WebSearchResult,Double> searchResScoreMap	= new HashMap<WebSearchResult,Double>();
				for(WebSearchResult result : searchResults) {

					String	title	= result.getTitle();
					String	summary	= result.getSummary();
					String	url		= result.getUrl();
					title	= replaceEntities(title);
					summary	= replaceEntities(summary);
					summary	= replaceSummary(summary);

					JCas rsViewCas	= jCas.createView(String.format("%s.%d", RS_RESULT_VIEW_NAME_PREFIX, count));
					int beginIndex	= 0;
					int endIndex	= summary.length();
					// DocumentとCasに格納
					Document	document = new Document(rsViewCas);
					document.setRank(count);
					document.setTitle(title);
					document.setContent(summary);
					document.setDocId(url);
					document.setBegin(beginIndex);
					document.setEnd(endIndex);
//					document.setScore((float)score);
					document.addToIndexes();

//System.out.println("ViewName="+rsViewCas.getViewName()+" docId="+url);
					rsViewCas.setDocumentText(summary+"\n");
					SourceDocumentInformation rsSofaSourceDocumentInformationFS =
													new SourceDocumentInformation(rsViewCas);
					rsSofaSourceDocumentInformationFS.addToIndexes();
					rsSofaSourceDocumentInformationFS.setUri(topicId);

					count++;
				}
			}

		} catch(CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}

		return;
	}


	//----------------------------------------------------------------
	// private methods
	//----------------------------------------------------------------

	/**
	 * 助数辞を読み込み、設定する.
	 * 
	 * @param path
	 * @throws ResourceInitializationException
	 */
//	private void setJosujiList(String path) throws ResourceInitializationException
//	{
//		// 助数辞の読み込み
//		BufferedReader reader = null;
//		try {
//			File	decisionFile	= resourceLoader.getResourceFile(Paths.get(path));
//			if(decisionFile != null) {
//				reader	= new BufferedReader(new FileReader(decisionFile));
//				List<String> list	= new ArrayList<>();
//				String line	= null;
//				while((line = reader.readLine()) != null) {
//					list.add(line);
//				}
//				josujiList	= list.toArray(new String[0]);
//				reader.close();
//			}
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//System.out.println("josujiList error:"+e);
//			throw new ResourceInitializationException();
//		} catch(Exception e) {
//			e.printStackTrace();
//System.out.println(e);
//			throw new ResourceInitializationException();
//		} finally {
//			if(reader != null) {
//				try {
//					reader.close();
//				} catch(IOException e) {
//					e.printStackTrace();
//					throw new ResourceInitializationException();
//				}
//			}
//		}
//		return;
//	}

	/**
	 * サブクエリのリストを作ります.
	 * 
	 * @param keytermList KeyTermのリスト
	 * @param morphemeList 形態素解析結果のリスト
	 */
	private List<String> makeSubqueries(List<KeyTerm> keytermList, List<Morpheme> morphemeList)
	{
		if(keytermList == null || morphemeList == null) {
			return null;
		}

		Map<Point, Morpheme> map = new TreeMap<Point, Morpheme>();
		for(Morpheme morpheme : morphemeList){
			map.put(new Point(morpheme.getBegin(), morpheme.getEnd()), morpheme);
		}

		List<String> subQueries = new ArrayList<String>();

		for(KeyTerm keyTerm : keytermList) {
			Point mpKey = new Point(keyTerm.getBegin(), keyTerm.getEnd());
			if(map.containsKey(mpKey)){
				Morpheme morpheme = map.get(mpKey);

				String pos = morpheme.getPos();
				if(!pos.equals("動詞") && !pos.equals("形容詞")){
					subQueries.add(morpheme.getBaseForm());
				}
				if(pos.equals("形容詞")){
					subQueries.add(morpheme.getSurfaceForm());
				}
			}
		}

		return subQueries;
	}

	private static class Point implements Comparable<Object>
	{
		private int x;
		private int y;
		//public int getX(){ return x; }
		//public int getY(){ return y; }

		public Point(int x, int y){
			this.x = x;
			this.y = y;
		}

		@Override
		public int compareTo(Object object)
		{
			// TODO Auto-generated method stub
			if(object instanceof Point) {
				Point p = (Point)object;
				if(x > p.x) return 1;
				else if(x < p.x) return -1;
				else {
					if(y > p.y) return 1;
					else if(y < p.y) return -1;
					else return 0;
				}
			}
			else throw new Error();
		}
	}

	/**
	 * @param str 文字列
	 * @return 英数字アンダーバー以外の文字をアンダーバーにした文字列.
	 */
//	private static String sanitize(String str)
//	{
//		return str.replaceAll("[^0-9a-zA-Z_]", "_");
//	}

	private String replaceEntities(String str)
	{
		if(str == null || str.length() <= 0) {
			return "";
		}

		str	= str.replaceAll("&nbsp;"," ");
		str	= str.replaceAll("&cent;","¢");
		str	= str.replaceAll("&pound;","£");
		str	= str.replaceAll("&yen;","¥");
		str	= str.replaceAll("&sect;","§");
		str	= str.replaceAll("&copy;","(c)");
		str	= str.replaceAll("&laquo;","《");
		str	= str.replaceAll("&not;","¬");
		str	= str.replaceAll("&reg;","(R)");
		str	= str.replaceAll("&macr;","‾");
		str	= str.replaceAll("&deg;","°");
		str	= str.replaceAll("&micro;","μ");
		str	= str.replaceAll("&para;","¶");
		str	= str.replaceAll("&middot;","・");
		str	= str.replaceAll("&cedil;","’");
		str	= str.replaceAll("&raquo;","》");
		str	= str.replaceAll("&frac14;","1/4");
		str	= str.replaceAll("&frac12;","1/2");
		str	= str.replaceAll("&frac34;","3/4");
		str	= str.replaceAll("&times;","×");
		str	= str.replaceAll("&divide;","÷");

		str	= str.replaceAll("&Alpha;","Α");
		str	= str.replaceAll("&Beta;","Β");
		str	= str.replaceAll("&Gamma;","Γ");
		str	= str.replaceAll("&Delta;","Δ");
		str	= str.replaceAll("&Epsilon;","Ε");
		str	= str.replaceAll("&Zeta;","Ζ");
		str	= str.replaceAll("&Eta;","Η");
		str	= str.replaceAll("&Theta;","Θ");
		str	= str.replaceAll("&Iota;","Ι");
		str	= str.replaceAll("&Kappa;","Κ");
		str	= str.replaceAll("&Lambda;","Λ");
		str	= str.replaceAll("&Mu;","Μ");
		str	= str.replaceAll("&Nu;","Ν");
		str	= str.replaceAll("&Xi;","Ξ");
		str	= str.replaceAll("&Omicron;","Ο");
		str	= str.replaceAll("&Pi;","Π");
		str	= str.replaceAll("&Rho;","Ρ");
		str	= str.replaceAll("&Sigma;","Σ");
		str	= str.replaceAll("&Tau;","Τ");
		str	= str.replaceAll("&Upsilon;","Υ");
		str	= str.replaceAll("&Phi;","Φ");
		str	= str.replaceAll("&Chi;","Χ");
		str	= str.replaceAll("&Psi;","Ψ");
		str	= str.replaceAll("&Omega;","Ω");

		str	= str.replaceAll("&alpha;","α");
		str	= str.replaceAll("&beta;","β");
		str	= str.replaceAll("&gamma;","γ");
		str	= str.replaceAll("&delta;","δ");
		str	= str.replaceAll("&epsilon;","ε");
		str	= str.replaceAll("&zeta;","ζ");
		str	= str.replaceAll("&eta;","η");
		str	= str.replaceAll("&theta;","θ");
		str	= str.replaceAll("&iota;","ι");
		str	= str.replaceAll("&kappa;","κ");
		str	= str.replaceAll("&lambda;","λ");
		str	= str.replaceAll("&mu;","μ");
		str	= str.replaceAll("&nu;","ν");
		str	= str.replaceAll("&xi;","ξ");
		str	= str.replaceAll("&omicron;","ο");
		str	= str.replaceAll("&pi;","π");
		str	= str.replaceAll("&rho;","ρ");
		str	= str.replaceAll("&sigma;","σ");
		str	= str.replaceAll("&tau;","τ");
		str	= str.replaceAll("&upsilon;","υ");
		str	= str.replaceAll("&phi;","φ");
		str	= str.replaceAll("&chi;","χ");
		str	= str.replaceAll("&psi;","ψ");
		str	= str.replaceAll("&omega;","ω");

		str	= str.replaceAll("&hellip;","…");
		str	= str.replaceAll("&prime;","’");
		str	= str.replaceAll("&Prime;","”");
		str	= str.replaceAll("&trade;","TM");

		str	= str.replaceAll("&larr;","←");
		str	= str.replaceAll("&uarr;","↑");
		str	= str.replaceAll("&rarr;","→");
		str	= str.replaceAll("&darr;","↓");
		str	= str.replaceAll("&rArr;","⇒");
		str	= str.replaceAll("&hArr;","⇔");

		str	= str.replaceAll("&forall;","∀");
		str	= str.replaceAll("&part;","∂");
		str	= str.replaceAll("&exist;","∃");
		str	= str.replaceAll("&empty;","");

		str	= str.replaceAll("&nabla;","∇");
		str	= str.replaceAll("&isin;","∈");
		str	= str.replaceAll("&ni;","∋");
		str	= str.replaceAll("&prod;","Π");
		str	= str.replaceAll("&sum;","Σ");
		str	= str.replaceAll("&minus;","−");
		str	= str.replaceAll("&lowast;","＊");
		str	= str.replaceAll("&radic;","√");

		str	= str.replaceAll("&prop;","∝");
		str	= str.replaceAll("&infin;","∞");
		str	= str.replaceAll("&ang;","∠");
		str	= str.replaceAll("&and;","∧");
		str	= str.replaceAll("&or;","∨");
		str	= str.replaceAll("&cap;","∩");
		str	= str.replaceAll("&cup;","∪");
		str	= str.replaceAll("&int;","∫");
		str	= str.replaceAll("&there4;","∴");
		str	= str.replaceAll("&sim;","〜");
		str	= str.replaceAll("&ne;","≠");
		str	= str.replaceAll("&equiv;","≡");
		str	= str.replaceAll("&le;","≦");
		str	= str.replaceAll("&ge;","≧");
		str	= str.replaceAll("&sub;","⊂");
		str	= str.replaceAll("&sup;","⊃");
		str	= str.replaceAll("&sube;","⊆");
		str	= str.replaceAll("&supe;","⊇");
		str	= str.replaceAll("&sdot;","・");

		str	= str.replaceAll("&lang;","〈");
		str	= str.replaceAll("&rang;","〉");

		str	= str.replaceAll("&quot;","”");
		str	= str.replaceAll("&amp;","＆");
		str	= str.replaceAll("&lt;","＜");
		str	= str.replaceAll("&gt;","＞");

		return str;
	}

	private String replaceSummary(String str)
	{
		if(str == null || str.length() <= 0) {
			return "";
		}
		str	= str.replaceAll("^．．．","");
		str	= str.replaceAll("．{3,}","。");
		if(str.matches("。$")) {
			str	= str + "。";
		}
		str	= str.replaceAll("[　| ]","");
		str	= str.replaceAll("[。！？．，：｜＊−・]+?、","。");
		str	= str.replaceAll("、+","、");
		str	= str.replaceAll("[、！？．，：｜＊−・]+?。","。");
		str	= str.replaceAll("([！？])","$1。");
		str	= str.replaceAll("。+/","。");

		return str;
	}

	/**
	 * 助数辞情報を取得する.
	 * 
	 * @return
	 */
//	private HashMap<String,Integer> getJosuji()
//	{
//		if(josujiList == null) {
//			return null;
//		}
//
//		HashMap<String,Integer>	map	= new HashMap<String,Integer>();
//		for(String str : josujiList) {
//			map.put(str, 1);
//		}
//		return map;
//	}

/*
	private static List<WebSearchResult> sortDocumentList(HashMap<WebSearchResult,Double> map)
	{
		if(map == null || map.size() <= 0) {
			return null;
		}

		List<Map.Entry<WebSearchResult,Double>> entries = new ArrayList<Map.Entry<WebSearchResult,Double>>(map.entrySet());
		Collections.sort(entries, new Comparator<Map.Entry<WebSearchResult,Double>>() {
			public int compare(Map.Entry<WebSearchResult,Double> o1, Map.Entry<WebSearchResult,Double> o2){
				Map.Entry<WebSearchResult,Double> e1 = o1;
				Map.Entry<WebSearchResult,Double> e2 = o2;
				return ((Double)e1.getValue()).compareTo((Double)e2.getValue());
			}
		});

		List<WebSearchResult>	tmpList	= new ArrayList<WebSearchResult>();
		for(Map.Entry<WebSearchResult,Double> entry : entries) {
			tmpList.add(entry.getKey());
		}
		return tmpList;
	}
*/
}
