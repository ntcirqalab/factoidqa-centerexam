package org.kachako.components.analysis_engine;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.TOP;
import org.apache.uima.resource.ResourceInitializationException;
import org.kachako.components.factoid_qa.manage.KeyWord;
import org.kachako.components.factoid_qa.module.GetKeyword04;
import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

import org.kachako.types.qa.KeyTerm;
import org.kachako.types.qa.Question;

/**
 * 質問文を解析します.
 * 
 * @author JSA Saitou
 *
 */
public class QueryAnalysis extends JCasAnnotator_ImplBase
{
	// 質問文が格納されているView(Sofa)名
	private final String PARAM_QUESTION_VIEW_NAME	= "QuestionViewName";

	// 質問文が格納されているView(Sofa)名
	private  String questionViewName		= null;

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		super.destroy();
	}

	@Override
	public void initialize(UimaContext aContext) throws ResourceInitializationException
	{
		super.initialize(aContext);

		// 質問文が格納されているView名の取得
		questionViewName = (String)aContext.getConfigParameterValue(PARAM_QUESTION_VIEW_NAME);
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException
	{
		System.out.println("--------QueryAnalysis start---------");

		try {
			JCas questionViewCas	=  jCas.getView(questionViewName);

			Iterator<TOP> questionFSIterator = questionViewCas.getJFSIndexRepository().getAllIndexedFS(Question.type);
			String	questionText	= null;
			if(questionFSIterator.hasNext()) {
				Question	questionFS	= (Question)questionFSIterator.next();
				questionText	= questionFS.getCoveredText();
System.out.println("questionText=["+questionText+"]");
			}

			// 形態素解析結果取得
			Iterator<TOP> morphemeFSIterator = questionViewCas.getJFSIndexRepository().getAllIndexedFS(Morpheme.type);

			List<Morpheme> morphemeList = new ArrayList<>();
			while(morphemeFSIterator.hasNext()) {
				morphemeList.add((Morpheme)morphemeFSIterator.next());
			}

			// キーワード取得
			List<KeyWord> keyWordList	= GetKeyword04.getKeyword(morphemeList);
			for(KeyWord keyWord : keyWordList) {
				Morpheme morpheme 	= morphemeList.get(keyWord.getLocation());
System.out.println("keyword="+keyWord.getWord());
				// キーワードをKeyTermに設定
				KeyTerm keyTerm = new KeyTerm(questionViewCas);
				keyTerm.setBegin(morpheme.getBegin());
				keyTerm.setEnd(morpheme.getEnd());
				keyTerm.setScore(1.0);
				keyTerm.addToIndexes();	
			}
		} catch(CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}

		return;
	}


	//----------------------------------------------------------------
	// private methods
	//----------------------------------------------------------------
/*
	private LinkedHashSet<String> getKeyword(List<Morpheme> morphemeList)
	{
		LinkedHashSet<String> keyWordHs	= new LinkedHashSet<>();

		// キーワード取得
		List<KeyWord> keyWordList	= GetKeyword04.getKeyword(morphemeList);
		if(keyWordList == null) {
			return keyWordHs;
		}

		for(KeyWord keyword : keyWordList) {
//System.out.println("keyword="+keyword.getWord());
			if(!keyWordHs.contains(keyword.getWord())) {
				keyWordHs.add(keyword.getWord());
			}
		}
		return keyWordHs;
	}
*/	
}
