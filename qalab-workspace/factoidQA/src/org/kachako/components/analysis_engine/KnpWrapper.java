package org.kachako.components.analysis_engine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.kachako.components.utils.AbstractStdioWrapper;
import org.apache.uima.UimaContext;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.StringArray;
import org.apache.uima.resource.ResourceInitializationException;
import org.kachako.types.japanese.syntactic.KnpSegment;
import org.kachako.types.japanese.syntactic.KyotoDependency;
import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

/**
 * 形態素解析システムJumanの解析結果から、構文・格解析エンジンKNPを実行しCasに結果を格納する.
 * 
 * @author JSA Saitou
 *
 */
public class KnpWrapper  extends AbstractStdioWrapper
{
	// 正規表現のパターン
	private static final String SENTENCE_START_PATTERN = "^# S-ID:(\\d+).*$";
	private static final String SEGMENT_PATTERN = "^\\* (-?\\d+)(\\w) (.*)$";
	private static final String NOT_MORPHEME_PATTERN = "^([#\\+\\*]|EOS).*$";

	// 区切り文字
	private static final String ENTRY_SEPARATOR = "\\s";

	// JUMAN実行ファイルパス
	private final String PARAM_JUMAN_EXEC_FILE	= "JumanExecFile";

	// KNP実行ファイルパス
	private final String PARAM_KNP_EXEC_FILE	= "KnpExecFile";

	// 文字エンコード
	private final String PARAM_ENCODE		= "Encode";
	
	// 文分割モード
	private final String PARAM_SPLIT_SENTENCE = "SplitSentence";
	

	private  String jumanExecFile	= null;
	private  String knpExecFile	= null;
	private  String encode		= null;

	private int beginOfMorpheme	= 0;
	private String morphemeSurfaceForm	= "";
	private int knpTargetId	= 0;
	private String knpLabelString	= null;
	private int beginOfSegment	= 0;
	private int endOfSegment		= 0;
	private List<KnpSegment> segmentArrayList	= null;
	private StringArray strArrayKnpSegmet	= null;

	private HashMap<KnpSegment, Integer> knpSegmentToTargetIdMap = null;
	private HashMap<Integer,String> IdToLabelMap = null;

	
	private boolean splitSentenceFlag = false;	// 入力テキストを句点で分割する 	
	private int currentSentenceId;
	private List<Integer> sentenceBegins;
		
	
	public void initialize(UimaContext aContext) throws ResourceInitializationException
	{
		super.initialize(aContext);

System.out.println("--------KnpWrapper initialize---------");

		// JUMAN実行ファイルパス取得
		jumanExecFile = (String)aContext.getConfigParameterValue(PARAM_JUMAN_EXEC_FILE);

		// KNP実行ファイルパス取得
		knpExecFile = (String)aContext.getConfigParameterValue(PARAM_KNP_EXEC_FILE);

		// 文字エンコード取得
		encode = (String)aContext.getConfigParameterValue(PARAM_ENCODE);
		
		// 文分割モード
		if (aContext.getConfigParameterValue(PARAM_SPLIT_SENTENCE) != null)
			splitSentenceFlag = (Boolean)aContext.getConfigParameterValue(PARAM_SPLIT_SENTENCE);
		
		sentenceBegins = new ArrayList<Integer>();
	}

	@Override
	public String[] getCommand(String os)
	{
		List<String> commands	= new ArrayList<String>();

		String	commandStr	= jumanExecFile + " -e | " + knpExecFile + " -tab";

		if(os.contains("Windows")) {
			// Windowsの場合
			commands.add("cmd");
			commands.add("/c");
			commands.add(commandStr);
		}
		else {
			commands.add("/bin/sh");
			commands.add("-c");
			commands.add(commandStr);
		}

		return commands.toArray(new String[0]);
	}

	@Override
	public String getStdinEncodingName() {
		return encode;
	}

	@Override
	public String getStdoutEncodingName() {
		return encode;
	}

	@Override
	public boolean redirectErrorStream() {
		// TODO 自動生成されたメソッド・スタブ
		return false;
	}

	@Override
	public void recoverProcess() {
		// TODO 自動生成されたメソッド・スタブ
	}

	@Override
	public boolean readLineAtInitialization(String line) {
		// TODO 自動生成されたメソッド・スタブ
		return true;
	}

	@Override
	public boolean readLineAndUpdateCas(String line, JCas jCas) {
		if (!splitSentenceFlag) {
		
			// Morpheme生成
			setMorpheme(line, jCas, -1);

			// KnpSegmentとKyotoDependencies生成
			setSegments(line, jCas, -1);

			if(line.equals("EOS")) {
				beginOfMorpheme	= 0;
				morphemeSurfaceForm	= "";
				beginOfSegment	= 0;
				endOfSegment		= 0;
				segmentArrayList	= null;
				strArrayKnpSegmet	= null;

				if(knpSegmentToTargetIdMap != null) {
					knpSegmentToTargetIdMap.clear();
				}
				knpSegmentToTargetIdMap	= null;
				if(IdToLabelMap != null) {
					IdToLabelMap.clear();
				}
				IdToLabelMap	= null;
				return true;
			}
			return false;
		}
		
		// 文分割モード
		if (line == null) 
			return true;

		int beginOffset = sentenceBegins.get(currentSentenceId);
		setMorpheme(line, jCas, beginOffset);		// Morpheme生成
		// KnpSegmentとKyotoDependencies生成
		setSegments(line, jCas, beginOffset);
		
		if (line.contains("EOS")) {
			beginOfMorpheme	= 0;
			morphemeSurfaceForm	= "";
			beginOfSegment	= 0;
			endOfSegment		= 0;
			segmentArrayList	= null;
			strArrayKnpSegmet	= null;

			if(knpSegmentToTargetIdMap != null)
				knpSegmentToTargetIdMap.clear();
			
			knpSegmentToTargetIdMap	= null;
			if(IdToLabelMap != null)
				IdToLabelMap.clear();
	
			IdToLabelMap	= null;
			currentSentenceId++;

			if (currentSentenceId >= sentenceBegins.size()) {
				return true;
			}
			
		}
		return false;
	}

/*	
	@Override
	public String convertCasToTextLines(JCas jCas)
	{
		String	text	= jCas.getDocumentText();
//System.out.println("["+text+"]");
		String lineSep	= System.lineSeparator();
		
		if (text == null || text.length() == 0)
			return lineSep;
		Pattern pattern	= Pattern.compile(lineSep+"\\z");
		Matcher matcher	= pattern.matcher(text);
		if(!matcher.find()) {
			text	= text + lineSep;
		}
		return text;
	}
 */	

	private int trimLeftCount(String value){
		
		if (value == null || value.equals("")) {
		    return 0;
		}
		int pos = 0;
		for (int i = 0; i < value.length(); i++) {
		    char c = value.charAt(i);
			if (c > '\u0020') {
		        break;
		    }
		    pos = i + 1;
		}
		return pos;
	}
	
	@Override
	public String convertCasToTextLines(JCas jCas) {

//		System.out.println("knpWrapper:"+jCas.getViewName());
		String texts = jCas.getDocumentText();
		String lineSep	= System.lineSeparator();
		if (texts == null || texts.length() == 0)
			return lineSep;
		
		if (splitSentenceFlag) {
			currentSentenceId = 0;
			StringBuffer sb = new StringBuffer();
			sentenceBegins.clear();
			
			// 文分割の記号は原文テキスト位置計算のため必要 
			int begin = 0;
			int end = 0;
			
			Pattern pattern = Pattern.compile(".+?[。！？]");
			Matcher matcher = pattern.matcher(texts);
			while (matcher.find()) {
				begin = matcher.start();
				end = matcher.end();
				String onesent = texts.substring(begin, end);
				int leftCount = trimLeftCount(onesent);
				onesent = onesent.trim();	// 前後の空白を取り除く
				
				if (onesent.length() > 0) {	
					sb.append(onesent);
					sb.append(lineSep);
					sentenceBegins.add(begin+leftCount);
				}
			}
			if (end < texts.length()) {
				String onesent = texts.substring(end);
				int leftCount = trimLeftCount(onesent);
				onesent = onesent.trim();	// 前後の空白を取り除く
				
				if (onesent.length() > 0) {	
					sb.append(onesent);
					sb.append(lineSep);
					sentenceBegins.add(begin+leftCount);
				}
			}			
			texts = sb.toString();
		}
		Pattern pattern	= Pattern.compile(lineSep+"\\z");
		Matcher matcher	= pattern.matcher(texts);
		if(!matcher.find()) {
			texts = texts + lineSep;
		}
		return texts;
	}
	
	@Override
	public void destroy() {
		super.destroy();
	}

	private void setMorpheme(String line, JCas jCas, int beginOffset) {
		if(Pattern.matches(SENTENCE_START_PATTERN, line)) 
			return;

		if(Pattern.matches(SEGMENT_PATTERN, line)) 
			return;

		if(line.equals("EOS")) 
			return;

		if(!Pattern.matches(NOT_MORPHEME_PATTERN, line)) {
			String[] str = line.split(ENTRY_SEPARATOR);

			String[] allReadings	= { str[1] };
			StringArray strArray = new StringArray(jCas, allReadings.length);
			strArray.copyFromArray(allReadings, 0, 0, allReadings.length);
	
			Morpheme morpheme = new Morpheme(jCas);
			morpheme.setSurfaceForm(str[0]);
			morpheme.setReadings(strArray);
			morpheme.setBaseForm(str[2]);
			morpheme.setPos(str[3]);
			morpheme.setDetailedPos(str[5]);
			if (beginOffset < 0) {
				morpheme.setBegin(beginOfMorpheme);
				morpheme.setEnd(beginOfMorpheme + str[0].length());
			} else {	// 複数文モード
				morpheme.setBegin(beginOffset+beginOfMorpheme);
				morpheme.setEnd(beginOffset+beginOfMorpheme+str[0].length());
			}
			morpheme.setConjugateType(str[7]);
			morpheme.setConjugateForm(str[9]);
			morpheme.addToIndexes();
			beginOfMorpheme += str[0].length();
			morphemeSurfaceForm	+= str[0];
		}
		return;
	}

	// KnpSegmentとKyotoDependencies生成
	private void setSegments(String line, JCas jCas, int beginOffset) {
		
		if(Pattern.matches(SENTENCE_START_PATTERN, line))
			return;

		if(!Pattern.matches(NOT_MORPHEME_PATTERN, line)) 
			return;

		Pattern segmentPattern = Pattern.compile(SEGMENT_PATTERN);
		Matcher segmentMatcher = segmentPattern.matcher(line);

		if(segmentMatcher.find() || line.equals("EOS")) {
			KnpSegment segment	= null;
			if(morphemeSurfaceForm.length() > 0) {
				// Segmentを生成し、ArrayListに格納
				endOfSegment += morphemeSurfaceForm.length();
				if (beginOffset < 0) 
					segment = new KnpSegment(jCas, beginOfSegment, endOfSegment);
				else	// 複数文モード
					segment = new KnpSegment(jCas, beginOffset+beginOfSegment, beginOffset+endOfSegment);
				segment.setKnpBunsetsuFeatures(strArrayKnpSegmet);
				segment.addToIndexes();

				beginOfSegment = endOfSegment;
				if(segmentArrayList == null) {
					segmentArrayList	= new ArrayList<KnpSegment>();
				}
				segmentArrayList.add(segment);
				if(knpSegmentToTargetIdMap == null) {
					knpSegmentToTargetIdMap	= new  HashMap<KnpSegment, Integer>();
				}
				knpSegmentToTargetIdMap.put(segment,knpTargetId);
				if(IdToLabelMap == null) {
					IdToLabelMap	= new  HashMap<Integer,String>();
				}
				IdToLabelMap.put(knpTargetId, knpLabelString);

				morphemeSurfaceForm = "";
			}

			if(!line.equals("EOS")) {
				// target文節ID
				knpTargetId	= Integer.parseInt(segmentMatcher.group(1));
				// targetのラベル（D、P、A）を取得する
				knpLabelString	= segmentMatcher.group(2);
				String strKnpSegment = segmentMatcher.group(3);
//System.out.println(strKnpSegment);
				strArrayKnpSegmet	= null;
				String[] knpSegmentList = strKnpSegment.split("<");
				if(knpSegmentList.length > 0) {
					ArrayList<String>	tempList	= new ArrayList<String>();
					for(int index = 0; index < knpSegmentList.length; index++) {
//System.out.println(knpSegmentList[index]);
						if(knpSegmentList[index].length() <= 0) {
							continue;
						}

						String	str	= knpSegmentList[index].substring(0, knpSegmentList[index].length() -1);
						tempList.add(str);
					}
					knpSegmentList	= (String[])tempList.toArray(new String[tempList.size()]);
					strArrayKnpSegmet = new StringArray(jCas, knpSegmentList.length);
					strArrayKnpSegmet.copyFromArray(knpSegmentList, 0, 0, knpSegmentList.length);
				}
			}
			else {
				if(segmentArrayList != null) {
					for(int i = 0; i < segmentArrayList.size(); i++) {
						KnpSegment knpSegment = segmentArrayList.get(i);
						int targetId	= knpSegmentToTargetIdMap.get(knpSegment);
						String labelString	= IdToLabelMap.get(targetId);
	
						KnpSegment targetSegment = null;
						if(targetId != -1) {
							targetSegment	= segmentArrayList.get(targetId);
						}
						int beginOfSegment = knpSegment.getBegin();
						int endOfSegment = knpSegment.getEnd();
	
						// KyotoDependencies生成
						KyotoDependency dependency = new KyotoDependency(jCas, beginOfSegment, endOfSegment);
						//dependency.setLabel(label);
						dependency.setSource(knpSegment);
						if(targetSegment != null) {
							dependency.setTarget(targetSegment);
						}
						dependency.setLabel(labelString);
						dependency.addToIndexes();
					}
				}
			}
		}
		return;
	}
}
