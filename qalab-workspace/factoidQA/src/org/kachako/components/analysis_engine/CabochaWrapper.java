package org.kachako.components.analysis_engine;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.kachako.components.utils.AbstractStdioWrapper;

import org.apache.uima.UimaContext;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.kachako.share.regex.RegexCache;
import org.kachako.types.japanese.semantic.NAISTNamedEntity;

/**
 * Cabocha(固有表現あり)の実行結果をCasに格納します.
 * 
 * @author JSA Saitou
 *
 */
public class CabochaWrapper  extends AbstractStdioWrapper
{
	// 実行ファイルパス
	private final String PARAM_EXEC_FILE	= "CabochaExecFile";

	// 文字エンコード
	private final String PARAM_ENCODE		= "Encode";

	// 正規表現パターン
	private static final String NAMED_ENTITY_PATTERN = "^[BI]-(.+)$";
	private static final String B_NAMED_ENTITY_PATTERN = "^B-(.+)$";
	private static final String I_NAMED_ENTITY_PATTERN = "^I-(.+)$";

	private String execFile	= null;
	private String encode	= null;

	private final RegexCache regexCache		= new RegexCache();

	private int beginIndex	= 0;	
	private NAISTNamedEntity lastNamedEntity = null;

	public void initialize(UimaContext aContext) throws ResourceInitializationException
	{
		super.initialize(aContext);

System.out.println("--------CabochaWrapper initialize---------");

		// 実行ファイルパス取得
		execFile = (String)aContext.getConfigParameterValue(PARAM_EXEC_FILE);

		// 文字エンコード取得
		encode = (String)aContext.getConfigParameterValue(PARAM_ENCODE);
	}

	@Override
	public String[] getCommand(String os) {
		String[] commands	= {execFile, "-f1", "-n1"};
		return commands;
	}

	@Override
	public String getStdinEncodingName() {
		return encode;
	}

	@Override
	public String getStdoutEncodingName() {
		return encode;
	}

	@Override
	public boolean redirectErrorStream() {
		// TODO 自動生成されたメソッド・スタブ
		return false;
	}

	@Override
	public void recoverProcess() {
		// TODO 自動生成されたメソッド・スタブ
	}

	@Override
	public boolean readLineAtInitialization(String line) {
		// TODO 自動生成されたメソッド・スタブ
		return true;
	}

	@Override
	public boolean readLineAndUpdateCas(String line, JCas jCas)
	{
		if(line.equals("EOS")) {
			beginIndex	= 0;
			lastNamedEntity	= null;
			return true;
		}

		String[] str = line.split("\t");
		if(str != null && str.length == 3) {
			String surfaceForm	= str[0];	// 表層
			//String features	= str[1]; // 形態素解析情報
			String namedEntityString	= str[2];	// 固有表現

			int endIndex	= beginIndex + surfaceForm.length();
			setNamedEntity(namedEntityString, beginIndex, endIndex, jCas);
			beginIndex += surfaceForm.length();
		}
		return false;
	}

	@Override
	public String convertCasToTextLines(JCas jCas)
	{
		String	text	= jCas.getDocumentText();
		
		if (text == null || text.length() == 0)
			return "";

		String lineSep	= System.lineSeparator();
		Pattern pattern	= Pattern.compile(lineSep+"\\z");
		Matcher matcher	= pattern.matcher(text);
		if(!matcher.find()) {
			text	= text + lineSep;
		}
		return text;
	}

	// NamedEntityを生成
	private void setNamedEntity(String namedEntityString, int beginIndex, int endIndex, JCas jCas)
	{
		if (isBNamedEntity(namedEntityString)) {
			// B-ooooooの場合は、新しいNamedEntityを生成する
			String namedEntityType = getNamedEntityType(namedEntityString);

			lastNamedEntity = new NAISTNamedEntity(jCas, beginIndex, endIndex);
			lastNamedEntity.setNamedEntityType(namedEntityType);
			lastNamedEntity.addToIndexes();
		}
		else if (isINamedEntity(namedEntityString)) {
			// I-ooooooの場合は以前のNamedEntityの続き
			if (lastNamedEntity == null) {
				// 直前のMorphemeにB-ooooooまたはI-ooooooがあるはずだが、もしnullなら新しく生成する
				String namedEntityType = getNamedEntityType(namedEntityString);
				lastNamedEntity = new NAISTNamedEntity(jCas, beginIndex, endIndex);
				lastNamedEntity.setNamedEntityType(namedEntityType);
				lastNamedEntity.addToIndexes();
			}
			else {
				lastNamedEntity.setEnd(endIndex);
			}
		}
		else if (isONamedEntity(namedEntityString)) {
			lastNamedEntity = null;
		}
	}

	// NamedEntity（B-DATE、B-LOCATIONなど）の判別
	private boolean isBNamedEntity(String line) {
		return regexCache.matches(B_NAMED_ENTITY_PATTERN, line);
	}

	// NamedEntity（I-DATE、I-LOCATIONなど）の判別
	private boolean isINamedEntity(String line) {
		return regexCache.matches(I_NAMED_ENTITY_PATTERN, line);
	}

	private boolean isONamedEntity(String line) {
		return line.equals("O");
	}

	private String getNamedEntityType(String line) {
		String namedEntityType = "";
		if(regexCache.find(NAMED_ENTITY_PATTERN, line)) {
			namedEntityType	= regexCache.lastMatched(1);
		}
		return namedEntityType;
	}
}
