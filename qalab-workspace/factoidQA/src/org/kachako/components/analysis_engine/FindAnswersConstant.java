package org.kachako.components.analysis_engine;

/**
 * 解抽出のための定数.
 * 
 * @author saitou
 *
 */
public class FindAnswersConstant
{
	public static int KEYEXP		= 0;	// キーワード拡張(1:yes 0:no) 良くないっぽい
	public static int NUMEXT		= 1;	// 数量表現抽出器の使用(1:yes 0:no)
	public static int KEYWGT		= 0;	// キーワードの重み情報の使用(1:yes 0:no)
	public static int KNPMAX		= 103;	// KNPに入力可能な形態素数の最大
	public static int MINFOFLAG		= 0;	// 形態素スコアを出力する(1:yes 0:no)

	public static double DEFAULT_PARA_RELATION	= 1;	// 構文距離スコア（最大値）
	public static double DEFAULT_PARA_WKAKU		= 0.20;	// 疑問詞、解候補の格一致（＊キーワード重要度の総和、最大値）
	public static double DEFAULT_PARA_KAKU		= 0.5;	// キーワードの格一致（最大値）
	public static double DEFAULT_PARA_KARAMADE	= 0.8;	// カラ格、マデ格以外（乗算）
	public static double DEFAULT_PARA_KAKKO		= 0.9;	// 括弧内（＊キーワード重要度の総和、最大値）
	public static double DEFAULT_PARA_BTOK		= 1;
	public static double DEFAULT_PARA_NUM		= 0.6;	// 数量表現に与える最低点の比率
	public static double DEFAULT_PARA_BORDER	= 0.80;	// 単純な枝刈りまたはビームサーチのときの閾値

	// スコアの倍率
	public static double BIGRAM		= 0.2;	// キーワード重要度の総和
	public static double KEITAISO	= 4;	// 対象キーワードの重要度
	public static double KOUBUN		= 11;	// 対象キーワードの重要度
	public static double NE			= 4.5;	// キーワード重要度の総和
	public static double NUM		= 9.5;	// キーワード重要度の総和

	// ストップワード（解候補としない語、JUMANの出力の形式で記述）
	public static String DEFAULT_PARA_S_PART = "(動|副|助動?|形容|接続|指示|判定)詞|接頭辞|接尾辞\\s\\S+\\s(動|形容)詞性|名詞\\s\\S+\\s(副詞的|形式)|(特殊\\s\\S+\\s(?!記号))";
	public static String DEFAULT_PARA_S_WORD = "もと|[！？・]";
}
