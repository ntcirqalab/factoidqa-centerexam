package org.kachako.components.analysis_engine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.StringArray;
import org.apache.uima.jcas.cas.TOP;
import org.apache.uima.resource.ResourceInitializationException;
import org.kachako.components.factoid_qa.manage.AnswerParam;
import org.kachako.components.factoid_qa.manage.KeyWord;
import org.kachako.components.factoid_qa.manage.KnpBnstHash;
import org.kachako.components.factoid_qa.manage.LineInfo;
import org.kachako.components.factoid_qa.manage.QueryInfo;
import org.kachako.components.factoid_qa.manage.QueryKeywordInfo;
import org.kachako.components.factoid_qa.manage.RequireInfo;
import org.kachako.components.factoid_qa.manage.ScoreInfo;
import org.kachako.components.factoid_qa.module.CwqCalScr05;
import org.kachako.components.factoid_qa.module.CwqCalScrNum01;
import org.kachako.components.factoid_qa.module.CwqGet2gram01;
import org.kachako.components.factoid_qa.module.CwqGetRelation04;
import org.kachako.components.factoid_qa.module.CwqMakeAns04;
import org.kachako.components.factoid_qa.module.GetKeyword04;
import org.kachako.components.factoid_qa.module.GetRequireInfo;
import org.kachako.components.factoid_qa.module.Knp2hash01;
import org.kachako.components.factoid_qa.module.PicknumQa01;
import org.kachako.components.factoid_qa.module.TrLoc01;
import org.kachako.share.regex.RegexCache;
import org.kachako.types.qa.AnswerCandidate;
import org.kachako.types.qa.AnswerType;
import org.kachako.types.qa.KeyTerm;
import org.kachako.types.qa.Question;
import org.kachako.types.qa.ViewName;
import org.kachako.ui.model.ssh.ResourceLoader;
import org.kachako.types.japanese.semantic.NAISTNamedEntity;
import org.kachako.types.japanese.syntactic.KnpSegment;
import org.kachako.types.japanese.syntactic.KyotoDependency;
import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

/**
 * 解を生成します.
 *
 * @author JSA Saitou
 *
 */
public class FindAnswers extends JCasAnnotator_ImplBase
{
	/* RS実行結果 VIEW名 PREFIX */
	//private static final String RS_RESULT_VIEW_NAME_PREFIX	= "RS";

	/* FindAnswer実行結果 VIEW名 PREFIX */
	private static final String AG_RESULT_VIEW_NAME_PREFIX	= "AG";

	// 質問文が格納されているView(Sofa)の名前
	private final String PARAM_QUESTION_VIEW_NAME	= "QuestionViewName";
	// 探索モード
	private final String PARAM_SEARCH_MODE	= "SearchMode";
	// 求める解の数
	private final String PARAM_ANS_NUM	= "AnsNum";
	// 求める解の上限値
	private final String PARAM_ANS_NUM_LIMIT	= "AnsNumLimit";
	// 日付解の補完を行うか
	private final String PARAM_DATE_FLAG	= "DateFlag";
	// 複数文処理を行うか
	private final String PARAM_MULTI_SENT	= "MultiSent";
	// 係り受け構造照合の「和」のスコアを使うか
	private final String PARAM_DEPMATCH_SUM	= "DepmatchSum";
	// 係り受け構造照合の「差」のスコアを使うか
	private final String PARAM_DEPMATCH_DIFF	= "DepmatchDiff";
	// 距離のスコアのみにするか
	private final String PARAM_DIST_ONLY	= "DistOnly";
	// 時間計測を行うか
	private final String PARAM_TIME_COUNT	= "TimeCount";

	// 質問文が格納されているView名
	private String questionViewName		= null;

	private int searchMode	= 3; // デフォルト（MAX,近似 時間計測なし）
	private  int ansNum	= 0;
	private  int ansNumLimit	= 0;
	private  boolean dateFlag	= false;
	private  boolean multiSentFlag	= false;
	private  boolean depmatchSumFlag	= false;
	private  boolean depmatchDiffFlag = false;
	private  boolean distOnlyFlag = false;
	private  boolean timeCountFlag	= false;
	
	// 属性の判定 決定リスト
	private String[] decisionList	= null;
	private String[] atrList		= null;

	// 助数辞
	private String[] josujiList		= null;

	// 人物パターン形式
	private String personSuffixIn	= null;
	private String personSuffixOut	= null;

	// EDR辞書読み込みMap
	private HashMap<String,String> cpcBySubMap	= null;
	private HashMap<String,String> cphByIdMap	= null;
	private HashMap<String,String> cphByNameMap	= null;

	// パラメータ情報
	private AnswerParam answerParam = null;

	private final RegexCache		regexCache		= new RegexCache();
	private final ResourceLoader	resourceLoader	= ResourceLoader.getInstance();
	
	private int ag_count = 1;

	@Override
	public void initialize(UimaContext aContext) throws ResourceInitializationException
	{
		super.initialize(aContext);

System.out.println("--------FindAnswers initialize---------");

		// 質問文が格納されているView名の取得
		questionViewName = (String)aContext.getConfigParameterValue(PARAM_QUESTION_VIEW_NAME);

		//--------------------------------
		// オプション取得
		//--------------------------------
		// 探索モードの取得
		searchMode		= (Integer)aContext.getConfigParameterValue(PARAM_SEARCH_MODE);
		// 求める解の数の取得
		ansNum	= (Integer)aContext.getConfigParameterValue(PARAM_ANS_NUM);
		// 求める解の上限値の取得
		ansNumLimit	= (Integer)aContext.getConfigParameterValue(PARAM_ANS_NUM_LIMIT);
		// 日付解の補完を行うかのフラグ取得
		dateFlag	= (Boolean)aContext.getConfigParameterValue(PARAM_DATE_FLAG);
		// 複数文処理を行うかのフラグ取得
		multiSentFlag	= (Boolean)aContext.getConfigParameterValue(PARAM_MULTI_SENT);
		// 係り受け構造照合の「和」のスコアを使うかのフラグ取得
		depmatchSumFlag	= (Boolean)aContext.getConfigParameterValue(PARAM_DEPMATCH_SUM);
		// 係り受け構造照合の「差」のスコアを使うかのフラグ取得
		depmatchDiffFlag	= (Boolean)aContext.getConfigParameterValue(PARAM_DEPMATCH_DIFF);
		// 距離のスコアのみにするかのフラグ取得
		distOnlyFlag	= (Boolean)aContext.getConfigParameterValue(PARAM_DIST_ONLY);
		// 時間計測を行うかのフラグ取得
		timeCountFlag	= (Boolean)aContext.getConfigParameterValue(PARAM_TIME_COUNT);
		//--------------------------------

		// 固定パラメータ初期設定
		setParam();

		// 助数辞の読み込み
		setJosujiList("org/kachako/components/analysis_engine/suffix.txt");
		// 決定リストの読み込み
		setDecisionList("org/kachako/components/analysis_engine/d_list3000.lst");
		setAtrList("org/kachako/components/analysis_engine/atr.dat");

		// 人物パターンの文字列の読み込み
		setPersonSuffixIn("org/kachako/components/analysis_engine/pat_PERSON_Suffix_in.txt");
		setPersonSuffixOut("org/kachako/components/analysis_engine/pat_PERSON_Suffix_out.txt");

		// 概念体系辞書読み込み
		setCpcDic("org/kachako/resources/CPC.DIC");
		// 概念見出し辞書読み込み
		setCphDic("org/kachako/resources/CPH.DIC");
		
		ag_count = 1;
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException
	{
System.out.println("--------FindAnswers start---------");

		try {
			Iterator<JCas> questionJCasIterator = jCas.getViewIterator(questionViewName);
			if(questionJCasIterator != null) {
				while(questionJCasIterator.hasNext()) {
					JCas	questionViewCas	= questionJCasIterator.next();
					// 質問文の取得
					Iterator<TOP> questionFSIterator = questionViewCas.getJFSIndexRepository().getAllIndexedFS(Question.type);
					String	questionText	= null;
					if(questionFSIterator.hasNext()) {
						Question	questionFS	= (Question)questionFSIterator.next();
						questionText	= questionFS.getCoveredText();
System.out.println(questionViewCas.getViewName()+" questionText=["+questionText+"]");
					}

					// TOPIC IDの取得
					String	topicId	= null;
					Iterator<TOP> sourceDocInfoFSIterator = questionViewCas.getJFSIndexRepository().getAllIndexedFS(SourceDocumentInformation.type);
					if(sourceDocInfoFSIterator.hasNext()) {
						// TOPIC_ID 取得
						topicId = ((SourceDocumentInformation) sourceDocInfoFSIterator.next()).getUri();
System.out.println("topic id="+topicId);
					}
					// キーワード取得
					List<KeyTerm> keytermList = getKeyTermList(questionViewCas);
					for(KeyTerm keyt : keytermList) {
						System.out.println("key term = "+keyt.getCoveredText());
					}

					// 形態素解析結果取得
					List<Morpheme> morphemeList	= getMorphemeList(questionViewCas,0,0);

					// KNP結果取得
					List<KyotoDependency> kyotoDependencyList	= getKyotoDependencyList(questionViewCas,0,0);
					if(kyotoDependencyList == null || kyotoDependencyList.size() <= 0) {
						System.err.println("KyotoDependency null.");
//						return;
						continue;
					}

					// KnpSegmentの取得
					List<KnpSegment> knpSegmentList = getKnpSegmentList(questionViewCas,0,0);
					if(knpSegmentList == null || knpSegmentList.size() <= 0) {
						System.err.println("KnpSegment null.");
//						return;
						continue;
					}

					// KnpSegmentの形態素情報取得
					Map<KnpSegment,List<Morpheme>>	knpInfoMap	= getKnpSegmentMorphemeInfo(knpSegmentList,morphemeList);
					if(knpInfoMap == null) {
System.out.println("knpInfoMap null");
//						return;
						continue;
					}

					HashMap<String,Integer> josujiMap	= getJosuji();
					// 質問文（クエリ）から抽出すべき情報を取得する。
					RequireInfo requireInfo	= GetRequireInfo.get(questionText, morphemeList, josujiMap);

					// AnswerTypeの設定
					AnswerType answerType = new AnswerType(questionViewCas);
					answerType.setAnswerType(requireInfo.getType());
					answerType.setScore(1.0);
					answerType.addToIndexes();

					Iterator<TOP> qaViewNameFSIterator = questionViewCas.getJFSIndexRepository().getAllIndexedFS(ViewName.type);
					if (!qaViewNameFSIterator.hasNext()) {
System.out.println("no ViewId in QA view");
						continue;
					}

					ViewName qaViewNameFS = (ViewName)qaViewNameFSIterator.next();
					StringArray qaFsChildren = qaViewNameFS.getChildren();
					if(qaFsChildren == null || qaFsChildren.size() <= 0) {
						// 子Viewなし(RSなし)
System.out.println("no RS View");
						continue;
					}
System.out.println("qaFsChildren size="+qaFsChildren.size());
					// Documentごとの一文のIDリスト
					List<String> lidList	= new ArrayList<String>();
					// 一文IDとLineInfoのMap
					LinkedHashMap<String, LineInfo> lineInfoMap	= new LinkedHashMap<String, LineInfo>();
					// 一文IDとScoreInfoのMap
					LinkedHashMap<String, ScoreInfo> scoreInfoMap	= new LinkedHashMap<String, ScoreInfo>();

					// RS結果を取得
					for (int index=0; index < qaFsChildren.size(); index++) {
						String qaViewName	= qaFsChildren.get(index);
						JCas	rsViewCas	= jCas.getView(qaViewName);

						Iterator<TOP> viewNameFSIterator = rsViewCas.getJFSIndexRepository().getAllIndexedFS(ViewName.type);
						if(!viewNameFSIterator.hasNext()) {
System.out.println("knpInfoMap null");
							continue;
						}

						StringArray fsRsChildren = null;
						while (viewNameFSIterator.hasNext()) {
							ViewName rsViewNameFS = (ViewName)viewNameFSIterator.next();
							fsRsChildren = rsViewNameFS.getChildren();
							if(fsRsChildren == null || fsRsChildren.size() <= 0) {
								System.out.println("no IX View");
								continue;
							} else {
								break;
							}
						}
						if(fsRsChildren == null) {
							System.out.println("no IX View");
							continue;
						}
System.out.println("fsRsChildren size="+fsRsChildren.size());						
						List<JCas> ixViewJCasList	= new ArrayList<>();
						for(int i=0; i < fsRsChildren.size(); i++) {
							String childrenViewName	= fsRsChildren.get(i);
							if(childrenViewName == null) {
								System.out.println("RS children is null");
								continue;
							}
							JCas ixViewCas = jCas.getView(childrenViewName);
							ixViewJCasList.add(ixViewCas);
						}
						// ソート
						Collections.sort(ixViewJCasList, new Comparator<JCas>(){
							public int compare(JCas x, JCas y){
								return x.getViewName().compareTo(y.getViewName());
							}
						});

						LinkedHashSet<String> lidSetTmp = setPassageInfo(ixViewJCasList, keytermList, requireInfo,lineInfoMap, scoreInfoMap);
						lidList.addAll(lidSetTmp);
					}

					String[] numLines	= null;
					if(regexCache.find("^(length|speed|area|weight|year|vol|money|num)", requireInfo.getType())) {
						// 数量表現抽出
						numLines	= PicknumQa01.picknum(decisionList, atrList, "i", "-999i",
								kyotoDependencyList, knpSegmentList, knpInfoMap, cpcBySubMap, cphByIdMap, cphByNameMap);
					}

					// 質問文解析の結果の情報を取得
					QueryInfo	queryInfo = getQueryInfo(questionText,morphemeList,knpSegmentList,kyotoDependencyList,knpInfoMap,requireInfo,numLines);
					if(queryInfo == null) {
						System.err.println("Error getQueryInfo().");
					}
					else {
						// パラメタ決定
						decideParam(queryInfo);

						// 初期スコアの導出
						scoreInfoMap	= CwqCalScr05.calScrC2gram(answerParam, queryInfo.get2gramList(), lidList, lineInfoMap, scoreInfoMap);
						scoreInfoMap	= CwqCalScr05.calScrCNum(answerParam, queryInfo, lidList, lineInfoMap, scoreInfoMap);
						if(answerParam.getMode() > 1) {    // 近似
							scoreInfoMap	= CwqCalScr05.calScrCKey(answerParam, queryInfo, lidList, lineInfoMap, scoreInfoMap);
							scoreInfoMap	= CwqCalScr05.calScrCConst(answerParam, queryInfo, lidList, lineInfoMap, scoreInfoMap,depmatchSumFlag, depmatchDiffFlag);
						}

						if(answerParam.getMode() != 0) {
							scoreInfoMap	= CwqCalScr05.getScore0(answerParam, queryInfo, lidList, scoreInfoMap);
						}

						// 初期リスト保存
						//List<String> tempLidList	= lidList;

						HashMap<String,String> id2AnsMap	= new HashMap<String,String>();	 // Key:行ID_形態素ID, Value:解表層
						HashMap<String,String> ans2IdMap	= new HashMap<String,String>(); // Key:解表層, Value:行ID_形態素ID
						HashMap<String,Double> rankMap		= new HashMap<String,Double>();  // Key:行ID_形態素ID, Value:最終スコア
						HashMap<String,Double> ansListMap	= new HashMap<String,Double>(); // Key:解表層, Value:最終スコア

						// 探索はじまり
						startSearch(lidList, lineInfoMap, scoreInfoMap, queryInfo,id2AnsMap,ans2IdMap,rankMap,ansListMap);

						if(id2AnsMap != null && id2AnsMap.size() > 0) {
							for(Map.Entry<String, String> entry : id2AnsMap.entrySet()) {
								if(rankMap.get(entry.getKey()) != ansListMap.get( entry.getValue())) {
									rankMap.remove(entry.getKey());
								}
							}
						} else {
System.out.println("id2AnsMap is null");
						}

System.out.println("rankMap.size="+rankMap.size());
						// 最終結果生成
						if(rankMap.size() > 0) {
							List<String> rankingList = sortRank(rankMap);

							StringArray agFsChildren = new StringArray(questionViewCas, rankingList.size());

							int num = 1;
							for(int  i = 0; i < rankingList.size(); i++) {
								String id	= rankingList.get(i);
								String ans = id2AnsMap.get(id);
								double rankScore	= rankMap.get(id);
								double ansScore	= ansListMap.get(ans);
								String[] ids	= id.split("__");
								String docId	= ids[0];
//								if(regexCache.find("^(.+?)_(\\d+?)$",docId)) {
//									docId	= regexCache.lastMatched(1);
//								}
System.out.println(ans + " " + docId+ "  "+ rankScore+" " + ansScore);
								// View生成
								String agViewName = String.format("%s.%d", AG_RESULT_VIEW_NAME_PREFIX, ag_count);
								JCas agViewCas = jCas.createView(agViewName);
								ag_count++;
								//---- View間の依存関係を設定 ----
								ViewName agChildViewNameFS	= new ViewName(agViewCas);
								agChildViewNameFS.addToIndexes();
								StringArray agFsParents = new StringArray(agViewCas,1);
								agFsParents.set(0, questionViewCas.getViewName());
								agFsChildren.set(i, agViewName);
								
								agChildViewNameFS.setParents(agFsParents);

								int	beginIndex	= 0;
								int	endIndex	= ans.length();
								AnswerCandidate	faAnswerCandidate	= new AnswerCandidate(agViewCas);
								faAnswerCandidate.setBegin(beginIndex);
								faAnswerCandidate.setEnd(endIndex);
								faAnswerCandidate.setScore(ansScore);
								faAnswerCandidate.setRank(num);
								faAnswerCandidate.setDocId(docId);
								faAnswerCandidate.addToIndexes();
								agViewCas.setDocumentText(ans+"\n");

								SourceDocumentInformation agSofaSourceDocumentInformationFS = new SourceDocumentInformation(agViewCas);
								agSofaSourceDocumentInformationFS.addToIndexes();
								agSofaSourceDocumentInformationFS.setUri(topicId);
								num++;
							}
							qaViewNameFS.setChildren(agFsChildren);
						}
					}
				}
			}
		} catch(CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}

		return;
	}


	//----------------------------------------------------------------
	// private methods
	//----------------------------------------------------------------

	/**
	 * パラメータ初期設定
	 */
	private void setParam()
	{
		answerParam	= new AnswerParam();
		answerParam.setRelation(FindAnswersConstant.DEFAULT_PARA_RELATION);
		answerParam.setWkaku(FindAnswersConstant.DEFAULT_PARA_WKAKU);
		answerParam.setKaku(FindAnswersConstant.DEFAULT_PARA_KAKU);
		answerParam.setKaramade(FindAnswersConstant.DEFAULT_PARA_KARAMADE);
		answerParam.setKakko(FindAnswersConstant.DEFAULT_PARA_KAKKO);
		answerParam.setBtok(FindAnswersConstant.DEFAULT_PARA_BTOK);
		answerParam.setNum(FindAnswersConstant.DEFAULT_PARA_NUM);
		answerParam.setBorder(FindAnswersConstant.DEFAULT_PARA_BORDER);
		answerParam.setSPart(FindAnswersConstant.DEFAULT_PARA_S_PART);
		answerParam.setSWord(FindAnswersConstant.DEFAULT_PARA_S_WORD);
		answerParam.setCharaScoreKeyword(FindAnswersConstant.KEITAISO);
		answerParam.setCharaScoreConst(FindAnswersConstant.KOUBUN);
		answerParam.setPreScoreConst(FindAnswersConstant.KOUBUN);
		answerParam.setRealScoreKeyword(FindAnswersConstant.KEITAISO);
		answerParam.setRealScoreConst(FindAnswersConstant.KOUBUN);
		answerParam.setAlp0(0);
		answerParam.setAlp1(0);
		answerParam.setAlp2(0);
		answerParam.setNeMode(0);
		answerParam.setNeType("");
		answerParam.setMode(searchMode <= 3 || searchMode >= -2 ? searchMode : 3);

		return;
	}

	/**
	 * 助数辞を読み込み、設定する.
	 *
	 * @param path
	 * @throws ResourceInitializationException
	 */
	private void setJosujiList(String path) throws ResourceInitializationException
	{
		// 助数辞の読み込み
		BufferedReader reader = null;
		try {
			File	decisionFile	= resourceLoader.getResourceFile(Paths.get(path));
			if(decisionFile != null) {
				reader	= new BufferedReader(new FileReader(decisionFile));
				List<String> list	= new ArrayList<>();
				String line	= null;
				while((line = reader.readLine()) != null) {
					list.add(line);
				}
				josujiList	= list.toArray(new String[0]);
				reader.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
System.out.println("josujiList error:"+e);
			throw new ResourceInitializationException();
		} catch(Exception e) {
			e.printStackTrace();
System.out.println(e);
			throw new ResourceInitializationException();
		} finally {
			if(reader != null) {
				try {
					reader.close();
				} catch(IOException e) {
					e.printStackTrace();
					throw new ResourceInitializationException();
				}
			}
		}
		return;
	}

	/**
	 * 決定リストを読み込み設定する.
	 *
	 * @param path
	 * @throws ResourceInitializationException
	 */
	private void setDecisionList(String path) throws ResourceInitializationException
	{
		// 決定リストの読み込み
		BufferedReader reader = null;
		try {
			File	decisionFile	= resourceLoader.getResourceFile(Paths.get(path));
			if(decisionFile != null) {
				reader	= new BufferedReader(new FileReader(decisionFile));
				List<String> list	= new ArrayList<>();
				String line	= null;
				while((line = reader.readLine()) != null) {
					list.add(line);
				}
				decisionList	= list.toArray(new String[0]);
				reader.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
System.out.println("decisionList error:"+e);
			throw new ResourceInitializationException();
		} catch(Exception e) {
			e.printStackTrace();
System.out.println(e);
			throw new ResourceInitializationException();
		} finally {
			if(reader != null) {
				try {
					reader.close();
				} catch(IOException e) {
					e.printStackTrace();
					throw new ResourceInitializationException();
				}
			}
		}
		return;
	}

	/**
	 * 決定リストを読み込み設定する.
	 *
	 * @param path
	 * @throws ResourceInitializationException
	 */
	private void setAtrList(String path) throws ResourceInitializationException
	{
		BufferedReader reader = null;
		try {
			File	atrFile	= resourceLoader.getResourceFile(Paths.get(path));
			if(atrFile != null) {
				reader	= new BufferedReader(new FileReader(atrFile));
				List<String> list	= new ArrayList<>();
				String line	= null;
				while((line = reader.readLine()) != null) {
					list.add(line);
				}
				atrList	= list.toArray(new String[0]);
				reader.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
System.out.println("atrList error:"+e);
			throw new ResourceInitializationException();
		} catch (Exception e) {
			e.printStackTrace();
System.out.println(e);
			throw new ResourceInitializationException();
		} finally {
			if(reader != null) {
				try {
					reader.close();
				} catch(IOException e) {
					e.printStackTrace();
					throw new ResourceInitializationException();
				}
			}
		}
		return;
	}

	/**
	 * 人物パターンの文字列を読み込み、設定する.
	 *
	 * @param path
	 * @throws ResourceInitializationException
	 */
	private void setPersonSuffixIn(String path) throws ResourceInitializationException
	{
		BufferedReader reader = null;
		try {
			File	suffixFile	= resourceLoader.getResourceFile(Paths.get(path));
			if(suffixFile != null) {
				reader	= new BufferedReader(new FileReader(suffixFile));
				List<String> list	= new ArrayList<>();
				String line	= null;
				while((line = reader.readLine()) != null) {
					list.add(line);
				}
				personSuffixIn	= list.toString();
				reader.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
System.out.println("personSuffixIn error:"+e);
			throw new ResourceInitializationException();
		} catch (Exception e) {
			e.printStackTrace();
System.out.println(e);
			throw new ResourceInitializationException();
		} finally {
			if(reader != null) {
				try {
					reader.close();
				} catch(IOException e) {
					e.printStackTrace();
					throw new ResourceInitializationException();
				}
			}
		}
		return;
	}

	/**
	 * 人物パターンの文字列を読み込み、設定する.
	 *
	 * @param path
	 * @throws ResourceInitializationException
	 */
	private void setPersonSuffixOut(String path) throws ResourceInitializationException
	{
		BufferedReader reader = null;
		try {
			File	suffixFile	= resourceLoader.getResourceFile(Paths.get(path));
			if(suffixFile != null) {
				reader	= new BufferedReader(new FileReader(suffixFile));
				List<String> list	= new ArrayList<>();
				String line	= null;
				while((line = reader.readLine()) != null) {
					list.add(line);
				}
				personSuffixOut	= list.toString();
				reader.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
System.out.println("personSuffixOut error:"+e);
			throw new ResourceInitializationException();
		} catch (Exception e) {
			e.printStackTrace();
System.out.println(e);
			throw new ResourceInitializationException();
		} finally {
			if(reader != null) {
				try {
					reader.close();
				} catch(IOException e) {
					e.printStackTrace();
					throw new ResourceInitializationException();
				}
			}
		}
		return;
	}

	/**
	 * 概念体系辞書を読み込み、設定する.
	 *
	 * @param path
	 * @throws ResourceInitializationException
	 */
	private void setCpcDic(String path) throws ResourceInitializationException
	{
		// 概念体系辞書読み込み
		BufferedReader reader = null;
		try {
			File	cpcFile	= resourceLoader.getResourceFile(Paths.get(path));
			if(cpcFile != null) {
				cpcBySubMap	= new HashMap<String,String>();
				reader	= new BufferedReader(new InputStreamReader(new FileInputStream(cpcFile),"EUC-JP"));
				String line	= null;
				while((line = reader.readLine()) != null) {
					//System.out.println(line);
					String[] item	= line.split("\t");
					if(item.length != 4) {
						continue;
					}
					if(cpcBySubMap.get(item[2]) != null) {
						String value	= cpcBySubMap.get(item[2]) + "|" + item[1];
						cpcBySubMap.put(item[2], value);
					}
					else {
						cpcBySubMap.put(item[2], item[1]);
					}
				}
				reader.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
System.out.println("CPC.DIC error:"+e);
			throw new ResourceInitializationException();
		} catch (Exception e) {
			e.printStackTrace();
System.out.println(e);
			throw new ResourceInitializationException();
		} finally {
			if(reader != null) {
				try {
					reader.close();
				} catch(IOException e) {
					e.printStackTrace();
					throw new ResourceInitializationException();
				}
			}
		}
		return;
	}

	/**
	 * 概念見出し辞書を読み込み、設定する.
	 *
	 * @param path
	 * @throws ResourceInitializationException
	 */
	private void setCphDic(String path) throws ResourceInitializationException
	{
		// 概念見出し辞書読み込み
		BufferedReader reader = null;
		try {
			File	cphFile	= resourceLoader.getResourceFile(Paths.get(path));
			if(cphFile != null) {
				cphByIdMap	= new HashMap<String,String>();
				cphByNameMap	= new HashMap<String,String>();
				reader	= new BufferedReader(new InputStreamReader(new FileInputStream(cphFile),"EUC-JP"));
				String line	= null;
				while((line = reader.readLine()) != null) {
//					System.out.println(line);
					String[] item	= line.split("\t");
					if(item.length != 7) {
						continue;
					}
					String name	= null;
					if(!item[3].equals("\"\"")) {
						name = item[3];
					} else if(!item[2].equals("\"\"")) {
						name = item[2];
					} else if(!item[5].equals("\"\"")) {
						name = item[5];
					} else if(!item[4].equals("\"\"")) {
						name = item[4];
					} else {
						name = "(無名)";
					}
					if(name.indexOf("\"") >= 0) {
						name	= name.replaceAll("[\"]", "");
					}
					if(name.indexOf(" ") >= 0) {
						name	= name.replaceAll("[ ]", "_");
					}

					if(cphByIdMap.get(item[1]) != null) {
						String value	= cphByIdMap.get(item[1]) + "|" + name;
						cphByIdMap.put(item[1], value);
					}
					else {
						cphByIdMap.put(item[1], name);
					}
					if(cphByNameMap.get(name) != null) {
						String value	= cphByNameMap.get(name) + "|" + item[1];
						cphByNameMap.put(name, value);
					}
					else {
						cphByNameMap.put(name, item[1]);
					}
				}
				reader.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
System.out.println("CPH.DIC error:"+e);
			throw new ResourceInitializationException();
		} catch (Exception e) {
			e.printStackTrace();
System.out.println(e);
			throw new ResourceInitializationException();
		} finally {
			if(reader != null) {
				try {
					reader.close();
				} catch(IOException e) {
					e.printStackTrace();
					throw new ResourceInitializationException();
				}
			}
		}
		return;
	}

	/**
	 * キーワードを取得する.
	 *
	 * @param jCas
	 * @return
	 */
	private List<KeyTerm> getKeyTermList(JCas jCas)
	{
		Iterator<TOP> keytermFSIterator = jCas.getJFSIndexRepository().getAllIndexedFS(KeyTerm.type);

		List<KeyTerm> keytermList = new ArrayList<>();
		while(keytermFSIterator.hasNext()) {
			keytermList.add((KeyTerm)keytermFSIterator.next());
		}

		if(keytermList != null) {
			// ソート
			Collections.sort(keytermList, new Comparator<KeyTerm>(){
				public int compare(KeyTerm x, KeyTerm y){
					return x.getBegin() - y.getBegin();
				}
			});

		}
		return keytermList;
	}

	/**
	 * 形態素解析結果情報を取得する.
	 *
	 * @param jCas
	 * @param begin
	 * @param end
	 * @return
	 */
	private List<Morpheme> getMorphemeList(JCas jCas, int begin, int end)
	{
		// 形態素解析結果取得
		Iterator<TOP> morphemeFSIterator = jCas.getJFSIndexRepository().getAllIndexedFS(Morpheme.type);

		List<Morpheme> morphemeList = new ArrayList<>();
		while(morphemeFSIterator.hasNext()) {
			Morpheme morpheme	= (Morpheme)morphemeFSIterator.next();
			if(end != 0) {
				if(morpheme.getBegin() >= begin && morpheme.getEnd() <= end) {
					morphemeList.add(morpheme);
				}
			}
			else {
				morphemeList.add(morpheme);
			}
		}
		if(morphemeList != null) {
			// ソート
			Collections.sort(morphemeList, new Comparator<Morpheme>(){
				public int compare(Morpheme x, Morpheme y){
					return x.getBegin() - y.getBegin();
				}
			});

		}
		return morphemeList;
	}

	/**
	 * KyotoDependency(係り受け情報)を取得する.
	 * 
	 * @param jCas
	 * @param begin
	 * @param end
	 * @return
	 */
	private List<KyotoDependency> getKyotoDependencyList(JCas jCas, int begin, int end)
	{
		Iterator<TOP> kyotoDependencyFSIterator = jCas.getJFSIndexRepository().getAllIndexedFS(KyotoDependency.type);

		List<KyotoDependency> kyotoDependencyList = new ArrayList<>();
		while(kyotoDependencyFSIterator.hasNext()) {
			KyotoDependency dependency = (KyotoDependency)kyotoDependencyFSIterator.next();
			if(end != 0) {
				if(dependency.getBegin() >= begin && dependency.getEnd() <= end) {
					kyotoDependencyList.add(dependency);
				}
			}
			else {
				kyotoDependencyList.add(dependency);
			}
		}

		if(kyotoDependencyList != null) {
			// ソート
			Collections.sort(kyotoDependencyList, new Comparator<KyotoDependency>(){
				public int compare(KyotoDependency x, KyotoDependency y){
					return x.getBegin() - y.getBegin();
				}
			});
		}

		return kyotoDependencyList;
	}

	/**
	 * KnpSegment(文節情報)を取得する.
	 * 
	 * @param jCas
	 * @param begin
	 * @param end
	 * @return
	 */
	private List<KnpSegment> getKnpSegmentList(JCas jCas, int begin, int end)
	{
		Iterator<TOP> knpSegmentFSIterator = jCas.getJFSIndexRepository().getAllIndexedFS(KnpSegment.type);

		List<KnpSegment> knpSegmentList = new ArrayList<>();
		while(knpSegmentFSIterator.hasNext()) {
			KnpSegment knpSegment = (KnpSegment)knpSegmentFSIterator.next();
			if(end != 0) {
				if(knpSegment.getBegin() >= begin && knpSegment.getEnd() <= end) {
					knpSegmentList.add(knpSegment);
				}
			}
			else {
				knpSegmentList.add(knpSegment);
			}

//System.out.println("knpSegment bunsetsu:"+knpSegment.getKnpBunsetsuFeatures());
		}

		if(knpSegmentList != null) {
			// ソート
			Collections.sort(knpSegmentList, new Comparator<KnpSegment>(){
				public int compare(KnpSegment x, KnpSegment y){
					return x.getBegin() - y.getBegin();
				}
			});
		}

		return knpSegmentList;
	}

	/**
	 * KnpSegmentに対する形態素情報を取得する.
	 *
	 * @param knpSegmentList
	 * @param morphemeList
	 * @return
	 */
	private Map<KnpSegment,List<Morpheme>> getKnpSegmentMorphemeInfo(List<KnpSegment> knpSegmentList ,List<Morpheme> morphemeList)
	{
		if(knpSegmentList == null || morphemeList == null) {
			return null;
		}

		Map<KnpSegment,List<Morpheme>>	segmentMorphemesMap	= new LinkedHashMap<KnpSegment,List<Morpheme>>();
		for(int i = 0; i < knpSegmentList.size(); i++) {
			KnpSegment	knpSegment	= knpSegmentList.get(i);
			int currentKnpSegmentBegin	= knpSegment.getBegin();
			int currentKnpSegmentEnd	= knpSegment.getEnd();

//System.out.println("knp="+knpSegment.getCoveredText());

			List<Morpheme> tmpMorphemes = new ArrayList<Morpheme>();
			for(int im = 0; im < morphemeList.size(); im++) {
				Morpheme	morpheme	= morphemeList.get(im);
				if(morpheme.getBegin() >= currentKnpSegmentBegin
							&& morpheme.getEnd() <= currentKnpSegmentEnd) {
//System.out.println("morpheme="+morpheme.getSurfaceForm());
					tmpMorphemes.add(morpheme);
				} else if(morpheme.getBegin() >= currentKnpSegmentEnd) {
					break;
				}
			}
			segmentMorphemesMap.put(knpSegment, tmpMorphemes);
		}

		return segmentMorphemesMap;
	}

	/**
	 * 形態素に対するNAISTNamedEntityを取得する.
	 *
	 * @param jCas
	 * @param morphemeList
	 * @return
	 */
	private List<String> getNamedEntityList(JCas jCas, List<Morpheme> morphemeList)
	{
		Iterator<TOP> neFSIterator = jCas.getJFSIndexRepository().getAllIndexedFS(NAISTNamedEntity.type);

//System.out.println("viewName="+jCas.getViewName());
		List<NAISTNamedEntity> namedEntityList	= new ArrayList<>();
		while(neFSIterator.hasNext()) {
			NAISTNamedEntity ne = (NAISTNamedEntity)neFSIterator.next();
			namedEntityList.add(ne);
		}

		if(namedEntityList == null || namedEntityList.size() <= 0) {
			return null;
		}

		// Jumanの形式に固有表現タグをつける
		List<String> neData	= new ArrayList<>();
		HashMap<Morpheme,String> morpheBioMap	= new HashMap<Morpheme,String>();

		for(int i = 0; i < namedEntityList.size(); i++) {
			NAISTNamedEntity ne = namedEntityList.get(i);
			setMorphemeFromBIO(morphemeList,ne,morpheBioMap);
		}
		for(int i = 0; i < morphemeList.size(); i++) {
			Morpheme	morpheme	= morphemeList.get(i);
			String neType	= "";
			if(morpheBioMap.containsKey(morpheme)) {
				neType	= morpheBioMap.get(morpheme);
			}
			String surfaceForm	= morpheme.getSurfaceForm();
			String read				= morpheme.getReadings(0);
			String baseForm	= morpheme.getBaseForm();
			String pos			= morpheme.getPos();
			String detailedPos	= morpheme.getDetailedPos();
			String conjugateType	= morpheme.getConjugateType();
			String conjugateForm = morpheme.getConjugateForm();

			StringBuilder buf = new StringBuilder();
			buf.append(surfaceForm).append(" ");
			buf.append(read).append(" ");
			buf.append(baseForm).append(" ");
			buf.append(pos).append(" ");
			buf.append("0").append(" ");
			buf.append(detailedPos).append(" ");
			buf.append("0").append(" ");
			buf.append(conjugateType).append(" ");
			buf.append("0").append(" ");
			buf.append(conjugateForm).append(" ");
			buf.append("0").append(" ");
			if(!neType.equals("")) {
				buf.append("<"+neType+">");
			}
			neData.add(buf.toString());
		}
/*
System.out.println("--------------------");
for(String str : neData) {
	System.out.println(str);
}
System.out.println("--------------------");
*/
		return neData;
	}


	/**
	 * 質問文解析の結果の情報を取得する.
	 *
	 * @param query
	 * @param morphemeList
	 * @param knpSegmentList
	 * @param kyotoDependencyList
	 * @param knpInfoMap
	 * @param requireInfo
	 * @param numLines
	 * @return
	 */
	private QueryInfo getQueryInfo(String query,List<Morpheme> morphemeList,
												List<KnpSegment> knpSegmentList,
												List<KyotoDependency> kyotoDependencyList,
												Map<KnpSegment,List<Morpheme>> knpInfoMap,
												RequireInfo requireInfo,String[] numLines)
	{
		if(requireInfo == null) {
			return null;
		}

System.out.println("type="+requireInfo.getType());
System.out.println("q_word="+requireInfo.getQword());
System.out.println("pattern1="+requireInfo.getPattern1());
System.out.println("pattern2="+requireInfo.getPattern2());

		QueryInfo	queryInfo	= new QueryInfo();

		queryInfo.setType(requireInfo.getType());

		// 質問文から文字列をバイグラムで取り出す。
		{
			String[] gramList	= CwqGet2gram01.get2gram(query);
			
			for (int i = 0 ; i < gramList.length; i++) {
				gramList[i] = gramList[i].replaceAll("\\?","\\\\?");
			}
			
			queryInfo.set2gramList(gramList);
		}

		// 質問文に対応するパタンを保存
		{
			queryInfo.setPattern1(requireInfo.getPattern1());
			if(regexCache.find("^date\\.",requireInfo.getType())) {
				queryInfo.setPattern2(requireInfo.getPattern2());
			}
			if(requireInfo.getPattern1() != null) {
				Pattern p = Pattern.compile("^\\(\\[\\^.+?\\]\\|\\^\\)");
				Matcher m = p.matcher(requireInfo.getPattern1());
				queryInfo.setPattern1(m.replaceAll(""));
			}
		}

		// 質問文の数量表現情報を整理
		{
			if(numLines != null && numLines.length > 0) {
				String num	= numLines[0].substring(0, numLines[0].length()-1);
				String[] numArray	= num.split(",");
				if(numArray.length >= 2 && numArray[1] != null && numArray[1].length() > 0) {
					if(regexCache.find("\\S+$", numArray[1])) {
						queryInfo.setObjectWord(regexCache.lastMatched());
					}
					queryInfo.setObjectSplt("");
					String	word	= queryInfo.getObjectWord();
					String[] array	= word.split(".");
					StringBuffer buf = new StringBuffer();
					for(String str : array) {
						buf.append(str).append("|");
					}
					String	splt	= buf.toString();
					if(splt.lastIndexOf("|") == 0) {
						splt	= splt.substring(0,splt.length()-1);
					}
					queryInfo.setObjectSplt(splt);
					queryInfo.setObjectLength(word.length() / 2);
				}
				if(numArray.length >= 3 && numArray[2] != null && numArray[2].length() > 0) {
					if(regexCache.find("\\S+$", numArray[2])) {
						queryInfo.setAttributeWord(regexCache.lastMatched());
					}

					queryInfo.setAttributeSplt("");
					String word	= queryInfo.getAttributeWord();
					String[] array	= word.split(".");
					StringBuffer buf = new StringBuffer();
					for(String str : array) {
						buf.append(str).append("|");
					}
					String	splt	= buf.toString();
					if(splt.lastIndexOf("|") == 0) {
						splt	= splt.substring(0,splt.length()-1);
					}
					queryInfo.setAttributeSplt(splt);
					queryInfo.setAttributeLength(word.length() / 2);
				}
			}
		}

		// キーワード保存
		{
			List<KeyWord> keyWordList	= GetKeyword04.getKeyword(morphemeList);
			List<QueryKeywordInfo> qKeywordList	= new ArrayList<QueryKeywordInfo>();
			for(KeyWord keyword : keyWordList) {
				QueryKeywordInfo	info	= new QueryKeywordInfo();
				info.setWord(keyword.getWord());
				info.setLocation(keyword.getLocation());
				info.setPart(keyword.getPart());

				// キーワードの文節、形態素位置を記録
				int[] numList	= TrLoc01.juman2knp(keyword.getLocation(), knpInfoMap);
				info.setBnstNo(numList[0]);
				info.setBnstMorphemeNo(numList[1]);

				// キーワード重み・・・重み付けなし固定
				//if(FindAnswersConstant.KEYWGT == 0) {
					info.setWeight(1.0);
				//}
				qKeywordList.add(info);
			}
			queryInfo.setKeyWordInfos(qKeywordList);
		}

		// 質問文から疑問詞＆キーワード関係を得る。
		{
			KnpBnstHash knpHash	= Knp2hash01.knp2Hash(knpSegmentList.size()-1, kyotoDependencyList, knpSegmentList, true);
			queryInfo.setKnpHash(knpHash);
			queryInfo	= CwqGetRelation04.getRelationQuery(queryInfo, kyotoDependencyList, knpSegmentList, knpInfoMap, query);
		}

		// 質問文タイプ設定
		{
			String neType	= neNeeds(requireInfo.getType());
			answerParam.setNeType(neType);
			if(regexCache.find("^(PERSON|ORGANIZATION|LOCATION)",neType)) {
				// 固有表現抽出器を利用
				answerParam.setNeMode(1);
			}
			else if(regexCache.find("^(length|speed|area|vol|year|weight|num|money)",neType))
			{
				// 数量表現抽出器を利用
				answerParam.setNeMode(2);
			}
			else {
				// rate|interval|period|date|time / other
				answerParam.setNeMode(0);
			}
		}

		return queryInfo;
	}

	/**
	 * 形態素解析結果にBIO情報を設定する.
	 *
	 * @param morphemeList
	 * @param ne
	 * @param morpheBioMap
	 */
	private void setMorphemeFromBIO(List<Morpheme> morphemeList, NAISTNamedEntity ne, HashMap<Morpheme,String> morpheBioMap)
	{
		if(ne == null) {
			return;
		}
//		String text	= ne.getCoveredText();
		String neType	= ne.getNamedEntityType();
		int neBegin	= ne.getBegin();
		int neEnd	= ne.getEnd();
//System.out.println("NAISTNamedEntity text:"+text+" type:"+neType);

		for(int i = 0; i < morphemeList.size(); i++) {
			Morpheme	morpheme	= morphemeList.get(i);
			if(morpheme.getBegin() >= neBegin && morpheme.getEnd() <= neEnd) {
				morpheBioMap.put(morpheme, neType);
			} else if (morpheme.getBegin() >= neEnd) {
				break;
			}
		}
		return;
	}

	/**
	 * パッセージの情報を設定する.
	 *
	 * @param ixViewJCasList
	 * @param keytermList
	 * @param requireInfo
	 * @param lineInfoMap
	 * @param scoreInfoMap
	 * @return
	 */
	private LinkedHashSet<String> setPassageInfo(List<JCas> ixViewJCasList, List<KeyTerm> keytermList,RequireInfo requireInfo,
													LinkedHashMap<String, LineInfo> lineInfoMap, LinkedHashMap<String, ScoreInfo> scoreInfoMap)
	{
		LinkedHashSet<String> lineIdSet = new LinkedHashSet<String>();
		LinkedHashMap<String, LineInfo> tempLineInfoMap	= lineInfoMap;
		LinkedHashMap<String, ScoreInfo> tempScoreInfoMap	= scoreInfoMap;

		String pattern1	= requireInfo.getPattern1();
		String pattern2	= requireInfo.getPattern2();

		for(int index=0; index < ixViewJCasList.size(); index++) {
			JCas ixViewCas	= ixViewJCasList.get(index);

			String lineTotal = "";
			String lid	= "";
			boolean flag	= false;
			Map<String,String>	preLidMap	= new HashMap<String,String>();

			int lineNo	= 0;
			int startLineNo	= 0;
			String docId	= null;

			// AnswerCandidate取得
			Iterator<TOP> answerCandidateFSIterator = ixViewCas.getJFSIndexRepository()
																				.getAllIndexedFS(AnswerCandidate.type);

			while(answerCandidateFSIterator.hasNext()) {
				AnswerCandidate	answerCandidateFS = (AnswerCandidate)answerCandidateFSIterator.next();
				String answerText	= answerCandidateFS.getCoveredText();
				String answerDocId	= answerCandidateFS.getDocId();
				if(answerText == null || answerDocId == null) {
					continue;
				}
				int answerBegin	= answerCandidateFS.getBegin();
				int answerEnd		= answerCandidateFS.getEnd();
				LineInfo	lineInfo	= new  LineInfo();

//System.out.println("ViewName="+ixViewCas.getViewName()+" answerText="+answerText+" docId="+answerDocId);

				// 形態素解析結果取得
				List<Morpheme> ixMmorphemeList = getMorphemeList(ixViewCas,answerBegin,answerEnd);

				// 形態素解析結果設定
				lineInfo.setMorphemeList(ixMmorphemeList);
				List<String> hyosoList	= new ArrayList<>();
				for(Morpheme morpheme : ixMmorphemeList) {
					hyosoList.add(morpheme.getSurfaceForm());
				}

				// 形態素解析結果の表層表現リスト設定
				lineInfo.setMorphemeHyosoList(hyosoList);

				// KNP結果取得
				List<KyotoDependency> kyotoDependencyList = getKyotoDependencyList(ixViewCas,answerBegin,answerEnd);
				lineInfo.setKyotoDependencyList(kyotoDependencyList);

				// KnpSegmentの取得
				List<KnpSegment> knpSegmentList = getKnpSegmentList(ixViewCas,answerBegin,answerEnd);
				lineInfo.setKnpSegmentList(knpSegmentList);

				// KnpSegmentの形態素情報取得
				Map<KnpSegment,List<Morpheme>>	knpInfoMap	= getKnpSegmentMorphemeInfo(knpSegmentList,ixMmorphemeList);
				lineInfo.setKnpInfoMap(knpInfoMap);

				// 形態素情報に固有表現タグ設定
				List<String> neDataList	= getNamedEntityList(ixViewCas,ixMmorphemeList);
				lineInfo.setNeDataList(neDataList);

				List<Integer> keyCountList	= getKeywordCount(answerText,keytermList);
				List<Integer> neCountList	= getNeCount(answerText, pattern1, pattern2);
				List<Integer> neTotal			= lineInfo.getNeTotal();
				List<Integer> newNeTotal		= new ArrayList<Integer>();
				if(neCountList != null && neCountList.size() >= 1) {
					for(int i = 0; i < neCountList.size(); i++) {
						int n = (neTotal != null && neTotal.get(i) != null ? neTotal.get(i) : 0) + neCountList.get(i);
						newNeTotal.add(n);
					}
				}

				if(flag) {
					// ２文目以降
					String preId	= lid;
					lineNo++;
					lid	= docId + "_" + lineNo;
//System.out.println("docId="+docId+" answerDocId="+answerDocId+" lid="+lid);

					preLidMap.put(lid,preId);	// 前の行のid
					LineInfo	preLineInfo	= tempLineInfoMap.get(preLidMap.get(lid));
					List<Integer> keyCountTotal		= new ArrayList<>();
					List<Integer> preKeyCountTotal	= preLineInfo.getKeycountTotal();
					for(int i = 0; i < keyCountList.size(); i++) {
						int preKeyCount	= preKeyCountTotal.get(i);
						int keyCount		= keyCountList.get(i);
						int total	= preKeyCount + keyCount;
						keyCountTotal.add(total);
					}
					lineInfo.setKeycountTotal(keyCountTotal);
				}
				else {
					// 1文目
					flag	= true;
					docId = answerDocId.substring(0,answerDocId.lastIndexOf("_"));
					startLineNo	= Integer.parseInt(answerDocId.substring(answerDocId.lastIndexOf("_")+1));
					lineNo	= startLineNo;
					lid	= answerDocId;
					preLidMap.put(lid,"");
					lineInfo.setKeycountTotal(keyCountList);
				}

				lineInfo.setKeycount(keyCountList);
				lineTotal	= answerText + lineTotal;
				lineInfo.setLine(answerText+"\n");
				lineInfo.setSource(docId);
				lineInfo.setLineNo(lineNo);
				if(lineInfo.getStartLineNo() == 0
						|| lineInfo.getStartLineNo() > startLineNo) {
					lineInfo.setStartLineNo(startLineNo);
				}
				lineInfo.setLevel(1);	// 処理レベル初期値
				lineInfo.setNeTotal(newNeTotal);
				ScoreInfo scoreInfo	= new ScoreInfo();
				scoreInfo.setLine(0);	// 一行スコア初期値

				if(lineInfo.getStartLineNo() == startLineNo) {
					LineInfo preLineInfo	= tempLineInfoMap.get(preLidMap.get(lid));
					if(multiSentFlag
							&& (lineInfo.getLineNo() > lineInfo.getStartLineNo())
							&& (addArg(lineInfo.getKeycountTotal())
									> addArg(preLineInfo.getKeycountTotal()))
									&& (addArg(lineInfo.getKeycountTotal())
											> addArg(lineInfo.getKeycount())))
					{
						// キーワード網羅数が条件を満たせば、
						// 複数文の構文解析結果を結合する。
						lineInfo.setMulticonst(true);
						lineInfo.setLineL0(lineTotal);
						lineInfo.setKeycountL0(lineInfo.getKeycountTotal());
					}
					else {
						// 複数文の構文解析結果を結合しない
						lineInfo.setMulticonst(false);
						lineInfo.setLineL0(answerText);
						lineInfo.setKeycountL0(keyCountList);
					}
				}
				tempLineInfoMap.put(lid, lineInfo);
				tempScoreInfoMap.put(lid, scoreInfo);
				lineIdSet.add(lid);
			}
		}
		lineInfoMap	= tempLineInfoMap;
		scoreInfoMap = tempScoreInfoMap;

		return lineIdSet;
	}

	/**
	 * MAXスコア、パラメタを決定する.
	 *
	 * @param keytermList
	 * @param queryInfo
	 */
	private void decideParam(QueryInfo queryInfo)
	{
		// キーワードの重みの総和
		int sumWgt = 0;
		List<QueryKeywordInfo> queryKeywordInfo	= queryInfo.getKeyWordInfos();
//		for(QueryKeywordInfo keyWordInfo : queryKeywordInfo) {
//			sumWgt += 1.0;	// 重みは1.0で固定
//		}
		if (queryKeywordInfo != null)
			sumWgt = queryKeywordInfo.size();
		
		answerParam.setWkaku(answerParam.getWkaku()*sumWgt);
		answerParam.setKakko(answerParam.getKakko()*sumWgt);

		answerParam.setCBigram(sumWgt * FindAnswersConstant.BIGRAM);
		answerParam.setRealBigram(answerParam.getCBigram());
		if(answerParam.getNeMode() == 1) {
			answerParam.setAlp2(sumWgt * FindAnswersConstant.NE);
		}
		else {
			answerParam.setAlp2(sumWgt * FindAnswersConstant.NUM);
		}

		answerParam.setCNe(answerParam.getAlp2());
		answerParam.setPreNe(answerParam.getAlp2());
		answerParam.setRealNe(answerParam.getAlp2());

		if(answerParam.getMode() == 1) {
			// MAXのみのとき使用
			for(QueryKeywordInfo keyWordInfo : queryKeywordInfo) {
				double weight	= keyWordInfo.getWeight();
				int temp	= 0;
				int relUp	= keyWordInfo.getRelUp();
				int relDown	= keyWordInfo.getRelDown();
				int rel2Up	= keyWordInfo.getRel2Up();
				int rel2Down	= keyWordInfo.getRel2Down();
				if(relUp != 0 || relDown != 0) {
					double s1	= CwqCalScr05.matchConst(relUp, relDown, relUp, relDown, depmatchSumFlag, depmatchDiffFlag);
					if(rel2Up != 0 || rel2Down != 0) {
						double s2	= CwqCalScr05.matchConst(rel2Up, rel2Down, rel2Up, rel2Down, depmatchSumFlag, depmatchDiffFlag);
						if(s2 > s1) {
							s1 = s2;
						}
					}
					temp += ( s1 * answerParam.getRelation());
				}
				if(keyWordInfo.getKaku() != null && keyWordInfo.getKaku().length() > 0) {
					temp += answerParam.getKaku();
					answerParam.setAlp1(answerParam.getAlp1() + temp + weight);
					answerParam.setAlp0(answerParam.getAlp0() + (1 + answerParam.getKaku()) * weight);
				}
			}
			if(queryKeywordInfo.get(0).getWkaku() != null && queryKeywordInfo.get(0).getWkaku().length() > 0) {
				answerParam.setAlp0(answerParam.getAlp0() + answerParam.getWkaku());
			}
			else if(regexCache.find("^(date|time)", answerParam.getNeType())) {
				answerParam.setAlp1(answerParam.getAlp1() + answerParam.getWkaku() * 0.8);
			}
			answerParam.setAlp1(answerParam.getAlp1() * FindAnswersConstant.KOUBUN);
			if(answerParam.getNeType().equals("0")) {
				answerParam.setAlp0(answerParam.getAlp0() + answerParam.getKakko());
			}
			answerParam.setAlp0(answerParam.getAlp0() * FindAnswersConstant.KEITAISO);
		}
	}

	/**
	 * 助数辞情報を取得する.
	 *
	 * @return
	 */
	private HashMap<String,Integer> getJosuji()
	{
		if(josujiList == null) {
			return null;
		}

		HashMap<String,Integer>	map	= new HashMap<String,Integer>();
		for(String str : josujiList) {
			map.put(str, 1);
		}
		return map;
	}

	/**
	 * NEが必要かの診断を行う.
	 *
	 * @param queryType クエリータイプ
	 * @return クエリータイプ
	 */
	private String neNeeds(String queryType)
	{
		if(regexCache.find("place|organization", queryType)) {
			return "LOCATION|ORGANIZATION";
		}
		if(queryType.equals("person")) {
			return "PERSON";
		}
		if(regexCache.find("^place", queryType)) {
			return "LOCATION";
		}
		if(queryType.equals("organization")) {
			return "ORGANIZATION";
		}
		if(regexCache.find("^(length|speed|area|rate|vol|year|weight|num|money|interval|period|date|time)", queryType)) {
			return queryType;
		}
		return "0";
	}

	/**
	 * 文に含まれるキーワードをカウントする.
	 *
	 * @param text
	 * @param keyTermList
	 * @return
	 */
	private List<Integer> getKeywordCount(String text, List<KeyTerm> keyTermList)
	{
		List<Integer> keyCountList	= new ArrayList<Integer>();
		for(KeyTerm keyTerm : keyTermList) {
			int count	= 0;
			String word = keyTerm.getCoveredText();
			word = word.replaceAll("\\?","\\\\?");
			Pattern p = Pattern.compile(word);
			Matcher m = p.matcher(text);
			m.reset();
			while(m.find()) {
				count++;
			}
			keyCountList.add(count);
		}

		return keyCountList;
	}

	/**
	 * 文に含まれる数値情報をカウントする.
	 *
	 * @param text
	 * @param pattern1
	 * @param pattern2
	 * @return
	 */
	private List<Integer> getNeCount(String text, String pattern1, String pattern2)
	{
		int count1	= 0;
		int count2	= 0;
		if(pattern1 != null && pattern2 != null) {
			Pattern p = Pattern.compile(pattern1+"|"+pattern2);
			Matcher m = p.matcher(text);
			m.reset();
			while(m.find()) {
				if(m.group().matches(pattern1)) {
					count1++;
				}
				if(m.group().matches(pattern2)) {
					count2++;
				}
			}
		}
		else if(pattern1 != null) {
			Pattern p = Pattern.compile(pattern1);
			Matcher m = p.matcher(text);
			m.reset();
			while(m.find()) {
				count1++;
			}				
		}
		else if(pattern2 != null) {
			Pattern p = Pattern.compile(pattern2);
			Matcher m = p.matcher(text);
			m.reset();
			while(m.find()) {
				count2++;
			}				
		}

		List<Integer>	countList	= new ArrayList<Integer>();
		countList.add(count1);
		countList.add(count2);

		return countList;
	}

	/**
	 * 配列の１以上の要素数を返す.
	 *
	 * @param keyCountList
	 * @return
	 */
	private int addArg(List<Integer> keyCountList)
	{
		int total = 0;	// 1以上の要素の数
		for(Integer count : keyCountList) {
			if(count > 0) {
				++total;
			}
		}
		return total;
	}

	/**
	 * 探索を開始する.
	 *
	 * @param lidList
	 * @param lineInfoMap
	 * @param scoreInfoMap
	 * @param queryInfo
	 * @param id2AnsMap
	 * @param ans2IdMap
	 * @param rankMap
	 * @param ansListMap
	 */
	private void startSearch(List<String> lidList, LinkedHashMap<String, LineInfo> lineInfoMap, LinkedHashMap<String, ScoreInfo> scoreInfoMap, QueryInfo queryInfo,
														HashMap<String,String> id2AnsMap, HashMap<String,String> ans2IdMap	,
														HashMap<String,Double> rankMap,	HashMap<String,Double> ansListMap)
	{

		int nCallStage5	= 0;	// 解生成ステージを呼んだ回数

		HashMap<String,Integer> timeCount	= new HashMap<String,Integer>();
		timeCount.put("juman", 0);
		timeCount.put("knp", 0);
		timeCount.put("ne", 0);
		timeCount.put("num", 0);
		timeCount.put("p_const", 0);
		timeCount.put("p_ne", 0);

		if(answerParam.getMode() >= 0) {    // A*探索または探索制御なし
			if(answerParam.getMode() > 0) {    // スタックをソート
				lidList	= sortScoreList(scoreInfoMap,lidList,true);
			}

			if(FindAnswersConstant.MINFOFLAG != 0) {
				// 形態素スコアを出す時にはとりあえず形態素解析までは進める
				for(String lid : lidList) {
					if(lineInfoMap.get(lid).getLevel() == 1) {
						// 形態素解析
						stage1(lid, queryInfo,lineInfoMap,scoreInfoMap,timeCount);
						if(answerParam.getMode() > 1) {
							lineInfoMap.get(lid).setLevel(2);
						}
						else {
							lineInfoMap.get(lid).setLevel(3);
						}
					}
				}
			}

			// 処理リストの先頭（最大スコアの解候補を持つ文）から処理を実行
			// 一つの処理を終えたら処理レベル＋１して、リストをソートする。
			while(lidList.size() > 0) {
				String lid = lidList.get(0);    // 文書id
				if(lineInfoMap.get(lid).getLevel() == 1) {
					// 形態素解析
					stage1(lid, queryInfo,lineInfoMap,scoreInfoMap,timeCount);
					if(answerParam.getMode() > 0) {
						lidList	= sortScoreList(scoreInfoMap,lidList,false);
					}
					if(answerParam.getMode() > 1) {
						lineInfoMap.get(lid).setLevel(2);
					}
					else {
						lineInfoMap.get(lid).setLevel(3);
					}
				}
				else if(lineInfoMap.get(lid).getLevel() == 2 ) {
					// 構文、NE推定
					stage2(lid, queryInfo, lineInfoMap, scoreInfoMap, timeCount);
					if(answerParam.getMode() > 0) {
						lidList	= sortScoreList(scoreInfoMap,lidList,false);
					}
					lineInfoMap.get(lid).setLevel(3);
				}
				else if(lineInfoMap.get(lid).getLevel() == 3) {
					// 構文解析
					stage3(lid, queryInfo, lineInfoMap, scoreInfoMap, timeCount);
					if(answerParam.getMode() > 0) {
						lidList	= sortScoreList(scoreInfoMap,lidList,false);
					}
					lineInfoMap.get(lid).setLevel(4);
				}
				else if(lineInfoMap.get(lid).getLevel() == 4) {
					// NE,num
					stage4(lid, queryInfo, lineInfoMap, scoreInfoMap, timeCount);
					if(answerParam.getMode() > 0) {
						lidList	= sortScoreList(scoreInfoMap,lidList,false);
					}
					lineInfoMap.get(lid).setLevel(5);
				}
				else if(lineInfoMap.get(lid).getLevel() == 5) {
					// 回答作成
					nCallStage5	= stage5(lid, queryInfo, lineInfoMap, scoreInfoMap,id2AnsMap, ans2IdMap,
							 			rankMap, ansListMap, nCallStage5);
					if(scoreInfoMap.get(lid).getLine() <= 0) {
						lidList.remove(0);
					}
					if(answerParam.getMode() > 0) {
						if(ansListMap.size() >= ansNum) {
							break;
						}
						if(nCallStage5 >= ansNumLimit) {
							break;
						}
						lidList	= sortScoreList(scoreInfoMap,lidList,false);
					}
				}
				else {
					break;
				}
			}
		}
		else {
			// 原始的な探索制御
			int num = 0;
			Map<String, Map<String,Double>> lscoreInfoMap	= new HashMap<String, Map<String,Double>>();
//			%{ $lscore{0} } = %{ $score{'line'} };
			List<String> gotAnsList = new ArrayList<>();

			LOOP:
			while((num += ansNum) > 0) {
			//while(num += ansNum) {
				int[] array = new int[]{ 0, 1, 3, 4 };
				for(int level : array) {
					if(level == 0 ) {
						if(answerParam.getMode() == -1 ) {    // ビームサーチ
							//%{ $score{'line'} } = %{ $lscore{0} };
							if(gotAnsList.size() > 0) {
								for(String lid : gotAnsList) {
									scoreInfoMap.get(lid).setLine(-1.0);
								}
							}
						}
					}
					else {
						//%{ $score{'line'} } = ();
						if(level == 1) {
							for(String lid : lidList) {
								// 形態素解析
								if(lscoreInfoMap.containsKey("1") && lscoreInfoMap.get("1").containsKey(lid)) {
									scoreInfoMap.get(lid).setLine(lscoreInfoMap.get("1").get(lid));
								}
								else {
									stage1(lid, queryInfo,lineInfoMap,scoreInfoMap,timeCount);
									Map<String,Double> tempMap	= new HashMap<String,Double>();
									tempMap.put(lid, scoreInfoMap.get(lid).getLine());
									lscoreInfoMap.put("1", tempMap);
								}
							}
						}
						else if(level == 3) {
							for(String lid : lidList) {
								// 構文解析
								if(lscoreInfoMap.containsKey("3") && lscoreInfoMap.get("3").containsKey(lid)) {
									scoreInfoMap.get(lid).setLine(lscoreInfoMap.get("3").get(lid));
								}
								else {
									stage3(lid, queryInfo,lineInfoMap,scoreInfoMap,timeCount);
									Map<String,Double> tempMap	= new HashMap<String,Double>();
									tempMap.put(lid, scoreInfoMap.get(lid).getLine());
									lscoreInfoMap.put("3", tempMap);
								}
							}
						}
						else if(level == 4) {
							for(String lid : lidList) {
								// NE,num
								if(lscoreInfoMap.containsKey("4") && lscoreInfoMap.get("4").containsKey(lid)) {
									scoreInfoMap.get(lid).setLine(lscoreInfoMap.get("4").get(lid));
								}
								else {
									stage4(lid, queryInfo,lineInfoMap,scoreInfoMap,timeCount);
									Map<String,Double> tempMap	= new HashMap<String,Double>();
									tempMap.put(lid, scoreInfoMap.get(lid).getLine());
									lscoreInfoMap.put("4", tempMap);
								}
							}
						}
					}
					lidList	= sortScoreList(scoreInfoMap,lidList,true);
					lidList	= CwqCalScr05.cutBorder(answerParam, scoreInfoMap, lidList, ansNum, level);
				}

				// 回答作成
				if(answerParam.getMode() == -1 ) {
					// ビームサーチのとき
					while(ansListMap.size() < num && scoreInfoMap.get(lidList.get(0)).getLine() > 0 ) {
						nCallStage5	= stage5(lidList.get(0), queryInfo, lineInfoMap, scoreInfoMap,id2AnsMap, ans2IdMap,rankMap, ansListMap, nCallStage5);
					}
					List<String> ranking = sortRank(rankMap);
					int flag = 0;
					for(int i = 0 ; i < ( ansNum  - 1 == -1 ? ansNum : ansNum -1) ; i++ ) {
						if(!ranking.contains(i)) {
							flag = 1;
							break;
						}
						if(regexCache.find("^"+lidList.get(0), ranking.get(i))) {
							flag = 1;
							break;
						}
					}
					if(flag == 0) {
						break LOOP;
					}
					gotAnsList.add(lidList.get(0));
					if(gotAnsList.size() >= lscoreInfoMap.get("0").size()) {
						break LOOP;
					}
				}
				else {
					// 単純な枝刈りのとき
					for(String top : lidList) {
						while(ansListMap.size() < num && scoreInfoMap.get(top).getLine() > 0 ) {
							nCallStage5	= stage5(top, queryInfo, lineInfoMap, scoreInfoMap,id2AnsMap, ans2IdMap,
									rankMap, ansListMap, nCallStage5);
						}
						num += ansNum;
					}
					break LOOP;
				}
			}
		}
		return;
	}

	/**
	 * 形態素解析を行うステージ.
	 *
	 * @param lid
	 * @param queryInfo
	 * @param lineInfoMap
	 * @param scoreInfoMap
	 * @param timeCount
	 */
	private void stage1(String lid, QueryInfo queryInfo,
							LinkedHashMap<String,LineInfo> lineInfoMap,
							LinkedHashMap<String,ScoreInfo> scoreInfoMap,
							HashMap<String,Integer> timeCount)
	{
		LineInfo	lineInfo	= lineInfoMap.get(lid);
		if(lineInfo.getMulticonst()) {
			// 複数文照合で結合する文について形態素解析を実行
			for(int i = lineInfo.getStartLineNo(); i <= lineInfo.getLineNo(); i++) {
				String temp = lineInfo.getSource() + "_" + i;
				LineInfo tempLineInfo	= lineInfoMap.get(temp);
				if(tempLineInfo.getKeyWordList() == null) {
					List<KeyWord> keyWordList	= GetKeyword04.getKeyword(tempLineInfo.getMorphemeList());
					List<KeyWord> tempKeywordList	= new ArrayList<>();
					for(KeyWord keyWord : keyWordList) {
						String part	= keyWord.getPart();
						part	= part.replaceFirst("^unknown$", "noun");
						KeyWord newKeyWord	= new KeyWord(keyWord.getWord(),keyWord.getLocation(),
														keyWord.getLocationE(),part);
						tempKeywordList.add(newKeyWord);
					}
					tempLineInfo.setKeyWordList(tempKeywordList);
					lineInfoMap.put(temp, tempLineInfo);
				}
			}

			boolean flag = false;
			for(int i = lineInfo.getStartLineNo(); i <= lineInfo.getLineNo(); i++) {
				String temp = lineInfo.getSource() + "_" + i;
				LineInfo tempLineInfo	= lineInfoMap.get(temp);
				List<KeyWord> tempKeyWordList	= tempLineInfo.getKeyWordList();
				List<KeyWord> tempList	= new ArrayList<>();
				for(int j = 0 ; j < tempKeyWordList.size() ; j++)
				{
					if(tempKeyWordList.get(j).getWord() == null) {
						break;
					}
					String word		= tempKeyWordList.get(j).getWord();
					int location	= tempKeyWordList.get(j).getLocation();
					int locationE	= tempKeyWordList.get(j).getLocationE();
					String part		= tempKeyWordList.get(j).getPart();

					// locationは前にある文の形態素数だけずらす。
					if(flag) {
						location += lineInfo.getMorphemeTotalList() == null ? 0 : lineInfo.getMorphemeTotalList().size();
						locationE += lineInfo.getMorphemeTotalList() == null ? 0 : lineInfo.getMorphemeTotalList().size();
					}
					KeyWord	keyWord	= new KeyWord(word,location,locationE,part);
					tempList.add(keyWord);
				}
				List<KeyWord> keyTotal	= lineInfo.getKeyWordTotalList();
				if(keyTotal == null) {
					keyTotal	= new ArrayList<KeyWord>();
				}
				keyTotal.addAll(tempList);
				lineInfo.setKeyWordTotalList(keyTotal);

				List<String> hyosoTotal	= lineInfo.getMorphemeHyosoTotalList();
				if(hyosoTotal == null) {
					hyosoTotal	= new ArrayList<String>();
				}
				hyosoTotal.addAll(tempLineInfo.getMorphemeHyosoList());
				lineInfo.setMorphemeHyosoTotalList(hyosoTotal);

				List<Morpheme> morphemeTotal	= lineInfo.getMorphemeTotalList();
				if(morphemeTotal == null) {
					morphemeTotal	= new ArrayList<Morpheme>();
				}
				morphemeTotal.addAll(tempLineInfo.getMorphemeList());
				lineInfo.setMorphemeTotalList(morphemeTotal);

				if(!flag) {
					flag = true;
				}
			}
			lineInfoMap.put(lid, lineInfo);

			int offset	= lineInfo.getMorphemeTotalList().size() - lineInfo.getMorphemeList().size();
			scoreInfoMap	= CwqCalScr05.calScrRKey(answerParam, queryInfo, lid, lineInfoMap, scoreInfoMap, offset);

			// キーワードカウント
			TreeMap<Integer,Double> scoreRKeyTotalList	= scoreInfoMap.get(lid).getRkeywordTotal();

			// １行分のスコアだけを取得
			int size	= scoreRKeyTotalList.size() - lineInfo.getMorphemeList().size();
			TreeMap<Integer,Double> tempRkeyMap	= new TreeMap<Integer,Double>();
			if(size <= 0) {
				tempRkeyMap	= scoreRKeyTotalList;
			}
			else {
				int count = 0;
				for(Entry<Integer,Double> entry : scoreRKeyTotalList.entrySet()) {
					if(count < size) {
						++count;
						continue;
					}
					int i = entry.getKey();
					tempRkeyMap.put(i, entry.getValue());
				}
			}
			scoreInfoMap.get(lid).setRkeyword(tempRkeyMap);
		}
		else {
			// 複数文照合しない
			if(lineInfo.getKeyWordList() == null) {
				List<KeyWord> keyWordList	= GetKeyword04.getKeyword(lineInfo.getMorphemeList());
				List<KeyWord> tempKeywordList	= new ArrayList<>();
				for(KeyWord keyWord : keyWordList) {
					String part	= keyWord.getPart();
					part	= part.replaceFirst("^unknown$", "noun");
					KeyWord newKeyWord	= new KeyWord(keyWord.getWord(),keyWord.getLocation(),
													keyWord.getLocationE(),part);
					tempKeywordList.add(newKeyWord);
				}
				lineInfo.setKeyWordList(tempKeywordList);
				lineInfoMap.put(lid, lineInfo);
			}

			int offset	= 0;
			scoreInfoMap	= CwqCalScr05.calScrRKey(answerParam, queryInfo, lid, lineInfoMap, scoreInfoMap, offset);
		}
		if(answerParam.getMode() < 3 && timeCountFlag) {	// 時間計測
			timeCount.put("juman",timeCount.get("juman")+1);
		}
		scoreInfoMap	= CwqCalScr05.calScrR2gram(answerParam, lid, lineInfoMap, scoreInfoMap);
		scoreInfoMap	= CwqCalScr05.calScrPNum(answerParam, lid, lineInfoMap, scoreInfoMap);
		if(answerParam.getMode() != 0) {
			scoreInfoMap	= CwqCalScr05.getScore1(answerParam, lid, scoreInfoMap);
		}
		return;
	}

	/**
	 * 構文推定、固有表現推定のステージ.
	 *
	 * @param lid
	 * @param queryInfo
	 * @param lineInfoMap
	 * @param scoreInfoMap
	 * @param timeCount
	 */
	private void stage2(String lid, QueryInfo queryInfo,
						LinkedHashMap<String,LineInfo> lineInfoMap, LinkedHashMap<String,ScoreInfo> scoreInfoMap,
						HashMap<String,Integer> timeCount)
	{
		LineInfo lineInfo	= lineInfoMap.get(lid);

		if(lineInfo.getMulticonst()) {
			List<String> pBnstTotalList	= new ArrayList<>();
			for(int i = lineInfo.getStartLineNo(); i <= lineInfo.getLineNo(); i++) {
				String temp = lineInfo.getSource() + "_" + i;
				LineInfo tempLineInfo	= lineInfoMap.get(temp);
				List<Morpheme> morphemeList	= tempLineInfo.getMorphemeList();
				List<String> tempPbnstList	= tempLineInfo.getPbnst();
				if(tempPbnstList == null) {
					tempPbnstList = CwqCalScr05.cutBnst(morphemeList);
					tempLineInfo.setPbnst(tempPbnstList);
					lineInfoMap.put(temp, tempLineInfo);
				}
				pBnstTotalList.addAll(tempPbnstList);
			}
			scoreInfoMap	= CwqCalScr05.calScrPConst(answerParam, queryInfo, lid, lineInfoMap, scoreInfoMap, pBnstTotalList, depmatchSumFlag, depmatchDiffFlag);
		}
		else {
			List<Morpheme> morphemeList	= lineInfo.getMorphemeList();
			List<String> pbnstList		= lineInfo.getPbnst();
			if(pbnstList == null) {
				pbnstList = CwqCalScr05.cutBnst(morphemeList);
				lineInfo.setPbnst(pbnstList);
				lineInfoMap.put(lid, lineInfo);
			}
			scoreInfoMap	= CwqCalScr05.calScrPConst(answerParam, queryInfo, lid, lineInfoMap, scoreInfoMap, pbnstList, depmatchSumFlag, depmatchDiffFlag);
		}
		if(answerParam.getMode() < 3 && timeCountFlag) {	// 時間計測
			timeCount.put("p_const",timeCount.get("p_const")+1);
		}
		if(answerParam.getNeMode() == 1) {
			// NEスコアの推定
			scoreInfoMap	= CwqCalScr05.calScrPNe(answerParam, lid, lineInfoMap, scoreInfoMap);

			if(answerParam.getMode() < 3 && timeCountFlag ) {	// 時間計測
				timeCount.put("p_ne",timeCount.get("p_ne")+1);
			}
		}
		scoreInfoMap	= CwqCalScr05.getScore2(answerParam, lid, scoreInfoMap);

		return;
	}

	/**
	 * 構文解析を行うステージ.
	 *
	 * @param lid
	 * @param queryInfo
	 * @param lineInfoMap
	 * @param scoreInfoMap
	 * @param timeCount
	 */
	private void stage3(String lid, QueryInfo queryInfo,
							LinkedHashMap<String,LineInfo> lineInfoMap, LinkedHashMap<String,ScoreInfo> scoreInfoMap,
							HashMap<String,Integer> timeCount)
	{
		LineInfo lineInfo	= lineInfoMap.get(lid);
		ScoreInfo scoreInfo	= scoreInfoMap.get(lid);

		List<Morpheme> morphemeList	= lineInfo.getMorphemeList();

		if(morphemeList.size() > FindAnswersConstant.KNPMAX) {
			TreeMap<Integer,Double> r2gramMap	= scoreInfo.getR2gram();
			TreeMap<Integer,Double> rConstMap	= scoreInfo.getRconst();
			for(Entry<Integer,Double> entry : r2gramMap.entrySet()) {
				int i = entry.getKey();
				rConstMap.put(i, 0.0);
			}
			scoreInfo.setRconst(rConstMap);
			scoreInfoMap.put(lid, scoreInfo);
		}
		else if(lineInfo.getMulticonst()) {
			// キーワード網羅数が条件を満たせば、
			// 複数文の構文解析結果を結合する。
			int bnstNum = 0;    // 文節数

			List<KnpSegment> knpSegmentTotalList	= new ArrayList<KnpSegment>();
			List<KyotoDependency> kyotoDependencyTotalList	= new ArrayList<KyotoDependency>();
			Map<KnpSegment,List<Morpheme>> knpInfoTotalMap	=  new LinkedHashMap<KnpSegment,List<Morpheme>>();

//			HashMap<Integer,KnpBnstTree> knpHashTotal	= null;
			KnpBnstHash knpHashTotal	= null;
			for(int i = lineInfo.getStartLineNo(); i <= lineInfo.getLineNo(); i++) {
				String temp = lineInfo.getSource() + "_" + i;
				LineInfo tempLineInfo	= lineInfoMap.get(temp);
				List<Morpheme> tempMorphemeList	= tempLineInfo.getMorphemeList();
				List<KyotoDependency> kyotoDependencyList	= tempLineInfo.getKyotoDependencyList();
				List<KnpSegment> knpSegmentList	= tempLineInfo.getKnpSegmentList();
				Map<KnpSegment,List<Morpheme>> knpInfoMap	= tempLineInfo.getKnpInfoMap();
				if(tempMorphemeList.size() <= FindAnswersConstant.KNPMAX) {
					KnpBnstHash knpHash = Knp2hash01.knp2Hash(knpSegmentList.size()-1, kyotoDependencyList, knpSegmentList, true);
					tempLineInfo.setKnpHash(knpHash);
				}

				if(tempLineInfo.getKnpHash() != null) {
					if(knpHashTotal != null) {
						KnpBnstHash knpHash	= tempLineInfo.getKnpHash();
						// marge_hash4:次文の最初の提題文節に係り受けさせる
						knpHashTotal = Knp2hash01.margeHash4(knpHashTotal, knpHash, bnstNum, kyotoDependencyList, knpSegmentList, -1);
					}
					else {
						knpHashTotal	= tempLineInfo.getKnpHash();
					}
				}

				lineInfoMap.put(temp, tempLineInfo);

				// KnpSegmentとKyotoDependencyも照合
				knpSegmentTotalList.addAll(knpSegmentList);
				kyotoDependencyTotalList.addAll(kyotoDependencyList);
				for(Entry<KnpSegment,List<Morpheme>> entry : knpInfoMap.entrySet()) {
					knpInfoTotalMap.put(entry.getKey(), entry.getValue());
				}
				
				lineInfo.setKnpSegmentTotalList(knpSegmentTotalList);
				lineInfo.setKyotoDependencyTotalList(kyotoDependencyTotalList);
				lineInfo.setKnpInfoTotalMap(knpInfoTotalMap);

				if(i == lineInfo.getLineNo()) {
					break;
				}
				if(knpSegmentList.size()-1 >= 0 && knpSegmentList.contains(knpSegmentList.size()-1)) {
					KnpSegment lastKnpSegment	= knpSegmentList.get(knpSegmentList.size()-1);
					if(lastKnpSegment.getKnpBunsetsuFeatures() == null) {
						knpSegmentList.remove(knpSegmentList.size()-1);
					}					
				}
				bnstNum += knpSegmentList.size();
			}
			lineInfoMap.put(lid, lineInfo);
			scoreInfoMap	= CwqCalScr05.calScrRConst(answerParam, queryInfo, lid, lineInfoMap, scoreInfoMap, knpHashTotal, bnstNum, distOnlyFlag, depmatchSumFlag, depmatchDiffFlag);
		}
		else {
			List<KyotoDependency> kyotoDependencyList	= lineInfo.getKyotoDependencyList();
			List<KnpSegment> knpSegmentList	= lineInfo.getKnpSegmentList();
			KnpBnstHash knpHash = Knp2hash01.knp2Hash(knpSegmentList.size()-1, kyotoDependencyList, knpSegmentList, true);
			lineInfo.setKnpHash(knpHash);

			scoreInfoMap	= CwqCalScr05.calScrRConst(answerParam, queryInfo, lid, lineInfoMap, scoreInfoMap, knpHash, 0, distOnlyFlag, depmatchSumFlag, depmatchDiffFlag);
		}

		if(answerParam.getMode() < 3 && timeCountFlag) {    // 時間計測
			timeCount.put("knp",timeCount.get("knp")+1);
		}
		if(answerParam.getMode() != 0) {
			scoreInfoMap	= CwqCalScr05.getScore3(answerParam, lid, scoreInfoMap);
		}
		return;
	}

	/**
	 * 固有表現抽出、数量表現抽出を行うステージ.
	 *
	 * @param lid
	 * @param queryInfo
	 * @param lineInfoMap
	 * @param scoreInfoMap
	 * @param timeCount
	 */
	private void stage4(String lid, QueryInfo queryInfo,
							LinkedHashMap<String,LineInfo> lineInfoMap, LinkedHashMap<String,ScoreInfo> scoreInfoMap,
							HashMap<String,Integer> timeCount)
	{
		LineInfo lineInfo	= lineInfoMap.get(lid);
		ScoreInfo scoreInfo	= scoreInfoMap.get(lid);

		List<Morpheme> morphemeList	= lineInfo.getMorphemeList();
		List<KnpSegment> knpSegmentList	= lineInfo.getKnpSegmentList();

		if(answerParam.getNeType() != null && !answerParam.getNeType().equals("") && !answerParam.getNeType().equals("0")) {
			if(answerParam.getNeMode() == 1 && morphemeList != null) {
				scoreInfoMap	= CwqCalScr05.calScrRNe(answerParam, lid, lineInfoMap, scoreInfoMap);
				if(answerParam.getMode() < 3 && timeCountFlag) {    // 時間計測
					timeCount.put("ne",timeCount.get("ne")+1);
				}
			}
			else if(answerParam.getNeMode() == 2 && FindAnswersConstant.NUMEXT != 0 && knpSegmentList != null ) {
				// 数量表現抽出
				scoreInfo = CwqCalScrNum01.calscrRnum(answerParam, lineInfo, scoreInfo, queryInfo, decisionList, atrList,cpcBySubMap, cphByIdMap, cphByNameMap);
				if(answerParam.getMode() < 3 && timeCountFlag) {    // 時間計測
					timeCount.put("num",timeCount.get("num")+1);
				}
			}
			else { // rate|interval|period|date|time
				scoreInfo.setRne(scoreInfo.getPne()); // そのまま
			}
		}
		else {
			scoreInfo.setRne(scoreInfo.getPne()); // そのまま
		}
		scoreInfoMap.put(lid, scoreInfo);

		if(answerParam.getMode() != 0) {
			scoreInfoMap	= CwqCalScr05.getScore4(answerParam, lid, scoreInfoMap);
		}
		else {
			// 探索制御なしのとき
			scoreInfoMap	= CwqCalScr05.getAllScore(answerParam, lid, scoreInfoMap);
		}
		return;
	}

	/**
	 * 解作成を行うステージ.
	 *
	 * @param lid
	 * @param queryInfo
	 * @param lineInfoMap
	 * @param scoreInfoMap
	 * @param id2AnsMap Key:行ID_形態素ID, Value:解表層
	 * @param ans2IdMap Key:解表層, Value:行ID_形態素ID
	 * @param rankMap Key:行ID_形態素ID, Value:最終スコア
	 * @param ansListMap Key:解表層, Value:最終スコア
	 * @param nCallStage5 解生成ステージを呼んだ回数
	 */
	private int stage5(String lid, QueryInfo queryInfo,
						LinkedHashMap<String,LineInfo> lineInfoMap, LinkedHashMap<String,ScoreInfo> scoreInfoMap,
						HashMap<String,String> id2AnsMap, HashMap<String,String> ans2IdMap,
						HashMap<String,Double> rankMap, HashMap<String,Double> ansListMap, int nCallStage5)
	{
		//LineInfo lineInfo	= lineInfoMap.get(lid);
		ScoreInfo scoreInfo	= scoreInfoMap.get(lid);

		TreeMap<Integer,Double> stage4Map	= scoreInfo.getStage4();
		TreeMap<Integer,Double> stageFinal	= scoreInfo.getStageFinal();
		double lineScore	= scoreInfo.getLine();

		double max		= 0;
		String answer	= null;

		nCallStage5++;

		for(Entry<Integer,Double> entry : stage4Map.entrySet()) {
			int i = entry.getKey();

			if(stage4Map.get(i) == lineScore) {
				answer	= CwqMakeAns04.mkAns(answerParam, lid, i, queryInfo, lineInfoMap,dateFlag, personSuffixIn, personSuffixOut);

				// 解が "" なら次のものをみつける．
				if(answer.equals("")) {
					stage4Map.put(i, 0.0);
					continue;
				}
/*
				if(answerParam.getMode() > 1) {
					if(answerParam.getNeMode() == 1) {
						printf(
							"\(%3.1f\,%3.1f\,%3.1f\,1.0\):%3.1f\t",
							$$score{'r_2gram'}{$lid}[$i],
							$$score{'r_keyword'}{$lid}[$i],
							$$score{'c_const'}{$lid},
							$$score{'1'}{$lid}[$i]
						);
					}
					else {
						printf(
							"\(%3.1f\,%3.1f\,%3.1f\,%3.1f\):%3.1f\t",
							$$score{'r_2gram'}{$lid}[$i],
							$$score{'r_keyword'}{$lid}[$i],
							$$score{'c_const'}{$lid},
							$$score{'p_ne'}{$lid}[$i],
							$$score{'1'}{$lid}[$i]
						);
					}
					if ( @{ $$score{'p_const'}{$lid} } ) {
						printf(
							"\(%3.1f\,%3.1f\):%3.1f\t",
							$$score{'p_const'}{$lid}[$i],
							$$score{'p_ne'}{$lid}[$i],
							$$score{'2'}{$lid}[$i]
						);
					}
					else {
						printf(
							"\(%3.1f\):%3.1f\t",
							$$score{'p_ne'}{$lid}[$i],
							$$score{'2'}{$lid}[$i]
						);
					}
					printf(
						"\(%3.1f\):%3.1f\t",
						$$score{'r_const'}{$lid}[$i],
						$$score{'3'}{$lid}[$i]
					);
					printf(
						"\(%3.1f\):%3.2f\t\(%s\,%d\)%s\t%s\n",
						$$score{'r_ne'}{$lid}[$i],
						$$score{'4'}{$lid}[$i],
						$lid, $i, $answer, $$line{'line'}{$lid}
					);
				}
				else {    # 探索制御なし または MAXのみ
					printf(
	"\(s11:%3.1f\,s12:%3.1f\,s22:%3.1f\,s31:%3.1f\):%3.2f\t\(%s\,%d\)%s\t%s\n",
						$$score{'r_2gram'}{$lid}[$i],
						$$score{'r_keyword'}{$lid}[$i],
						$$score{'r_const'}{$lid}[$i],
						$$score{'r_ne'}{$lid}[$i],
						$$score{'4'}{$lid}[$i],
						$lid,
						$i,
						$answer,
						$$line{'line'}{$lid}
					);
				}

*/
				int flag = 0;
				if(ansListMap.get(answer) == null && answerParam.getNeType().matches("^(PERSON)")) {
					// 人名で、すでに似た解を得ている場合
					TreeMap<Integer,Double> rNeMap	= scoreInfo.getRne();
					for(String temp : ansListMap.keySet()) {
						if(answer.matches(temp) && rNeMap.get(i) == 1) {
							String id	= lid+"__"+i;
							id2AnsMap.put(id, answer);
							ans2IdMap.put(answer, id);
							id2AnsMap.remove(ans2IdMap.get(temp));
							rankMap.remove(ans2IdMap.get(temp));
							ans2IdMap.remove(temp);

							if(stage4Map.get(i) > ansListMap.get(temp)) {
								rankMap.put(id, stage4Map.get(i));
								ansListMap.put(answer, stage4Map.get(i));
							}
							else {
								rankMap.put(id, ansListMap.get(temp));
								ansListMap.put(answer, ansListMap.get(temp));
							}
							ansListMap.remove(temp);
							flag = 1;
							break;
						}
						else if(temp.matches(answer)) {
							String[] array	= ans2IdMap.get(temp).split("__");
							String id	= array[0];
							int kNo	= Integer.parseInt(array[1]);
							ScoreInfo tempScoreInfo	= scoreInfoMap.get(id);
							TreeMap<Integer,Double> tempRNeMap	= tempScoreInfo.getRne();
							if(tempRNeMap.get(kNo) != 1) {
								continue;
							}

							if(stage4Map.get(i) > ansListMap.get(temp)) {
								rankMap.put(ans2IdMap.get(temp), stage4Map.get(i));
								ansListMap.put(temp, stage4Map.get(i));
							}
							flag = 1;
							break;
						}
					}
				}

				if(flag == 0 && ( !ansListMap.containsKey(answer) || ansListMap.get(answer) == 0)
							|| ansListMap.get(answer) < stage4Map.get(i)) {
					String id	= lid+"__"+i;
					id2AnsMap.put(id, answer);
					ans2IdMap.put(answer, id);
					rankMap.put(id,stage4Map.get(i));
					ansListMap.put(answer, stage4Map.get(i));
				}

				stageFinal.put(i, stage4Map.get(i));
				// 次の解を求めるためにスコアを0に設定
				stage4Map.put(i, 0.0);
				scoreInfo.setStage4(stage4Map);
				scoreInfo.setStageFinal(stageFinal);
			}
			if(max < stage4Map.get(i)) {
				max = stage4Map.get(i);
			}
		}
		scoreInfo.setLine(max);
		scoreInfoMap.put(lid, scoreInfo);

		return nCallStage5;
	}

	/**
	 * スコアを降順ソートする.
	 *
	 * @param scoreInfoMap
	 * @param lidList
	 * @param flag
	 * @return
	 */
	private List<String> sortScoreList(Map<String, ScoreInfo> scoreInfoMap, List<String> lidList, boolean flag)
	{
		Map<String,Double> tempLineScoreMap	= new HashMap<String,Double>();
		for(Entry<String,ScoreInfo> entry : scoreInfoMap.entrySet()){
			String lid	= entry.getKey();
			ScoreInfo scoreInfo	= entry.getValue();
			double lineScore	= scoreInfo.getLine();
			tempLineScoreMap.put(lid, lineScore);
		}

		List<String> tmpList	= lidList;
		if(lidList == null) {
			tmpList	= new ArrayList<>();
		}
		if(flag) {
			// いちばん最初
			List<Map.Entry<String,Double>> entries = new ArrayList<Map.Entry<String,Double>>(tempLineScoreMap.entrySet());
			Collections.sort(entries, new Comparator<Map.Entry<String,Double>>() {
				public int compare(Map.Entry<String,Double> o1, Map.Entry<String,Double> o2){
					Map.Entry<String,Double> e1 = o1;
					Map.Entry<String,Double> e2 = o2;
					return ((Double)e2.getValue()).compareTo((Double)e1.getValue());
				}
			});
			for(Map.Entry<String, Double> entry : entries) {
				tmpList.add(entry.getKey());
			}
			return tmpList;
		}
		else {

			// ２回目以降、処理を行なった文だけ入れ換え
			// binary search 版
			if(tmpList.size() <= 1 ) {
				return tmpList;
			}
			else {
				String mkey = tmpList.remove(0);

				int i = 0;
				int j = tmpList.size() - 1;
				int k;

				do {
					if(scoreInfoMap.get(mkey).getLine() >= scoreInfoMap.get(tmpList.get(i)).getLine()) {
						List<String> addItem = new ArrayList<>();
						addItem.add(mkey);
						tmpList.addAll(i, addItem);
						return tmpList;
					}
					else if(scoreInfoMap.get(tmpList.get(j)).getLine() >= scoreInfoMap.get(mkey).getLine()) {
						List<String> addItem = new ArrayList<>();
						addItem.add(mkey);
						tmpList.addAll(j+1, addItem);
						return tmpList;
					}
					k = Math.round((i + j) / 2);
					if(scoreInfoMap.get(tmpList.get(k)).getLine() > scoreInfoMap.get(mkey).getLine()) {
						i = k + 1;
					}
					else {
						j = k - 1;
					}
				} while ( ( i <= j ) && ( (scoreInfoMap.get(tmpList.get(k)).getLine() != scoreInfoMap.get(mkey).getLine())));

				if(scoreInfoMap.get(tmpList.get(k)).getLine() == scoreInfoMap.get(mkey).getLine()) {
					List<String> addItem = new ArrayList<>();
					addItem.add(mkey);
					tmpList.addAll(k, addItem);
				}
				else {
System.err.println("error");
				}
			}
		}
		return tmpList;
	}

	/**
	 * ランクでソートする.
	 *
	 * @param rankMap
	 * @return
	 */
	private List<String> sortRank(Map<String, Double> rankMap)
	{
		List<String> tmpList	= new ArrayList<>();

		List<Map.Entry<String,Double>> entries = new ArrayList<Map.Entry<String,Double>>(rankMap.entrySet());
		Collections.sort(entries, new Comparator<Map.Entry<String,Double>>() {
			public int compare(Map.Entry<String,Double> o1, Map.Entry<String,Double> o2){
				Map.Entry<String,Double> e1 = o1;
				Map.Entry<String,Double> e2 = o2;
				return ((Double)e2.getValue()).compareTo((Double)e1.getValue());
			}
		});
		for(Map.Entry<String, Double> entry : entries) {
			tmpList.add(entry.getKey());
		}
		return tmpList;
	}

}
