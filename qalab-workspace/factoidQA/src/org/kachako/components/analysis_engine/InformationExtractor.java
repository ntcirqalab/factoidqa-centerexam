package org.kachako.components.analysis_engine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.TOP;
import org.apache.uima.resource.ResourceInitializationException;
import org.kachako.components.factoid_qa.manage.RequireInfo;
import org.kachako.components.factoid_qa.module.GetRequireInfo;
import org.kachako.components.factoid_qa.module.MakePassage;
import org.kachako.share.regex.RegexCache;
import org.kachako.types.qa.AnswerCandidate;
import org.kachako.types.qa.Document;
import org.kachako.types.qa.KeyTerm;
import org.kachako.types.qa.Question;
import org.kachako.types.qa.ViewId;
import org.kachako.ui.model.ssh.ResourceLoader;
import org.kachako.types.japanese.syntactic.morpheme.Morpheme;

/**
 * パッセージ情報を抽出します.
 * 
 * @author JSA saitou
 *
 */
public class InformationExtractor extends JCasAnnotator_ImplBase
{
	/* RS実行結果 VIEW名 PREFIX */
	private static final String RS_RESULT_VIEW_NAME_PREFIX	= "RS";
	/* IX実行結果 VIEW名 PREFIX */
	private static final String IX_RESULT_VIEW_NAME_PREFIX	= "IX";

	// 質問文が格納されているViewの名前
	private final String PARAM_QUESTION_VIEW_NAME	= "QuestionViewName";
	// パラメータ：検索結果数
//	private final String PARAM_RESULT_NUM	= "ResultNum";
	// パラメータ：1パッセージあたりの文数
	private final String PARAM_PASSAGE_LENGTH	= "PassageLength";
	// パラメータ：1文書あたりの採用パッセージ数
	private final String PARAM_PASSGE_NUM_PER_DOC	= "PassageNumPerDoc";
	// パラメータ：採用する全パッセージ数
	private final String PARAM_PASSGE_NUM_ALL	= "PassageNumAll";

	// 質問文が格納されているView名
	private  String questionViewName		= null;
	// 検索結果数
//	private static int resultNum		= 0;
	// 1パッセージあたりの文数
	private  int passageLength	= 0;
	// 1文書あたりの採用パッセージ数
	private  int passageNumPerDoc	= 0;
	// 採用する全パッセージ数
	private  int passageNumAll	= 0;

	// 助数辞
	private String[] josujiList		= null;

	private final ResourceLoader	resourceLoader	= ResourceLoader.getInstance();

//	private final String LINE_SEPARATOR = "\n";

	@Override
	public void initialize(UimaContext aContext) throws ResourceInitializationException
	{
		super.initialize(aContext);

System.out.println("--------InformationExtractor initialize---------");

		// 質問文が格納されているView名の取得
		questionViewName = (String)aContext.getConfigParameterValue(PARAM_QUESTION_VIEW_NAME);
		// 検索結果数の取得
		//resultNum	= (Integer)aContext.getConfigParameterValue(PARAM_RESULT_NUM);
		// 1パッセージあたりの文数取得
		passageLength = (Integer)aContext.getConfigParameterValue(PARAM_PASSAGE_LENGTH);
		// 1文書あたりの採用パッセージ数取得
		passageNumPerDoc = (Integer)aContext.getConfigParameterValue(PARAM_PASSGE_NUM_PER_DOC);
		// 採用する全パッセージ数取得
		passageNumAll = (Integer)aContext.getConfigParameterValue(PARAM_PASSGE_NUM_ALL);

		// 助数辞の読み込み
		setJosujiList("org/kachako/components/analysis_engine/suffix.txt");
	}

	private final RegexCache	regexCache		= new RegexCache();

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException
	{
System.out.println("--------InformationExtractor start---------");

		try {
			JCas questionViewCas	=  jCas.getView(questionViewName);

			// 質問文の取得
			Iterator<TOP> questionFSIterator = questionViewCas.getJFSIndexRepository().getAllIndexedFS(Question.type);

			String	questionText	= null;
			if(questionFSIterator.hasNext()) {
				Question	questionFS	= (Question)questionFSIterator.next();
				questionText	= questionFS.getCoveredText();
System.out.println("questionText=["+questionText+"]");
			}

			// TOPIC IDの取得
			String	topicId	= null;
			Iterator<TOP> sourceDocInfoFSIterator = questionViewCas.getJFSIndexRepository().getAllIndexedFS(SourceDocumentInformation.type);
			if(sourceDocInfoFSIterator.hasNext()) {
				// TOPIC_ID 取得
				topicId = ((SourceDocumentInformation) sourceDocInfoFSIterator.next()).getUri();
System.out.println("topic id="+topicId);
			}

			// 形態素解析結果取得
			Iterator<TOP> morphemeFSIterator = questionViewCas.getJFSIndexRepository().getAllIndexedFS(Morpheme.type);

			List<Morpheme> morphemeList = new ArrayList<>();
			while(morphemeFSIterator.hasNext()) {
				morphemeList.add((Morpheme)morphemeFSIterator.next());
			}

			// ソート
			Collections.sort(morphemeList, new Comparator<Morpheme>(){
				public int compare(Morpheme x, Morpheme y){
					return x.getBegin() - y.getBegin();
				}
			});

			HashMap<String,Integer> josujiMap	= getJosuji();
			// 質問文（クエリ）から抽出すべき情報を取得する。
			RequireInfo	requireInfo	= GetRequireInfo.get(questionText, morphemeList, josujiMap);
			String requireType	= requireInfo.getType();
			String pattern1	= requireInfo.getPattern1();
			String pattern2	= requireInfo.getPattern2();

			// キーワード取得
			Iterator<TOP> keytermFSIterator = questionViewCas.getJFSIndexRepository().getAllIndexedFS(KeyTerm.type);

			List<KeyTerm> keyTermList = new ArrayList<>();
			while(keytermFSIterator.hasNext()) {
				keyTermList.add((KeyTerm)keytermFSIterator.next());
			}

			// 検索結果（Document）取得
			Iterator<JCas> rsViewJCasIterator = jCas.getViewIterator(RS_RESULT_VIEW_NAME_PREFIX);
			if(rsViewJCasIterator != null) {
				if(rsViewJCasIterator.hasNext()) {
					// DocId(LineNum)とPassageのMap
					LinkedHashMap<String,Passage> lidPassageMap = new LinkedHashMap<String,Passage>();
					// DocId(LineNum)とViewのMap
					HashMap<String,JCas> lidViewMap	= new HashMap<String,JCas>();

					JCas	rsViewCas	= rsViewJCasIterator.next();
					//	String	rsViewName	= rsViewCas.getViewName();

					List<Document> documentList	= getDocumentList(rsViewCas);
					for(Document document : documentList) {
						String	docId	= document.getDocId();
						String	content	= document.getContent();
						int docBegin	= document.getDocBegin();
						int docEnd	= document.getDocEnd();
						String text	= content.substring(docBegin,docEnd);
						//String	content	= documentFS.getCoveredText();
						//double	score	= documentFS.getScore();
						//int		rank	= documentFS.getRank();

						// 文章を一文ずつに分ける。
						String[] textArray	= MakePassage.getTextLines(text, regexCache);

						LinkedHashMap<String,Passage> tempPassageMap	= new LinkedHashMap<String,Passage>(); 
						List<String> sentList	= new ArrayList<>();
						int lineNum	= 0;
						for(int index = 0; index < textArray.length; index++) {
							// パッセージ行数だけバッファに蓄える
							sentList.add(textArray[index]);
							while(sentList.size() > passageLength ) {
								sentList.remove(0);
							}
							lineNum++;
							List<String> newSentList	= new ArrayList<>();
							newSentList.addAll(sentList);

							// バッファの第一文にキーワードまたは必要な数値情報が存在するか，
							// パッセージを網羅する場合であれば，
							// パッセージを作成する
							String sentTop	= newSentList.get(0);
							List<Integer>	topKeyCount	= MakePassage.getKeywordCount(sentTop, keyTermList);
							List<Integer>	topNeCount		= MakePassage.getNeCount(sentTop, pattern1, pattern2);
							if(lineNum >= passageLength	&& 
									( ( passageNumPerDoc == 0 && passageNumAll == 0 )
											|| ( existCount(topKeyCount) || existCount(topNeCount) )
									)
								) {
								// 文書のスコアを計算する
								double score = MakePassage.getPassageScore(newSentList.toArray(new String[0]), docId, requireType, keyTermList, pattern1, pattern2);
								Passage	newPassage	= new Passage(score, newSentList);
								StringBuilder sb	= new StringBuilder();
								String lid	= sb.append(docId).append("_").append(lineNum - passageLength + 1).toString();
								tempPassageMap.put(lid, newPassage);
							}
						}
						// 現在のバッファでパッセージが作られていなければ、パッセージを作る
						if(lineNum < passageLength || tempPassageMap.size() <= 0) {
							// 文書のスコアを計算する
							double score = MakePassage.getPassageScore(sentList.toArray(new String[0]), docId, requireType, keyTermList, pattern1, pattern2);
							Passage	newPassage	= new Passage(score, sentList);
							StringBuilder sb	= new StringBuilder();
							String lid	= sb.append(docId).append("_").append(1).toString();
							tempPassageMap.put(lid, newPassage);
						}

						// ソート
						tempPassageMap	= sortPassage(tempPassageMap);

						// 上位のパッセージを全体の結果に加える
						//  passageNumPerDoc == 0 なら，全てのパッセージを加える
						int i	= 0;
						for(Entry<String,Passage> entry : tempPassageMap.entrySet()) {
							if(passageNumPerDoc != 0 && i >= passageNumPerDoc) {
								break;
							}
							String lid = entry.getKey();
							Passage passage	= entry.getValue();

							lidPassageMap.put(lid, passage);
							lidViewMap.put(lid, rsViewCas);
							++i;
						}
					}
					// 全体の結果をソートする
					lidPassageMap	= sortPassage(lidPassageMap);

					int passageNum	= 0;
//					int i = 0;
//					for(Entry<String,Passage> entry : lidPassageMap.entrySet()) {
					for(int i = 0 ; i < lidPassageMap.size(); i++) {
						passageNum++;
						i++;
						if(passageNumAll != 0 && i >= passageNumAll) {
							break;
						}
					}

					ViewId parentViewIdFS	= null;

					Iterator<TOP> viewIdFSIterator = rsViewCas.getJFSIndexRepository().getAllIndexedFS(ViewId.type);
					if(viewIdFSIterator.hasNext()) {
						parentViewIdFS	= (ViewId)viewIdFSIterator.next();
					}
					else {
						parentViewIdFS	= new ViewId(rsViewCas);
						parentViewIdFS.addToIndexes();						
					}
					FSArray fsChildren		= new FSArray(rsViewCas,passageNum);
//System.out.println("fsChildren size="+lineNum);

					int num	= 0;
					for(Entry<String,Passage> entry : lidPassageMap.entrySet()) {
						if(passageNumAll != 0 && num >= passageNumAll) {
							break;
						}
						String lid = entry.getKey();
						Passage passage	= entry.getValue();
						List<String> lineList	= passage.getSentList();
						double score	= passage.getScore();
						//JCas	rsViewCas = lidViewMap.get(lid);

						// View生成
						JCas ixViewCas	= jCas.createView(String.format("%s.%d", IX_RESULT_VIEW_NAME_PREFIX, num+1));

						//---- View間の依存関係を設定 ----
						ViewId childViewIdFS	= new ViewId(ixViewCas);
						childViewIdFS.addToIndexes();
						FSArray fsParents		= new FSArray(ixViewCas,1);
						fsParents.set(0,parentViewIdFS);
						fsChildren.set(num, childViewIdFS);

						childViewIdFS.setParents(fsParents);

						StringBuilder sb	= new StringBuilder();
						int beginIndex	= 0;
						int endIndex	= 0;
						for(int index = 0; index < lineList.size(); index++) {
							String line	= lineList.get(index);
//System.out.println("lid="+lid+" : "+line);
							beginIndex	= sb.length();
							sb.append(line);
							endIndex	= sb.length();
							AnswerCandidate answerCandidate = new AnswerCandidate(ixViewCas);
							answerCandidate.setDocId(lid);
							answerCandidate.setBegin(beginIndex);
							answerCandidate.setEnd(endIndex);
							answerCandidate.setScore(score);
							answerCandidate.setRank(num+1);
							answerCandidate.addToIndexes();
						}
						ixViewCas.setDocumentText(sb.toString());
						SourceDocumentInformation ixSofaSourceDocumentInformationFS =
								new SourceDocumentInformation(ixViewCas);
						ixSofaSourceDocumentInformationFS.addToIndexes();
						ixSofaSourceDocumentInformationFS.setUri(topicId);
						++num;
					}
//System.out.println("num="+num+" i="+i);
					parentViewIdFS.setChildren(fsChildren);
				}
			}
		} catch(CASException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException(e);
		}

		return;
	}

	/**
	 * 助数辞を読み込み、設定する.
	 * 
	 * @param path
	 * @throws ResourceInitializationException
	 */
	private void setJosujiList(String path) throws ResourceInitializationException
	{
		// 助数辞の読み込み
		BufferedReader reader = null;
		try {
			File	decisionFile	= resourceLoader.getResourceFile(Paths.get(path));
			if(decisionFile != null) {
				reader	= new BufferedReader(new FileReader(decisionFile));
				List<String> list	= new ArrayList<>();
				String line	= null;
				while((line = reader.readLine()) != null) {
					list.add(line);
				}
				josujiList	= list.toArray(new String[0]);
				reader.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
System.out.println("josujiList error:"+e);
			throw new ResourceInitializationException();
		} catch(Exception e) {
			e.printStackTrace();
System.out.println(e);
			throw new ResourceInitializationException();
		} finally {
			if(reader != null) {
				try {
					reader.close();
				} catch(IOException e) {
					e.printStackTrace();
					throw new ResourceInitializationException();
				}
			}
		}
		return;
	}

	/**
	 * 助数辞情報を取得する.
	 * 
	 * @return
	 */
	private HashMap<String,Integer> getJosuji()
	{
		if(josujiList == null) {
			return null;
		}

		HashMap<String,Integer>	map	= new HashMap<String,Integer>();
		for(String str : josujiList) {
			map.put(str, 1);
		}
		return map;
	}

	private List<Document> getDocumentList(JCas jCas)
	{
		Iterator<TOP> documentFSIterator = jCas.getJFSIndexRepository().getAllIndexedFS(Document.type);

		List<Document> documentList = new ArrayList<>();
		while(documentFSIterator.hasNext()) {
			documentList.add((Document)documentFSIterator.next());
		}

		if(documentList != null) {
			// ソート
			Collections.sort(documentList, new Comparator<Document>(){
				public int compare(Document x, Document y){
					return x.getBegin() - y.getBegin();
				}
			});

		}
		return documentList;
	}

	private boolean existCount(List<Integer> countList)
	{
		for(int count : countList) {
			if(count > 0) {
				return true;
			}
		}
		return false;
	}

	private static LinkedHashMap<String,Passage> sortPassage(LinkedHashMap<String,Passage> passageMap)
	{
		if(passageMap == null || passageMap.size() <= 0) {
			return null;
		}

		List<Map.Entry<String,Passage>> entries = new ArrayList<Map.Entry<String,Passage>>(passageMap.entrySet());
		Collections.sort(entries, new Comparator<Map.Entry<String,Passage>>() {
			public int compare(Map.Entry<String,Passage> o1, Map.Entry<String,Passage> o2){
				Map.Entry<String,Passage> e1 = o1;
				Map.Entry<String,Passage> e2 = o2;
				return ((Double)e2.getValue().getScore()).compareTo((Double)e1.getValue().getScore());
			}
		});

		LinkedHashMap<String,Passage> newMap	= new  LinkedHashMap<String,Passage>();
		for(Map.Entry<String,Passage> entry : entries) {
//System.out.println("lid="+entry.getKey()+" : score = "+entry.getValue().getScore()+" "+entry.getValue().getSentList());
			newMap.put(entry.getKey(), entry.getValue());
		}

		return newMap;
	}

	private class Passage
	{
		private double score		= 0;
		private List<String> sentList	= null;

		Passage(double score, List<String> sentList)
		{
			this.score		= score;
			this.sentList		= sentList;
		}

		public double getScore() {
			return score;
		}

		public List<String> getSentList() {
			return sentList;
		}
	}
}
