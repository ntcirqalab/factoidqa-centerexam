#Kachako factoidQA-センター試験解答器 利用方法

実行環境：  
実行するマシンのOSはWindows、Mac OS X、Linuxのいずれかです。

Java 7 (1.7.X_XX)の最新版がインストールされている必要があります。インストールされていない場合はOracleのサイト(https://java.com/download/)よりマシンにあったJREをダウンロードして事前にインストールしておいてください。

また実行には最低16GBの実メモリが必要となります。実際に必要なメモリ量は、一度に入力する問題数・検索結果の総数・形態素および係り受け解析の結果等により大きく異なります。お使いのマシンスペックに合わせて適宜設定やチューニングを試みてください。

## 1.Kachako起動用ファイルを取得
Webブラウザで http://kachako.org/file/kachako/kachako.jnlp にアクセスし、kachako.jnlpをPCに保存する。

例えばChromeを例にとると、上記URLをURLに入力すると、"This type of file can harm your computer. Do you want to keep kachako.jnlp anyway? " というダイアログが表示されるので [Keep] ボタンでDownload folderに設定した位置に保存されます。
  
ブラウザにWindowsのInternetExplorerを使用した場合は、上記URLへアクセスすると下部に『ファイルを開く』ボタンも表示されるので、kachako.jnlpを保存しないで直接起動することもできます。
  
コマンドラインから`javaws -viewer`を実行すると、設定画面が表示されます。複数のJavaがインストールされている場合は、どのJavaバージョンを用いるかの設定ができます。Java Console を表示するように設定しておくと、冒頭に利用中のバージョンが表示されます。これが最も確実に実行中の Java バージョンを確認する方法です。

## 2. kachako.jnlp からJWS機能でKachakoを起動
先ほど取得した kachako.jnlp をWindowsの場合はダブルクリックで起動、MacOS、LinuxではTerminal(コンソール)をから  kachako.jnlpを引数として


```
javaws kachako.jnlp
```

でKachakoを起動します。

なお、起動途中に、『このアプリケーションを実行しますか。』のダイアログが表示されますので『実行』ボタンをクリックしてください。ダイアログの『この発行者および前述の場所からのアプリケーションでは、次回から表示しない』のチェックボックスをONにすると次回からこのダイアログは表示されなくなるます。

無事起動するとKachakoの画面が表示されます。
## 3. kachako.orgからfactoidQA-センター試験解答器のコンポーネントを取得
Kachakoの Tool メニューの Edit Component Site... を選択し表示されるダイアログのURL欄に以下のURLを入力し、[Add...] ボタンを押すと追加するコンポーネントが表示されるので、全部のチェックボックスにチェックをして[OK]ボタンを押すと、使用するコンポーネントの情報が取得されます。

URL: http://kachako.org/file/kachako.org/descriptor_components/1.0/

を入れて [Add...] ボタン

## 4. 使用するコンポーネントを配置
KachakoのFileメニューから"Open Workspace..."で別途配布しているfactoidQA-センター試験解答器のワークスペースファイルを読み込みます。


## 5. 使用するコンポーネントの設定
環境に合わせて各種ファイルのある位置やディレクトリを指定します。ワークスペース上のコンポーネントの左端になる▼をクリックするとプルダウンメニューが表示されますので、"Edit Parameter"あるいは"Edit Parameter(New Dialog)"を選択し、パラメータ設定ダイアログで設定環境に合わせてパラメータを設定します。

各コンポーネントで設定する必要のある主な項目は以下のとおり

###ExamReader
   |   
----|----
InputDirectory | センター試験問題のあるフォルダ
AnswerTableDirectory | センター試験の解答ファイルのあるフォルダ
InputFileName | InputDirectoryにある問題のうち特定のファイルのみ実行する場合にファイル名を指定します。何も指定が無い場合はすべてのファイルが実行されます。

###CenterQuestionAnalysis
   |   
----|----
cabochaPath | インストールされているcabochaコマンドのパス
dbDirectory | CenterQuestionAnalysisで使用するデータベースのあるフォルダ
kachaoSystem_Vmargs | 実行Javaプロセスで使用するメモリ量、-Xms と -Xmxの次に使用するメモリ量を実行するコンピュータの搭載メモリにあった量で指定します。デフォルトは16GBが指定されています。

###KnpWrapper
   |   
----|----
jumanExecFile | インストールされているjumanコマンドのパスを指定します。
knpExecFile | インストールされているknpコマンドのパスを指定します。

###QueryAnalysisCE
特になし

###IndriAnalysisEngine
   |   
----|----
IndruIndicesFileDirectory | Indriのインデックスファイルのあるフォルダ
IndriCommandFile | インストールされているIndriのIndriRunQueryコマンドのパスを指定します。
IndriMaxDocs | Indriで取得するドキュメントの最大値を指定します。大きな値を指定すると実行時間が増加します。
DocumentDirectory | Indriで取得したドキュメントを保存するフォルダを指定します。

###InformationExtractorCE
特になし

###KnpWrapper 2
   |   
----|----
jumanExecFile | インストールされているjumanコマンドのパスを指定します。
knpExecFile | インストールされているknpコマンドのパスを指定します。

###CabochaWrapper
   |   
----|----
cabochaExecFile | インストールされているcabochaコマンドのパスを指定します。

###FindAnswers
   |   
----|----
AnsNum | 解答数を指定します。
AnsNumLimit | 解答数のリミットを指定します。


###CenterAnswerAnalysis
   |   
----|----
cabochaPath | インストールされているcabochaコマンドのパスを指定します。
dbDirectory | CenterAnswerAnalysisで使用するデータベースのあるフォルダCenterQuestionAnalysisのものと同じになります。

###※ワークフローで使用するメモリ量の設定方法
作成したワークフローを実行するときに、入力される問題数や内部でIndriにより検索した文書数に応じて必要メモリ量が増大します。Kachakoではワークフロー実行に使用するメモリ量は、ワークフローで使用されるコンポーネント内に設定されているメモリ量のうち最大のものが使用されるようになっています。今回配布しているfactoidQA-センター試験解答器のワークスペースファイルでは、CenterQuestionAnalysisコンポーネントで最大使用メモリ量を設定していますのでCenterQuestionAnalysisコンポーネントのパラメータ設定ダイアログのKachakoSystem_Vmargsの項のリストの-Xms○g及び-Xmx○gをクリックし、鉛筆の編集アイコンをクリックすることで値の編集が可能となりますので必要な値に変更してください(例えば-Xms16g -Xmx16gとした場合は16GBを指定したことになります)。
   

## 6. ワークフローの実行
コンポーネントのパラメータの設定が終了したら、ワークフローを実行します。ツールバーの[Run▼]を押すとプルダウンメニューが表示されるので"Run Locally"を選択するとワークフローが実行されます。実行に必要なコンポーネントのコードは、実行時に自動でKachako.orgからダウンロードされます。


## 7. 結果表示
ワークフローの実行が終わるとKachakoの画面が自動でResult画面に切り替わります。上部に表示されるテーブルから"Show"ボタンを押すと実行結果が表示され各種情報を参照することができます。
    
## 8. アンインストール
Kachakoのプログラムは、ダウンロードしたファイルをユーザのホームディレクトリ(Windowsでは、ユーザフォルダ）に .kachako、.ivy2のフォルダを作成し、その下に保存しています。また、それ以外にJavaが持っているキャッシュにもキャッシュファイルが保存されています。アンインストールする場合は、.kachako、.ivy2以下のファイルをすべて削除します。また、Javaのキャッシュは、Javaのコントロールパネル中（Linuxの場合は"ControlPanel"コマンドで起動）のGeneralタブの "Temporary Internet Files"の View...ボタンでJava Cache Viewerを表示し、Applicationの中からKachakoを削除します。
    
## 9. ライセンスおよび制作の経緯
本ソフトウェアのうち、日本語質問応答システム部分は横浜国立大学の森教授よりPerlコードのご提供をいただき、Java および UIMA 準拠、Kachako 対応に再構築したものです。また、質問応答システムのうち検索システム関連の部分はカーネギーメロン大学の三田村教授よりいただいたソースコードを基にしています。質問応答システムを用いてセンター試験の解答を行う部分は、国立情報学研究所の石下研究員の実装を元に Java および UIMA 準拠、Kachako 対応に再構築したものです。これらの再構築・互換コンポーネント化を含め、Kachako 関連の部分（プラットフォーム、データ型階層、その他のコンポーネント 等）は UIMA 準拠の汎用言語処理システムとして狩野が開発しているものです。

以下にライセンス文書（日本語版）のコピーを貼り付けます。異なるライセンスのものが含まれていますので取り扱いにご注意ください。
	
この文書は qalab-workspace で配布されるソフトウェアのライセンス概要について記述しています。配布されるすべてのソフトウェアは”AS IS"で無保証、無サポートです。ご自身の責任でお使いください。

配布されるUIMAコンポーネント（日本語質問応答システムおよびセンター試験問題リーダー）については、Apache License 2.0 で配布いたします。

Kachako プラットフォームおよびその type system は研究目的であれば無償でお使いいただけます。ご利用は内部利用に限定し、デコンパイルや修正、再配布は行わないでください。Kachako についての詳細は http://kachako.org/ をご参照ください。

サードパーティライブラリについては、これら以外のオープンソースライブラリのものが含まれる可能性があります。

2014年8月11日  
狩野　芳伸  
http://kachako.org/kano/